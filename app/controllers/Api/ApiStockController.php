<?php

class ApiStockController extends Controller {

	public function stock(){
		$stock = Stock::orderBy('roba_id','asc')->orderBy('orgj_id','asc')->get();

		return Response::json($stock,200);
	}


	public function setStock(){
        $data = Input::get();
        $regex = AdminSupport::regex();

        if(!isset($data['data']) || !is_array($data['data']) || count($data['data']) == 0){
            return Response::json(['success'=>false,'messages'=>'Input data is not set.'],200);
        }
        $data = $data['data'];

        if(count($data) > 20000){
        	return Response::json(['success'=>false,'messages'=>'Max input vars is 20000.'],200);
        }

        foreach($data as $row){
	        $rules = array(
	        	'id' => 'required|exists:roba,id_is',
		    	'location_id' => 'required|exists:orgj,orgj_id',
		    	'quantity' => 'required|numeric|digits_between:1,15'
	        );
	        $validator = Validator::make($row,$rules);
	        if($validator->fails()){
	        	return Response::json(['success'=>false,'messages'=>$validator->messages()],200);
	        	break;
	        }
        }

        $mappedByExternalCode = Article::mappedByExternalCode();

        foreach($data as $row){
	        $stock = Stock::where(['roba_id' => $mappedByExternalCode[$row['id']], 'orgj_id' => $row['location_id'], 'poslovna_godina_id'=>2014])->first();

        	if(is_null($stock)){
	            $stock = new Stock();
                $stock->poslovna_godina_id = 2014;
	            $stock->roba_id = $mappedByExternalCode[$row['id']];
                $stock->orgj_id = $row['location_id'];
        	    $stock->kolicina = $row['quantity'];
                $stock->save();
            }else{
                $updateData = [
                    'kolicina' => $row['quantity']
                ];
                DB::table('lager')->where(['roba_id' => $mappedByExternalCode[$row['id']], 'orgj_id' => $row['location_id'], 'poslovna_godina_id'=>2014])->update($updateData);
            }
            
        }

        return Response::json(['success'=>true],200);
	}

    public function stockDelete(){
        $data = Input::get();

        if(count($data) > 20000){
            return Response::json(['success'=>false,'messages'=>'Max input vars is 20000.'],200);
        }

        if(!isset($data[0])){
            return Response::json(['success'=>false,'messages'=>'Array of inputs is required.'],200);
        }

        foreach($data as $row){
            $rules = array(
                'id' => 'required|integer|exists:roba,roba_id',
                'location_id' => 'required|integer|exists:orgj,orgj_id'
            );
            $validator = Validator::make($row,$rules);
            if($validator->fails()){
                return Response::json(['success'=>false,'messages'=>$validator->messages()],200);
                break;
            }
        }

        foreach($data as $row){
            Stock::where(['roba_id' => $row['id'], 'orgj_id' => $row['location_id']])->delete();
        }        

        return Response::json(['success'=>true],200);
    }

}
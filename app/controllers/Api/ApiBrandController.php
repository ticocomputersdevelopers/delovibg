<?php

class ApiBrandController extends Controller {

	public function brands(){
		$brands = Brand::where('proizvodjac_id','>',0)->orderBy('naziv','asc')->get();

		return Response::json($brands,200);
	}

	public function setBrands(){
        $data = Input::get();
        $regex = AdminSupport::regex();

        if(!isset($data['data']) || !is_array($data['data']) || count($data['data']) == 0){
            return Response::json(['success'=>false,'messages'=>'Input data is not set.'],200);
        }
        $data = $data['data'];

        if(count($data) > 20000){
        	return Response::json(['success'=>false,'messages'=>'Max input vars is 20000.'],200);
        }
        if(DB::table('proizvodjac')->count() > 5000){
        	return Response::json(['success'=>false,'messages'=>'Max number of brands is 5000.'],200);
        }

        foreach($data as $row){
	        $rules = array(
	        	// 'id' => 'integer|exists:proizvodjac,proizvodjac_id',
		    	'id' => 'required|max:255',
		    	'name' => 'required|regex:'.$regex.'|max:100'
	        );
	        $validator = Validator::make($row,$rules);
	        if($validator->fails()){
	        	return Response::json(['success'=>false,'messages'=>$validator->messages()],200);
	        	break;
	        }
        }

        $mappedBrands = Brand::mappedByExternalCode();
        foreach($data as $row){
        	$brand = null;
        	if(isset($row['id'])){
	        	$brand = Brand::where('id_is',$row['id'])->first();
        	}

        	if(is_null($brand)){
	            // $nextId = Brand::max('proizvodjac_id')+1;
	            $brand = new Brand();
	            // $brand->proizvodjac_id = $nextId;
                $brand->id_is = $row['id'];
            	// $brand->sifra = $nextId;
	        	$brand->naziv = !empty($row['name']) ? $row['name'] : (isset($row['id']) ? $row['id'] : '');

        	}else{
        		if(isset($row['name'])){
		    		$brand->naziv = $row['name'];
        		}
        	}
        	

        	$brand->save();
        }

        return Response::json(['success'=>true],200);
	}

    public function brandsDelete(){
        $data = Input::get();
        
        if(!isset($data['data']) || !is_array($data['data']) || count($data['data']) == 0){
            return Response::json(['success'=>false,'messages'=>'Input data is not set.'],200);
        }
        $data = $data['data'];

        if(count($data) > 20000){
            return Response::json(['success'=>false,'messages'=>'Max input vars is 20000.'],200);
        }
        if(DB::table('proizvodjac')->count() > 5000){
            return Response::json(['success'=>false,'messages'=>'Max number of brands is 5000.'],200);
        }

        if(isset($data[0]['id'])){
            foreach($data as $row){
                $rules = array(
                    'id' => 'required|max:255'
                );
                $validator = Validator::make($row,$rules);
                if($validator->fails()){
                    return Response::json(['success'=>false,'messages'=>$validator->messages()],200);
                    break;
                }
            }
        }else{
            return Response::json(['success'=>false,'messages'=>'Element require id or id.'],200);
        }


        if(isset($data[0]['id'])){
            $codes = array_map(function($item){ return $item['id']; },$data);
            DB::table('proizvodjac')->whereIn('id_is',$codes)->delete();
        }

        return Response::json(['success'=>true],200);
    }

}
<?php

class ApiArticleController extends Controller {

	public function products(){
        $data = Input::get();
        $rules = array(
        	'page' => 'integer|min:1',
        	'limit' => 'integer|max:1000',
        	'id' => 'integer|exists:roba,roba_id',
        	'category_id' => 'integer|exists:grupa_pr,grupa_pr_id',
        	'brand_id' => 'integer|exists:proizvodjac,proizvodjac_id',
            'name' => 'regex:'.AdminSupport::regex().'|max:300'
        );
        $validator = Validator::make($data,$rules);
        if($validator->fails()){
        	return Response::json(['success'=>false,'messages'=>$validator->messages()],200);
        }

        $page = Input::get('page') ? intval(Input::get('page')) : 1;
        $limit = Input::get('limit') ? intval(Input::get('limit')) : 100;


		$articlesQuery = new Article();
		if(!is_null($id = Input::get('id'))){
			$articlesQuery = $articlesQuery->where('roba_id',$id);
		}
		if(!is_null($categoryId = Input::get('category_id'))){
		    $categoryIds=array();
		    Groups::allGroups($categoryIds,$categoryId);			
			$articlesQuery = $articlesQuery->whereIn('grupa_pr_id',$categoryIds);
		}
		if(!is_null($brandId = Input::get('brand_id'))){			
			$articlesQuery = $articlesQuery->where('proizvodjac_id',$brandId);
		}
		if(!empty($name = Input::get('name'))){
			$articlesQuery = $articlesQuery->whereRaw("naziv ilike '%".$name."%'");
		}

		$articles = $articlesQuery->limit($limit)->offset((($page-1)*$limit))->get()->toArray();


		return Response::json($articles,200);
	}

	public function setProducts(){
        $data = Input::get();
        $regex = AdminSupport::regex();

        if(!isset($data['data']) || !is_array($data['data']) || count($data['data']) == 0){
            return Response::json(['success'=>false,'messages'=>'Input data is not set.'],200);
        }
        $data = $data['data'];

        if(count($data) > 20000){
        	return Response::json(['success'=>false,'messages'=>'Max input vars is 20000.'],200);
        }
        if(DB::table('roba')->count() > 50000){
        	return Response::json(['success'=>false,'messages'=>'Max number of articles is 50000.'],200);
        }
        $mappedCategories = Category::mappedByExternalCode();
        $mappedBrands = Brand::mappedByIdAndExternalCode();

        foreach($data as $row){
	        $rules = array(
	        	// 'id' => 'integer|exists:roba,roba_id',
	        	'ean' => 'max:30',
		    	'id' => 'max:255',
                'product_code' => 'max:255',
		    	'brand_id' => 'exists:proizvodjac,id_is',
		    	'category_id' => (isset($row['subcategory_id']) ? 'required|' : '').'exists:grupa_pr,id_is,parrent_grupa_pr_id,0',
		    	'subcategory_id' => 'exists:grupa_pr,id_is',
		    	'base_price' => 'numeric|digits_between:1,15',
		    	'action_price' => 'numeric|digits_between:1,15',
		    	'status' => 'in:0,1',
		    	'action_offer' => 'in:0,1',
		    	'is_part' => 'in:0,1',
		    	'name' => ((isset($row['id']) || isset($row['id'])) ? '' : 'required|').'regex:'.$regex.'|max:300',
	        	'summary' => 'max:200',
		    	'description' => 'max:5000',
		    	// 'service_recommendation' => 'in:0,1',
		    	'old_price' => 'numeric|digits_between:1,15',
		    	'new_product' => 'in:0,1',
		    	'recommended_products' => 'max:255',
                'model' => 'max:2000',
	        	'for_brand' => 'integer',
	        	'for_device' => 'integer'
	        );
	        $validator = Validator::make($row,$rules);
	        if($validator->fails()){
	        	return Response::json(['success'=>false,'messages'=>$validator->messages()],200);
	        	break;
	        }
        }

        foreach($data as $row){
        	$article = null;
        	if(isset($row['id'])){
	        	$article = Article::where('id_is',$row['id'])->first();
        	}
        	if(is_null($article)){
        		$article = new Article;

		    	$article->barkod = isset($row['ean']) ? $row['ean'] : null;
		    	$article->proizvodjac_id = isset($row['brand_id']) && isset($mappedBrands[$row['brand_id']]) ? $mappedBrands[$row['brand_id']] : -1;
		    	$article->grupa_pr_id = isset($row['subcategory_id']) ? (isset($mappedCategories[$row['subcategory_id']]) ? $mappedCategories[$row['subcategory_id']] : -1) : (isset($row['category_id']) ? (isset($mappedCategories[$row['category_id']]) ? $mappedCategories[$row['category_id']] : -1) : -1);
		    	$article->web_cena = isset($row['base_price']) ? $row['base_price'] : 0.00;
		    	$article->akcijska_cena = isset($row['action_price']) ? $row['action_price'] : 0.00;
		    	$article->flag_prikazi_u_cenovniku = isset($row['status']) ? $row['status'] : 0;
		    	$article->akcija_flag_primeni = isset($row['action_offer']) ? $row['action_offer'] : 0;
		    	$article->naziv = !empty($row['name']) ? $row['name'] : (isset($row['id']) ? $row['id'] : '');
		    	$article->naziv_web = !empty($row['name']) ? $row['name'] : (isset($row['id']) ? $row['id'] : '');
		    	$article->web_opis = isset($row['description']) ? $row['description'] : null;
		    	$article->stara_cena = isset($row['old_price']) ? $row['old_price'] : 0.00;
		    	$article->model = isset($row['models']) ? $row['models'] : null;
                $article->sifra_is = isset($row['product_code']) ? $row['product_code'] : null;

		    	$article->flag_aktivan = 1;
        	}else{
        		if(isset($row['ean'])){
		    		$article->barkod = $row['ean'];
        		}
		    	if(isset($row['brand_id']) && isset($mappedBrands[$row['brand_id']])){
		    		$article->proizvodjac_id = $mappedBrands[$row['brand_id']];
		    	}
		    	// if(isset($row['subcategory_id'])){
		    	// 	$article->grupa_pr_id = isset($row['subcategory_id']) ? (isset($mappedCategories[$row['subcategory_id']]) ? $mappedCategories[$row['subcategory_id']] : -1) : (isset($row['category_id']) ? (isset($mappedCategories[$row['category_id']]) ? $mappedCategories[$row['category_id']] : -1) : -1);
		    	// }
        		if(isset($row['base_price'])){
		    		$article->web_cena = $row['base_price'];
        		}
        		if(isset($row['action_price'])){
		    		$article->akcijska_cena = $row['action_price'];
        		}
        		if(isset($row['status'])){
		    		$article->flag_prikazi_u_cenovniku = $row['status'];
        		}
        		if(isset($row['action_offer'])){
		    		$article->akcija_flag_primeni = $row['action_offer'];
        		}
       //  		if(isset($row['name'])){
		    	// 	$article->naziv = $row['name'];
		    	// 	$article->naziv_web = $row['name'];
		    	// }
        // 		if(isset($row['description'])){
		    		// $article->web_opis = $row['description'];
        // 		}
        		if(isset($row['old_price'])){
		    		$article->stara_cena = $row['old_price'];
        		}
        // 		if(isset($row['models'])){
		    		// $article->model = $row['models'];
        // 		}
                if(isset($row['product_code'])){
                    $article->sifra_is = isset($row['product_code']) ? $row['product_code'] : null;
                }
    		
        	}

			if(isset($row['id'])){
	        	$article->id_is = $row['id'];
        	}

        	$article->save();
	    }

        return Response::json(['success'=>true],200);
	}

    public function articlesDelete(){
        $data = Input::get();
        if(!isset($data['data']) || !is_array($data['data']) || count($data['data']) == 0){
            return Response::json(['success'=>false,'messages'=>'Input data is not set.'],200);
        }
        $data = $data['data'];

        if(count($data) > 20000){
            return Response::json(['success'=>false,'messages'=>'Max input vars is 20000.'],200);
        }
        if(DB::table('roba')->count() > 50000){
            return Response::json(['success'=>false,'messages'=>'Max number of articles is 50000.'],200);
        }

        if(isset($data[0]['id'])){
            foreach($data as $row){
                $rules = array(
                    'id' => 'required|integer'
                );
                $validator = Validator::make($row,$rules);
                if($validator->fails()){
                    return Response::json(['success'=>false,'messages'=>$validator->messages()],200);
                    break;
                }
            }
        }
        else if(isset($data[0]['product_code'])){
            foreach($data as $row){
                $rules = array(
                    'product_code' => 'required|max:255'
                );
                $validator = Validator::make($row,$rules);
                if($validator->fails()){
                    return Response::json(['success'=>false,'messages'=>$validator->messages()],200);
                    break;
                }
            }
        }else{
            return Response::json(['success'=>false,'messages'=>'Element require id or product_code.'],200);
        }


        if(isset($data[0]['id'])){
            $ids = array_map(function($item){ return $item['id']; },$data);
            DB::table('roba')->whereIn('roba_id',$ids)->delete();
        }
        else if(isset($data[0]['product_code'])){
            $codes = array_map(function($item){ return $item['product_code']; },$data);
            DB::table('roba')->whereIn('id_is',$codes)->delete();
        }

        return Response::json(['success'=>true],200);
    }

}

<?php

class PartnerExportController extends Controller {

	public function xml(){
		$korisnickoime = Input::get('korisnickoime');
		$password = Input::get('lozinka');
		$currency = Input::get('valuta');
		$description = Input::get('opis');
		$chars = Input::get('karakteristike');
		$imgs = Input::get('slike');
		$word = Input::get('kljucnarec');
		$category = Input::get('kategorija');
		$parentCategory = Input::get('nadkategorija');
		$manufacturer = Input::get('proizvodjac');
		$code = Input::get('sifra');
		$quantity = Input::get('kolicina');
		$short = Input::get('kratak');

		$baseUrl = Options::domain();
		$partner = DB::table('partner')->where(array('api_username'=>$korisnickoime,'api_password'=>$password,'aktivan_api'=>1))->whereNotNull('api_username')->whereNotNull('api_password')->first();

		$kurs = 1;
		$valuta = 'RSD';
		if(!empty($currency) && strtolower($currency) == 'eur'){
			$kurs = B2bOptions::kurs();
			$valuta = 'EUR';
		}
		$checkDescription = false;
		if(!empty($description) && $description == '1'){
			$checkDescription = true;
		}
		$checkCharacteristics = false;
		if(!empty($chars) && $chars == '1'){
			$checkCharacteristics = true;
		}
		$checkImages = false;
		if(!empty($imgs) && $imgs == '1'){
			$checkImages = true;
		}
		$checkQuantity = false;
		if(!empty($quantity) && $quantity == '1'){
			$checkQuantity = true;
		}
		$checkShort = false;
		if(!empty($short) && $short == '1'){
			$checkShort = true;
		}


		if(!$checkShort){
			$query = "SELECT r.roba_id, sifra_is, web_cena, mpcena, racunska_cena_nc, naziv_web, web_flag_karakteristike, web_karakteristike, tarifna_grupa_id, web_opis, grupa_pr_id, barkod, model, (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = (SELECT parrent_grupa_pr_id FROM grupa_pr WHERE grupa_pr_id = r.grupa_pr_id LIMIT 1) AND grupa_pr_id > 0) AS nadgrupa, (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = r.grupa_pr_id) AS grupa, (SELECT naziv FROM jedinica_mere WHERE jedinica_mere_id = r.jedinica_mere_id) AS jedinica_mere, (SELECT naziv FROM proizvodjac WHERE proizvodjac_id = r.proizvodjac_id) AS proizvodjac, (SELECT SUM(kolicina-rezervisano) FROM lager l WHERE l.roba_id=r.roba_id AND orgj_id = (SELECT orgj_id FROM imenik_magacin WHERE imenik_magacin_id = 20 LIMIT 1) GROUP BY roba_id, orgj_id) AS kolicina FROM roba r WHERE r.flag_aktivan = 1 AND r.flag_cenovnik = 1 ";
		}else{
			$query = "SELECT r.roba_id, sifra_is, web_cena, mpcena, racunska_cena_nc, (SELECT SUM(kolicina-rezervisano) FROM lager l WHERE l.roba_id=r.roba_id AND orgj_id = (SELECT orgj_id FROM imenik_magacin WHERE imenik_magacin_id = 20 LIMIT 1) GROUP BY roba_id, orgj_id) AS kolicina FROM roba r WHERE r.flag_aktivan = 1 AND r.flag_cenovnik = 1  ";
		}

		if(!empty($word)){
			$query .= " AND naziv_web ILIKE '%".strval($word)."%'";
		}
		if(!empty($category)){
			$query .= " AND grupa_pr_id = (SELECT grupa_pr_id FROM grupa_pr WHERE grupa ILIKE '%".strval($category)."%' LIMIT 1)";
		}
		if(!empty($parentCategory)){
			$query .= " AND grupa_pr_id IN (SELECT grupa_pr_id FROM grupa_pr WHERE parrent_grupa_pr_id = (SELECT grupa_pr_id FROM grupa_pr WHERE grupa ILIKE '%".strval($parentCategory)."%' LIMIT 1))";
		}
		if(!empty($manufacturer)){
			$query .= " AND proizvodjac_id = (SELECT proizvodjac_id FROM proizvodjac WHERE naziv ILIKE '%".strval($manufacturer)."%' LIMIT 1)";
		}
		if($checkQuantity){
			$query .= " AND r.roba_id IN (SELECT roba_id FROM lager WHERE (kolicina-rezervisano) > 0 AND orgj_id = (SELECT orgj_id FROM imenik_magacin WHERE imenik_magacin_id = 20 LIMIT 1))";
		}
		if(!empty($code) && is_numeric($code)){
			$query .= " AND r.sifra_is = '".strval($code)."'";
		}
		
		try {
			$products = DB::select($query);
		} catch (Exception $e) {
			return Response::make('Bad Request', 400);
		}

		$xml = new DOMDocument("1.0","UTF-8");
		$root = $xml->createElement("artikli");
		$xml->appendChild($root);
		foreach($products as $article){
			$b2bRabatCene = B2bArticle::b2bRabatCene($article->roba_id,$partner->partner_id);

			$product   = $xml->createElement("artikal");

		    PartnerExport::xml_node($xml,"sifra",$article->sifra_is,$product);
			if(!$checkShort){
			    PartnerExport::xml_node($xml,"barkod",$article->barkod,$product);
			    PartnerExport::xml_node($xml,"naziv",PartnerExport::string_format($article->naziv_web),$product);
			    PartnerExport::xml_node($xml,"pdv",AdminSupport::find_tarifna_grupa($article->tarifna_grupa_id,'porez'),$product);
			    PartnerExport::xml_node($xml,"nadgrupa",PartnerExport::string_format($article->nadgrupa),$product);
			    PartnerExport::xml_node($xml,"grupa",PartnerExport::string_format($article->grupa),$product);
			    PartnerExport::xml_node($xml,"proizvodjac",PartnerExport::string_format($article->proizvodjac),$product);
			    PartnerExport::xml_node($xml,"jedinica_mere",PartnerExport::string_format($article->jedinica_mere),$product);
			    PartnerExport::xml_node($xml,"model",PartnerExport::string_format($article->model),$product);
			}
		    PartnerExport::xml_node($xml,"kolicina",($article->kolicina > 10 ? '10+' : round($article->kolicina)),$product);
		    PartnerExport::xml_node($xml,"valuta",$valuta,$product);
		    // PartnerExport::xml_node($xml,"osnovna_cena", ($b2bRabatCene->osnovna_cena / $kurs) ,$product);
		    // PartnerExport::xml_node($xml,"rabat", $b2bRabatCene->ukupan_rabat ,$product);
		    // PartnerExport::xml_node($xml,"cena_rabat", ($b2bRabatCene->cena_sa_rabatom / $kurs) ,$product);
		    // PartnerExport::xml_node($xml,"cena_pdv", ($b2bRabatCene->ukupna_cena / $kurs) ,$product);
		    PartnerExport::xml_node($xml,"cena", round(($b2bRabatCene->cena_sa_rabatom / $kurs),2) ,$product);
		    PartnerExport::xml_node($xml,"mpcena", ($article->mpcena / $kurs) ,$product);

			if(!$checkShort){
			    if($checkDescription){
				    PartnerExport::xml_node($xml,"opis",PartnerExport::string_format($article->web_opis).'<br>'.($article->web_flag_karakteristike == 0 ? PartnerExport::string_format($article->web_karakteristike) : ''),$product);
				}

			    if($checkImages){
				    $images   = $xml->createElement("slike");
				    foreach(PartnerExport::slike($article->roba_id) as $slika){
					    PartnerExport::xml_node($xml,"slika",$baseUrl.$slika,$images);
					}
				    $product->appendChild($images);
				}

			    if($checkCharacteristics){
				    $characteristics   = $xml->createElement("karakteristike");
				    foreach(PartnerExport::characteristics($article->roba_id,$article->web_flag_karakteristike) as $karakteristika){
				    	$characteristic_group = $xml->createElement("karakteristike_grupa");
				    	$characteristic_group->setAttribute("ime", PartnerExport::string_format($karakteristika->group));
				    	
				    	foreach($karakteristika->characteristic as $karak){
				    		$karakteristika_xml = $xml->createElement("karakteristika");
				    		$karakteristika_xml->setAttribute("ime", PartnerExport::string_format($karak->name));
				    		PartnerExport::xml_node($xml,"vrednost",PartnerExport::string_format($karak->value),$karakteristika_xml);
				    		$characteristic_group->appendChild($karakteristika_xml);
				    	}
				    	$characteristics->appendChild($characteristic_group);
					}
				    $product->appendChild($characteristics);
				}
			}

			$root->appendChild($product);

		}

		$xml->formatOutput = true;
		$content = $xml->saveXML();

		$response = Response::make($content,200);
		$response->header('Content-Type', 'text/xml');
		return $response;
	}

}
<?php

class ApiCategoryController extends Controller {

	public function mainCategories(){
		$categories = Category::where('grupa_pr_id','>',0)->where(['parrent_grupa_pr_id'=>0])->orderBy('redni_broj','asc')->get();

		return Response::json($categories,200);
	}

    public function subcategories(){
        $categories = Category::where('grupa_pr_id','>',0)->where('parrent_grupa_pr_id','!=',0)->orderBy('redni_broj','asc')->get();

        return Response::json($categories,200);
    }

	public function setCategories(){
        $data = Input::get();
        $regex = AdminSupport::regex();

        if(!isset($data['data']) || !is_array($data['data']) || count($data['data']) == 0){
            return Response::json(['success'=>false,'messages'=>'Input data is not set.'],200);
        }
        $data = $data['data'];

        if(count($data) > 20000){
        	return Response::json(['success'=>false,'messages'=>'Max input vars is 20000.'],200);
        }
        if(DB::table('grupa_pr')->count() > 5000){
        	return Response::json(['success'=>false,'messages'=>'Max number of categories is 5000.'],200);
        }

        foreach($data as $row){
	        $rules = array(
	        	// 'id' => 'integer|exists:grupa_pr,grupa_pr_id',
		    	'id' => 'required|max:255',
		    	'name' => 'regex:'.$regex.'|max:200',
	        	'parent_id' => 'max:255|exists:grupa_pr,id_is,parrent_grupa_pr_id,0',
		    	'status' => 'in:0,1',
		    	'description' => 'max:2000'
	        );
	        $validator = Validator::make($row,$rules);
	        if($validator->fails()){
	        	return Response::json(['success'=>false,'messages'=>$validator->messages()],200);
	        	break;
	        }
        }

        $mappedCategories = Category::mappedByExternalCode();
        foreach($data as $row){
        	$category = null;
        	if(isset($row['id'])){
	        	$category = Category::where('id_is',$row['id'])->first();
        	}

        	if(is_null($category)){
	            $nextId = Category::max('grupa_pr_id')+1;
	            $category = new Category();
	            $category->grupa_pr_id = $nextId;
                $category->id_is = $row['id'];
                $category->sifra = $nextId;
                $category->grupa = !empty($row['name']) ? $row['name'] : (isset($row['id']) ? $row['id'] : '');
                $category->web_b2c_prikazi = isset($row['status']) ? $row['status'] : 0;
                $category->opis = isset($row['description']) ? $row['description'] : null;

                $category->parrent_grupa_pr_id = 0;
        	}else{
        // 		if(isset($row['name'])){
		    		// $category->grupa = $row['name'];
        // 		}
        // 		if(isset($row['status'])){
		    		// $category->web_b2c_prikazi = $row['status'];
        // 		}
        // 		if(isset($row['description'])){
		    		// $category->opis = $row['description'];
        // 		}
        	}

            if(isset($row['parent_id']) && isset($mappedCategories[$row['parent_id']])){
                $category->parrent_grupa_pr_id = $mappedCategories[$row['parent_id']];
            }else{
                $category->parrent_grupa_pr_id = 0;
            }        	

        	$category->save();
        }

        return Response::json(['success'=>true],200);
	}

    public function setSubcategories(){
        $data = Input::get();
        $regex = AdminSupport::regex();

        if(!isset($data['data']) || count($data['data']) == 0){
            return Response::json(['success'=>false,'messages'=>'Input data is not set.'],200);
        }
        $data = $data['data'];

        if(count($data) > 20000){
            return Response::json(['success'=>false,'messages'=>'Max input vars is 20000.'],200);
        }
        if(DB::table('grupa_pr')->count() > 5000){
            return Response::json(['success'=>false,'messages'=>'Max number of categories is 5000.'],200);
        }

        foreach($data as $row){
            $rules = array(
                // 'id' => 'integer|exists:grupa_pr,grupa_pr_id',
                'id' => 'required|max:255',
                'name' => 'regex:'.$regex.'|max:200',
                'category_id' => 'required|max:255|exists:grupa_pr,id_is,parrent_grupa_pr_id,0',
                'parent_id' => 'max:255|exists:grupa_pr,id_is',
                'status' => 'in:0,1',
                'description' => 'max:2000'
            );
            $validator = Validator::make($row,$rules);
            if($validator->fails()){
                return Response::json(['success'=>false,'messages'=>$validator->messages()],200);
                break;
            }
        }

        $mappedCategories = Category::mappedByExternalCode();
        foreach($data as $row){
            $category = null;
            if(isset($row['id'])){
                $category = Category::where('id_is',$row['id'])->first();
            }

            if(is_null($category)){
                $nextId = Category::max('grupa_pr_id')+1;
                $category = new Category();
                $category->grupa_pr_id = $nextId;
                $category->id_is = $row['id'];
                $category->sifra = $nextId;
                $category->grupa = !empty($row['name']) ? $row['name'] : (isset($row['id']) ? $row['id'] : '');
                $category->web_b2c_prikazi = isset($row['status']) ? $row['status'] : 0;
                $category->opis = isset($row['description']) ? $row['description'] : null;

                if(isset($row['parent_id']) && isset($mappedCategories[$row['parent_id']]) && !empty($mappedCategories[$row['parent_id']])){
                    $category->parrent_grupa_pr_id = $mappedCategories[$row['parent_id']];
                }else if(isset($row['category_id']) && isset($mappedCategories[$row['category_id']]) && !empty($mappedCategories[$row['category_id']])){
                    $category->parrent_grupa_pr_id = $mappedCategories[$row['category_id']];
                }else{
                    $category->parrent_grupa_pr_id = 0;
                }
            }else{
                if(isset($row['name'])){
                    $category->grupa = $row['name'];
                }
                if(isset($row['status'])){
                    $category->web_b2c_prikazi = $row['status'];
                }
                if(isset($row['description'])){
                    $category->opis = $row['description'];
                }
                if(isset($row['parent_id']) && isset($mappedCategories[$row['parent_id']]) && !empty($mappedCategories[$row['parent_id']])){
                    $category->parrent_grupa_pr_id = $mappedCategories[$row['parent_id']];
                }else if(isset($row['category_id']) && isset($mappedCategories[$row['category_id']]) && !empty($mappedCategories[$row['category_id']])){
                    $category->parrent_grupa_pr_id = $mappedCategories[$row['category_id']];
                }else{
                    $category->parrent_grupa_pr_id = 0;
                }
            }
            

            $category->save();
        }

        return Response::json(['success'=>true],200);
    }

    public function categoriesDelete(){
        $data = Input::get();
        if(!isset($data['data']) || !is_array($data['data']) || count($data['data']) == 0){
            return Response::json(['success'=>false,'messages'=>'Input data is not set.'],200);
        }
        $data = $data['data'];

        if(count($data) > 20000){
            return Response::json(['success'=>false,'messages'=>'Max input vars is 20000.'],200);
        }
        if(DB::table('grupa_pr')->count() > 5000){
            return Response::json(['success'=>false,'messages'=>'Max number of categories is 5000.'],200);
        }

        if(isset($data[0]['id'])){
            foreach($data as $row){
                $rules = array(
                    'id' => 'required|max:255'
                );
                $validator = Validator::make($row,$rules);
                if($validator->fails()){
                    return Response::json(['success'=>false,'messages'=>$validator->messages()],200);
                    break;
                }
            }
        }else{
            return Response::json(['success'=>false,'messages'=>'Element require id or cid.'],200);
        }


        if(isset($data[0]['id'])){
            $codes = array_map(function($item){ return $item['id']; },$data);
            DB::table('grupa_pr')->whereIn('id_is',$codes)->delete();
        }

        return Response::json(['success'=>true],200);
    }

}
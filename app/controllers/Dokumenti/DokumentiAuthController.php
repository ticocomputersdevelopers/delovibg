<?php

class DokumentiAuthController extends Controller {

    function login(){
        if(DokumentiOptions::user()){
            return Redirect::to(DokumentiOptions::base_url().'dokumenti');
        }

        $data=array(
            'strana'=>'login',
            "title"=>'Prijava',
            "description"=>'Prijava',
            "keywords"=>'prijava',

        );
        return View::make('dokumenti.pages.login',$data);
    }

    function loginStore(){
        $input = Input::all();

        $validator = Validator::make($input,array(
                'username' => 'required|between:3,50',
                'password' => 'required|between:3,50|exists:partner,password,login,'.$input['username'].''
            ),
            array(
                'required' => 'Niste popunili polje!',
                'regex' => 'Uneli ste ne dozvoljene karaktere!',
                'between' => 'Vaš sadzaj polja je prekratak ili predugačak!',
                'exists' => 'Uneli ste pogrešno korisničko ime ili lozinku!',
            )
        );
        if ($validator->fails()) {
            return Redirect::to(DokumentiOptions::base_url().'dokumenti/login')->withInput()->withErrors($validator);

        }else{

            $partner = B2bPartner::join('partner_je','partner_je.partner_id','=','partner.partner_id')->whereIn('partner_vrsta_id',array(122))->where(array('login'=>$input['username'],'password'=>$input['password']))->first();
            if(!is_null($partner)){
                Session::put('dokumenti_user_'.Options::server(),$partner->partner_id);
                Session::put('dokumenti_user_kind_'.Options::server(),'saradnik');
                Session::put('b2b_user_'.B2bOptions::server(), $partner->partner_id);
            }

            return Redirect::to(DokumentiOptions::base_url().'dokumenti');
        }
    }

    public function logout(){
        Session::forget('dokumenti_user_'.Options::server());
        Session::forget('dokumenti_user_kind_'.Options::server());
        Session::forget('dokumenti_ponuda_id'.Options::server());
        return Redirect::to(DokumentiOptions::base_url().'dokumenti/login');
    }

}
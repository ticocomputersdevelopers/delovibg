<?php

class PredracunController extends Controller {

    public function predracuni(){
        $limit = 30;
        $page = Input::get('page') ? Input::get('page') : 1;
        $sortColumn = Input::get('sort_column') ? Input::get('sort_column') : 'broj_dokumenta';
        $sortDirection = Input::get('sort_direction') ? Input::get('sort_direction') : 'desc';
        $search = Input::get('search');
        $vrstaDokumenta = Input::get('vrsta_dokumenta') ? Input::get('vrsta_dokumenta') : 'predracun';
        $partner_id = Input::get('partner_id') ? Input::get('partner_id') : null;
        $dokumenti_status_id = !is_null(Input::get('dokumenti_status_id')) ? Input::get('dokumenti_status_id') : 'all';


        $selectBrojDokumenta = "p.broj_dokumenta";
        $selectNazivPartnera = "(select naziv from partner where partner_id = p.partner_id limit 1)";
        $selectDatumPredracuni = "p.datum_predracuna";
        $selectRokPlacanja = "p.vazi_do";
        $selectIznos = "p.iznos";

        $predracuniQuery = "select predracun_id, ".$selectBrojDokumenta.", ".$selectNazivPartnera." as naziv_partnera, ".$selectDatumPredracuni.", ".$selectRokPlacanja.", ".$selectIznos." from predracun p";

        $whereArr = array();
        if(!is_null($partner=DokumentiOptions::user('saradnik'))){
            $whereArr[] = "(select parent_id from partner where partner_id = p.partner_id limit 1) = ".$partner->partner_id."";
        }else{
            if(is_null($partner_id)){
                $whereArr[] = "p.partner_id not in (select partner_id from partner where parent_id is not null)";
            }
        }       
        if(!is_null($search) && $search != ''){
            $whereArr[] = "(".$selectBrojDokumenta." ilike '%".$search."%' or ".$selectNazivPartnera." ilike '%".$search."%')";
        }
        if(!is_null($partner_id) && intval($partner_id) > 0){
            $whereArr[] = "(partner_id = ".$partner_id." or p.partner_id in (select partner_id from partner where parent_id = ".$partner_id."))";
        }
        if(!is_null($dokumenti_status_id) && $dokumenti_status_id != 'all' && intval($dokumenti_status_id) >= 0){
            $whereArr[] = "dokumenti_status_id ".($dokumenti_status_id==0 ? "IS NULL" : "= ".$dokumenti_status_id);
        }
        if(count($whereArr) > 0){
            $predracuniQuery .= " where ".implode(" and ",$whereArr);
        }

        $column = $selectNazivPartnera;
        if($sortColumn == 'naziv'){
            $column = $selectNazivPartnera;
        }else if($sortColumn == 'broj_dokumenta'){
            $column = $selectBrojDokumenta;
        }else if($sortColumn == 'datum_predracuna'){
            $column = $selectDatumPredracuni;
        }else if($sortColumn == 'vazi_do'){
            $column = $selectRokPlacanja;
        }else if($sortColumn == 'iznos'){
            $column = $selectIznos;
        }
        $predracuniQuery .= " order by ".$column." ".$sortDirection;


        $predracuniCount = count(DB::select($predracuniQuery));
        $predracuniQuery .= " limit ".strval($limit)." offset ".strval(($page-1)*$limit);
        $predracuni = DB::select($predracuniQuery);
        $data=array(
            'strana' => 'predracuni',
            'title' => 'predracuni',
            'search' => !is_null($search) ? $search : '',
            'predracuni' => $predracuni,
            'count' => $predracuniCount,
            'limit' => $limit,
            'sort_column' => $sortColumn,
            'sort_direction' => $sortDirection,
            'vrsta_dokumenta' => $vrstaDokumenta,
            'partner_id' => $partner_id,
            'dokumenti_status_id' => $dokumenti_status_id
            );
      
        return View::make('dokumenti/pages/predracuni', $data); 
    }

    public function partner_pretraga(){
        $search = trim(Input::get('partner'));
        $reci = explode(" ",$search);

        $nazivFormatiran = array();
        foreach($reci as $rec){
            $nazivFormatiran[] = "naziv ILIKE '%" . $rec . "%'";
        }

        $partnerQuery = "SELECT * FROM partner ".(count($nazivFormatiran)>0 ? "WHERE (".implode(' OR ',$nazivFormatiran).")" : "");
        if(!is_null($partner=DokumentiOptions::user('saradnik'))){
            $partnerQuery .= (count($nazivFormatiran)>0 ? " AND" : "WHERE")." parent_id = ".$partner->partner_id;
        }
        $partnerQuery .= " ORDER BY naziv ASC";

        header('Content-type: text/plain; charset=utf-8');          
        $list = "<ul class='JSPartnerSearchList'>";
        foreach (DB::select($partnerQuery) as $partner) {
            $list .= "
                <li class='JSPartnerSearchItem' data-partner_id='".$partner->partner_id."'>
                    <div class='JSPartnerSearchLink'>"
                        ."<span class='JSPartnerSearchNaziv'>" . $partner->naziv . "</span>"
                        ."<span class='JSPartnerSearchPib'>" . $partner->pib . "</span>"
                        ."<span class='JSPartnerSearchAdresa'>" . $partner->adresa . "</span>
                    </div>
                </li>";
        }
        $list .= "</ul>";
        echo $list;      
    }

    public function partner_podaci(){
        $partner_id = Input::get('partner_id');
        $partner = DB::table('partner')->where('partner_id',$partner_id)->first();

        $partnerData = array(
            'pib' => $partner->pib,
            'adresa' => $partner->adresa,
            'mesto' => $partner->mesto,
            'kontakt_osoba' => $partner->kontakt_osoba,
            'mail' => $partner->mail,
            'telefon' => $partner->telefon
        );
        return json_encode($partnerData);
    }

    public function predracun($predracun_id){
        if($predracun_id > 0 && !is_null($partner=DokumentiOptions::user('saradnik'))){
            if(count(DB::select("select * from predracun p where (select parent_id from partner where partner_id = p.partner_id limit 1) = ".$partner->partner_id." and predracun_id=".$predracun_id)) == 0){
                return Response::make('Forbidden', 403);
            }
        }

        $novaStavka = Input::get('nova_stavka') ? Input::get('nova_stavka') : 0;
        $podesavanja = Dokumenti::podesavanja();
        $predracun = $predracun_id > 0 ? DB::table('predracun')->where('predracun_id',$predracun_id)->first() : (object) array('predracun_id'=>0,'ponuda_id'=>0,'partner_id'=>null,'broj_dokumenta'=>'Novi predracun','datum_predracuna'=>date('Y-m-d'),'vazi_do'=>date('Y-m-d'),'dokumenti_status_id'=>null,'rok_isporuke'=>date('Y-m-d'),'nacin_placanja'=>null,'tekst'=>$podesavanja->sablon,'tekst_footer'=>$podesavanja->footer);

        $partner = $predracun_id > 0 ? DB::table('partner')->where('partner_id',$predracun->partner_id)->first() : (object) array('partner_id'=>0);

        $stavke = $predracun_id > 0 ? DB::table('predracun_stavka')->where('predracun_id',$predracun->predracun_id)->orderBy('broj_stavke','asc')->get() : array();
        $stavkeRoba = $predracun_id > 0 ? DB::table('predracun_stavka')->select('roba_id')->whereNotNull('roba_id')->where('predracun_id',$predracun->predracun_id)->orderBy('broj_stavke','asc')->get() : array();
        $data=array(
            'strana' => 'predracun',
            'title' => 'Predracun '.$predracun->broj_dokumenta,
            'partner' => $partner,
            'predracun' => $predracun,
            'stavke' => $stavke,
            'stavkeUkupanIznos' => DB::select("select sum(pcena*kolicina) as iznos from predracun_stavka where predracun_id=".$predracun_id)[0]->iznos,
            'roba_ids' => array_map('current',$stavkeRoba),
            'ponuda' => DB::table('ponuda')->where('ponuda_id',$predracun->ponuda_id)->first(),
            'racun' => DB::table('racun')->where('predracun_id',$predracun->predracun_id)->first(),
            'checkRacuni' => DB::table('racun')->where('predracun_id',$predracun->predracun_id)->count() > 0,
            'novaStavka' => ($novaStavka == 1)
            );

        return View::make('dokumenti/pages/predracun', $data); 
    }
    public function predracun_post(){
        $predracun_id = Input::get('predracun_id');
        $ponuda_id = Input::get('ponuda_id');
        $partner_id = Input::get('partner_id');
        $naziv = Input::get('naziv');
        $adresa = Input::get('adresa');
        $mesto = Input::get('mesto');
        $pib = Input::get('pib');
        $kontakt_osoba = Input::get('kontakt_osoba');
        $mail = Input::get('mail');
        $telefon = Input::get('telefon');
        $naziv_stavka = Input::get('naziv_stavka');
        $datum_predracuna = Input::get('datum_predracuna');
        $vazi_do = Input::get('vazi_do');
        $rok_isporuke = Input::get('rok_isporuke');
        $nacin_placanja = Input::get('nacin_placanja');
        $dokumenti_status_id = Input::get('dokumenti_status_id');
        $tekst = Input::get('tekst');
        $tekst_footer = Input::get('tekst_footer');
        $racun = Input::get('racun') ? 1 : 0;

        $oldPredracunSablon = $predracun_id > 0 ? DB::table('predracun')->select('tekst','tekst_footer')->where('predracun_id',$predracun_id)->first() : (object) array('tekst' => $tekst,'tekst_footer' => $tekst_footer);

        $data = array(
            'partner_id' => $partner_id,
            'naziv' => $naziv,
            'adresa' => $adresa,
            'mesto' => $mesto,
            'pib' => $pib,
            'kontakt_osoba' => $kontakt_osoba,
            'mail' => $mail,
            'telefon' => $telefon,
            'naziv_stavka' => $naziv_stavka,
            'datum_predracuna' => Dokumenti::dateInversion($datum_predracuna),
            'vazi_do' => Dokumenti::dateInversion($vazi_do),
            'rok_isporuke' => Dokumenti::dateInversion($rok_isporuke),
            'nacin_placanja' => $nacin_placanja,
            'dokumenti_status_id' => $dokumenti_status_id,
            'racun' => $racun
        );
        $validator_arr = array(
            'partner_id' => 'required|integer',
            'naziv' => 'required|regex:'.Support::regex().'|between:2,50',
            'adresa' => 'required|regex:'.Support::regex().'|between:3,100',
            'mesto' => 'required|regex:'.Support::regex().'|between:2,100', 
            'pib' => 'numeric|digits_between:9,9',
            'kontakt_osoba' => 'regex:'.Support::regex().'|max:50',
            'mail' => 'regex:'.Support::regex().'|email|max:50',
            'telefon' => 'regex:'.Support::regex().'|max:50',
            'datum_predracuna' => 'required|date',
            'vazi_do' => 'date',
            'rok_isporuke' => 'date',
            'nacin_placanja' => 'regex:'.Support::regex().'|max:200',
            'dokumenti_status_id' => 'integer',
            'naziv_stavka' => 'regex:'.Support::regex().'|max:255',
            'racun' => 'required|integer|in:0,1'
        );
        $messages = array(
            'required' => 'Niste popunili polje!',
            'regex' => 'Unesite odgovarajuči karakter!',
            'not_in' => 'Niste popunili polje!',
            'max' => 'Dužina sadržaja nije dozvoljena!',
            'integer' => 'Vrednost sadržaja nije dozvoljena!',
            'digits_between' => 'Dužina sadržaja nije dozvoljena!',
            'between' => 'Dužina sadržaja nije dozvoljena!',
            'date' => 'Neodgovarajući format!'
        );

        $validator = Validator::make($data, $validator_arr, $messages);


        if($validator->fails()){
            return Redirect::to(DokumentiOptions::base_url().'dokumenti/predracun/'.$predracun_id)->withInput()->withErrors($validator);
        }

        $partnerdata = array(
            'partner_id' => $data['partner_id'],
            'naziv' => $data['naziv'],
            'adresa' => $data['adresa'],
            'mesto' => $data['mesto'],
            'pib' => $data['pib'],
            'kontakt_osoba' => $data['kontakt_osoba'],
            'mail' => $data['mail'],
            'telefon' => $data['telefon']     
        );
        if($partnerdata['partner_id'] == 0){
            $data['partner_id'] = DB::select("SELECT nextval('partner_partner_id_seq')")[0]->nextval;
            $partnerdata['partner_id'] = $data['partner_id'];
            $partnerdata['drzava_id'] = 0;
            $partnerdata['rabat'] = 0.00;
            $partnerdata['limit_p'] = 0.00;
            $partnerdata['parent_id'] = !is_null($partnerSaradnik=DokumentiOptions::user('saradnik')) ? $partnerSaradnik->partner_id : null;
            DB::table('partner')->insert($partnerdata);            
        }else{
            unset($partnerdata['partner_id']);
            DB::table('partner')->where('partner_id', $data['partner_id'])->update($partnerdata);
        }

        $podesavanja = Dokumenti::podesavanja();
        $dbdata = array(
            'partner_id' => $data['partner_id'],
            'datum_predracuna' => $data['datum_predracuna'],
            'vazi_do' => $data['vazi_do'],
            'rok_isporuke' => $data['rok_isporuke'],
            'nacin_placanja' => $data['nacin_placanja'],
            'dokumenti_status_id' => $data['dokumenti_status_id'] == 0 ? null : $data['dokumenti_status_id'],
            'tekst' => !empty(strip_tags($tekst)) ? $tekst : $podesavanja->sablon,
            'tekst_footer' => !empty(strip_tags($tekst_footer)) ? $tekst_footer : $podesavanja->footer
        );
        if($predracun_id==0){
            $predracun_id = DB::select("SELECT nextval('predracun_predracun_id_seq') as predracun_id")[0]->predracun_id;
            $dbdata['predracun_id'] = $predracun_id;
            $dbdata['broj_dokumenta'] = "PRE".str_pad($predracun_id,5,"0", STR_PAD_LEFT);
            $dbdata['vrsta_dokumenta_id'] = 50;
            DB::table('predracun')->insert($dbdata);
        }else{
            DB::table('predracun')->where('predracun_id',$predracun_id)->update($dbdata);
        }

        $racun_id = null;
        if($racun == 1){
            $dbdata['datum_racuna'] = $dbdata['datum_predracuna'];
            $dbdata['rok_placanja'] = $dbdata['vazi_do'];;
            unset($dbdata['datum_predracuna']);
            unset($dbdata['vazi_do']);
            unset($dbdata['racun']);
            unset($dbdata['tekst']);
            unset($dbdata['tekst_footer']);
            $dbdata['predracun_id'] = $predracun_id;
            $racun_id = DB::select("SELECT nextval('racun_racun_id_seq1') as racun_id")[0]->racun_id;
            $dbdata['racun_id'] = $racun_id;
            $dbdata['broj_dokumenta'] = "RAC".str_pad($racun_id,5,"0", STR_PAD_LEFT);
            $dbdata['vrsta_dokumenta_id'] = 100;

            DB::table('racun')->insert($dbdata);

            DB::statement("insert into racun_stavka (racun_id,broj_stavke,naziv_stavke,roba_id,nab_cena,pdv,pcena,kolicina) select ".$racun_id.",broj_stavke,naziv_stavke,roba_id,nab_cena,pdv,pcena,kolicina from predracun_stavka where predracun_id=".$predracun_id);

            $iznos = DB::select("select sum(pcena*kolicina) as iznos from racun_stavka where racun_id=".$racun_id);
            DB::table('racun')->where('racun_id',$racun_id)->update(array('iznos'=>$iznos[0]->iznos));
        }

        $predracunSablon = DB::table('predracun')->select('tekst','tekst_footer')->where('predracun_id',$predracun_id)->first();
        $sablon_change = 0;
        if(
            ((!empty(strip_tags($tekst)) && $oldPredracunSablon->tekst != $predracunSablon->tekst && $tekst != $podesavanja->sablon) || 
            (!empty(strip_tags($tekst_footer)) && $oldPredracunSablon->tekst_footer != $predracunSablon->tekst_footer && $tekst_footer != $podesavanja->footer))
        ){
            $sablon_change = 1;
        }

        return Redirect::to(DokumentiOptions::base_url().'dokumenti/predracun/'.$predracun_id)->with('message','Uspešno ste sačuvali predračun!')->with('sablon_change',$sablon_change);
    }

    public function predracun_stavka_save(){
        $data = Input::get();
        $predracun_stavka = null;

        if($data['predracun_stavka_id'] == 0){
            if($data['change'] == 'roba'){
                $roba = DB::table('roba')->where('roba_id',$data['roba_id'])->first();
                $pdv = DB::table('tarifna_grupa')->where('tarifna_grupa_id', $roba->tarifna_grupa_id)->pluck('porez');
                $data['predracun_stavka_id'] = DB::select("SELECT nextval('predracun_stavka_predracun_stavka_id_seq')")[0]->nextval;

                $insertData = array(
                    'predracun_stavka_id' => $data['predracun_stavka_id'],
                    'predracun_id' => $data['predracun_id'],
                    'broj_stavke' => DB::table('predracun_stavka')->where('predracun_id',$data['predracun_id'])->max('broj_stavke')+1,
                    'roba_id' => $roba->roba_id,
                    'naziv_stavke' => $roba->naziv,
                    'kolicina' => 1,
                    'pdv' => $pdv,
                    'nab_cena' => $roba->racunska_cena_end,
                    'pcena' => $roba->racunska_cena_end * (1+$pdv/100)
                );
            }else if($data['change'] == 'naziv_stavke'){
                $data['predracun_stavka_id'] = DB::select("SELECT nextval('predracun_stavka_predracun_stavka_id_seq')")[0]->nextval;
                $insertData = array(
                    'predracun_stavka_id' => $data['predracun_stavka_id'],
                    'predracun_id' => $data['predracun_id'],
                    'broj_stavke' => DB::table('predracun_stavka')->where('predracun_id',$data['predracun_id'])->max('broj_stavke')+1,
                    'naziv_stavke' => $data['naziv_stavke'],
                    'kolicina' => 1,
                    'pdv' => 20,
                    'nab_cena' => 0,
                    'pcena' => 0
                );                
            }
            DB::table('predracun_stavka')->insert($insertData);
        }else{

            $predracun_stavka = DB::table('predracun_stavka')->where('predracun_stavka_id',$data['predracun_stavka_id'])->first();

            if($data['change'] == 'roba'){
                $roba = DB::table('roba')->where('roba_id',$data['roba_id'])->first();
                $updateData = array(
                    'roba_id' => $roba->roba_id,
                    'naziv_stavke' => $roba->naziv,
                    'nab_cena' => $roba->racunska_cena_end,
                    'pcena' => $roba->racunska_cena_end * (1+$predracun_stavka->pdv/100)
                );
            }else if($data['change'] == 'naziv_stavke'){
                $updateData = array(
                    'roba_id' => null,
                    'naziv_stavke' => $data['naziv_stavke']
                );
            }else if($data['change'] == 'kolicina'){
                $updateData = array(
                    'kolicina' => $data['kolicina']
                );
            }else if($data['change'] == 'pdv'){
                $updateData = array(
                    'pdv' => $data['pdv'],
                    'pcena' => $predracun_stavka->nab_cena * (1+$data['pdv']/100)
                );
            }else if($data['change'] == 'nab_cena'){
                $updateData = array(
                    'nab_cena' => $data['nab_cena'],
                    'pcena' => $data['nab_cena'] * (1+$predracun_stavka->pdv/100)
                );
            }else if($data['change'] == 'naziv_stavke'){
                $updateData = array(
                    'naziv_stavke' => $data['naziv_stavke'],
                    'roba_id' => null
                );
            }

            DB::table('predracun_stavka')->where('predracun_stavka_id',$data['predracun_stavka_id'])->update($updateData);
        }

        $predracun_stavka = DB::table('predracun_stavka')->where('predracun_stavka_id',$data['predracun_stavka_id'])->first();

        $iznos = DB::select("select sum(pcena*kolicina) as iznos from predracun_stavka where predracun_id=".$predracun_stavka->predracun_id);
        DB::table('predracun')->where('predracun_id',$predracun_stavka->predracun_id)->update(array('iznos'=>$iznos[0]->iznos));

        $responseData = array(
            'roba_id' => $predracun_stavka->roba_id,
            'nab_cena' => $predracun_stavka->nab_cena,
            'pdv' => $predracun_stavka->pdv,
            'pcena' => $predracun_stavka->pcena,
            'kolicina' => $predracun_stavka->kolicina,
            'opis_robe' => $predracun_stavka->opis_robe,
            'naziv_stavke' => $predracun_stavka->naziv_stavke,
            'iznos' => $iznos[0]->iznos
        );
        return json_encode($responseData);
    }

    public function predracun_stavka_delete($predracun_stavka_id){
        $predracun_id = DB::table('predracun_stavka')->where(array('predracun_stavka_id'=>$predracun_stavka_id))->pluck('predracun_id');

        DB::table('predracun_stavka')->where(array('predracun_stavka_id'=>$predracun_stavka_id))->delete();

        $iznos = DB::select("select sum(pcena*kolicina) as iznos from predracun_stavka where predracun_id=".$predracun_id);
        DB::table('predracun')->where('predracun_id',$predracun_id)->update(array('iznos'=>$iznos[0]->iznos));

        return Redirect::to(DokumentiOptions::base_url().'dokumenti/predracun/'.$predracun_id)->with('message','Uspešno ste uklonili stavku!');
    }
    public function predracun_delete($predracun_id){
        DB::table('predracun_stavka')->where(array('predracun_id'=>$predracun_id))->delete();
        DB::table('predracun')->where(array('predracun_id'=>$predracun_id))->delete();
        return Redirect::to(DokumentiOptions::base_url().'dokumenti/predracuni');
    }

    public function predracun_pdf($predracun_id){
        $saradnik = DokumentiOptions::user('saradnik');
        $predracun = DB::table('predracun')->where('predracun_id',$predracun_id)->first();

        $ukupno = 0;
        $pdv = 0;
        $ukupno_pdv = 0;
        foreach(DB::table('predracun_stavka')->where('predracun_id',$predracun_id)->get() as $stavka){
            if(is_null($saradnik) || is_null($stavka->roba_id)){
                $ukupno += $stavka->nab_cena*$stavka->kolicina;
                $pdv += $stavka->nab_cena*$stavka->kolicina*($stavka->pdv/100);
                $ukupno_pdv += $stavka->nab_cena*$stavka->kolicina*(1+$stavka->pdv/100);
            }else{
                $cene = B2bArticle::b2bRabatCene($stavka->roba_id);
                $ukupno += $stavka->nab_cena*(1-$cene->ukupan_rabat/100)*$stavka->kolicina;
                $pdv += $stavka->nab_cena*(1-$cene->ukupan_rabat/100)*$stavka->kolicina*($stavka->pdv/100);
                $ukupno_pdv += $stavka->nab_cena*(1-$cene->ukupan_rabat/100)*$stavka->kolicina*(1+$stavka->pdv/100);
            }
        }
        $predracunCene = (object) array(
            'ukupno' => $ukupno,
            'pdv' => $pdv,
            'ukupno_pdv' => $ukupno_pdv,
        );

        $show_header = true;
        $limit = 10;
        $stavke_count = DB::table('predracun_stavka')->where('predracun_id',$predracun->predracun_id)->count();
        $count_pages = floor($stavke_count,$limit);
        $mod = fmod($stavke_count,$limit);
        if($mod > 0){ $count_pages++; }
        if($mod > 5){
            $count_pages++;
            $show_header = false;
        }else if($mod == 0){
            $count_pages++;
            $show_header = false;
        }
        $rbr = 1;

        $data = array(
            'predracun' => $predracun,
            'saradnik' => $saradnik,
            'partner' => DB::table('partner')->where('partner_id',$predracun->partner_id)->first(),
            'podesavanja' => Dokumenti::podesavanja(),
            'predracunCene' => $predracunCene,
            'show_header' => $show_header,
            'limit' => $limit,
            'count_pages' => $count_pages,
            'rbr' => $rbr
        );

        $pdf = App::make('dompdf');
        $pdf->loadView('dokumenti.pdf.predracun', $data);

        return $pdf->stream();
    }


}
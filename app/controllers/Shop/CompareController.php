<?php 

class CompareController extends Controller {

	public function compareajax(){
		$lang = Language::multi() ? Request::segment(1) : null;

		$main_id = Input::get('main_id');
		if(!is_null($main_id)) {
			Session::push('compare_ids', $main_id);
		}

	  	if(Session::has('compare_ids')){
		  	if(count(Session::get('compare_ids')) <= 4 && !in_array(Input::get('id'), Session::get('compare_ids'))){
		  		Session::push('compare_ids', Input::get('id'));
		    }
		}else{
			Session::push('compare_ids', Input::get('id'));
		}
		return Response::json(array('count_compare'=>count(Session::get('compare_ids'))));
	}
	public function clear_compare(){
		$lang = Language::multi() ? Request::segment(1) : null;

		if(Session::has('compare_ids')){
			$ids = Session::get('compare_ids');
	        foreach($ids as $key => $id){
	            if(Input::get('clear_id') == $id){
	                unset($ids[$key]);
	            }
	        }
	     	Session::forget('compare_ids');
	     	foreach($ids as $id){
	     		Session::push('compare_ids', $id);
	     	}	        
		 }
		 return Response::json(array('count_compare'=>count(Session::get('compare_ids'))));
	} 

    public function compare(){
      	$lang = Language::multi() ? Request::segment(1) : null;

      	if(Session::has('compare_ids')){
	      	$ids = Session::get('compare_ids');
	     	if(Input::get('clear_id')!=null){
		        foreach($ids as $key => $id){
		            if(Input::get('clear_id') == $id){
		                unset($ids[$key]);
		            }
		        }
	     	}
	     	Session::forget('compare_ids');
	     	foreach($ids as $id){
	     		Session::push('compare_ids', $id);
	     	}
	     	$ids = Session::get('compare_ids');

	     	if(is_array($ids) && count($ids)>0){

		        $table = '<table class="table table-bordered">';
	        	$table .= '<tr><td></td>';

		        foreach($ids as $id) {
		            $table .= 	
		            	'<td class="relative">
			            		<div> '.Product::getRating($id).'</div>

			            		<div class="compare-image-wrapper flex"> 
			            			<span>
			            				<img src="'.Options::base_url().Product::web_slika($id).'">
			            			</span>
			            		</div>
			            		<div class="compare-product-name"> '.Language::trans(All::getCompare($id)[0]->naziv_web).'</div>
		            			<span class="JSclearCompare fa fa-times" data-id="'.$id.'" title="'.Language::trans('Obriši').'"></span>
		            		</td>';
		        }

		        $table .= '</tr><tr class="compare-manufac-price">';

		        	$table .= '<td><div>'.Language::trans('Proizvođač').'</div></td>';
			        foreach($ids as $id){
			        	$table .= '<td><div>'.All::getCompare($id)[0]->proizvodjac.'</div></td>';
			        }

		        $table .= '</tr>';

			    foreach(All::getCompare($ids) as $karak){
			        $table .= '<tr class="compare-manufac-price">';
		        		$table .= '<td> <div>'.Language::trans($karak->naziv).'</div></td>';

		        		foreach($ids as $id){
		                    if(All::getVrednost($id,$karak->naziv)){
		                        $table .= '<td><div class="JSCompareColorOrb" style="background: '.All::getVrednost($id,$karak->naziv).'"></div><div>'.All::getVrednost($id,$karak->naziv).'</div></td>';
		                    } else {
		                        $table .= '<td><div> &nbsp; </div></td>';
		                    }
		                } 

			        $table .= '</tr>'; 
			    } 
 
			    $table .= '<tr class="compare-manufac-price compare-price">';
		        $table .= '<td><div>'.Language::trans('Cena').'</div></td>'; 
			    foreach($ids as $id){
			        $table .= '<td>'; 
		        	$akcijska_cena = Cart::cena(All::getCompare($id)[0]->akcijska_cena);
		        	$web_cena = Cart::cena(Options::checkCena()=='web_cena'?All::getCompare($id)[0]->web_cena:All::getCompare($id)[0]->mpcena);
		        	if(All::getCompare($id)[0]->akcija_flag_primeni==1 && $akcijska_cena<$web_cena && $akcijska_cena!=0){
		        		$table .= '<div>'.$akcijska_cena.'</div>';
		        	}else{
		            	$table .= '<div>'.$web_cena.'</div>';
		        	}
		        	$table .= '</td>';
		        }      

		        $table .= '</tr></table>';

		        $content = $table;
	     	} else {
	     		$content = Language::trans('Najmanje dva artikla mogu biti upoređena').'!';
	     	}
     	} else {
     		$content = Language::trans('Najmanje dva artikla mogu biti upoređena').'!';
     	}
     	return Response::json(array('content'=>$content, 'count_compare'=> Session::has('compare_ids') ? count(Session::get('compare_ids')) : 0));
    } 


} 
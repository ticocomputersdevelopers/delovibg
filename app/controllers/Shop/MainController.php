<?php 
use Service\Mailer;

class MainController extends Controller {
    public function index(){
        if(Options::gnrl_options(3061) == 1 && !Session::has('b2c_admin'.Options::server())){
            return View::make('shop/site-unactive');
        }

        // return App::make('B2bController')->login();

        $strana=All::get_page_start();
        $seo = Seo::page($strana);
        $data=array(
            "strana"=>$strana,
            "org_strana"=>$strana,
            "title"=>$seo->title,
            "description"=>$seo->description,
            "keywords"=>$seo->keywords
        );
        
        $web_b2c_seo_id = Seo::get_page_id($strana);
        $stranica=DB::table('web_b2c_seo')->where(array('web_b2c_seo_id'=>$web_b2c_seo_id))->first();
        if($stranica->grupa_pr_id > 0){
            $grupaSlug = Url_mod::slug_trans(DB::table('grupa_pr')->where('grupa_pr_id',$stranica->grupa_pr_id)->pluck('grupa'));
            return Redirect::to(Options::base_url().$grupaSlug);
        }elseif($stranica->tip_artikla_id != -1){
            $redirect = Options::base_url();
            if($stranica->tip_artikla_id == 0){
                $redirect .= Url_mod::slug_trans('akcija');
            }else{
                $redirect .= Url_mod::slug_trans('tip').'/'.Url_mod::slug_trans(Support::tip_naziv($stranica->tip_artikla_id));
            }
            return Redirect::to($redirect);
        }elseif(!is_null($stranica->redirect_web_b2c_seo_id)){
            $stranica=DB::table('web_b2c_seo')->where(array('web_b2c_seo_id'=>$stranica->redirect_web_b2c_seo_id))->first();
            return Redirect::to(Options::base_url().Url_mod::page_slug($stranica->naziv_stranice)->slug);
        }

        $data['sekcije'] = DB::table('stranica_sekcija')->select('sekcija_stranice.*','sekcija_stranice_tip.naziv as tip_naziv')->where('web_b2c_seo_id',$web_b2c_seo_id)->join('sekcija_stranice','sekcija_stranice.sekcija_stranice_id','=','stranica_sekcija.sekcija_stranice_id')->join('sekcija_stranice_tip','sekcija_stranice_tip.sekcija_stranice_tip_id','=','sekcija_stranice.sekcija_stranice_tip_id')->orderBy('stranica_sekcija.rbr','asc')->get();

        $data['anketa_id'] = !is_null($stranica->anketa_id) ? $stranica->anketa_id : 0;
        $data['web_b2c_narudzbina_id'] = 0;
        $data['custom'] = 1;
        
        return View::make('shop/themes/'.Support::theme_path().'pages/content',$data);  
    }
    
    public function lang(){
        $lang = Language::multi() ? Request::segment(1) : null;
        return Redirect::to('/');
            
    }
    public function page(){
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
        $orgStrana = Request::segment(1+$offset);

        $strana = Url_mod::slug_convert_page($orgStrana);

        if($strana==""){
            $strana=All::get_page_start();
        }
        
        if(Seo::get_page_id($strana) != 0){
            $seo = Seo::page($strana);
            $data=array(
                "strana"=>$strana,
                "org_strana" => $orgStrana,
                "naziv"=>str_replace(' - '.Options::company_name(),'',$seo->title),
                "title"=>$seo->title,
                "description"=>$seo->description,
                "keywords"=>$seo->keywords
            );
        }else{

            if(($new_link = Url_mod::checkOldLink(str_replace(Options::base_url(),'',Request::url()))) != null){
                return Redirect::to($new_link);
            }

            return App::make('ArticlesController')->artikli_first(array('grupa1' => Request::segment(1+$offset)));
            // $data=array(
            //     "strana"=>'Not found',
            //     "title"=>'Not found',
            //     "description"=>'Not found',
            //     "keywords"=>'Not found'
            // );
            // $content = View::make('shop/themes/'.Support::theme_path().'pages/not_found', $data)->render();
            // return Response::make($content, 404);
        }
        $web_b2c_seo_id = Seo::get_page_id($strana);
        $stranica=DB::table('web_b2c_seo')->where(array('web_b2c_seo_id'=>$web_b2c_seo_id))->first();

        if($strana == 'svi-proizvodi'){
            
            $offset = Language::segment_offset();
            $lang = Language::multi() ? Request::segment(1) : null;

            $grupa_pr_id=0;
            All::check_category($grupa_pr_id);
              
            $level2=array();
            Groups::allGroups($level2,$grupa_pr_id);

            $select="SELECT DISTINCT r.roba_id";
            $roba=" FROM roba r LEFT JOIN roba_grupe rg ON rg.roba_id = r.roba_id".Product::checkLager('join').Product::checkActiveGroup('join').Product::checkImage('join').Product::checkCharacteristics('join')."";
            $where=" WHERE r.flag_aktivan = 1 AND r.flag_prikazi_u_cenovniku = 1 ".Product::checkLager().Product::checkActiveGroup().Product::checkImage().Product::checkPrice().Product::checkDescription().Product::checkCharacteristics()."AND ( r.grupa_pr_id IN(".implode(",",$level2).") OR rg.grupa_pr_id IN(".implode(",",$level2)."))";
            $where_init=$where;;


            $count=count(DB::select($select.$roba.$where));

                if(Session::has('limit')){
                    $limit=Session::get('limit');
                }
                else {
                    $limit=20;
                }

                if(Input::get('page')){
                    $pageNo = Input::get('page');
                }else{
                    $pageNo = 1;
                }

                $offset = ($pageNo-1)*$limit;
                
                if(Session::has('order')){
                    if(Session::get('order')=='price_asc')
                    {
                    $artikli=DB::select($select.", r.".Options::checkCena()."".$roba.$where." ORDER BY r.".Options::checkCena()." ASC LIMIT ".$limit." OFFSET ".$offset."");
                    }
                    else if(Session::get('order')=='price_desc'){
                    $artikli=DB::select($select.", r.".Options::checkCena()."".$roba.$where." ORDER BY r.".Options::checkCena()." DESC LIMIT ".$limit." OFFSET ".$offset."");
                    }
                    else if(Session::get('order')=='news'){
                    $artikli=DB::select($select.", r.roba_id".$roba.$where." ORDER BY r.roba_id DESC LIMIT ".$limit." OFFSET ".$offset."");
                    }
                    else if(Session::get('order')=='name'){
                    $artikli=DB::select($select.", r.naziv_web".$roba.$where." ORDER BY r.naziv_web ASC LIMIT ".$limit." OFFSET ".$offset."");
                    }
                    else if(Session::get('order')=='rbr'){
                    $artikli=DB::select($select.", r.rbr".$roba.$where." ORDER BY r.rbr ASC LIMIT ".$limit." OFFSET ".$offset."");
                    }
                }
                else {
                    $artikli=DB::select($select.", r.roba_id".$roba.$where." ORDER BY r.roba_id DESC LIMIT ".$limit." OFFSET ".$offset."");
                }

        $filter_prikazi = count(Groups::deca($grupa_pr_id));
        $seo_grupa = Seo::grupa($grupa_pr_id);
        $data=array(
        "strana"=>'artikli',
        "grupa_pr_id"=>0,
        "grupa_pr_ids"=>$level2,
        "title"=> "Svi artikli | ".Options::company_name(),
        "description"=>"Svi artikli",
        "keywords"=>"svi, artikli",
        "sub_cats" => Articles::subGroups($grupa_pr_id),
        "articles"=>$artikli,
        "limit"=>$limit,
        "count_products"=>$count,
        "url"=>"",
        "filter_prikazi" => 0
        );

        $path = 'shop/themes/'.Support::theme_path().'pages/products_list';
        return View::make($path,$data);
        }elseif($strana== 'kontakt'){
            $path = 'shop/themes/'.Support::theme_path().'pages/contact';
        }elseif($strana== 'infinite-articles'){
            $path = 'shop/themes/'.Support::theme_path().'pages/infinite_articles';
        }elseif($strana== 'korpa'){
            $path = 'shop/themes/'.Support::theme_path().'pages/cart_content';
        }elseif($strana=='konfigurator' AND Options::web_options(121)){
            $konfigurator = DB::table('konfigurator')->where(array('dozvoljen'=>1))->first();
            if(!is_null($konfigurator) && Options::web_options(121)==1){
                return Redirect::to(Options::base_url().Url_mod::slug_trans('konfigurator').'/'.$konfigurator->konfigurator_id);
            }else{
                return Redirect::to(Options::base_url());
            }
        }elseif($strana=='prijava'){
            if(Session::has('b2c_kupac') || !Options::user_registration()==1 || in_array(DB::table('prodavnica_stil')->where('izabrana',1)->first()->prodavnica_tema_id, array(2,5,9,10))){
                return Redirect::to(Options::base_url());
            }
            $path = 'shop/themes/'.Support::theme_path().'pages/login';
        }elseif($strana=='registracija'){
            if(!Options::user_registration()==1 || in_array(DB::table('prodavnica_stil')->where('izabrana',1)->first()->prodavnica_tema_id, array(9))){
                return Redirect::to(Options::base_url());
            }
            $data['web_kupac_id'] = Request::get('id');
            $path = 'shop/themes/'.Support::theme_path().'pages/registration';
        }elseif($strana=='pravno-lice'){
            if(!Options::user_registration()==1 || in_array(DB::table('prodavnica_stil')->where('izabrana',1)->first()->prodavnica_tema_id, array(6,8,9))){
                return Redirect::to(Options::base_url());
            }
            $path = 'shop/themes/'.Support::theme_path().'pages/company';
        }elseif($strana=='fizicko-lice'){
            if(!Options::user_registration()==1 || in_array(DB::table('prodavnica_stil')->where('izabrana',1)->first()->prodavnica_tema_id, array(6,8,9))){
                return Redirect::to(Options::base_url());
            }
            $path = 'shop/themes/'.Support::theme_path().'pages/private_user';
        }
        elseif($strana=='sve-kategorije'){
            $path = 'shop/themes/'.Support::theme_path().'pages/all_categories';
        }elseif($strana=='uporedi'){
            $data['compareArticles'] = [];
            if(Session::has('compare_ids') && count(Session::get('compare_ids')) > 0){
                $data['compareArticles'] = 
                DB::select("select r.roba_id, gp3.grupa, gp3.grupa_pr_id from roba r
                right join (select grupa_pr_id, grupa, parrent_grupa_pr_id, redni_broj from grupa_pr) gp3 on gp3.grupa_pr_id = r.grupa_pr_id 
                left join (select grupa_pr_id, grupa, parrent_grupa_pr_id, redni_broj from grupa_pr) gp2 on gp2.grupa_pr_id = gp3.parrent_grupa_pr_id 
                left join (select grupa_pr_id, grupa, parrent_grupa_pr_id, redni_broj from grupa_pr) gp1 on gp1.grupa_pr_id = gp2.parrent_grupa_pr_id 
                where r.grupa_pr_id > 0 and roba_id in (".implode(',',Session::get('compare_ids')).") order by gp1.redni_broj asc, gp2.redni_broj asc, gp3.redni_broj asc");
            }

            $path = 'shop/themes/'.Support::theme_path().'pages/compare';
        }
        elseif($strana=='katalozi'){
            $path = 'shop/themes/'.Support::theme_path().'pages/catalogs';
        }else{
            if($stranica->grupa_pr_id > 0){
                $grupaSlug = Url_mod::slug_trans(DB::table('grupa_pr')->where('grupa_pr_id',$stranica->grupa_pr_id)->pluck('grupa'));
                return Redirect::to(Options::base_url().$grupaSlug);
            }elseif($stranica->tip_artikla_id != -1){
                $redirect = Options::base_url();
                if($stranica->tip_artikla_id == 0){
                    $redirect .= Url_mod::slug_trans('akcija');
                }else{
                    $redirect .= Url_mod::slug_trans('tip').'/'.Url_mod::slug_trans(Support::tip_naziv($stranica->tip_artikla_id));
                }
                return Redirect::to($redirect);
            }elseif(!is_null($stranica->redirect_web_b2c_seo_id)){
                $stranica=DB::table('web_b2c_seo')->where(array('web_b2c_seo_id'=>$stranica->redirect_web_b2c_seo_id))->first();
                return Redirect::to(Options::base_url().Url_mod::page_slug($stranica->naziv_stranice)->slug);
            }

            // $data['content'] = '';
            // $stranica_jezik=DB::table('web_b2c_seo_jezik')->where(array('web_b2c_seo_id'=>Seo::get_page_id($strana), 'jezik_id'=>Language::lang_id()))->first();
            // if(!is_null($stranica_jezik)){
            //     $data['content'] = $stranica_jezik->sadrzaj;
            // }

            $data['sekcije'] = DB::table('stranica_sekcija')->select('sekcija_stranice.*','sekcija_stranice_tip.naziv as tip_naziv')->where('web_b2c_seo_id',$web_b2c_seo_id)->join('sekcija_stranice','sekcija_stranice.sekcija_stranice_id','=','stranica_sekcija.sekcija_stranice_id')->join('sekcija_stranice_tip','sekcija_stranice_tip.sekcija_stranice_tip_id','=','sekcija_stranice.sekcija_stranice_tip_id')->orderBy('stranica_sekcija.rbr','asc')->get();

            $filteri = DB::table('stranica_sekcija')->select('sekcija_stranice_id')->where('web_b2c_seo_id', $web_b2c_seo_id)->get();
            $data['filteri'] = $filteri;

            $data['anketa_id'] = !is_null($stranica->anketa_id) ? $stranica->anketa_id : 0;
            $data['web_b2c_narudzbina_id'] = 0;
            $data['custom'] = 1;
            $data['ch'] = Request::get('ch');

            $path = 'shop/themes/'.Support::theme_path().'pages/content';
        }
      
        return View::make($path,$data);
    } 

    function coment_add(){
        $lang = Language::multi() ? Request::segment(1) : null;

        $roba_id=Input::get('roba_id');
        $ime=Input::get('ime');
        $pitanje=Input::get('pitanje');
        $ocena=Input::get('ocena');
        
        $ip=All::ip_adress();
        $data=array(
        'roba_id'=>$roba_id,
        'ip_adresa'=>$ip,
        'ime_osobe'=>$ime,
        'pitanje'=>$pitanje,
        'komentar_odobren'=>0,
        'datum'=>date("Y-m-d"),
        'odgovoreno'=>0,
        'ocena'=>$ocena
        );
        DB::table('web_b2c_komentari')->insert($data);
        echo "Ok";
    }

    public function comment_add(){
        $lang = Language::multi() ? Request::segment(1) : null;

        $data=Input::get();

       $validator_arr = array(
            'comment-name' => 'required|regex:'.Support::regex().'|max:200',
            'comment-review' => 'numeric',
            'comment-message' => 'required|regex:'.Support::regex().'|max:1000'
            );
        $translate_mess = Language::validator_messages();

       $validator = Validator::make($data,$validator_arr,$translate_mess);
        if($validator->fails() || !Captcha::check()){
            if(!Captcha::check()){
                $validator->getMessageBag()->add('captcha', $translate_mess['captcha']);
            }
            return Redirect::to(URL::previous().'#product_preview_tabs')->withInput()->withErrors($validator->messages())->with('contactError',true);
        }
        else {
            
            $ip=All::ip_adress();
            $data=array(
            'roba_id'=>$data['comment-roba_id'],
            'ip_adresa'=>$ip,
            'ime_osobe'=>$data['comment-name'],
            'pitanje'=>$data['comment-message'],
            'komentar_odobren'=>0,
            'datum'=>date("Y-m-d"),
            'odgovoreno'=>0,
            'ocena'=>$data['comment-review']
            );
            
            DB::table('web_b2c_komentari')->insert($data);

            return Redirect::back()->with('success_comment_message',true);
        }
    }

    public function newsletter_add(){
        $lang = Language::multi() ? Request::segment(1) : null;

            $email=Input::get('email');
            
            if(Session::has('newsletter_time_security') && (time() - intval(Session::get('newsletter_time_security'))) < (24*60*60)){
                echo Language::trans("E-mail je već registrovan").".";
            }else if(DB::table('web_b2c_newsletter')->where('email',$email)->count() > 0){
                echo Language::trans("E-mail je već registrovan").".";
            }
            else {
                $kod = Support::custom_encrypt($email);

                 $body="Poštovani,<br><br>Prijavili ste se za prijem novosti iz naše prodavnice.<br>
                       Da biste aktivirali prijem novosti, molimo Vas da <a href='".Options::base_url().Url_mod::slug_trans('newsletter-potvrda')."/".$kod."' target='_blank'>KLIKNETE OVDE<a> kako biste potvrdili Vašu adresu.<br><br>
                       Ukoliko se niste Vi prijavili i ovu poruku poruku ste primili greškom, molimo Vas da je zanemarite.<br><br>
                       S poštovanjem,<br>
                       ". Options::company_name()   ."<br>"
                        . Options::company_adress() .", ". Options::company_city() ."<br>"    
                        ."mob: ". Options::company_phone()  ."<br>"
                        ."fax: ".   Options::company_fax();

                $subject="Prijava za newsletter na ".Options::company_name();

                WebKupac::send_email_to_client($body,$email,$subject);

                Session::put('newsletter_time_security',time());
                echo Language::trans("Potvrdite prijavu za newsletter preko vašeg mail-a").".";

            }
            
    }

    public function newsletter_confirm(){
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
        $kod = Request::segment(2+$offset);

        $email = Support::custom_decrypt($kod);
        $validator = Validator::make(array('email'=>$email),array('email' => 'required|email|unique:web_b2c_newsletter,email'),Language::validator_messages());

        if(!$validator->fails()){
            $data=array('email'=>$email,'aktivan'=>1);
            DB::table('web_b2c_newsletter')->insert($data);
        }
        
        return Redirect::to('/');
    }


    public function poll_answer(){
        $ip_adress = All::ip_adress();
        $web_b2c_narudzbina_id= Input::get('order_id') ? Input::get('order_id') : null;
        $inputs = Input::get();
        unset($inputs['order_id']);

        foreach($inputs as $name => $value){
            if(strpos($name, 'poll_text_answer_') !== false){
                $anketa_pitanje_id = intval(str_replace('poll_text_answer_','',$name));
                $validator = Validator::make(['poll_answer_text' => $value],['poll_answer_text' => 'regex:'.Support::regex().'|max:500'],Language::validator_messages());
                if($validator->fails() || is_null(DB::table('anketa_odgovor')->where('anketa_pitanje_id',$anketa_pitanje_id)->first())){
                    if($validator->fails()){
                        return Redirect::back()->withInput()->with('pollTextError',Language::trans('Polje sadrži ne doyvoljene karaktere!'));
                    }
                    if(is_null(DB::table('anketa_odgovor')->where('anketa_pitanje_id',$anketa_pitanje_id)->first())){
                        return Redirect::back()->withInput()->with('pollTextError',Language::trans('Niste popunili neko od polja!'));
                    }
                }
            }
        }

        $dateNow = date('Y-m-d H:i:s');
        foreach($inputs as $name => $value){
            $anketa_odgovor_text = null;
            $anketa_odgovor_id = null;
            if(strpos($name, 'poll_text_answer_') !== false){
                $anketa_pitanje_id = intval(str_replace('poll_text_answer_','',$name));
                $anketa_odgovor_text = $value;
                $anketa_odgovor_id = DB::table('anketa_odgovor')->where('anketa_pitanje_id',$anketa_pitanje_id)->pluck('anketa_odgovor_id');
            }else{
                $anketa_pitanje_id = intval(str_replace('poll_check_answer_','',$name));
                $anketa_odgovor_id = $value;
            }
            DB::table('anketa_odgovor_korisnik')->insert(array('anketa_pitanje_id'=>$anketa_pitanje_id, 'anketa_odgovor_id'=>$anketa_odgovor_id, 'anketa_odgovor_text' => $anketa_odgovor_text, 'ip'=>$ip_adress, 'web_kupac_id'=>(($kupac_id = Cart::kupac_id()) > 0 ? $kupac_id : null), 'web_b2c_narudzbina_id' => $web_b2c_narudzbina_id, 'datum'=> $dateNow));
        }



        return Redirect::back()->with('pollAnswerSuccess',Language::trans('Vaša anketa je poslata').'.');        
    }

    public function poll_content(){
        $web_b2c_narudzbina_id= Input::get('order_id') ? Input::get('order_id') : null;
        $anketa = DB::table('anketa')->where('narudzbina_izabran',1)->first();
        if(is_null($anketa)){
            return '';
        }

        return View::make('shop/themes/'.Support::theme_path().'partials/poll',array('anketa_id'=>$anketa->anketa_id, 'web_b2c_narudzbina_id'=>$web_b2c_narudzbina_id));
    }

}


 
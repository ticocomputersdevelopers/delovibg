<?php
use Service\Mailer;

class ArticleController extends Controller {

	public function article(){
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
        $article = Request::segment(2+$offset);
        $slika_id = Request::segment(3+$offset);

        $roba_id=Product::get_product_id($article);
        if($roba_id != 0){
           if($roba_id>0 && Product::checkView($roba_id)){
            All::articleView($roba_id);
            $seo = Seo::article($roba_id);
            $image = DB::table('web_slika')->where(array('roba_id'=>$roba_id, 'flag_prikazi'=>1))->orderBy('akcija','desc')->first();

            $slika_parent_id = 0;
            if(Options::web_options(310) == 1){
                $glavne_slike = Product::get_list_images($roba_id,true);
                if($slika_id){
                    $slika_parent_id = $slika_id;
                }else{
                    $slika_parent_id = isset($glavne_slike[0]) ? $glavne_slike[0]->web_slika_id : 0;
                }
                $slike = Product::get_list_child_images($roba_id,$slika_parent_id);
            }else{
                $slike = Product::get_list_images($roba_id);
                $glavne_slike = array();
            }

            $data=array(
            "strana"=>'artikal',
            "title"=>$seo->title,
            "description"=>$seo->description,
            "keywords"=>$seo->keywords,
            "og_image"=> !is_null($image) ? $image->putanja : null,
            "roba_id"=>$roba_id,
            "grupa_pr_id"=>DB::table('roba')->where('roba_id',$roba_id)->pluck('grupa_pr_id'),
            "proizvodjac_id"=>DB::table('roba')->where('roba_id',$roba_id)->pluck('proizvodjac_id'),
            "articles"=> Product::get_related($roba_id),
            "vezani_artikli"=>DB::table('vezani_artikli')->where('roba_id',$roba_id)->orderBy('vezani_roba_id','asc')->get(),
            "srodni_artikli_ids"=> Product::srodniIds($roba_id),
            "srodni_artikli"=>DB::table('srodni_artikli')->where('roba_id',$roba_id)->orderBy('srodni_roba_id','asc')->get(),
            "fajlovi" => DB::select("SELECT * FROM web_files WHERE roba_id = ".$roba_id." ORDER BY vrsta_fajla_id ASC"),
            "glavne_slike" => $glavne_slike,
            "slika_parent_id" => $slika_parent_id,
            "slike" => $slike,
            "slika_big" => Product::web_slika_big($roba_id,$slika_parent_id)
            );

            return View::make('shop/themes/'.Support::theme_path().'pages/article_details',$data);
           }
        }
        
        if(($new_link = Url_mod::checkOldLink(str_replace(Options::base_url(),'',Request::url()))) != null){
            return Redirect::to($new_link);
        }
        $data=array(
            "strana"=>'Not found',
            "org_strana"=>'Not found',
            "title"=>'Not found',
            "description"=>'Not found',
            "keywords"=>'Not found'
        );
        $content = View::make('shop/themes/'.Support::theme_path().'pages/not_found', $data)->render();
        return Response::make($content, 404);
	}

    public function customer_inform(){

        $roba_id = Input::get('roba_id');
        $email = Input::get('mail');
        $name = Input::get('name');
        $telefon = Input::get('telefon');

        $response = 'NO';
        $count = DB::table('customer_inform')->where('roba_id',$roba_id)->where('email',$email)->count();
        if($count<1){
            DB::table('customer_inform')->insert(array('roba_id'=>$roba_id,'email'=>$email,'name'=>$name,'telefon'=>$telefon));


            $body=" <img src='https://delovi.bgelektronik.rs/images/bg-logo - black.png' style='max-height:65px;'/><p style='padding:8px 0;'> <div style='padding-bottom:10px; padding-top:0;'> Poštovani, </div> Kupac ".$name." sa mailom <span style='color:#0083c1;'>".$email."</span> i brojem telefona: ".$telefon.", želi da bude obavešten kada traženi proizvod: <b>" . Product::seo_title($roba_id)." </b> bude na stanju.<p>";

            $subject="Obaveštenje za proizvod ". Product::seo_title($roba_id)." ";
            Mailer::send(Options::company_email(),Options::company_email(),$subject, $body);   
            // Mailer::send('milica.mikic@bgelektronik.com','milica.mikic@bgelektronik.com',$subject, $body);   
            $response = 'OK';
        }
        return $response; 

    }

    public function blanco()
    {
        $offset = Language::segment_offset();
        $id = Request::segment(2+$offset);

        $data=array(
            'strana'=>'blanco',
            'title'=>'blanco',
            "description"=>'blanco',
            "keywords"=>'blanco'
        );
        return View::make('shop/themes/'.Support::theme_path().'pages/blanco', $data);
    }

    public function infinite_articles()
    {
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;

        $orgStrana = Request::segment(2+$offset);
        $strana = Url_mod::slug_convert_page($orgStrana);

        if($strana==""){
            $strana=All::get_page_start();
        }

        if($strana == 'svi-proizvodi'){
            
            $offset = Language::segment_offset();
            $lang = Language::multi() ? Request::segment(1) : null;

            $grupa_pr_id=0;
            All::check_category($grupa_pr_id);
              
            $level2=array();
            Groups::allGroups($level2,$grupa_pr_id);

            $select="SELECT DISTINCT r.roba_id";
            $roba=" FROM roba r LEFT JOIN roba_grupe rg ON rg.roba_id = r.roba_id".Product::checkLager('join').Product::checkActiveGroup('join').Product::checkImage('join').Product::checkCharacteristics('join')."";
            $where=" WHERE r.flag_aktivan = 1 AND r.flag_prikazi_u_cenovniku = 1 ".Product::checkLager().Product::checkActiveGroup().Product::checkImage().Product::checkPrice().Product::checkDescription().Product::checkCharacteristics()."AND ( r.grupa_pr_id IN(".implode(",",$level2).") OR rg.grupa_pr_id IN(".implode(",",$level2)."))";
            $where_init=$where;;


            $count=count(DB::select($select.$roba.$where));

            if(Session::has('limit')){
                $limit=Session::get('limit');
            }
            else {
                $limit=20;
            }

            if(Input::get('page')){
                $pageNo = Input::get('page');
            }else{
                $pageNo = 1;
            }

            $offset = ($pageNo-1)*$limit;
            
            if(Session::has('order')){
                if(Session::get('order')=='price_asc')
                {
                $artikli=DB::select($select.", r.".Options::checkCena()."".$roba.$where." ORDER BY r.".Options::checkCena()." ASC LIMIT ".$limit." OFFSET ".$offset."");
                }
                else if(Session::get('order')=='price_desc'){
                $artikli=DB::select($select.", r.".Options::checkCena()."".$roba.$where." ORDER BY r.".Options::checkCena()." DESC LIMIT ".$limit." OFFSET ".$offset."");
                }
                else if(Session::get('order')=='news'){
                $artikli=DB::select($select.", r.roba_id".$roba.$where." ORDER BY r.roba_id DESC LIMIT ".$limit." OFFSET ".$offset."");
                }
                else if(Session::get('order')=='name'){
                $artikli=DB::select($select.", r.naziv_web".$roba.$where." ORDER BY r.naziv_web ASC LIMIT ".$limit." OFFSET ".$offset."");
                }
                else if(Session::get('order')=='rbr'){
                $artikli=DB::select($select.", r.rbr".$roba.$where." ORDER BY r.rbr ASC LIMIT ".$limit." OFFSET ".$offset."");
                }
            }
            else {
                $artikli=DB::select($select.", r.".Options::checkCena()."".$roba.$where." ORDER BY r.".Options::checkCena()." ".(Options::web_options(207) == 0 ? "ASC" : "DESC")." LIMIT ".$limit." OFFSET ".$offset."");
            }

            $newArtikli = $artikli;
        } elseif($strana == 'artikli') {
            // ARTIKLI FIRST
            $proizvodjac = Request::get('m');
            $karakteristike = Request::get('ch');
            $cene = Request::get('pr');

            $grupa_pr_id=Request::segment(3+$offset);
            All::check_category($grupa_pr_id);

      
            $level2=array();
            Groups::allGroups($level2,$grupa_pr_id);

            $select="SELECT DISTINCT r.roba_id";
            $roba=" FROM roba r LEFT JOIN roba_grupe rg ON rg.roba_id = r.roba_id".Product::checkLager('join').Product::checkActiveGroup('join').Product::checkImage('join').Product::checkCharacteristics('join')."";
            $where=" WHERE r.flag_aktivan = 1 AND r.flag_prikazi_u_cenovniku = 1 ".Product::checkLager().Product::checkActiveGroup().Product::checkImage().Product::checkPrice().Product::checkDescription().Product::checkCharacteristics()."AND ( r.grupa_pr_id IN(".implode(",",$level2).") OR rg.grupa_pr_id IN(".implode(",",$level2)."))";
            $where_init=$where;
            $proiz_array=array();
            $krak_array=array();

            if(!is_null($proizvodjac) && !is_null($karakteristike)){
                if($proizvodjac!="0" && $karakteristike!="0"){

                    $krak_array=explode("-",$karakteristike);
                    $proiz_array=explode("-",$proizvodjac);

                    if(Options::filters_type()==0){
                        $query_proz=""; 
                        foreach($proiz_array as $proiz_id){
                            $query_proz .= $proiz_id.",";
                        }
                        $where .= " AND proizvodjac_id IN (".substr($query_proz,0,-1).")";

                        $query_karak="";
                        foreach($krak_array as $row){
                            $query_karak .= " grupa_pr_vrednost_id = ".$row." OR";
                        }

                        $i = count(DB::select("SELECT DISTINCT grupa_pr_naziv_id FROM grupa_pr_vrednost WHERE grupa_pr_vrednost_id IN (".implode(",",$krak_array).")"));

                        $where .= " AND r.roba_id IN ( SELECT roba_id FROM web_roba_karakteristike WHERE ".substr($query_karak, 0, -3)." GROUP BY roba_id HAVING COUNT(roba_id) >= ".$i.")";                    

                    }else{
                        $where .= " AND proizvodjac_id = ".$proizvodjac."";
                        $query_karak="";
                        $i=0;
                        foreach($krak_array as $row){
                            $i++;
                            $query_karak .= " grupa_pr_vrednost_id = ".$row." OR";
                        }

                        $where .= " AND r.roba_id IN ( SELECT roba_id FROM web_roba_karakteristike WHERE ".substr($query_karak, 0, -3)." GROUP BY roba_id HAVING COUNT(roba_id) = ".$i.")";                 

                    }       

                }

                if($proizvodjac!="0" && $karakteristike=="0"){
                    $proiz_array=explode("-",$proizvodjac);
                    $krak_array[]="-1";
                    if(Options::filters_type()==0){
                        $query_proz="";
                        foreach($proiz_array as $proiz_id){
                            $query_proz .= $proiz_id.",";
                        }
                        $where .= " AND proizvodjac_id IN (".substr($query_proz,0,-1).")";
                    }else{
                        $where .= " AND proizvodjac_id = ".$proizvodjac."";
                    }   

                }

                if($proizvodjac=="0" && $karakteristike!="0"){
                    $proiz_array[]="0";     

                    $krak_array=explode("-",$karakteristike);

                    if(Options::filters_type()==0){

                        $query_karak="";
                        foreach($krak_array as $row){
                            $query_karak .= " grupa_pr_vrednost_id = ".$row." OR";
                        }

                        $i = count(DB::select("SELECT DISTINCT grupa_pr_naziv_id FROM grupa_pr_vrednost WHERE grupa_pr_vrednost_id IN (".implode(",",$krak_array).")"));

                        $where .= " AND r.roba_id IN ( SELECT roba_id FROM web_roba_karakteristike WHERE ".substr($query_karak, 0, -3)." GROUP BY roba_id HAVING COUNT(roba_id) >= ".$i.")";                    

                    }else{

                        $query_karak="";
                        $i=0;
                        foreach($krak_array as $row){
                            $i++;
                            $query_karak .= " grupa_pr_vrednost_id = ".$row." OR";
                        }

                        $where .= " AND r.roba_id IN ( SELECT roba_id FROM web_roba_karakteristike WHERE ".substr($query_karak, 0, -3)." GROUP BY roba_id HAVING COUNT(roba_id) = ".$i.")";                 

                    }   

                }
                
            }


            if(count(Groups::deca($grupa_pr_id)) == 0){
                $select_max = "SELECT MAX(web_cena) AS max_web_cena";
                $artikli_cena = DB::select($select_max.$roba.$where);
                $max_web_cena = $artikli_cena[0] ? $artikli_cena[0]->max_web_cena : 0;

                $select_min = "SELECT MIN(web_cena) AS min_web_cena";
                $artikli_cena = DB::select($select_min.$roba.$where);
                $min_web_cena = $artikli_cena[0] ? $artikli_cena[0]->min_web_cena : 0;
            }
            if(!is_null($cene)){
                $cene_arr = explode('-',$cene);
                if($cene_arr[1]>0 && $cene_arr[1]>=$cene_arr[0]){
                    $where .= " AND web_cena BETWEEN ".$cene_arr[0]." AND ".ceil($cene_arr[1])."";
                }
            }

            $count=count(DB::select($select.$roba.$where));

            if(Session::has('limit')){
                $limit=Session::get('limit');
            }
            else {
                $limit=20;
            }

            if(Input::get('page')){
                $pageNo = Input::get('page');
            }else{
                $pageNo = 1;
            }

            $offset = ($pageNo-1)*$limit;
            
            if(Session::has('order')){
                if(Session::get('order')=='price_asc')
                {
                $artikli=DB::select($select.", r.".Options::checkCena()."".$roba.$where." ORDER BY r.".Options::checkCena()." ASC LIMIT ".$limit." OFFSET ".$offset."");
                }
                else if(Session::get('order')=='price_desc'){
                $artikli=DB::select($select.", r.".Options::checkCena()."".$roba.$where." ORDER BY r.".Options::checkCena()." DESC LIMIT ".$limit." OFFSET ".$offset."");
                }
                else if(Session::get('order')=='news'){
                $artikli=DB::select($select.", r.roba_id".$roba.$where." ORDER BY r.roba_id DESC LIMIT ".$limit." OFFSET ".$offset."");
                }
                else if(Session::get('order')=='name'){
                $artikli=DB::select($select.", r.naziv_web".$roba.$where." ORDER BY r.naziv_web ASC LIMIT ".$limit." OFFSET ".$offset."");
                }
                else if(Session::get('order')=='rbr'){
                $artikli=DB::select($select.", r.rbr".$roba.$where." ORDER BY r.rbr ASC LIMIT ".$limit." OFFSET ".$offset."");
                }
            }
            else {
                // $artikli=DB::select($select.", r.".Options::checkCena()."".$roba.$where." ORDER BY r.".Options::checkCena()." ".(Options::web_options(207) == 0 ? "ASC" : "DESC")." LIMIT ".$limit." OFFSET ".$offset."");
                $artikli=DB::select($select.", r.roba_id".$roba.$where." ORDER BY r.roba_id DESC LIMIT ".$limit." OFFSET ".$offset."");
            }

            $newArtikli = $artikli;

            // END OF ARTIKLI FIRST
        }

        $data=array(
            'strana'=>'infinite-articles',
            'title'=>'infinite-articles',
            'newArtikli'=>$newArtikli,
            "description"=>'infinite-articles',
            "keywords"=>'infinite-articles'
        );
        return View::make('shop/themes/'.Support::theme_path().'pages/infinite_articles', $data);
    }

    public function article_link(){
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
        $roba_id = Input::get('roba_id');

        return Url_mod::slug_trans('artikal').'/'.Url_mod::slugify(Product::seo_title($roba_id));
    }
    
    public function quick_view(){
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
        $roba_id = Input::get('roba_id');

        $roba = DB::table('roba')->where('roba_id',$roba_id)->first();
        $image = Product::web_slika($roba_id);
        $proizvodjac = DB::table('proizvodjac')->where('proizvodjac_id',$roba->proizvodjac_id)->first();

        $data = array(
            'roba_id' => $roba_id,
            'image' => $image,
            'proizvodjac' => $proizvodjac,
            );
        return View::make('shop/themes/'.Support::theme_path().'partials/products/ajax/quick_view_contant',$data)->render();
    }
    
    public function stampanje()
    {
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
       
        $roba_id = Request::segment(2+$offset);

        $data = [
            'roba_id' => $roba_id,
        ];
        $pdf = App::make('dompdf');
        //var_dump($pdf);die;
        
        $pdf->loadView('shop.themes.bsmodern.pages.stampanje', $data);
        try {
            return $pdf->stream();
        } catch (Exception $e) {
            return Redirect::to(Options::base_url().Url_mod::slug_trans('artikal').'/'.Url_mod::slugify(Product::seo_title($roba_id)));
        }


    }

}
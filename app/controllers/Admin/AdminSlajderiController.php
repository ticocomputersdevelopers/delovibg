<?php

class AdminSlajderiController extends Controller {

    public function slider($slider_id,$slider_item_id=null,$lang_id=null){
    	if(is_null($slider_item_id)){
    		if(!is_null($slider_id) && $slider_id > 0){
    			$slider_item_id = DB::table('slajder_stavka')->where('slajder_id',$slider_id)->orderBy('rbr','asc')->pluck('slajder_stavka_id');
    		}else{
                $slider_item_id = 0;
            }
    	}
    	if(is_null($lang_id)){
    		$lang_id = AdminLanguage::defaultLangObj()->jezik_id;
    	}

        $selectedSlider = $slider_id > 0 ? DB::table('slajder')->where('slajder_id',$slider_id)->first() : (object) ['slajder_id' => 0, 'slajder_tip_id' =>  DB::table('slajder_tip')->orderBy('slajder_tip_id','asc')->pluck('slajder_tip_id'), 'naziv'=>'', 'flag_aktivan' => 1, 'slajder_device_id' => 0];
        $selectedSliderItem = $slider_item_id > 0 && !is_null($slider_item=AdminSlajder::sliderItem($slider_item_id)) ? $slider_item : (object) ['slajder_id' => $selectedSlider->slajder_id, 'slajder_stavka_id' => 0, 'flag_aktivan' => 1, 'image_path' => '', 'datum_od' => null, 'datum_do' => null];
        
    	$data = array(
    		'title' => 'Slajder',
            'strana' => 'slajder',
    		'lang_id' => $lang_id,
            "jezici" => DB::table('jezik')->where('aktivan',1)->get(),
            'sliders' => DB::table('slajder')->orderBy('slajder_id','asc')->get(),
            'sliderItems' => $slider_id > 0 ? AdminSlajder::sliderItems($slider_id) : [],
            'selectedSlider' => $selectedSlider,
            'selectedSliderItem' => $selectedSliderItem,
            'sliderItemLang' => $slider_item_id > 0 && !is_null($slider_item_lang=AdminSlajder::sliderItemLang($slider_item_id,$lang_id)) ? $slider_item_lang : (object) ['slajder_stavka_id' => $selectedSliderItem->slajder_stavka_id, 'jezik_id' => $lang_id, 'naslov' => '', 'sadrzaj' => '', 'naslov_dugme' => '', 'link' => '', 'alt' => '']
    	);

        return View::make('admin/page',$data);
    }

    public function slider_edit(){
    	$data = Input::get();
    	$slajder_id = $data['slajder_id'];
    	$jezik_id = $data['jezik_id'];
    	unset($data['slajder_id']);
    	unset($data['jezik_id']);

        $validator = Validator::make($data,
        array(
            'slajder_tip_id' => 'required|integer',
            'slajder_device_id' => 'required|integer',
            'naziv' => 'required|regex:'.AdminSupport::regex().'|max:255',
            'flag_aktivan' => 'required|in:0,1'
        ),
        array(
    		'required' => 'Niste popunili polje!',
    		'regex' => 'Unesite odgovarajuči karakter!',
    		'max' => 'Uneli ste neodgovarajući broj karaktera!',
    		'integer' => 'Vrednost mora da bude broj',
    		'in' => 'Unesite odgovarajuči karakter!'
        ));

        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }else{
        	if($slajder_id == 0){
        		DB::table('slajder')->insert([
		            'slajder_tip_id' => $data['slajder_tip_id'],
                    'slajder_device_id' => $data['slajder_device_id'],
		            'naziv' => $data['naziv'],
		            'flag_aktivan' => $data['flag_aktivan']
        		]);

        		$slajder_id = DB::table('slajder')->max('slajder_id');

                $tipNaziv = DB::table('slajder_tip')->where('slajder_tip_id',$data['slajder_tip_id'])->pluck('naziv');
                if($tipNaziv == 'Slajder'){
                    AdminSupport::saveLog('SLAJDERI_DODAJ', array($slajder_id));  
                }else{
                    AdminSupport::saveLog('BANERI_DODAJ', array($slajder_id));
                }
        	}else{
        		DB::table('slajder')->where('slajder_id',$slajder_id)->update([
                    'slajder_tip_id' => $data['slajder_tip_id'],
                    'naziv' => $data['naziv'],
                    'slajder_device_id' => $data['slajder_device_id'],
                    'flag_aktivan' => $data['flag_aktivan']
        		]);

                $tipNaziv = DB::table('slajder_tip')->where('slajder_tip_id',$data['slajder_tip_id'])->pluck('naziv');
                if($tipNaziv == 'Slajder'){
                    AdminSupport::saveLog('SLAJDERI_IZMENI', array($slajder_id));  
                }else{
                    AdminSupport::saveLog('BANERI_IZMENI', array($slajder_id));
                }
        	}

        	return Redirect::to('/admin/slider/'.$slajder_id)->with('message','Uspešno ste sačuvali podatke.');
        }    
    }

    public function slider_item_edit(){
        $data = Input::get();
        $slajder_stavka_id = $data['slajder_stavka_id'];
        $slajder_id = $data['slajder_id'];
        $jezik_id = $data['jezik_id'];
        unset($data['slajder_stavka_id']);
        unset($data['slajder_id']);
        unset($data['jezik_id']);
        $slajder = DB::table('slajder')->where('slajder_id',$slajder_id)->first();
        $sliderTip = DB::table('slajder_tip')->where('slajder_tip_id',$slajder->slajder_tip_id)->first();

        $validator = Validator::make($data,
        array(
            'flag_aktivan' => 'required|in:0,1',
            'link' => 'max:500',
            'alt' => 'max:255',
            'naslov' => 'max:100',
            'naslov_dugme' => 'max:100'
        ),
        array(
            'required' => 'Niste popunili polje!',
            'max' => 'Uneli ste neodgovarajući broj karaktera!',
            'in' => 'Unesite odgovarajuči karakter!'
        ));

        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }else{
            $max_image_size = 10000;
            $image = Input::file('image_path');

            $validator = Validator::make(
                array('image_path' => $image),
                array('image_path' => ($slajder_stavka_id == 0 ? 'required|' : '').'mimes:jpg,png,jpeg,gif|max:'.strval($max_image_size)),
                array(
                    'required' => 'Niste izabrali fajl.',
                    'mimes' => 'Neodgovarajući format slike. Dozvoljeni formati su jpg, png i jpeg.',
                    'max' => 'Maksimalna veličina slike je '.strval($max_image_size).'.',
                    )
                );
            if($validator->fails()){
                return Redirect::back()->withInput()->withErrors($validator->messages());
            }
            if($sliderTip->naziv == 'Slajder'){
                $path = 'images/slider/';
            }else{
                $path = 'images/banners/';
            }

            if($slajder_stavka_id == 0){
                $rbr = 1;
                if(count(DB::table('slajder_stavka')->where('slajder_id',$slajder_id)->get()) > 0){
                    $rbr = DB::table('slajder_stavka')->where('slajder_id',$slajder_id)->max('rbr') + 1;
                }
                DB::table('slajder_stavka')->insert([
                    'slajder_id' => $slajder_id,
                    'flag_aktivan' => $data['flag_aktivan'],
                    'rbr' => $rbr,
                    'datum_od' => !empty($data['datum_od']) ? $data['datum_od'] : null,
                    'datum_do' => !empty($data['datum_do']) ? $data['datum_do'] : null
                ]);

                $slajder_stavka_id = DB::table('slajder_stavka')->max('slajder_stavka_id');
            }else{
                DB::table('slajder_stavka')->where('slajder_stavka_id',$slajder_stavka_id)->update([
                    'flag_aktivan' => $data['flag_aktivan'],
                    'datum_od' => !empty($data['datum_od']) ? $data['datum_od'] : null,
                    'datum_do' => !empty($data['datum_do']) ? $data['datum_do'] : null
                ]);
            }
            if($image){
                $name = $slajder_stavka_id.'.'.$image->getClientOriginalExtension();
                $image->move($path, $name);
                DB::table('slajder_stavka')->where('slajder_stavka_id',$slajder_stavka_id)->update(array('image_path'=>$path.$name)); 
            }

            if(!empty($data['sadrzaj']) || !empty($data['link']) || !empty($data['alt']) || !empty($data['naslov']) || !empty($data['naslov_dugme'])){ 
                $langData = [
                        'sadrzaj' => !empty($data['sadrzaj']) ? $data['sadrzaj'] : null,
                        'link' => !empty($data['link']) ? $data['link'] : null,
                        'alt' => !empty($data['alt']) ? $data['alt'] : null,
                        'naslov' => !empty($data['naslov']) ? $data['naslov'] : null,
                        'naslov_dugme' => !empty($data['naslov_dugme']) ? $data['naslov_dugme'] : null
                    ];

                if(!is_null(DB::table('slajder_stavka_jezik')->where(['slajder_stavka_id'=>$slajder_stavka_id,'jezik_id'=>$jezik_id])->first())){

                    DB::table('slajder_stavka_jezik')->where(['slajder_stavka_id'=>$slajder_stavka_id,'jezik_id'=>$jezik_id])->update($langData);
                }else{
                    $langData['slajder_stavka_id'] = $slajder_stavka_id;
                    $langData['jezik_id'] = $jezik_id;

                    DB::table('slajder_stavka_jezik')->insert($langData);
                }
            }

            $tipNaziv = DB::table('slajder_tip')->where('slajder_tip_id',DB::table('slajder')->where('slajder_id',$slajder_id)->pluck('slajder_tip_id'))->pluck('naziv');
            if($tipNaziv == 'Slajder'){
                AdminSupport::saveLog('SLAJDERI_IZMENI', array($slajder_id));  
            }else{
                AdminSupport::saveLog('BANERI_IZMENI', array($slajder_id));
            }

            return Redirect::to('/admin/slider/'.$slajder_id.'/'.$slajder_stavka_id.'/'.$jezik_id)->with('message','Uspešno ste sačuvali podatke.');

        }    
    }

    public function slider_delete($slider_id){
        DB::table('sekcija_stranice')->where(['slajder_id'=>$slider_id])->update(['slajder_id'=>null]);
        DB::table('slajder_stavka')->where('slajder_id',$slider_id)->delete();
        DB::table('slajder')->where('slajder_id',$slider_id)->delete();

        $tipNaziv = DB::table('slajder_tip')->where('slajder_tip_id',DB::table('slajder')->where('slajder_id',$slider_id)->pluck('slajder_tip_id'))->pluck('naziv');
        if($tipNaziv == 'Slajder'){
            AdminSupport::saveLog('SLAJDERI_OBRISI', array($slider_id));  
        }else{
            AdminSupport::saveLog('BANERI_OBRISI', array($slider_id));
        }

    	return Redirect::to('/admin/slider/0');
    }
    public function slider_item_delete($slider_item_id){
        $slider_id = DB::table('slajder_stavka')->where('slajder_stavka_id',$slider_item_id)->pluck('slajder_id');
        DB::table('slajder_stavka')->where('slajder_stavka_id',$slider_item_id)->delete();

        $tipNaziv = DB::table('slajder_tip')->where('slajder_tip_id',DB::table('slajder')->where('slajder_id',$slider_id)->pluck('slajder_tip_id'))->pluck('naziv');
        if($tipNaziv == 'Slajder'){
            AdminSupport::saveLog('SLAJDERI_IZMENI', array($slider_id));  
        }else{
            AdminSupport::saveLog('BANERI_IZMENI', array($slider_id));
        }

        return Redirect::to('/admin/slider/'.$slider_id.'/0');
    }

    public function slider_items_position(){
        $slajder_id = Input::get('slajder_id');
        $order_arr = Input::get('order');

        foreach($order_arr as $key => $val){
            DB::table('slajder_stavka')->where(['slajder_id' => $slajder_id, 'slajder_stavka_id'=>$val])->update(array('rbr'=>($key+1)));
        }
        
        $tip = DB::table('slajder_tip')->where('slajder_tip_id',DB::table('slajder')->where('slajder_id',$slajder_id)->pluck('slajder_tip_id'))->pluck('naziv');

        if($tip == 'Slajder'){
            AdminSupport::saveLog('SLAJDER_POZICIJA', array($slajder_id));
        }else{
            AdminSupport::saveLog('BANER_POZICIJA', array($slajder_id));
        }
    }

    public function bg_uload(){
        
        $slika_ime=$_FILES['bgImg']['name'];
        $slika_tmp_ime=$_FILES['bgImg']['tmp_name'];
        $naziv = Input::get('naziv');
        if( Input::get('link')){
            $link  = Input::get('link');
        }else{
             $link  = '#!';
        }
        if( Input::get('link2')){
            $link2  = Input::get('link2');
        }else{
             $link2  = '#!';
        }
        if ( !empty($slika_ime) ) {
            move_uploaded_file($slika_tmp_ime,"./images/upload/" . $slika_ime);

        $data = array(
            'naziv' => $naziv,
            'link' => $link,
            'link2' => $link2,
            'img' => "./images/upload/" . $slika_ime,
            'tip_prikaza' => 3
        );
        
        $log_ids_arr = DB::select("SELECT baneri_id from baneri where tip_prikaza = '3'");

        $count = DB::table('baneri')->where('tip_prikaza', 3)->count();

        if($count == 1){
            DB::table('baneri')->where('tip_prikaza', 3)->update($data);
        } else {
            DB::table('baneri')->where('tip_prikaza', 3)->insert($data);
        }          

        AdminSupport::saveLog('BANERI_SLIKA');
        return Redirect::back()->with('message','Uspešno ste dodali sliku.');
        }elseif(!empty($link) || empty($link)){
            $data = array(
                'naziv' => $naziv,
                'link' => $link,
                'link2' => $link2,
                'tip_prikaza' => 3
            );

            $count = DB::table('baneri')->where('tip_prikaza', 3)->count();

            if($count == 1){
                DB::table('baneri')->where('tip_prikaza', 3)->update($data);
            } else {
                DB::table('baneri')->where('tip_prikaza', 3)->insert($data);
            } 

            $log_ids_arr = DB::table('baneri')->max('baneri_id');

            AdminSupport::saveLog('BANERI_LINKOVI');

            return Redirect::back()->with('message','Uspešno ste dodali linkove.'); 
        }elseif(!empty($link2) || empty($link2)){
            $data = array(
                'naziv' => $naziv,
                'link' => $link,
                'link2' => $link2,
                'tip_prikaza' => 3
            );

            $count = DB::table('baneri')->where('tip_prikaza', 3)->count();

            if($count == 1){
                DB::table('baneri')->where('tip_prikaza', 3)->update($data);
            } else {
                DB::table('baneri')->where('tip_prikaza', 3)->insert($data);
            } 

            AdminSupport::saveLog('BANERI_LINKOVI', $log_ids_arr);

            return Redirect::back()->with('message','Uspešno ste dodali linkove.');
        }else{
            return Redirect::back()->with('message','Unesite podatke.');
        }
    }

    public function bgImgDelete() {
        AdminSupport::saveLog('BANERI_SLIKA_OBRISI');
        DB::table('baneri')->where('tip_prikaza', 3)->delete();
        return Redirect::back();
    }

}
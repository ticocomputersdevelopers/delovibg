<?php
use Service\Drip;

class AdminKupciPartneriController extends Controller {



	public function getKupci($vrsta_kupca='n',$status_registracije='n',$search=null){

		$kupci = AdminKupci::getKupci($vrsta_kupca,$status_registracije,$search);

		$data=array(
	                "strana" =>'kupci',
	                "title" => 'Kupci',
	                "vrsta_kupca" => $vrsta_kupca,
	                "status_registracije" => $status_registracije,
	                "search" => !is_null($search) ? $search : '',
	                "web_kupac" => $kupci
	            );
		return View::make('admin/page', $data);
		}
	public function getKupciSearch($vrsta_kupca='n',$status_registracije='n'){
		return Redirect::to(AdminOptions::base_url().'admin/kupci_partneri/kupci/'.$vrsta_kupca.'/'.$status_registracije.'/'.urlencode(Input::get('search')));
	}
	public function getPartneri(){
		$page = Input::get('page');
		$search = Input::get('search');
		$partner_vrsta_id = Input::get('partner_vrsta_id');
		$api_aktivan = Input::get('api_aktivan');
		$b2b_access = Input::get('b2b_access');
		$dokumenti = Input::get('dokumenti') ? true : false;
		$parent_id = Input::get('parent_id') ? Input::get('parent_id') : null;

		$page = !is_null($page) ? $page : 1;
		$search = !is_null($search) ? $search : null;
		$partner_vrsta_id = !is_null($partner_vrsta_id) ? $partner_vrsta_id : 0;
		$api_aktivan = (!is_null($api_aktivan) && $api_aktivan != 'null') ? $api_aktivan : null;
		$b2b_access = (!is_null($b2b_access) && $b2b_access != 'null') ? ($b2b_access == '1' ? true : false) : null;


		//EXPORT
		if(Input::get('export') == 1){
			$partneri=AdminKupci::getPartneriSearch($search,$partner_vrsta_id,$api_aktivan,$b2b_access,null,1,$parent_id);

			$vrsta_fajla = Input::get('vrsta_fajla') ? Input::get('vrsta_fajla') : 'xls';
			$imena_kolona = Input::get('imena_kolona') ? Input::get('imena_kolona') : 'null';

			if($imena_kolona != 'null'){
				$kolone = explode('-',$imena_kolona);
			}else{
				$kolone = array('naziv','telefon','email');
			}

			if($vrsta_fajla == 'xml'){

				$xml = new DOMDocument("1.0","UTF-8");
				$root = $xml->createElement("partneri");
				$xml->appendChild($root);

				foreach($partneri as $row){
					
					$partner   = $xml->createElement("partner");
					
					if(in_array('naziv',$kolone)){
						AdminSupport::xml_node($xml,'naziv',$row->naziv,$partner);
					}
					if(in_array('telefon',$kolone)){
						AdminSupport::xml_node($xml,'telefon',$row->telefon,$partner);
					}
					if(in_array('email',$kolone)){
						AdminSupport::xml_node($xml,'email',$row->mail,$partner);
					}
					if(in_array('adresa',$kolone)){
						AdminSupport::xml_node($xml,'adresa',$row->adresa,$partner);
					}
					if(in_array('mesto',$kolone)){
						AdminSupport::xml_node($xml,'mesto',$row->mesto,$partner);
					}
					if(in_array('sifra',$kolone)){
						AdminSupport::xml_node($xml,'sifra',$row->sifra,$partner);
					}
					
					$root->appendChild($partner);
				}
				$xml->formatOutput = true;
				if(File::exists("files/partneri.xml")){
					File::delete("files/partneri.xml");
				}
				$xml->save("files/partneri.xml") or die("Error");
				$file = "files/partneri.xml";  

				AdminSupport::saveLog('EXPORT_MAIL', array());
				return Response::download($file);
			}elseif($vrsta_fajla == 'xls'){
				header("Content-Type: application/vnd.ms-excel");
				header("Content-disposition: attachment; filename=export_kupci.xls");
		    	$printNaziv="";
		    	$printAdresa="";
		    	$printMesto="";
		    	$printEmail="";
		    	$printTelefon="";	
		    	$printSifra="";			
					if(in_array('naziv',$kolone)){						
						$printNaziv="Naziv";
					}
					if(in_array('adresa',$kolone)){
						$printAdresa="Adresa";
					}
					if(in_array('mesto',$kolone)){
						$printMesto="Mesto";
					}
					if(in_array('email',$kolone)){
						$printEmail="Email";
					}
					if(in_array('telefon',$kolone)){
						$printTelefon="Telefon";
					}
					if(in_array('sifra',$kolone)){
						$printSifra="Sifra";
					}
				echo $printNaziv."\t".$printAdresa."\t".$printMesto."\t".$printEmail."\t".$printTelefon."\t".$printSifra."\t\n";
		    	foreach($partneri as $row){				
		    		$naziv="";
		    		$adresa="";
		    		$mesto="";
		    		$email="";
		    		$telefon="";
		    		$sifra="";
					if(in_array('naziv',$kolone)){						
						$naziv=$row->naziv;
					}
					if (in_array('adresa',$kolone)) {
						$adresa=$row->adresa;
					}
					if (in_array('mesto',$kolone)) {
						$mesto=$row->mesto;
					}
					if (in_array('email',$kolone)) {
						$email=$row->mail;
					}
					if (in_array('telefon',$kolone)) {
						$telefon=$row->telefon;
					}
					if (in_array('sifra',$kolone)) {
						$sifra=$row->sifra;
					}
					echo $naziv."\t".$adresa."\t".$mesto."\t".$email."\t".$telefon."\t".$sifra."\t\n";
		    	}
		    	die;
		    }else{
			    return Redirect::to(AdminOptions::base_url().'/admin/kupci_partneri/partneri');
			}
		}

		$partneri=AdminKupci::getPartneriSearch($search,$partner_vrsta_id,$api_aktivan,$b2b_access,20,$page,$parent_id);
		$data=array(
	        "strana"=>'partneri',
	        "title"=> 'Partneri',
	        "search" => !is_null($search) ? $search : '',
	        "partner_vrsta_id" => strval($partner_vrsta_id),
	        "api_aktivan" => strval($api_aktivan),
	        "b2b_access" => $b2b_access,
	        "dokumenti" => $dokumenti,
	        "partneri" => $partneri,
	        "count" => count(AdminKupci::getPartneriSearch($search,$partner_vrsta_id,$api_aktivan,$b2b_access,null,1,$parent_id)),
			"partner_vrste" => DB::table('partner_vrsta')->orderBy('partner_vrsta_id','asc')->get()
	    );

	    if($dokumenti){
			return View::make('dokumenti/pages/partneri', $data);
	    }else{
	    	return View::make('admin/page', $data);
	    }
	}

	public function partneriFilter(){
		$inputs = Input::get();
		$inputs = array_map(function($input){
			return urlencode($input);
		},$inputs);

		return Redirect::to(AdminOptions::base_url().((isset($inputs['dokumenti']) && $inputs['dokumenti'] == '1') ? 'dokumenti' : 'admin/kupci_partneri').'/partneri?'.http_build_query($inputs));
	}


	public function getKupac($id) {
		$data=array(
			"strana"=>'kupac',
			"title"=> 'Kupac ',
			"web_kupac_id" => AdminKupci::getKupac($id, 'web_kupac_id') == null ? 0 : AdminKupci::getKupac($id, 'web_kupac_id'),
			"ime" => AdminKupci::getKupac($id, 'ime'),
			"prezime" => AdminKupci::getKupac($id, 'prezime'),
			"flag_vrsta_kupca" => AdminKupci::getKupac($id, 'flag_vrsta_kupca'),
			"naziv" => AdminKupci::getKupac($id, 'naziv'),
			"pib" => AdminKupci::getKupac($id, 'pib'),
			"adresa" => AdminKupci::getKupac($id, 'adresa'),
			"maticni_br" => AdminKupci::getKupac($id, 'maticni_br'),
			"mesto" => AdminKupci::getKupac($id, 'mesto'), 
			"telefon" => AdminKupci::getKupac($id, 'telefon'),
			"fax" => AdminKupci::getKupac($id, 'fax'),
			"email" => AdminKupci::getKupac($id, 'email'),
			"telefon_mobilni" => AdminKupci::getKupac($id, 'telefon_mobilni'),
			"lozinka" => base64_decode(AdminKupci::getKupac($id, 'lozinka')),
			"flag_prima_poruke" => AdminKupci::getKupac($id, 'flag_prima_poruke'),
			"status_registracije" => AdminKupci::getKupac($id, 'status_registracije'),
			"ulica_id" => AdminKupci::getKupac($id, 'ulica_id'),
			"broj" => AdminKupci::getKupac($id, 'broj'),

		);
		return View::make('admin/page', $data);
		}	
		
	public function deleteKupac($id,$confirm){
		$check_confirm = $confirm=='1'?true:false;
		$obj_korpa = DB::table('web_b2c_korpa')->select('web_b2c_korpa_id')->where('web_kupac_id', $id);
		$obj_narudzbina = DB::table('web_b2c_narudzbina')->select('web_b2c_narudzbina_id')->where('web_kupac_id', $id);
		$check_korpa = $obj_korpa->count()>0?false:true;
		$check_narudzbina = $obj_narudzbina->count()>0?false:true;

		if($check_korpa && $check_narudzbina){
			AdminSupport::saveLog('KUPCI_OBRISI', array($id));
			DB::table('web_kupac')->where('web_kupac_id', $id)->delete();
		}else{
			if($check_confirm){
				AdminSupport::saveLog('KUPCI_OBRISI', array($id));
				DB::table('web_b2c_narudzbina_stavka')->whereIn('web_b2c_narudzbina_id',array_map('current',$obj_narudzbina->get()))->delete();
				$obj_narudzbina->delete();
				DB::table('web_b2c_korpa_stavka')->whereIn('web_b2c_korpa_id',array_map('current',$obj_korpa->get()))->delete();
				$obj_korpa->delete();

				DB::table('web_kupac')->where('web_kupac_id', $id)->delete();
			}else{
				return Redirect::to('admin/kupci_partneri/kupci')->with('confirm_kupac_id',$id);
			}
		}
		return Redirect::to('admin/kupci_partneri/kupci')->with('message','Uspešno ste obrisali sadržaj.'); 
	}

	public function snimiKaoPartner($id){
		$kupac = DB::table('web_kupac')->where('web_kupac_id', $id)->get();

		foreach ($kupac as $row) {
			$naziv = $row->naziv;
			$adresa = $row->adresa;	
			$telefon = $row->telefon;
			$email = $row->email;
			$mesto = $row->mesto;
			$fax = $row->fax;
			$pib = $row->pib;
		}
		$old_value = DB::table('partner')->max('partner_id');
		DB::table('partner')->insert([
			'naziv' => $naziv, 
			'adresa' => $adresa,
			'telefon' => $telefon,
			'mail' => $email,
			'mesto' => $mesto,
			'drzava_id' => 0,
			'fax' => $fax,
			'pib' => $pib
			]);
		$new_value = DB::table('partner')->max('partner_id');

		AdminSupport::saveLog('KUPAC_KAO_PARTNER_DODAJ', range($old_value+1, $new_value));
		return Redirect::to('admin/kupci_partneri/kupci'); 
	}

	public function saveKupac($id){
		$data = Input::get();
		
		if(isset($data['flag_vrsta_kupca'])){
			$data['flag_vrsta_kupca'] = 1;
		} else {
			$data['flag_vrsta_kupca'] = 0;
		}
		if($data['flag_vrsta_kupca'] == 0){
		
			$data['maticni_br'] = 0;
		}
		if(isset($data['status_registracije'])){
			$data['status_registracije'] = 1;
		} else {
			$data['status_registracije'] = 0;
		}

		if(isset($data['flag_prima_poruke'])){
			$data['flag_prima_poruke'] = 1;
		} else {
			$data['flag_prima_poruke'] = 0;
		}
		
		$api_posta = false;
		if(AdminNarudzbine::api_posta(3)){
			$api_posta = true;
		}

		$messages = array(
    		'required' => 'Niste popunili polje!',
    		'regex' => 'Unesite odgovarajuči karakter!',
    		'email' => 'Unesite odgovarajuči mail!',
    		'between' => 'Uneli ste neodgovarajući broj karaktera!',
    		'digits_between' => 'Uneli ste neodgovarajući broj karaktera!',
    		'max' => 'Uneli ste neodgovarajući broj karaktera!',
    		'unique' => 'Mail već postoji u bazi!',
    		'numeric' => 'Vrednost mora da bude broj'
		);

		$validator_arr = array(
 			'telefon' => 'regex:'.Support::regex().'|between:3,15',
            'email' => 'email|unique:web_kupac,email,'.$data['web_kupac_id'].',web_kupac_id,status_registracije,1|between:5,50',
            'fax' => 'between: 3, 20|regex:'.Support::regex().'',
            'telefon_mobilni' => 'between: 3, 20|regex:'.Support::regex().''
		);

		if ($data['flag_vrsta_kupca']==1) {
			$validator_arr['naziv'] = 'required|regex:'.Support::regex().'|max:200';
            $validator_arr['pib'] = 'required|numeric|digits_between:9,9';
            $validator_arr['maticni_br'] = 'numeric|digits_between:8,8';
		}else{
			$validator_arr['ime'] = 'required|regex:'.Support::regex().'|max:100';
			$validator_arr['prezime'] = 'required|regex:'.Support::regex().'|max:100';
		}

		if ($data['status_registracije']==1) {
			$validator_arr['lozinka'] = 'required|regex:'.Support::regex().'|between:3,20';
		}else{
			$validator_arr['lozinka'] = 'regex:'.Support::regex().'|between:3,20';
		}

		if(AdminNarudzbine::api_posta(3)){
			$validator_arr['broj'] = 'required|regex:'.Support::regex().'|max:20';
		}else{
			$validator_arr['adresa'] = 'required|regex:'.Support::regex().'|between:3,50';
 			$validator_arr['mesto'] = 'required|regex:'.Support::regex().'|between:3,50';
		}

		$validator = Validator::make($data, $validator_arr, $messages);
		if ($validator->passes()) {
			unset($data['opstina']);
			unset($data['mesto_id']);
			$data['lozinka'] = base64_encode($data['lozinka']);
			if(isset($data['maticni_br']) && empty($data['maticni_br'])){
				$data['maticni_br'] = null;
			}

			if($data['web_kupac_id']==0){
				unset($data['web_kupac_id']);
				$data['datum_kreiranja'] = date('Y-m-d H:i:s');
				DB::table('web_kupac')->insert($data);
				AdminSupport::saveLog('KUPAC_DODAJ', array(DB::table('web_kupac')->max('web_kupac_id')));
				$data['web_kupac_id'] = DB::select("SELECT currval('web_kupac_web_kupac_id_seq') as new_id")[0]->new_id;
			}else{
				DB::table('web_kupac')->where('web_kupac_id',$data['web_kupac_id'])->update($data);
				AdminSupport::saveLog('KUPAC_IZMENI', array($data['web_kupac_id']));
			}

            if(Config::get('app.livemode') && Options::gnrl_options(3060) == 1 && $data['status_registracije'] == 1){
                $drip = new Drip();
                $drip->addOrUpdateSubscriber(DB::table('web_kupac')->where('web_kupac_id',$data['web_kupac_id'])->first());
            }			
			$message='Uspešno ste sačuvali podatke.';
			return Redirect::to(AdminOptions::base_url().'admin/kupci_partneri/kupci/'.$data['web_kupac_id'])->with('message',$message);
		}else{
			return Redirect::back()->withInput()->withErrors($validator);
		}
	}

	public function getPartner($id) {
		if($id > 0 && !is_null($user = AdminSupport::getLoggedUser('KOMERCIJALISTA')) 
			&& is_null(DB::table('partner')->rightJoin('partner_komercijalista','partner.partner_id','=','partner_komercijalista.partner_id')->where(['partner.partner_id' => $id, 'partner_komercijalista.imenik_id' => $user->imenik_id])->first())){
			return Response::make('Forbidden', 403);
		}

		$dokumenti = (Input::get('dokumenti') && Input::get('dokumenti') == '1');
		$import_check = in_array(117, AdminKupci::checkVrstaPartner($id));

		$data=array(
			"strana"=>'partner',
			"title"=> 'Partner',
			"partner_id" => AdminKupci::getPartner($id, 'partner_id') == null ? 0 : AdminKupci::getPartner($id, 'partner_id'),
			"naziv" => AdminKupci::getPartner($id, 'naziv'),
			"pib" => trim(AdminKupci::getPartner($id, 'pib')),
			"adresa" => AdminKupci::getPartner($id, 'adresa'),
			"mesto" => AdminKupci::getPartner($id, 'mesto'),
			"telefon" => AdminKupci::getPartner($id, 'telefon'),
			"fax" => AdminKupci::getPartner($id, 'fax'),
			"mail" => AdminKupci::getPartner($id, 'mail'),
			"sifra" => AdminKupci::getPartner($id, 'sifra'),
			"naziv_puni" => AdminKupci::getPartner($id, 'naziv_puni'),
			"drzava_id" => AdminKupci::getPartner($id, 'drzava_id'),
			"racun" => AdminKupci::getPartner($id, 'racun'),
			"broj_maticni" => AdminKupci::getPartner($id, 'broj_maticni'),
			"broj_registra" => AdminKupci::getPartner($id, 'broj_registra'),
			"broj_resenja" => AdminKupci::getPartner($id, 'broj_resenja'),
			"delatnost_sifra" => AdminKupci::getPartner($id, 'delatnost_sifra'),
			"delatnost_naziv" => AdminKupci::getPartner($id, 'delatnost_naziv'),
			"rabat" => AdminKupci::getPartner($id, 'rabat'),
			"limit_p" => AdminKupci::getPartner($id, 'limit_p'),
			"valuta_id" => AdminKupci::getPartner($id, 'valuta_id'),
			"tip_cene_id" => AdminKupci::getPartner($id, 'tip_cene_id'),
			"login" => AdminKupci::getPartner($id, 'login'),
			"password" => AdminKupci::getPartner($id, 'password'),
			"api_username" => AdminKupci::getPartner($id, 'api_username'),
			"api_password" => AdminKupci::getPartner($id, 'api_password'),
			"aktivan_api" => AdminKupci::getPartner($id, 'aktivan_api'),
			"napomena" => AdminKupci::getPartner($id, 'napomena'),
			"preracunavanje_cena" => AdminKupci::getPartner($id, 'preracunavanje_cena'),
			"kontakt_osoba" => AdminKupci::getPartner($id,'kontakt_osoba'),
			"imenik_id" => array_map('current',DB::table('partner_komercijalista')->select('imenik.imenik_id AS imenik_id')->join('imenik','imenik.imenik_id','=','partner_komercijalista.imenik_id')->where(array('partner_id'=>$id,'kvota'=>32.00))->get()),
			"ulica_id" => AdminKupci::getPartner($id,'ulica_id') ? AdminKupci::getPartner($id,'ulica_id') : 0,
			"broj" => AdminKupci::getPartner($id,'broj'),
			"service_username" => $import_check ? AdminKupci::findPartnerImport($id,'service_username') : '',
			"service_password" => $import_check ? AdminKupci::findPartnerImport($id,'service_password') : '',
			"auto_link" => $import_check ? AdminKupci::findPartnerImport($id,'auto_link') : '',
			"import_naziv_web" => $import_check ? AdminKupci::findPartnerImport($id,'import_naziv_web') : '',
			"auto_import" => $import_check ? AdminKupci::findPartnerImport($id,'auto_import') : '',
			"auto_import_short" => $import_check ? AdminKupci::findPartnerImport($id,'auto_import_short') : '',
			"zadnji_kurs" => $import_check ? AdminKupci::findPartnerImport($id,'zadnji_kurs') : '',
			"kartica" => AdminPartneri::getB2Bkartica($id),
			"sumPotrazuje" => AdminPartneri::getPotrazuje($id),
			"saldo" => AdminPartneri::getSum($id),
			"sumDuguje" => AdminPartneri::getDuguje($id),
			"dokumenti" => $dokumenti
		);

	    if($dokumenti){
			return View::make('dokumenti/pages/partner', $data);
	    }else{
	    	return View::make('admin/page', $data);
	    }
	}

	public function savePartner($id){
		$inputs = Input::get();
		$dokumenti = (isset($inputs['dokumenti']) && $inputs['dokumenti'] == '1');
		unset($inputs['dokumenti']);
		$data = $inputs;

		if(isset($data['preracunavanje_cena'])) {
			$data['preracunavanje_cena'] = 1;
		} else {
			$data['preracunavanje_cena'] = 0;
		}

		unset($data['service_username']);
		unset($data['service_password']);
		unset($data['auto_link']);
		unset($data['zadnji_kurs']);
		unset($data['import_naziv_web']);
		unset($data['auto_import']);
		unset($data['auto_import_short']);
		unset($data['imenik_id']);

		$messages = array(
    		'required' => 'Niste popunili polje!',
    		'regex' => 'Unesite odgovarajuči karakter!',
    		'email' => 'Unesite odgovarajuči mail!',
    		'between' => 'Unesite više od tri karakera ili brojeva!',
    		'unique' => 'Mail već postoji u bazi!',
    		'max'=>'Prekoračili ste limit',
    		'numeric'=>'Unesite samo brojeve',
    		'digits_between' => 'Dužina sadržaja nije odgovarajuća'
		);
		if($id == 0 || $id == null){
			$mail_validation = 'email|max:50|unique:partner,mail';
		}else{
			$mail_validation = 'email|max:50|unique:partner,mail,'.$data['partner_id'].',partner_id';
		}

	    if(isset($data['aktivan_api'])){
            $data['aktivan_api'] = 1;
        }else{
            $data['aktivan_api'] = 0; 
        }
		$rules = array(
                'naziv' => 'required|regex:'.Support::regex().'|between:2,50',
                'sifra' => 'regex:'.Support::regex().'|max:15',
                'adresa' => 'required|regex:'.Support::regex().'|between:3,100',
                'mesto' => 'regex:'.Support::regex().'|between:2,100',                
                'fax' => 'regex:'.Support::regex().'|numeric',                
                'limit_p' => 'digits_between:0,9|numeric',
                'pib' => 'required|numeric|digits_between:9,9',
                'broj_maticni' => 'numeric|digits_between:8,8',
                'napomena' => 'regex:'.Support::regex().'|max:500',
                'login' => 'regex:'.Support::regex().'|max:200',
                'naziv_puni' => 'regex:'.Support::regex().'|max:200',
                'racun' => 'regex:'.Support::regex().'|between:0,35',
                'broj_registra' => 'regex:'.Support::regex().'|max:30',
                'broj_resenja' => 'regex:'.Support::regex().'|max:15',
                'delatnost_sifra' => 'digits_between:4,4|numeric',
                'delatnost_naziv' => 'regex:'.Support::regex().'|max:50',
                'rabat' => 'digits_between:0,9|numeric',
                'password' => 'regex:'.Support::regex().'|between:3,20',
                'service_username' => 'max:255',
                'service_password' => 'max:255',
                'auto_link' => 'max:500'
            );
		if(AdminNarudzbine::api_posta(3)){
			$rules['broj'] = 'required|regex:'.Support::regex().'|max:25';
		}else{
			$rules['adresa'] = 'required|regex:'.Support::regex().'|between:3,100';
            $rules['mesto'] = 'required|regex:'.Support::regex().'|between:2,100';
		}
		$validator = Validator::make($inputs, $rules, $messages);
        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator);
        } else {

            if(AdminNarudzbine::api_posta(3)){
        		unset($data['opstina']);
                unset($data['mesto_id']);
                $data['drzava_id'] = 1;
            }        	
			if($id == 0 || $id == null){
				$data['partner_id'] = DB::select("SELECT nextval('partner_partner_id_seq')")[0]->nextval;
				$data['rabat'] = 0.00;
				$data['limit_p'] = 0.00;
				$data['datum_kreiranja'] = date('Y-m-d H:i:s');
				
				// unset($data['partner_id']);
				DB::table('partner')->insert($data);

				AdminSupport::saveLog('PARTNER_DODAJ', array(DB::table('partner')->max('partner_id')));

				if(!is_null($user = AdminSupport::getLoggedUser('KOMERCIJALISTA'))){
					AdminPartneri::saveKomercijalista($data['partner_id'],array($user->imenik_id));
				}
			} else {
				DB::table('partner')->where('partner_id', $data['partner_id'])->update($data);

				if(!$dokumenti && in_array(117, AdminKupci::checkVrstaPartner($data['partner_id'])) && Admin_model::check_admin()){
					$data_kolone = array();
					$data_kolone['service_username'] = $inputs['service_username'];
					$data_kolone['service_password'] = $inputs['service_password'];
					$data_kolone['auto_link'] = $inputs['auto_link'];
					$data_kolone['import_naziv_web'] = $inputs['import_naziv_web'];
					$data_kolone['auto_import'] = $inputs['auto_import'];
					$data_kolone['auto_import_short'] = $inputs['auto_import_short'];
					$data_kolone['zadnji_kurs'] = $inputs['zadnji_kurs'];

					DB::table('dobavljac_cenovnik_kolone')->where('partner_id', $data['partner_id'])->update($data_kolone);
				}


				AdminSupport::saveLog('PARTNER_IZMENI', array($data['partner_id']));
			}

			if(!is_null(AdminSupport::getLoggedUser('ADMIN'))){			
				if(isset($inputs['imenik_id'])){
					$imenik_ids = $inputs['imenik_id'];
				}else{
					$imenik_ids = array();
				}
				AdminPartneri::saveKomercijalista($data['partner_id'],$imenik_ids);
			}

			return Redirect::to(AdminOptions::base_url().(!$dokumenti ? 'admin/kupci_partneri' : 'dokumenti').'/partner/'.$data['partner_id'])->with('message','Uspešno ste sačuvali podatke');
		}	
	}

	public function deletePartner($id,$confirm){
		$dokumenti = (Input::get('dokumenti') && Input::get('dokumenti') == '1');
		if($id == 1){
			return Redirect::to(AdminOptions::base_url().(!$dokumenti ? 'admin/kupci_partneri':'dokumenti').'/partneri');
		}

		$check_confirm = $confirm=='1'?true:false;
		$obj_narudzbina = DB::table('web_b2b_narudzbina')->select('web_b2b_narudzbina_id')->where('partner_id', $id);
		$check_narudzbina = $obj_narudzbina->count()>0?false:true;
		$obj_korpa = DB::table('web_b2b_korpa')->select('web_b2b_korpa_id')->where('partner_id', $id);
		$check_korpa = $obj_korpa->count()>0?false:true;
		$obj_partner_je = DB::table('partner_je')->where('partner_id', $id);
		$check_partner_je = $obj_partner_je->count()>0?false:true;
		$obj_dobavljac_cenovnik_kolone = DB::table('dobavljac_cenovnik_kolone')->where('partner_id', $id);
		$check_dobavljac_cenovnik_kolone = $obj_dobavljac_cenovnik_kolone->count()>0?false:true;
		$obj_dobavljac = DB::table('dobavljac_cenovnik')->where('partner_id', $id);
		$check_dobavljac = $obj_dobavljac->count()>0?false:true;
		$obj_kartica = DB::table('partner_kartica')->where('partner_id', $id);
		$check_kartica = $obj_kartica->count()>0?false:true;
		$obj_komercijalista = DB::table('partner_komercijalista')->where('partner_id', $id);
		$check_komercijalista = $obj_komercijalista->count()>0?false:true;
		$obj_kontakt = DB::table('partner_kontakt')->where('partner_id', $id);
		$check_kontakt = $obj_kontakt->count()>0?false:true;
		$obj_lokacija = DB::table('partner_lokacija')->where('partner_id', $id);
		$check_lokacija = $obj_lokacija->count()>0?false:true;
		$obj_rabat_grupa = DB::table('partner_rabat_grupa')->where('partner_id', $id);
		$check_rabat_grupa = $obj_rabat_grupa->count()>0?false:true;
		$obj_racun = DB::table('partner_racun')->where('partner_id', $id);
		$check_racun = $obj_racun->count()>0?false:true;
		$obj_vrsta_dokumenta = DB::table('partner_vrsta_dokumenta')->where('partner_id', $id);
		$check_vrsta_dokumenta = $obj_vrsta_dokumenta->count()>0?false:true;
		$obj_log_b2b_partner = DB::table('log_b2b_partner')->where('partner_id', $id);
		$check_log_b2b_partner = $obj_log_b2b_partner->count()>0?false:true;
		$obj_ponuda = DB::table('ponuda')->where('partner_id', $id);
		$check_ponuda = $obj_ponuda->count()>0?false:true;

		if($check_korpa && $check_narudzbina && $check_partner_je && $check_dobavljac_cenovnik_kolone && $check_dobavljac && $check_kartica && $check_komercijalista && $check_kontakt && $check_lokacija && $check_rabat_grupa && $check_racun && $check_vrsta_dokumenta && $check_log_b2b_partner && $check_ponuda){
			AdminSupport::saveLog('PARTNER_OBRISI', array($id));
			DB::table('partner')->where('partner_id', $id)->delete();
		}else{
			if($check_confirm){
				AdminSupport::saveLog('PARTNER_OBRISI', array($id));
				DB::table('web_b2b_narudzbina_stavka')->whereIn('web_b2b_narudzbina_id',array_map('current',$obj_narudzbina->get()))->delete();
				$obj_narudzbina->delete();
				DB::table('web_b2b_korpa_stavka')->whereIn('web_b2b_korpa_id',array_map('current',$obj_korpa->get()))->delete();
				$obj_korpa->delete();
				$obj_partner_je->delete();
				$obj_dobavljac_cenovnik_kolone->delete();
				DB::table('dobavljac_cenovnik_karakteristike')->where('partner_id', $id)->delete();
				DB::table('dobavljac_cenovnik_slike')->where('partner_id', $id)->delete();

				// $roba_ids = array_map('current',DB::table('roba')->select('roba_id')->where('dobavljac_id', $id)->get());
		        // DB::table('vezani_artikli')->whereIn('vezani_roba_id',$roba_ids)->delete();
		        // DB::table('roba_export')->whereIn('roba_id',$roba_ids)->delete();
		        // DB::table('web_roba_karakteristike')->whereIn('roba_id',$roba_ids)->delete();
		        // DB::table('osobina_roba')->whereIn('roba_id',$roba_ids)->delete();
		        // DB::table('roba')->where('dobavljac_id', $id)->delete();
		        DB::table('roba')->where('dobavljac_id', $id)->update(array('dobavljac_id'=>null,'sifra_d'=>null));

				$obj_dobavljac->delete();
				$obj_kartica->delete();
				$obj_komercijalista->delete();
				$obj_kontakt->delete();
				$obj_lokacija->delete();
				$obj_rabat_grupa->delete();
				$obj_racun->delete();
				$obj_vrsta_dokumenta->delete();
				$obj_log_b2b_partner->delete();
				$obj_ponuda->delete();

				DB::table('partner')->where('partner_id', $id)->delete();
			}else{
				return Redirect::to((!$dokumenti ? 'admin/kupci_partneri/partneri':'dokumenti/partneri'))->with('confirm_partner_id',$id);
			}
		}
		return Redirect::to((!$dokumenti ? 'admin/kupci_partneri':'dokumenti').'/partneri')->with('message','Uspešno ste obrisali sadržaj.'); 

	}

	public function exportKupac($vrsta_fajla,$vrsta_kupca,$status_registracije,$imena_kolona='null',$search=null){

		if($imena_kolona != 'null'){
			$kolone = explode('-',$imena_kolona);
		}else{
			$kolone = array('ime','prezime','naziv','pib','email');
		}

		$db_data = AdminKupci::getKupci($vrsta_kupca,$status_registracije,$search,false);

		if($vrsta_fajla == 'xml'){
			$xml = new DOMDocument("1.0","UTF-8");
			$root = $xml->createElement("kupci");
			$xml->appendChild($root);

			foreach($db_data as $row){
				$kupac   = $xml->createElement("kupac");

				if($row->flag_vrsta_kupca == 0){
					if(in_array('ime',$kolone)){
						AdminSupport::xml_node($xml,'ime',$row->ime,$kupac);
					}
					if(in_array('prezime',$kolone)){
						AdminSupport::xml_node($xml,'prezime',$row->prezime,$kupac);
					}				
				}elseif($row->flag_vrsta_kupca == 1){
					if(in_array('naziv',$kolone)){
						AdminSupport::xml_node($xml,'naziv',$row->naziv,$kupac);
					}
					if(in_array('pib',$kolone)){
						AdminSupport::xml_node($xml,'pib',$row->pib,$kupac);
					}
				}

				if(in_array('email',$kolone)){
					AdminSupport::xml_node($xml,'email',$row->email,$kupac);
				}

				if(in_array('adresa',$kolone)){
					AdminSupport::xml_node($xml,'adresa',$row->adresa,$kupac);
				}

				if(in_array('mesto',$kolone)){
					AdminSupport::xml_node($xml,'mesto',$row->mesto,$kupac);
				}

				if(in_array('telefon',$kolone)){
					AdminSupport::xml_node($xml,'telefon',$row->telefon,$kupac);
				}
				
				$root->appendChild($kupac);
			}
			$xml->formatOutput = true;
			if(File::exists("files/kupci.xml")){
				File::delete("files/kupci.xml");
			}
			$xml->save("files/kupci.xml") or die("Error");
			$file = "files/kupci.xml";  

			// AdminSupport::saveLog('KUPAC_EKSPORT', array());
	    	return Response::download($file);
	    }elseif($vrsta_fajla == 'xls'){
	    	header("Content-Type: application/vnd.ms-excel");
			header("Content-disposition: attachment; filename=export_kupci.xls");
	    	$printIme="";
	    	$printPrezime="";
	    	$printNaziv="";
	    	$printPib="";
	    	$printEmail="";
	    	$printAdresa="";
	    	$printMesto="";
	    	$printTelefon="";
				if(in_array('ime',$kolone)){
						$printIme="Ime";
					}
					if(in_array('prezime',$kolone)){
						$printPrezime="Prezime";
					}				
					if(in_array('naziv',$kolone)){						
						 $printNaziv="Naziv";
					}
					if(in_array('pib',$kolone)){
						 $printPib="Pib";
					}
					if(in_array('email',$kolone)){
						 $printEmail="Email";
					}
					if(in_array('adresa',$kolone)){
						 $printAdresa="Adresa";
					}
					if(in_array('mesto',$kolone)){
						 $printMesto="Mesto";
					}
					if(in_array('telefon',$kolone)){
						 $printTelefon="Telefon";
					}
			echo $printIme."\t".$printPrezime."\t".$printNaziv."\t".$printPib."\t".$printEmail."\t".$printAdresa."\t".$printMesto."\t".$printTelefon."\t\n";
	    	foreach($db_data as $row){
	    		$ime="";
	    		$prezime="";
	    		$naziv="";
	    		$pib="";
	    		$email="";
	    		$adresa="";
	    		$mesto="";
	    		$telefon="";
	    		if($row->flag_vrsta_kupca == 0){
					if(in_array('ime',$kolone)){
						$ime = $row->ime;
					}
					if(in_array('prezime',$kolone)){
						$prezime = $row->prezime;
					}				
				}elseif($row->flag_vrsta_kupca == 1){
					if(in_array('naziv',$kolone)){						
						 $naziv=$row->naziv;
					}
					if(in_array('pib',$kolone)){
						 $pib=$row->pib;
					}
				}
				if (in_array('email',$kolone)) {
					$email=$row->email;
				}
				if (in_array('adresa',$kolone)) {
					$adresa=$row->adresa;
				}
				if (in_array('mesto',$kolone)) {
					$mesto=$row->mesto;
				}
				if (in_array('telefon',$kolone)) {
					$telefon=$row->telefon;
				}
				echo $ime."\t".$prezime."\t".$naziv."\t".$pib."\t".$email."\t".$adresa."\t".$mesto."\t".$telefon."\t\n";
	    	}
	    }else{
		    return Redirect::to(AdminOptions::base_url().'/admin/kupci_partneri/kupci');
		}
	}

	public function partneri_search(){
		$word = trim(Input::get('search'));

		$data=array(
	                "strana"=>'partneri',
	                "title"=> 'Partneri',
	                "partner" => AdminKupci::searchPartneri($word),
	                "word" => $word
	            );
			return View::make('admin/page', $data);
	}
	public function export_newSletter(){
		// header('Content-type: text/plain; charset=utf-8');
		header('Content-Type: application/csv');
	   	header('Content-Disposition: attachment; filename="newsletter.csv"');

		$query=DB::table('web_b2c_newsletter')->get();
		foreach ($query as $email) {
			echo addslashes($email->email), "\n";		
		}
		// var_dump($query);die;
		
       exit();
            
	}

	public function card_refresh($partner_id){
        if(AdminOptions::info_sys('calculus')){
            Calculus::updateCart($partner_id);
        }elseif(AdminOptions::info_sys('infograf')){
            Infograf::updateCart($partner_id);
        }elseif(AdminOptions::info_sys('logik')){
            Logik::updateCart($partner_id);
        }
        
        return Redirect::back();  
	}
	function partner_import()
        {
		$file = Input::file('import_file');

        $validator = Validator::make(array('import_file' => Input::file('import_file')), array('import_file' => 'required'), array('required' => 'Niste izabrali fajl.'));
        if($validator->fails()){
            return Redirect::back()->withErrors($validator->messages());
        }else{
            $extension = $file->getClientOriginalExtension();
            if(Input::hasFile('import_file') && $file->isValid() && ($extension == 'xml' || $extension == 'xls' || $extension == 'xlsx' || $extension == 'csv' || $extension == 'zip')){
                
                if($extension != 'zip'){
                    $file->move("./files/", 'partner_import.'.$extension);
                }else{
                    $file->move("./files/", 'backup_images.'.$extension);
                }

                $success = false;
                if($extension == 'xml'){
                    $success = AdminDirectImport::xml_execute();
                }elseif($extension == 'xls'){
                    $success = AdminDirectImport::xls_execute_partner();
                }elseif($extension == 'xlsx'){
                    $success = AdminDirectImport::xlsx_execute_partner();
                }elseif($extension == 'csv'){
                    $success = AdminDirectImport::csv_execute();
                }elseif($extension == 'zip'){
                    $success = AdminSupport::extract_images();
                }

                if($extension != 'zip'){
                    File::delete('files/partner_import.'.$extension);
                }else{
                    File::delete('files/backup_images.'.$extension);
                }

                if(!$success){
                    return Redirect::back()->withErrors(array('import_file'=>'Fajl je neispravan.'));
                }
                return Redirect::back();
            }
            return Redirect::back()->withErrors(array('import_file'=>'Import iz ovog fajla nije podrzan.'));
        }
    }

    public function userKorisnici($partner_id){
        $partnerKorisnici = DB::table('partner_korisnik')->where('partner_id',$partner_id);
        $partnerKorisnici = $partnerKorisnici->orderBy('naziv','asc')->paginate(20);

        $data =array(
        	'strana'=>"korisnici_partnera",
            'title'=>"Korsnici partnera",
            'description'=>"Korsnici partnera",
            'partner_id'=>$partner_id,
            'partnerKorisnici'=>$partnerKorisnici
        );

        return View::make('admin/page', $data);
    }

    public function userKorisnik($partner_id,$partner_korisnik_id){

        if($partner_korisnik_id > 0){
            $partnerKorisnik = DB::table('partner_korisnik')->where('partner_id',$partner_id)->where('partner_korisnik_id',$partner_korisnik_id)->first();
        }else{
            $partner = DB::table('partner')->where('partner_id',$partner_id)->first();
            $partnerKorisnik = (object) array(
                'partner_korisnik_id' => 0,
                'korisnicko_ime' => '',
                'lozinka' => '',
                'naziv' => '',
                'adresa' => $partner->adresa,
                'mesto' => $partner->mesto,
                'telefon' => $partner->telefon,
                'email' => $partner->mail,
                'aktivan' => 1
            );
        }

        $data=array(
        	'strana'=>"korisnik_partnera",
            "title"=>"korsnik_partnera",
            "description"=>"Korsnik Partnera",
            "partner_id" => $partner_id,
            "partnerKorisnik" => $partnerKorisnik
        );


        return View::make('admin/page', $data);
    }
    public function userKorisnikEdit(){
        $inputs = Input::all();
        $partner_korisnik_id = $inputs['partner_korisnik_id'];

        $rules = array(
                'naziv' => 'required|regex:'.Support::regex().'|between:2,500',
                'adresa' => 'required|regex:'.Support::regex().'|between:3,100',
                'mesto' => 'required|regex:'.Support::regex().'|between:2,100',
                'telefon' => 'required|regex:'.Support::regex().'|between:5,30',
                'email' => 'required|email|max:50|unique:partner_korisnik,email,'.$partner_korisnik_id.',partner_korisnik_id',
                'korisnicko_ime' => 'required|between:2,255|unique:partner_korisnik,korisnicko_ime,'.$partner_korisnik_id.',partner_korisnik_id',
                'lozinka' => 'required|between:3,100'
               
            );
        $messages = array(
                'required' => 'Niste popunili polje!',
                'regex' => 'Polje sadrži neodgovarajuće karaktere!',
                'email' => 'E-mail adresa je neodgovarajuća!',
                'between' => 'Dužina sadžaja nije odgovarajuća!',
                'max' => 'Dužina sadžaja je neodgovarajuća!',
                'numeric'=>'Polje može sadržati samo brojeve',
                'digits_between' => 'Dužina sadžaja nije odgovarajuća!',
                'unique' => 'Vrednost polja već postoji u bazi!'
            );

         $validator = Validator::make($inputs, $rules, $messages);
        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator);
        } else{
            if($partner_korisnik_id > 0){
                DB::table('partner_korisnik')->where('partner_korisnik_id',$partner_korisnik_id)->update($inputs);
            }else{
                unset($inputs['partner_korisnik_id']);
                DB::table('partner_korisnik')->insert($inputs);
            }
            // AdminSupport::saveLog('B2B_PARTNER_IZMENI', array($partner_id));

            return Redirect::to('admin/partner/'.$inputs['partner_id'].'/korisnik/'.($partner_korisnik_id > 0 ? $partner_korisnik_id : DB::table('partner_korisnik')->max('partner_korisnik_id')))->with('message','Uspešno ste sačuvali podatke!');
        }
    }

    public function userKorisnikObrisi($partner_id,$partner_korisnik_id){
        $partnerKorisnik = DB::table('partner_korisnik')->where(array('partner_id'=>$partner_id,'partner_korisnik_id'=>$partner_korisnik_id))->first();
        if(!is_null($partnerKorisnik)){
            DB::table('partner_korisnik')->where('partner_korisnik_id',$partner_korisnik_id)->delete();
            return Redirect::to(AdminOptions::base_url().'admin/partner/'.$partner_id.'/korisnici')->with('message','Uspešno ste obrisali korisnika!');            
        }
        return Redirect::back();
    }

    public function commercialist_b2b_login($partner_id){
		if(!is_null($user = AdminSupport::getLoggedUser('KOMERCIJALISTA')) 
		&& !is_null(DB::table('partner')->rightJoin('partner_komercijalista','partner.partner_id','=','partner_komercijalista.partner_id')->where(['partner.partner_id' => $partner_id, 'partner_komercijalista.imenik_id' => $user->imenik_id])->first())){

    		Session::put('b2b_user_'.B2bOptions::server(), $partner_id);
    		return Redirect::to(AdminOptions::base_url().'b2b');
		}
    	return Redirect::back();
    }

}
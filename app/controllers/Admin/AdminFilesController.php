<?php
use Import\Support;

class AdminFilesController extends Controller {
    function index(){
        if(AdminOptions::gnrl_options(3002)==0){
        AdminSupport::saveLog('WEB_IMPORT_FILE_UPLOAD');
            return Redirect::to(AdminOptions::base_url()."admin/web_import/0/0/0/nn-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn/0/nn-nn");
        }
        $data=array(
            "strana"=>'file_upload',
            "title"=>'Aploadovanje fajla',
        );
        return View::make('admin/page', $data);    	
    }

    function save(){
        $file = Input::file('import_file');
        $podrzan_import_id = Input::get('podrzan_import_id');
        $first = DB::table('podrzan_import')->where('podrzan_import_id',$podrzan_import_id)->first();

        $status = false;
        if($podrzan_import_id != 0 && Input::hasFile('import_file') && $file->isValid()){
            $path_info = Support::import_file_path($first);
            if($file->getClientOriginalExtension() == $first->file_type){
                $file->move($path_info->path,$path_info->name);
                $status = true;
            }
            elseif($file->getClientOriginalExtension() == 'xls' && $first->file_type=='xlsx'){
                if(Support::xls_to_xlsx($file->getPathName(),$path_info->path.$path_info->name)){
                    $status = true;
                }
            }

        }

        if($status){
            return Redirect::to(AdminOptions::base_url().'admin/file-upload')->with('message','Fajl je uspesno aploadovan!');
        }else{
            return Redirect::to(AdminOptions::base_url().'admin/file-upload')->with('alert','Proverite strukturu fajla za dati import!');
        }
    }
}
<?php

class AdminOsobineController extends Controller {

	function index($id = null){
		 $data=array(
            "strana" => 'osobine',
            "title" => 'Osobine',
            "osobina_naziv_id" => $id == null ? 0 : $id,
            "osobine" => AdminOsobine::getAll(),
            "osobina" => AdminOsobine::getProperty($id),
            "vrednosti" => AdminOsobine::getPropertyValues($id)
            );

		  return View::make('admin/page', $data);
	}

	function editProperty(){
		$inputs = Input::get();
		
		if(isset($inputs['aktivna'])){
			$aktivna = 1;
		} else {
			$aktivna = 0;
		}

		if(isset($inputs['prikazi_vrednost'])){
			$prikazi_vrednost = 1;
		} else {
			$prikazi_vrednost = 0;
		}

		if(isset($inputs['color_picker'])){
			$color_picker = 1;
		} else {
			$color_picker = 0;
		}

		if($inputs['rbr'] != ''){
			$rbr = $inputs['rbr'];
		} else {
			$rbr = 0;
		}

		$validator = Validator::make($inputs, array('naziv' => 'required', 'rbr' => 'numeric'));
        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }else{
        	if($inputs['osobina_naziv_id'] == 0){
        		DB::table('osobina_naziv')->insert(array('naziv' => $inputs['naziv'], 'aktivna' => $aktivna, 'prikazi_vrednost' => $prikazi_vrednost, 'color_picker' => $color_picker, 'rbr' => $rbr));

        		AdminSupport::saveLog('SIFARNIK_OSOBINA_DODAJ', array(DB::table('osobina_naziv')->max('osobina_naziv_id')));
        		$message='Uspešno ste sačuvali podatke.';
        		return Redirect::to(AdminOptions::base_url().'admin/osobine/0')->with('message',$message);
			} else {
				DB::table('osobina_naziv')->where('osobina_naziv_id', $inputs['osobina_naziv_id'])->update(array('prikazi_vrednost' => $prikazi_vrednost, 'naziv' => $inputs['naziv'], 'aktivna' => $aktivna, 'color_picker' => $color_picker, 'rbr' => $rbr));

				AdminSupport::saveLog('SIFARNIK_OSOBINA_IZMENI', array($inputs['osobina_naziv_id']));
				$message='Uspešno ste sačuvali podatke.';
				return Redirect::to(AdminOptions::base_url().'admin/osobine/'.$inputs['osobina_naziv_id'])->with('message',$message);
			}
        }
	}

	function editPropertyValue($id, $osobina_vrednost_id){
		$inputs = Input::get();

		if(isset($inputs['aktivna'])){
			$aktivna = 1;
		} else {
			$aktivna = 0;
		}

		if(isset($inputs['boja_css'])){
			$boja_css = $inputs['boja_css'];
		} else {
			$boja_css = '#888888';
		}

		// if($inputs['rbr'] != ''){
		// 	$rbr = $inputs['rbr'];
		// } else {
		// 	$rbr = 0;
		// }

		$validator = Validator::make($inputs, array('vrednost' => 'required', 'rbr' => 'numeric'));

		if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }else{
        	if($osobina_vrednost_id == 0){

        		DB::table('osobina_vrednost')->insert(array('osobina_naziv_id' => $id, 'vrednost' => $inputs['vrednost'], 'aktivna' => $aktivna, 'boja_css' =>  $boja_css));

        		AdminSupport::saveLog('SIFARNIK_OSOBINA_VREDNOST_DODAJ', array(DB::table('osobina_vrednost')->max('osobina_vrednost_id')));
        		$message='Uspešno ste sačuvali podatke.';
        		return Redirect::to(AdminOptions::base_url().'admin/osobine/' . $id)->with('message',$message);
			} else {
				DB::table('osobina_vrednost')->where('osobina_vrednost_id', $osobina_vrednost_id)->update(array('vrednost' => $inputs['vrednost'], 'aktivna' => $aktivna, 'boja_css' => $boja_css));

				AdminSupport::saveLog('SIFARNIK_OSOBINA_VREDNOST_IZMENI', array($osobina_vrednost_id));
				$message='Uspešno ste sačuvali podatke.';
				return Redirect::to(AdminOptions::base_url().'admin/osobine/' . $id)->with('message',$message);
			}
        }
	}

	function deleteProperty($id){
		$query_osobina_vrednost = DB::table('osobina_vrednost')->where('osobina_naziv_id', $id);
			AdminSupport::saveLog('SIFARNIK_OSOBINA_OBRISI', array($id));
		DB::table('osobina_roba')->whereIn('osobina_vrednost_id',array_map('current',$query_osobina_vrednost->select('osobina_vrednost_id')->get()))->delete();
		DB::table('osobina_roba')->where('osobina_naziv_id',$id)->delete();
		$query_osobina_vrednost->delete();
		$delete = DB::table('osobina_naziv')->where('osobina_naziv_id', $id)->delete();

		if($delete){

			return Redirect::to(AdminOptions::base_url().'admin/osobine/0')->with('message', 'Uspešno ste obrisali osobinu!');
		} else {
			return Redirect::to(AdminOptions::base_url().'admin/osobine/0')->with('message', 'Problem sa brisanjem.');
		}
	}

	function deletePropertyValue($id, $vrednost_id){
		AdminSupport::saveLog('SIFARNIK_OSOBINA_VREDNOST_OBRISI', array($vrednost_id));
		DB::table('osobina_roba')->where('osobina_vrednost_id',$vrednost_id)->delete();
		$query = DB::table('osobina_vrednost')->where('osobina_vrednost_id', $vrednost_id)->delete();

		if($query){

			return Redirect::to(AdminOptions::base_url().'admin/osobine/' . $id)->with('message', 'Uspešno ste obrisali vrednost!');
		} else {
			return Redirect::to(AdminOptions::base_url().'admin/osobine/' . $id)->with('message', 'Problem sa brisanjem.');
		}

	}


	// PRODUCT OSOBINE
	public function product_osobine($roba_id,$osobina_kombinacija_id=null){
		$osobine_nazivi = array();
		$osobina_vrednosti = array();
		$kombinacija_vrednosti = array();

		if(!is_null($osobina_kombinacija_id)){
			$osobine_nazivi = DB::table('osobina_naziv')->whereNotIn('osobina_naziv_id',
								array_map('current',DB::table('osobina_vrednost')->select('osobina_naziv_id')->whereIn('osobina_vrednost_id',
									array_map('current',DB::table('osobina_kombinacija_vrednost')->select('osobina_vrednost_id')->where('osobina_kombinacija_id',$osobina_kombinacija_id)->get())
											)->get())
								)->orderBy('rbr','asc')->get();
			if(count($osobine_nazivi) > 0){
				$osobina_vrednosti = DB::table('osobina_vrednost')->where('osobina_naziv_id',$osobine_nazivi[0]->osobina_naziv_id)->get();
			}

			$kombinacija_vrednosti = DB::table('osobina_kombinacija_vrednost')->leftJoin('osobina_vrednost','osobina_vrednost.osobina_vrednost_id','=','osobina_kombinacija_vrednost.osobina_vrednost_id')->leftJoin('osobina_naziv','osobina_naziv.osobina_naziv_id','=','osobina_vrednost.osobina_naziv_id')->where('osobina_kombinacija_id',$osobina_kombinacija_id)->orderBy('osobina_naziv.rbr','asc')->get();
		}

		$data=array(
			"strana" => 'product_osobine',
			"title" => 'Osobine artikla',
			"roba_id" => $roba_id,
			"web_cena" => DB::table('roba')->where('roba_id',$roba_id)->pluck('web_cena'),
			"kombinacije" => DB::table('osobina_kombinacija')->where('roba_id',$roba_id)->orderBy('osobina_kombinacija_id','asc')->get(),
			"osobina_kombinacija_id" => $osobina_kombinacija_id,
			"osobine_nazivi" => $osobine_nazivi,
			"osobina_vrednosti" => $osobina_vrednosti,
			"kombinacija_vrednosti" => $kombinacija_vrednosti
		);

		return View::make('admin/page', $data);
	}
	public function product_osobine_save($roba_id,$osobina_kombinacija_id){
		$data = Input::get();
		$data['aktivna'] = isset($data['aktivna']) && $data['aktivna'] == 'on' ? 1 : 0;

		$validator = Validator::make($data, array('kolicina' => 'required|numeric|digits_between:1,15'));
        if($validator->fails()){
            return Redirect::back()->with('alert', AdminLanguage::transAdmin('Neko od polja nije broj.'));
        }else{
			if($osobina_kombinacija_id == 0){
				$data['roba_id'] = $roba_id;
				DB::table('osobina_kombinacija')->insert($data);
			}else{
				$oldKolicina = DB::table('osobina_kombinacija')->where('osobina_kombinacija_id',$osobina_kombinacija_id)->pluck('kolicina');
				DB::table('osobina_kombinacija')->where('osobina_kombinacija_id',$osobina_kombinacija_id)->update($data);
				$data['kolicina'] -= $oldKolicina;
			}

			$lagerData = array('orgj_id'=>DB::table('imenik_magacin')->where('izabrani',1)->pluck('orgj_id'),'roba_id'=>$roba_id);
			if(DB::table('lager')->where($lagerData)->count() > 0){
				DB::table('lager')->where($lagerData)->increment('kolicina',$data['kolicina']);
			}else{
				$lagerData['kolicina'] = $data['kolicina'];
				DB::table('lager')->insert($lagerData);
			}
		}
		return Redirect::to(AdminOptions::base_url().'admin/product-osobine/'.$roba_id.'/'.$osobina_kombinacija_id)->with('message', AdminLanguage::transAdmin('Uspešno ste sačuvali kombinaciju.'));
	}

	public function product_osobine_delete($roba_id,$osobina_kombinacija_id){
		$data = Input::get();

		$lagerData = array('orgj_id'=>DB::table('imenik_magacin')->where('izabrani',1)->pluck('orgj_id'),'roba_id'=>$roba_id);
		DB::table('lager')->where($lagerData)->decrement('kolicina',DB::table('osobina_kombinacija')->where('osobina_kombinacija_id',$osobina_kombinacija_id)->pluck('kolicina'));

		DB::table('osobina_kombinacija')->where('osobina_kombinacija_id',$osobina_kombinacija_id)->delete();

		return Redirect::to(AdminOptions::base_url().'admin/product-osobine/'.$roba_id)->with('message', AdminLanguage::transAdmin('Uspešno ste obrisali kombinaciju.'));
	}

	public function product_osobine_vrednost_save($roba_id,$osobina_kombinacija_id){
		$data = Input::get();
		$validator = Validator::make($data, array('osobina_vrednost_id' => 'required'));

        if($validator->fails()){
            return Redirect::back()->with('alert', AdminLanguage::transAdmin('Polje ne sme biti prazno.'));
        }

        $kombinacija_vrednosti_ids = array_map('current',DB::table('osobina_kombinacija_vrednost')->select('osobina_vrednost_id')->where(array('osobina_kombinacija_id'=>$osobina_kombinacija_id))->get());
        if($key = array_search($data['old_osobina_vrednost_id'],$kombinacija_vrednosti_ids)){
        	$kombinacija_vrednosti_ids[$key] = $data['osobina_vrednost_id'];
        }else{
        	$kombinacija_vrednosti_ids[] = $data['osobina_vrednost_id'];
        }

        foreach(DB::table('osobina_kombinacija')->where('roba_id',$roba_id)->where('osobina_kombinacija_id','<>',$osobina_kombinacija_id)->get() as $osobina_kombinacija){
        	$postojece_kombinacija_vrednosti_ids = array_map('current',DB::table('osobina_kombinacija_vrednost')->select('osobina_vrednost_id')->where(array('osobina_kombinacija_id'=>$osobina_kombinacija->osobina_kombinacija_id))->get());
        	if(count(array_diff($postojece_kombinacija_vrednosti_ids,$kombinacija_vrednosti_ids)) == 0 && count(array_diff($kombinacija_vrednosti_ids,$postojece_kombinacija_vrednosti_ids)) == 0){
        		return Redirect::back()->with('alert', AdminLanguage::transAdmin('Ova kombinacija već postoji.'));
        		break;
        	}
        }

		if(DB::table('osobina_kombinacija_vrednost')->where(array('osobina_kombinacija_id'=>$osobina_kombinacija_id,'osobina_vrednost_id'=>$data['osobina_vrednost_id']))->count() > 0){
			return Redirect::back()->with('alert', AdminLanguage::transAdmin('Vrednost osobine već postoji.'));
		}

		if($data['old_osobina_vrednost_id'] == 0){
			DB::table('osobina_kombinacija_vrednost')->insert(array('osobina_kombinacija_id'=>$osobina_kombinacija_id,'osobina_vrednost_id'=>$data['osobina_vrednost_id']));
		}else{
			DB::table('osobina_kombinacija_vrednost')->where(array('osobina_kombinacija_id'=>$osobina_kombinacija_id,'osobina_vrednost_id'=>$data['old_osobina_vrednost_id']))->update(array('osobina_vrednost_id'=>$data['osobina_vrednost_id']));
		}

		return Redirect::back()->with('message', AdminLanguage::transAdmin('Uspešno ste sačuvali vrednost.'));
	}

	public function product_osobine_vrednost_delete($roba_id,$osobina_kombinacija_id,$osobina_vrednost_id){
		$data = Input::get();

		DB::table('osobina_kombinacija_vrednost')->where(array('osobina_kombinacija_id'=>$osobina_kombinacija_id,'osobina_vrednost_id'=>$osobina_vrednost_id))->delete();

		return Redirect::to(AdminOptions::base_url().'admin/product-osobine/'.$roba_id.'/'.$osobina_kombinacija_id)->with('message', AdminLanguage::transAdmin('Uspešno ste obrisali vrednost.'));
	}

	public function product_osobine_naziv_vrednosti(){
		$result = '';
		foreach (AdminOsobine::getPropertyValues(Input::get('osobina_naziv_id')) as $vrednost) {
			$result .= '<option value="'.$vrednost->osobina_vrednost_id.'">'.$vrednost->vrednost.'</option>';
		}
		return $result;
	}

	// function product_osobine_index($id,$osobina_naziv_id){
	// 	$data=array(
 //            "strana" => 'product_osobine',
 //            "title" => 'Osobine artikla',
 //            "roba_id" => $id,
 //            "osobina_naziv_id" => $osobina_naziv_id,
 //        	"osobine" => AdminOsobine::getActiveProperties($osobina_naziv_id),
 //        	"naziv" => AdminOsobine::getNaziv(),
 //        	"osobine_artikla" => AdminOsobine::getPropertiesProduct($id)
 //            );

	// 	  return View::make('admin/page', $data);
	// }

	// function product_osobine_add($roba_id){
	// 	$osobina_naziv_id = Input::get('osobina_naziv_id');

	// 	if($osobina_naziv_id > 0){

	// 		DB::table('osobina_roba')->insert(array('roba_id' => $roba_id, 'aktivna' => 0, 'osobina_naziv_id' => $osobina_naziv_id, 'osobina_vrednost_id' => 0, 'rbr' => 0));

	// 		AdminSupport::saveLog('PRODUCT_OSOBINA_ROBA_DODAJ', array($roba_id));

	// 		return Redirect::to(AdminOptions::base_url().'admin/product_osobine/' . $roba_id .'/'. $osobina_naziv_id)->with('message', 'Uspešno ste dodelili osobinu.');
	// 	} else {
	// 		return Redirect::to(AdminOptions::base_url().'admin/product_osobine/' . $roba_id .'/'. $osobina_naziv_id)->with('message', 'Morate izabrati osobinu.');
	// 	}    
	// }

// 	function product_osobine_add_all($roba_id,$osobina_naziv_id){
// //$osobina_naziv_id = Input::get('osobina_naziv_id');var_dump($osobina_naziv_id);die;
// 		foreach(AdminOsobine::getActiveProperties($osobina_naziv_id) as $row){

// 			foreach(DB::table('osobina_vrednost')->where(array('osobina_naziv_id'=>$osobina_naziv_id, 'aktivna'=>1))->get() as $row2){

// 				if(DB::table('osobina_roba')->where(array('roba_id' => $roba_id, 'osobina_naziv_id' => $osobina_naziv_id, 'osobina_vrednost_id' => $row2->osobina_vrednost_id))->count() == 0){

// 					DB::table('osobina_roba')->insert(array('roba_id' => $roba_id, 'aktivna' => 1, 'osobina_naziv_id' => $osobina_naziv_id, 'osobina_vrednost_id' => $row2->osobina_vrednost_id, 'rbr' => $row2->rbr));
// 				}
// 			}
// 			AdminSupport::saveLog('PRODUCT_OSOBINA_DODAJ_SVE', array('id' => $roba_id, 'naziv' => $osobina_naziv_id));
// 		}
// 		return Redirect::to(AdminOptions::base_url().'admin/product_osobine/' . $roba_id .'/'. $osobina_naziv_id);
// 	}

	// function product_osobine_edit($id, $osobina_naziv_id){
	// 	$rbr = Input::get('rbr');
	// 	$osobina_vrednost_id = Input::get('osobina_vrednost_id');
	// 	$aktCheck = Input::get('aktivna');
	// 	$old_vrednost_id = Input::get('old_vrednost_id');

	// 	if(isset($aktCheck)){
	// 		$aktivna = 1;
	// 	} else {
	// 		$aktivna = 0;
	// 	}

	// 	$check = DB::table('osobina_roba')->where('roba_id', $id)->where('osobina_naziv_id', $osobina_naziv_id)->where('osobina_vrednost_id', $osobina_vrednost_id)->first();

	// 	if(!$check){

	// 		DB::table('osobina_roba')->where('roba_id', $id)->where('osobina_naziv_id', $osobina_naziv_id)->where('osobina_vrednost_id', $old_vrednost_id)->update(array('aktivna' => $aktivna, 'osobina_vrednost_id' => $osobina_vrednost_id, 'rbr' => $rbr));

	// 		AdminSupport::saveLog('PRODUCT_OSOBINA_IZMENI', array('id' => $id, 'vrednost'=>$osobina_vrednost_id));

	// 		return Redirect::to(AdminOptions::base_url().'admin/product_osobine/' . $id .'/'. $osobina_vrednost_id)->with('message', 'Uspešno ste sačuvali podatke.');

	// 	} else {

	// 		if($osobina_vrednost_id == 0){

	// 			return Redirect::to(AdminOptions::base_url().'admin/product_osobine/' . $id.'/'. $osobina_vrednost_id)->with('alert', 'Morate izabrati vrednost.');

	// 		} elseif($osobina_vrednost_id == $old_vrednost_id) {

	// 			DB::table('osobina_roba')->where('roba_id', $id)->where('osobina_naziv_id', $osobina_naziv_id)->where('osobina_vrednost_id', $old_vrednost_id)->update(array('aktivna' => $aktivna, 'rbr' => $rbr));


	// 			AdminSupport::saveLog('PRODUCT_OSOBINA_IZMENI', array('id' => $id, 'vrednost'=>$old_vrednost_id));
	// 			return Redirect::to(AdminOptions::base_url().'admin/product_osobine/' . $id.'/'. $osobina_vrednost_id)->with('message', 'Uspešno ste sačuvali podatke.');


	// 		} else {
	// 			return Redirect::to(AdminOptions::base_url().'admin/product_osobine/' . $id.'/'. $osobina_vrednost_id)->with('alert', 'Nije moguće dodeliti vrednost zato što već postoji.');
	// 		}	
	// 	}
	// }

	// function product_osobine_delete($roba_id, $osobina_naziv_id, $osobina_vrednost_id) {

	// 	AdminSupport::saveLog('PRODUCT_OSOBINA_OBRISI', array('id' => $roba_id, 'vrednost'=>$osobina_vrednost_id));
	// 	DB::table('osobina_roba')->where('roba_id', $roba_id)->where('osobina_naziv_id', $osobina_naziv_id)->where('osobina_vrednost_id', $osobina_vrednost_id)->delete();


	// 	return Redirect::to(AdminOptions::base_url().'admin/product_osobine/' . $roba_id .'/'. $osobina_naziv_id)->with('message', 'Uspešno ste obrisali vrednost.');
	// }
	
	// function product_osobine_grupa_delete($roba_id, $osobina_naziv_id, $osobina_vrednost_id) {

	// 	AdminSupport::saveLog('PRODUCT_OSOBINA_OBRISI_GRUPA', array('id' => $roba_id, 'naziv'=>$osobina_naziv_id));
	// 	DB::table('osobina_roba')->where('roba_id', $roba_id)->where('osobina_naziv_id', $osobina_naziv_id)->delete();


	// 	return Redirect::to(AdminOptions::base_url().'admin/product_osobine/' . $roba_id .'/'. $osobina_naziv_id)->with('message', 'Uspešno ste obrisali vrednost.');
	// }
	function position_osobine(){
        $order_arr = Input::get('order');
        $moved = Input::get('moved');
         
        foreach($order_arr as $key => $val){
            DB::table('osobina_vrednost')->where('osobina_vrednost_id',$val)->update(array('rbr'=>$key));
        }
        AdminSupport::saveLog('SIFARNIK_OSOBINA_VREDNOST_POZICIJA', array($moved));
        
    }

	function product_osobine_delete_all($roba_id, $osobina_naziv_id, $osobina_vrednost_id) {
		AdminSupport::saveLog('PRODUCT_OSOBINA_OBRISI_SVE', array($roba_id));

		DB::table('osobina_roba')->where('roba_id', $roba_id)->delete();


		return Redirect::to(AdminOptions::base_url().'admin/product_osobine/' . $roba_id .'/'. $osobina_naziv_id)->with('message', 'Uspešno ste obrisali vrednost.');
	}

}
<?php

class AdminKontaktController extends Controller {
    
    public function kontakt_podaci(){
        
        $preduzece=DB::table('preduzece')->where('preduzece_id',1)->first();
        $lat_long = AdminCommon::lat_long();
		$data=array(
            "strana"=>'kontakt',
            "title"=>"Kontakt podaci",
            "preduzece"=>$preduzece,
            "latitude"=>$lat_long[0],
            "longitude"=>$lat_long[1]
		);
        return View::make('admin/page',$data);

    }
    public function kontakt_update(){
        $data = Input::get();
        $validarr = array(
            'naziv' => 'required|regex:'.AdminSupport::regex().'|max:150',
            'adresa' => 'required|regex:'.AdminSupport::regex().'|max:100',
            'email' => 'required|email',
            'kontakt_osoba' => 'required|regex:'.AdminSupport::regex().'|max:255',
            'telefon' => 'required|regex:'.AdminSupport::regex().'|max:30',
            'fax' => 'regex:'.AdminSupport::regex().'|max:30',
            'mesto' => 'required|regex:'.AdminSupport::regex().'|max:200',
            'ziro' => 'regex:'.AdminSupport::regex().'|max:20',
            'matbr_registra' => 'numeric|digits_between:1,8',
            'pib' => 'required|numeric|digits_between:1,9',
            'delatnost_sifra' => 'regex:'.AdminSupport::regex().'|max:100',
            'latitude' => 'numeric|digits_between:1,15',
            'longitude' => 'numeric|digits_between:1,15',
            'facebook' => 'regex:'.AdminSupport::regex().'|max:200',
            'twitter' => 'regex:'.AdminSupport::regex().'|max:200',
            'google_p' => 'regex:'.AdminSupport::regex().'|max:200',
            'skype' => 'regex:'.AdminSupport::regex().'|max:200'
            );
        if(AdminNarudzbine::api_posta(3)){
            unset($validarr['adresa']);
            unset($validarr['mesto']);
            $validarr['broj'] = 'required|regex:'.AdminSupport::regex().'|max:20';
        }

        $validator = Validator::make($data,$validarr);
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'admin/kontakt-podaci')->withInput()->withErrors($validator->messages());
        }else{
            $preduzece_id = $data['preduzece_id'];
            $data['mapa'] = $data['latitude'].';'.$data['longitude'];
            unset($data['preduzece_id']);
            unset($data['latitude']);
            unset($data['longitude']);
            unset($data['opstina']);

            if(AdminNarudzbine::api_posta(3)){
                $ulica = AdminNarudzbine::ulica($data['ulica_id']);
                if(!is_null($ulica)){
                    $data['adresa'] = $ulica->naziv.' '.$data['broj'];
                }
                $mesto = AdminNarudzbine::mesto($data['ulica_id']);
                if(!is_null($mesto)){
                    $data['mesto'] = $mesto->naziv;
                }
            }

            DB::table('preduzece')->where('preduzece_id',$preduzece_id)->update($data);

            AdminSupport::saveLog('KONTAKT_PODACI_IZMENI');

            return Redirect::to(AdminOptions::base_url().'admin/kontakt-podaci')->with('message','Uspešno ste sačuvali podatke.');
        }

    }

    function upload_logo(){
       $slika_ime=$_FILES['logo']['name'];
         $slika_tmp_ime=$_FILES['logo']['tmp_name'];
            move_uploaded_file($slika_tmp_ime,"images/$slika_ime");
        $data=array('logo'=>"images/".$slika_ime);
        DB::table('preduzece')->where('preduzece_id',1)->update($data);
        AdminSupport::saveLog('LOGO_IZMENI');
          
          return Redirect::back();
    }
    function upload_potpis(){
       $slika_ime=$_FILES['potpis']['name'];
         $slika_tmp_ime=$_FILES['potpis']['tmp_name'];
            move_uploaded_file($slika_tmp_ime,"images/$slika_ime");
        $data=array('potpis'=>"images/".$slika_ime);
        DB::table('preduzece')->where('preduzece_id',1)->update($data);
        AdminSupport::saveLog('Potpis_IZMENI');
          
          return Redirect::back();
    }

}
<?php

class AdminLanguage {

    //prevodi reci za dati jezik
    public static function trans($string,$lang=null){
        if($lang==null){
            $lang = DB::table('jezik')->where('izabrani',1)->pluck('kod');
        }

        $jezik_id = DB::table('jezik')->where(array('kod'=>$lang))->pluck('jezik_id');
        $izabrani_jezik_id =  DB::table('jezik')->where(array('aktivan'=>1, 'izabrani'=>1))->pluck('jezik_id');

        if($izabrani_jezik_id == $jezik_id){
            return $string;
        }else{
            $query = DB::select("SELECT * FROM prevodilac WHERE izabrani_jezik_id = ".$izabrani_jezik_id." AND jezik_id = ".$jezik_id." AND izabrani_jezik_reci IS NOT NULL AND izabrani_jezik_reci ILIKE '".pg_escape_string($string)."'");

            if(count($query) > 0){
                return $query[0]->reci;
            }else{
                $str_arr = explode(' ',$string);
                $trans_arr = array();
                $not_translate = false;
                foreach($str_arr as $str){
                    $query_str = DB::select("SELECT * FROM prevodilac WHERE izabrani_jezik_id = ".$izabrani_jezik_id." AND jezik_id = ".$jezik_id." AND izabrani_jezik_reci IS NOT NULL AND izabrani_jezik_reci ILIKE '".pg_escape_string($str)."'");
                    if(count($query_str) > 0){
                        $trans_arr[] = $query_str[0]->reci;
                    }else{
                        $not_translate = true;
                        $trans_arr[] = $str;
                    }
                }
                return implode(' ',$trans_arr);
            }
        }
    }

    public static function convert($string,$lang=null){
        if($lang==null){
            $lang = DB::table('jezik')->where('izabrani',1)->pluck('kod');
        }

        $jezik_id = DB::table('jezik')->where(array('aktivan'=>1, 'kod'=>$lang))->pluck('jezik_id');
        $izabrani_jezik_id =  DB::table('jezik')->where(array('aktivan'=>1, 'izabrani'=>1))->pluck('jezik_id');

        if($izabrani_jezik_id == $jezik_id){
            return $string;
        }else{
            $query = DB::select("SELECT * FROM prevodilac WHERE izabrani_jezik_id = ".$izabrani_jezik_id." AND jezik_id = ".$jezik_id." AND reci IS NOT NULL AND reci ILIKE '".pg_escape_string($string)."'");

            if(count($query) > 0){
                return $query[0]->izabrani_jezik_reci;
            }else{
                return $string;
                $str_arr = explode(' ',$string);
                $trans_arr = array();
                $not_translate = false;
                foreach($str_arr as $str){
                    $query_str = DB::select("SELECT * FROM prevodilac WHERE izabrani_jezik_id = ".$izabrani_jezik_id." AND jezik_id = ".$jezik_id." AND reci IS NOT NULL AND reci ILIKE '".pg_escape_string($str)."'");
                    if(count($query_str) > 0){
                        $trans_arr[] = $query_str[0]->izabrani_jezik_reci;
                    }else{
                        $not_translate = true;
                        $trans_arr[] = $str;
                    }
                }
                return implode(' ',$trans_arr);
            }
        }
    }

    public static function transAdmin($string){
        $default_jezik_id =  DB::table('jezik')->where(array('kod'=>'sr'))->pluck('jezik_id');
        $admin_jezik_id =  DB::table('jezik')->where(array('aktivan'=>1, 'admin_izabrani'=>1))->pluck('jezik_id');

        if($admin_jezik_id == $default_jezik_id){
            return $string;
        }else{
            $query = DB::select("SELECT * FROM admin_prevodilac WHERE izabrani_jezik_id = ".$default_jezik_id." AND izabrani_jezik_reci IS NOT NULL AND izabrani_jezik_reci ILIKE '".pg_escape_string($string)."'");

            if(count($query) > 0){
                return $query[0]->reci;
            }
            return $string;
        }
    }

	public static function multi(){
		return DB::table('jezik')->where('aktivan',1)->count() > 1;
	}
	
	public static function lang(){
		if(Session::has('lang')){
			return Session::get('lang');
		}
		return DB::table('jezik')->where(array('aktivan'=>1, 'izabrani'=>1))->pluck('kod');
	}

    public static function defaultLangObj(){
        return DB::table('jezik')->where('izabrani',1)->first();
    }
}

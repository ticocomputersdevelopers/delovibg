<?php

class AdminNacinPlacanja {

	public static function all() {
		return DB::table('web_nacin_placanja')->where('web_nacin_placanja_id', '!=', -1)->get();
	}

	public static function getSingle($id){
		return DB::table('web_nacin_placanja')->where('web_nacin_placanja_id', $id)->first();
	}

}
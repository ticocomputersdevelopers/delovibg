<?php

class AdminKupci {

	public static function getKupci($vrsta_kupca='n',$status_registracije='n',$search=null,$paginate=true){
		$where = array();
		if($vrsta_kupca!='n'){
			$where['flag_vrsta_kupca'] = $vrsta_kupca;
		}
		if($status_registracije!='n'){
			$where['status_registracije'] = $status_registracije;
		}

		$kupci_query = DB::table('web_kupac')->where($where);
		if(!is_null($search) && $search != ''){
			$search = urldecode($search);
			foreach(explode(' ',$search) as $word){
				$kupci_query = $kupci_query->where(function($query) use ($word){
					$query->orWhere('ime','ILIKE','%'.$word.'%')->orWhere('email','ILIKE','%'.$word.'%')->orWhere('prezime','ILIKE','%'.$word.'%');
				});
			}
		}
		if ($paginate) {
			return $kupci_query->orderBy('adresa', 'DESC')->paginate(20);		
		}else{
			return $kupci_query->orderBy('adresa', 'DESC')->get();
		}
	}

	public static function getKupciPravna(){
		$kupci = DB::table('web_kupac')->where('web_kupac_id', '!=', -1)->where('flag_vrsta_kupca', 1)->orderBy('web_kupac_id', 'ASC')->paginate(20);
		return $kupci;
	}


	public static function getMesto($mesto_id){
		$mesto = DB::table('mesto')->where('mesto_id', $mesto_id)->pluck('mesto');
		return $mesto;
	}

	public static function getKupac($id, $column){
		$kupac = DB::table('web_kupac')->where('web_kupac_id', $id)->pluck($column);
		return $kupac;
	}

	public static function getPartner($id, $column){
		$partner = DB::table('partner')->where('partner_id', $id)->pluck($column);
		return $partner;
	}

	public static function listDrzava($selected = null){
		foreach(DB::table('drzava')->get() as $row){
				if($row->drzava_id == $selected){
					echo "<option value=".$row->drzava_id." selected>".$row->naziv."</option>";
				}else{
					echo "<option value=".$row->drzava_id.">".$row->naziv."</option>";
				}
			}
	}
	public static function listValuta($selected = null){
		foreach(DB::table('valuta')->get() as $row){
				if($row->valuta_id == $selected){
					echo "<option value=".$row->valuta_id." selected>".$row->valuta_sl."</option>";
				}else{
					echo "<option value=".$row->valuta_id.">".$row->valuta_sl."</option>";
				}
			}
	}
	public static function listTipCene($selected = null){
		foreach(DB::table('tip_cene')->get() as $row){
				if($row->tip_cene_id == $selected){
					echo "<option value=".$row->tip_cene_id." selected>".$row->opis."</option>";
				}else{
					echo "<option value=".$row->tip_cene_id.">".$row->opis."</option>";
				}
			}
	}

	public static function checkVrstaPartner($partner_id){
			return array_map('current', DB::table('partner_je')->where('partner_id', $partner_id)->select('partner_vrsta_id')->get());
	}

	public static function getPartneri(){
		$partneri = DB::table('partner')->where('partner_id', '!=', -1)->orderBy('partner_id', 'ASC')->paginate(20);
		return $partneri;
	}

	public static function findPartnerImport($id, $column){
		return DB::table('dobavljac_cenovnik_kolone')->where('partner_id', $id)->pluck($column);
	}
	public static function podrzaniImporti($selected = null){
		$string = '<option value="" selected></option>';
		foreach(DB::table('podrzan_import')->where('dozvoljen',1)->orderBy('naziv','asc')->get() as $row){					
			if($selected == $row->vrednost_kolone){
				$string .= '<option value="'.$row->vrednost_kolone.'" selected>'.$row->naziv.'</option>';
			}else{
				$string .= '<option value="'.$row->vrednost_kolone.'">'.$row->naziv.'</option>';
			}
		}
		echo $string;
	}
	public static function podrzaniImportiUpload($selected = null){
		$string = '<option value="0" selected></option>';
		foreach(DB::table('podrzan_import')->where(array('dozvoljen'=>1,'file_upload'=>1))->orderBy('naziv','asc')->get() as $row){					
			if($selected == $row->vrednost_kolone){
				$string .= '<option value="'.$row->podrzan_import_id.'" selected>'.$row->naziv.' ('.$row->file_type.')</option>';
			}else{
				$string .= '<option value="'.$row->podrzan_import_id.'">'.$row->naziv.' ('.$row->file_type.')</option>';
			}
		}
		echo $string;
	}
	public static function searchKupci($word){
		$search = DB::table('web_kupac')->
			where('web_kupac_id', '!=', -1)->
			where('ime', 'ILIKE', "%$word%")->
			orWhere('prezime', 'ILIKE', "%$word%")->
			orWhere('naziv', 'ILIKE', "%$word%")->
			orWhere('email', 'ILIKE', "%$word%")->
			orWhere('pib', 'ILIKE', "%$word%")->
			orderBy('web_kupac_id', 'ASC')->
			paginate(20);
		return $search;
	}

	public static function getPartneriSearch($search=null,$partnerVrstaId=0,$partnerApi=null,$partnerB2BAccess=null,$limit=null,$page=1,$parent_id=null){

		$kupci_query=DB::table('partner')->distinct()->select('partner.*')->where('partner.partner_id', '!=', -1)->leftJoin('partner_je','partner.partner_id','=','partner_je.partner_id');
		if(!is_null($user = AdminSupport::getLoggedUser('KOMERCIJALISTA'))){
			$kupci_query = $kupci_query->rightJoin('partner_komercijalista','partner.partner_id','=','partner_komercijalista.partner_id')->where('partner_komercijalista.imenik_id',$user->imenik_id);
		}

		if(!is_null($parent_id)){
			$kupci_query =$kupci_query->where('partner.parent_id',$parent_id);
		}
		if(!is_null($search) && $search != ''){
			$search = urldecode($search);
			foreach(explode(' ',$search) as $word){
				$kupci_query =$kupci_query->where(function($query) use ($word){
					$query->orWhere('naziv','ILIKE','%'.$word.'%')->orWhere('adresa','ILIKE','%'.$word.'%')->orWhere('mail', 'ILIKE', '%'.$word.'%')->orWhere('mesto','ILIKE','%'.$word.'%');
				});
			}
		}
		if(is_numeric($partnerVrstaId) && $partnerVrstaId > 0){
			$kupci_query = $kupci_query->where('partner_vrsta_id',$partnerVrstaId);
		}
		if(!is_null($partnerApi) && in_array($partnerApi,array(0,1))){
			$kupci_query = $kupci_query->where('aktivan_api',$partnerApi);
		}
		if(is_bool($partnerB2BAccess)){
			if($partnerB2BAccess){
				$kupci_query = $kupci_query->whereNotNull('login')->whereNotNull('password')->where('login','<>','')->where('password','<>','');
			}else{
				$kupci_query = $kupci_query->where(function($query){
					return $query->orWhereNull('login')->orWhereNull('password')->orWhere('login','')->orWhere('password','');
				});
			}
		}

		if (!is_null($limit)) {
			return $kupci_query->orderBy('naziv', 'ASC')->limit($limit)->offset(($page-1)*$limit)->get();		
		}else{
			return $kupci_query->orderBy('naziv', 'ASC')->get();
		}
	}
	
	public static function kupacPodaci($web_kupac_id) {
		if($web_kupac_id){
			$web_kupac = DB::table('web_kupac')->where('web_kupac_id',$web_kupac_id)->first(); 
		}else{ 
			$web_kupac = (object) array('adresa'=>'','mesto'=>'','telefon'=>'','telefon_mobilni'=>'', 'email'=>'');
		}
		return $web_kupac;
	}

	public static function listMesto($mesto_id) {

		foreach(DB::table('mesto')->where('mesto_id','>',0)->orderBy('mesto','asc')->get() as $row_m) {

        	if(Input::old('mesto_id')) {
            	if(Input::old('mesto_id') == $row_m->mesto_id) {
            		echo "<option value='" . $row_m->mesto_id . "' selected>" . $row_m->mesto . " (" . $row_m->ptt . ")</option>";
            	} else {
					echo "<option value='" . $row_m->mesto_id . "'>" . $row_m->mesto . " (" . $row_m->ptt . ")</option>";
            	}
            } else {
            	if($mesto_id == $row_m->mesto_id) {
            		echo "<option value='" . $row_m->mesto_id . "' selected>" . $row_m->mesto . " (" . $row_m->ptt . ")</option>";
            	} else {
            		echo "<option value='" . $row_m->mesto_id . "'>" . $row_m->mesto . " (" . $row_m->ptt . ")</option>";
            	}
            }
		}
	}


}

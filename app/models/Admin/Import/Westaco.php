<?php
namespace Import;
use Import\Support;
use DB;
use File;
use PHPExcel; 
use PHPExcel_IOFactory;

class Westaco {

	public static function execute($dobavljac_id,$extension=null){
		if($extension==null){
			self::download();
			$products_file = "files/westaco/westaco.csv";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}

		if($continue){
			Support::initQueryExecute();
		
			$i = 0;
			$handle = fopen($products_file, "r");
		
			while (($data = fgetcsv($handle,10000,",")) !== FALSE) {
		
			$sifra = $data[3];
			$naziv = $data[4];
			$opis  = $data[5];
			$kolicina = $data[6];
			$kategorija = $data[8];
			$cena = $data[9];
		
			if(!empty($sifra) AND is_numeric($cena) AND $cena > 0) {
				$naziv = str_replace(array('Ć','Č','Š','Ž'),array('ć','č','š','ž'), $naziv);
				$kategorija = str_replace(array('Ć','Č','Š','Ž'),array('ć','č','š','ž'), $kategorija);
					$sPolja = '';
					$sVrednosti = '';					
					
					$sPolja .= "partner_id,";					$sVrednosti .= "" . $dobavljac_id . ",";
					$sPolja .= "sifra_kod_dobavljaca,";			$sVrednosti .= "'" . addslashes($sifra) . "',";					
					$sPolja .= "naziv,";						$sVrednosti .= "'" . pg_escape_string(Support::encodeTo1250(ucfirst(strtolower($kategorija)))) .' '.pg_escape_string(Support::encodeTo1250(ucfirst(strtolower($naziv)))) ."',";
					$sPolja .= "opis,";							$sVrednosti .= "'" . pg_escape_string(Support::encodeTo1250($opis)) . "',";
					$sPolja .= "grupa,";						$sVrednosti .= "'" . pg_escape_string(Support::encodeTo1250($kategorija)) . "',";
					$sPolja .= "kolicina,";						$sVrednosti .= "'" . number_format(intval($kolicina), 2, '.', '') . "',";
					$sPolja .= "cena_nc,";						$sVrednosti .= "" . number_format($cena, 2, '.', '') . ",";
					$sPolja .= "web_flag_karakteristike";		$sVrednosti .= " 0";
										
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");			
			}	
		}
			Support::queryExecute($dobavljac_id,array('i','u'),array(),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}
	

	public static function executeShort($dobavljac_id,$extension=null){

		if($extension==null){
			self::download();
			$products_file = "files/westaco/westaco.csv";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}

		if($continue){
			Support::initQueryExecute();
		
			$i = 0;
			$handle = fopen($products_file, "r");
		
			while (($data = fgetcsv($handle,10000,",")) !== FALSE) {
		
			$sifra = $data[3];
			$kolicina = $data[6];
			$cena = $data[9];
		
			if(!empty($sifra) AND is_numeric($cena) AND $cena > 0) {
				
					$sPolja = '';
					$sVrednosti = '';					
					
					$sPolja .= "partner_id,";					$sVrednosti .= "" . $dobavljac_id . ",";
					$sPolja .= "sifra_kod_dobavljaca,";			$sVrednosti .= "'" . addslashes($sifra) . "',";					
					$sPolja .= "kolicina,";						$sVrednosti .= "'" . number_format(intval($kolicina), 2, '.', '') . "',";
					$sPolja .= "cena_nc";						$sVrednosti .= "" . number_format($cena, 2, '.', '') . "";
										
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");			
			}	
		}

			//Support::queryShortExecute($dobavljac_id);
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}

	public static function lower($string){
		$firstletter = str_split($string)[0];
		$lower = mb_strtolower($string);
		return $firstletter.substr($lower,1);
	}
	public static function download() {
		$username = 'INFOGRAFGOTI.B2B';
		$password = 'RFV089';
		//$loginUrl = 'https://www.moglytoys.com/dilerkatalog/login/main_login.php';
		$file='files/westaco/westaco.csv';

        $userAgent = 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.2309.372 Safari/537.36';
        $cookieFile = 'cookie.txt';
        $loginFormUrl = 'https://www.merp-b2b.com/westaco/login';
        $loginActionUrl = 'https://www.merp-b2b.com/westaco/LoginServlet';

        $postValues = array(
            'user' => $username,
            'password' => $password,
            'lang' => 'sr'
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $loginActionUrl);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($postValues));
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        // curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($curl, CURLOPT_COOKIEJAR, $cookieFile);
        curl_setopt($curl, CURLOPT_USERAGENT, $userAgent);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_REFERER, $loginFormUrl);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        $response = curl_exec($curl);
        if(curl_errno($curl)){
            throw new Exception(curl_error($curl));
        }
        // All::dd(curl_getinfo($curl));
//echo $response; die;

        curl_setopt($curl, CURLOPT_URL, 'https://www.merp-b2b.com/westaco/getItemsAllCsv');
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($curl, CURLOPT_COOKIEJAR, $cookieFile);
        curl_setopt($curl, CURLOPT_USERAGENT, $userAgent);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_FILE, fopen($file, "w"));
        curl_exec($curl);
        curl_close($curl);

	}
}
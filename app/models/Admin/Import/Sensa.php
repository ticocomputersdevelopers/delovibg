<?php
namespace Import;
use Import\Support;
use DB;
use File;

class Sensa {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/sensa/sensa_xml/sensa.xml');
			$products_file = "files/sensa/sensa_xml/sensa.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}
			$artikals = simplexml_load_file($products_file);
	     	
			foreach ($artikals as $artikal){
				$id= $artikal->id;
				$naziv= $artikal->naziv;
				$proizvodjac = $artikal->proizvodjac;
				$kolicina = $artikal->kolicina;
				$cena_nc= $artikal->cena;
				$mpcena= $artikal->cena_mp;

				$sPolja = '';
				$sVrednosti = '';
				$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
				$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . $id . "',";
				$sPolja .= "naziv,";					$sVrednosti .= "'".addslashes(Support::encodeTo1250($proizvodjac))." " . addslashes(Support::encodeTo1250($naziv)) ." ( ". $id ." )"."',";
				$sPolja .= "proizvodjac,";				$sVrednosti .= "'" . addslashes(Support::encodeTo1250($proizvodjac)). "', ";
				$sPolja .= "kolicina,";		$sVrednosti .= "'". number_format(floatval($kolicina),2,'.','') . "',";
				$sPolja .= " cena_nc, ";				$sVrednosti .= " " . number_format(Support::replace_empty_numeric($cena_nc,1,$kurs,$valuta_id_nc),2, '.', ''). ", ";
				$sPolja .= "mpcena ";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($mpcena,1,$kurs,$valuta_id_nc),2, '.', ''). "";
				
		
				DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");	

			}

			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$extension=null){
		if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/sensa/sensa_xml/sensa.xml');
			$products_file = "files/sensa/sensa_xml/sensa.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}
			$artikals = simplexml_load_file($products_file);
	        
			foreach ($artikals as $artikal){
				$id= $artikal->id;
				$kolicina = $artikal->kolicina;
				$cena_nc= $artikal->cena;
				$mpcena= $artikal->cena_mp;

				$sPolja = '';
				$sVrednosti = '';
				$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
				$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . $id . "',";
				$sPolja .= "kolicina,";		$sVrednosti .= "'". number_format(floatval($kolicina),2,'.','') . "',";
				$sPolja .= " cena_nc,";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($cena_nc,1,$kurs,$valuta_id_nc),2, '.', ''). ", ";
				$sPolja .= "mpcena";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($mpcena,1,$kurs,$valuta_id_nc),2, '.', ''). "";

		
				DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

			}

			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}


}
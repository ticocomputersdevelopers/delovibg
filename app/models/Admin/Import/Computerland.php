<?php
namespace Import;
use Import\Support;
use DB;
use AdminOptions;
use All;
use File;
use DOMDocument;
use ZipArchive;

class Computerland {

	public static function execute($dobavljac_id,$extension=null){
		self::images($dobavljac_id,$extension=null);
		Support::initQueryExecute();

		if($extension==null){
			
			self::autoDownload();
		
			$file_name = Support::file_name("files/computerland/computerland_xml/");
			$products = simplexml_load_file("files/computerland/computerland_xml/".$file_name);
		}else{
			$file = AdminOptions::base_url().'files/import.'.$extension;
			$products = simplexml_load_file($file);	
		}
			
		//podaci o proizvodima
		foreach ($products as $product) {
			//All::dd($product->xpath('description'));die;
		
			$naziv= (string)$product->name;
			$sifra =(string)$product->erp_code;
			$proizvodjac =(object)$product->manufacturer;
			$manufacture=(string)$proizvodjac->name;
			$model=(string)$product->oem;
			//var_dump($model);die;
			$grupa = $product->xpath('item_group/parent_group');
			foreach ($grupa as $gr) {
			
			$group=(string)$gr->name;
			
			}

			$podgrupa = $product->xpath('item_group');
			foreach ($podgrupa as $podg) {
				$podgroup=(string)$podg->name;
			}
			$opis = (string)$product->description;
			//var_dump($opis);die;
			
			$insert_arr['partner_id'] = $dobavljac_id;
			$insert_arr['sifra_kod_dobavljaca'] =Support::encodeTo1250($sifra);
			$insert_arr['proizvodjac'] = Support::encodeTo1250($manufacture);
			if(!empty($model)){
			$insert_arr['naziv'] = Support::encodeTo1250($naziv." ( ".$model." ) ");
			}else{
			$insert_arr['naziv'] = Support::encodeTo1250($naziv);
			}
			$insert_arr['grupa'] = Support::encodeTo1250($group);
			$insert_arr['podgrupa'] = Support::encodeTo1250($podgroup);
			$insert_arr['kolicina'] = (string)$product->attributes()->on_stock == "true" ? "1" : "0";		
			$insert_arr['cena_nc'] = (string)$product->attributes()->price_with_discounts;
			$insert_arr['mpcena'] =(string)$product->attributes()->retail_price;
			$insert_arr['pmp_cena'] =(string)$product->attributes()->retail_price;			
			$insert_arr['flag_opis_postoji'] = $opis == "" ? "0" : "1";
			$insert_arr['opis'] = Support::encodeTo1250($opis);
			$insert_arr['web_flag_karakteristike'] = "0";
			//$insert_arr['flag_slika_postoji'] = $product->image == "" || $product->image == "https://b2b.computerland.rs/b2b/res/basic/images/no_image.png" ? "0" : "1";

			DB::table('dobavljac_cenovnik_temp')->insert($insert_arr);

			// if($insert_arr['flag_slika_postoji'] == '1'){
			// 	$sPolja = '';
			// 	$sVrednosti = '';
			// 	$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
			// 	$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "" . Support::quotedStr($product->attributes()->code) . ",";
			// 	$sPolja .= "akcija,";		            $sVrednosti .= "" . 1 . ",";
			// 	$sPolja .= "putanja";				    $sVrednosti .= "'" . $product->image. "'";
				
					
			// 	DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
			// }
		}

		Support::queryExecute($dobavljac_id,array('i','u'),array(),array('i','u'));
		DB::statement("UPDATE dobavljac_cenovnik a set flag_slika_postoji=1 WHERE EXISTS(SELECT sifra_kod_dobavljaca FROM dobavljac_cenovnik_slike f
			WHERE f.sifra_kod_dobavljaca = a.sifra_kod_dobavljaca AND a.partner_id=20)");

		//Brisemo fajl
		$file_name = Support::file_name("files/computerland/computerland_xml/");
		if($extension!=null){
			File::delete('files/import.'.$extension);
		}
		elseif($file_name!==false){
			File::delete("files/computerland/computerland_xml/".$file_name);
		}

	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){
		Support::initQueryExecute();

		if($extension==null){
			
			self::autoDownload();
		
			$file_name = Support::file_name("files/computerland/computerland_xml/");
			$products = simplexml_load_file("files/computerland/computerland_xml/".$file_name);
		}else{
			$file = AdminOptions::base_url().'files/import.'.$extension;
			$products = simplexml_load_file($file);	
		}
			
		//podaci o proizvodima
		foreach ($products as $product) {
			//All::dd($product->xpath('description'));die;
		
			$naziv= (string)$product->name;
			$sifra =(string)$product->erp_code;
						
			$insert_arr['partner_id'] = $dobavljac_id;
			$insert_arr['sifra_kod_dobavljaca'] =Support::encodeTo1250($sifra);
			$insert_arr['kolicina'] = (string)$product->attributes()->on_stock == "true" ? "1" : "0";		
			$insert_arr['cena_nc'] = (string)$product->attributes()->price_with_discounts;
			$insert_arr['mpcena'] =(string)$product->attributes()->retail_price;
			$insert_arr['pmp_cena'] =(string)$product->attributes()->retail_price;			


			DB::table('dobavljac_cenovnik_temp')->insert($insert_arr);
		}

		//Support::queryShortExecute($dobavljac_id);

		//Brisemo fajl
		$file_name = Support::file_name("files/computerland/computerland_xml/");
		if($extension!=null){
			File::delete('files/import.'.$extension);
		}
		elseif($file_name!==false){
			File::delete("files/computerland/computerland_xml/".$file_name);
		}

	}

	public static function autoDownload(){

		$url = "http://b2b.computerland.rs/StockWebservice/StockWebservice?wsdl";
	    $soap_request = '<?xml version="1.0" encoding="utf-8"?>
	    				   <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://webservice.b2b.navigator.rs/">
	                       <soapenv:Header/>
	                       <soapenv:Body>
	                          <web:getAllItems>
									<arg0>599a2c40-b7ec-435c-9fd5-2beee42b9f9a</arg0>
	                             	<arg1>4</arg1>
	                             <arg2>?</arg2>
	                          </web:getAllItems>
	                       </soapenv:Body>
	                    </soapenv:Envelope>';

	    $header = array(
	        "Content-type: text/xml;charset=\"utf-8\"",
	        "Accept: text/xml",
	        "Cache-Control: no-cache",
	        "Pragma: no-cache",
	        "Content-length: ".strlen($soap_request),
	    );

	    $soap_do = curl_init();
	    curl_setopt($soap_do, CURLOPT_URL,            $url);
	    curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true );
	    curl_setopt($soap_do, CURLOPT_POST,           true );
	    curl_setopt($soap_do, CURLOPT_POSTFIELDS,     $soap_request);
	    curl_setopt($soap_do, CURLOPT_HTTPHEADER,     $header);
	    $result = curl_exec($soap_do);

	       $result = str_replace(array('<return>','</return>','</soap:Envelope>','<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">',"<soap:Body>","</soap:Body>","</ns2:getAllItemsResponse>",'<ns2:getAllItemsResponse xmlns:ns2="http://webservice.b2b.navigator.rs/">'),array("<items>","</items>","","","","","",""),$result);


    if($result === false) {
        $err = 'Curl error: ' . curl_error($soap_do);
        curl_close($soap_do);
        print $err;
    } else {
        $myfile = fopen("files/computerland/computerland_xml/computerland.xml", "w");
        fwrite($myfile, $result);
        fclose($myfile);
    } 
		
	}

	// public static function getComputerlandImageContent($image,$destination){
	// 	$arrContextOptions=array(
	// 	    "ssl"=>array(
	// 	        "verify_peer"=>false,
	// 	        "verify_peer_name"=>false,
	// 	    ),
	// 	); 
	// 	$content = file_get_contents($image,false,stream_context_create($arrContextOptions));
	// 	// $fp = fopen(str_replace('.jpg','.png',$destination), "w");
	// 	$fp = fopen($destination, "w");
	// 	fwrite($fp, $content); 
	// 	fclose($fp);
	// }
	public static function images($dobavljac_id,$extension=null){	
		

		if($extension==null){
			if(Support::autoLink($dobavljac_id) != null && Support::serviceUsername($dobavljac_id) != null && Support::servicePassword($dobavljac_id) != null){
				self::autoDownload_images(Support::autoLink($dobavljac_id),Support::serviceUsername($dobavljac_id),Support::servicePassword($dobavljac_id));
			}
			$file_name = Support::file_name("files/computerland/computerland_images/");
			if($file_name!==false){
				$products = simplexml_load_file("files/computerland/computerland_images/".$file_name);
			}else{
				$products = array();
			}
		}else{
			$file = AdminOptions::base_url().'files/import.'.$extension;
			$products = simplexml_load_file($file);			
		}

		//podaci o proizvodima
		foreach ($products as $product) {
			$insert_arr = array();
			$insert_arr['partner_id'] = $dobavljac_id;
			$insert_arr['sifra_kod_dobavljaca'] = $product->attributes()->code;
			$insert_arr['flag_slika_postoji'] = $product->image == "" || $product->image == "https://b2b.computerland.rs/b2b/res/basic/images/no_image.png" ? "0" : "1";
			$insert_arr['flag_opis_postoji'] = $product->description == "" ? "0" : "1";

			DB::table('dobavljac_cenovnik_temp')->insert($insert_arr);

			if($insert_arr['flag_slika_postoji'] == '1'){
				$sPolja = '';
				$sVrednosti = '';
				$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
				$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "" . Support::quotedStr($product->attributes()->code) . ",";
				$sPolja .= "akcija,";		            $sVrednosti .= "" . 1 . ",";
				$sPolja .= "putanja";				    $sVrednosti .= "'" . $product->image. "'";
				
					
				DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
			}
		}

		Support::queryExecute($dobavljac_id,array(),array('i'),array());

		//Brisemo fajl
		$file_name = Support::file_name("files/computerland/computerland_images/");
		if($extension!=null){
			File::delete('files/import.'.$extension);
		}
		elseif($file_name!==false){
			File::delete("files/computerland/computerland_images/".$file_name);
		}

	}
	public static function autoDownload_images($links,$username,$password){

		$links = explode('---',$links);
		$postinfo = "j_username=".$username."&j_password=".$password."";
		$cookie_file_path = 'cookie.txt';

		$myfile = fopen("files/computerland/computerland_images/computerland.zip", "w");

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_NOBODY, false);
		curl_setopt($ch, CURLOPT_URL, $links[0]);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

		curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file_path);
		curl_setopt($ch, CURLOPT_COOKIE, "cookiename=0");
		curl_setopt($ch, CURLOPT_USERAGENT,
		    "Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.7.12) Gecko/20050915 Firefox/1.0.7");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_REFERER, $_SERVER['REQUEST_URI']);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);

		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postinfo);
		curl_exec($ch);

		//dovlacenje xml-a
		curl_setopt($ch, CURLOPT_URL, $links[1]);
		curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($ch, CURLOPT_FILE, $myfile);
		curl_exec ($ch);
		curl_close($ch);

		$zip = new ZipArchive;
		if ($zip->open('files/computerland/computerland_images/computerland.zip') === TRUE) {
		    $zip->extractTo('files/computerland/computerland_images');
		    $zip->close();
		    File::delete('files/computerland/computerland_images/computerland.zip');
		}
	}

}

?>
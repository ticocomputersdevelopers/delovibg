<?php
namespace Import;
use Import\Support;
use DB;
use File;

class AsbisOld {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){
		if($extension==null){
			$products_file = "files/asbisold/asbisold_xml/asbisold.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

		    $products = simplexml_load_file($products_file);
		    $products_2 = $products->xpath('PRICES/PRICE');
		    foreach ($products_2 as $product):
		        $sifra = $product->WIC;
		        $naziv = $product->DESCRIPTION;
		        $grupa = $product->GROUP_NAME;
		        $proizvodjac = $product->VENDOR_NAME;
		        $kolicina = $product->AVAIL!="0"?"1":"0";
		        $cena_nc = $product->MY_PRICE;
		        $barkod = $product->EAN;
		        $slika = $product->SMALL_IMAGE;

		        if(!empty($sifra)){

		            $sPolja = '';
		            $sVrednosti = '';
		        
		            $sPolja .= "partner_id,";               $sVrednosti .= "" . $dobavljac_id . ",";
		            $sPolja .= "sifra_kod_dobavljaca,";     $sVrednosti .= "'" . addslashes(Support::encodeTo1250($sifra)) . "',";
		            $sPolja .= "naziv,";                    $sVrednosti .= "'" . addslashes(Support::encodeTo1250($naziv) . " ( " . Support::encodeTo1250($sifra) . " )") . "',";
		            $sPolja .= "grupa,";                    $sVrednosti .= "'" . addslashes(Support::encodeTo1250($grupa)) . "',";
		            $sPolja .= "proizvodjac,";                    $sVrednosti .= "'" . addslashes(Support::encodeTo1250($proizvodjac)) . "',";
		            $sPolja .= "barkod,";                    $sVrednosti .= "'" . addslashes(Support::encodeTo1250($barkod)) . "',";
		            $sPolja .= "kolicina,";                 $sVrednosti .= "" . floatval($kolicina) . ",";
		            $sPolja .= "cena_nc";                   $sVrednosti .= "" . number_format(floatval(Support::replace_empty_numeric($cena_nc,1,$kurs,$valuta_id_nc)), 2, '.', '') . "";
		            
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
		            DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",'".addslashes(Support::encodeTo1250($sifra))."','".$slika."',1)");    
		        }

		    endforeach;

			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			$products_file = "files/asbisold/asbisold_xml/asbisold.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}
			
		    $products = simplexml_load_file($products_file);
		    $products_2 = $products->xpath('PRICES/PRICE');
		    foreach ($products_2 as $product):
		        $sifra = $product->WIC;
		        $kolicina = $product->AVAIL!="0"?"1":"0";
		        $cena_nc = $product->MY_PRICE;

		        if(!empty($sifra)){

		            $sPolja = '';
		            $sVrednosti = '';
		        
		            $sPolja .= "partner_id,";               $sVrednosti .= "" . $dobavljac_id . ",";
		            $sPolja .= "sifra_kod_dobavljaca,";     $sVrednosti .= "'" . addslashes(Support::encodeTo1250($sifra)) . "',";
		            $sPolja .= "kolicina,";                 $sVrednosti .= "" . floatval($kolicina) . ",";
		            $sPolja .= "cena_nc";                   $sVrednosti .= "" . number_format(floatval(Support::replace_empty_numeric($cena_nc,1,$kurs,$valuta_id_nc)), 2, '.', '') . "";
		            
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");   
		        }

		    endforeach;

			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}

}
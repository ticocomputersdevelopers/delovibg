<?php
namespace Import;
use Import\Support;
use DB;
use AdminOptions;
use File;

class PCCentar {

	public static function execute($dobavljac_id,$extension=null){
		Support::initQueryExecute();

		if($extension==null){
			if(Support::autoLink($dobavljac_id) != null){
				Support::autoDownload(Support::autoLink($dobavljac_id),'files/pccentar/pccentar_xml/pccentar.xml');
			}
			$file_name = Support::file_name("files/pccentar/pccentar_xml/");
			if($file_name!==false){
				$products = simplexml_load_file("files/pccentar/pccentar_xml/".$file_name);
			}else{
				$products = array();
			}
		}else{
			$file = AdminOptions::base_url().'files/import.'.$extension;
			$products = simplexml_load_file($file);			
		}

			foreach ($products as $product) {
			
			//karakteristike
			$specifikacija=$product->Specifikacija;			
			$spec=explode("§", $specifikacija);

			foreach ($spec as $el) {
				$karakteristike=explode(": ",Support::encodeTo1250($el));
				
				if(isset($karakteristike[1]) && trim($karakteristike[1]) != '--' && trim($karakteristike[1]) != '-'){
					$sPolja = '';
					$sVrednosti = '';
					$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
					$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250( $product->Sifra)) . ",";	
					$sPolja .= "karakteristika_naziv,";		$sVrednosti .= "" . Support::quotedStr(trim($karakteristike[0])) . ",";
					$sPolja .= "karakteristika_vrednost";	$sVrednosti .= "'" . trim($karakteristike[1]) . "'";
						//var_dump($karakteristike[1]);die;
					DB::statement("INSERT INTO dobavljac_cenovnik_karakteristike_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
				}
			}

			
			//slike
						if ($product->Slika1!='' && isset($product->Slika1)) {
				$sPolja = '';
				$sVrednosti = '';
				$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
				$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250( $product->Sifra)) . ",";
				$sPolja .= "akcija,";					$sVrednosti .= "" . 1 . ",";
				$sPolja .= "putanja";				    $sVrednosti .= "'" . $product->Slika1 . "'";
					
				DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
				
			}

			if ($product->Slika2!='' && isset($product->Slika2)) {
				$sPolja = '';
				$sVrednosti = '';
				$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
				$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250( $product->Sifra)) . ",";
				$sPolja .= "akcija,";					$sVrednosti .= "" . 0 . ",";
				$sPolja .= "putanja";				    $sVrednosti .= "'" . $product->Slika2 . "'";
					
				DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
				
			}

			if ($product->Slika3!='' && isset($product->Slika3)) {
				$sPolja = '';
				$sVrednosti = '';
				$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
				$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250( $product->Sifra)) . ",";
				$sPolja .= "akcija,";					$sVrednosti .= "" . 0 . ",";
				$sPolja .= "putanja";				    $sVrednosti .= "'" . $product->Slika3 . "'";
					
				DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
				
			}
			//var_dump($product->Sifra);die;
	
			$pmp_cena= $product->{'Preporucena-cijena'};
			$ncena = $product->{'Nabavna-cijena'};
			
			//proizvodi
			$sPolja = '';
			$sVrednosti = '';
			$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
			$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250( $product->Sifra)) . ",";
			$sPolja .= "naziv,";					$sVrednosti .= "" . Support::quotedStrPC(Support::encodeTo1250($product->Naziv)) . ",";
			$sPolja .= "grupa,";					$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($product->Kategorija1)) . ",";			
		
			$sPolja .= "flag_opis_postoji,";		$sVrednosti .= "" . $product->Opis == "" ? "0," : "1,";
			$sPolja .= "opis,";						$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250( $product->Opis)) . ",";
			$sPolja .= "web_flag_karakteristike,";	$sVrednosti .= " 2,";

			$sPolja .= "kolicina,";					$sVrednosti .= "" . $product->Kolicina . ",";
			
			$sPolja .= "cena_nc,";					$sVrednosti .= "" . number_format(Support::replace_empty_numeric($ncena,1,1,2),2, '.', '') . ",";	
			
			$sPolja .= "pmp_cena,";					$sVrednosti .= "" . number_format(Support::replace_empty_numeric($pmp_cena,1,1,2),2, '.', '') . ",";	

			$sPolja .= "pdv,";						$sVrednosti .= "" . str_replace(',', '.', $product->Porez) . ",";

			$sPolja .= "flag_slika_postoji";		$sVrednosti .= "" . $product->Slika1 == "" ? "0" : "1";
							
			DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

		}


		Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array('i','u'));
		
		//Brisemo fajl
		$file_name = Support::file_name("files/pccentar/pccentar_xml/");
		if($extension!=null){
			File::delete('files/import.'.$extension);
		}
		elseif($file_name!==false){
			File::delete("files/pccentar/pccentar_xml/".$file_name);
		}
	
	}

	public static function executeShort($dobavljac_id,$extension=null){

		Support::initQueryExecute();

		if($extension==null){
			if(Support::autoLink($dobavljac_id) != null){
				Support::autoDownload(Support::autoLink($dobavljac_id),'files/pccentar/pccentar_xml/ewe.xml');
			}
			$file_name = Support::file_name("files/pccentar/pccentar_xml/");
			if($file_name!==false){
				$products = simplexml_load_file("files/pccentar/pccentar_xml/".$file_name);
			}else{
				$products = array();
			}
		}else{
			$file = AdminOptions::base_url().'files/import.'.$extension;
			$products = simplexml_load_file($file);			
		}

		foreach ($products as $product) {
			$pmp_cena= $product->{'PreporucenaCena'};
			$ncena = $product->{'NabavnaCena'};

			$sPolja = '';
			$sVrednosti = '';
			$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
			$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250( $product->Sifra)) . ",";
			$sPolja .= "kolicina,";					$sVrednosti .= "" . $product->Kolicina . ",";	
			$sPolja .= "cena_nc,";					$sVrednosti .= "" . str_replace(',', '.', $ncena) . ",";
			$sPolja .= "pmp_cena";					$sVrednosti .= "" . number_format(Support::replace_empty_numeric($pmp_cena,1,1,2),2, '.', '') . "";
							
			DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

		}
		
		//Support::queryShortExecute($dobavljac_id);
		
		//Brisemo fajl
		$file_name = Support::file_name("files/pccentar/pccentar_xml/");
		if($extension!=null){
			File::delete('files/import.'.$extension);
		}
		elseif($file_name!==false){
			File::delete("files/pccentar/pccentar_xml/".$file_name);
		}
	
	}

}
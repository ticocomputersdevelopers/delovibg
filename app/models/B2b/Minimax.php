<?php

class Minimax {

public static function create_order($orderId){
        $order = DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id',$orderId)->first();

    	$orderNumber=$order->broj_dokumenta;
    	$cartItems=DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_id',$orderId)->get();
        $nacin_isporuke = DB::table('web_nacin_isporuke')->where('web_nacin_isporuke_id',$order->web_nacin_isporuke_id)->pluck('naziv');
        $nacin_placanja = DB::table('web_nacin_placanja')->where('web_nacin_placanja_id',$order->web_nacin_placanja_id)->pluck('naziv');
    	$napomena='Način isporuke: '.$nacin_isporuke.'. Način plaćanja: '.$nacin_placanja.'. Napomena: '.$order->napomena;

        $partner = DB::table('partner')->where('partner_id',$order->partner_id)->first();
        
        $data = array(
        	'OrderId'=>null,
	       	'ReceivedIssued'=>'P',
	       	'Year'=>date('Y'),
	       	'Number'=>null,
	       	'Date'=> date("Y-m-d\TH:i:sP"),
	       	'Customer'=>array(
	       		'ID'=>$partner->sifra,
	       		'Name'=>null,
	       		'ResourceUrl'=>null
	       	),
	       	'CustomerName'=> $partner->naziv,
	  		'CustomerAddress'=>  $partner->adresa,
	  		'CustomerPostalCode'=> null,
	  		'CustomerCity'=> $partner->mesto,
	  		'CustomerCountry'=>null,
	  		'CustomerCountryName'=>null,
	  		'Analytic'=>null,
	  		'DueDate'=>null,
	  		'Reference'=>null,
	  		'Currency'=>array(
	  			'ID'=>2,
	  			'Name'=>null,
	       		'ResourceUrl'=>null
	  		),
	  		'Notes'=>$napomena,
	  		'Document'=>null,
	  		'DateConfirmed'=>null,
	  		'DateCompleted'=>null,
	  		'DateCanceled'=>null,
	       	'Status'=>null,
	       	'DescriptionAbove'=>null,
	       	'DescriptionBelow'=>null,

		    'OrderRows'=>array(),
		    	
	        'RecordDtModified'=>null,
	        'RowVersion'=>null,
	        '_links'=>0.00
        	);
        $success = false;
        $order_id = 0;
        foreach($cartItems as $row){
            $roba = DB::table('roba')->where('roba_id',$row->roba_id)->first();
            if(isset($roba->id_is) && $roba->id_is != null){
                $data['OrderRows'][]=[
		        	'OrderRowId'=>null,
			    	'Order'=>null,
			    	'Item'=>array(
			    		'ID'=>$roba->id_is,
			    		'Name'=>null,
		       			'ResourceUrl'=>null
			    	),
			        'ItemName'=> $roba->naziv,
		            'ItemCode'=> null,
		            'Description'=>null,
		            'Quantity'=>$row->kolicina,
		            'Price'=>$row->jm_cena,
		            'UnitOfMeasurement'=>null,
		            'RecordDtModified'=>null,
		            'RowVersion'=>null,
		            '_links'=>0.00
		        ];
            }
        }
  		

	$params = array(
        'client_id'=> 'Aquatec',
        'client_secret'=> 'Aquatec',
        'grant_type'=> 'password',
        'username'=> 'racunovodstvoaquatec@gmail.com',
        'password'=> 'aqu123',
        'scope' => 'minimax.rs'
    );
    $request = array(
        'http' => array(
            'method'=> 'POST',
            'header'=> array(
                'Content-type: application/x-www-form-urlencoded'
            ),
            'content'=> http_build_query($params),
            'timeout'=> 10
        )
    );
    if(!$response = file_get_contents('https://moj.minimax.rs/RS/aut/oauth20/token', false, stream_context_create($request))) {
        die('auth error');
    }
    $token = json_decode($response);
    // All::dd($token);
   // Session::put('minimaxtoken',$token->access_token);
   
 //die;
    $curl = curl_init();
   		curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://moj.minimax.rs/RS/api/api/orgs/1877/orders',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_CUSTOMREQUEST => "POST",
        // CURLOPT_USERPWD => $this->accessToken,
        CURLOPT_POSTFIELDS => http_build_query($data),
        CURLOPT_HTTPHEADER => array(
            'Content-type: application/x-www-form-urlencoded',
            'Authorization: Bearer ' . $token->access_token
        ),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);

    if($err) {
        echo $err;
    }else{
    	$success = true;
    } 
	return (object) array('success'=>$success, 'OrderId'=>$order_id);

}
}
<?php

class PageLang extends Eloquent
{
    protected $table = 'web_b2c_seo_jezik';
    
    public $timestamps = false;

	protected $hidden = array(
		'web_b2c_seo_id',
        'jezik_id',
        'naziv',
        'sadrzaj'
      	);

	public function getPageIdAttribute()
	{
	    return isset($this->attributes['web_b2c_seo_id']) ? $this->attributes['web_b2c_seo_id'] : null;
	}
    public function getLangIdAttribute()
    {
        return isset($this->attributes['jezik_id']) ? $this->attributes['jezik_id'] : null;
    }
    public function getNameAttribute()
    {
        return isset($this->attributes['naziv']) ? $this->attributes['naziv'] : null;
    }
    public function getContentAttribute()
    {
        return isset($this->attributes['sadrzaj']) ? $this->attributes['sadrzaj'] : null;
    }

	protected $appends = array(
    	'page_id',
        'lang_id',
        'name',
        'content'
    	);

    public function page(){
        return $this->belongsTo('Page','web_b2c_seo_id');
    }

    public function lang(){
        return $this->belongsTo('Languages','jezik_id');
    }
}
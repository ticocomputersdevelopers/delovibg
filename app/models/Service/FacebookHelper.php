<?php
namespace Service;
use Config;
use Options;
use Facebook;

class FacebookHelper {

    public static function auth($request)
    {
		$appId = Config::get('public_auth.fb_client_id');
		$appSecret = Config::get('public_auth.fb_client_secret');
		$redirectURL = Options::base_url().'fb-authorization';

		$fb = new Facebook\Facebook([
		  'app_id' => $appId,
		  'app_secret' => $appSecret,
		  'default_graph_version' => 'v2.10',
		  ]);

		$helper = $fb->getRedirectLoginHelper();

		if (isset($request['state'])) {
		    $helper->getPersistentDataHandler()->set('state', $request['state']);
		}

		$error_message = null;
		try {
			$accessToken = $helper->getAccessToken();
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
			$error_message = $e->getMessage();
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
			$error_message = $e->getMessage();
		}

	    if(!is_null($error_message)){
	    	return (object) array('action'=>'error', 'result'=>$error_message);
	    }

	    if(is_null($accessToken)){
			$permissions = ['email'];
			$loginUrl = $helper->getLoginUrl($redirectURL,$permissions);
			return (object) array('action'=>'link', 'result'=>$loginUrl);
	    }

	    try {
			$response = $fb->get('/me?fields=id,name,email', $accessToken);
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
			$error_message = $e->getMessage();
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
			$error_message = $e->getMessage();
		}

	    if(is_null($error_message)){
	    	$fbUser = $response->getGraphUser();
	    	return (object) array('action'=>'data', 'result'=> (object) array('name'=>$fbUser->getField('name'),'email'=>$fbUser->getField('email')));
	    }else{
	    	return (object) array('action'=>'error', 'result'=>$error_message);
	    }

		// echo $data->getName();


    }


}
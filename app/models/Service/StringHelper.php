<?php 
class StringHelper {

    public static function regex(){
        return '/(^[A-Za-z0-9\ \!\%\&\(\)\=\*\/\,\.\+\-\_\@\?\:\;\<\>\'\č\ć\ž\š\đ\Č\Ć\Ž\Š\Đ\"]+$)+/';
    }
    public static function regexDigitsBlank(){
        return '/^[\s\d]*$/';
    }
    public static function regexTelephone(){
        return '/(^[0-9\ \+\"]+$)+/';
    }
    public static function regexOneDigit(){
        return '/(?<!\d)\d{1}(?!\d)/';
    }
    public static function random_string($length) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $chars_string = substr(str_shuffle($chars),0,$length-1);

        $num = "0123456789";
        $num_string = substr(str_shuffle($num),0,1);

        return $chars_string . $num_string;
    }

    public static function create_string_of_number($number,$string_length) {
        if($string_length > 20){
            return false;
        }

        $result = '';
        $number_length = strlen(strval($number));
        if($number_length < $string_length){
            $diff_length = $string_length - $number_length;
            foreach(range(0,$diff_length-1) as $time){
                $result .= '0';
            }
            $result = $result.strval($number);
        }else if($number_length > $string_length){
            $diff_length = $number_length - $string_length;
            $result = substr(strval($number), $diff_length, $number_length-1);
        }else{
            $result = strval($number);
        }

        return $result;
    }

    public static function crete_payment_invoice($prefix,$payment_id){
        // return $prefix.self::create_string_of_number($payment_id,5).strval(time());
        return $prefix.self::create_string_of_number($payment_id,7);
    }

    public static function custom_encrypt($string){
    	return base64_encode($string);
    }

    public static function custom_decrypt($string){
    	return base64_decode($string);
    }

    public static function create_slug($str){
        $table = array(
                'Š'=>'S', 'š'=>'s', 'Đ'=>'Dj', 'đ'=>'dj', 'Ž'=>'Z', 'ž'=>'z', 'Č'=>'C', 'č'=>'c', 'Ć'=>'C', 'ć'=>'c',
                'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
                'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O',
                'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss',
                'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e',
                'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o',
                'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b',
                'ÿ'=>'y', 'Ŕ'=>'R', 'ŕ'=>'r', '/' => '-', ' ' => '-'
        );
        $string = strtolower(strtr($str, $table));
        return strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $string)));
    }

    public static function make_keywords($string){
        return str_replace(array(' ','  ','   '),array(', ',', ',', '),trim(preg_replace('/[^a-z-šđčćžć ]+/', '', strtolower(str_replace(array('Š','Đ','Č','Ć','Ž'),array('š','đ','č','ć','ž'), $string)))));
    }

    public static function short_new($new_content){
        // $first_position = strpos($new_content, '<p>') + 3;
        // $second_position = strpos($new_content, '</p>');
        // $length = $second_position - $first_position;
        // $content = strip_tags(substr($new_content,$first_position,$length));
        $content = strip_tags($new_content);

        $sentences = explode('.', $content);
        $export_content = '';
        $snt_length = 0;
        foreach ($sentences as $sentence) {
            $snt_length += strlen($sentence);
            if($snt_length < 300){
                $export_content .= $sentence.'.';
            }else{
                break;
            }
        }
        return $export_content.'..';
    }

    public static function file_extension($path){
        $path_arr = explode('/',$path);
        $file_name = $path_arr[count($path_arr)-1];
        $file_name_arr = explode('.',$file_name);
        return $file_name_arr[count($file_name_arr)-1];
    }
}
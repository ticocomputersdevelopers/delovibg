<?php
namespace Service;
use Config;
use Options;
use Google_Client;
use Google_Service_Oauth2;
use Session;

class GoogleHelper {

    public static function o2auth($request)
    {
		$clientId = Config::get('public_auth.google_client_id');
		$clientSecret = Config::get('public_auth.google_client_secret');
		$redirectURL = Options::base_url().'/google-authorization';
	 
		$client = new Google_Client();
		$client->setApplicationName('Registration to '.Config::get('public_auth.open_domain'));
		$client->setClientId($clientId);
		$client->setClientSecret($clientSecret);
		$client->setScopes(array(
			"https://www.googleapis.com/auth/plus.login",
			"https://www.googleapis.com/auth/userinfo.email",
			"https://www.googleapis.com/auth/userinfo.profile",
			"https://www.googleapis.com/auth/plus.me"
		));
		$client->setRedirectUri($redirectURL);
	 
		$service = new Google_Service_Oauth2($client);

		if (isset($request['logout'])) {
			Session::forget('google_token');
		}

	    if(isset($request['code'])){
	    	$client->authenticate($request['code']);
	    	Session::put('google_token',$client->getAccessToken());
	    	return (object) array('action'=>'redirect', 'result'=>filter_var($redirectURL, FILTER_SANITIZE_URL));
	    }

		if (Session::has('google_token')) {
			$client->setAccessToken(Session::get('google_token'));
			if ($client->isAccessTokenExpired()) {
				Session::forget('google_token');
			}
		}

	    if ($client->getAccessToken()) {
	    	$userProfile = $service->userinfo->get();
			return (object) array('action'=>'data', 'result'=>$userProfile);
	    }else{
			$authUrl = $client->createAuthUrl();
			return (object) array('action'=>'link', 'result'=>$authUrl);
	    }
    }


}
<?php 

class Options {
	
	public static function base_url(){
		$suffix = '';
		if(DB::table('jezik')->where('aktivan',1)->count() > 1){
			$suffix = Language::lang().'/';
		}
		return DB::table('options')->where('options_id',1316)->pluck('str_data').$suffix;
	}
	
	public static function domain(){
		return DB::table('options')->where('options_id',1316)->pluck('str_data');
	}
	
	public static function company_name(){
		return Language::trans_chars(DB::table('preduzece')->pluck('naziv'));
	}
	public static function company_logo(){
		$logo = DB::table('preduzece')->pluck('logo');
		if(!is_null($logo) && $logo != ''){
			return $logo;
		}
		return 'images/No-image-available.jpg';
	}

	public static function seotitle($roba_id){
		return DB::table('roba')->where('roba_id',$roba_id)->pluck('seo');	
		}
				
	public static function company_adress(){
		return Language::trans_chars(DB::table('preduzece')->pluck('adresa'));
	}
	public static function company_phone(){
		return DB::table('preduzece')->pluck('telefon');
	}
	public static function company_fax(){
		return DB::table('preduzece')->pluck('fax');
	}
	public static function company_city(){
		return Language::trans_chars(DB::table('preduzece')->pluck('mesto'));
	}
	public static function company_ziro(){
		return DB::table('preduzece')->pluck('ziro');
	}
	public static function company_maticni(){
		return DB::table('preduzece')->pluck('matbr_registra');
	}
	public static function company_pib(){
		return DB::table('preduzece')->pluck('pib');
	}
	public static function company_email(){
		return DB::table('preduzece')->pluck('email');
	}
	public static function company_delatnost_sifra(){
		return DB::table('preduzece')->pluck('delatnost_sifra');
	}
    
   	public static function company_map(){
		return DB::table('preduzece')->pluck('mapa');
	}

	public static function user_cnfirm_registration(){
			$cn_query=DB::table('web_options')->where('web_options_id',100)->get();
			foreach($cn_query as $row){
				return $row->int_data;
			}
	}
	public static function user_registration(){
				$cn_query=DB::table('web_options')->where('web_options_id',0)->get();
			foreach($cn_query as $row){
				return $row->int_data;
			}
	}
    public static function neregistrovani_korisnici(){
				$cn_query=DB::table('web_options')->where('web_options_id',1)->get();
			foreach($cn_query as $row){
				return $row->int_data;
			}
	}
	public static function vodjenje_lagera(){
				$cn_query=DB::table('web_options')->where('web_options_id',103)->get();
			foreach($cn_query as $row){
				return $row->int_data;
			}
	}
    	public static function newsletter(){
				$cn_query=DB::table('web_options')->where('web_options_id',104)->get();
			foreach($cn_query as $row){
				return $row->int_data;
			}
	}
    
    	public static function product_view(){
				$cn_query=DB::table('web_options')->where('web_options_id',106)->get();
			foreach($cn_query as $row){
				return $row->int_data;
			}
	}
    	public static function product_number(){
				$cn_query=DB::table('web_options')->where('web_options_id',107)->get();
			foreach($cn_query as $row){
				return $row->int_data;
			}
	}
    
    	public static function product_sort(){
				$cn_query=DB::table('web_options')->where('web_options_id',108)->get();
			foreach($cn_query as $row){
				return $row->int_data;
			}
	}
    
    	public static function product_currency(){
				$cn_query=DB::table('web_options')->where('web_options_id',109)->get();
			foreach($cn_query as $row){
				return $row->int_data;
			}
	}
    
    	public static function menu_img(){
				$cn_query=DB::table('web_options')->where('web_options_id',102)->get();
			foreach($cn_query as $row){
				return $row->int_data;
			}
	}
    	public static function category_type(){
				$cn_query=DB::table('web_options')->where('web_options_id',112)->get();
			foreach($cn_query as $row){
				return $row->int_data;
			}
	}
    	public static function category_view(){
				$cn_query=DB::table('web_options')->where('web_options_id',115)->get();
			foreach($cn_query as $row){
				return $row->int_data;
			}
	}
    	public static function all_category(){
				$cn_query=DB::table('web_options')->where('web_options_id',116)->get();
			foreach($cn_query as $row){
				return $row->int_data;
			}
	}
    	public static function compare(){
				$cn_query=DB::table('web_options')->where('web_options_id',117)->get();
			foreach($cn_query as $row){
				return $row->int_data;
			}
	}

	public static function header_type(){
		$cn_query=DB::table('web_options')->where('web_options_id',114)->get();
		foreach($cn_query as $row){
		return $row->int_data;
		}
	}

	public static function gnrl_options($options_id,$kind='int_data'){
		return DB::table('options')->where('options_id',$options_id)->pluck($kind);
	}
	public static function str_options($options_id,$kind='str_data'){
		return DB::table('options')->where('options_id',$options_id)->pluck($kind);
	}
	public static function web_options($options_id,$kind='int_data'){
		return DB::table('web_options')->where('web_options_id',$options_id)->pluck($kind);
	}

	public static function stranice_count(){
		return DB::table('web_b2c_seo')->where('menu_top',1)->count();
	}

    public static function kurs($valuta_id=null){
        $def_valuta_id = DB::table('valuta')->where('izabran',1)->pluck('valuta_id');
        if(is_null($valuta_id)){
            if(Session::has('valuta') && is_numeric(Session::get('valuta'))){
                $valuta_id = Session::get('valuta');
            }else{
                $valuta_id = $def_valuta_id;
                Session::put('valuta',$valuta_id);
            }
        }

        if($valuta_id == $def_valuta_id){
            return 1;
        }
        
        $obj = DB::table('kursna_lista')->where('valuta_id',$valuta_id)->orderBy('kursna_lista_id','desc')->first();
        $vrednost = 1;
        if(isset($obj)){
            $izabrani_kurs = self::web_options(205,'str_data');
            if($izabrani_kurs == 'kupovni'){
                $vrednost = $obj->kupovni;
            }elseif($izabrani_kurs == 'srednji'){
                $vrednost = $obj->srednji;
            }elseif($izabrani_kurs == 'ziralni'){
                $vrednost = $obj->ziralni;
            }elseif($izabrani_kurs == 'prodajni'){
                $vrednost = $obj->prodajni;
            }else{
                $vrednost = $obj->web;
            }
        }

        if($vrednost <= 0){
            $vrednost = 1;
        }
        
        return $vrednost;
    }

	public static function poslovna_godina(){
	
			return DB::table('poslovna_godina')->where('status',0)->pluck('poslovna_godina_id');
	} 
     
    public static function social_icon(){
        foreach(DB::table('preduzece')->where('preduzece_id',1)->get() as $row){
            if($row->facebook!='' and $row->facebook!="''" and $row->facebook!=null ){
               echo '<a class="facebook" target="_blank" href="'.$row->facebook.'"></a>'; 
            }
            if($row->twitter!='' and $row->twitter!="''" and $row->twitter!=null){
               echo '<a class="twitter" target="_blank" href="'.$row->twitter.'"></a>'; 
            }
              if($row->google_p!='' and $row->google_p!="''" and $row->google_p!=null){
               echo '<a class="google-plus" target="_blank" href="'.$row->google_p.'"></a>'; 
            }
             if($row->skype!='' and $row->skype!="''" and $row->skype!=null){
               echo '<a class="skype" target="_blank" href="'.$row->skype.'"></a>'; 
            }
             if($row->instagram!='' and $row->instagram!="''" and $row->instagram!=null){
               echo '<a class="instagram" target="_blank" href="'.$row->instagram.'"></a>'; 
            }
             if($row->linkedin!='' and $row->linkedin!="''" and $row->linkedin!=null){
               echo '<a class="linkedin" target="_blank" href="'.$row->linkedin.'"></a>'; 
            }
             if($row->youtube!='' and $row->youtube!="''" and $row->youtube!=null){
               echo '<a class="youtube" target="_blank" href="'.$row->youtube.'"></a>'; 
            }
        }
      
    }
    public static function server(){
        return DB::table('options')->where('options_id',1326)->pluck('str_data');
    }
	
	public static function enable_filters(){
			return DB::table('web_options')->where('web_options_id',110)->pluck('int_data');
	}

	public static function filters_type(){
			return DB::table('web_options')->where('web_options_id',111)->pluck('int_data');
	}

	public static function limit_liste_robe(){
		$limit =  DB::table('options')->where('options_id',1329)->pluck('int_data');
		if($limit > 400){
			$limit = 400;
		}
		return $limit;
	}

	public static function checkB2B(){
		if(in_array(DB::table('web_options')->where('web_options_id',130)->pluck('int_data'),array(1,2))){
			return true;
		}else{
			return false;
		}
	}
	public static function checkWebCena(){
		if(DB::table('web_options')->where('web_options_id',132)->pluck('int_data')==1){
			return true;
		}else{
			return false;
		}
	}
	public static function checkCena(){
		if(self::checkWebCena()){
			return 'web_cena';
		}else{
			return 'mpcena';
		}
	}
	public static function checkTezina(){
		if(DB::table('web_options')->where('web_options_id',133)->pluck('int_data')==1){
			return true;
		}else{
			return false;
		} 
	}

	public static function checkTroskoskovi_isporuke(){
		if(DB::table('web_options')->where('web_options_id',150)->pluck('int_data')==1){
			return true;
		}else{
			return false;
		} 
	}

	public static function checkTags(){
		if(DB::table('web_options')->where('web_options_id',134)->pluck('int_data')==1){
			return true;
		}else{
			return false;
		}
	}
	public static function checkNarudzbine(){
		if(DB::table('web_options')->where('web_options_id',135)->pluck('int_data')==1){
			return true;
		}else{
			return false;
		}
	}

	public static function googleAnalyticsID(){
		return DB::table('options')->where('options_id',3019)->first();
	}

	public static function facebookPixel(){
		return DB::table('options')->where('options_id',3036)->first();
	}

	public static function loadbee(){
		return DB::table('options')->where('options_id',3058)->pluck('str_data');
	}

	public static function flixmedia(){
		return DB::table('options')->where('options_id',3059)->pluck('str_data');
	}

	public static function pitchprint(){
		return DB::table('options')->where('options_id',3062)->pluck('str_data');
	}
	public static function pitchprint_aktiv(){
		return DB::table('options')->where('options_id',3062)->pluck('int_data');
	}
	public static function chat(){
		return DB::table('options')->where('options_id',3020)->first();
	}

	public static function prodavnica_boje($prodavnica_boje_id,$default=false){
		$prodavnica_boje = DB::table('prodavnica_boje')->where('prodavnica_boje_id',$prodavnica_boje_id)->first();
		if(!$default && isset($prodavnica_boje->kod) && $prodavnica_boje->kod != ''){
			return $prodavnica_boje->kod;
		}
		return $prodavnica_boje->default_kod;
	}
    public static function info_sys($sifra){
        return DB::table('informacioni_sistem')->where(array('sifra'=>$sifra,'aktivan'=>1,'izabran'=>1))->first();
    }

    public static function is_shop(){
    	$stil = DB::table('prodavnica_stil')->where(array('aktivna'=>1,'izabrana'=>1))->first();
    	return DB::table('prodavnica_tema')->where('prodavnica_tema_id',$stil->prodavnica_tema_id)->pluck('shop') == 1;
    }
}
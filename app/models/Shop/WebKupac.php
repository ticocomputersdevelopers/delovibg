<?php
use Service\Mailer;

class WebKupac {

    public static function send_user_request($web_kupac_id){
        
       $user = DB::table('web_kupac')->where('web_kupac_id',$web_kupac_id)->first();

        $body='Da biste zavrsili Vašu registraciju, potrebno je da kliknete na link <a href="'.Options::base_url().'potvrda-registracije/'.$user->kod.'/'.$web_kupac_id.'" target="_blank">'.Options::base_url().'potvrda-registracije/'.$user->kod.'/'.$web_kupac_id.'</a>';
        $subject="Potvrda regisistracije za ".Options::server();
        WebKupac::send_email_to_client($body,$user->email,$subject);
    }

    public static function send_email_to_client($body,$email,$subject){
        
        if(Options::gnrl_options(3003)){        

             Mail::send('shop.email.order', array('body'=>$body), function($message) use ($email, $subject)
            {
                $message->from(EmailOption::mail_from(), EmailOption::name());

                $message->to($email)->subject($subject);
            });
        }else{
            Mailer::send(Options::company_email(),$email, $subject, $body);    
        }

    }

    public static function get_user_name(){
        $query=DB::table('web_kupac')->where('web_kupac_id',Session::get('b2c_kupac'));
        foreach($query->get() as $row){
            return $row->ime." ".$row->prezime;
        }
    }

    public static function get_company_name(){
        $query=DB::table('web_kupac')->where('web_kupac_id',Session::get('b2c_kupac'));
        foreach($query->get() as $row){
            return $row->naziv;
        }
    }

    public static function bodovi(){
        if(!Session::has('b2c_kupac') || Options::web_options(314) == 0){
            return 0;
        }
        $daniTrajanja = Options::web_options(316);
        $bodovi=DB::table('web_kupac')->where('web_kupac_id',Session::get('b2c_kupac'))->pluck('bodovi');

        if($daniTrajanja == 0){
            return $bodovi;
        }elseif($daniTrajanja > 0){
            $lastOrder = DB::table('web_b2c_narudzbina')->where('web_kupac_id',Session::get('b2c_kupac'))->where('popust','>',0.00)->orderBy('datum_dokumenta','desc')->first();
            if(!is_null($lastOrder)){
                $date = $lastOrder->datum_dokumenta;
            }else{
                $date = date('Y-m-d');
            }

            $nowDate = date('Y-m-d');
            $expiredDate = date('Y-m-d', strtotime($date. ' + '.$daniTrajanja.' days'));
            if($expiredDate < $nowDate){
                return 0;
            }
            return $bodovi;
        }else{
            return 0;
        }
    }

    public static function check_distributive_user($id){ 
       $distributive = DB::table('web_kupac')->where('web_kupac_id', $id)->where('status_registracije',1)->where('aktivan_popust',1)->get();
       if($distributive){
        return true;
       }else{
        return false;
       }    
    }

    public static function check_first_discount($id){ 
       $distributive = DB::table('web_b2c_narudzbina')->where('web_kupac_id',$id)->get();
       if($distributive){
        return true;
       }else{
        return false;
       }    
    }
        
    public static function distributive_code() {
        $unique_id = Session::get('client_uid');
        if(Session::has('b2c_kupac')) {
            $web_id = Session::get('b2c_kupac');
        } else {
            $web_id = DB::table('web_kupac')->orderBy('web_kupac_id', 'desc')->pluck('web_kupac_id')+1;
        }
        $dist_code = $unique_id.$web_id;
        return $dist_code;
    }

    public static function trajanjeBodova(){
        $daniTrajanja = Options::web_options(316);
        if(!Session::has('b2c_kupac') || Options::web_options(314) == 0 || $daniTrajanja <= 0){
            return null;
        }

        $lastOrder = DB::table('web_b2c_narudzbina')->where('web_kupac_id',Session::get('b2c_kupac'))->where('popust','>',0.00)->orderBy('datum_dokumenta','desc')->first();
        if(!is_null($lastOrder)){
            $date = $lastOrder->datum_dokumenta;
        }else{
            $date = date('Y-m-d');
        }
        $expiredDate = date('Y-m-d', strtotime($date. ' + '.$daniTrajanja.' days'));
        return date("j.m.Y",strtotime($expiredDate));
    }

    public static function user_name($web_kupac_id){
       return DB::table('web_kupac')->where('web_kupac_id',$web_kupac_id)->first();
        
    }

    // public static function user_surname($web_kupac_id){
        
    //     $prezime=DB::table('web_kupac')->where('web_kupac_id',$web_kupac_id)->pluck('prezime');
        
    //     return $prezime;
    // }
    
    // public static function user_company($web_kupac_id){
        
    //     $naziv=DB::table('web_kupac')->where('web_kupac_id',$web_kupac_id)->pluck('naziv');
        
    //     return $naziv;
    // }

    // public static function user_pib($web_kupac_id){
        
    //     $pib=DB::table('web_kupac')->where('web_kupac_id',$web_kupac_id)->pluck('pib');
        
    //     return $pib;
    // }

    // public static function user_email($web_kupac_id){
        
    //     $email=DB::table('web_kupac')->where('web_kupac_id',$web_kupac_id)->pluck('email');
        
    //     return $email;
    // }


    // public static function user_lozinka($web_kupac_id){
        
    //     $lozinka=DB::table('web_kupac')->where('web_kupac_id',$web_kupac_id)->pluck('lozinka');
        
    //     return base64_decode($lozinka);
    // }

    // public static function user_address($web_kupac_id){
    //     $adresa=DB::table('web_kupac')->where('web_kupac_id',$web_kupac_id)->pluck('adresa');
    //     return $adresa;
    // }

    // public static function user_mesto($web_kupac_id){
        
    //     $mesto_id=DB::table('web_kupac')->where('web_kupac_id',$web_kupac_id)->pluck('mesto_id');
        
    //     return $mesto_id;
    // }
    // 
    
    // public static function user_phone($web_kupac_id){
        
    //     $telefon=DB::table('web_kupac')->where('web_kupac_id',$web_kupac_id)->pluck('telefon');
        
    //     return $telefon;
    // }

    // public static function tip_usera($web_kupac_id){
    //    $tip_kupca=DB::table('web_kupac')->where('web_kupac_id',$web_kupac_id)->pluck('flag_vrsta_kupca');
        
    //     return $tip_kupca; 
    // }

    
}
<?php

class Category extends Eloquent
{
    protected $table = 'grupa_pr';

    protected $primaryKey = 'grupa_pr_id';
    
    public $timestamps = false;

    protected $mappedByExternalCode = [];

	protected $hidden = array(
		'grupa_pr_id',
    	'grupa',
    	'putanja_slika',
		'opis', 
    	'parrent_grupa_pr_id',
    	'sifra',
    	'web_b2b_prikazi',
    	'web_b2c_prikazi',
    	'prikaz',
    	'sifra_connect',
    	'sablon_opis', 
    	'sablon_karakteristike', 
    	'css_opis', 
    	'css_karakteristike', 
    	'b2c_konf_prikazi',
    	'redni_broj',
    	'keywords', 
    	'rabat', 
    	'sifra_is', 
    	'id_is', 
    	'pozadinska_slika'
    	);


	public function getIdAttribute()
	{
        return isset($this->attributes['grupa_pr_id']) ? $this->attributes['grupa_pr_id'] : null;
	}
	public function getNameAttribute()
	{
	    return isset($this->attributes['grupa']) ? $this->attributes['grupa'] : null;
	}
	public function getParentIdAttribute()
	{
        return $this->parent()->pluck('id_is');
	}
    public function getDescriptionAttribute()
    {
        return isset($this->attributes['opis']) ? $this->attributes['opis'] : null;
    }
    public function getStatusAttribute()
    {
        return isset($this->attributes['web_b2c_prikazi']) ? $this->attributes['web_b2c_prikazi'] : null;
    }
    public function getCategoryIdAttribute()
    {
        return isset($this->attributes['id_is']) ? $this->attributes['id_is'] : null;
    }
	// public function getImagePathAttribute()
	// {
	//     return isset($this->attributes['putanja_slika']) ? $this->attributes['putanja_slika'] : null;
	// }
 //    public function getSortAttribute()
 //    {
 //        return isset($this->attributes['redni_broj']) ? $this->attributes['redni_broj'] : null;
 //    }
 //    public function getShowAttribute()
 //    {
 //        return isset($this->attributes['web_b2c_prikazi']) ? $this->attributes['web_b2c_prikazi'] : null;
 //    }
 //    public function getShowB2bAttribute()
 //    {
 //        return isset($this->attributes['web_b2b_prikazi']) ? $this->attributes['web_b2b_prikazi'] : null;
 //    }
      

	protected $appends = array(
    	// 'id',
        'category_id',
        'parent_id',
        'name',
        'description',
        'status',
        // 'show',
        // 'show_b2b',
        // 'image_path',
        // 'sort'
    	);

    public function parent(){
        return $this->belongsTo('Category','parrent_grupa_pr_id');
    }

    public function childs(){
        return $this->hasMany('Category', 'parrent_grupa_pr_id', 'grupa_pr_id');
    }

    public function categoryLangs(){
        return $this->hasMany('CategoryLang', 'grupa_pr_id');
    }

    public function articles(){
        return $this->hasMany('Article', 'grupa_pr_id');
    }


    public function getIdByCode($name,$mapped=[]){
        if(isset($mapped[$name])){
            return $mapped[$name];
        }
        return -1;

        // if(is_null($categoryId = self::where('grupa',$name)->pluck('grupa_pr_id'))){
        //     $nextId = Category::max('grupa_pr_id')+1;

        //     $category = new Category();
        //     $category->grupa_pr_id = $nextId;
        //     $category->sifra = $nextId;
        //     $category->parrent_grupa_pr_id = 0;
        //     $category->grupa = $name;
        //     $category->web_b2c_prikazi = 1;
        //     $category->web_b2b_prikazi = 1;
        //     $category->save();
        //     $categoryId = $category->grupa_pr_id;
        // }

        // return $categoryId;
    }

    public function mappedByExternalCode(){
        $mapped = [];
        foreach(self::select('grupa_pr_id','id_is')->whereNotNull('id_is')->where('id_is','<>','')->where('grupa_pr_id','>',0)->get() as $category){
            $mapped[$category->id_is] = $category->grupa_pr_id;
        }
        return $mapped;
    }

    public function mappedByIdAndExternalCode(){
        $mapped = [];
        foreach(self::select('grupa_pr_id','id_is')->whereNotNull('id_is')->where('id_is','<>','')->get() as $category){
            $mapped[$category->grupa_pr_id] = $category->id_is;
        }
        return $mapped;
    }

    public function mappedByParentId(){
        $mapped = [];
        foreach(self::select('grupa_pr_id','parrent_grupa_pr_id')->where('grupa_pr_id','>',0)->get() as $category){
            $mapped[$category->grupa_pr_id] = $category->parrent_grupa_pr_id;
        }
        return $mapped;
    }
    public function mappedByName(){
        $mapped = [];
        foreach(self::select('grupa_pr_id','grupa')->where('grupa_pr_id','>',0)->get() as $category){
            $mapped[$category->grupa_pr_id] = $category->grupa;
        }
        return $mapped;
    }
}
<?php

class Brand extends Eloquent
{
    protected $table = 'proizvodjac';

    protected $primaryKey = 'proizvodjac_id';
    
    public $timestamps = false;

	protected $hidden = array(
		'proizvodjac_id',
        'naziv',
        'sifra',
        'partner_id',
        'drzava_id',
        'sifra_connect',
        'slika',
        'opis',
        'keywords',
        'brend_prikazi',
        'rbr',
        'rabat',
        'id_is',
        'produzena_garancija',
        'produzena_garancija_slika',
        'produzena_garancija_link'
    	);

	public function getIdAttribute()
	{
	    return isset($this->attributes['id_is']) ? $this->attributes['id_is'] : null;
	}
	public function getNameAttribute()
	{
	    return isset($this->attributes['naziv']) ? $this->attributes['naziv'] : null;
	}


	protected $appends = array(
    	'id',
    	'name'
    	);

    public function articles(){
        return $this->hasMany('Article', 'proizvodjac_id');
    }


    public function getIdByName($name,$mapped=[]){
        if(isset($mapped[$name])){
            return $mapped[$name];
        }
        
        if(is_null($brandId = self::where('naziv',$name)->pluck('proizvodjac_id'))){
            DB::statement("SELECT setval('proizvodjac_proizvodjac_id_seq', (SELECT MAX(proizvodjac_id)+1 FROM proizvodjac), FALSE)");

            $brand = new Brand();
            $brand->naziv = $name;
            $brand->save();
            $brandId = $brand->proizvodjac_id;
        }

        return $brandId;
    }

    public function mappedByName(){
        $mapped = [];
        foreach(self::select('proizvodjac_id','naziv')->get() as $brand){
            $mapped[$brand->naziv] = $brand->proizvodjac_id;
        }
        return $mapped;
    }

    public function mappedByExternalCode(){
        $mapped = [];
        foreach(self::select('proizvodjac_id','id_is')->get() as $brand){
            $mapped[$brand->proizvodjac_id] = $brand->id_is;
        }
        return $mapped;
    }
    public function mappedByIdAndExternalCode(){
        $mapped = [];
        foreach(self::select('proizvodjac_id','id_is')->get() as $brand){
            $mapped[$brand->id_is] = $brand->proizvodjac_id;
        }
        return $mapped;
    }
}
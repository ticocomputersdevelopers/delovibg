<?php
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 3600);

use IsSkala\DBData;
use IsSkala\Support;
use IsSkala\Group;
use IsSkala\Manufacturer;
use IsSkala\Article;
use IsSkala\Stock;
use IsSkala\ArticleGroup;
// use IsSkala\Course;
use IsSkala\Image;


class AdminIsSkala {

    public static function execute(){
        try {
            $dbdata = new DBData;

            // //kurs
            // $courses = $dbdata->courses();
            // Course::query_insert_update($courses);

            //groups
            $groups = $dbdata->groups();
            $resultGroup = Group::table_body($groups);
            Group::query_insert_update($resultGroup->body,array('grupa','parrent_grupa_pr_id'));
            Group::query_update_unexists($resultGroup->body);
            Support::updateGroupsParent($groups);

            //manufacturers
            $manufacturers = $dbdata->manufacturers();
            $resultManufacturer = Manufacturer::table_body($manufacturers);
            Manufacturer::query_insert_update($resultManufacturer->body,array('naziv'));

            //articles
            $articles = $dbdata->articles();
            $resultArticle = Article::table_body($articles);
            Article::query_insert_update($resultArticle->body,array('naziv','proizvodjac_id','naziv_displej','naziv_web','web_opis','racunska_cena_nc'));
            Article::query_update_unexists($resultArticle->body);

            $mappedArticles = Support::getMappedArticles();
            $mappedGroups = Support::getMappedGroups();
            
            //articles_groups
            $articles_groups = $dbdata->articles_groups();
            Support::updateAricleMainGroups($articles_groups->main_articles_groups,$mappedArticles,$mappedGroups);
            $resultArticleGroup = ArticleGroup::table_body($articles_groups->secondary_articles_groups,$mappedArticles,$mappedGroups);
            ArticleGroup::query_insert_delete($resultArticleGroup->body);

            //stock
            $resultStock = Stock::table_body($articles);
            Stock::query_insert_update($resultStock->body);

            //images
            $images = $dbdata->images();
            $resultImage = Image::table_body($images,$mappedArticles);
            $new_images = Image::new_images($resultImage->body);
            Image::query_insert_update($resultImage->body);
            Image::query_update_unexists($resultImage->body);
            Support::save_image_files($images,$new_images);

            AdminB2BIS::saveISLog('true');
            return (object) array('success'=>true);
        }catch (Exception $e){

            AdminB2BIS::saveISLog('false');
            return (object) array('success'=>false,'message'=>$e->getMessage());
        }
    }



}
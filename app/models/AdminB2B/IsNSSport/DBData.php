<?php
namespace IsNSSport;

use DB;

class DBData {

	// public static function articles($magacin){
	// 	return DB::connection('sqlsrv')->select("SELECT a.*, g.naziv_gr, pg.naziv_pgr, (SELECT proizvodjac FROM dbo.artiklid WHERE id_art=a.id_art) as proizvodjac, pz.naziv_por, pz.stopa, l.kolicina, l.nabcena ,l.cenapp FROM dbo.artikli a LEFT JOIN dbo.artikli_grupe g ON a.id_artgr=g.id_artgr LEFT JOIN dbo.artikli_podgrupe pg ON a.id_artpgr=pg.id_artpgr LEFT JOIN dbo.porezi pz ON a.id_porez=pz.id_porez LEFT JOIN dbo.lager l ON a.id_art=l.id_art WHERE id_mag IN (SELECT id_mag FROM dbo.magacini WHERE naziv_mag='".$magacin."')");
	// }

	// public static function articles($magacin,$cenovnik=1){
	// 	return DB::connection('sqlsrv')->select("SELECT a.*, (SELECT naziv_gr FROM dbo.artikli_grupe WHERE id_artgr=a.id_artgr) as naziv_gr, (SELECT proizvodjac FROM dbo.artiklid WHERE id_art=a.id_art) as proizvodjac, pz.naziv_por, pz.stopa, l.kolicina, l.nabcena ,l.cenapp, c.cena AS web_cena FROM dbo.artikli a LEFT JOIN dbo.porezi pz ON a.id_porez=pz.id_porez LEFT JOIN dbo.lager l ON a.id_art=l.id_art LEFT JOIN dbo.cenovnik c ON a.id_art=c.id_art WHERE l.id_mag IN (SELECT id_mag FROM dbo.magacini WHERE naziv_mag='".$magacin."') AND c.id_cenc IN (SELECT id_cenc FROM dbo.cenovnici WHERE broj_cenc='".$cenovnik."')");
	// }

	public static function articles($magacin,$cenovnik=1){
		return DB::connection('sqlsrv')->select(
			"SELECT a.*, 
			(SELECT naziv_gr FROM dbo.artikli_grupe WHERE id_artgr=a.id_artgr) as naziv_gr, 
			(SELECT proizvodjac FROM dbo.artiklid WHERE id_art=a.id_art) as proizvodjac,
			(SELECT naziv_por FROM dbo.porezi pz WHERE a.id_porez=pz.id_porez) as naziv_por,
			(SELECT stopa FROM dbo.porezi pz WHERE a.id_porez=pz.id_porez) as stopa, 
			(SELECT l.kolicina from dbo.lager l WHERE a.id_art=l.id_art and l.id_mag IN (SELECT id_mag FROM dbo.magacini WHERE naziv_mag='".$magacin."')) AS kolicina,
			(SELECT l.nabcena from dbo.lager l WHERE a.id_art=l.id_art and l.id_mag IN (SELECT id_mag FROM dbo.magacini WHERE naziv_mag='".$magacin."')) AS nabcena,
			(SELECT l.cenapp from dbo.lager l WHERE a.id_art=l.id_art and l.id_mag IN (SELECT id_mag FROM dbo.magacini WHERE naziv_mag='".$magacin."')) AS cenapp, 
			c.cena AS web_cena 
			FROM dbo.artikli a LEFT JOIN dbo.cenovnik c ON a.id_art=c.id_art WHERE c.id_cenc IN (SELECT id_cenc FROM dbo.cenovnici WHERE broj_cenc='".$cenovnik."')");
	}

	public static function groups(){
		return DB::connection('sqlsrv')->select("SELECT * FROM grupa_real");
	}

	public static function manufacturers(){
		return DB::connection('sqlsrv')->select("SELECT * FROM proizvodjaci");
	}

	public static function partners(){
		return DB::connection('sqlsrv')->select("SELECT * FROM partneri WHERE neaktivan = 0");
	}

	public static function images(){
		return DB::connection('sqlsrv')->select("SELECT * FROM slike");
	}

	public static function courses(){
		return DB::connection('sqlsrv')->select("SELECT * FROM kurs");
	}
}
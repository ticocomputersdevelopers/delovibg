<?php
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 3600);


use IsPanteon\DBData;
use IsPanteon\Article;
use IsPanteon\Stock;
use IsPanteon\Partner;
use IsPanteon\PartnerCard;
use IsPanteon\Support;



class AdminIsPanteon {

    public static function execute(){

        try {
            //partner
            $partners = DBData::partners();
            $resultPartner = Partner::table_body($partners);
            Partner::query_insert_update($resultPartner->body,array('naziv','adresa'));
            $mappedPartners = Support::getMappedPartners();

            //partner card
            $partnersCards = DBData::partnersCards();
            $resultPartner = PartnerCard::table_body($partnersCards,$mappedPartners);
            PartnerCard::query_insert_update($resultPartner->body);

            //articles
            $articles = DBData::articles();
            $groups = Support::filteredGroups($articles);
            Support::saveGroups($groups);

            $resultArticle = Article::table_body($articles);
            Article::query_insert_update($resultArticle->body,array('flag_aktivan','flag_prikazi_u_cenovniku','naziv','naziv_web','proizvodjac_id','barkod','racunska_cena_nc','racunska_cena_a','racunska_cena_end','mpcena','web_cena','jedinica_mere_id','model','garancija','tarifna_grupa_id','barkod','tezinski_faktor','trans_pak','kom_pak'));
            // Article::query_update_unexists($resultArticle->body);

            $mapped_articles = Support::getMappedArticles();

            $resultStock = Stock::table_body($articles,$mapped_articles);
            Stock::query_insert_update($resultStock->body);

            Support::updateGroupVisible();

            AdminB2BIS::saveISLog('true');
            return (object) array('success'=>true);
        }catch (Exception $e){
            AdminB2BIS::saveISLog('false');
            return (object) array('success'=>false,'message'=>$e->getMessage());
        }
    }



}
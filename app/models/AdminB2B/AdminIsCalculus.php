<?php
use ISCalculus\CalculusAPI;
use ISCalculus\Support;
use ISCalculus\Group;
use ISCalculus\Article;
use ISCalculus\Price;
use ISCalculus\Stock;
use ISCalculus\Partner;


class AdminISCalculus {

    public static function execute(){
      try {
      	//groups
        $groups = CalculusAPI::groups();
        $resultGroup = Group::table_body($groups);
      	Group::query_insert_update($resultGroup->body);
      	Group::query_update_unexists($resultGroup->body);
      	Support::updateGroupsParent($groups);

        //articles
        $articles = CalculusAPI::articles();
        $resultArticle = Article::table_body($articles);
        Article::query_insert_update($resultArticle->body,array('id_is','naziv','jedinica_mere_id','proizvodjac_id','naziv_displej','naziv_web','web_opis','akcija_flag_primeni','barkod'));
        Article::query_update_unexists($resultArticle->body);

        //b2b prices
        $prices = CalculusAPI::prices();
        $resultPrice = Price::table_body($prices);
        Price::query_update($resultPrice->body);

        //b2b stock
        if(AdminB2BOptions::info_sys('calculus')->b2b_magacin){
          $stock = CalculusAPI::stock(AdminB2BOptions::info_sys('calculus')->b2b_magacin);
          $resultStock = Stock::table_body($stock);
          Stock::query_insert_update($resultStock->body);
        }
        //b2c stock
        if(AdminB2BOptions::info_sys('calculus')->b2c_magacin){
          $stock = CalculusAPI::stock(AdminB2BOptions::info_sys('calculus')->b2c_magacin);
          $resultStock = Stock::table_body($stock,1);
          Stock::query_insert_update($resultStock->body);
        }

        //partner
        $partners = CalculusAPI::partners();
        $resultPartner = Partner::table_body($partners,array('id_kategorije'));
        Partner::query_insert_update($resultPartner->body);

        AdminB2BIS::saveISLog('true');
        return (object) array('success'=>true);
      }catch (Exception $e){
        AdminB2BIS::saveISLog('false');
          return (object) array('success'=>false,'message'=>$e->getMessage());
      }
    }



}
<?php
namespace IsPanteon;

use DB;


class DBData {

	public static function articles(){
		return DB::connection('sqlsrv')->select("SELECT * FROM _b2bSetItem WHERE acIdent NOT LIKE 'OLD_%'");
	}

	public static function partners(){
		return DB::connection('sqlsrv')->select("SELECT * FROM _b2bSetSubj");
	}

	public static function partnersCards(){
		return DB::connection('sqlsrv')->select("SELECT * FROM _b2bAcctTransItem");
	}

}
<?php
namespace IsPanteon;

use DB;
use Groups;


class Support {


	public static function getPartnerId($sifra_kupca_logik){
		$partner = DB::table('partner')->where('id_is',$sifra_kupca_logik)->first();
		if(!is_null($partner)){
			return $partner->partner_id;
		}
		return null;
	}

	public static function getTarifnaGrupaId($naziv_tarifne_grupe,$vrednost_tarifne_grupe=20){
		$tg = DB::table('tarifna_grupa')->where(array('porez'=>$vrednost_tarifne_grupe))->first();

		if(is_null($tg)){
			$next_id = DB::table('tarifna_grupa')->max('tarifna_grupa_id')+1;
			DB::table('tarifna_grupa')->insert(array(
				'tarifna_grupa_id' => $next_id,
				'sifra' => $next_id,
				'naziv' => substr($naziv_tarifne_grupe,0,99),
				'porez' => $vrednost_tarifne_grupe,
				'active' => 1,
				'default_tarifna_grupa' => 0,
				'tip' => substr($vrednost_tarifne_grupe,0,19)
				));
			$tg = DB::table('tarifna_grupa')->where('porez',$vrednost_tarifne_grupe)->first();
		}
		return $tg->tarifna_grupa_id;
	}

	public static function getJedinicaMereId($jedinica_mere){
		$jm = DB::table('jedinica_mere')->where('naziv',$jedinica_mere)->first();

		if(is_null($jm)){
			DB::table('jedinica_mere')->insert(array(
				'jedinica_mere_id' => DB::table('jedinica_mere')->max('jedinica_mere_id')+1,
				'naziv' => $jedinica_mere
				));
			$jm = DB::table('jedinica_mere')->where('naziv',$jedinica_mere)->first();
		}
		return $jm->jedinica_mere_id;
	}

	public static function getProizvodjacId($proizvodjac){
		$pro = DB::table('proizvodjac')->where('naziv',$proizvodjac)->first();

		if(is_null($pro)){
			DB::table('proizvodjac')->insert(array(
				'naziv' => $proizvodjac,
				'sifra_connect' => 0
				));
			$pro = DB::table('proizvodjac')->where('naziv',$proizvodjac)->first();
		}
		return $pro->proizvodjac_id;
	}

	public static function getGrupaIdByParent($parent_group_name,$group_name){
		$parent_group_name = trim($parent_group_name);
		$group_name = trim($group_name);
		$groups = DB::table('grupa_pr')->where('grupa',$group_name)->whereNotNull('sifra_is')->where('sifra_is','!=','')->get();
		$parent_groups = DB::table('grupa_pr')->where('grupa',$parent_group_name)->whereNotNull('sifra_is')->where('sifra_is','!=','')->get();

		$main_group_id = self::getNewGrupaId('PANTEON GRUPE OSTALO');

		$grupa_pr_id = $main_group_id;
		foreach($groups as $group){
			foreach($parent_groups as $parent_group){
				if($parent_group->grupa_pr_id == $group->parrent_grupa_pr_id){
					$grupa_pr_id = $group->grupa_pr_id;
					break;
				}
			}
			if($grupa_pr_id != $main_group_id){
				break;
			}
		}
		return $grupa_pr_id;
	}

	public static function getGrupaId($group_name){
		$group_name = trim($group_name);
		$group = DB::table('grupa_pr')->where('grupa',$group_name)->whereNotNull('sifra_is')->where('sifra_is','!=','')->first();

		if(is_null($group)){
			$grupa_pr_id = self::getNewGrupaId('PANTEON GRUPE');
		}else{
			$grupa_pr_id = $group->grupa_pr_id;
		}
		return $grupa_pr_id;
	}

	public static function getNewGrupaId($group_name){
		$group_name = trim($group_name);
		$group = DB::table('grupa_pr')->where('grupa',$group_name)->whereNotNull('sifra_is')->where('sifra_is','!=','')->first();

		if(is_null($group)){
			$grupa_pr_id = DB::select("SELECT MAX(grupa_pr_id) + 1 AS max FROM grupa_pr")[0]->max;
			DB::table('grupa_pr')->insert(array(
				'grupa_pr_id' => $grupa_pr_id,
				'grupa' => $group_name,
				'parrent_grupa_pr_id' => 0,
				'sifra' => $grupa_pr_id,
				'sifra_is' => $group_name
				));
		}else{
			$grupa_pr_id = $group->grupa_pr_id;
		}
		return $grupa_pr_id;
	}

	public static function saveSingleGroup($group_name,$group_parent=null,$update_parent=false){
		$group_name = trim($group_name);
		$group = DB::table('grupa_pr')->where('grupa',$group_name)->whereNotNull('sifra_is')->where('sifra_is','!=','')->first();

		$group_parent_id = self::getGrupaId('PANTEON GRUPE');
		if(!is_null($group_parent) && trim($group_parent) != ''){
			$group_parent_id = self::getGrupaId(trim($group_parent));
		}

		if(is_null($group)){
			$grupa_pr_id = DB::select("SELECT MAX(grupa_pr_id) + 1 AS max FROM grupa_pr")[0]->max;
			if(is_null($grupa_pr_id)){
				$grupa_pr_id = 1;
			}
			DB::table('grupa_pr')->insert(array(
				'grupa_pr_id' => $grupa_pr_id,
				'grupa' => $group_name,
				'parrent_grupa_pr_id' => $group_parent_id,
				'sifra' => $grupa_pr_id,
				'sifra_is' => $group_name
				));
		}else{
			$grupa_pr_id = $group->grupa_pr_id;
			if($update_parent){
				DB::table('grupa_pr')->where('grupa_pr_id',$grupa_pr_id)->update(array('parrent_grupa_pr_id' => $group_parent_id));				
			}
		}
		return $grupa_pr_id;
	}

	public static function filteredGroups($articles){
		$groups = array();
		foreach($articles as $article) {
			if(isset($article->acClassif2) && $article->acClassif2 != ''){
				$groups[mb_convert_encoding($article->acClassif2,mb_detect_encoding($article->acClassif2),"UTF-8")] = mb_convert_encoding($article->acClassif,mb_detect_encoding($article->acClassif),"UTF-8");
			}
		}
		return $groups;
	}

	public static function parentGroups($allgroups,$group,&$tree){
		if(!is_null($group) && $group != ''){
			$tree[] = $group;
			if(isset($allgroups[$group]) && $allgroups[$group] != ''){
				self::parentGroups($allgroups,$allgroups[$group],$tree);
			}
		}
	}

	public static function saveGroups($groups){
		foreach($groups as $grupa => $nadgrupa) {
			if($grupa != $nadgrupa){
				$tree = array($grupa);
				self::parentGroups($groups,$nadgrupa,$tree);
				for ($i=(count($tree)-1);$i>=0;$i--) {
					self::saveSingleGroup($tree[$i],(isset($tree[$i+1]) ? $tree[$i+1] : null),true);
				}
			}
		}
	}

	public static function getPartnerCategoryId($category_name){
		$category_name = trim($category_name);
		$category = DB::table('partner_kategorija')->where('naziv',$category_name)->first();

		if(is_null($category)){
			$id_kategorije = DB::select("SELECT MAX(id_kategorije) + 1 AS max FROM partner_kategorija")[0]->max;
			if(is_null($id_kategorije)){
				$id_kategorije = 1;
			}
			DB::table('partner_kategorija')->insert(array(
				'id_kategorije' => $id_kategorije,
				'naziv' => $category_name,
				'rabat' => 0,
				'active' => 1
				));
		}else{
			$id_kategorije = $category->id_kategorije;
		}
		return $id_kategorije;
	}

	public static function getMappedArticles(){
		$mapped = array();
		$articles = DB::table('roba')->whereNotNull('id_is')->where('id_is','!=','')->get();
		foreach($articles as $article){
			$mapped[$article->id_is] = $article->roba_id;
		}
		return $mapped;
	}

	public static function getMappedPartners(){
		$mapped = array();

		$partners = DB::table('partner')->select('sifra','partner_id')->whereNotNull('sifra')->where('sifra','!=','')->get();
		foreach($partners as $partner){
			$mapped[trim($partner->sifra)] = $partner->partner_id;
		}

		return $mapped;
	}
	
	public static function convert($text){
		$text = preg_replace('/[^a-zA-Z0-9\ \!\%\&\(\)\=\*\/\,\.\+\-\_\@\?\:\;\<\>\'\"\č\ć\ž\š\đ\Č\Ć\Ž\Š\Đ]/', '',$text);
		// $text = mb_convert_encoding($text, 'UTF-8', mb_detect_encoding($text, mb_detect_order(), true));
		$text = iconv("UTF-8", "WINDOWS-1250//TRANSLIT//IGNORE",$text);
		$text = pg_escape_string($text);
		return $text;
	}

	public static function updateGroupVisible(){
		foreach(DB::table('grupa_pr')->where('grupa_pr_id','>',0)->where(function($q){ return $q->whereNull('sifra_is')->orWhere('sifra_is',''); })->get() as $grupa){
			$ids=array();
		    Groups::allGroups($ids,$grupa->grupa_pr_id);
		    if(count(DB::table('roba')->where('flag_aktivan',1)->where(array('flag_aktivan'=>1,'flag_cenovnik'=>1))->whereIn('grupa_pr_id',$ids)->get()) > 0){
		    	DB::table('grupa_pr')->whereIn('grupa_pr_id',$ids)->update(array('web_b2b_prikazi'=>1,'web_b2c_prikazi'=>1));
		    }else{
		    	DB::table('grupa_pr')->whereIn('grupa_pr_id',$ids)->update(array('web_b2b_prikazi'=>0,'web_b2c_prikazi'=>0));
		    }
		}

	}

}
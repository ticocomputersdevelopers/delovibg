<?php
namespace IsPanteon;

use DB;

class Partner {

	public static function table_body($partners){

		$result_arr = array();

		$partner_id = DB::select("SELECT nextval('partner_partner_id_seq')")[0]->nextval;
		foreach($partners as $partner) {
			if(isset($partner->anQID) && strlen($partner->acSubject) <= 10){
		    	$partner_id++;
	            $id_is=$partner->anQID;
	            $sifra= $partner->acSubject;
	            $naziv=isset($partner->acName2) ? substr(Support::convert($partner->acName2),0,250) : null;
	            // $mesto=isset($partner->mesto) ? pg_escape_string(substr($partner->mesto,0,250)) : null;
	            $mesto=null;
	            $adresa=isset($partner->acAddress) ? substr(Support::convert($partner->acAddress),0,250) : null;
	            $telefon=isset($partner->acPhone) ? pg_escape_string(substr($partner->acPhone,0,250)) : null;
	            $pib=isset($partner->acCode) && is_numeric(intval($partner->acCode)) ? pg_escape_string(substr($partner->acCode,0,100)) : null;
	            $broj_maticni=isset($partner->acRegNo) ? substr($partner->acRegNo,0,30) : null;
	            $mail=isset($partner->acEmail) ? substr($partner->acEmail,0,100) : null;
	            $rabat= isset($partner->anRebate) && is_numeric(floatval($partner->anRebate)) ? $partner->anRebate : 0;

		        $result_arr[] = "(".strval($partner_id).",'".$sifra."','".trim($naziv)."','".trim($adresa)."','".trim($mesto)."',0,'".trim($telefon)."',NULL,'".strval($pib)."','".strval($broj_maticni)."',NULL,NULL,NULL,NULL,".strval($rabat).",0,0,'".trim(substr($naziv,0,200))."',0,0,1,1,0,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,'".$mail."',0,1,0,0,NULL,1,'".$id_is."',(NULL)::integer,NULL,NULL,(NULL)::INTEGER,NULL,NULL,0)";
		    }
		}

		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='partner'"));
		$table_temp = "(VALUES ".$table_temp_body.") partner_temp(".implode(',',$columns).")";

		DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");

		// update
		$updated_columns=array();
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="partner_id" && $col!="id_is" && $col!="login" && $col!="password"){
		    	$updated_columns[] = "".$col." = partner_temp.".$col."";
			}
		}
		DB::statement("UPDATE partner t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.id_is=partner_temp.id_is");

		//insert
		DB::statement("INSERT INTO partner (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM partner t WHERE t.id_is=partner_temp.id_is))");
		DB::statement("SET CLIENT_ENCODING TO 'UTF8'");

		DB::statement("SELECT setval('partner_partner_id_seq', (SELECT MAX(partner_id) FROM partner) + 1, FALSE)");
	}
}
<?php
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 3600);

use IsGSM\FileData;
use IsGSM\Article;
use IsGSM\Stock;
use IsGSM\Support;


class AdminIsGSM {

    public static function execute(){

      try {
            //articles
            $articleDetails = FileData::articles();
            // $groups = Support::filteredGroups($articleDetails->articles);
            // Support::saveGroups($groups);

            $resultArticle = Article::table_body($articleDetails->articles);
            Article::query_insert_update($resultArticle->body,array('racunska_cena_nc','racunska_cena_end','mpcena','proizvodjac_id'));
            Support::linkedWidthDC();
            $mappedArticles = Support::getMappedArticles();

            //stock
            $resultStock = Stock::table_body($articleDetails->articles,$mappedArticles);
            Stock::query_insert_update($resultStock->body);

            AdminB2BIS::saveISLog('true');
            return (object) array('success'=>true);
        }catch (Exception $e){
            AdminB2BIS::saveISLog('false');
            return (object) array('success'=>false,'message'=>$e->getMessage());
        }
    }



}
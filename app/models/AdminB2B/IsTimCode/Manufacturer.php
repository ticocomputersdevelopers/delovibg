<?php
namespace IsTimCode;

use DB;

class Manufacturer {

	public static function table_body($manufacturers){

		$result_arr = array();

		$manufacturer_id = DB::select("SELECT MAX(proizvodjac_id) + 1 AS max FROM proizvodjac")[0]->max;
		foreach($manufacturers as $manufacturer) {
		    $manufacturer_id++;

			$id = $manufacturer->manufID;
			$sifra = pg_escape_string($manufacturer->sifra);
			$name = pg_escape_string(Support::convert($manufacturer->naziv));

			$result_arr[] = "(".$manufacturer_id.",'".$name."','".$sifra."',(NULL)::integer,(NULL)::integer,0,NULL,NULL,NULL,0,0,(NULL)::integer,'".$id."',0,NULL,NULL)";
  
		}
		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}
		$all_columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='proizvodjac'"));
		$columns = $all_columns;
		$columns_without_id = $all_columns;
		unset($columns_without_id[0]);
		$table_temp = "(VALUES ".$table_temp_body.") proizvodjac_temp(".implode(',',$columns).")";

		// update
		$updated_columns=array();
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="proizvodjac_id" && $col!="id_is"){
		    	$updated_columns[] = "".$col." = proizvodjac_temp.".$col."";
			}
		}
		DB::statement("UPDATE proizvodjac t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.id_is=proizvodjac_temp.id_is");

		//insert
		$where = "";
		if(DB::table('proizvodjac')->count() > 0){
			$where = " WHERE proizvodjac_temp.id_is NOT IN (SELECT id_is FROM proizvodjac WHERE id_is IS NOT NULL AND id_is <> '')";
		}
		DB::statement("INSERT INTO proizvodjac(".implode(',',$columns_without_id).") SELECT ".implode(',',$columns_without_id)." FROM ".$table_temp.$where."");

		DB::statement("SELECT setval('proizvodjac_proizvodjac_id_seq', (SELECT MAX(proizvodjac_id) FROM proizvodjac) + 1, FALSE)");
		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
	}

	public static function query_delete_unexists($table_temp_body) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='proizvodjac'"));
		$table_temp = "(VALUES ".$table_temp_body.") proizvodjac_temp(".implode(',',$columns).")";

		DB::statement("DELETE FROM proizvodjac t WHERE NOT EXISTS(SELECT * FROM ".$table_temp." WHERE t.id_is=proizvodjac_temp.id_is) and proizvodjac_id NOT IN (-1,0)");
	}

}
<?php
namespace IsTimCode;
use AdminB2BIS;
use DB;

class Article {

	public static function table_body($articles){
		$getMappedGroups = Support::getMappedGroups();
		$mappedManufacturrersIds = Support::getMappedMAnufacturers();

		$result_arr = array();
		$codes = array();

		$roba_id = DB::select("SELECT nextval('roba_roba_id_seq')")[0]->nextval;
		$sifra_k = DB::table('roba')->max('sifra_k')+1;

		foreach($articles as $article) {
			$id_is = $article->robaID;

			$roba_id++;
			$sifra_k++;
			$sifra_is = pg_escape_string($article->sifra);
			$grupa_pr_id = isset($getMappedGroups[$article->grprobaID]) ? $getMappedGroups[$article->grprobaID] : -1;
			$naziv = preg_replace('/\'/', '',substr(Support::convert($article->naziv),0,300));
			// $tarifna_grupa_id = $article->porStopaVrsta == 2 ? 0 : 1;
			$tarifna_grupa_id = 0;
			$jedinica_mere_id = Support::getJedinicaMereId(strtolower($article->jm));
			$proizvodjac_id = $article->manufID > 0 && isset($mappedManufacturrersIds[$article->manufID]) ? $mappedManufacturrersIds[$article->manufID] : -1;
			$web_opis = substr(Support::convert($article->opis),0,1000);

			$racunska_cena_nc = 0;
			$mpcena = 0;
			$web_cena = 0;
			if(is_array($article->robaMag) && count($article->robaMag) > 0 && isset($article->robaMag[0]->prodCena) && is_numeric($article->robaMag[0]->prodCena)){
				$racunska_cena_nc = floatval($article->robaMag[0]->prodCena);
				$mpcena = $racunska_cena_nc;
				$web_cena = $racunska_cena_nc;				
			}

			$barkod = $article->barkod && is_array($article->barkod) && count($article->barkod) > 0 ? $article->barkod[0]->kod : '';
			$akcija = is_array($article->dodPolje) && count($article->dodPolje) > 0 && $article->dodPolje[0]->naziv == 'AKCIJSKI PROIZVOD' && $article->dodPolje[0]->vrednost == 'Da' ? 1 : 0;

			$flag_aktivan = '0';
			$flag_prikazi_u_cenovniku = '0';
			if(!is_null($roba_id) && is_array($article->robaMag) && count($article->robaMag) > 0 && isset($article->robaMag[0]->stanje) && is_numeric($article->robaMag[0]->stanje) && $article->robaMag[0]->stanje > 0){
				$flag_prikazi_u_cenovniku = '1';
				$flag_aktivan = '1';
			}

			$result_arr[] = "(".strval($roba_id).",NULL,'".$naziv."',NULL,NULL,NULL,".$grupa_pr_id.",".strval($tarifna_grupa_id).",".strval($jedinica_mere_id).",".strval($proizvodjac_id).",-1,".strval($sifra_k).",NULL,NULL,'".substr($naziv,0,20)."',0,-1,0,0,0,0,9,0,0,0,0,1,".$flag_prikazi_u_cenovniku.",0,NULL,".$flag_aktivan.",".strval($racunska_cena_nc).",0,".strval($racunska_cena_nc).",0,NULL,".strval($mpcena).",false,0,(NULL)::integer,'".$naziv."',1,NULL,NULL,(NULL)::integer,(NULL)::integer,0,0,0,-1,-1,".strval($web_cena).",1,0,'".strval($web_opis)."',NULL,NULL,0,".strval($akcija).",0,NULL,NULL,NULL,NULL,1,0,'".$barkod."',0,0,1,1,-1,NULL,NULL,NULL,NULL,0,0.00,0.00,0.00,0,'".strval($sifra_is)."',(NULL)::date,(NULL)::date,(NULL)::integer,NULL,'".strval($sifra_is)."','".strval($id_is)."',0,0,0,(NULL)::date,(NULL)::date,0.00,0.00,1)";
		}

		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}

		$all_columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$columns = $all_columns;
		$columns_without_id = $all_columns;
		unset($columns_without_id[0]);
		
		$table_temp = "(VALUES ".$table_temp_body.") roba_temp(".implode(',',$columns).")";

		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
		
		// update
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="roba_id" && $col!="sifra_d" && $col!="id_is"){
		    	$updated_columns[] = "".$col." = roba_temp.".$col."";
			}
		}

		DB::statement("UPDATE roba t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.id_is=roba_temp.id_is::varchar AND t.id_is IS NOT NULL AND t.id_is <> '' AND t.flag_zakljucan = 'false'");

		$id_iss = DB::select("SELECT id_is FROM ".$table_temp." WHERE id_is NOT IN (SELECT id_is FROM roba WHERE id_is IS NOT NULL AND id_is <> '')");

		//insert
		$where = "";
		if(DB::table('roba')->count() > 0){
			$where = " WHERE roba_temp.id_is NOT IN (SELECT id_is FROM roba WHERE id_is IS NOT NULL AND id_is <> '')";
		}
		DB::statement("INSERT INTO roba(".implode(',',$columns_without_id).") SELECT ".implode(',',$columns_without_id)." FROM ".$table_temp.$where."");

		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
		DB::statement("SELECT setval('roba_roba_id_seq', (SELECT MAX(roba_id) FROM roba) + 1, FALSE)");

		return array_map('current',$id_iss);
	}

	public static function query_update_unexists($table_temp_body) {

		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$table_temp = "(VALUES ".$table_temp_body.") roba_temp(".implode(',',$columns).")";

		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		DB::statement("UPDATE roba t SET flag_aktivan = 0 WHERE t.id_is IS NOT NULL AND NOT EXISTS(SELECT * FROM ".$table_temp." WHERE t.id_is=roba_temp.id_is)");
		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
	}
}
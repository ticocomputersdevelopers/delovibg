<?php
namespace IsGSM;
use DB;
use All;

class Article {

	public static function table_body($articles){
		$result_arr = array();

		$roba_id =  DB::table('roba')->max('roba_id')+1;
		$sifra_k = DB::table('roba')->max('sifra_k')+1;

		foreach($articles as $article) {
			if(!empty($article->SIFRA)){
				$id_is = trim($article->SIFRA);
				$roba_id++;
				$sifra_k++;
				
				$sifra_is = $id_is;
				$sifra_d = !empty($article->KATALOG) ? "'".pg_escape_string(trim($article->KATALOG))."'" : 'NULL';
				$naziv = !empty($article->NAZIV) ? pg_escape_string(Support::convert(substr($article->NAZIV,0,300))) : '';
				// $web_opis = !empty($article->opis) ? pg_escape_string(nl2br(htmlspecialchars(Support::convert($article->opis)))) : '';
				$web_opis = '';
				$barkod = !empty($article->BARCODE) ? Support::convert($article->BARCODE) : '';
				//$proizvodjac_id = -1;
				$proizvodjac_id = !empty($article->BRAND) && trim($article->BRAND) != '' ? Support::getProizvodjacId(Support::convert($article->BRAND)) : -1;
				$vrednost_tarifne_grupe = !empty($article->PDV) && is_numeric($article->PDV) ? $article->PDV : 20;
				$tarifna_grupa_id = Support::getTarifnaGrupaId('Opšta',$vrednost_tarifne_grupe);
				// $tarifna_grupa_id = !empty($article->tarifna_grupa) ? Support::getTarifnaGrupaId($article->tarifna_grupa,$vrednost_tarifne_grupe) : 0;
				$jedinica_mere_id = !empty($article->JM) && $article->JM != '' ? Support::getJedinicaMereId($article->JM) : 1;
				// $grupa_pr_id = !empty($article->PODGRUPA) && $article->PODGRUPA != '' ? Support::getGrupaId($article->PODGRUPA) : -1;
				$grupa_pr_id = Support::getGrupaId('NOVI ARTIKLI');
				// $racunska_cena_nc = !empty($article->cena_nc) && is_numeric(floatval($article->cena_nc)) ? floatval($article->cena_nc) : 0;
				// $web_cena = !empty($article->web_cena) && is_numeric(floatval($article->web_cena)) ? floatval($article->web_cena) : 0;
				// $akcijska_cena = !empty($article->akcijska_cena) && is_numeric(floatval($article->akcijska_cena)) ? floatval($article->akcijska_cena) : 0;
				// $akcija_flag_primeni = !empty($article->akcija_aktivna) && in_array($article->akcija_aktivna,array(1,0)) ? intval($article->akcija_aktivna) : 0;

				$jedinica_mere_id = 1;
				$akcija_flag_primeni = 0;

				$racunska_cena_nc = !empty($article->NABAVNA) && is_numeric(floatval($article->NABAVNA)) ? floatval($article->NABAVNA) : 0;
				$akcijska_cena = 0;
				$akcija_flag_primeni = 0;
				// $mpcena = !empty($article->MPCENA) && is_numeric(floatval($article->mpcena)) ? floatval($article->mpcena) : 0;
				$mpcena = !empty($article->MPCENA) && is_numeric(floatval($article->MPCENA)) ? floatval($article->MPCENA) : 0;
				$web_cena = $mpcena;
				$flag_aktivan = '1';
				$flag_prikazi_u_cenovniku = '0';

				$result_arr[] = "(".strval($roba_id).",".trim($sifra_d).",'".$naziv."',NULL,NULL,NULL,".$grupa_pr_id.",".strval($tarifna_grupa_id).",".strval($jedinica_mere_id).",".strval($proizvodjac_id).",-1,".strval($sifra_k).",NULL,NULL,'".substr($naziv,0,20)."',0,-1,0,0,0,0,9,0,0,0,0,1,".$flag_prikazi_u_cenovniku.",0,NULL,".$flag_aktivan.",".strval($racunska_cena_nc).",0,".strval($racunska_cena_nc).",0,NULL,".strval($mpcena).",false,0,(NULL)::integer,'".$naziv."',1,NULL,NULL,(NULL)::integer,(NULL)::integer,0,0,0,-1,-1,".strval($web_cena).",1,0,'".$web_opis."',NULL,NULL,0,".strval($akcija_flag_primeni).",0,NULL,NULL,NULL,NULL,1,0,'".$barkod."',0,0,1,1,-1,NULL,NULL,NULL,NULL,0,0.00,0.00,".strval($akcijska_cena).",0,NULL,(NULL)::date,(NULL)::date,(NULL)::integer,NULL,'".strval($sifra_is)."','".strval($id_is)."',0)";
			}
		}

		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}

		$all_columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$columns = $all_columns;
		$columns_without_id = $all_columns;
		unset($columns_without_id[0]);
		
		$table_temp = "(VALUES ".$table_temp_body.") roba_temp(".implode(',',$columns).")";

		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
		
		// update
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="roba_id" && $col!="sifra_d" && $col!="sifra_is" && $col!="id_is"){
		    	$updated_columns[] = "".$col." = roba_temp.".$col."";
			}
		}

		DB::statement("UPDATE roba t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.id_is=roba_temp.id_is::varchar AND t.id_is IS NOT NULL AND t.id_is <> ''");	
		//insert
		DB::statement("INSERT INTO roba(".implode(',',$columns_without_id).") SELECT ".implode(',',$columns_without_id)." FROM ".$table_temp." WHERE roba_temp.id_is NOT IN (SELECT id_is FROM roba WHERE id_is IS NOT NULL AND id_is <> '')");


		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");

		DB::statement("SELECT setval('roba_roba_id_seq', (SELECT MAX(roba_id) FROM roba) + 1, FALSE)");
	}

	public static function query_update_unexists($table_temp_body) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$table_temp = "(VALUES ".$table_temp_body.") roba_temp(".implode(',',$columns).")";

		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		DB::statement("UPDATE roba t SET flag_aktivan = 0 WHERE t.id_is IS NOT NULL AND NOT EXISTS(SELECT * FROM ".$table_temp." WHERE t.id_is=roba_temp.id_is::varchar)");
		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
	}

}
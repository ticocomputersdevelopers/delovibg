<?php

class Stockroom extends Eloquent
{
    protected $table = 'orgj';

    protected $primaryKey = 'orgj_id';
    
    public $timestamps = false;

	protected $hidden = array(
            'orgj_id',
            'preduzece_id',
            'naziv',
            'sifra',
            'parent_orgj_id',
            'level',
            'adresa',
            'mesto_id',
            'telefon',
            'fax',
            'ukupno_radnika',
            'tip_magacina_id',
            'flag_vazi',
            'sef_imenik_id',
            'cena_rada',
            'cena_norma_cas',
            'flag_magacin',
            'pdv_orgj_id',
            'farbarski',
            'flag_licenca',
            'licenca_broj',
            'licenca_datum_isteka',
            'tip_orgj_id',
            'flag_knjizi',
            'master_orgj',
            'odgovorno_lice',
            'master_orgj_id',
            'flag_view',
            'platna_grupa_id',
            'b2c',
            'b2b'
    	);

    public function getIdAttribute()
    {
        return isset($this->attributes['orgj_id']) ? $this->attributes['orgj_id'] : null;
    }
    public function getNameAttribute()
    {
        return isset($this->attributes['orgj_id']) ? ($this->attributes['orgj_id']==1 ? 'RetailWarehouse' : ($this->attributes['orgj_id']==4 ? 'WholesalelWarehouse' : null)) : null;
    }

	protected $appends = array(
        'id',
    	'name'
    	);

    public function stocks(){
        return $this->hasMany('Stock', 'orgj_id');
    }

}
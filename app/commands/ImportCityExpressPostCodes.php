<?php
require __DIR__.'/../../vendor/autoload.php';

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

// use PHPExcel; 
// use PHPExcel_IOFactory;

class ImportCityExpressPostCodes extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'add:citycode';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{	
		$posta_slanje=DB::table('posta_slanje')->where('posta_slanje_id',2)->first();
		if($posta_slanje->api_aktivna == 0 || $posta_slanje->difolt == 0 ){
			die;
		}
		$items=file_get_contents('http://webapi.cityexpress.rs/api/data/GetAllPickupAndDeliveryIntervals?ApiKey='.$posta_slanje->auth_code);
		$items = json_decode($items);

		$data_body = '';
		foreach($items->Intervals as $town) {
			$townId = 'NULL';
			$cityId = 'NULL';			
			$name = $town->Name;
			$ptt = $town->PostalCode;

			$data_body .= "(".$townId.",".$cityId.",'".$name."','".$ptt."',".$posta_slanje->posta_slanje_id."),";
		}
		if($data_body != ''){
		    DB::statement("INSERT INTO narudzbina_mesto (code,narudzbina_opstina_code,naziv,ptt,posta_slanje_id) SELECT * FROM (VALUES ".substr($data_body,0,-1).")
			redovi(code,narudzbina_opstina_code,naziv,ptt,posta_slanje_id)
		    ");
		}
		
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
		// 	array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
		// 	array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}

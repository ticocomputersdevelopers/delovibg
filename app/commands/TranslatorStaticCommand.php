<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Service\TranslatorService;


class TranslatorStaticCommand extends Command {
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'static:translate';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Translate.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	

	public function fire()
	{
		$izabraniJezik = DB::table('jezik')->where(array('aktivan'=>1,'izabrani'=>1))->first();
		$jezik = DB::table('jezik')->where(array('kod'=>'sr'))->first();

		$column = null;
		if($izabraniJezik->kod == 'en'){
			$column = 'C';
		}else if($izabraniJezik->kod == 'de'){
			$column = 'D';
		}else if($izabraniJezik->kod == 'cir'){
			$column = 'A';
		}else{
			$this->info('Please choose language.');
			die;
		}
		$translator = new TranslatorService($jezik->kod,$izabraniJezik->kod);

		$products_file = 'files/langs/Prevod.xlsx';
		PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
        $excelObj = $excelReader->load($products_file);
        $worksheet = $excelObj->getSheet(0);
        $lastRow = $worksheet->getHighestRow();

		//static
		for ($row = 2; $row <= $lastRow; $row++) {
			$check = trim($worksheet->getCell('A'.$row)->getValue());
			$srWord = trim($worksheet->getCell('B'.$row)->getValue());
			$defaultWord = $izabraniJezik->kod == 'cir' ? $translator->translate($srWord) : trim($worksheet->getCell($column.$row)->getValue());
			if ($check == 'f'){		
				if(trim($srWord) && trim($srWord) != ''){
					if(DB::table('prevodilac')->where(array('izabrani_jezik_id'=>$izabraniJezik->jezik_id,'jezik_id'=>$jezik->jezik_id,'reci'=>trim($srWord)))->count() == 0){
						DB::table('prevodilac')->insert(array('izabrani_jezik_id'=>$izabraniJezik->jezik_id,'jezik_id'=>$jezik->jezik_id,'izabrani_jezik_reci'=>trim($defaultWord),'reci'=>trim($srWord),'is_js'=>1));
					}
				}
			}			
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			// array('langs', InputArgument::OPTIONAL, 'Languages.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
		// 	array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
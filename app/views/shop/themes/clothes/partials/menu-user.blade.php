@if(Session::has('b2c_kupac') AND $web_kupac = DB::table('web_kupac')->where('web_kupac_id',Session::get('b2c_kupac'))->first())
<h2><span class="section-title text-center">{{ Language::trans('Lični podaci') }}</span></h2>

<div class="user-info-form clearfix">
	<div id="JSUserAccountForm" action="{{ Options::base_url()}}korisnik-edit" method="post" class="login-form" autocomplete="off">
		<!-- PERSONAL INFO CHANGE -->
		<div class="user-section flex">
		@if($web_kupac->flag_vrsta_kupca == 0)
			<div class="form-group">
				<div><label for="name">{{ Language::trans('Ime') }}</label></div>
				<input name="ime" type="text" value="{{ $web_kupac->ime }}">
				<div id="JSImeError" class="error red-dot-error"></div>
			</div>

		<!-- COMPANY INFO CHANGE -->
			@elseif($web_kupac->flag_vrsta_kupca == 1) 
				<div class="form-group">
					<div><label for="company-name">{{ Language::trans('Naziv firme') }}</label></div>
					<input name="naziv" type="text" value="{{ $web_kupac->naziv }}">
					<div id="JSNazivError" class="error red-dot-error"></div>
				</div>
				
				<div class="form-group">
					<div><label for="pib">{{ Language::trans('PIB') }}</label></div>
					<input name="pib" type="text" value="{{ $web_kupac->pib }}">
					<div id="JSPibError" class="error red-dot-error"></div>
				</div> 
			@endif
	 		<div class="form-group">
				<div><label for="lozinka_user">{{ Language::trans('Lozinka') }}</label></div>
				<input name="lozinka" type="password" value="{{ base64_decode($web_kupac->lozinka) }}">
				<div id="JSLozinkaError" class="error red-dot-error"></div>
			</div>
			<div class="form-group">
				<div><label for="e-mail">{{ Language::trans('E-mail') }}</label></div>
				<input name="email" type="text" value="{{ $web_kupac->email }}">
				<div id="JSEmailError" class="error red-dot-error"></div>
			</div>
			
			<div class="form-group">
				<div><label for="telefon">{{ Language::trans('Telefon') }}</label></div>
				<input name="telefon" type="text" value="{{ $web_kupac->telefon }}">
				<div id="JSTelefonError" class="error red-dot-error"></div>
			</div>	
 
			<div class="form-group">
				<div><label for="address">{{ Language::trans('Adresa') }}</label></div>
				<input name="adresa" type="text" value="{{ $web_kupac->adresa }}">
				<div id="JSAdresaError" class="error red-dot-error"></div>
			</div>
			<div class="form-group">
				<div><label for="mesto">{{ Language::trans('Mesto') }}</label></div>
				<input name="mesto" type="text" value="{{ $web_kupac->mesto }}">
				<div id="JSMestoError" class="error red-dot-error"></div>
			</div>
		</div>
 
		<div class="text-center">  
			<input type="hidden" name="web_kupac_id" value="{{ $web_kupac->web_kupac_id }}" />
			<input type="hidden" name="flag_vrsta_kupca" value="{{ $web_kupac->flag_vrsta_kupca }}" />
			<button type="submit" id="JSUserAccountFormSubmit" class="user-changes-btn no-padding">{{ Language::trans('SAČUVAJ IZMENE') }}</button> 
		</div>
	</div>
	@if(Session::get('message'))

	<div class="row"> 
		<div class="success bg-success col-md-12 col-sm-12 col-xs-12">{{ Language::trans('Vaši podaci su uspešno sačuvani') }}!</div>
	</div>
	@endif
</div>
@endif
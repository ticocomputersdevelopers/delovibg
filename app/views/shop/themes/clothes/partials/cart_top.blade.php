 
<div class="header-cart-container inline-block relative">  
	
	<a class="header-cart text-center flex header-seperate" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('korpa') }}">
		<span class="inline-block bg-sprite sprite-cart"></span>
		<span class="menu-ico-txt flex">{{ Language::trans('Korpa') }}</span>
	
		<span class="JScart_num"> {{ Cart::broj_cart() }} </span> 

		<input type="hidden" id="h_br_c" value="{{ Cart::broj_cart() }}" />	
	</a>

	<div class="JSheader-cart-content hidden-sm hidden-xs text-left">
		@include('shop/themes/'.Support::theme_path().'partials/mini_cart_list') 
	</div>
</div> 
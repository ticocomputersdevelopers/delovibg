<div class="row">    
    <div class="quick-view-wrapper col-md-5 col-sm-6 col-xs-12 JSproduct-preview-image">
        <div class="row"> 
            <div class="col-md-12 col-sm-12 col-xs-12 disableZoomer relative product-main-image-wrap no-padding"> 

                <a class="fancybox" href="{{ Options::domain().$image }}">

                    @if(!empty(Product::design_id($roba_id)) AND Options::pitchprint_aktiv() == 1)

                        <span id="zoom_03" class="JSzoom_03" style="display: block;" data-zoom-image="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_1.jpg?k=0.4491390433183766">
                            
                            <img src="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_1.jpg?k=0.4491390433183766" id="art-img" class="JSzoom_03 img-responsive" alt="{{ Product::seo_title($roba_id)}}" />
                        
                        </span>

                    @else
                        <span id="zoom_03" class="JSzoom_03" style="display: block;" data-zoom-image="{{ Options::domain().$image }}">
                           
                            <img class="JSzoom_03 img-responsive" id="art-img" src="{{ Options::domain().$image }}" alt="{{ Product::seo_title($roba_id)}}" />

                        </span>
                    @endif
                </a>

                <div class="product-sticker flex">
                    @if( B2bArticle::stiker_levo($roba_id) != null )
                        <a class="article-sticker-img">
                            <img class="img-responsive" src="{{ Options::domain() }}{{B2bArticle::stiker_levo($roba_id) }}"  />
                        </a>
                    @endif 
                    
                    @if( B2bArticle::stiker_desno($roba_id) != null )
                        <a class="article-sticker-img clearfix">
                            <img class="img-responsive pull-right" src="{{ Options::domain() }}{{B2bArticle::stiker_desno($roba_id) }}"  />
                        </a>
                    @endif   
                </div>
            </div> 

            <div id="gallery_01" class="col-md-12 col-sm-12 col-xs-12 text-center sm-no-padd hidden">

                @if(!empty(Product::design_id($roba_id)) AND Options::pitchprint_aktiv() == 1)                             
                    @if(Product::pitchPrintImageExist($roba_id) == TRUE)                          
                        <a class="elevatezoom-gallery JSp_print_small_img" 
                        href="javascript:void(0)" 
                        data-image="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_2.jpg?k=0.4491390433183766" 
                        data-zoom-image="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_2.jpg?k=0.4491390433183766">
                       
                        <img src="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_2.jpg?k=0.4491390433183766" 
                        alt="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_2.jpg?k=0.4491390433183766"/> 

                        </a>
                    @endif   
                        <a class="elevatezoom-gallery JSp_print_small_img" 
                        href="javascript:void(0)" 
                        data-image="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_1.jpg?k=0.4491390433183766" 
                        data-zoom-image="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_1.jpg?k=0.4491390433183766">
                       
                        <img src="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_1.jpg?k=0.4491390433183766" 
                        alt="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_1.jpg?k=0.4491390433183766"/> 

                    </a>   

                @else
                    <div class="JSgallery_slick row">
                        @foreach(Product::get_list_images($roba_id) as $image)
                        <div class="col-md-3 col-sm-3 col-xs-3 gallery-slick-card">
                            <a class="elevatezoom-gallery" href="javascript:void(0)" data-image="/{{$image->putanja}}" data-zoom-image="{{ Options::domain().$image->putanja }}">

                                <img alt="{{ Options::domain().$image->putanja }}" id="{{ Options::domain().$image->web_slika_id }}" src="{{ Options::domain().$image->putanja }}"/>

                            </a>
                        </div>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    </div>



    <div class="col-md-7 col-sm-6 col-xs-12 product-preview-info">
        <h1 class="article-heading">{{ Product::seo_title($roba_id) }}</h1>
        
        <div>
            <!-- ARTICLE PASSWORD -->
            @if(AdminOptions::sifra_view_web()==1)
            <span class="article-code">{{Language::trans('Šifra') }}: {{Product::sifra($roba_id)}}</span>
            @elseif(AdminOptions::sifra_view_web()==4)                       
            <span class="article-code">{{Language::trans('Šifra') }}: {{Product::sifra_d($roba_id)}}</span>
            @elseif(AdminOptions::sifra_view_web()==3)                       
            <span class="article-code">{{Language::trans('Šifra') }}: {{Product::sku($roba_id)}}</span>
            @elseif(AdminOptions::sifra_view_web()==2)                       
            <span class="article-code">{{Language::trans('Šifra') }}: {{Product::sifra_is($roba_id)}}</span>
            @endif 
        </div>

        <!-- PRICE -->

        <div class="flex">
         <!--    <div class="modal-amount flex">
                <button onClick="var result = document.getElementById('productAmount'); var productAmount = result.value; if( !isNaN( productAmount ) &amp;&amp; productAmount &gt; 1 ) result.value--;return false;" class="product-count" type="button"><i class="fa fa-minus"></i></button>
               
                <input id="productAmount" type="text" name="kolicina" class="cart-amount" value="{{ Input::old('kolicina') ? Input::old('kolicina') : '1' }}">

                <button onClick="var result = document.getElementById('productAmount'); var productAmount = result.value; if( !isNaN( productAmount )) result.value++;return false;" class="product-count" type="button"><i class="fa fa-plus"></i></button>
            </div>
 -->
            <div class="product-preview-price">
                @if(Product::getStatusArticlePrice($roba_id) == 1)

    <!--                         @if(Product::pakovanje($roba_id)) 
                    <div> 
                        <span class="price-label">{{ Language::trans('Pakovanje') }}:</span>
                        <span class="price-num">{{ Product::ambalaza($roba_id) }}</span> 
                    </div>
                    @endif  -->                                

                    @if(All::provera_akcija($roba_id))    

                    <div class="inline-block"> 
                        <span class="price-label hidden">{{ Language::trans('Akcijska cena')}}:</span> 
                        <span class="JSaction_price price-num main-price" data-action_price="{{Product::get_price($roba_id)}}">
                            {{ Cart::cena(Product::get_price($roba_id,true,false,(!is_null(Input::old('kamata')) ? Input::old('kamata') : 0 ))) }}
                        </span>
                    </div>

                    @if(Product::get_mpcena($roba_id) != 0) 
                    <div class="inline-block">
                        <span class="price-label hidden">{{ Language::trans('Maloprodajna cena') }}:</span>
                        <span class="price-num mp-price product-old-price">{{ Cart::cena(Product::get_mpcena($roba_id)) }}</span> 
                    </div>
                    @endif  


                    @if(Product::getPopust_akc($roba_id)>0)
                    <div class="hidden"> 
                        <span class="price-label hidden">{{ Language::trans('Popust') }}: </span>
                        <span class="price-num discount">{{ Cart::cena(Product::getPopust_akc($roba_id)) }}</span>
                    </div>
                    @endif

                    @else

                    @if(Product::get_mpcena($roba_id) != 0) 
                    <div class="inline-block hidden"> 
                        <span class="price-label hidden">{{ Language::trans('Maloprodajna cena') }}:</span>
                        <span class="price-num mp-price">{{ Cart::cena(Product::get_mpcena($roba_id)) }}</span> 
                    </div>
                    @endif

                    <div class="inline-block"> 
                        <span class="price-label hidden">{{ Language::trans('WebCena') }}:</span>
                        <span class="JSweb_price price-num main-price" data-cena="{{Product::get_price($roba_id)}}">
                           {{ Cart::cena(Product::get_price($roba_id,true,false,(!is_null(Input::old('kamata')) ? Input::old('kamata') : 0 ))) }}
                       </span>
                   </div>

                   @if(Product::getPopust($roba_id)>0)
                       @if(AdminOptions::web_options(132)==1)
                       <div class="hidden">  
                        <span class="price-label hidden">{{ Language::trans('Popust') }}:</span>
                        <span class="price-num discount">{{ Cart::cena(Product::getPopust($roba_id)) }}</span>
                        </div>
                        @endif
                    @endif

                @endif
            @endif 
            </div>
        </div>

        <div class="modal-msg">
            Poštovani, ovaj proizvod nemamo trenutno na stanju. Ukoliko želite da poručite zahtevanu količinu, molimo vas da nas kontaktirate putem telefona na broj <a class="bold" href="tel:{{ Options::company_phone() }}">{{ Options::company_phone() }}</a> ili putem forme ispod.
        </div>

        <form method="POST" action="{{ Options::base_url() }}enquiry-message-send">
            <input type="hidden" id="JSStateArtID" name="roba_id" value="">
           
            <div class="flex enqury-field">
                <label id="label_name" class="labelFocus">{{ Language::trans('Vaše ime') }} *</label>
                <input class="contact-name" name="contact-name" id="JScontact_name"  type="text" value="{{ Input::old('contact-name') }}">
                <!-- <div class="error red-dot-error">{{ $errors->first('contact-name') ? $errors->first('contact-name') : "" }}</div> -->
            </div> 

            <div class="flex enqury-field">
                <label id="label_email" class="labelFocus">{{ Language::trans('Vaša e-mail adresa') }} *</label>
                <input class="contact-email" name="contact-email" id="JScontact_email"  type="text" value="{{ Input::old('contact-email') }}" >
                <!-- <div class="error red-dot-error">{{ $errors->first('contact-email') ? $errors->first('contact-email') : "" }}</div> -->
            </div>     

            <div class="flex enqury-field">
                <label id="label_phone" class="labelFocus">{{ Language::trans('Telefon') }} *</label>
                <input class="contact-phone" name="contact-phone" id="JScontact_phone" type="text" value="{{ Input::old('contact-phone') }}" >
                <!-- <div class="error red-dot-error">{{ $errors->first('contact-phone') ? $errors->first('contact-phone') : "" }}</div> -->
            </div>  

            <div class="flex enqury-field">
                <label id="label_phone" class="labelFocus">{{ Language::trans('Količina') }} *</label>
                <input class="contact-message" name="contact-phone" id="contact-message" type="text" value="{{ Input::old('contact-message') }}" >
                <!-- <div class="error red-dot-error">{{ $errors->first('contact-phone') ? $errors->first('contact-phone') : "" }}</div> -->
            </div>  

            <!-- <div class="flex enqury-field">   
                <label id="label_message" class="labelFocus">{{ Language::trans('Vaša poruka') }} </label>
                <textarea class="contact-message" name="contact-message" rows="5" id="message">{{ Input::old('contact-message') }}</textarea>
            </div>  -->

            <button type="submit" class="button bg-button">{{ Language::trans('Pošalji') }}</button>
        </form>

    </div>
</div>

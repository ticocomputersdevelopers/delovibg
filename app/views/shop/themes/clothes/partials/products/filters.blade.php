<?php // echo $kara; ?>
<div class="filters">
<!-- CHECKED FILTERS -->
   		<div>
   			<br>

		    <div class="clearfix JShidden-if-no-filters">
		    	<span>{{ Language::trans('Izabrani filteri') }}:</span>
		    	<a class="JSreset-filters-button inline-block pull-right" role="button" href="javascript:void(0)" rel=”nofollow”>{{ Language::trans('Poništi filtere') }} <span class="fas fa-times"></span></a>
		    </div> 
			
			<ul class="selected-filters">
				<?php $br=0;
				foreach($niz_proiz as $row){
					$br+=1;
					if($row>0){
				?>
					<li>
					    {{All::get_manofacture_name($row)}}     
					    <a href="javascript:void(0)" rel=”nofollow”>
					        <span class="JSfilter-close fas fa-times" data-type="1" data-rb="{{$br}}" data-element="{{$row}}"></span>
					    </a>
					</li>
				<?php }}

				$br=0;
				foreach($niz_karakteristike as $row){
					$br+=1;
					if($row>0){
				?>
					<li>
						{{All::get_fitures_name($row)}}
						<a href="javascript:void(0)" rel=”nofollow”>
					        <span class="JSfilter-close fas fa-times" data-type="2" data-rb="{{$br}}" data-element="{{$row}}"></span>
					    </a>
					</li>
					<?php }}?>
			</ul>
			<input type="hidden" id="JSmanufacturer_ids" value="{{$proizvodjac_ids}}"/>
			<input type="hidden" id="JSmanufacturer_attr" value="{{$kara}}"/>
		</div>	

	  
	<!-- END CHECKED -->

	<!-- SLAJDER ZA CENU -->
	<div class="price-slider-wrap"> 
		<span class="filter-type-label">{{ (Language::trans('Cena')) }}<br><br></span>
		<div id="JSslider-range"></div><br>
		<span id="JSamount" class="filter-price hidden"></span>
		
		<div class="row">
			<div class="col-xs-6 no-padding">
				<span id="JSfilter-minprice"></span> <span class="text-uppercase">rsd</span>
			</div>
			<div class="col-xs-6 no-padding text-right">
				<span id="JSfilter-maxprice"></span> <span class="text-uppercase">rsd</span>
			</div>
		</div>

	</div>
	
	<script>
		var max_web_cena_init = {{ $max_web_cena }};
		var min_web_cena_init = {{ $min_web_cena }};
		var max_web_cena = {{ $cene!='0-0' ? explode('-',$cene)[1] : $max_web_cena }};
		var min_web_cena = {{ $cene!='0-0' ? explode('-',$cene)[0] : $min_web_cena }};
		var kurs = {{ Session::has('valuta') ? (Session::get('valuta')!="2" ? 1 : Options::kurs()) : 1 }};
	</script>

	  		<ul>
	  			<br>

			    <!-- FILTER ITEM -->
			    @if(count($manufacturers)>0)
				<li class="filter-box">
					<span class="filter-type-label">{{ (Language::trans('Proizvođač')) }}</span>
                    <a href="javascript:void(0)" class="filter-links JSfilters-slide-toggle" rel=”nofollow”>
                    <span class="filter-choice-wrap">
                    	@if(count($niz_proiz) > 0 AND !in_array(0,$niz_proiz))
                			@foreach($manufacturers as $key => $value) 
                    			@if(in_array($key,$niz_proiz))  
	                    			{{ All::get_manofacture_name($key).' ' }}
                    			@endif
        					@endforeach
                    	@else
                    		{{ Language::trans('Bilo koji') }}
                    	@endif

                    	<!-- <span class="choosed_filter">@foreach($manufacturers as $key => $value) @if(in_array($key,$niz_proiz)) {{ All::get_manofacture_name($key).' ' }} @endif @endforeach </span> -->
                	<!-- <i class="fas fa-plus"></i> -->
                	</span>
                	<img class="JSfilter-arrow" src="{{Options::base_url()}}images/bg-arrow-right.png">
                	</a>
			 	 	
			 	 	<a class="JSfilters-slide-toggle-content">
						@foreach($manufacturers as $key => $value)
						@if(in_array($key,$niz_proiz))
							<label>
								<input name="proizvodjac[]" onchange="filters_send()" value="{{ $key }}" type="checkbox" checked>
								<span class="filter-text">
									{{All::get_manofacture_name($key)}} 
								</span>
								<span class="filter-count">
									@if(Options::filters_type()) {{ $value }} @endif
								</span>
							</label>
						@else
							<label>
								<input name="proizvodjac[]" onchange="filters_send()" value="{{ $key }}" type="checkbox">
								<span class="filter-text">
									{{All::get_manofacture_name($key)}} 
								</span>
								<span class="filter-count">
									@if(Options::filters_type()) {{ $value }} @endif
								</span>
							</label>
						@endif
						@endforeach
					</a>				 				 
				</li>
				@endif

				@foreach($characteristics as $keys => $values)
				<li class="filter-box">
					<span class="filter-type-label">{{ Language::trans($keys) }}</span>
                    <a href="javascript:void(0)" class="filter-links JSfilters-slide-toggle" rel=”nofollow”>
                    <span class="filter-choice-wrap">
                    	<?php $checkCharact = false; ?>
                    	@if(count($niz_karakteristike) > 0 AND !in_array(0,$niz_karakteristike))
                			@foreach($values as $key => $value) 
                    			@if(in_array($key,$niz_karakteristike))
                    				<?php $checkCharact = true; ?>
	                    			{{ All::get_fitures_name($key).' ' }}
                    			@endif
        					@endforeach
        					@if(!$checkCharact)
        						{{ Language::trans('Bilo koja') }}
        					@endif
                    	@else
                    		{{ Language::trans('Bilo koja') }}
                    	@endif
                	</span>
                	<img class="JSfilter-arrow" src="../images/bg-arrow-right.png">
                	</a>

					<div class="JSfilters-slide-toggle-content">
						@foreach($values as $key => $value)
						<?php if(in_array($key, $niz_karakteristike)){ ?>
						<label>
							<input type="checkbox" onchange="filters_send()" name="vrednost[]" value="{{ $key }}" checked>
							<span class="filter-text">
								{{All::get_fitures_name($key)}}
							</span>
							<span class="filter-count">
								@if(Options::filters_type()) {{ $value }} @endif
							</span>
						</label>
						<?php }else {?>
						<label>
							<input type="checkbox" onchange="filters_send()" name="vrednost[]" value="{{ $key }}"> 
							<span class="filter-text">
								{{All::get_fitures_name($key)}}
							</span>
							<span class="filter-count">
								@if(Options::filters_type()) {{ $value }} @endif
							</span>
						</label>
						<?php } ?>
						@endforeach
					</div>
				</li>
				@endforeach		

					
            </ul>	 
		<input type="hidden" id="JSfilters-url" value="{{$url}}" />
 		
</div>

 
 
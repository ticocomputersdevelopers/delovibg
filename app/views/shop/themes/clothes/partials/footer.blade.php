<!-- FOOTER.blade -->
 <footer {{ Options::web_options(322, 'str_data') != '' ? 'style=background-color:' . Options::web_options(322, 'str_data') : '' }}>
    <div class="container{{(Options::web_options(322)==1) ? '-fluid' : '' }}">
        <div class="row JSfooter-cols">  
            @foreach(All::footer_sections() as $footer_section)
            @if($footer_section->naziv == 'slika')
            <div class="col-md-8 col-sm-8 col-xs-12 no-padding">
                <a class="logo inline-block" href="{{ B2bOptions::base_url()}}b2b" title="{{B2bOptions::company_name()}}">
                    <img class="img-responsive md-margin" src="{{ B2bOptions::base_url()}}images/bg-logo - black.png" alt="{{B2bOptions::company_name()}}" />
                </a>
            </div>
            <!-- <div class="col-md-4 col-sm-4 col-xs-12 no-padding">
                <div class="w-50">        
                @if(!is_null($footer_section->slika))
                <a href="{{ $footer_section->link }}" rel="nofollow">
                    <img class="footer-logo img-responsive" src="{{ Options::domain() }}{{ $footer_section->slika }}" alt="{{Options::company_name()}}" />
                </a>
                @else
                <a href="/" title="{{Options::company_name()}}" rel="nofollow">
                    <img class="footer-logo img-responsive" src="{{ Options::domain() }}{{Options::company_logo()}}" alt="{{Options::company_name()}}" />
                </a>
                @endif

                <div class="JSInlineFull" data-target='{"action":"footer_section_content","id":"{{$footer_section->futer_sekcija_id}}"}'>
                    {{ $footer_section->sadrzaj }} 
                </div> 
                </div>
            </div> -->
            @elseif($footer_section->naziv == 'text')
            <div class="col-md-4 col-sm-4 col-xs-12 no-padding">
                <div class="footer-text-width"> 
                <!-- <h5 class="ft-section-title JSInlineShort" data-target='{"action":"footer_section_label","id":"{{$footer_section->futer_sekcija_id}}"}'>
                    {{ $footer_section->naslov }}
                </h5> -->

                <div class="JSInlineFull" data-target='{"action":"footer_section_content","id":"{{$footer_section->futer_sekcija_id}}"}'>
                    {{ $footer_section->sadrzaj }} 
                </div> 
                </div>
            </div>
            @elseif($footer_section->naziv == 'linkovi')
            <div class="col-md-4 col-sm-4 col-xs-12 no-padding">
                <div class="w-50 bg-footer-links relative"> 
                <!-- <h5 class="ft-section-title JSInlineShort" data-target='{"action":"footer_section_label","id":"{{$footer_section->futer_sekcija_id}}"}'>
                    {{ $footer_section->naslov }}
                </h5> -->

                <ul class="footer-links">
                    @foreach(All::footer_section_pages($footer_section->futer_sekcija_id) as $page)
                    <li>
                        <a href="{{ Options::base_url().Url_mod::page_slug($page->naziv_stranice)->slug }}">{{ Url_mod::page_slug($page->naziv_stranice)->naziv }}                            
                            <span class="fas fa-caret-right links-right">&nbsp;</span>                           
                        </a>
                    </li>
                    @endforeach
                </ul>  
                </div>
            </div>
            @elseif($footer_section->naziv == 'kontakt')
            <!-- <div class="col-md-8 col-sm-12 col-xs-12 no-padding md-center">
                <a class="logo inline-block" href="{{ B2bOptions::base_url()}}b2b" title="{{B2bOptions::company_name()}}">
                    <img class="img-responsive md-margin" src="{{ B2bOptions::base_url()}}images/bg-logo - black.png" alt="{{B2bOptions::company_name()}}" />
                </a>
            </div> -->
            <div class="col-md-4 col-sm-4 col-xs-12 no-padding">
                <div class="w-50"> 
                <!-- <h5 class="ft-section-title JSInlineShort" data-target='{"action":"footer_section_label","id":"{{$footer_section->futer_sekcija_id}}"}'>
                    {{ $footer_section->naslov }}
                </h5> -->

                <ul class="sm-flex footer-contact">
                    @if(Options::company_phone())
                    <li class="relative">
                        <i class="hidden bg-footer-icons fas fa-phone-alt"></i>
                        <a class="flex mailto" href="tel:{{ Options::company_phone() }}">
                            <span class="inline-block bg-sprite sprite-black sprite-phone"></span>
                            <span class="bg-footer-contact-text">{{ Options::company_phone() }}</span>
                        </a>
                    </li>                          
                    @endif

                    @if(Options::company_email())
                    <li class="relative">
                        <i class="hidden bg-footer-icons far fa-envelope"></i>
                        <a class="flex mailto" href="mailto:{{ Options::company_email() }}">
                            <span class="inline-block bg-sprite sprite-black sprite-mail"></span>
                            <span class="bg-footer-contact-text">{{ Options::company_email() }}</span>
                        </a>
                    </li>
                    @endif

                    @if(Options::company_adress() OR Options::company_city())
                    <li class="relative flex" id="bg-foter-adress"> 
                        <span class="inline-block bg-sprite sprite-black sprite-map"></span>
                        <i id="bg-footer-adress-icon" class="hidden bg-footer-icons fas fa-map-marked-alt" style=""></i>
                        <span class="bg-footer-contact-text">{{ Options::company_adress() }}<br>{{ Options::company_city() }}</span>
                    </li>
                    @endif

                    
                    <li class="relative">
                        <a class="flex" href="https://www.instagram.com/bg.elektronik/">
                            <span class="inline-block bg-sprite sprite-black sprite-instagram"></span>
                            <i class="hidden bg-footer-icons fab fa-instagram"></i>
                            <span class="bg-footer-contact-text">bg.elektronik</span>
                        </a>
                    </li>
                </ul>
                </div>
            </div>
            @elseif($footer_section->naziv == 'drustvene_mreze')
            <div class="col-md-4 col-sm-4 col-xs-12 no-padding">
                <div class="w-50"> 
                <!-- <h5 class="ft-section-title JSInlineShort" data-target='{"action":"footer_section_label","id":"{{$footer_section->futer_sekcija_id}}"}'>
                    {{ $footer_section->naslov }}
                </h5> -->
                <div class="social-icons">
                    {{Options::social_icon()}}
                </div>
                </div>  
            </div>  
            @elseif($footer_section->naziv == 'mapa' AND Options::company_map() != '' AND Options::company_map() != ';')
            <div class="col-md-4 col-sm-4 col-xs-12 no-padding">
                    <div class="col-md-6">
                <iframe src="https://maps.google.com/maps?q={{ All::lat_long()[0] }},{{ All::lat_long()[1] }}&output=embed" width="100%" height="100%" frameborder="0" aria-hidden="false" tabindex="0"></iframe>
            </div>                  
            @endif
            @endforeach 

            <?php $newslatter_description = All::newslatter_description(); ?>
            @if(Options::newsletter()==1)
            <div class="col-md-4 col-sm-4 col-xs-12 no-padding">
                    <div class="col-md-6">
                <h5 class="ft-section-title JSInlineShort" data-target='{"action":"newslatter_label"}'>
                    {{ $newslatter_description->naslov }}
                </h5> 
                <!-- <p class="JSInlineFull" data-target='{"action":"newslatter_content"}'>
                    {{ $newslatter_description->sadrzaj }}
                </p> -->

                <div class="newsletter relative">        
                    <input type="text" placeholder="E-mail" id="newsletter" />
             
                    <button onclick="newsletter()" class="button">{{ Language::trans('Prijavi se') }}</button>
                </div>
                </div>
            </div>
            @endif       
        </div> 
    </div>

 <span class="JSscroll-top"><i class="fas fa-angle-up"></i></span>
 
</footer>

<div class="container-fluid JSto-overflow">
    <div class="row"> 
        <div class="text-center foot-note col-xs-12">
         
            <div class="JSInlineFull" data-target='{"action":"front_admin_content","id":4}'>
                {{ Support::front_admin_content(4) }}
            </div>

            <p class="hidden">{{ Options::company_name() }} &copy; {{ date('Y') }}. {{Language::trans('Sva prava zadržana')}}. - 
                <a href="https://www.selltico.com/">{{Language::trans('Izrada internet prodavnice')}}</a> - 
                <a href="https://www.selltico.com/"> Selltico. </a>
            </p>  
            <span class="footer-tm">Produced by Selltico. <br class="hidden-to-xs"> Designed by Šema.</span>
        </div> 
    </div>
</div>

@if(Support::banca_intesa()) 
<div class="after-footer"> 
    <div class="container"> 
        <div class="banks flex {{(empty(Options::gnrl_options(3023,'str_data')) && empty(Options::gnrl_options(3024,'str_data'))) ? 'justify-between' : 'justify-center' }}">
            <ul class="list-inline sm-text-center text-right">
                <li><img alt="bank-logo" class="JSlazy_load" src="{{Options::base_url()}}images/quick_view_loader.gif" data-src="{{ Options::domain() }}images/cards/master-card.png"></li>
                <li><img alt="bank-logo" class="JSlazy_load" src="{{Options::base_url()}}images/quick_view_loader.gif" data-src="{{ Options::domain() }}images/cards/maestro-card.png"></li>
                <li><img alt="bank-logo" class="JSlazy_load" src="{{Options::base_url()}}images/quick_view_loader.gif" data-src="{{ Options::domain() }}images/cards/visa-card.png"></li> 

                @if(!empty(Options::gnrl_options(3023,'str_data')) && !empty(Options::gnrl_options(3024,'str_data')))
                <li>
                    <img alt="bank-logo" class="JSlazy_load" src="{{Options::base_url()}}images/quick_view_loader.gif" data-src="{{ Options::domain() }}images/cards/dinacard.png">
                </li>
                @endif                          
                
                <li><img alt="bank-logo" class="JSlazy_load" src="{{Options::base_url()}}images/quick_view_loader.gif" data-src="{{ Options::domain() }}images/cards/american-express.png"></li>
            </ul>
    
            <ul class="list-inline sm-text-center">
                <li>
                    @if(!empty(Options::gnrl_options(3023,'str_data')) && !empty(Options::gnrl_options(3024,'str_data')))
                    <a href="https://www.bancaintesa.rs" class="banc_main_logo" target="_blank" rel="nofollow">
                        <img alt="bank-logo" class="JSlazy_load" src="{{Options::base_url()}}images/quick_view_loader.gif" data-src="{{ Options::domain() }}images/cards/banca-intesa.png">
                    </a>
                    @else
                    <a href="http://www.e-services.rs/" target="_blank" rel="nofollow">
                        <img alt="bank-logo" class="JSlazy_load" src="{{Options::base_url()}}images/quick_view_loader.gif" data-src="{{ Options::domain() }}images/cards/chipCard.jpg">
                    </a> 
                    @endif
                </li> 
                <li>
                    <a href="https://rs.visa.com/pay-with-visa/security-and-assistance/protected-everywhere.html" target="_blank" rel="nofollow">
                        <img alt="bank-logo" class="JSlazy_load" src="{{Options::base_url()}}images/quick_view_loader.gif" data-src="{{ Options::domain() }}images/cards/verified-by-visa.jpg">
                    </a>
                </li>
                <li>
                    <a href="https://www.mastercard.rs/sr-rs/consumers/find-card-products/credit-cards.html" target="_blank" rel="nofollow">
                        <img alt="bank-logo" class="JSlazy_load" src="{{Options::base_url()}}images/quick_view_loader.gif" data-src="{{ Options::domain() }}images/cards/master-card-secure.gif">
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div> 
@endif  
<!-- FOOTER.blade END -->
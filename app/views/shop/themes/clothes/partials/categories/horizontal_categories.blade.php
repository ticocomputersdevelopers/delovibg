<li class="level-1-list">        
                <a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($grupa) }}" class="">         
                    @if(Groups::check_image_grupa($putanja_slika))
                        <img class="level-1-img hidden-sm hidden-xs" src="{{ Options::domain() }}{{$putanja_slika}}" alt="{{ $grupa }}" />
                    @endif
                    {{ Language::trans($grupa)  }}                
                </a>             
            </li>
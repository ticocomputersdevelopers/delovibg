<?php 
$baner_id=DB::table('tip_artikla')->where('tip_artikla_id',$tip_id)->pluck('baner_id');
$slika = DB::table('slajder_stavka')->where('slajder_id',$baner_id)->get();

?>

<div class="row padding-bg ">
    <div class="col-xs-12 JSlike_slick JStip-slider"> 
        <h2><span class="section-title JSInlineShort">{{Language::trans('Možda će ti se i ovo svideti')}}</span></h2>
        @foreach($slika as $baner)
            <div class="relative banners"> 
                <a href="{{ !empty(Articles::baner_link($baner->slajder_stavka_id)->link) ? Articles::baner_link($baner->slajder_stavka_id)->link : '#' }}">
                    <div class="bg-img" style="background-image: url('{{ Options::domain() }}{{ $baner->image_path }}')"></div>
                </a>
            </div>
        @endforeach
    </div>
<!--     <div class="expand-slider">
        <a href="#">
            <i class="fas fa-chevron-down"></i>
        </a>
    </div> -->
</div>


<div class="manufacturer-categories JSmc-tip hidden"> 
    <h3 class="text-center">{{$title}}</h3>
    <ul>
    @foreach(Support::tip_categories($tip_id) as $key => $value)
    <li>
        <a href="{{ Options::base_url()}}{{ Url_mod::slug_trans('tip') }}/{{ Url_mod::slug_trans($tip) }}/{{ Url_mod::slug_trans($key) }}">
            <span>{{ Language::trans($key) }}</span>
            <span>{{ $value }}</span>
        </a>
    </li>

                    
    @endforeach 
    </ul>
</div> 


<script src="{{Options::domain()}}js/slick.min.js"></script>

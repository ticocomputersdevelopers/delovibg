<svg style="display: none;">
    <style>
  
        table tr td { padding: 6px; border-bottom: 1px solid #ccc; }
        
        table tr th { 
            padding: 6px; 
            font-size: 17px; 
            text-align: left;
        }
        
        .table-title {
            background: #f47c1f;
            color: white; 
        }
        .table-regular {
            width: 100%;
            table-layout: fixed;
            border: 1px solid #ccc;
        } 
        @media screen and (max-width: 768px){
            .table-respons tr td, .table-respons tr th {
                white-space: nowrap;
            }
            table tr th , table tr td { 
                word-break: break-word; 
            }
        }
        .fine-print {
            color: #a9a6a6;
            font-size: 10px;
            margin-top: 3px;
            float: right!important;
        }
    </style>
</svg>

<!-- ************************ -->

<div class="order-page" style="max-width: 1170px; margin: auto; padding: 0 10px; font-size:14px;">
    
    <div class="row text-center" style="margin: auto;">
        
        @if(!is_null($bank_result))
            @if($bank_result->realized == 1)
            <!-- {{ Language::trans('Račun platne kartice je zadužen') }}. -->
            <div class="col-xs-12" style="margin-bottom: 20px;"><img class="img-responsive margin-auto" src="../images/bg-check.png" /></div>
            <div class="col-xs-12" style="font-size: 200%; margin: 20px 0; font-weight: 600;">{{ Language::trans('Hvala ti!') }}</div>
            <div class="col-xs-12" style="font-size: 130%; font-weight: 600;">
                <span style="color: #bbb; font-weight: 600;">{{ Language::trans('Tvoja narudžbina broj') }} <span style="color: #000;">{{Order::broj_dokumenta($web_b2c_narudzbina_id)}}</span>
                {{ Language::trans('je evidentirana') }}. <br> {{ Language::trans('Nakon što porudžbina bude obrađena, stići će Vam mail sa kodom za praćenje pošiljke') }}. <br> {{ Language::trans('Račun platne kartice je zadužen') }}.</span>
            </div>
            @else
            <!-- {{ Language::trans('Plaćanje nije uspešno, račun platne kartice nije zadužen. Najčešći uzrok je pogrešno unet broj kartice, datum isteka ili sigurnosni kod, pokušaje ponovo, u slučaju uzastopnih greški pozovite vašu banku') }}. -->
            <div class="col-xs-12" style="margin-bottom: 20px;"><img class="img-responsive margin-auto" src="../images/bg-ex.png" /></div>
            <div class="col-xs-12" style="font-size: 200%; margin: 20px 0; font-weight: 600;">{{ Language::trans('Hvala ti!') }}</div>
            <div class="col-xs-12" style="font-size: 130%; font-weight: 600;">
                <span style="color: #bbb; font-weight: 600;">{{ Language::trans('Tvoja narudžbina broj') }} <span style="color: #000;">{{Order::broj_dokumenta($web_b2c_narudzbina_id)}}</span>
                {{ Language::trans('je evidentirana') }}. <br> {{ Language::trans('Plaćanje nije uspešno, račun platne kartice nije zadužen. Najčešći uzrok je pogrešno unet broj kartice, datum isteka ili sigurnosni kod, pokušaje ponovo, u slučaju uzastopnih greški pozovite vašu banku') }}.</span>
            </div>
            @endif
        @else
            <div class="col-xs-12" style="margin-bottom: 20px;"><img class="img-responsive margin-auto" src="../images/bg-check.png" /></div>
            <div class="col-xs-12" style="font-size: 200%; margin: 20px 0; font-weight: 600;">{{ Language::trans('Hvala ti!') }}</div>
            <div class="col-xs-12" style="font-size: 130%; font-weight: 600;">
                <span style="color: #bbb; font-weight: 600;">{{ Language::trans('Tvoja narudžbina broj') }} <span style="color: #000;">{{Order::broj_dokumenta($web_b2c_narudzbina_id)}}</span>
                {{ Language::trans('je evidentirana') }}. <br> {{ Language::trans('Nakon što porudžbina bude obrađena, stići će Vam mail sa kodom za praćenje pošiljke') }}.</span>
            </div>
        @endif
        <div class="col-xs-12" style="margin: 20px 0;">
            @if(Session::has('b2c_kupac')) 
                <button style="margin: 15px 0;" class="button bg-button text-uppercase"><a style="display: block; text-align: center; color: #fff;" href="{{Options::base_url()}}">{{ Language::trans('Nastavi kupovinu') }}</a></button>
            
            @else 
                <div class="col-xs-12" style="font-size: 130%; font-weight: 600;">
                    {{ Language::trans('Da li želiš da napraviš nalog') }} <br> {{ Language::trans('sa podacima koje su upravo uneo/la?') }}
                </div>
                <div class="col-xs-12">
                    <button style="margin: 15px 0;" class="button bg-button text-uppercase"><a style="display: block; text-align: center; color: #fff;" href="{{Options::base_url()}}{{ Url_mod::slug_trans('registracija') }}">{{ Language::trans('Napravi nalog') }}</a></button>
                </div>
                <div class="col-xs-12">
                    <button class="button bg-button text-uppercase" style="background: unset; color: black; border: 1px solid black;"><a style="color: black;" href="{{Options::base_url()}}">{{ Language::trans('Nastavi kupovinu') }}</a></button>
                </div>
            
            @endif
        </div>
    </div>


<!-- ************************ -->

    <table class="table-regular">
        <thead>
            <tr>
                <th colspan="2" class="table-title">
                    {{ Language::trans('Informacije o narudžbini') }}:
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{ Language::trans('Broj porudžbine') }}:</td>
                <td>{{Order::broj_dokumenta($web_b2c_narudzbina_id)}}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Datum porudžbine') }}:</td>
                <td>{{Order::datum_porudzbine($web_b2c_narudzbina_id)}}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Način isporuke') }}:</td>
                <td>{{Order::n_i($web_b2c_narudzbina_id)}}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Način plaćanja') }}:</td>
                <td>{{Order::n_p($web_b2c_narudzbina_id)}}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Napomena') }}:</td>
                <td>{{Order::napomena_nar($web_b2c_narudzbina_id)}}</td>
            </tr>
        </tbody> 
    </table> 

    <br>
 <!-- ************************ -->

    <table class="table-regular">
        <thead>
            <tr>
                <th colspan="2" class="table-title">
                    {{ Language::trans('Informacije o kupcu') }}:
                </th>
            </tr>
        </thead>
        <tbody>
            @if($kupac->flag_vrsta_kupca == 0)
            <tr>  
                <td>{{ Language::trans('Ime i Prezime') }}:</td>
                <td>{{ Language::trans_chars($kupac->ime.' '.$kupac->prezime )}}</td>
            </tr>
            @else
            <tr>
                <td>{{ Language::trans('Firma i PIB') }}:</td>
                <td>{{ Language::trans_chars($kupac->naziv).' '.$kupac->pib }}</td>
            </tr>
            <tr> 
                <td>{{ Language::trans('Matični broj') }}:</td>
                <td>{{ Language::trans_chars($kupac->maticni_br) }}</td> 
            </tr>
            @endif
 
            <tr>
                <td>{{ Language::trans('Adresa') }}:</td>
                <td>{{ Language::trans_chars($kupac->adresa) }}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Mesto') }}:</td>
                <td>{{ Language::trans_chars($kupac->mesto) }}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Telefon') }}:</td>
                <td>{{ $kupac->telefon }}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('E-mail') }}:</td>
                <td>{{ $kupac->email }}</td>
            </tr>
        </tbody>
    </table>

    <br>
 <!-- ************************ -->
 
     <table class="table-regular">
        <thead>
            <tr>
                <th colspan="2" class="table-title">
                    {{ Language::trans('Informacije o prodavcu') }}:
                </th>
            </tr>
        </thead>
        <tbody> 
            <tr>  
                <td>{{ Language::trans('Naziv prodavca') }}:</td>
                <td>{{Options::company_name()}}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Adresa') }}:</td>
                <td>{{Options::company_adress()}}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Telefon') }}:</td>
                <td>{{Options::company_phone()}}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Fax') }}:</td>
                <td>{{Options::company_fax()}}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('PIB') }}:</td>
                <td>{{Options::company_pib()}}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Šifra delatnosti') }}:</td>
                <td>{{Options::company_delatnost_sifra()}}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Žiro račun') }}:</td>
                <td>{{Options::company_ziro()}}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('E-mail') }}:</td>
                <td>{{Options::company_email()}}</td>
            </tr>
        </tbody>
    </table>

    <br>

<!-- ************************ -->
 
    @if(!is_null($bank_result))
    <table class="table-regular">
        <thead>
            <tr>
                <th colspan="2" class="table-title">
                    {{ Language::trans('Informacije o transakciji') }}:
                </th>
            </tr>
        </thead>
        <tbody> 
            <tr>  
                <td>{{ Language::trans('Broj narudžbine') }}:</td>
                <td>{{ isset($bank_result->oid) ? $bank_result->oid : '' }}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Autorizacioni kod') }}</td>
                <td>{{ isset($bank_result->auth_code) ? $bank_result->auth_code : '' }}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Status transakcije') }}</td>
                <td>{{ isset($bank_result->response) ? $bank_result->response : '' }}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Kod statusa transakcije') }}</td>
                <td>{{ isset($bank_result->result_code) ? $bank_result->result_code : '' }}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Broj transakcije') }}</td>
                <td>{{ isset($bank_result->trans_id) ? $bank_result->trans_id : '' }}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Datum transakcije') }}</td>
                <td>{{ isset($bank_result->post_date) ? $bank_result->post_date : '' }}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Statusni kod transakcije') }}</td>
                <td>{{ isset($bank_result->md_status) ? $bank_result->md_status : '' }}</td>
            </tr>
        </tbody>
    </table>

    <br>
    @endif 
  
 <!-- ************************ -->
 
     <div style="overflow-x: auto;"> 
         <table class="table-respons" style="width: 100%; border: 1px solid #ccc; border-collapse: initial;">
            <thead>
                <tr>
                    <th colspan="5" class="table-title">
                        {{ Language::trans('Informacije o naručenim proizvodima') }}:
                    </th>
                </tr>
                <tr>
                    @if(AdminOptions::sifra_view()==1)
                        <th>{{ AdminLanguage::transAdmin('Roba_id') }}</th>
                    @elseif(AdminOptions::sifra_view()==2)
                        <th>{{ AdminLanguage::transAdmin('Šifra IS') }}</th>
                    @elseif(AdminOptions::sifra_view()==3)
                        <th>{{ AdminLanguage::transAdmin('SKU') }}</th>
                    @elseif(AdminOptions::sifra_view()==4)
                        <th>{{ AdminLanguage::transAdmin('Šifra Dob') }}</th> 
                    @endif
                    <th>{{ Language::trans('Naziv proizvoda') }}:</th>
                    <th>{{ Language::trans('Cena') }}:</th>
                    <th>{{ Language::trans('Količina') }}:</th>
                    <th>{{ Language::trans('Ukupna cena') }}:</th>
                </tr>
            </thead>
            <tbody> 
                @foreach(DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->join('roba', 'roba.roba_id' , '=', 'web_b2c_narudzbina_stavka.roba_id')->get() as $row)
                <tr>
                    @if(AdminOptions::sifra_view()==1)
                        <td>{{ $row->roba_id}}</td>
                    @elseif(AdminOptions::sifra_view()==2)
                        <td>{{$row->sifra_is }}</td>
                    @elseif(AdminOptions::sifra_view()==3)
                        <td>{{ $row->sku }}</td>
                    @elseif(AdminOptions::sifra_view()==4)
                        <td>{{ $row->sifra_d }}</td>
                    @endif
                    <td>
                        <span style="max-width: 450px; overflow: hidden; display: inline-block;">{{ Product::short_title($row->roba_id) }}</span>
                        {{ Product::getOsobineStr($row->roba_id,$row->osobina_vrednost_ids) }}
                    </td>
                    <td>{{ Cart::cena($row->jm_cena) }}</td>
                    <td>{{ (int)$row->kolicina }}</td>
                    <td>{{ Cart::cena(($row->kolicina*$row->jm_cena)) }}</td>
                </tr>
                @endforeach

                <?php $troskovi = Cart::troskovi($web_b2c_narudzbina_id);
                      $popust = Order::popust($web_b2c_narudzbina_id); ?>
                <tr>
                    <td colspan="2"></td>
                    <td><b>{{ Language::trans('Cena artikala') }}: </b></td>
                    <td><b>{{Cart::cena(Order::narudzbina_ukupno($web_b2c_narudzbina_id))}} </b></td>
                </tr>
                @if($troskovi>0 AND DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->pluck('web_nacin_isporuke_id') != 2)
                <tr>
                    <td colspan="2"></td> 
                    <td><b>{{ Language::trans('Troškovi isporuke') }}: </b></td>
                    <td><b>{{Cart::cena($troskovi)}} </b></td>
                </tr>
                @else
                <?php $troskovi = 0; ?>
                @endif
                @if($popust>0)
                <tr>
                    <td colspan="2"></td> 
                    <td><b>{{ Language::trans('Popust') }}: </b></td>
                    <td><b>{{Cart::cena($popust)}} </b></td>
                </tr>
                @endif
                <tr>
                    <td colspan="3"></td> 
                    @if(WebKupac::check_distributive_user(Session::get('b2c_kupac')))
                    <td><b>{{ Language::trans('Popust') }}: </b></td>
                    <td><b>  {{AdminCommon::check_discount($web_b2c_narudzbina_id)}}  </b></td>
                    @endif
                </tr>
                <tr>
                    <td colspan="3"></td> 
                    @if(WebKupac::check_distributive_user(Session::get('b2c_kupac')))
                        @if($troskovi>0)
                        <td><b>{{ Language::trans('Ukupno sa popustom') }}: </b></td>
                        <td><b> {{ Cart::cena(AdminCommon::order_distributive_discount($web_b2c_narudzbina_id)+$troskovi-$popust) }} </b></td>
                        @else
                        <td><b>{{ Language::trans('Ukupno sa popustom') }}: </b></td>
                        <td><b> {{ Cart::cena(AdminCommon::order_distributive_discount($web_b2c_narudzbina_id)) }} </b></td>
                        @endif
                    @else
                    <td><b>{{ Language::trans('Ukupno') }}: </b></td>                   
                    <td><b>{{Cart::cena(Order::narudzbina_ukupno($web_b2c_narudzbina_id)+$troskovi-$popust)}} </b></td>
                    @endif
                </tr>
            </tbody>
        </table>
        <span class="pull-right fine-print">*Cene su prikazane sa PDV-om</span>
    </div>
      
</div>

@if(!empty(All::getShortListNews()))
 
	@if(Session::has('b2c_admin'.Options::server()))
	<h2><span class="section-title JSInlineShort" data-target='{"action":"blogs","id":"27"}'>
		{{Language::trans(Support::title_blogs())}}
	</h2>
	@else

	<h2><a class="section-title" href="{{ Options::base_url() }}{{Url_mod::slug_trans('blog')}}">{{Language::trans(Support::title_blogs())}}</a>
	</h2>

	@endif 


	<div class="JSblog-slick">   
		@foreach(All::getShortListNews() as $row) 
		<div class="col-md-4">
			<div class="card-blog"> 
				@if(in_array(Support::fileExtension($row->slika),array('jpg','png','jpeg','gif')))
					<h3 class="blogs-title">
						<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}">{{ $row->naslov }}</a>
					</h3>

					<a class="bg-img center-block" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}" style="background-image: url('{{ $row->slika }}');"></a>

				@else

					<iframe src="{{ $row->slika }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>                   

				@endif

				<!-- <h3 class="blogs-title">
					<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}">{{ $row->naslov }}</a>
				</h3> -->

				<div class="blogs-date text-uppercase">
					{{ Support::date_convert($row->datum) }}
				</div>
			</div>
		</div>
		@endforeach 

	</div> 
 
@endif
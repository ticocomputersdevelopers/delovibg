
@if(Session::has('compare_ids') AND count(Session::get('compare_ids')) > 0)
<ol class="compare-items">
    @foreach(All::getComparedArticles(Session::get('compare_ids')) as $row)
        <li class="compare-item flex">
            <input type="hidden">
            <i class="fas fa-chevron-right"></i><a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($row->roba_id))}}" class="product-name">{{Product::short_title($row->roba_id)}}</a>
            <a href="javascript:void(0)" data-id="{{ $row->roba_id }}" title="{{ Language::trans('Ukloni artikal') }}" class="JScompare" rel=”nofollow”><i class="fas fa-times"></i></a>
        </li>
    @endforeach
</ol>

<a href="{{Options::base_url()}}{{Url_mod::slug_trans('uporedi')}}"><button class="button side-content-compare-btn"><i class="far fa-chart-bar"></i>{{ Language::trans('Uporedi') }}</button></a>
<a href="{{Options::base_url()}}clear-compare"><button class="button side-content-compare-erase-btn"><i class="fas fa-eraser"></i>{{ Language::trans('Poništi') }}</button></a>
@endif
@if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array('ARTIKLI')) AND Admin_model::check_admin(array('ARTIKLI_PREGLED')))
<div id="admin-menu"> 
    <div class="container-fluid text-right">
        <a href="#!" data-toggle="modal" data-target="#FAProductsModal"> <i class="fas fa-clipboard-list"></i> {{ Language::trans('Artikli') }}</a>
        |
        @if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
        <a href="#!" id="JSShortAdminSave"> <i class="fas fa-save"></i> {{ Language::trans('Sačuvaj izmene') }}</a>
        @endif
        | 
        <span class="ms admin-links"><a target="_blank" href="{{ Options::domain() }}admin"><i class="fas fa-cogs"></i> {{ Language::trans('Admin Panel') }}</a></span> |
        <span class="ms admin-links"><a href="{{ Options::domain() }}admin-logout">{{ Language::trans('Odjavi se') }}</a></span>
    </div>
</div> 

@include('shop/front_admin/modals/products')
@include('shop/front_admin/modals/product')
@endif

<div id="preheader">

    <div class="social-icons hidden-sm hidden-xs">  
        {{Options::social_icon()}} 
    </div>

    <div class="container-fluid"> 
        <div class="top-menu row">

            <div class="col-md-8 col-sm-6 col-xs-2  hidden-xs">     
                
                @if(Options::stranice_count() > 0)
                <span class="JStoggle-btn inline-block color-white hidden-md hidden-lg">
                   <i class="fas fa-bars"></i>                 
                </span>
                @endif 

                <ul class="hidden-small JStoggle-content">
                    @foreach(All::menu_top_pages() as $row)
                    <li class="inline-block"><a href="{{ Options::base_url().Url_mod::page_slug($row->naziv_stranice)->slug }}">{{ Url_mod::page_slug($row->naziv_stranice)->naziv }}</a></li>
                    @endforeach
                </ul>  

            </div>

            <div class="col-md-4 col-sm-6 col-xs-12 text-center"> 
               <!--  @if(Options::checkB2B())
                <a id="b2b-login-icon" href="{{Options::domain()}}b2b/login" class="inline-block">B2B</a> 
                @endif  -->
                <span class="menu-top-phone flex pull-right">{{ Language::trans('PORUČIVANJE I PODRŠKA') }} <a href="tel:+381668100216">+381 66 8100 216</a></span>
            </div>   
 
        </div> 
    </div>
</div>



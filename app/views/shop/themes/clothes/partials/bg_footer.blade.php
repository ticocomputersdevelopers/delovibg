<style>
	footer {
		color: black;
		background: white;
		border-top: 1px solid #ddd;
		border-bottom: 1px solid #ddd;
	}
	.bg-footer-pages div {
		padding: 0;
	}
	.you-are-here {
		text-align: right;
	}
	.you-are-here span {
		color: white;
		padding: 0px 5px;
		background: #fdcf39;
		border-radius: 5px;
	}
	.bg-footer-box-1, .bg-footer-box-2, .bg-footer-box-3, .bg-footer-box-4  {
		content: '';
		padding: 1px 10px;
		border-radius: 5px;
	}
	.bg-footer-box-1 {
		background: #FCD400;
	}
	.bg-footer-box-2 {
		background: #EA078A;
	}
	.bg-footer-box-3 {
		background: #F17C20;
	}
	.bg-footer-box-4 {
		background: #EB1F27;
	}
	.bg-footer-arrow {
		margin: 0;
	    position: absolute;
	    top: 50%;
	    left: 100%;
	    transform: translate(-50%, -50%);
	}
	.bg-footer-pages-text {
		padding: 10px 0;
		font-size: 12px;
	}
	#logoBgCustom {
		max-height: 60px;
		margin-bottom: 2rem;
	}
	.bg-footer-contact li {
		margin: 2rem 0 !important;
	}
	.bg-custom-flex {
		display: flex;
		justify-content: space-between;
		align-items: center;
		margin-top: 2rem !important;

	}

	.bg-footer-pages-title {
		font-size: 16px;
	    font-weight: 600;
	    padding: 0 10px !important;
	}
	.bg-footer-right {
		font-weight: 600;
	}
	.bg-footer-icons {
		font-size: 24px;
		font-weight: 600;
		padding-right: 10px;
	}
	.bg-footer-icons .fa-instagram {
		font-weight: 500;
	}
	.bg-footer-contact-text {
	    position: relative;
    	bottom: 5px;
	}
	#bg-footer-adress-icon {
		position: relative;
    	top: 14px;
	}
	.foot-note {
		background: #fff;
		color: #000;		
	}
	.bg-footer-pages-card {
		padding: 10px 0px 0px 10px;
	}
	.bg-footer-pages-card:hover {
		box-shadow: 0 0 10px 0px #ddd;
	}
</style>	

<body>

<footer>
	<div class="container">
		<div class="row">

			<!-- PAGES -->
			<div class="col-md-12 col-sm-12 col-xs-12 bg-footer-pages">
				<div class="row">
					<div class="col-md-12 bg-footer-pages-card">
						<div class="row">
							<div class="col-md-10 no-padding">
								<div class="row">
									<div class="col-md-1 col-sm-1 col-xs-1 bg-footer-box-1">&nbsp;</div>
									<div class="col-md-11 bg-footer-pages-title"><span>SHOP</span></div>
									<div class="col-md-12">
										<p class="bg-footer-pages-text">Lorem ipsum dolor sit atmet lorem ipsum Lorem ipsum dolor sit atmet lorem ipsum.</p>
									</div>
								</div>
							</div>
							<div class="col-md-1 bg-footer-arrow">
								<div class="fas fa-caret-right">&nbsp;</div>
							</div>			
						</div>
					</div>

					<div class="col-md-12 bg-footer-pages-card">
						<div class="row">
							<div class="col-md-10 no-padding">
								<div class="row">
									<div class="col-md-1 col-sm-1 col-xs-1 bg-footer-box-2">&nbsp;</div>
									<div class="col-md-11 bg-footer-pages-title"><span>SHOP</span></div>
									<div class="col-md-12">
										<p class="bg-footer-pages-text">Lorem ipsum dolor sit atmet lorem ipsum Lorem ipsum dolor sit atmet lorem ipsum.</p>
									</div>
								</div>
							</div>
							<div class="col-md-1 bg-footer-arrow">
								<div class="fas fa-caret-right">&nbsp;</div>
							</div>			
						</div>
					</div>

					<div class="col-md-12 bg-footer-pages-card">
						<div class="row">
							<div class="col-md-10 no-padding">
								<div class="row">
									<div class="col-md-1 col-sm-1 col-xs-1 bg-footer-box-3">&nbsp;</div>
									<div class="col-md-11 bg-footer-pages-title"><span>SHOP</span></div>
									<div class="col-md-12">
										<p class="bg-footer-pages-text">Lorem ipsum dolor sit atmet lorem ipsum Lorem ipsum dolor sit atmet lorem ipsum.</p>
									</div>
								</div>
							</div>
							<div class="col-md-1 bg-footer-arrow">
								<div class="fas fa-caret-right">&nbsp;</div>
							</div>			
						</div>
					</div>

					<div class="col-md-12 bg-footer-pages-card">
						<div class="row">
							<div class="col-md-10 no-padding">
								<div class="row">
									<div class="col-md-1 col-sm-1 col-xs-1 bg-footer-box-4">&nbsp;</div>
									<div class="col-md-11 bg-footer-pages-title"><span>SHOP</span></div>
									<div class="col-md-12">
										<p class="bg-footer-pages-text">Lorem ipsum dolor sit atmet lorem ipsum Lorem ipsum dolor sit atmet lorem ipsum.</p>
									</div>
								</div>
							</div>
							<div class="col-md-1 bg-footer-arrow">
								<div class="fas fa-caret-right">&nbsp;</div>
							</div>			
						</div>
					</div>
				</div>
			</div>
			<!-- END OF PAGES -->
			<div class="you-are-here">Ti si ovde!</div>
<div class="you-are-here">Ti si ovde!</div>
<div class="you-are-here">Ti si ovde!</div>
<div class="you-are-here">Ti si ovde!</div>
&nbsp;
			<div class="row bg-footer-right">
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-12">
							<a class="logo" href="{{ B2bOptions::base_url()}}b2b" title="{{B2bOptions::company_name()}}">
	        					<img class="img-responsive" src="{{ B2bOptions::base_url()}}{{B2bOptions::company_logo()}}" alt="{{B2bOptions::company_name()}}" id="logoBgCustom" />
	   						</a>
						</div>

						<div class="col-md-6 col-sm-6 col-xs-12">
							<!-- CONTACT -->
							<div class="col-md-12 bg-footer-contact no-padding">
								<div class="row">
									<div class="col-md-12 no-padding">
										<ul>
											@if(Options::company_phone())
						 					<li>
						 						<i class="bg-footer-icons fas fa-phone-alt"></i>
						 						<a class="mailto bg-footer-contact-text" href="tel:{{ Options::company_phone() }}">
						 							{{ Options::company_phone() }}
						 						</a>
						 					</li>
						 					@else
						 					<li>
						 						<i class="bg-footer-icons fas fa-phone-alt"></i>
						 						<span><a href="tel:0112086666">011 / 208 6666</a></span>
						 					</li>		 					
						 					@endif

						 					@if(Options::company_email())
						 					<li>
						 						<i class="bg-footer-icons far fa-envelope"></i>
						 						<a class="mailto bg-footer-contact-text" href="mailto:{{ Options::company_email() }}">{{ Options::company_email() }}</a>
						 					</li>
						 					@else
						 					<li>
						 						<i class="bg-footer-icons far fa-envelope"></i>
						 						<span><a href="mailto:servis@bgelektronik.com">servis@bgelektronik.com</a></span>
						 					</li>
						 					@endif

						 					@if(Options::company_adress() OR Options::company_city())
						 					<li id="bg-foter-adress"> 
						 						<i id="bg-footer-adress-icon" class="bg-footer-icons fas fa-map-marked-alt" style=""></i>
						 						<span class="bg-footer-contact-text">{{ Options::company_adress() }}</span>
						 						<br> 
						 						<i class="bg-footer-icons fas fa-map-marked-alt" style="visibility: hidden;"></i>
						 						<span class="bg-footer-contact-text">{{ Options::company_city() }}</span>
						 					</li>
						 					@else
						 					<li>
						 						<i class="bg-footer-icons fas fa-map-marked-alt" style=""></i>
						 						<span>Dragoslava Srejovića 1b</span>
							 					<br>
							 					<i class="bg-footer-icons fas fa-map-marked-alt" style="visibility: hidden;"></i>
							 					<span>11000 Beograd</span>
						 					</li>
						 					@endif


						 					<li>
						 						<i class="bg-footer-icons fab fa-instagram"></i>
						 						<span class="bg-footer-contact-text">bg.elektronik</span>
						 					</li>
						 				</ul>
									</div>
								</div>
							</div>
							<!-- END OF CONTACT -->
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<!-- ABOUT -->
							<div class="col-md-12 bg-footer-about no-padding">
								<div class="row">
									<div class="col-md-10 bg-custom-flex">
										<div><span>O isporukama</span></div>
										<div><i class="fas fa-caret-right">&nbsp;</i></div>
									</div>
									
									<div class="col-md-10 bg-custom-flex">
										<div><span>O reklamacijama</span></div>
										<div><i class="fas fa-caret-right">&nbsp;</i></div>
									</div>

									<div class="col-md-10 bg-custom-flex">
										<div><span>Uslovi korišćenja</span></div>
										<div><i class="fas fa-caret-right">&nbsp;</i></div>
									</div>

									<div class="col-md-10 bg-custom-flex">
										<div><span>Politika privatnosti</span></div>
										<div><i class="fas fa-caret-right">&nbsp;</i></div>
									</div>
								</div>								
							</div>
							<!-- END OF ABOUT -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	

	  <a class="JSscroll-top" href="javascript:void(0)">
        <i class="fa fa-angle-up"></i>
    </a> 
</footer>	
</div>
	    <div class="container">
        <div class="text-center foot-note"> 
            <span>{{B2bOptions::company_name() }}  {{ date('Y') }}. Sva prava zadržana <i class="far fa-copyright"></i></span> 
            <a href="https://www.selltico.com/" target="_blank">- Selltico.</a> 
        </div>
    </div>
<!-- GROUPS ON GRID -->
<div class="groups-list no-padding">
	<div class="groups-list-card">
		<div class="group-list-img-wrp relative">
			<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row2->grupa) }}" class="flex">
				<img src="{{ Options::domain() }}{{$row2->putanja_slika}}" alt="{{ $row2->grupa }}" />
			</a>
		</div>
		<span class="groups-list-title inline-block">{{ Language::trans($row2->grupa) }}</span>
		<span class="groups-list-count">{{Articles::brojiArtikleRekurzivno($row2->grupa_pr_id)}} {{ Language::trans('artikla') }}</span>

	</div>
</div>
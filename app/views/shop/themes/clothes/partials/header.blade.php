<!-- header.blade -->
<header class="relative">   
    <div id="JSfixed_header" style="background-color: {{ Options::web_options(321,'str_data') }}">  
        <div class="container{{(Options::web_options(321)==1) ? '-fluid' : '' }} JS-bg">  

            <div class="row flex header-top"> 

                <!-- <div class="sm-border-box"></div> -->
                <div class="col-md-3 col-sm-3 col-xs-6 sm-text-center no-padding"> 
                   <h1 class="seo">{{ Options::company_name() }}</h1>
                    
                    <div class="logo"><a class="flex" href="/pocetna" title="{{Options::company_name()}}" rel="canonical">
                        <img src="{{ Options::domain() }}images/bg-logo.png" alt="{{Options::company_name()}}" class="JSlogo sm-margin img-responsive logo-white"/>
                        <img src="{{ Options::domain() }}images/bg-logo - black.png" alt="{{Options::company_name()}}" class="JSlogo hidden sm-margin img-responsive logo-black"/>
                    </a></div>
                </div>

                <div class="col-md-9 col-sm-9 col-xs-6 text-right sm-text-center header-menu flex relative no-padding"> 
                    
                    <div class="pointer JSopen-search-modal flex header-seperate">
                        <span class="inline-block bg-sprite sprite-search"></span>
                        <span class="flex search-text menu-ico-txt">{{ Language::trans('Pretraga') }}</span>
                    </div>

                   
                    
                    @include('shop/themes/'.Support::theme_path().'partials/cart_top')

                    <div class="resp-nav-btn flex pointer">
                        <div class="menu-open flex">
                            <span class="inline-block bg-sprite sprite-menu JSmenu-bars"></span>
                            <span class="flex JSmenu-bars menu-ico-txt">{{ Language::trans('Meni') }}</span>
                        </div>
                        <div class="menu-close">
                            <span class=" bg-sprite sprite-close"></span>
                        </div>
                    </div> 

                </div>  

                 <div class="header-search JSsearch-modal">     
                    @if(Options::gnrl_options(3055) == 0)

                        <div class="no-padding JSselectTxt hidden">  
                            {{ Groups::firstGropusSelect('2') }} 
                        </div>

                    @else

                        <input type="hidden" class="JSSearchGroup2 hidden" value="">

                    @endif

                    <div class="no-padding JSsearchContent2 flex search-content sm-bg-width">  
                        <form autocomplete="off" class="relative sm-bg-width no-margin">
                            <input class="search-field sm-bg-width" type="text" id="JSsearch2" placeholder="{{ Language::trans('Pretraži BG Elektronik Shop...') }}" />
                        </form>
                            <button type="button" onclick="search2()" class="JSsearch-button2"> <span class=" bg-sprite sprite-search-2 hidden-xs"></span> </button>          
                    </div>
                </div>  
            </div>
  
            <div id="responsive-nav" class="text-center">

                <!-- <div class="JSclose-nav text-right">&times;</div>  -->
                
                <!-- MAIN MENU -->
                <button class="menu-back"><span class="menu-icon fas fa-angle-left"></span></button>
                <div id="main-menu" class="valign">
                    <div class="main-menu-content flex">
                        <!-- MENU LINKS -->
                        <ul class="menu-links">
                            @foreach(All::header_menu_pages() as $row)
                            <li>
                               
                                @if(All::broj_cerki($row->web_b2c_seo_id) > 0)  
                               
                                <a href="{{ Options::base_url().Url_mod::page_slug($row->naziv_stranice)->slug }}">{{Url_mod::page_slug($row->naziv_stranice)->naziv}}</a>

                                <ul class="drop-2 text-left">
                                    @foreach(All::header_menu_pages($row->web_b2c_seo_id) as $row2)
                                    <li> 
                                        <a href="{{ Options::base_url().Url_mod::page_slug($row2->naziv_stranice)->slug }}">{{Url_mod::page_slug($row2->naziv_stranice)->naziv}}</a>
                                        <ul class="drop-3">
                                            @foreach(All::header_menu_pages($row2->web_b2c_seo_id) as $row3)
                                            <li> 
                                                <a href="{{ Options::base_url().Url_mod::page_slug($row3->naziv_stranice)->slug }}">{{Url_mod::page_slug($row3->naziv_stranice)->naziv}}</a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                    @endforeach
                                </ul>
                               
                                @else   
                               
                                <a href="{{ Options::base_url().Url_mod::page_slug($row->naziv_stranice)->slug }}">{{Url_mod::page_slug($row->naziv_stranice)->naziv}}</a> 
                               
                                @endif                    
                            </li>                     
                            @endforeach 

                            @if(Options::web_options(121)==1)
                                <?php $konfiguratori = All::getKonfiguratos(); ?>
                               
                                @if(count($konfiguratori) > 0)
                                    @if(count($konfiguratori) > 1)
                                    <li>
                                        <a href="#!" class="konf-dropbtn">
                                            {{Language::trans('Konfiguratori')}}
                                        </a>
                                        <ul class="drop-2 text-left">
                                            @foreach($konfiguratori as $row)
                                            <li>
                                                <a href="{{ Options::base_url() }}{{Url_mod::slug_trans('konfigurator')}}/{{ $row->konfigurator_id }}">{{ Language::trans($row->naziv) }}</a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                    @else
                                    <li>
                                        <a href="{{ Options::base_url() }}{{Url_mod::slug_trans('konfigurator')}}/{{ $konfiguratori[0]->konfigurator_id }}">
                                            {{Language::trans('Konfigurator')}}
                                        </a>
                                    </li>
                                    @endif
                                @endif
                            @endif
                        </ul>

                        <!-- LOGIN AND REGISTRATION -->
                        <div class="menu-user">
                            @if(Session::has('b2c_kupac'))
                            <div class="log-user"> 
                                <span class="flex-fix log-user-name" href="{{Options::base_url()}}{{Url_mod::slug_trans('korisnik')}}/{{Url_mod::slug_trans(WebKupac::get_user_name())}}">{{ WebKupac::get_user_name() }}</span>

                                <span class="flex-fix log-user-name" href="{{Options::base_url()}}{{Url_mod::slug_trans('korisnik')}}/{{Url_mod::slug_trans(WebKupac::get_company_name())}}">{{ WebKupac::get_company_name() }}</span>

                                <a class="flex-fix JS-wish-btn relative" href="javascript:void(0)"><span class="inline-block bg-sprite sprite-wish"></span><span class="hidden JSbroj_wish menu-icon far fa-heart">{{ Cart::broj_wish() }}</span><span class="inline-block">{{ (Language::Trans('Lista želja')) }}</span></a>

                                <a class="flex-fix JS-user-btn relative" href="javascript:void(0)"><span class="inline-block bg-sprite sprite-user"></span><span class="hidden menu-icon far fa-user"></span><span class="inline-block">{{ (Language::Trans('Lični podaci')) }}</span></a>

                                <a class="flex-fix" id="logout-button" href="{{Options::base_url()}}logout" title="{{ Language::trans('Odjavi se') }}">
                                    <i>{{ (Language::trans('Odjavi se')) }}</i>&nbsp; &nbsp;<i class="menu-icon fas fa-sign-out-alt"></i>
                                </a>
                            </div>
                            
                            @else 
                        </div>    

                        <div class="menu-user-2">
                            <h2 class="user-header">{{ (Language::Trans('NALOG')) }}</h2>
                            <div class="">
                                <ul class="login-dropdown"> 
                                    <li>
                                        <a id="registration-icon" href="{{Options::base_url()}}{{ Url_mod::slug_trans('registracija') }}">{{ Language::trans('Napravi nalog') }}</a>
                                    </li>

                                    <li> 
                                        <!-- <a href="#" id="login-icon" data-toggle="modal" data-target="#loginModal">{{ Language::trans('Prijavi se') }}</a> -->
                                        <a id="login-icon" href="{{Options::base_url()}}{{ Url_mod::slug_trans('prijava') }}">{{ Language::trans('Prijavi se') }}</a>
                                    </li>                           
                                </ul>
                            </div> 
                            @endif
                        </div>

                        <div class="menu-pages hidden-lg hidden-md hidden-sm">
                            <h2 class="user-header">{{ (Language::Trans('STRANE')) }}</h2>
                            <ul class="JStoggle-content hidden-md hidden-sm">
                                @foreach(All::menu_top_pages() as $row)
                                <li ><a href="{{ Options::base_url().Url_mod::page_slug($row->naziv_stranice)->slug }}">{{ Url_mod::page_slug($row->naziv_stranice)->naziv }}</a></li>
                                @endforeach
                            </ul> 
                        </div>
                    </div>

                    <!-- WISH LIST -->
                    @if(Session::has('b2c_kupac'))
                    <div class="wish-list">
                        <h2><span class="section-title text-center">{{ Language::trans('Lista želja') }}</span></h2> 
                        
                        @if(Session::has('b2c_kupac') AND $web_kupac = DB::table('web_kupac')->where('web_kupac_id',Session::get('b2c_kupac'))->first() AND count(All::getWishIds($web_kupac->web_kupac_id)) > 0)

                        <div class="wish-container">
                            @foreach(All::getWishIds($web_kupac->web_kupac_id) as $row)
                            <div class="JSproduct col-xs-12 no-padding"> 
                                <div class="shop-product-card relative row flex">  

                                    <div class="wish-remove no-padding">
                                    @if(Product::getStatusArticle($row->roba_id) == 1)  
                                        @if(Cart::check_avaliable($row->roba_id) > 0)    
                                            <button class="JSukloni" data-roba_id="{{$row->roba_id}}"><span class="inline-block bg-sprite sprite-close"></span></button>
                                        @else
                                            <button class="JSukloni" data-roba_id="{{$row->roba_id}}"><span class="inline-block bg-sprite sprite-close"></span></button>
                                        @endif
                                        @else
                                            <button class="JSukloni" data-roba_id="{{$row->roba_id}}"><span class="inline-block bg-sprite sprite-close"></span></button>
                                    @endif 
                                    </div>

                                    <div class="product-image-wrapper relative no-padding">

                                        <a class="flex wish-img-link" href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($row->roba_id))}}">
                                            <div class="wish-img-wrap">
                                                <img class="product-image img-responsive JSlazy_load" src="{{Options::domain()}}images/quick_view_loader.gif" data-src="{{ Options::domain() }}{{ Product::web_slika($row->roba_id) }}" alt="{{ Product::seo_title($row->roba_id) }}" />
                                            </div>
                                        </a>  

                                    </div>

                                    <div class="product-meta"> 
                                        <div class="row">
                                            <h2 class="product-name"> 
                                                <a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($row->roba_id))}}">{{ Product::short_title($row->roba_id) }}</a>
                                            </h2>
                                            <div class="no-padding">
                                                <div class="row product-inner-meta">
                                                    <div class="price-holder col-xs-12 no-padding text-left">
                                                        <div> {{ Cart::cena(Product::get_price($row->roba_id)) }} </div>
                                                        @if(All::provera_akcija($row->roba_id))
                                                        <span class="product-old-price">{{ Cart::cena(Product::old_price($row->roba_id)) }}</span>
                                                        @endif 
                                                    </div>    

                                                </div>
                                            </div>
                                        </div>
                                        <div class="text-right wish-add-wrap hidden-xs">   
                                            @if(Product::getStatusArticle($row->roba_id) == 1)  
                                            @if(Cart::check_avaliable($row->roba_id) > 0)    
                                            <button class="wish-button JSadd-to-cart text-uppercase" data-roba_id="{{$row->roba_id}}">{{ Language::trans('Kupi') }}</button>                
                                            @else    
                                            <button class="not-available wish-button text-uppercase"> {{ Language::trans('Nije dostupno') }}</button>   
                                            @endif 

                                            @else  
                                            <button class="wish-button text-uppercase">{{ Product::find_flag_cene(Product::getStatusArticle($row->roba_id),'naziv') }}</button>
                                            @endif  
                                        </div>
                                    </div> 
                                </div>

                                <div class="text-right wish-add-wrap xs-hidden">   
                                    @if(Product::getStatusArticle($row->roba_id) == 1)  
                                    @if(Cart::check_avaliable($row->roba_id) > 0)    
                                    <button class="wish-button JSadd-to-cart text-uppercase" data-roba_id="{{$row->roba_id}}">{{ Language::trans('Kupi') }}</button>                
                                    @else    
                                    <button class="not-available wish-button text-uppercase"> {{ Language::trans('Nije dostupno') }}</button>   
                                    @endif 

                                    @else  
                                    <button class="wish-button text-uppercase">{{ Product::find_flag_cene(Product::getStatusArticle($row->roba_id),'naziv') }}</button>
                                    @endif  
                                </div>

                            </div>

                            @endforeach 
                        </div>
                        
                        @else

                        <div class="empty-cart">
                            <img src="../images/wish_empty.png" alt="wish_list_empty" class="img-responsive margin-auto">
                            <div class="empty-page-label">{{ Language::trans('Tvoja lista želja je i dalje prazna') }}.</div>
                            <div class="empty-page-label-2">{{ Language::trans('Na sreću, postoji lako rešenje') }}.</div>
                            <a href="{{Options::base_url()}}"><button class="text-center button bg-button text-uppercase">{{ Language::trans('Kreni u kupovinu') }}</button></a>
                        </div>

                        @endif
                    @endif      
                </div> 

                <!-- USER INFO  -->
                <div class="user-info">
                    @include('shop/themes/'.Support::theme_path().'partials/menu-user')
                </div>

                <!-- END OF MENU -->
            </div>   
        </div>  
    </div> 
</header>
  

<!-- header.blade end -->


<!--========= QUICK VIEW MODAL ======== -->
<div class="modal fade" id="JSQuickView" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
            <button type="button" id="JSQuickViewCloseButton" class="close close-me-btn">
              <span>&times;</span>
            </button>
          </div>
          <div class="modal-body" id="JSQuickViewContent">
            <img alt="loader-image" class="gif-loader" src="{{Options::base_url()}}images/quick_view_loader.gif">
          </div>
      </div>    
    </div>
</div>

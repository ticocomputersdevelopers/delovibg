@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')

<?php

?>

@if(!Session::has('b2c_kupac'))
<div class="cart-user-type">
	<div class="cart-log-in without-btn"><a href="{{Options::base_url()}}{{Url_mod::slug_trans('prijava')}}">{{ Language::trans('Imaš nalog? Prijavi se!') }}</a></div>
	
	<div class="cart-user without-btn JScart-user-btn">{{ Language::trans('Fizičko ili') }} <br> {{ Language::trans('pravno lice?') }}</div>

	@if(Input::old('flag_vrsta_kupca') == 1)
	<div class="cart-user JS-user-flag JScart-user-1-p">
		<div class="cart-user-holder flex"> 
			<div class="JScheck_user_type without-btn personal" data-vrsta="personal">
				{{ Language::trans('Fizičko Lice') }} 
			</div>
		</div>
	</div>

	<div class="cart-user JS-user-flag JScart-user-1-np">
		<div class="cart-user-holder flex"> 
			<div class="JScheck_user_type without-btn active none-personal" data-vrsta="non-personal">
				{{ Language::trans('Pravno Lice') }}
			</div>
		</div>
	</div>
	@else
	<div class="cart-user JS-user-flag JScart-user-0-p">
		<div class="cart-user-holder flex"> 
			<div class="JScheck_user_type without-btn active personal" data-vrsta="personal">
				{{ Language::trans('Fizičko Lice') }}
			</div>
		</div>
	</div>

	<div class="cart-user JS-user-flag JScart-user-0-np">
		<div class="cart-user-holder flex"> 
			<div class="JScheck_user_type without-btn none-personal" data-vrsta="non-personal">
				{{ Language::trans('Pravno Lice') }}
			</div>
		</div>
	</div>
	@endif 

</div>

@endif


<div class="bg-cont"> 

	@if(Session::has('b2c_korpa') and Cart::broj_cart() >= 1 )

	<div class="above-cart flex relative">
		<h2><span class="page-title">{{ Language::trans('Korpa') }}</span></h2>
		@if(Session::has('b2c_kupac') AND Options::web_options(318)==1) <!-- Promenjeo iz AND u OR da bi se promo kod video bez naloga -->
		<button class="xs-hidden promo-btn"><span class="text-uppercase">Unesi promo kod</span></button>
		<div class="cpn-wrap">
			<div class="coupon-holder text-right">
				<input type="text" name="vaucer_code" placeholder="{{ Language::trans('Promo kôd') }}" value="{{ htmlentities(Input::old('vaucer_code') ? Input::old('vaucer_code') : '') }}">
				<div class="error red-dot-error">{{ Session::has('vaucer_error') ? Session::get('vaucer_error') : '' }}</div>
				<button class="coupon-btn "><span class="button text-uppercase">Ostvari popust</span></button>	
			</div>
		</div>
		@endif

		<div class="row clearfix JScart-sum-holder-2 xs-hidden">  
			<div class="cart-sum col-md-12 col-sm-12 col-xs-12 pull-right text-right ">
				
				<h4 class="pull-left">{{ Language::trans('Ukupno') }} </h4>

				@if(($troskovi = Cart::troskovi())>0) 
			
				<span class="JStotal_amount JStotal_amount_weight sum-amount">{{Cart::cena(Cart::cart_ukupno()+$troskovi)}}</span>

				<div class="JSDelivery">
					<span class="sum-label text-right">{{ Language::trans('Troškovi isporuke') }}: </span>
					<span class="JSexpenses sum-amount">{{ $troskovi }}</span>
				</div>

				

				@else
				<div>
					<span class="JSdelivery_total_amount sum-amount">{{Cart::cena(Cart::cart_ukupno())}}</span>
					
					<div class="JSfree_delivery text-right">	
						<i class="fas fa-plus"></i> {{ Language::trans('Besplatna dostava') }} 
					</div> 
				</div>			
				@endif
			</div>
		</div>
	</div>

	@if(Session::has('failure-message'))
	<h4>{{ Session::get('failure-message') }}</h4>
	@endif
	<div class="JScart-top">
		<ul class="cart-labels row flex">
			<li class="col-md-2 col-sm-3 col-xs-12 JScart-count"></li>
			<li class="col-md-3 col-sm-2 hidden-xs text-uppercase">{{ Language::trans('Naziv') }}</li>			 
			<li class="col-md-2 col-sm-2 hidden-xs text-uppercase">{{ Language::trans('Boja') }}</li>
			<li class="col-md-2 col-sm-2 hidden-xs text-uppercase">{{ Language::trans('Količina') }}</li>
			<li class="col-md-2 col-sm-2 hidden-xs text-uppercase">{{ Language::trans('Cena') }}</li> 	 
		</ul>
		@foreach(DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_id',Cart::korpa_id())->orderBy('web_b2c_korpa_stavka_id','asc')->get() as $row)
		<ul class="JScart-item flex row">	 

			<li class="cart-img-wrap col-md-2 col-sm-3 col-xs-3 no-padding">
				<img src="{{ Options::domain() }}{{Product::web_slika($row->roba_id) }}" alt="{{ Product::short_title($row->roba_id) }}" class="cart-image img-responsive"/>
			</li>

			<!-- <div class="resp-cart-wrap"> THIS LINE WRAPS IN JS -->
				<li class="cart-name col-md-3 col-sm-2 col-xs-8 JSwrap no-padding sm-order-0">
					<a class="inline-block cart-flow-wrap sm-margin-unset" href="{{ Options::base_url() }}{{Url_mod::slug_trans('artikal')}}/{{ Url_mod::slugify(Product::seo_title($row->roba_id)) }}">
						{{ Product::short_title($row->roba_id) }}
					</a>
					{{Product::getOsobineStr($row->roba_id,$row->osobina_vrednost_ids)}}   
				</li>

				<li class="sm-order-2 JSwrap col-md-2 col-sm-2 col-xs-8 {{ !empty($boja = AdminArticles::find($row->roba_id,'stari_konto')) ? '' : 'hidden resp-override' }} no-padding"><span class="sm-margin-unset">
					<div class="option-variant"> 
			            <div class="box inline-block {{ $row->roba_id ? 'active' : '' }}">                     
			                <div class="flex"> 
			                    <span class="color-circle inline-block" style="
			                    	background: {{ !empty($boja = AdminArticles::find($row->roba_id,'stari_konto')) ? $boja : 'hide' }};
	                                border: {{ ((AdminArticles::find($row->roba_id,'stari_konto')) == '#ffffff') ? '1px solid black' : '1px solid transparent';}} ;
		                    	"></span> &nbsp;
			                    <span >{{ !empty($colorName = Product::colorLabel($row->roba_id)) ? $colorName : '' }}</span>
			                </div>
			            </div> 
			        </div>
		        </span></li>

				<li class="JSwrap col-md-2 col-sm-2 col-xs-8 resp-col-hide no-padding">
					<div class="cart-add-amount clearfix sm-margin-unset">
						<a data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}" class="JScart-less" href="javascript:void(0)" rel=”nofollow”><i class="fas fa-minus"></i></a>

						<input type="text" class="JScart-amount" value="{{ round($row->kolicina) }}" onkeypress="validate(event)" data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}">

						<a data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}" class="JScart-more" href="javascript:void(0)" rel=”nofollow”><i class="fas fa-plus"></i></a>
					</div>
				</li>

				<li class="sm-order-1 JSwrap cart-price col-md-2 col-sm-2 col-xs-8 no-padding"><span class="sm-margin-unset">{{ Cart::cena($row->jm_cena) }}</span></li>

			<!-- </div> -->
				<li class="cart-remove col-md-1 col-sm-1 col-xs-1 no-padding">
					<a href="javascript:void(0)" data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}" class="remove-item JSdelete_cart_item" rel=”nofollow”> <span class=" bg-sprite sprite-close"></span></a>
				</li>			 
		</ul>
		@endforeach

		<br>

		<div class="resp-buy-wrap">
			<div class="row clearfix JScart-sum-holder-1">  
				<div class="cart-sum col-md-12 col-sm-12 col-xs-12 pull-right text-right">
					
					<span class="pull-left">{{ Language::trans('Ukupno') }} </span>

					 <?php $troskovi = Cart::troskovi(); ?>

					<div id="JSExpensesContent" {{ !($troskovi>0 AND Input::old('web_nacin_isporuke_id') != 2) ? 'hidden="hidden"' : '' }}>	
						<span class="JStotal_amount JStotal_amount_weight sum-amount">{{Cart::cena(Cart::cart_ukupno()+$troskovi)}}</span>

						<div class="JSDelivery">
							<span class="sum-label text-right">{{ Language::trans('Troškovi isporuke') }}: </span>
							<span class="JSexpenses sum-amount">{{ $troskovi }}</span>
						</div>
					</div>
					
					<div id="JSWithoutExpensesContent" {{ ($troskovi>0 AND Input::old('web_nacin_isporuke_id') != 2) ? 'hidden="hidden"' : '' }}>
						<span class="JSdelivery_total_amount sum-amount">{{Cart::cena(Cart::cart_ukupno())}}</span>
						
						<div class="JSfree_delivery text-right">	
							<i class="fas fa-plus"></i> {{ Language::trans('Besplatna dostava') }} 
						</div> 
					</div>			
					

				</div>
			</div>

			<div class="text-center xs-hidden"> 
				<button id="JScart-switch" class="resp-btn bg-button button text-uppercase">{{ Language::trans('Unesi podatke') }}</button>
			</div>
		</div>

		<span class="pull-right fine-print">*Cene su prikazane sa PDV-om</span>
	</div>

	<br class="hidden-xs">

	<div id="JSRegToggleSec" class="JScart-bottom"> 
		
		<form method="POST" action="{{ Options::base_url() }}order-create" id="JSOrderForm" class="without-reg-form row">
		<div class="JScart-bottom-wrap">
			@if(Options::neregistrovani_korisnici()==1 AND !Session::has('b2c_kupac'))
			<div class="row">
				<div class="col-md-8 col-sm-8 col-xs-12 no-padding">
					<div class="row form-holder sm-flex">
						<label class="sm-order-0 col-md-6 col-sm-6 col-xs-12 no-padding payment-label JSwithout-reg-personal {{ Input::old('flag_vrsta_kupca') == 1 ? '' : 'active' }}">{{ Language::trans('Gde da dostavimo?') }}</label>

						<label class="sm-order-3 col-md-6 col-sm-6 col-xs-12 no-padding payment-label JSwithout-reg-personal {{ Input::old('flag_vrsta_kupca') == 1 ? '' : 'active' }}">{{ Language::trans('Kako da te kontaktiramo?') }}</label>

						<label class="col-md-6 col-sm-6 col-xs-12 no-padding payment-label JSwithout-reg-none-personal {{ Input::old('flag_vrsta_kupca') == 1 ? '' : 'inactive' }}">{{ Language::trans('Podaci za dostavu') }}</label>

						<label class="hidden-xs col-md-6 col-sm-6 col-xs-12 no-padding payment-label JSwithout-reg-none-personal {{ Input::old('flag_vrsta_kupca') == 1 ? '' : 'inactive' }}">&nbsp;</label>

						<div class="order-0 col-md-6 col-sm-6 col-xs-12 no-padding form-group JSwithout-reg-none-personal {{ Input::old('flag_vrsta_kupca') == 1 ? 'active' : '' }}">
							<!-- <label for="without-reg-company"><span class="red-dot-error"><i class="fas fa-star-of-life"></i></span> {{ Language::trans('Naziv Firme') }}:</label> -->
							<input id="without-reg-company" name="naziv" type="text" tabindex="1" placeholder="{{ Language::trans('Naziv firme') }}" value="{{ htmlentities(Input::old('naziv') ? Input::old('naziv') : '') }}">
							<div class="error red-dot-error">{{ $errors->first('naziv') ? $errors->first('naziv') : "" }}</div>
						</div>

						<div class="order-3 sm-order-1 col-md-6 col-sm-6 col-xs-12 no-padding form-group"> 
							<!-- <label for="without-reg-address"><span class="red-dot-error"><i class="fas fa-star-of-life"></i></span> {{ Language::trans('Adresa za dostavu') }}</label> -->
							<input id="without-reg-address" name="adresa" type="text" tabindex="5" placeholder="{{ Language::trans('Adresa') }}" value="{{ htmlentities(Input::old('adresa') ? Input::old('adresa') : '') }}">
							<div class="error red-dot-error">{{ $errors->first('adresa') ? $errors->first('adresa') : "" }}</div>
						</div>

						<div class="order-2 col-md-6 col-sm-6 col-xs-12 no-padding form-group JSwithout-reg-none-personal {{ Input::old('flag_vrsta_kupca') == 1 ? 'active' : '' }}">
							<!-- <label for="without-reg-pib">{{ Language::trans('PIB') }}:</label> -->
							<input id="without-reg-pib" name="pib" type="text" tabindex="2" placeholder="{{ Language::trans('PIB') }}" value="{{ htmlentities(Input::old('pib') ? Input::old('pib') : '') }}">
							<div class="error red-dot-error">{{ $errors->first('pib') ? $errors->first('pib') : "" }}</div>
						</div>

						<div class="order-6 sm-order-4 col-md-6 col-sm-6 col-xs-12 no-padding form-group JSwithout-reg-personal {{ Input::old('flag_vrsta_kupca') == 1 ? '' : 'active' }}">
							<input id="without-reg-name" name="ime" type="text" value="{{ htmlentities(Input::old('ime') ? Input::old('ime') : '') }}" placeholder="{{ Language::trans('Ime i Prezime') }}" >
							<div class="error red-dot-error">{{ $errors->first('ime') ? $errors->first('ime') : "" }}</div>
						</div>

						<div class="order-5 sm-order-2 col-md-6 col-sm-6 col-xs-12 no-padding form-group"> 
							<!-- <label for="without-reg-city"><span class="red-dot-error"><i class="fas fa-star-of-life"></i></span> {{ Language::trans('Mesto') }}/{{ Language::trans('Grad') }}</label>				  -->
							<input type="text" name="mesto" tabindex="6" placeholder="{{ Language::trans('Grad ili mesto') }}" value="{{ htmlentities(Input::old('mesto') ? Input::old('mesto') : '') }}">
							<div class="error red-dot-error">{{ $errors->first('mesto') ? $errors->first('mesto') : "" }}</div>
						</div>

						<!-- <div class="order-7 sm-order-5 col-md-6 col-sm-6 col-xs-12 no-padding form-group JSwithout-reg-personal {{ Input::old('flag_vrsta_kupca') == 1 ? '' : 'active' }}">
							<input id="without-reg-surname" name="prezime" type="text" tabindex="2" placeholder="{{ Language::trans('Prezime') }}" value="{{ htmlentities(Input::old('prezime') ? Input::old('prezime') : '') }}">
							<div class="error red-dot-error">{{ $errors->first('prezime') ? $errors->first('prezime') : "" }}</div>
						</div> -->

						<label class="hidden col-md-6 col-sm-6 col-xs-12 no-padding payment-label JSwithout-reg-personal {{ Input::old('flag_vrsta_kupca') == 1 ? '' : 'active' }}">&nbsp;</label>

						<div class="order-4 sm-order-6 col-md-6 col-sm-6 col-xs-12 no-padding form-group"> 
							<!-- <label for="without-reg-phone"><span class="red-dot-error"><i class="fas fa-star-of-life"></i></span> {{ Language::trans('Telefon') }}</label> -->
							<input id="without-reg-phone" name="telefon" type="text" tabindex="3" placeholder="{{ Language::trans('Telefon') }}" value="{{ htmlentities(Input::old('telefon') ? Input::old('telefon') : '') }}">
							<div class="error red-dot-error">{{ $errors->first('telefon') ? $errors->first('telefon') : "" }}</div>
						</div>

						<label class="hidden-xs col-md-6 col-sm-6 col-xs-12 no-padding payment-label JSwithout-reg-personal {{ Input::old('flag_vrsta_kupca') == 1 ? '' : 'active' }}">&nbsp;</label>

						<div class="order-1 sm-order-7 col-md-6 col-sm-6 col-xs-12 no-padding form-group"> 
							<!-- <label for="without-reg-e-mail"><span class="red-dot-error"><i class="fas fa-star-of-life"></i></span> {{ Language::trans('E-mail') }}</label> -->
							<input id="JSwithout-reg-email" name="email" type="text" tabindex="4" placeholder="{{ Language::trans('E-mail') }}" value="{{ htmlentities(Input::old('email') ? Input::old('email') : '') }}">
							<div class="error red-dot-error">{{ $errors->first('email') ? $errors->first('email') : "" }}</div>
						</div>	
					</div>
				</div>

				@elseif(Session::has('b2c_kupac') AND $web_kupac = DB::table('web_kupac')->where('web_kupac_id',Session::get('b2c_kupac'))->first())
				<div class="row">
					<div class="col-md-8 col-sm-8 col-xs-12 form-group">
						<label class="col-md-6 col-sm-6 col-xs-12 payment-label text-left">{{ Language::trans('Podaci za dostavu') }}</label>
						<label class="hidden-xs col-md-6 col-sm-6 col-xs-12 payment-label">&nbsp;</label>

						<div class="col-md-6 col-sm-6 col-xs-12 cart-user-in-form"> 
							<!-- <label for="without-reg-e-mail"><span class="red-dot-error"><i class="fas fa-star-of-life"></i></span> {{ Language::trans('E-mail') }}</label> -->
							<input id="JSwithout-reg-email" name="email" type="text" tabindex="4" placeholder="{{ Language::trans('E-mail') }}" value="{{ Input::old('email') ? Input::old('email') : $web_kupac->email }}">
							<div class="error red-dot-error">{{ $errors->first('email') ? $errors->first('email') : "" }}</div>
						</div>

						<div class="col-md-6 col-sm-6 col-xs-12 cart-user-in-form"> 
							<!-- <label for="without-reg-address"><span class="red-dot-error"><i class="fas fa-star-of-life"></i></span> {{ Language::trans('Adresa za dostavu') }}</label> -->
							<input id="without-reg-address" name="adresa" type="text" tabindex="5" placeholder="{{ Language::trans('Adresa') }}" value="{{ Input::old('adresa') ? Input::old('adresa') : $web_kupac->adresa }}">
							<div class="error red-dot-error">{{ $errors->first('adresa') ? $errors->first('adresa') : "" }}</div>
						</div>

						<div class="col-md-6 col-sm-6 col-xs-12 cart-user-in-form"> 
							<!-- <label for="without-reg-phone"><span class="red-dot-error"><i class="fas fa-star-of-life"></i></span> {{ Language::trans('Telefon') }}</label> -->
							<input id="without-reg-phone" name="telefon" type="text" tabindex="3" placeholder="{{ Language::trans('Telefon') }}" value="{{ Input::old('telefon') ? Input::old('telefon') : $web_kupac->telefon }}">
							<div class="error red-dot-error">{{ $errors->first('telefon') ? $errors->first('telefon') : "" }}</div>
						</div>

						<div class="col-md-6 col-sm-6 col-xs-12 cart-user-in-form"> 
							<!-- <label for="without-reg-city"><span class="red-dot-error"><i class="fas fa-star-of-life"></i></span> {{ Language::trans('Mesto') }}/{{ Language::trans('Grad') }}</label>				  -->
							<input type="text" name="mesto" tabindex="6" placeholder="{{ Language::trans('Grad ili mesto') }}" value="{{ Input::old('mesto') ? Input::old('mesto') : $web_kupac->mesto }}">
							<div class="error red-dot-error">{{ $errors->first('mesto') ? $errors->first('mesto') : "" }}</div>
						</div>
					</div>

				@endif

				<div class="col-md-4 col-sm-4 col-xs-12 no-padding">
					<div class="row bg-divider"> 
						<label class="payment-label">{{ Language::trans('Koji način plaćanja ti odgovara?') }}</label>
						<div class="col-md-12 col-sm-12 col-xs-12 no-padding form-group">
							<ul class="payment-method">
							@foreach(DB::table('web_nacin_placanja')->where('selected',1)->orderBy('b2c_default','desc')->get() as $key => $row)
								<li>
									<input type="radio" id="pay-method-{{ $row->web_nacin_placanja_id }}" name="web_nacin_placanja_id" value="{{ ($row->web_nacin_placanja_id) }}" {{ (Input::old('web_nacin_placanja_id') && Input::old('web_nacin_placanja_id') == $row->web_nacin_placanja_id) ? 'checked' : $key == 0 ? 'checked' : '' }}> <label for="pay-method-{{ $row->web_nacin_placanja_id }}" class="pay-method-{{ $row->web_nacin_placanja_id }}"><span class="payment-span">{{ Language::trans($row->naziv) }}</span></label>
								</li>
							@endforeach
							</ul>
						</div>
					</div>
				</div>

			<!-- Nacin isporuke -->
			<div class="col-md-4 col-sm-4 col-xs-12 no-padding pull-right">
				<div class="row bg-divider">
					<label for="" class="delivery-label">{{ Language::trans('Način isporuke') }}:</label>
					<div class="col-md-12 col-sm-12 col-xs-12 no-padding form-group">
						<ul class="delivery-method">
							@foreach(DB::table('web_nacin_isporuke')->where('selected', 1)->get() as $key => $nacinIsporuke)
								<li>
									<input type="radio" id="delivery-method-{{ $nacinIsporuke->web_nacin_isporuke_id }}" name="web_nacin_isporuke_id" value="{{ $nacinIsporuke->web_nacin_isporuke_id }}" {{ (Input::old('web_nacin_isporuke_id') && Input::old('web_nacin_isporuke_id') == $nacinIsporuke->web_nacin_isporuke_id) ? 'checked' : $key == 0 ? 'checked' : '' }}>  
									<label for="delivery-method-{{ $nacinIsporuke->web_nacin_isporuke_id }}" class="delivery-method-{{ $nacinIsporuke->web_nacin_isporuke_id }}"> 
										<span class="delivery-span">{{ Language::trans($nacinIsporuke->naziv) }}</span>	
									</label>	
								</li>
							@endforeach
						</ul>	
					</div>
				</div>
			</div>


				<!-- <div class="col-md-4 col-sm-4 col-xs-12 no-padding">
					<div class="row bg-divider">
						<label class="payment-label">{{ Language::trans('Način isporuke') }}:</label>
						@foreach(DB::table('web_nacin_isporuke')->where('selected',1)->get() as $nacinIsporuke)
						<label class="padding-v-5">
							<input type="radio" class="JSdeliveryInput web_nacin_isporuke_{{ $nacinIsporuke->web_nacin_isporuke_id }}" name="web_nacin_isporuke_id" value="{{ $nacinIsporuke->web_nacin_isporuke_id }}">
							{{ $nacinIsporuke->naziv }}
						</label>
						@endforeach
					</div>
				</div> -->
			<!-- Kraj nacina isporuke -->



			</div>

			@if(Options::neregistrovani_korisnici()==1 OR Session::has('b2c_kupac'))
			@if(Session::has('b2c_kupac') AND Options::web_options(314)==1)
			<div class="row">
				<div class="col-md-6 col-sm-12 col-xs-12 form-group">
					<label>{{ Language::trans('Bodovi') }}</label>
					<div>{{ Language::trans('Ovom kupovinom broj bodova koji možete ostvariti je') }} <span id="JSAchievePoints">{{ Cart::bodoviOstvareniBodoviKorpa() }}</span>.</div>
					<div>{{ Language::trans('Broj bodova kiji imate je') }} {{ WebKupac::bodovi() }}.
						@if(!is_null($trajanjeBodova = WebKupac::trajanjeBodova()))
						{{ Language::trans('Rok važenja bodova je') }} {{ $trajanjeBodova }}.
						@endif
					</div>
					<div>{{ Language::trans('Maksimalni broj bodova kiji možete iskoristiti u ovoj kupovini je') }} <span id="JSMaxUsingPoints">{{ Cart::bodoviPopustBodoviKorpa() }}</span>.</div>
				</div>
				<div class="col-md-6 col-sm-12 col-xs-12 form-group">
					<label>{{ Language::trans('Unesite bodove koje želite da iskoristite') }}:</label>
					<input type="text" name="bodovi" tabindex="6" value="{{ htmlentities(Input::old('bodovi') ? Input::old('bodovi') : '') }}">
					<div class="error red-dot-error">{{ Session::has('bodovi_error') ? Session::get('bodovi_error') : '' }}</div>
				</div>
			</div>		
			@endif

			<div class="row"> 

				<div class="text-center col-xs-12 form-group no-padding"> 
					<label class="hidden-xs">&nbsp;</label>
					<div class="capcha text-center"> 
						{{ Captcha::img(5, 160, 50) }}<br>
						<span>{{ Language::trans('Unesite kod sa slike') }}</span>
						<input type="text" name="captcha-string" tabindex="10" autocomplete="off">
						<div class="error red-dot-error">{{ $errors->first('captcha') ? $errors->first('captcha') : "" }}</div>
					</div>
				</div>
			</div>
			@endif
		</div>

			@if(Options::neregistrovani_korisnici()==1 OR Session::has('b2c_kupac'))
			<div class="text-center JStosFlag">
				<div>
					{{ Language::trans('Saglasan/a sam i prihvatam Uslove korišćenja') }}
				</div>
				<input type="checkbox" id="JSConditionsCart"> <a href="{{ Options::base_url() }}uslovi-kupovine" target="_blank">
				{{ Language::trans('Uslovi korišćenja') }}</a><br>
			</div>
			<div class="cart-bottom-order flex">
				<div class="JScart-back hidden"><span class="menu-icon fas fa-angle-left fa-2x"></span></div>
				<button id="JSOrderSubmit" disabled="disabled" class="bg-button button text-uppercase">{{ Language::trans('Poruči') }}</button>
			</div>
			@endif

			<input type="hidden" name="flag_vrsta_kupca" value="{{ Input::old('flag_vrsta_kupca') == 1 ? '1' : '0' }}">
		</form>	
	</div>

	@else
	<div class="empty-cart">
		<img src="../images/cart_empty.png" alt="cart_empty" class="img-responsive margin-auto">
		<div class="empty-page-label">{{ Language::trans('Tvoja korpa je i dalje prazna') }}.</div>
		<div class="empty-page-label-2">{{ Language::trans('Na sreću, postoji lako rešenje') }}.</div>
		<a href="{{Options::base_url()}}"><button class="text-center button bg-button text-uppercase">{{ Language::trans('Kreni u kupovinu') }}</button></a>
	</div>

	@endif
</div>

<br>

@endsection
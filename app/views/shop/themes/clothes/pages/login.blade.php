@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
<div class="row login-page-padding hidden">
	<!-- <div class="col-md-5 login-page-image hidden">
		<img class="hidden" src="{{ Options::domain() }}images/login.jpg" alt="login-image">
 	</div> -->

	<div class="col-md-4 login-form-wrapper">
		 <form action="{{ Options::base_url()}}login" method="post" class="login-form" autocomplete="off">
			<div class="row">
				<div class="col-md-9 col-md-offset-3">
					<p class="welcome-login"><span>Dobrodošli</span>Za pristup Vašem nalogu unesite E-mail i lozinku.</p>
				</div>
			</div>
			<div class="field-group row">
				<div class="col-md-3"> 
 					<label for="email_login">E-mail</label> 
				</div>

				<div class="col-md-9">
					<?php if(Input::old('email_login')){ $old_mail = Input::old('email_login'); }else{ if(Input::old('email_fg')){ $old_mail = Input::old('email_fg'); }else{$old_mail ='';} } ?>
					<input class="login-form__input" placeholder="E-mail adresa" name="email_login" type="text" value="{{ $old_mail }}" autocomplete="off">
				</div>
			</div>
			
			<div class="field-group row">
				<div class="col-md-3"> 
					<label for="lozinka_login">Lozinka</label>
				</div>
				<div class="col-md-9"> 
				    <input class="login-form__input" placeholder="Lozinka" autocomplete="off" name="lozinka_login" type="password" value="{{ Input::old('lozinka_login') ? Input::old('lozinka_login') : '' }}">
				</div>
			 </div>

			 <div class="row"> 
			 	<div class="col-md-2"> 
					<button type="submit" class="login-form-button admin-login">Login</button>
				</div>
			</div>
		</form>

		<div class="field-group error-login">
			<div class="row"> 
				<div class="col-md-9 wrong-email"> 
			<?php if($errors->first('email_login')){ echo $errors->first('email_login'); }elseif($errors->first('lozinka_login')){ echo $errors->first('lozinka_login'); } ?>

			@if(Session::get('confirm'))
				Niste potvrdili registraciju.<br>Posle registracije dobili ste potvrdu na vašu e-mail adresu!
			@endif

			@if(Session::get('message'))
				Novu lozinku za logovanje dobili ste na navedenu e-mail adresu.
			@endif
				</div>
			</div>
		</div>

		<div class="row"> 
			<div class="col-md-8"> 
				<form class="forgot_pass" action="{{ Options::base_url()}}zaboravljena-lozinka" method="post" autocomplete="off">
					<div class="error">{{ $errors->first('email_fg') ? $errors->first('email_fg') : "" }}</div>
					<?php if(Input::old('email_fg')){ $old_mail_fg = Input::old('email_login'); }else{ if(Input::old('email_login')){ $old_mail_fg = Input::old('email_login'); }else{$old_mail_fg ='';} } ?>
					<input name="email_fg" type="hidden" value="{{ $old_mail_fg }}" >
					<button class="btn-forgot-pass" type="submit">Zaboravili ste lozinku?</button>
				</form>
			</div>
		</div>
	</div> <!-- end of .login-form-wrapper -->
	<!-- <div class="large-1 large-push-6 form-spacer">&nbsp;</div> -->
</div>
<!-- ------ -->
<div id="loginModal" role="dialog">
    <div class="bg-login">
        <div>
            <div>
                <button type="button" class="close hidden" data-dismiss="modal"><i class="menu-icon fas fa-times"></i></button>
                <h4 class="bg-width login-title text-center login-set">{{ Language::trans('Prijavi se') }}</h4>
                <h4 class="bg-width login-title text-center password-set">{{ Language::trans('Zaboravio si/la lozinku?') }}</h4>
            </div>
            <div class="password-set bg-width">
            	{{ Language::trans('Molimo te da uneseš svoju email adresu. Stići će ti link putem kog ćeš moći da napraviš novu lozinku.') }}
            </div>
            <div class="login-wrap">
                <div class="form-group">
                    <!-- <label for="JSemail_login">{{ Language::trans('E-mail') }}</label> -->
                    <input class="bg-width" placeholder="{{ Language::trans('E-mail') }}" id="JSemail_login" type="text" value="" autocomplete="off">
                </div>
                <div class="form-group login-set">
                    <!-- <label for="JSpassword_login">{{ Language::trans('Lozinka') }}</label> -->
                    <input class="bg-width" placeholder="{{ Language::trans('Lozinka') }}" autocomplete="off" id="JSpassword_login" type="password" value="">
                </div>
            </div>

            <!-- <div class="modal-body">
                <a class="login-icons row facebook-registration" href="/fb-authorization">
                    <button type="submit" class="no-padding row" >
                        <i class="fab fa-facebook-f facebook col-md-3"></i><span class="text-center col-md-9"> {{ Language::trans('Facebook prijava') }}</span>
                    </button>
                </a>
                <a class="login-icons row google-registration" href="/google-authorization">
                    <button type="submit" class="no-padding" >
                        <i class="fab fa-google-plus-g google-plus"></i><span class="text-center"> {{ Language::trans('Google prijava') }}</span>
                    </button>
                </a>
            </div> -->
            <div class="row text-center margin-auto">
                <a class="button inline-block text-center hidden" href="{{Options::base_url()}}{{ Url_mod::slug_trans('registracija') }}">{{Language::trans('Registruj se')}}</a>
                
                <div class="col-xs-12 no-padding login-set">
                	<button type="submit" id="login" onclick="user_login()" class="button bg-button">{{ Language::trans('Prijavi se') }}</button>
                </div>

                <div class="col-xs-12 no-padding login-set">
                	<button class="forgot-psw no-padding">{{ Language::trans('Zaboravio si/la lozinku?') }}</button>
                </div>

                <div class="col-xs-12 no-padding password-set">
                	<button type="submit" onclick="user_forgot_password()" class="button bg-button">{{ Language::trans('Pošalji') }}</button>
                </div>

                <div class="field-group error-login JShidden-msg col-xs-12" id="JSForgotSuccess"><br>
                    {{ Language::trans('Novu lozinku za logovanje dobili ste na navedenoj e-mail adresi') }}.
                </div> 
            </div>

            <div class="text-center margin-auto soc-wrap login-set">
                <div class="flex bg-button login-google">
                    <a class="google-registration text-uppercase" href="/google-authorization">
						<span>{{ Language::trans('Prijavi se putem google-a') }}
                    </a>
                </div>

                <div class="flex bg-button login-facebook">
                    <a class="facebook-registration text-uppercase" href="/fb-authorization">
						{{ Language::trans('Prijavi se putem facebook-a') }}
                    </a>
                </div>
            </div>

            <div class="register-line col-xs-12 no-padding login-set">
            	<a href="{{Options::base_url()}}{{ Url_mod::slug_trans('registracija') }}" class="no-padding">{{ Language::trans('Nemaš nalog? Napravi ga') }}</a>
            </div>
        </div>   
    </div>
</div>
@endsection
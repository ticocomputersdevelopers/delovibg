

<script src="{{Options::domain()}}/js/slick.min.js" ></script>		 
<link href="{{Options::domain()}}css/slick.css" rel="stylesheet" type="text/css" />



<div class="single-new half-width">

	<h2><span class="blog-title center-block text-center">{{ $naslov }}</span></h2>

	<div class="blogs-date text-uppercase">
		{{ Support::date_convert($datum) }}  
	</div>


	@if(in_array(Support::fileExtension($slika),array('jpg','png','jpeg','gif'))) 

		<div class="bg-img center-block" style="background-image: url('{{ $slika }}');"></div>

	@else

		<iframe src="{{ $slika }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>     

	@endif


	<div class="soc-cont">    
		<a class="fb fab fa-facebook-f" href="https://www.facebook.com/sharer.php?u={{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($id) }}" target="_blank" onclick="javascript:window.open(this.href,
	  	'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"></a>	

		<a class="tw fab fa-twitter" href="https://twitter.com/share?url={{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($id) }}" onclick="javascript:window.open(this.href,
	  	'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"></a>

		<a class="ln fab fa-linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url={{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($id) }}" onclick="javascript:window.open(this.href,
	  	'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"></a>	

		<a class="pin fab fa-pinterest-p" href="https://www.pinterest.com/share?url={{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($id) }}" onclick="javascript:window.open(this.href,
	  	'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"></a>	

        <a class="vib fab fa-viber" href="viber://forward?text={{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($id) }}"></a>

        <a class="mail fas fa-mail-bulk" href="mailto:?subject={{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($id) }}&body={{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($id) }}"></a>   
	</div>

	<br>

	<!-- DESCRIPTION -->
 	{{ $sadrzaj }} 


	<div class="JSblog-slick row">   
		
		@foreach(All::getShortListNews($id) as $new) 
		<div class="col-md-4">
			<div class="card-blog"> 
				@if(in_array(Support::fileExtension($new->slika),array('jpg','png','jpeg','gif')))

					<a class="bg-img center-block" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($new->web_vest_b2c_id) }}" style="background-image: url('{{ $new->slika }}');"></a>

				@else

					<iframe src="{{ $new->slika }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>                   

				@endif

				<h3 class="blogs-title">
					<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($new->web_vest_b2c_id) }}">{{ $new->naslov }}</a>
				</h3>

				<div class="blogs-date text-uppercase">
					{{ Support::date_convert($new->datum) }}
				</div>
			</div>
		</div>
		@endforeach 

	</div> 
  

</div>  
@endsection
@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
<div id="categories-scroll" class="flex">
  <a class="JSCategory-section relative" href="#categories-first"></a>
  <a class="JSCategory-section relative" href="#categories-second"></a>
  <a class="JSCategory-section relative" href="#categories-third"></a>
</div>
    <div class="above-categories">
        <div class="page-title">
            <h2>{{ Language::trans('Kategorije') }}</h2>
        </div>
    </div>
    <div class="categories-section" data-spy="scroll" data-target="#categories-scroll" data-offset="0">    
        <!-- <div id="categories-first" class=" flex">
            <?php 
                $query_category_first=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>0,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); 
            ?>

            @foreach ($query_category_first as $row1)
                @if(Groups::broj_cerki($row1->grupa_pr_id) >0)
                <a id="{{ Language::trans($row1->grupa_pr_id)  }}" class="">{{ Language::trans($row1->grupa) }} <span class="">({{Articles::brojiArtikleRekurzivno($row1->grupa_pr_id)}})</span></a>
                <a class="category-label text-bold" href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}"><span>{{ Language::trans($row1->grupa) }} ({{Articles::brojiArtikleRekurzivno($row1->grupa_pr_id)}})</span></a>
                    <div class="groups-list no-padding">
                        <div class="groups-list-card">
                            <div class="group-list-img-wrp relative">
                                <a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}" class="flex">
                                    <img src="{{ Options::domain() }}{{$row1->putanja_slika}}" alt="{{ $row1->grupa }}" />
                                </a>
                            </div>
                            <span id="{{ Language::trans($row1->grupa_pr_id)  }}" class="groups-list-title text-bold inline-block">{{ Language::trans($row1->grupa) }}</span>
                            <span class="text-bold inline-block categories-count">{{Articles::brojiArtikleRekurzivno($row1->grupa_pr_id)}} {{ Language::trans('artikla') }}</span>
                        </div>
                    </div>
                @endif
            @endforeach
        </div> -->

        
            <!-- REMOVED TO SHOW ALL CATEGORIES -->
        

        <div id="categories-second" class=" flex">
            <?php 
                $query_category_first=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>0,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); 
            ?>

            @foreach ($query_category_first as $row1)
                <div class="JScategory-wrap flex">
                <?php $query_category_second=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row1->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc') ?>
    
                    <span class="category-dropdown-label">
                        @if(Groups::broj_cerki($row1->grupa_pr_id) >0)
                            <i class="fas fa-angle-right"></i>
                        @else
                            <div class="single-category-prefix">&nbsp;</div>
                        @endif
                    </span>
                    <a class="category-label text-bold" href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}"><span>{{ Language::trans($row1->grupa) }}</span>
                    <span class="category-label category-count hidden">{{Articles::brojiArtikleRekurzivno($row1->grupa_pr_id)}}<span class="JScategory-count-text">{{ Language::trans('Artikla') }}</span></span>
                    </a>
                    <!-- <div class="category-dropdown-menu"> THIS IS WRAPPED IN JS -->
                    @foreach ($query_category_second->get() as $row2)
                       <div class="groups-list no-padding">
                            <div class="groups-list-card">
                                <div class="group-list-img-wrp relative">
                                    <a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}" class="flex">
                                        <img src="{{ Options::domain() }}{{$row2->putanja_slika}}" alt="{{ $row2->grupa }}" />
                                    </a>
                                </div>
                                <span id="{{ Language::trans($row2->grupa_pr_id)  }}" class="groups-list-title text-bold inline-block">{{ Language::trans($row2->grupa) }}</span>
                                <span class="text-bold inline-block categories-count">{{Articles::brojiArtikleRekurzivno($row2->grupa_pr_id)}} {{ Language::trans('artikla') }}</span>
                            </div>
                        </div>
                    @endforeach
                    <!-- </div> -->
                </div>
            @endforeach
        </div>

        <!-- <div id="categories-third" class=" flex">
                <?php $query_category_first=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>0,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>

                @foreach ($query_category_first as $row1)
                    @if(Groups::broj_cerki($row1->grupa_pr_id) >0)

                    <?php $query_category_second=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row1->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc') ?>

                        @foreach ($query_category_second->get() as $row2)
                            <?php $query_category_third=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row2->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
                            <a class="category-label text-bold" href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}"><span>{{ Language::trans($row2->grupa) }} ({{Articles::brojiArtikleRekurzivno($row2->grupa_pr_id)}})</span></a>

                            @foreach($query_category_third as $row3)
                                <div class="groups-list no-padding">
                                    <div class="groups-list-card">
                                        <div class="group-list-img-wrp relative">
                                            <a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row3->grupa) }}" class="flex">
                                                <img src="{{ Options::domain() }}{{$row3->putanja_slika}}" alt="{{ $row3->grupa }}" />
                                            </a>
                                        </div>
                                        <span id="{{ Language::trans($row3->grupa_pr_id)  }}" class="groups-list-title text-bold inline-block">{{ Language::trans($row3->grupa) }}</span>
                                        <span class="text-bold inline-block categories-count">{{Articles::brojiArtikleRekurzivno($row3->grupa_pr_id)}} {{ Language::trans('artikla') }}</span>
                                    </div>
                                </div>
                            @endforeach
                        @endforeach
                    @endif
                @endforeach
            </div>
        </div> -->
    </div>

    <div class="category-listing col-md-9 col-sm-12 col-xs-12 no-padding hidden">
        <?php 
            $query_category_first=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>0,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); 
        ?>

        @foreach ($query_category_first as $row1)
            @if(Groups::broj_cerki($row1->grupa_pr_id) >0)
       
                <h2 id="{{ Language::trans($row1->grupa_pr_id)  }}" class="category-heading">{{ Language::trans($row1->grupa) }} <span class="category__list__item__link__span">({{Articles::brojiArtikleRekurzivno($row1->grupa_pr_id)}})</span></h2>

                <?php $query_category_second=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row1->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc') ?>

                @foreach ($query_category_second->get() as $row2)
                   <div class="link-wrap">
                        <a class="category-name-link" href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}">{{ Language::trans($row2->grupa) }} <span class="category__list__item__link__span">({{Articles::brojiArtikleRekurzivno($row2->grupa_pr_id)}})</span></a>

                        <ul class="category__list">
                            <?php $query_category_third=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row2->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
                            @foreach($query_category_third as $row3)
                            <li class="category__list__item">
                                <a class="category__list__item__link" href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}/{{ Url_mod::slug_trans($row3->grupa) }}">{{ Language::trans($row3->grupa) }} 
                                    <span class="category__list__item__link__span">({{Articles::brojiArtikleRekurzivno($row3->grupa_pr_id)}})</span>
                                </a>
                                <ul class="category__list">
                                    <?php $query_category_forth=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row3->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
                                    @foreach($query_category_forth as $row4)
                                    <li class="category__list__item">
                                        <a class="category__list__item__link" href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}/{{ Url_mod::slug_trans($row3->grupa) }}/{{ Url_mod::slug_trans($row4->grupa) }}">{{ Language::trans($row4->grupa) }} 
                                            <span class="category__list__item__link__span">({{Articles::brojiArtikleRekurzivno($row4->grupa_pr_id)}})</span>
                                        </a>
                                    </li>
                                    @endforeach
                                </ul>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                @endforeach
 
                <!-- END ROW 1 -->
            @else
        
                <h2 id="{{ Language::trans($row1->grupa_pr_id)  }}" class="category-heading">{{ Language::trans($row1->grupa)  }} 
                    <span class="category__list__item__link__span">({{Articles::brojiArtikleRekurzivno($row1->grupa_pr_id)}})</span></h2>

                <?php $query_category_second=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row1->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc') ?>

                @foreach ($query_category_second->get() as $row2)
                   <div class="link-wrap">
                        <a class="category-name-link" href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}">{{ Language::trans($row2->grupa) }} <span class="category__list__item__link__span">({{Articles::brojiArtikleRekurzivno($row2->grupa_pr_id)}})</span></a>

                        <ul class="category__list">
                            <?php $query_category_third=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row2->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
                            @foreach($query_category_third as $row3)
                            <li class="category__list__item">
                                <a class="category__list__item__link" href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}/{{ Url_mod::slug_trans($row3->grupa) }}">{{ Language::trans($row3->grupa) }} 
                                    <span class="category__list__item__link__span">({{Articles::brojiArtikleRekurzivno($row3->grupa_pr_id)}})</span>
                                </a>
                                <ul class="category__list">
                                    <?php $query_category_forth=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row3->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
                                    @foreach($query_category_forth as $row4)
                                    <li class="category__list__item">
                                        <a class="category__list__item__link" href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}/{{ Url_mod::slug_trans($row3->grupa) }}/{{ Url_mod::slug_trans($row4->grupa) }}">{{ Language::trans($row4->grupa) }} 
                                            <span class="category__list__item__link__span">({{Articles::brojiArtikleRekurzivno($row4->grupa_pr_id)}})</span>
                                        </a>
                                    </li>
                                    @endforeach
                                </ul>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                @endforeach 

            @endif
        @endforeach 

    </div> 

     <!-- MAIN CONTENT -->
    <div class="sticky-element col-md-3 col-sm-3 col-xs-3 hidden-sm hidden-xs hidden">  
        <ul class="JScategory-sidebar__list">
            <li class="JScategory-sidebar__list__toggler"></li>
            <li> 
                <ul class="scrollable"> 
                    <div class="JSproduct-list ">
                    @foreach ($query_category_first as $row1)
                    <li class="category-sidebar__list__item">
                        <a href="#{{ Language::trans($row1->grupa_pr_id) }}" class="category-sidebar__list__item__link">{{ Language::trans($row1->grupa)  }} 
                            <span class="category-sidebar__list__item__link__span">({{Articles::brojiArtikleRekurzivno($row1->grupa_pr_id)}})</span>
                        </a>
                    </li>
                    @endforeach 
                    </div>
                </ul>
            </li>
        </ul>

    </div>

@endsection
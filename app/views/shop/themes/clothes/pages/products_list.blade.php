@extends('shop/themes/'.Support::theme_path().'templates/products')

@section('products_list')



<!-- <div class="text-center">
	{{ Paginator::make($articles, $count_products, $limit)->links() }}
</div> -->
 
   <!-- MODAL FOR COMPARED ARTICLES -->
  <div class="modal fade" id="compared-articles" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
          <h4 class="modal-title text-center">{{ Language::trans('Upoređeni artikli') }}</h4>
        </div>
        <div class="modal-body">
			<div class="compare-section">
				<div id="compare-article">
					<div id="JScompareTable" class="compare-table text-center table-responsive"></div>
				</div>
			</div>
        </div>
        <div class="modal-footer">
          <button type="button" class="button" data-dismiss="modal">{{ Language::trans('Zatvori') }}</button>
        </div>
      </div>    
    </div>
  </div>

<!-- PRODUCTS -->
@if(Session::has('list') or Options::product_view()==3)
<!-- LIST PRODUCTS -->
<div class="JSproduct-list list-view">
	@foreach($articles as $row)
		@include('shop/themes/'.Support::theme_path().'partials/products/product_on_list') 
	@endforeach
</div>

@else
<!-- Grid proucts -->
<div class="JSproduct-list row">
	@foreach($articles as $row)
		@include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
	@endforeach
</div>		
@endif

@if($count_products == 0) 
	<br>
	<div class="empty-page-label"> 
		 {{ Language::trans('Trenutno nema artikla za date kategorije') }} 
	</div> 
@endif

<div class="text-center"> 
	{{ Paginator::make($articles, $count_products, $limit)->links() }}
</div>

@endsection
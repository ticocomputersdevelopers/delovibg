@extends('shop/themes/'.Support::theme_path().'templates/article')

@section('article_details')
 
<div id="fb-root"></div> 
<script>(function(d, s, id) {
var js, fjs = d.getElementsByTagName(s)[0];
if (d.getElementById(id)) return;
js = d.createElement(s); js.id = id;
js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12';
fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

@if(Session::has('success_add_to_cart'))
    $(document).ready(function(){    
        alertSuccess("{{ Language::trans('Artikal je dodat u korpu') }}.");
    });
@endif
@if(Session::has('success_comment_message'))
$(document).ready(function(){
    alertSuccess("{{ Language::trans('Vaš komentar je poslat') }}.");
});
@endif
</script>

<div class="d-content JSmain relative"> 
    <div class="container-fluid"> 
 
       <!--  <ul class="breadcrumb"> 
            {{ Product::product_bredacrumps(DB::table('roba')->where('roba_id',$roba_id)->pluck('grupa_pr_id')) }}
        </ul>
 -->
        <div id="product-preview" class="clearfix">

            <div class="row bg-article">
                <div class="product-preview-info col-md-6 col-sm-6 col-xs-12">
                    <div class="row">
                       <!-- ADMIN BUTTON-->
                        @if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array('ARTIKLI')) AND Admin_model::check_admin(array('ARTIKLI_PREGLED')))
                            <div class="admin-article inline-block"> 
                                @if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
                                    <a class="article-level-edit-btn JSFAProductModalCall" data-roba_id="{{$roba_id}}" href="javascript:void(0)" rel="nofollow">{{ Language::trans('IZMENI ARTIKAL') }}</a> 
                                @endif
                                <span class="supplier"> {{ Product::get_dobavljac($roba_id) }}</span> 
                                <span class="supplier">{{ Language::trans('NCP') }}: {{ Product::get_ncena($roba_id) }}</span>
                            </div>
                        @endif
                        <div class="col-md-10 col-sm-12 col-xs-12 sm-product-flex">

                            <h1 class="article-heading">{{ Product::seo_title($roba_id) }}</h1>

                            <!-- PRICE -->
                            <div class="product-preview-price sm-order-0">
                            @if(Product::getStatusArticlePrice($roba_id) == 1)
                                <span class="pricelabel">CENA: </span>
                                <!-- @if(Product::pakovanje($roba_id))
                                    <div>
                                        <span class="price-label">{{Language::trans('Pakovanje')}}: </span>
                                        <span class="price-num">{{ Product::ambalaza($roba_id) }}</span>
                                    </div>
                                @endif   -->   

                                @if(All::provera_akcija($roba_id))                                      
                                   
                                    @if(Product::get_mpcena($roba_id) != 0)
                                    <div class="hidden inline-block">
                                        <span class="price-label hidden">{{Language::trans('MP cena')}}: </span>
                                        <span class="price-num">{{ Cart::cena(Product::get_mpcena($roba_id)) }}</span>
                                    </div>
                                    @endif 

                                    <div class="inline-block">
                                        <span class="price-label hidden" >{{Language::trans('Akcija')}}:</span>
                                        <span class="JSaction_price price-num" data-akc_cena="{{Product::get_price($roba_id)}}">{{ Cart::cena(Product::get_price($roba_id,true,false,(!is_null(Input::old('kamata')) ? Input::old('kamata') : 0 ))) }}</span>
                                    </div>

                                    @if(Product::getPopust_akc($roba_id)>0)
                                     <div class="hidden inline-block">
                                        <span class="price-label hidden">{{Language::trans('Ušteda')}}:</span>
                                        <span class="price-num">{{ Cart::cena(Product::getPopust_akc($roba_id)) }}</span> 
                                    </div>
                                    @endif

                                @else

                                    @if(Product::get_mpcena($roba_id) != 0)
                                        <div class="hidden inline-block">
                                            <span class="price-label hidden">{{Language::trans('MP cena')}}: </span>
                                            <span class="price-num">{{ Cart::cena(Product::get_mpcena($roba_id)) }}</span>
                                        </div>
                                    @endif

                                    <div>
                                        <span class="price-label hidden">{{Language::trans('Web cena')}}:</span>
                                        <span class="JSweb_price price-num" data-cena="{{Product::get_price($roba_id)}}">
                                           {{ Cart::cena(Product::get_price($roba_id,true,false,(!is_null(Input::old('kamata')) ? Input::old('kamata') : 0 ))) }}
                                        </span>
                                    </div>

                                    @if(Product::getPopust($roba_id)>0)
                                        @if(AdminOptions::web_options(132)==1)
                                            <span class="price-label hidden">{{Language::trans('Ušteda')}}:</span>
                                            <span class="price-num hidden">{{ Cart::cena(Product::getPopust($roba_id)) }}</span>
                                        @endif
                                    @endif

                                @endif
                            @endif 
                            <div class="fine-print">*Cene su prikazane sa PDV-om</div>
                            </div>


                            <!-- COLOR HOLDER  -->
                            

                            <!-- ADD TO CART -->
                            <div class="add-to-cart-area clearfix sm-order-2">     
                                 @if(Product::getStatusArticle($roba_id) == 1)
                                    @if(Cart::check_avaliable($roba_id) > 0)
                                    <form method="POST" action="{{ Options::base_url() }}product-cart-add" id="JSAddCartForm" class="article-form"> 

                                    @if(Product::check_osobine($roba_id))
                                       
                                        @foreach(Product::osobine_nazivi($roba_id) as $osobina_naziv_id)
                                            <div class="attributes text-bold">
                                               
                                                <div>{{ Product::find_osobina_naziv($osobina_naziv_id,'naziv') }}</div>
                                                 
                                                @foreach(Product::osobine_vrednosti($roba_id,$osobina_naziv_id) as $osobina_vrednost_id)
                                                <label class="relative" style="background-color: {{ Product::find_osobina_vrednost($osobina_vrednost_id, 'boja_css') }}">

                                                    <input type="radio" name="osobine{{ $osobina_naziv_id }}" value="{{ $osobina_vrednost_id }}" {{ Product::check_osobina_vrednost($roba_id,$osobina_naziv_id,$osobina_vrednost_id,Input::old('osobine'.$osobina_naziv_id)) }}>
                                                
                                                    <!-- <span class="inline-block">{{ Product::osobina_vrednost_checked($osobina_naziv_id, $osobina_vrednost_id) }}</span> -->
                                                    <span class="inline-block">{{ Product::find_osobina_vrednost($osobina_vrednost_id, 'vrednost') }}</span>
         
                                                </label>
                                                @endforeach
                                         
                                            </div>
                                        @endforeach
                              
                                    @endif

                                    @if(AdminOptions::web_options(313)==1) 
                                    <div class="num-rates"> 
                                        <div> 
                                            <div class="inline-block lorem-1">{{ Language::trans('Broj rata') }}</div>
                                        </div>
                                        <select class="JSKamata" name="kamata">
                                            {{ Product::broj_rata(Input::old('kamata')) }}
                                        </select>
                                    </div>
                                    @endif
                                    
                                    <!-- <div class="printer inline-block" title="{{ Language::trans('Štampaj') }}">  
                                        <a href="{{Options::base_url()}}stampanje/{{ $roba_id }}" target="_blank" rel="nofollow"><i class="fas fa-print"></i></a>
                                    </div> -->

                                    @if(Cart::kupac_id() > 0)
                                    <!-- <button type="button" class="like-it JSadd-to-wish" data-roba_id="{{$roba_id}}" title="{{ Language::trans('Dodaj na listu želja') }}"><i class="far fa-heart"></i></button> --> 
                                    @else
                                    <!-- <button type="button" class="like-it JSnot_logged" data-roba_id="{{$roba_id}}" title="{{ Language::trans('Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima') }}"><i class="far fa-heart"></i></button>  -->
                                    @endif  

                                    <input type="hidden" name="roba_id" value="{{ $roba_id }}">
                                    <div class="inline-block hidden">&nbsp;{{Language::trans('Količina')}}&nbsp;</div>
                                    <input type="text" name="kolicina" class="cart-amount hidden" value="{{ Input::old('kolicina') ? Input::old('kolicina') : '1' }}">
                                    <button type="submit" id="JSAddCartSubmit" class="button text-uppercase">{{Language::trans('Dodaj u korpu')}}</button>
                                     
                                    <input type="hidden" name="projectId" value=""> 
                                    
                                    <div>{{ $errors->first('kolicina') ? $errors->first('kolicina') : '' }}</div>  
                                 </form>
                                @else
                                    <!-- <button class="button not-available">{{Language::trans('Nije dostupno')}}</button>             -->
                                    <button class="button buy-btn JSenquiry JSQuickViewButton text-uppercase" data-roba_id="{{$roba_id}}" title="Pozovite">{{ Language::trans('POŠALJI UPIT') }}</button>                   

                                @endif

                                @else
                                <button class="button" data-roba-id="{{$roba_id}}">
                                    {{ Product::find_flag_cene(Product::getStatusArticle($roba_id),'naziv') }}
                                </button>
                                @endif
                            </div> 

                            <!-- <div class="rate-me-artical">
                                {{ Product::getRating($roba_id) }}
                            </div> -->

                            <!-- ARTICLE TOP DESCRIPTION -->
                            <div class="desc-wrap sm-order-4">  
                                <h4 class="hidden"><p>{{ Language::trans('Opis') }}</p></h4> 
                                <div class="description-section relative">
                                    <div class="desc-sec-wrap">
                                        {{ Product::get_opis($roba_id) }}
                                    </div>
                                    
                                    <div class="desc-short">
                                        
                                    </div>

                                    <span class="JSread-more">{{ Language::trans('Vidi više') }}</span>
                                    <span class="hidden">{{ Product::get_karakteristike($roba_id) }}</span>
                                </div>

                                <div class="row article-info">
                                    <div class="col-xs-12 bg-product-meta">
                                        <div class="row">
                                            <!-- ARTICLE PASSWORD -->
                                            <div class="desc-label col-md-3 col-xs-6 product-password">
                                                {{Language::trans('Šifra') }}:
                                            </div>

                                            @if(AdminOptions::sifra_view_web()==1)
                                            <div class="col-md-9 col-xs-6 article-info-text">
                                                {{Product::sifra($roba_id)}}
                                            </div>

                                            @elseif(AdminOptions::sifra_view_web()==4)
                                            <div class="col-md-9 col-xs-6 article-info-text">
                                                {{Product::sifra_d($roba_id)}}
                                            </div>

                                            @elseif(AdminOptions::sifra_view_web()==3)
                                            <div class="col-md-9 col-xs-6 article-info-text">
                                                {{Product::sku($roba_id)}}
                                            </div>

                                            @elseif(AdminOptions::sifra_view_web()==2)
                                            <div class="col-md-9 col-xs-6 article-info-text">
                                                {{Product::sifra_is($roba_id)}}
                                            </div>
                                            @endif

                                            <!-- ARTICLE GROUP -->
                                            @if($grupa_pr_id != -1)
                                            <div class="desc-label col-md-3 col-xs-6 product-group">
                                                {{Language::trans('Kategorija')}}:
                                            </div>
                                            <div class="col-md-9 col-xs-6 article-info-text">
                                                {{ Product::get_grupa($roba_id) }}
                                            </div>
                                            @endif

                                            <!-- ARTICLE MANUFACTURER -->
                                            @if($proizvodjac_id != -1)
                                            <div class="desc-label col-md-3 col-xs-6 product-manufacturer">
                                                {{Language::trans('Proizvođač')}}:
                                            </div>
                                            @if(Support::checkBrand($roba_id))
                                            <div class="col-md-9 col-xs-6 article-info-text">
                                                <a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('proizvodjac')}}/{{ Url_mod::slug_trans(Product::get_proizvodjac($roba_id)) }}">
                                                {{Product::get_proizvodjac($roba_id)}}
                                                </a>
                                            </div>
                                            @else
                                            <div class="col-md-9 col-xs-6 article-info-text">
                                                {{Product::get_proizvodjac($roba_id)}}
                                            </div>
                                            @endif
                                            @endif

                                            <!-- ARTICLE MODEL -->
                                            @if(Product::get_model($roba_id))
                                            <div class="desc-label col-md-3 col-xs-6 prdouct-model">
                                                {{Language::trans('Model')}}:
                                            </div>
                                            <div class="col-md-9 col-xs-6 article-info-text">
                                                {{Product::get_model($roba_id)}}
                                            </div>
                                            @endif
                                            
                                            <div class="col-md-12 col-xs-12 details-decleration">Deklaracija</div>
                                            @if(Product::get_barkod($roba_id))
                                            <div class="desc-label col-md-3 col-xs-6">
                                                EAN:
                                            </div>
                                            <div class="col-md-9 col-xs-6">
                                                {{Product::get_barkod($roba_id)}} 
                                            </div>
                                            @endif
                                            <div class="desc-label col-md-3 col-xs-6">
                                                {{Language::trans('Proizvođač')}}:
                                            </div>
                                            <div class="col-md-9 col-xs-6">
                                                {{Product::get_proizvodjac($roba_id)}}
                                            </div>

                                            <div class="desc-label col-md-3 col-xs-6">
                                                {{Language::trans('Uvoznik')}}:
                                            </div>
                                            <div class="col-md-9 col-xs-6">
                                                BG Elektronik DOO <br> Beograd
                                            </div>
                                            <div class="desc-label col-md-3 col-xs-6">
                                                {{Language::trans('Prava potrošača')}}:
                                            </div>
                                            <div class="col-md-9 col-xs-6">
                                                Zagarantovana sva prava kupaca <br> po osnovu zakona o zaštiti potrošača.
                                            </div>
                                            <div class="desc-label col-md-3 col-xs-6 JSinfoA1">
                                            </div>
                                            <div class="col-md-9 col-xs-6 JSinfoA2">
                                            </div>

                                            <div class="desc-label col-md-3 col-xs-6 JSinfoB1">
                                            </div>
                                            <div class="col-md-9 col-xs-6 JSinfoB2">
                                            </div>

                                            <div class="short_description hidden">{{ Product::get_short_opis($roba_id) }}</div>
                                        @if(Product::get_garancija($roba_id)>0)
                                            <div class="col-md-12 col-xs-12 details-warranty">Garancija</div>

                                            <div class="desc-label col-md-3 col-xs-6">
                                                {{Language::trans('Period reklamacije')}}:
                                            </div>
                                            <div class="col-md-9 col-xs-6">
                                                {{Product::get_garancija($roba_id)}} {{Language::trans('meseci')}}
                                            </div>
                                        @endif

                                             <div class="desc-label col-md-3 col-xs-6 JSinfoC1">
                                            </div>
                                            <div class="col-md-9 col-xs-6 JSinfoC2">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- ARTICLE PASSWORD -->
                            <!-- @if(AdminOptions::sifra_view_web()==1)
                            <div>{{Language::trans('Šifra') }}: {{Product::sifra($roba_id)}}</div>
                            @elseif(AdminOptions::sifra_view_web()==4)                       
                            <div>{{Language::trans('Šifra') }}: {{Product::sifra_d($roba_id)}}</div>
                            @elseif(AdminOptions::sifra_view_web()==3)                       
                            <div>{{Language::trans('Šifra') }}: {{Product::sku($roba_id)}}</div>
                            @elseif(AdminOptions::sifra_view_web()==2)                       
                            <div>{{Language::trans('Šifra') }}: {{Product::sifra_is($roba_id)}}</div>
                            @endif  -->

                  
                            <!-- <div class="product-manufacturer">                             
                                @if($proizvodjac_id != -1)
                                    @if( Product::slikabrenda($roba_id) != null )
                                    <a class="article-brand-img inline-block valign" href="{{Options::base_url()}}{{Url_mod::slug_trans('proizvodjac')}}/{{Url_mod::slug_trans(Product::get_proizvodjac($roba_id)) }}">
                                        <img src="{{ Options::domain() }}{{product::slikabrenda($roba_id) }}" alt="{{ product::get_proizvodjac($roba_id) }}" />
                                    </a>
                                    @else
                                    <a class="artical-brand-text inline-block" href="{{Options::base_url()}}{{Url_mod::slug_trans('proizvodjac')}}/{{Url_mod::slug_trans(Product::get_proizvodjac($roba_id)) }}">{{ product::get_proizvodjac($roba_id) }}</a>
                                    @endif                                   
                                @endif
                                
                            </div> -->
         

                            <!-- <ul>
                                 @if($grupa_pr_id != -1)
                                 <li class="product-group">{{Language::trans('Proizvod iz grupe')}}:{{ Product::get_grupa($roba_id) }}</li>
                                 @endif
                                @if($proizvodjac_id != -1)
                                <li class="product-manufacturer">{{Language::trans('Proizvođač')}}: 
                                    @if(Support::checkBrand($roba_id))
                                    <a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('proizvodjac')}}/{{ Url_mod::slug_trans(Product::get_proizvodjac($roba_id)) }}">
                                        {{Product::get_proizvodjac($roba_id)}}
                                    </a>
                                    @else
                                    <span class="article-manufacturer-text"> {{Product::get_proizvodjac($roba_id)}} </span>
                                    @endif
                                </li>
                                @endif  -->

                               <!--  @if(Options::vodjenje_lagera() == 1)
                                    <li class="product-available-amount">{{Language::trans('Dostupna količina')}}: <span> {{Cart::check_avaliable($roba_id)}}</span></li>
                                @endif -->

                                <!-- @if(Options::checkTezina() == 1 AND Product::tezina_proizvoda($roba_id)>0) -->
                                    <!-- <li class="product-available-amount">{{Language::trans('Težina artikla')}}:   {{Product::tezina_proizvoda($roba_id)/1000}} kg</li> -->
                                <!-- @endif -->
                                
        <!--                                 @if(Options::checkTags() == 1)
                                    @if(Product::tags($roba_id) != '')
                                    <li class="product-available-amount">
                                        {{Language::trans('Tagovi')}}: {{ Product::tags($roba_id) }}
                                    </li>
                                    @else
                                    <li class="product-available-amount">
                                        {{Language::trans('Tagovi')}}: {{Language::trans('Nema tagova')}}
                                    </li>
                                    @endif
                                @endif -->
                            <!-- </ul> -->

                        
                            
                          <!-- Facebook button -->
        <!--                         <div class="facebook-btn-share flex"> 
                                      <div class="soc-network inline-block"> 
                                         <div class="fb-like" data-href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($roba_id))}}" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
                                      </div> 
                                     <div class="soc-network"> 
                                        <a href="https://twitter.com/share" class="twitter-share-button">Tweet</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                                     </div>  
                                </div> -->
                    <!-- FEATURES -->
                         
                            
                            

                            @if(!empty(Product::get_labela($roba_id)))
                            <div class="custom-label inline-block relative">
                                <i class="fa fa-info-circle"></i>
                                {{Product::get_labela($roba_id)}} 
                            </div>
                            @endif 

                            <br>

                            <div class="product-tags">  
                                @if(Options::checkTags() == 1)
                                    @if(Product::tags($roba_id) != '')
                                        
                                        <span>{{ Language::trans('Tagovi') }}:</span>
                                   
                                        {{ Product::tags($roba_id) }} 
                                     
                                    @endif
                                @endif    
                            </div> 
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 JSproduct-preview-image">
                    
                    <div class="row"> 
                        <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12 disableZoomer hidden-xs no-padding">  
                            <a class="product-img-wrap" href="javascript:void(0);">
                                <img class="JSmain_img img-responsive" src="{{ Options::domain() }}{{ $slika_big }}" alt="{{ Product::seo_title($roba_id)}}" />  

                                @if(Cart::kupac_id() > 0)
                                <button class="like-it JSadd-to-wish" data-roba_id="{{$roba_id}}" title="{{ Language::trans('Dodaj na listu želja') }}"><div class="like-it-heart"></div></button> 
                                @else
                                <button class="like-it JSnot_logged" data-roba_id="{{$roba_id}}" title="{{ Language::trans('Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima') }}"><div class="like-it-heart"></div></button> 
                                @endif  
                            </a>
                            <!-- STICKER -->
                            <div class="product-sticker flex">
                                @if( B2bArticle::stiker_levo($roba_id) != null )
                                    <a class="article-sticker-img">
                                        <img class="img-responsive margin-auto" src="{{ Options::domain() }}{{B2bArticle::stiker_levo($roba_id) }}"  />
                                    </a>
                                @endif 
                                
                                @if( B2bArticle::stiker_desno($roba_id) != null )

                                       <a class="article-sticker-img">
                                        <img class="img-responsive margin-auto" src="{{ Options::domain() }}{{B2bArticle::stiker_desno($roba_id) }}"  />
                                    </a>
                                @endif   
                            </div>

                            <!-- CUSTOM MODAL -->
                            <div class="JSmodal">
                                <div class="flex full-screen relative"> 
                                  
                                    <div class="modal-cont relative text-center"> 
                                  
                                        <div class="inline-block relative"> 
                                            <span class="JSclose-modal"><i class="fas fa-times"></i></span>
                                      
                                            <img src="" class="img-responsive" id="JSmodal_img" alt="modal image">
                                        </div>

                                        <button class="btn JSleft_btn"><i class="fas fa-chevron-left"></i></button>
                                        <button class="btn JSright_btn"><i class="fas fa-chevron-right"></i></button>
                                    </div>
                            
                                </div>
                            </div>
                        </div> 

                        <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 text-center img-gallery-sidebar no-padding">
                            @foreach($slike as $image)
                            <div class="img-card">
                                <a href="javascript:void(0)" class="img-gallery flex JSimg-gallery"> 
                                    <img alt="{{ Options::domain().$image->putanja }}" id="{{ Options::domain().$image->web_slika_id }}" src="{{ Options::domain().$image->putanja }}"/>
                                </a>
                            </div>

                            @if(Cart::kupac_id() > 0)
                            <button class="like-it JSadd-to-wish hidden-to-xs" data-roba_id="{{$roba_id}}" title="{{ Language::trans('Dodaj na listu želja') }}"><div class="like-it-heart"></div></button> 
                            @else
                            <button class="like-it JSnot_logged hidden-to-xs" data-roba_id="{{$roba_id}}" title="{{ Language::trans('Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima') }}"><div class="like-it-heart"></div></button> 
                            @endif 

                            @endforeach
                        </div>
                    </div>
                   

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 additional_img">    
                            @foreach($glavne_slike as $slika)
                            <a class="inline-block text-center" href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($roba_id))}}/{{$slika->web_slika_id}}"> 
                                <img src="{{ Options::domain().$slika->putanja }}" alt=" {{ Options::domain().$slika->putanja }}" class="img-responsive inline-block"> 
                            </a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>

            <!-- RELATED PRODUCTS --> 
            <div class="related-products">
                <div class="row">
                    
                    
                    <div class="col-xs-12 no-padding sm-order-3 bought-together">         
                        @if(Options::web_options(118))
                        <br>
                        <h2 class="slickTitle"><span class="section-title">{{Language::trans('Često kupljeni zajedno')}}</span></h2>
                        <div class="JSbought_slick row">
                        @foreach($vezani_artikli as $vezani_artikl)
                            @if(Product::checkView($vezani_artikl->roba_id))
                            <div class="JSproduct col-md-3 col-sm-4 col-xs-12 no-padding">
                                <div class="shop-product-card relative"> 
                                    <!-- PRODUCT IMAGE -->
                                    <div class="product-image-wrapper relative">

                                        <a class="flex" href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($vezani_artikl->vezani_roba_id))}}">
                                            <img class="product-image img-responsive JSlazy_load" src="{{Options::domain()}}images/quick_view_loader.gif" data-src="{{ Options::domain() }}{{ Product::web_slika($vezani_artikl->vezani_roba_id) }}" alt="{{ Product::seo_title($vezani_artikl->vezani_roba_id) }}" />
                                        </a> 

                                    </div>

                                    <div class="product-meta"> 
                                        
                                        <span class="review hidden">{{ Product::getRating($vezani_artikl->roba_id) }}</span> 
                                        
                                        <h2 class="product-name">
                                            <a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($vezani_artikl->vezani_roba_id))}}">{{ Product::short_title($vezani_artikl->vezani_roba_id) }}</a>
                                        </h2>

                                        <div class="price-holder hidden">
                                         {{ Cart::cena(Product::get_price_vezani($vezani_artikl->roba_id,$vezani_artikl->vezani_roba_id)) }}
                                        </div>    
          
                                    </div>
                                    
                                    @if($vezani_artikl->flag_cena == 1)
                                        <div class="add-to-cart-container">
                                            <!-- WISH LIST  --> 
                                            <!-- @if(Cart::kupac_id() > 0)
                                            <button class="like-it JSadd-to-wish" data-roba_id="{{$vezani_artikl->vezani_roba_id}}" title="{{ Language::trans('Dodaj na listu želja') }}"><i class="far fa-heart"></i></button> 
                                            @else
                                            <button class="like-it JSnot_logged" data-roba_id="{{$vezani_artikl->vezani_roba_id}}" title="{{ Language::trans('Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima') }}"><i class="far fa-heart"></i></button> 
                                            @endif -->   
                                               
                                            @if(Product::getStatusArticle($vezani_artikl->roba_id) == 1)
                                                @if(Cart::check_avaliable($vezani_artikl->vezani_roba_id) > 0)
                                                    <button class="buy-btn JSadd-to-cart-similar" data-vezani_roba_id="{{ $vezani_artikl->vezani_roba_id }}" data-roba_id="{{ $vezani_artikl->roba_id }}">
                                                    {{Language::trans('U korpu')}}</button>
                                                    <!-- <input type="text" class="JSkolicina linked-articles-input like-it" value="1" onkeypress="validate(event)"> -->
                                                @else  
                                                    <!-- <button class="not-available">{{Language::trans('Nije dostupno')}}</button> -->
                                                    <button class="buy-btn JSenquiry JSQuickViewButton text-uppercase" data-roba_id="{{$roba_id}}" title="Pozovite">{{ Language::trans('POŠALJI UPIT') }}</button>                  

                                                @endif
                                                @else
                                                    <button class="buy-btn">
                                                        {{ Product::find_flag_cene(Product::getStatusArticle($vezani_artikl->roba_id),'naziv') }}
                                                    </button>
                                              @endif
                                        </div>
                                    @endif

                
                                    @if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
                                        <a class="hidden-xs article-edit-btn JSFAProductModalCall" data-roba_id="{{$vezani_artikl->vezani_roba_id}}" href="#">{{Language::trans('IZMENI ARTIKAL')}}</a>
                                        <a class="xs-hidden article-edit-btn JSFAProductModalCall" data-roba_id="{{$vezani_artikl->vezani_roba_id}}" href="#">{{Language::trans('=')}}</a>
                                     @endif
                                </div>
                            </div>
                            @endif
                        @endforeach 
                        </div> 
                        @endif
                    </div>

                    <div class="col-xs-12 no-padding related-details">
                        <!-- <br> -->
                        <h2 class="slickTitle"><span class="section-title">{{Language::trans('Slični proizvodi')}}</span></h2>
                        <div class="JSproducts_slick row">
                        @foreach(Product::get_related($roba_id, 6) as $row)
                            @include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
                        @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
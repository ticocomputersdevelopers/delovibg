@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')

<h2><span class="page-title text-center">{{ Language::trans('Unesi podatke') }}</span></h2> 

<div class="login-title text-center">
	@if(Input::old('flag_vrsta_kupca') == 1)
		<span class="inline-block pointer private-user">{{ Language::trans('fizičko lice') }}</span>
		<span class="inline-block pointer active-user company-user">{{ Language::trans('pravno lice') }}</span>
	@else
		<span class="inline-block pointer active-user private-user">{{ Language::trans('fizičko lice') }}</span>
		<span class="inline-block pointer company-user">{{ Language::trans('pravno lice') }}</span>
	@endif
</div> 


	<form action="{{ Options::base_url() }}registracija-post" method="post" class="registration-form" autocomplete="off"> 

		<input class="bg-width" type="hidden" name="flag_vrsta_kupca" value="0"> 

		<div class="user-section create-user flex text-center">

		<div class="form-group">
			<!-- <div><label for="email">{{ Language::trans('E-mail') }}</label></div> -->
			<input class="bg-width" id="email" autocomplete="off" name="email" type="text" value="{{Session::get('web_kupac_id') > 0 ? WebKupac::user_name(Session::get('web_kupac_id'))->email : ''}}" placeholder="{{ Language::trans('E-mail') }}" >
			<div class="error red-dot-error">{{ $errors->first('email') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('email') : "" }}
			</div>
		</div>	

		<div class="form-group">
			<!-- <div><label for="lozinka">{{ Language::trans('Lozinka') }}</label></div> -->
			<input class="bg-width" id="lozinka" name="lozinka" type="password" value="" placeholder="{{ Language::trans('Lozinka') }}">
			<div class="error red-dot-error">{{ $errors->first('lozinka') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('lozinka') : "" }}</div>
		</div>

		<div class="form-group">
			<!-- <div><label for="ime">{{ Language::trans('Ime') }}</label></div> -->
			<input class="bg-width" id="ime" name="ime" type="text" value="{{Session::get('web_kupac_id') > 0 ? WebKupac::user_name(Session::get('web_kupac_id'))->ime : ''}}" placeholder="{{ Language::trans('Ime i Prezime') }}" >
			<div class="error red-dot-error">{{ $errors->first('ime') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('ime') : "" }}
			</div>
		</div>

		<div class="form-group">
			<!-- <div><label for="naziv">{{ Language::trans('Naziv firme') }}:</label></div> -->
			<input class="bg-width" id="naziv" name="naziv" type="text" value="{{Session::get('web_kupac_id') > 0 ? WebKupac::user_name(Session::get('web_kupac_id'))->naziv : ''}}" placeholder="{{ Language::trans('Naziv firme') }}" >
			<div class="error red-dot-error">{{ $errors->first('naziv') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('naziv') : "" }}
			</div>
		</div>

		<div class="form-group">
			<!-- <div><label for="pib">{{ Language::trans('PIB') }}</label></div> -->
			<input class="bg-width" id="pib" name="pib" type="text" value="{{Session::get('web_kupac_id') > 0 ? WebKupac::user_name(Session::get('web_kupac_id'))->pib : ''}}" placeholder="{{ Language::trans('PIB') }}" >
			<div class="error red-dot-error">{{ $errors->first('pib') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('pib') : "" }}</div>
		</div>

		<div class="form-group">
			<!-- <div><label for="maticni_br">{{ Language::trans('Matični broj') }}</label></div> -->
			<input class="bg-width" id="maticni_br" name="maticni_br" type="text" value="{{Session::get('web_kupac_id') > 0 ? WebKupac::user_name(Session::get('web_kupac_id'))->maticni_br : ''}}" placeholder="{{ Language::trans('Matični broj') }}" >
			<div class="error red-dot-error">{{ $errors->first('maticni_br') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('maticni_br') : "" }}</div>
		</div>	

		<!-- <div class="form-group"> -->
			<!-- <div><label for="prezime">{{ Language::trans('Prezime') }}</label></div> -->
			<!-- <input class="bg-width" id="prezime" name="prezime" type="text" value="{{Session::get('web_kupac_id') > 0 ? WebKupac::user_name(Session::get('web_kupac_id'))->prezime : ''}}" placeholder="{{ Language::trans('Prezime') }}" > -->
			<!-- <div class="error red-dot-error">{{ $errors->first('prezime') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('prezime') : "" }}</div> -->
		<!-- </div> -->
		
		<div class="form-group">
			<input class="bg-width" id="datum" name="datum" type="text" value="{{Session::get('web_kupac_id') > 0 ? WebKupac::user_name(Session::get('web_kupac_id'))->datum : ''}}" placeholder="{{ Language::trans('Dan rođenja') }}" >
			<div class="error red-dot-error">{{ $errors->first('datum') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('datum') : "" }}</div>
		</div>

		<div class="form-group">
			<!-- <div><label for="telefon">{{ Language::trans('Telefon') }}</label></div> -->
			<input class="bg-width" id="telefon" name="telefon" type="text" value="{{Session::get('web_kupac_id') > 0 ? WebKupac::user_name(Session::get('web_kupac_id'))->telefon : ''}}" placeholder="{{ Language::trans('Telefon') }}" >
			<div class="error red-dot-error">{{ $errors->first('telefon') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('telefon') : "" }}</div>
		</div>

		<div class="form-group">
			<!-- <div><label for="adresa">{{ Language::trans('Adresa') }}</label></div> -->
			<input class="bg-width" id="adresa" name="adresa" type="text" value="{{Session::get('web_kupac_id') > 0 ? WebKupac::user_name(Session::get('web_kupac_id'))->adresa : ''}}" placeholder="{{ Language::trans('Ulica i broj') }}" >
			<div class="error red-dot-error">{{ $errors->first('adresa') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('adresa') : "" }}</div>
		</div>

		<div class="form-group">
			<!-- <div><label for="mesto"><span class="red-dot"></span> {{ Language::trans('Mesto') }}/{{ Language::trans('Grad') }} </label></div> -->
			<input class="bg-width" id="mesto" type="text" name="mesto" value="{{Session::get('web_kupac_id') > 0 ? WebKupac::user_name(Session::get('web_kupac_id'))->mesto : ''}}" placeholder="{{ Language::trans('Grad ili mesto') }}" >
			<div class="error red-dot-error">{{ $errors->first('mesto') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('mesto') : "" }}</div>
		</div>
		
		<div class="form-group">  
			<div class="capcha text-center"> 
				{{ Captcha::img(5, 160, 50) }}<br>
				<span>{{ Language::trans('Unesite kod sa slike') }}</span>
				<input type="text" name="captcha-string" tabindex="10" autocomplete="off">
				<div class="error red-dot-error">{{ $errors->first('captcha') ? $errors->first('captcha') : "" }}</div>
			</div>
		</div>

		<div class="text-center reg-btn">   
			<button type="submit" class="bg-button button text-uppercase reg-btn no-margin">{{ Language::trans('Napravi nalog') }}</button>
		</div> 
		<br>
		<a href="{{ Options::base_url() }}prijava" class="reg-link" id="login-icon">{{ Language::trans('Već imaš nalog? Prijavi se') }}</a>

		</div>
	</form>


@if(Session::get('message'))
<script>
	$(document).ready(function(){  

		swal(trans('Poslali smo Vam potvrdu o registraciji na e-mail koji ste uneli. Molimo Vas da potvrdite Vašu e-mail adresu klikom na link iz e-mail poruke'));

	});
</script>
@endif

<br>

@endsection 
@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page') 



@foreach($sekcije as $sekcija)
	<div {{ !empty($sekcija->boja_pozadine) ? 'style="background-color:'.($sekcija->boja_pozadine).'"' : (($sekcija->puna_sirina == 1) ? 'style="background-color: #fff"' : '') }};">   

	@if($sekcija->tip_naziv == 'Text')
	<!-- TEXT SECTION -->
	<div class="{{ ($sekcija->puna_sirina == 1) ? 'container-fluid no-padding' : 'container' }}"> 
		<div class="row padding-bg">
			<div class="col-xs-12">

				{{ Support::pageSectionContent($sekcija->sekcija_stranice_id) }}

			</div>
		</div>
	</div>

	@elseif($sekcija->tip_naziv == 'Lista artikala' AND !is_null($sekcija->tip_artikla_id))
	<!-- PRODUCTS SECTION -->
	<div class="{{ ($sekcija->puna_sirina == 1) ? 'container-fluid no-padding' : 'container' }}"> 
		<div class="row padding-bg">
			<div class="col-xs-12">			

				@if($sekcija->tip_artikla_id == -1 AND count($latestAdded = Articles::latestAdded(20)) > 0)


				<h2><span class="section-title JSInlineShort" data-target='{"action":"page_section","id":"{{ $sekcija->sekcija_stranice_id }}"}'>{{Language::trans($sekcija->naziv)}}<span class="show-more relative"></span></h2>
				

				<div class="products-on-list hidden-to-responsive">
					@foreach(Articles::latestAdded(3) as $row)
					@include('shop/themes/'.Support::theme_path().'partials/products/product_on_list')
					@endforeach
				</div>

				<div class="JSslick-toggle"> 
					@foreach(Articles::latestAdded(20) as $row)
					@include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
					@endforeach
				</div> 

				@elseif($sekcija->tip_artikla_id == -2 AND count($mostPopularArticles = Articles::mostPopularArticles(20)) > 0)

				<h2><span class="section-title JSInlineShort" data-target='{"action":"page_section","id":"{{ $sekcija->sekcija_stranice_id }}"}'>{{Language::trans($sekcija->naziv)}}<span class="show-more relative"></span></h2>

				<div class="products-on-list hidden-to-responsive">
					@foreach(Articles::mostPopularArticles(3) as $row)
					@include('shop/themes/'.Support::theme_path().'partials/products/product_on_list')
					@endforeach
				</div>

				<div class="JSslick-toggle"> 
					@foreach($mostPopularArticles as $row)
					@include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
					@endforeach 
				</div> 

				@elseif($sekcija->tip_artikla_id == -3 AND count($bestSeller = Articles::bestSeller()) > 0)

				<h2><span class="section-title JSInlineShort" data-target='{"action":"page_section","id":"{{ $sekcija->sekcija_stranice_id }}"}'>{{Language::trans($sekcija->naziv)}}<span class="show-more relative"></span></h2>

				<div class="products-on-list hidden-to-responsive">
					@foreach(Articles::bestSeller(3) as $row)
					@include('shop/themes/'.Support::theme_path().'partials/products/product_on_list')
					@endforeach
				</div>

				<div class="JSslick-toggle"> 
					@foreach($bestSeller as $row)
					@include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
					@endforeach
				</div> 

				@elseif($sekcija->tip_artikla_id == 0 AND All::broj_akcije() > 0)

				<h2><span class="section-title JSInlineShort" data-target='{"action":"page_section","id":"{{ $sekcija->sekcija_stranice_id }}"}'>{{Language::trans($sekcija->naziv)}}<span class="show-more relative"></span></h2>

				<div class="JSslick-toggle"> 
					@foreach(Articles::akcija(null) as $row)
					@include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
					@endforeach
				</div> 

				@elseif(All::provera_tipa($sekcija->tip_artikla_id) AND !is_null($tip_naziv = Support::tip_naziv($sekcija->tip_artikla_id))) 

				<h2><span class="section-title JSInlineShorjavascript:void(0) }}">{{Language::trans($sekcija->naziv)}}<span class="show-more relative"><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('tip') }}/{{Url_mod::slug_trans($tip_naziv) }}">{{Language::trans('Prikaži sve')}} <i class="fas fa-caret-right">&nbsp;</i></a></span></h2>

				<div class="products-on-list hidden-to-responsive">
					@foreach(Articles::artikli_tip($sekcija->tip_artikla_id, 3) as $row)
					@include('shop/themes/'.Support::theme_path().'partials/products/product_on_list')
					@endforeach
				</div>

				<div class="JSslick-toggle"> 
					@foreach(Articles::artikli_tip($sekcija->tip_artikla_id,20) as $row)
					@include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
					@endforeach
				</div>   

				@endif

			</div>
		</div>
	</div>

	@elseif($sekcija->tip_naziv == 'Lista vesti')
	<!-- BLOGS SECTION-->
	<div class="{{ ($sekcija->puna_sirina == 1) ? 'container-fluid no-padding' : 'container' }}"> 
		<div class="row padding-bg">
			<div class="col-xs-12">

				<h2><span class="section-title JSInlineShort" data-target='{"action":"page_section","id":"{{ $sekcija->sekcija_stranice_id }}"}'>{{Language::trans($sekcija->naziv)}}</span></h2>

				<div class="JSblog-slick">   
					@foreach(All::getShortListNews() as $row) 
					<div class="col-md-4">
						<div class="card-blog"> 
							@if(in_array(Support::fileExtension($row->slika),array('jpg','png','jpeg','gif')))

							<a class="bg-img center-block" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}" style="background-image: url('{{ $row->slika }}');"></a>

							@else

							<iframe src="{{ $row->slika }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>                   

							@endif

							<h3 class="blogs-title">
								<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}">{{ $row->naslov }}</a>
							</h3>

							<div class="blogs-date text-uppercase">
								{{ Support::date_convert($row->datum) }}
							</div>
						</div>
					</div>
					@endforeach 

				</div> 

			</div>
		</div>
	</div>

	@elseif($sekcija->tip_naziv == 'Slajder' AND !is_null($sekcija->slajder_id))

	<div class="{{ ($sekcija->puna_sirina == 1) ? 'container-fluid no-padding' : 'container' }}"> 

		@if($slajder = Slider::slajder($sekcija->slajder_id) AND count($slajderStavke = Slider::slajderStavke($slajder->slajder_id)) > 0)

			@if($slajder->slajder_tip_naziv == 'Slajder')

			<div class="JSmain-slider JSmain-pull">

				@foreach($slajderStavke as $slajderStavka)

				<div class="relative">

					<a class="slider-link" href="{{ $slajderStavka->link }}"></a>

					<div class="bg-img" style="background-image: url( '{{ Options::domain() }}{{ $slajderStavka->image_path }}' )"></div>

					<div class="sliderText"> 

						@if($slajderStavka->naslov != '')
						<div>
							<h2 class="main-desc JSInlineShort" data-target='{"action":"slide_title","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
								{{ $slajderStavka->naslov }}
							</h2>
						</div>
						@endif

						@if($slajderStavka->sadrzaj != '')
						<div>
							<div class="short-desc JSInlineFull" data-target='{"action":"slide_content","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
								{{ $slajderStavka->sadrzaj }}
							</div>
						</div>
						@endif

						@if($slajderStavka->naslov_dugme != '')
						<div>
							<a href="{{ $slajderStavka->link }}" class="slider-btn-link JSInlineShort" data-target='{"action":"slide_button","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
								{{ $slajderStavka->naslov_dugme }}
							</a>
						</div>
						@endif
					</div>
				</div>

				@endforeach
			</div>

			@elseif($slajder->slajder_tip_naziv == 'Baner')
			<!-- BANNERS SECTION -->
			<div class="row padding-bg">
				<div class="col-xs-12 responsive-banner"> 
					<h2><span class="section-title JSInlineShort" data-target='{"action":"page_section","id":"{{ $sekcija->sekcija_stranice_id }}"}'>{{Language::trans($sekcija->naziv)}}</span></h2>
					@foreach($slajderStavke as $slajderStavka)
					<div class="">
						<div class="relative banners"> 
							<a class="slider-link" href="{{ $slajderStavka->link }}"></a>

							<div class="bg-img" style="background-image: url('{{ Options::domain() }}{{ $slajderStavka->image_path }}')"></div>

							@if($slajderStavka->sadrzaj != '') 
								<div class="short-desc JSInlineFull content bannerTxt" data-target='{"action":"slide_content","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
									{{ $slajderStavka->sadrzaj }}
								</div> 
							@endif
						</div> 
					</div>
					@endforeach
				</div>
			</div>

			@elseif($slajder->slajder_tip_naziv == 'Tekst Baner (tekst levo)')
			<!-- TEXT BANNER LEFT SECTION -->
			<!-- TIP SLIDER && Maybe you'll like this too -->
			<div class="row padding-bg">
				<div class="col-xs-12 JStip-slider"> 
					<h2><span class="section-title JSInlineShort" data-target='{"action":"page_section","id":"{{ $sekcija->sekcija_stranice_id }}"}'>{{Language::trans($sekcija->naziv)}}</span></h2>
					@foreach($slajderStavke as $slajderStavka)
						<div class="relative banners xs-no-padd"> 
							<a class="slider-link" href="{{ $slajderStavka->link }}"></a>

							<div class="bg-img" style="background-image: url('{{ Options::domain() }}{{ $slajderStavka->image_path }}')"></div>

							<!-- @if($slajderStavka->sadrzaj != '') 
								<div class="short-desc JSInlineFull content bannerTxt" data-target='{"action":"slide_content","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
									{{ $slajderStavka->sadrzaj }}
								</div> 
							@endif -->
						</div> 
					@endforeach
					<div class="expand-slider">
						<a href="javascript:null(0);">
							<i class="fas fa-chevron-down"></i>
						</a>
					</div>
				</div>
			</div>

			@elseif($slajder->slajder_tip_naziv == 'Tekst Baner (tekst desno)')
			<!-- TEXT BANNER RIGHT SECTION -->
			<!-- Maybe you'll like this too -->
			<div class="row padding-bg">
				<div class="col-xs-12 JSyoulllike_slick"> 
					<h2><span class="section-title JSInlineShort" data-target='{"action":"page_section","id":"{{ $sekcija->sekcija_stranice_id }}"}'>{{Language::trans($sekcija->naziv)}}</span></h2>
					@foreach($slajderStavke as $slajderStavka)
					<div class="">
						<div class="relative banners"> 
							<a class="slider-link" href="{{ $slajderStavka->link }}"></a>

							<div class="bg-img" style="background-image: url('{{ Options::domain() }}{{ $slajderStavka->image_path }}')"></div>

							@if($slajderStavka->sadrzaj != '') 
								<div class="short-desc JSInlineFull content bannerTxt" data-target='{"action":"slide_content","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
									{{ $slajderStavka->sadrzaj }}
								</div> 
							@endif
						</div> 
					</div>
					@endforeach
				</div>
			</div>
			
	
			@elseif($slajder->slajder_tip_naziv == 'Galerija Baner')
			<!-- GALLERY BANNER SECTION -->
			<div class="row padding-bg">
				<div class="col-xs-12 JSbanners_gallery_slick"> 
					<h2><span class="section-title JSInlineShort" data-target='{"action":"page_section","id":"{{ $sekcija->sekcija_stranice_id }}"}'>{{Language::trans($sekcija->naziv)}}</span></h2>
					@foreach($slajderStavke as $slajderStavka)
					<div class="">
						<div class="relative banners"> 
							<a class="slider-link" href="{{ $slajderStavka->link }}"></a>

							<div class="bg-img" style="background-image: url('{{ Options::domain() }}{{ $slajderStavka->image_path }}')"></div>

							@if($slajderStavka->sadrzaj != '') 
								<div class="short-desc JSInlineFull content bannerTxt" data-target='{"action":"slide_content","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
									{{ $slajderStavka->sadrzaj }}
								</div> 
							@endif
						</div> 
					</div>
					@endforeach
				</div>
			</div>

			<!-- <div class="row padding-bg">
				<div class="col-xs-12 JSbanners_gallery_slick responsive-banner"> 
					<h2><span class="section-title JSInlineShort" data-target='{"action":"page_section","id":"{{ $sekcija->sekcija_stranice_id }}"}'>{{Language::trans($sekcija->naziv)}}</span></h2>
					@foreach($slajderStavke as $slajderStavka)
					<div class="">
						<div class="relative banners"> 
							<a class="slider-link" href="{{ $slajderStavka->link }}"></a>

							<div class="bg-img" style="background-image: url('{{ Options::domain() }}{{ $slajderStavka->image_path }}')"></div>

							@if($slajderStavka->sadrzaj != '') 
								<div class="short-desc JSInlineFull content bannerTxt" data-target='{"action":"slide_content","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
									{{ $slajderStavka->sadrzaj }}
								</div> 
							@endif
						</div> 
					</div>
					@endforeach
				</div>
			</div> -->
			
			@elseif($slajder->slajder_tip_naziv == 'Galerija Slajder')
			<!-- GALLERY SLIDER SECTION -->
			<div class="row padding-bg">  
				<div class="col-xs-12"> 
					<div class="main_imgGallery">
	                    <img class="JSmain_img img-responsive" src="{{ Options::domain() }}{{ $slajderStavke[0]->image_path }}" alt="" />
	                </div>
	                <!-- CUSTOM MODAL -->
	                <div class="JSmodal">
	                    <div class="flex full-screen relative"> 
	                      
	                        <div class="modal-cont galery-cont relative text-center"> 
	                      
	                            <div class="inline-block relative"> 
	                                <span class="JSclose-modal"><i class="fas fa-times"></i></span>
	                          
	                                <img src="" class="img-responsive" id="JSmodal_img" alt="modal image">
	                            </div>

	                            <button class="btn JSleft_btn"><i class="fas fa-chevron-left"></i></button>
	                            <button class="btn JSright_btn"><i class="fas fa-chevron-right"></i></button>
	                        </div>
	                
	                    </div>
	                </div>
	     
					<div class="gallery_slider"> 
						@foreach($slajderStavke as $slajderStavka) 
							<a class="flex JSimg-gallery relative" href="javascript:void(0)">
									
								<img class="img-responsive" src="{{ Options::domain().$slajderStavka->image_path }}" id="{{ Options::domain().$slajderStavka->image_path }}">

								<!-- @if($slajderStavka->naslov != '')
								<h2 class="gallery-title"> 
									{{ $slajderStavka->naslov }}
								</h2> 
								@endif -->

							</a> 
						@endforeach 
					</div>
				</div>
			</div>
			@endif  

		@endif
	</div> <!-- CONTAINER -->

	<!-- GROUPS ON GRID -->
	@elseif($sekcija->tip_naziv == 'Lista kategorija')
	<div class="{{ ($sekcija->puna_sirina == 1) ? 'container-fluid no-padding' : 'container' }}">
		<div class="row padding-bg"> 	
			<div class="col-xs-12">
				<h2><span class="groups-title section-title JSInlineShort" data-target='{"action":"page_section","id":"{{ $sekcija->sekcija_stranice_id }}"}'>{{Language::trans($sekcija->naziv)}}<a class="fas fa-caret-right groups-more" href="{{ Options::base_url() }}{{ Language::trans('sve-kategorije') }}">&nbsp;</a></span></h2>

				<div class="JSgroups_slick"> 
					@foreach (DB::select("select grupa_pr_id, grupa, putanja_slika from grupa_pr where parrent_grupa_pr_id = 0 and web_b2c_prikazi = 1") as $row2)
						@include('shop/themes/'.Support::theme_path().'partials/groups_on_grid')
					@endforeach
				</div>
			</div>
		</div>
	</div>

	@elseif($sekcija->tip_naziv == 'Newsletter' AND Options::newsletter()==1 AND !is_null($newslatter_description = All::newslatter_description()))

	<div class="{{ ($sekcija->puna_sirina == 1) ? 'container-fluid no-padding' : 'container' }}">
		<div class="newsletter"> 
			<div class="row"> 

				<div class="col-md-6 col-sm-6 col-xs-12"> 
					<h5 class="JSInlineShort" data-target='{"action":"newslatter_label"}'>
						{{ $newslatter_description->naslov }}
					</h5> 

					<p class="JSInlineFull" data-target='{"action":"newslatter_content"}'>
						{{ $newslatter_description->sadrzaj }}
					</p>
				</div> 


				<div class="col-md-6 col-sm-6 col-xs-12">          
					<div class="relative"> 
						<input type="text" placeholder="E-mail" id="newsletter" /> 
						<button type="button" class="text-uppercase button" onclick="newsletter()">{{ Language::trans('Prijavi se') }}</button>
					</div>
				</div>

			</div>
		</div>
	</div>

	@endif

</div> <!-- END BACKGROUND COLOR -->
@endforeach   

@if(isset($anketa_id) AND $anketa_id>0)
@include('shop/themes/'.Support::theme_path().'partials/poll')		
@endif

@if(Session::has('pollTextError'))
<script>
	$(document).ready(function(){     
		bootboxDialog({ message: "<p>{{ Session::get('pollTextError') }}</p>" }); 
	});
</script>
@endif

@endsection

<header>   
    <div id="JSfixed_header" style="background-color: {{ Options::web_options(321,'str_data') }}">  
        <div class="container{{(Options::web_options(321)==1) ? '-fluid' : '' }}"> 
            <div class="row flex">   
            
                <!-- LOGO  -->
                <div class="col-md-3 col-sm-3 col-xs-12 sm-text-center">
                    <h1 class="seo">{{ Options::company_name() }}</h1>
                    
                    <a class="logo inline-block" href="/" title="{{Options::company_name()}}" rel="canonical">
                        <img src="{{ Options::domain() }}{{Options::company_logo()}}" alt="{{Options::company_name()}}" class="img-responsive"/>
                    </a>
                </div>

                <!-- SEARCH FIELD -->
            
                <div class="header-search col-md-offset-1 {{ Session::has('b2c_kupac') == 1 ? 'col-md-4 col-sm-5' : 'col-md-5 col-sm-6' }} col-xs-12">
                    <div class="row"> 
                        @if(Options::gnrl_options(3055) == 0)
    
                            <div class="col-xs-3 JSselectTxt no-padding">  
                               
                                {{ Groups::firstGropusSelect('2') }} 
                    
                            </div>
                
                        @else  
                
                            <input type="hidden" class="JSSearchGroup2" value="">
                
                        @endif 
                       
                        <div class="{{ Options::gnrl_options(3055) == 0 ? 'col-xs-9 no-radius' : 'col-xs-12' }} no-padding JSsearchContent2">  
                            <form autocomplete="off">
                                <input class="search-field" type="text" id="JSsearch2" placeholder="{{ Language::trans('Pretraga') }}" />
                            </form>      
                            <button onclick="search2()" class="JSsearch-button2"> <i class="fas fa-search"></i> </button>
                        </div>
              
                    </div>
                </div>
            
                <!-- LOGIN AND REGISTRATION -->
                <div class="{{ Session::has('b2c_kupac') == 1 ? 'col-md-4 col-sm-4 text-center' : 'col-md-3 col-sm-3 text-right sm-text-center' }} col-xs-12">

                    @if(Session::has('b2c_kupac'))
                        <div class="inline-block">

                            &nbsp;<span class="JSbroj_wish far fa-heart">{{ Cart::broj_wish() }}</span>&nbsp;  

                            <a class="logged-user" href="{{Options::base_url()}}{{Url_mod::slug_trans('korisnik')}}/{{Url_mod::slug_trans(WebKupac::get_user_name())}}">{{ WebKupac::get_user_name() }}</a>

                            <a class="logged-user" href="{{Options::base_url()}}{{Url_mod::slug_trans('korisnik')}}/{{Url_mod::slug_trans(WebKupac::get_company_name())}}">{{ WebKupac::get_company_name() }}</a>

                            <a id="logout-button" class="inline-block" href="{{Options::base_url()}}logout">{{ Language::trans('Odjavi se') }}</a>
                        </div>

                    @else 
                        <div class="dropdown inline-block">
                            <button class="dropdown-toggle login-btn" type="button" data-toggle="dropdown">
                              <i class="far fa-user"> </i>
                            </button>
                            <ul class="dropdown-menu login-dropdown">
                                <!-- ====== LOGIN MODAL TRIGGER ========== -->
                                <li>
                                    <a href="#" id="login-icon" data-toggle="modal" data-target="#loginModal">
                                   {{ Language::trans('Prijava') }}</a>
                                </li>
                                <li>
                                    <a id="registration-icon" href="{{Options::base_url()}}{{ Url_mod::slug_trans('registracija') }}"> 
                                    {{ Language::trans('Registracija') }}</a>
                                </li>
                            </ul>
                        </div> 
                    @endif

                    <!-- CART AREA -->   
                    @include('shop/themes/'.Support::theme_path().'partials/cart_top')
                    <div class="resp-nav-btn inline-block hidden-md hidden-lg"><span class="fas fa-bars"></span></div>

                </div> 
          
            </div> 
        </div>  
    </div> 
</header>
        
<div class="menu-background">   
    <div class="container"> 
        <div id="responsive-nav" class="row">

            @if(Options::category_view()==1)
                @include('shop/themes/'.Support::theme_path().'partials/categories/category')
            @endif               

            <div class="pages-cont inline-block col-md-9 col-sm-12 col-xs-12 sm-no-padd">

            <!-- PAGES IN MENU -->
                <ul id="main-menu">
                    @foreach(All::header_menu_pages() as $row)
                    <li class="header-dropdown">
                        @if(All::broj_cerki($row->web_b2c_seo_id) > 0)  
                        <a href="{{ Options::base_url().Url_mod::page_slug($row->naziv_stranice)->slug }}">{{Url_mod::page_slug($row->naziv_stranice)->naziv}}</a>
                        <ul class="drop-2">
                            @foreach(All::header_menu_pages($row->web_b2c_seo_id) as $row2)
                            <li> 
                                <a href="{{ Options::base_url().Url_mod::page_slug($row2->naziv_stranice)->slug }}">{{Url_mod::page_slug($row2->naziv_stranice)->naziv}}</a>
                                <ul class="drop-3">
                                    @foreach(All::header_menu_pages($row2->web_b2c_seo_id) as $row3)
                                    <li> 
                                        <a href="{{ Options::base_url().Url_mod::page_slug($row3->naziv_stranice)->slug }}">{{Url_mod::page_slug($row3->naziv_stranice)->naziv}}</a>
                                    </li>
                                    @endforeach
                                </ul>
                            </li>
                            @endforeach
                        </ul>
                        @else   
                        <a href="{{ Options::base_url().Url_mod::page_slug($row->naziv_stranice)->slug }}">{{Url_mod::page_slug($row->naziv_stranice)->naziv}}</a> 
                        @endif                    
                    </li>                     
                    @endforeach 

                    @if(Options::web_options(121)==1)
                    <?php $konfiguratori = All::getKonfiguratos(); ?>
                    @if(count($konfiguratori) > 0)
                    @if(count($konfiguratori) > 1)
                    <li class="header-dropdown">
                        <a href="#!" class="konf-dropbtn">
                            {{Language::trans('Konfiguratori')}}
                        </a>
                        <ul class="drop-2">
                            @foreach($konfiguratori as $row)
                            <li>
                                <a href="{{ Options::base_url() }}{{Url_mod::slug_trans('konfigurator')}}/{{ $row->konfigurator_id }}">{{ Language::trans($row->naziv) }}</a>
                            </li>
                            @endforeach
                        </ul>
                    </li>
                    @else
                    <li>
                        <a href="{{ Options::base_url() }}{{Url_mod::slug_trans('konfigurator')}}/{{ $konfiguratori[0]->konfigurator_id }}">
                            {{Language::trans('Konfigurator')}}
                        </a>
                    </li>
                    @endif
                    @endif
                    @endif
                </ul>   
            </div> 
        </div>
    </div>    
</div> 

<!-- LOGIN MODAL --> 
<div class="modal fade" id="loginModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><span class="welcome-login"><span>{{ Language::trans('Dobrodošli') }}</span>{{ Language::trans('Za pristup Vašem nalogu unesite Vaš e-mail i lozinku') }}.</span></h4>
            </div>
            <div class="modal-body"> 
                <label for="JSemail_login">{{ Language::trans('E-mail') }}</label>
                <input id="JSemail_login" type="text" value="" autocomplete="off">
        
                <label for="JSpassword_login">{{ Language::trans('Lozinka') }}</label>
                <input autocomplete="off" id="JSpassword_login" type="password" value=""> 
            </div>

            <div class="modal-footer text-right">
                <a class="inline-block button" href="{{Options::base_url()}}{{ Url_mod::slug_trans('registracija') }}">{{Language::trans('Registruj se')}}</a>
                
                <button type="button" id="login" onclick="user_login()" class="button">{{ Language::trans('Prijavi se') }}</button>
                
                <button type="button" onclick="user_forgot_password()" class="forgot-psw pull-left">{{ Language::trans('Zaboravljena lozinka') }}</button>

                <div class="field-group error-login JShidden-msg" id="JSForgotSuccess"><br>
                    {{ Language::trans('Novu lozinku za logovanje dobili ste na navedenoj e-mail adresi') }}.
                </div> 
            </div>
        </div>   
    </div>
</div>


<!--========= QUICK VIEW MODAL ======== -->
<div class="modal fade" id="JSQuickView" role="dialog">
<div class="modal-dialog modal-lg">
  <div class="modal-content">
      <div class="modal-header">
        <button type="button" id="JSQuickViewCloseButton" class="close close-me-btn">
          <span>&times;</span>
        </button>
      </div>
      <div class="modal-body" id="JSQuickViewContent">
        <img alt="loader-image" class="gif-loader" src="{{Options::base_url()}}images/quick_view_loader.gif">
      </div>
  </div>    
</div>
</div>
@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
 
<div class="ktlg-page">
	@foreach(All::getKatalog() as $row)  
	<div class="box"> 
		<div class="row"> 		
			<div class="col-md-8 col-sm-8 col-xs-12"> 		
				<h2><span>{{ $row->naziv }}</span></h2>		 
			</div>
			<div class="col-md-4 col-sm-4 col-xs-12 text-right"> 	
				<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('katalog') }}/{{ $row->katalog_id }}" target="_blank" class="button">{{ Language::trans('Preuzmi katalog') }}</a>
			</div> 
		</div>
	</div>
	@endforeach
</div>
@endsection



<!-- PRODUCT PREVIEW TABS-->


    <div class="col-md-12 col-sm-12 col-xs-12"> 

        <div class="product-preview-tabs">

        <div class="tab-titles">
           
            <a href="#!" class="active description">{{Language::trans('Opis')}}</a>

            <a class="documentation" href="#!">{{Language::trans('Sadržaji')}}</a>
     
            <a class="comments" href="#!">{{Language::trans('Komentari')}}</a>
        
        </div>

            <div class="tab-content"> 

                <!-- Description -->
                <div id="description-tab">
                    <p itemprop="description"> {{ Product::get_opis($roba_id) }} </p>
                    {{ Product::get_karakteristike($roba_id) }}
                </div>

                <!-- Technical docs -->
                <div id="technical-docs">
                    @if(Options::web_options(120))
                        @if(count($fajlovi) > 0)
                        <div>
                          <!--   <h2 class="h2-margin">Dodatni fajlovi</h2> -->
                            @foreach($fajlovi as $row)
                            <div class="files-list-item">
                                <a class="files-link" href="{{ $row->putanja != null ? Options::domain().$row->putanja : $row->putanja_etaz }}" target="_blank">
                                    <img src="{{ Options::domain() }}images/file-icon.png" alt="{{ $row->naziv }}">
                                    <div class="files-list-item">
                                        <div class="files-name">{{ Language::trans($row->naziv) }}</div> <!-- {{ AdminSupport::getExtension($row->vrsta_fajla_id) }} --> 
                                    </div>
                                </a>
                            </div>
                            @endforeach
                        </div>
                        @endif
                     @endif
                </div>

                <!-- Comments -->
                <div id="the-comments">
                
                    <?php $query_komentary=DB::table('web_b2c_komentari')->where(array('roba_id'=>$roba_id,'komentar_odobren'=>1));
                    if($query_komentary->count() > 0){?>

                        <div class="comment-content-div">

                            <ul class="comments">
                                <?php foreach($query_komentary->get() as $row)
                                { ?>
                                <li class="comment coment-content-li">
                                    <div>
                                        <div class="comment-name-div">
                                            <div class="comment-name">{{$row->ime_osobe}}</div>
                                            <div class="comment-rating">{{Product::getRatingStars($row->ocena)}}</div>
                                        </div>
                                       
                                        <div class="comment-date">{{$row->datum}}</div>
                                        
                                        <div class="comment-text">{{ $row->pitanje }}</div>
                                    </div>

                                    <!-- Replies -->
                                    @if($row->odgovoreno == 1)

                                    <ul class="replies">
                                        <li class="comment">
                                            <span class="comment-name">
                                                {{ Options::company_name() }}
                                            </span>
                                            <p class="comment-text">
                                                {{ $row->odgovor }}
                                            </p> 
                                        </li>
                                    </ul>

                                    @endif

                                </li>

                                <?php }?>

                            </ul>
                        

                        <?php }?>

                        <div class="comments-form {{ $query_komentary->count() > 0 ? 'comments-form2' : '' }}"> 
                            
                            <div class="frm-control">
                                <label for="JScomment_name">
                                    {{Language::trans('Vaše ime')}}
                                </label>

                                <input id="JScomment_name" onchange="check_fileds('JScomment_name')" type="text"  />
                            </div>
                           
                            
                            <div class="frm-control">
                                <label for="JScomment_message">{{Language::trans('Komentar')}}</label>

                                <textarea class="comment-message" rows="5" id="JScomment_message" onchange="check_fileds('JScomment_message')"></textarea>

                                <input type="hidden" value="{{ $roba_id }}" id="JSroba_id" />
                            </div>

                            <div class="review-div">
                                <span class="review comment-review">
                                    <span>{{Language::trans('Ocena')}}:</span>
                                    <i id="JSstar1" class="fa fa-star review-star" aria-hidden="true"></i>
                                    <i id="JSstar2" class="fa fa-star review-star" aria-hidden="true"></i>
                                    <i id="JSstar3" class="fa fa-star review-star" aria-hidden="true"></i>
                                    <i id="JSstar4" class="fa fa-star review-star" aria-hidden="true"></i>
                                    <i id="JSstar5" class="fa fa-star review-star" aria-hidden="true"></i>
                                    <input id="JSreview-number" value="0" type="hidden"/>
                                </span>
                            </div>
                            
                            <div>
                                <button class="atractive-comment-btn JScomment_add">
                                    {{Language::trans('Pošalji')}}
                                </button>
                            </div>
                
                        </div>
                    
                    </div> <!-- End of Comment-content-div -->
                   
                </div> <!-- The-comments -->

            </div> <!-- Tab-content -->

            <!-- BRENDOVI SLAJDER -->
            <!--  <div class="row">
            <div class="col-md-12">
                <div class="dragg JSBrandSlider"> 
                       <?php //foreach(DB::table('proizvodjac')->where('brend_prikazi',1)->get() as $row){ ?>
                    <div class="col-md-12 col-sm-6 end sub_cats_item_brend">
                        <a class="brand-link" href="{{Options::base_url()}}{{ Url_mod::convert_url('proizvodjac') }}/<?php //echo $row->naziv; ?> ">
                             <img src="{{ Options::domain() }}<?php //echo $row->slika; ?>" />
                         </a>
                    </div>
                    <?php //} ?>
                </div>
            </div>
            </div> -->

        </div> <!-- End of col -->
    
</div> <!-- End of product-preview-tabs -->
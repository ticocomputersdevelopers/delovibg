<header>   
    <div id="JSfixed_header" style="background-color: {{ Options::web_options(321,'str_data') }}">  
        <div class="container{{(Options::web_options(321)==1) ? '-fluid' : '' }}"> 
            <div class="row flex relative"> 

                <!-- LOGO  -->
                <div class="col-md-3 col-sm-4 col-xs-10">

                    <h1 class="seo">{{ Options::company_name() }}</h1>

                    <a class="logo inline-block" href="/" title="{{Options::company_name()}}" rel="canonical">
                        <img src="{{ Options::domain() }}{{Options::company_logo()}}" alt="{{Options::company_name()}}" class="img-responsive"/>
                    </a>
             
                </div>

                <!-- SEARCH BUTTON -->
                <div class="col-md-6 col-sm-6 col-xs-12 header-search">  
                    <div class="row"> 
                    
                        @if(Options::gnrl_options(3055) == 0)
        
                            <div class="col-md-3 col-sm-3 col-xs-3 no-padding JSselectTxt">  
                        
                                {{ Groups::firstGropusSelect('2') }}  
                        
                            </div>
                    
                        @else  
                    
                            <input type="hidden" class="JSSearchGroup2" value="">
                    
                        @endif 

                         
                        <div class="col-md-9 col-sm-9 @if(Options::gnrl_options(3055) == 0) col-xs-9 @endif col-xs-12 no-padding JSsearchContent2">  
 
                            <form autocomplete="off" class="relative">
                                <input class="search-field" type="text" id="JSsearch2" placeholder="{{ Language::trans('Pretraga') }}" />
                                <button type="button" onclick="search2()" class="JSsearch-button2"> <i class="fas fa-search"></i> </button>
                            </form>   
                             
                            <script> 
                                if ( ! $('.JSselectTxt').length) {
                                    $('.search-field').css({
                                        borderLeft: '2px solid var(--blue)',
                                        borderRadius: 50
                                    });
                                } 
                            </script> 

                        </div>      
                    </div> 


                    <div class="mostPopular hidden-sm hidden-xs">
                        <span class="v-align">{{ Language::trans('Najtraženije') }}:</span>
  
                        @foreach(Articles::mostPopularArticlesSearch() as $row)
                      
                            <a class="inline-block overflow-hdn v-align" href="{{Options::base_url()}}artikal/{{ Url_mod::slug_trans($row->naziv) }}" title="{{$row->naziv}}">{{$row->naziv}}</a>
                  
                        @endforeach 
                    </div> 

                </div>
 

                <!-- CART AREA -->   
                @include('shop/themes/'.Support::theme_path().'partials/cart_top')
 
                <div class="resp-nav-btn hidden-md hidden-lg"><span class="fas fa-bars"></span></div>
            
            </div> 

        </div>  
    </div> 
</header>
 
<div class="JSdark_bg"></div> 

<div class="container"> 
    <div id="responsive-nav" class="row bw">

        @if(Options::category_view()==1)
            @include('shop/themes/'.Support::theme_path().'partials/categories/category')
        @endif               

        <div class="pages-cont inline-block col-md-9 col-sm-12 col-xs-12">

        <!-- PAGES IN MENU -->
            <ul id="main-menu" class="relative">
                @foreach(All::header_menu_pages() as $row)
                <li class="header-dropdown">
                    @if(All::broj_cerki($row->web_b2c_seo_id) > 0)  
                    <a href="{{ Options::base_url().Url_mod::page_slug($row->naziv_stranice)->slug }}">{{Url_mod::page_slug($row->naziv_stranice)->naziv}}</a>
                    <ul class="drop-2">
                        @foreach(All::header_menu_pages($row->web_b2c_seo_id) as $row2)
                        <li> 
                            <a href="{{ Options::base_url().Url_mod::page_slug($row2->naziv_stranice)->slug }}">{{Url_mod::page_slug($row2->naziv_stranice)->naziv}}</a>
                            <ul class="drop-3">
                                @foreach(All::header_menu_pages($row2->web_b2c_seo_id) as $row3)
                                <li> 
                                    <a href="{{ Options::base_url().Url_mod::page_slug($row3->naziv_stranice)->slug }}">{{Url_mod::page_slug($row3->naziv_stranice)->naziv}}</a>
                                </li>
                                @endforeach
                            </ul>
                        </li>
                        @endforeach
                    </ul>
                    @else   
                    <a href="{{ Options::base_url().Url_mod::page_slug($row->naziv_stranice)->slug }}">{{Url_mod::page_slug($row->naziv_stranice)->naziv}}</a> 
                    @endif                    
                </li>                     
                @endforeach 

                @if(Options::web_options(121)==1)
                <?php $konfiguratori = All::getKonfiguratos(); ?>
                @if(count($konfiguratori) > 0)
                @if(count($konfiguratori) > 1)
                <li class="header-dropdown">
                    
                    <a href="#!">{{Language::trans('Konfiguratori')}}</a>

                    <ul class="drop-2">
                        @foreach($konfiguratori as $row)
                        <li>
                            <a href="{{ Options::base_url() }}{{Url_mod::slug_trans('konfigurator')}}/{{ $row->konfigurator_id }}">{{ Language::trans($row->naziv) }}</a>
                        </li>
                        @endforeach
                    </ul>
                </li>
                @else
                <li>
                    <a href="{{ Options::base_url() }}{{Url_mod::slug_trans('konfigurator')}}/{{ $konfiguratori[0]->konfigurator_id }}">
                        {{Language::trans('Konfigurator')}}
                    </a>
                </li>
                @endif
                @endif
                @endif
            </ul>   
        </div> 
    </div>
</div>     

<!-- LOGIN MODAL --> 
<div class="modal fade" id="loginModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
                <h4 class="modal-title text-center">{{ Language::trans('Moj nalog') }}</h4>
            </div>
            <div class="modal-body"> 
                <label for="JSemail_login">{{ Language::trans('E-mail') }}</label>
                <input id="JSemail_login" type="text" value="" autocomplete="off">
        
                <label for="JSpassword_login">{{ Language::trans('Lozinka') }}</label>
                <input autocomplete="off" id="JSpassword_login" type="password" value=""> 
            </div>

            <div class="modal-footer text-right">
                <a class="inline-block button" href="{{Options::base_url()}}{{ Url_mod::slug_trans('registracija') }}">{{Language::trans('Registracija')}}</a>
                
                <button type="button" id="login" onclick="user_login()" class="button">{{ Language::trans('Prijava') }}</button>
                
                <button type="button" onclick="user_forgot_password()" class="forgot-psw pull-left">{{ Language::trans('Zaboravljena lozinka') }}</button>

                <div class="field-group error-login JShidden-msg" id="JSForgotSuccess"><br>
                    {{ Language::trans('Novu lozinku za logovanje dobili ste na navedenoj e-mail adresi') }}.
                </div> 
            </div>
        </div>   
    </div>
</div>


<!--========= QUICK VIEW MODAL ======== -->
<div class="modal fade" id="JSQuickView" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
           
            <div class="modal-header">
                <button type="button" id="JSQuickViewCloseButton" class="close close-me-btn"><i class="fas fa-times"></i></button>
            </div>

            <div class="modal-body text-center" id="JSQuickViewContent">
                <img alt="loader-image" class="img-responsive margin-auto" src="{{Options::base_url()}}images/quick_view_loader.gif">
            </div>
        </div>    
    </div>
</div>


<!-- LOADER -->
<!-- <div class="JSspinner">
    <div class="sk-cube-grid">
        <div class="sk-cube sk-cube1"></div>
        <div class="sk-cube sk-cube2"></div>
        <div class="sk-cube sk-cube3"></div>
        <div class="sk-cube sk-cube4"></div>
        <div class="sk-cube sk-cube5"></div>
        <div class="sk-cube sk-cube6"></div>
        <div class="sk-cube sk-cube7"></div>
        <div class="sk-cube sk-cube8"></div>
        <div class="sk-cube sk-cube9"></div>
    </div>
</div> -->


 
 

<!-- header.blade -->
<header style="background-color: {{ Options::web_options(321,'str_data') }}">   
    <div class="relative"> 
        <div class="container{{(Options::web_options(321)==1) ? '-fluid' : '' }}"> 
            <div class="row flex">
                <div class="col-md-3 col-sm-5 col-xs-6 mob-pr-0"> 
        
                    <div class="inline-block"> 

                        <h1 class="seo">{{ Options::company_name() }}</h1>
                        
                        <a class="logo inline-block" href="/" title="{{Options::company_name()}}" rel="canonical">
                            <img src="{{ Options::domain() }}{{Options::company_logo()}}" alt="{{Options::company_name()}}" class="img-responsive"/>
                        </a>
                        
                    </div> 
                </div>

                <div class="col-md-9 col-sm-7 col-xs-6 sm-static xs-no-lpadd">

                    <ul class="text-right header-list"> 
                        <li class="inline-block sm-above-relative">
                            <div class="inline-block pointer JSopen-search-modal">
                                <span class="inline-block bg-sprite valign sprite-search"></span>
                                <!-- <i class="fas fa-search"></i> -->
                                <span class="hidden-xs"> {{Language::trans('Pretraga')}} </span>
                            </div>


                            <!-- SEARCH BUTTON -->
                            <div class="header-search JSsearch-modal">  
                                <div class="search-content relative"> 
                                     
                                    <div class="row flex justify-center"> 
                                        @if(Options::gnrl_options(3055) == 0)
                                           
                                            <div class="col-xs-4 hidden no-padding JSselectTxt">  
                                                {{ Groups::firstGropusSelect('2') }} 
                                            </div>

                                        @else

                                            <input type="hidden" class="JSSearchGroup2" value="">

                                        @endif
                 
                                        <div class="col-xs-12 no-padding JSsearchContent2">  
                                            <form autocomplete="off" class="relative">
                                                <input class="search-field" type="text" id="JSsearch2" placeholder="{{ Language::trans('Pretraži') }}" />

                                                <button type="button" onclick="search2()" class="JSsearch-button2 hidden-xs"> 
                                                    <span class="inline-block bg-sprite valign sprite-search"></span>
                                                    <!-- <i class="fas fa-search"></i> --> 
                                                </button>
                                            </form>      
                                        </div>

                                    </div>  
                                </div> 
                                
                                <!-- <span class="inline-block pointer close-search"><i class="fas fa-times"></i></span> -->
                            </div>
                        </li>

                        <li class="inline-block">
                            @include('shop/themes/'.Support::theme_path().'partials/cart_top')
                        </li>

                        <li class="inline-block">
                            <div class="JSmeni inline-block pointer">
                                <!-- <span class="inline-block bg-sprite valign sprite-meni"></span> -->

                                <div class="inline-block valign text-center">
                                    <div class="resp-nav-btn valign inline-block">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                     </div>  
                                 </div>

                                <span class="hidden-xs"> {{Language::trans('Meni')}} </span>
                            </div>
                        </li>
                    </ul>

                    <!-- <div class="resp-nav-btn inline-block pointer hidden-md hidden-lg">
                        <span class="fas fa-bars"></span>
                    </div>  -->

                </div>
            </div> 
 

            <div id="responsive-nav" class="md-text-center">
                <div>
                    <!-- <div class="JSclose-nav text-right hidden-md hidden-lg">&times;</div>  -->

                    <!-- LOGIN AND REGISTRATION NOT LOGGED-->
                    @if(Session::has('b2c_kupac'))
                    <ul class="inline-block log-user">

                        <li> {{ WebKupac::get_user_name() }} {{ WebKupac::get_company_name() }} </li>
                        <li class="JScall-wish">
                            @if(WebKupac::get_user_name())
                                <a href="{{Options::base_url()}}{{Url_mod::slug_trans('korisnik')}}/{{Url_mod::slug_trans(WebKupac::get_user_name())}}">
                                    <!-- <span class="JSbroj_wish "> {{ Cart::broj_wish() }} </span> -->
                                    <span class="inline-block bg-sprite valign sprite-wish"></span>
                                    <span> {{Language::trans('Lista želja')}} </span>
                                </a> 
                            @elseif(WebKupac::get_company_name())
                                <a href="{{Options::base_url()}}{{Url_mod::slug_trans('korisnik')}}/{{Url_mod::slug_trans(WebKupac::get_company_name())}}">
                                    <!-- <span class="JSbroj_wish "> {{ Cart::broj_wish() }} </span> -->
                                    <span class="inline-block bg-sprite valign sprite-wish"><!-- {{ Cart::broj_wish() }} --></span> 
                                    <span> {{Language::trans('Lista želja')}} </span>
                                </a> 
                            @endif
                        </li>

                        <li class="JScall-edit-user"> 
                            @if(WebKupac::get_user_name())
                                <a href="{{Options::base_url()}}{{Url_mod::slug_trans('korisnik')}}/{{Url_mod::slug_trans(WebKupac::get_user_name())}}">
                                   <span class="user-icon inline-block bg-sprite valign sprite-wish"><!-- {{ Cart::broj_wish() }} --></span> 
                                   <span> {{Language::trans('Lični podaci')}} </span>
                                </a> 
                            @elseif(WebKupac::get_company_name())
                                <a href="{{Options::base_url()}}{{Url_mod::slug_trans('korisnik')}}/{{Url_mod::slug_trans(WebKupac::get_company_name())}}">
                                    <span class="user-icon inline-block bg-sprite valign sprite-wish"><!-- {{ Cart::broj_wish() }} --></span> 
                                   <span> {{Language::trans('Lični podaci')}} </span>
                                </a> 
                            @endif
                        </li>

                        <li> 
                            <a id="logout-button" href="{{Options::base_url()}}logout" title="{{ Language::trans('Odjavi se') }}">
                                <span>{{Language::trans('Odjavi se')}}</span>
                                <i class="fas fa-sign-out-alt"></i>
                            </a>
                        </li>
                    </ul>
                    @endif 

                    <ul id="main-menu" class="valign">
                        @foreach(All::header_menu_pages() as $row)
                        <li>
                           
                            @if(All::broj_cerki($row->web_b2c_seo_id) > 0)  
                           
                            <a href="{{ Options::base_url().Url_mod::page_slug($row->naziv_stranice)->slug }}">{{Url_mod::page_slug($row->naziv_stranice)->naziv}}</a>

                            <ul class="drop-2 text-left">
                                @foreach(All::header_menu_pages($row->web_b2c_seo_id) as $row2)
                                <li> 
                                    <a href="{{ Options::base_url().Url_mod::page_slug($row2->naziv_stranice)->slug }}">{{Url_mod::page_slug($row2->naziv_stranice)->naziv}}</a>
                                    <ul class="drop-3">
                                        @foreach(All::header_menu_pages($row2->web_b2c_seo_id) as $row3)
                                        <li> 
                                            <a href="{{ Options::base_url().Url_mod::page_slug($row3->naziv_stranice)->slug }}">{{Url_mod::page_slug($row3->naziv_stranice)->naziv}}</a>
                                        </li>
                                        @endforeach
                                    </ul>
                                </li>
                                @endforeach
                            </ul>
                           
                            @else   
                           
                            <a href="{{ Options::base_url().Url_mod::page_slug($row->naziv_stranice)->slug }}">{{Url_mod::page_slug($row->naziv_stranice)->naziv}}</a> 
                           
                            @endif                    
                        </li>                     
                        @endforeach 

                        @if(Options::web_options(121)==1)
                            <?php $konfiguratori = All::getKonfiguratos(); ?>
                           
                            @if(count($konfiguratori) > 0)
                                @if(count($konfiguratori) > 1)
                                <li>
                                    <a href="#!" class="konf-dropbtn">
                                        {{Language::trans('Konfiguratori')}}
                                    </a>
                                    <ul class="drop-2 text-left">
                                        @foreach($konfiguratori as $row)
                                        <li>
                                            <a href="{{ Options::base_url() }}{{Url_mod::slug_trans('konfigurator')}}/{{ $row->konfigurator_id }}">{{ Language::trans($row->naziv) }}</a>
                                        </li>
                                        @endforeach
                                    </ul>
                                </li>
                                @else
                                <li>
                                    <a href="{{ Options::base_url() }}{{Url_mod::slug_trans('konfigurator')}}/{{ $konfiguratori[0]->konfigurator_id }}">
                                        {{Language::trans('Konfigurator')}}
                                    </a>
                                </li>
                                @endif
                            @endif
                        @endif
                    </ul>    



                    <!-- LOGIN AND REGISTRATION NOT LOGGED-->
                    <div>
                        @if(!Session::has('b2c_kupac'))
                            <p class="user-acount"> {{ Language::trans('Nalog') }} </p>
                        
                            <div class="inline-block">
                                
                                <!-- <a class="dropdown-toggle inline-block login-btn" href="#" role="button" data-toggle="dropdown">
                                    <span class="fas fa-user"></span>
                                </a> -->
                                
                                <ul class="login-dropdown"> 
                                    <li>
                                        <a id="registration-icon" href="{{Options::base_url()}}{{ Url_mod::slug_trans('registracija') }}">{{ Language::trans('Napravi nalog') }}</a>
                                    </li>
                                    <li>
                                       <!--  <a href="#" id="login-icon" data-toggle="modal" data-target="#loginModal">{{ Language::trans('Prijavi se') }}</a> -->
                                        <a id="login-icon" href="{{Options::base_url()}}{{ Url_mod::slug_trans('prijava') }}">{{ Language::trans('Prijavi se') }}</a>
                                    </li>

                                </ul>
                             </div> 
                         @endif 
                    </div>
                </div>  
            </div>
        </div>  
    </div> 
</header>
<!-- OPEN MODAL ON CLICK AVAILABLE SOON BUTTON --> 
<div class="modal fade" id="available_soon" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" >
       
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center">{{ Language::trans('Ukoliko želite da Vas obavestimo kada proizvod ponovo bude na stanju, upišite Vaše ime i e-mail adresu.') }}</h4> 
            </div>

            <div class="modal-body">
              <input class="hidden" id="JSRobaId" value=""> 

                <div class="form-group">
                    <label for="">{{ Language::trans('Vaše ime') }}:</label>
                    <input class="form-control" autocomplete="off" id="name">
                </div>

                <div class="form-group">
                    <label for="JSemail_login">{{ Language::trans('Vaš') }} e-mail: </label>
                    <input class="form-control" placeholder="" id="mail" autocomplete="off">
                </div>
                
                <div class="form-group">
                    <label for="">{{ Language::trans('Vaš') }} broj telefona: </label>
                    <input class="form-control" required id="telefon" type="text" value="" autocomplete="off">
                </div>
            </div>
            
            <div class="modal-footer text-right">
                <button type="submit" class="button JSInformMe" >{{ Language::trans('Pošalji') }}</button>
            </div>
       
        </div>   
    </div>
</div> 
  
<!-- header.blade end -->

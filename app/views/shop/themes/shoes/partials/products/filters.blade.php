<?php // echo $kara; ?>
<div class="filters row">
<!-- CHECKED FILTERS -->
   		<div class="col-xs-12 no-padding">
   			<!-- <br> -->

		    <div class="clearfix margin-v-10 JShidden-if-no-filters">
		    	<span>{{ Language::trans('Izabrani filteri') }}:</span>
		    	<a class="JSreset-filters-button inline-block pull-right" role="button" href="javascript:void(0)" rel=”nofollow”>{{ Language::trans('Poništi filtere') }} <span class="fas fa-times"></span></a>
		    </div> 
			
			<ul class="selected-filters">
				<?php $br=0;
				foreach($niz_proiz as $row){
					$br+=1;
					if($row>0){
				?>
					<li>
					    {{All::get_manofacture_name($row)}}     
					    <a href="javascript:void(0)" rel=”nofollow”>
					        <span class="JSfilter-close fas fa-times" data-type="1" data-rb="{{$br}}" data-element="{{$row}}"></span>
					    </a>
					</li>
				<?php }}

				$br=0;
				foreach($niz_karakteristike as $row){
					$br+=1;
					if($row>0){
				?>
					<li>
						@if(empty($boja = All::get_fitures_color($row))) 
						{{All::get_fitures_name($row)}}
            			@else
            			<span class="valign color-circle relative" style="
               				background: {{ !empty($boja = All::get_fitures_color($row)) ? $boja : 'transparent' }};
                            border: {{ ((All::get_fitures_color($row)) == '#ffffff') ? '1px solid black' : '1px solid transparent'}} ;
                            display: {{ ((All::get_fitures_color($row)) == '') ? 'none' : 'inline-block'}} ;
               			">
                        	<span class="checkmark" style="filter: {{ ((All::get_fitures_color($row)) == '#ffffff') ? 'unset' : '';}} ;"></span>
               			
               			</span>
               			@endif
						<a href="javascript:void(0)" rel=”nofollow”>
					        <span class="JSfilter-close fas fa-times" data-type="2" data-rb="{{$br}}" data-element="{{$row}}"></span>
					    </a>
					</li>
					<?php }}?>
			</ul>
			<input type="hidden" id="JSmanufacturer_ids" value="{{$proizvodjac_ids}}"/>
			<input type="hidden" id="JSmanufacturer_attr" value="{{$kara}}"/>
		</div>	
		<!-- END CHECKED -->


		<!-- SLAJDER ZA CENU -->
		<div> 
			<span class="filter-label pb-15">{{ (Language::trans('Cena')) }}</span>
			<div id="JSslider-range"></div><br>
			<span id="JSamount" class="filter-price hidden"></span>
			
			<div class="row text-bold font-14">
				<div class="col-xs-6 no-padding">
					<span id="JSfil-min-price"></span> <span class="text-uppercase">rsd</span>
				</div>
				<div class="col-xs-6 no-padding text-right">
					<span id="JSfil-max-price"></span> <span class="text-uppercase">rsd</span>
				</div>
			</div>
		</div>

  		<ul class="col-md-12 col-sm-12 col-xs-12 no-padding"> 

		    <!-- FILTER ITEM -->
		    @if(count($manufacturers)>0)
			<li class="filter-box">
				<span class="filter-label">{{ (Language::trans('Proizvođač')) }}</span>
                <a href="javascript:void(0)" class="filter-links JSfilters-slide-toggle" rel=”nofollow”>
                	<!-- <span class="choosed_filter">@foreach($manufacturers as $key => $value) @if(in_array($key,$niz_proiz)) {{ All::get_manofacture_name($key).' ' }} @endif @endforeach </span> -->
               		<!-- <i class="fas fa-plus"></i> -->

	               	 	<span class="text-bold">
	                    	@if(count($niz_proiz) > 0 AND !in_array(0,$niz_proiz))
	                			@foreach($manufacturers as $key => $value) 
	                    			@if(in_array($key,$niz_proiz))  
		                    			{{ All::get_manofacture_name($key).' ' }}
	                    			@endif
	        					@endforeach
	                    	@else
	                    		{{ Language::trans('Izaberi') }}
	                    	@endif

	                    	<!-- <span class="choosed_filter">@foreach($manufacturers as $key => $value) @if(in_array($key,$niz_proiz)) {{ All::get_manofacture_name($key).' ' }} @endif @endforeach </span> -->
	                	<!-- <i class="fas fa-plus"></i> -->
	                	</span>
	                	<i class="fa fa-caret-right"></i>
	                </a>

			 	 	<div class="JSfilters-slide-toggle-content">
						@foreach($manufacturers as $key => $value)
							@if(in_array($key,$niz_proiz))
								<label>
									<input name="proizvodjac[]" onchange="filters_send()" value="{{ $key }}" type="checkbox" checked>
									<span class="filter-text">
										{{All::get_manofacture_name($key)}} 
									</span>
									<span class="filter-count">
										@if(Options::filters_type()) {{ $value }} @endif
									</span>
								</label>
							@else
								<label>
									<input name="proizvodjac[]" onchange="filters_send()" value="{{ $key }}" type="checkbox">
									<span class="filter-text">
										{{All::get_manofacture_name($key)}} 
									</span>
									<span class="filter-count">
										@if(Options::filters_type()) {{ $value }} @endif
									</span>
								</label>
							@endif
						@endforeach
					</div>				 				 
				</li>
			@endif

			@foreach($characteristics as $keys => $values)
			<li class="filter-box">
				<span class="filter-label">{{ Language::trans($keys) }}</span>
 				<a  href="javascript:void(0)" class="filter-links JSfilters-slide-toggle" rel=”nofollow”>
					<!-- {{Language::trans($keys)}}  -->
					<!-- <span class="choosed_filter"> @foreach($values as $key => $value) @if(in_array($key, $niz_karakteristike)) {{ All::get_fitures_name($key) }} @endif @endforeach </span> -->
					 <span class="text-bold">
                    	<?php $checkCharact = false; ?>
                    	@if(count($niz_karakteristike) > 0 AND !in_array(0,$niz_karakteristike))
                			@foreach($values as $key => $value) 
                    			@if(in_array($key,$niz_karakteristike))
                    				<?php $checkCharact = true; ?>
                    				@if(empty($boja = All::get_fitures_color($key))) 
	                    			{{ All::get_fitures_name($key).' ' }}
	                    			@else
	                    			<span class="valign color-circle relative BGtooltip" style="
			               				background: {{ !empty($boja = All::get_fitures_color($key)) ? $boja : 'transparent' }};
			                            border: {{ ((All::get_fitures_color($key)) == '#ffffff') ? '1px solid black' : '1px solid transparent'}} ;
			                            display: {{ ((All::get_fitures_color($key)) == '') ? 'none' : 'inline-block'}} ;
			               			">
		                            	<span class="checkmark" style="filter: {{ ((All::get_fitures_color($key)) == '#ffffff') ? 'unset' : '';}} ;"></span>
		                            	<span class="BGtooltiptext">{{All::get_group_by_fiture($key)}}</span>
			               			
			               			</span>
			               			@endif
                    			@endif
        					@endforeach
        					@if(!$checkCharact)
        						{{ Language::trans('Izaberi') }}
        					@endif
                    	@else
                    		{{ Language::trans('Izaberi') }}
                    	@endif
                	</span>
					<i class="fa fa-caret-right"></i>						
				</a>
				<div class="JSfilters-slide-toggle-content">
					@foreach($values as $key => $value)
					<?php if(in_array($key, $niz_karakteristike)){ ?>
					<label class="@if($boja = All::get_fitures_color($key)) filters-colors relative @endif">
						<input type="checkbox" onchange="filters_send()" name="vrednost[]" value="{{ $key }}" checked>
						<span class="filter-text">
							@if(!$boja = All::get_fitures_color($key))
								<span> {{All::get_fitures_name($key)}} </span>
							@endif
							<span class="valign color-circle relative BGtooltip" style="
	               				background: {{ !empty($boja = All::get_fitures_color($key)) ? $boja : 'transparent' }};
	                            border: {{ ((All::get_fitures_color($key)) == '#ffffff') ? '1px solid black' : '1px solid transparent'}} ;
	                            display: {{ ((All::get_fitures_color($key)) == '') ? 'none' : 'inline-block'}} ;
	               			">
                            	<span class="checkmark" style="filter: {{ ((All::get_fitures_color($key)) == '#ffffff') ? 'unset' : '';}} ;"></span>
                            	<span class="BGtooltiptext">{{All::get_group_by_fiture($key)}}</span>
	               			
	               			</span>
						</span>
						<span class="filter-count">
							@if(Options::filters_type()) {{ $value }} @endif
						</span>
					</label>
					<?php }else {?>
					<label class="@if($boja = All::get_fitures_color($key)) filters-colors relative @endif">
						<input type="checkbox" onchange="filters_send()" name="vrednost[]" value="{{ $key }}"> 
						<span class="filter-text">
							@if(!$boja = All::get_fitures_color($key))
								<span> {{All::get_fitures_name($key)}} </span>
							@endif
							
							<span class="valign color-circle relative BGtooltip" style="
	               				background: {{ !empty($boja = All::get_fitures_color($key)) ? $boja : 'transparent' }};
	                            border: {{ ((All::get_fitures_color($key)) == '#ffffff') ? '1px solid black' : '1px solid transparent'}} ;
	                            display: {{ ((All::get_fitures_color($key)) == '') ? 'none' : 'inline-block'}} ;
	               			">
	               				<span class="checkmark" style="filter: {{ ((All::get_fitures_color($key)) == '#ffffff') ? 'unset' : '';}} ;"></span>
	               				<span class="BGtooltiptext">{{All::get_group_by_fiture($key)}}</span>
	               			</span>
						</span>
						<span class="filter-count">
							@if(Options::filters_type()) {{ $value }} @endif
						</span>
					</label>
					<?php } ?>
					@endforeach
				</div>
			</li>
			@endforeach			
        </ul>	 
		<input type="hidden" id="JSfilters-url" value="{{$url}}" />

	<script>
		var max_web_cena_init = {{ $max_web_cena }};
		var min_web_cena_init = {{ $min_web_cena }};
		var max_web_cena = {{ $cene!='0-0' ? explode('-',$cene)[1] : $max_web_cena }};
		var min_web_cena = {{ $cene!='0-0' ? explode('-',$cene)[0] : $min_web_cena }};
		var kurs = {{ Session::has('valuta') ? (Session::get('valuta')!="2" ? 1 : Options::kurs()) : 1 }};
	</script>
</div>

 
 
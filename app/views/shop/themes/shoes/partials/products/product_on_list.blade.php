<!-- PRODUCT ON LIST -->
<div class="shop-product-card-list"> 

	<!-- @if(All::provera_akcija($row->roba_id)) 
	<span class="ribbon-sale"> {{ Language::trans('Akcija') }} </span>  
	@endif -->

	<div class="row flex list-wrapp-align"> 

		<div class="col-md-3 col-sm-3 col-xs-4 product-image-wrapper">
			<a class="flex relative" href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($row->roba_id))}}" class="">
				<img class="product-image img-responsive JSlazy_load" src="{{Options::base_url()}}images/quick_view_loader.gif" data-src="{{ Options::domain() }}{{ Product::web_slika($row->roba_id) }}" alt="{{ Product::seo_title($row->roba_id) }}" />
			</a>
		</div> 

		<div class="col-md-9 col-sm-9 col-xs-8 no-padding"> 

			<div class="list-flex">
				<h2 class="product-name">
					<a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($row->roba_id))}}"> {{ Product::short_title($row->roba_id) }}</a> 
				</h2>

				<!-- @if(AdminOptions::web_options(153)==1)
				<div class="generic_car_list col-md-6 col-sm-7 col-xs-12 no-padding">
					{{ Product::get_web_roba_karakteristike_short($row->roba_id) }}
				</div>
				@endif -->

				<div class="flex price-btn-wrapper">
					<div class="price-holder"> 
						@if(All::provera_akcija($row->roba_id))
						<span class="product-old-price">{{ Cart::cena(Product::old_price($row->roba_id)) }}</span>
						@endif 
						<span>{{ Cart::cena(Product::get_price($row->roba_id)) }}</span>
					</div>

					<!-- <span class="review">{{ Product::getRating($row->roba_id) }}</span>  -->

					<div class="add-to-cart-container text-right"> 

						@if(Cart::kupac_id() > 0)
						<!-- <button class="like-it JSadd-to-wish" data-roba_id="{{$row->roba_id}}" title="{{ Language::trans('Dodaj na listu želja') }}"><i class="far fa-heart"></i></button>  -->
						@else
					  	<!-- <button class="like-it JSnot_logged" data-roba_id="{{$row->roba_id}}" title="{{ Language::trans('Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima') }}"><i class="far fa-heart"></i></button>  -->
						@endif	
						<!--  <div class="">Šifra artikla: {{ Product::get_sifra($row->roba_id) }}</div> -->

						@if(Product::getStatusArticle($row->roba_id) == 1)
							@if(Cart::check_avaliable($row->roba_id) > 0)

								@if(!Product::check_osobine($row->roba_id)) 
								<button class="buy-btn JSadd-to-cart" data-roba_id="{{$row->roba_id}}">{{ Language::trans('Kupi') }}</button>

								@else

								<a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($row->roba_id))}}" class="buy-btn">
									{{ Language::trans('Vidi artikal') }}			 
								</a>			    
								@endif

								@if(Options::compare()==1 AND isset($filter_prikazi) AND $filter_prikazi == 1)
								<!-- <button class="like-it JScompare {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}" title="{{ Language::trans('Uporedi') }}">
									<i class="fas fa-exchange-alt"></i>
								</button> -->
								@endif	

								@else 	

									<button class="not-available">{{ Language::trans('Nije dostupno') }}</button>

									@if(Options::compare()==1 AND isset($filter_prikazi) AND $filter_prikazi == 1)
									<!-- <button class="like-it JScompare {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}" title="{{ Language::trans('Uporedi') }}">
										<i class="fas fa-exchange-alt"></i>
									</button> -->
									@endif		 
								@endif
								
							@else	 

								<button class="buy-btn">{{ Product::find_flag_cene(Product::getStatusArticle($row->roba_id),'naziv') }}</button>

								@if(Options::compare()==1 AND isset($filter_prikazi) AND $filter_prikazi == 1)
								<!-- <button class="like-it JScompare {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}" title="{{ Language::trans('Uporedi') }}">
									<i class="fas fa-exchange-alt"></i>
								</button> -->
							@endif		
						@endif
					</div>

				</div>

				<div class="article_top_info flex">
				
					@if(Cart::check_avaliable($row->roba_id) == 0)
					<div class="sale-wrap"> 
						<div class="sale-label inline-block">
							{{ Language::trans('Uskoro!') }} 
						</div>
					</div>
					@endif
					@if(All::provera_akcija($row->roba_id))
					<div class="sale-wrap"> 
						<div class="sale-label inline-block text-uppercase">
							{{ Language::trans('Šok cena!') }}  <!-- {{ Product::getSale($row->roba_id) }}% --> 
						</div>
					</div>
					@endif
					<?php $tip_cene = DB::table('roba')->where('roba_id', $row->roba_id)->pluck('tip_cene') ?>
					@if($tip_cene == 3)
					<div class="sale-wrap"> 
						<div class="sale-label inline-block">
							{{ Language::trans('Novo!') }} 
						</div>
					</div>
					@endif
		
			</div>
			</div>
		</div> 

		<!-- OPIS KARAKTERISTIKE -->
		<!-- <div class="col-md-5  ">
			{{ Product::get_karakteristike_short_grupe($row->roba_id) }}
		</div>  -->

		@if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
		<a class="article-edit-btn JSFAProductModalCall" data-roba_id="{{$row->roba_id}}" href="javascript:void(0)" rel="nofollow">{{ Language::trans('IZMENI ARTIKAL') }}</a>
		@endif

	</div>
</div>

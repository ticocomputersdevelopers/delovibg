<style>
.text-field {
    height: 21px;
    line-height: 15px;
    border: none;
    margin: 0;
    border: 1px solid #9D9D9D;
    border-radius: 0 !important;
    padding: 0 5px;
    box-shadow: none;
    color: #000;
    font-size: 12px;
    background-color: #fff;
}
    
.row::after {content: ""; clear: both; display: table;}

[class*="col-"] { float: left; padding: 5px; display: inline-block;}

.col-1 {width: 8.33%;}
.col-2 {width: 16.66%;}
.col-3 {width: 25%;}
.col-4 {width: 33.33%;}
.col-5 {width: 41.66%;}
.col-6 {width: 50%;}
.col-7 {width: 58.33%;}
.col-8 {width: 66.66%;}
.col-9 {width: 75%;}
.col-10 {width: 83.33%;}
.col-11 {width: 91.66%;}
.col-12 {width: 100%;}

* {margin: 0; font-family: DejaVu Sans; box-sizing: border-box;}

body {font-size: 16px;}

table {border-collapse: collapse; font-size: 12px; margin-bottom: 15px; width: 100%;}

td, th {border: 1px solid #cacaca; text-align: left; padding: 3px;}

tr:nth-child(even) {background-color: #f5f6ff;}

h2 {margin-bottom: 15px;}

.container {width: 90%; margin: 42px auto;}

.logo img { max-width: 150px; width: 100%;}

.text-right {text-align: right;}

.signature {background: none; color: #808080;}

.signature td, th {border: none; background: none;}
 
.company-info {font-size: 11px;}

.comp-name {font-size: 18px; font-weight: bold;}
 
.kupac-info td {border: none;}

.kupac-info { border: 1px solid #ddd; }

thead {background: #eee;}

.ziro {font-weight: bold; font-size: 14px; margin: 10px 0;}
 
.napomena p {margin: 10px 0; font-size: 13px;}

.info-1 {margin-bottom: 30px;}

.rbr {width: 50px;}

.artikli td {text-align: center;}

.cell-product-name {text-align: left !important;}

.sum-span {margin-right: 20px;}

.page-break {
    page-break-after: always;
}
</style>

<div class="row">
    <div class="col-5">
        <div>
            <div>Uplatilac:</div>
            <textarea cols="30" rows="10">$kupac->ime</textarea>
        </div>

        <div>
            <div>Svrha uplate:</div>
            <textarea cols="30" rows="10">Porudzbina broj $web_b2c_narudzbina->web_b2c_narudzbina_id</textarea>
        </div>

        <div>
            <div>Primalac:</div>
            <textarea cols="30" rows="10">BG Elektronik d.o.o Beograd <br> Viline Vode 47, Beograd</textarea>
        </div>
    </div>

    <div class="col-1"></div>

    <div class="col-5" style="text-align: right;">
        <div>
            <div>Šifra plaćanja:</div>
            <div class="text-field">289</div>
        </div>

        <div>
            <div>Valuta:</div>
            <div class="text-field">RSD</div>
        </div>

        <?php $troskovi = Cart::troskovi($web_b2c_narudzbina_id);
          $popust = Order::popust($web_b2c_narudzbina_id); 
          $vaucer_popust = Order::vaucer_popust($web_b2c_narudzbina_id); ?>

        <div>
            <div>Iznos:</div>
            <div>
                <span class="text-field">{{Cart::cena(Order::narudzbina_ukupno($web_b2c_narudzbina_id)+$troskovi-$popust-$vaucer_popust)}}</span>
            </div>
        </div>

        <div>
            <div>Račun:</div>
            <div class="text-field">265-1100310004520-69</div>
        </div>

        <div>
            <div>Model:</div>
            <div class="text-field">97</div>
        </div>

        <div>
            <div>Poziv:</div>
            <div class="text-field"></div>
        </div>
    </div>
</div>
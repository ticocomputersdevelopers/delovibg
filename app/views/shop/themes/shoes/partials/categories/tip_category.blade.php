<div class="manufacturer-categories"> 
    <h3>{{$title}}</h3>
    <ul>
    @foreach(Support::tip_categories($tip_id) as $key => $value)
    <li>
        <a href="{{ Options::base_url()}}{{ Url_mod::slug_trans('tip') }}/{{ Url_mod::slug_trans($tip) }}/{{ Url_mod::slug_trans($key) }}">
            <span>{{ Language::trans($key) }}</span>
            <!-- <span>{{ $value }}</span> -->
        </a>
    </li>
    @endforeach 
    </ul>
</div> 

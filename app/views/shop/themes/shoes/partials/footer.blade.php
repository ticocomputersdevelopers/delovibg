 <footer {{ Options::web_options(322, 'str_data') != '' ? 'style=background-color:' . Options::web_options(322, 'str_data') : '' }}>
 	<div class="container{{(Options::web_options(322)==1) ? '-fluid' : '' }}">
 		<div class="row JSfooter-cols">  
 			@foreach(All::footer_sections() as $footer_section)
 			@if($footer_section->naziv == 'slika')
 			<div class="col-md-4 col-sm-4 col-xs-12 footer-column">		 
				@if(!is_null($footer_section->slika))
				<a class="footer-logo" href="{{ $footer_section->link }}">
					<img src="{{ Options::domain() }}{{ $footer_section->slika }}" alt="{{Options::company_name()}}" />
				</a>
				@else
				<a class="footer-logo" href="/" title="{{Options::company_name()}}">
					<img src="{{ Options::domain() }}{{Options::company_logo()}}" alt="{{Options::company_name()}}" />
				</a>
				@endif

				<div class="JSInlineFull" data-target='{"action":"footer_section_content","id":"{{$footer_section->futer_sekcija_id}}"}'>
					{{ $footer_section->sadrzaj }} 
				</div> 
 			</div>
 			@elseif($footer_section->naziv == 'text')
 			<div class="col-md-4 col-sm-4 col-xs-12 footer-column text-footer">

 				<a class="hidden-lg hidden-md hidden-sm logo v-align inline-block" href="/" title="{{Options::company_name()}}" rel="nofollow">
                    <img src="{{ Options::domain() }}images/footer_logo.png" alt="{{Options::company_name()}}" class="img-responsive"/>
                </a>
 
				<h5 class="hidden ft-section-title JSInlineShort" data-target='{"action":"footer_section_label","id":"{{$footer_section->futer_sekcija_id}}"}'>
					{{ $footer_section->naslov }}
				</h5>

				<div class="JSInlineFull" data-target='{"action":"footer_section_content","id":"{{$footer_section->futer_sekcija_id}}"}'>
					{{ $footer_section->sadrzaj }} 
				</div> 
 			</div>
 			@elseif($footer_section->naziv == 'linkovi')
 			<div class="col-md-4 col-sm-4 col-xs-12 footer-column links-foot"> 
 				<h5 class="hidden ft-section-title JSInlineShort" data-target='{"action":"footer_section_label","id":"{{$footer_section->futer_sekcija_id}}"}'>
 					{{ $footer_section->naslov }}
 				</h5>

 				<ul class="footer-links">
 					@foreach(All::footer_section_pages($footer_section->futer_sekcija_id) as $page)
 					<li class="relative">
 						<a href="{{ Options::base_url().Url_mod::page_slug($page->naziv_stranice)->slug }}">{{ Url_mod::page_slug($page->naziv_stranice)->naziv }}</a>
 					</li>
 					@endforeach
 				</ul>  
 			</div>
 			@elseif($footer_section->naziv == 'kontakt')
 			<div class="col-md-4 col-sm-4 col-xs-12 footer-column"> 

 				<a class="hidden-xs logo v-align center-block" href="/" title="{{Options::company_name()}}" rel="nofollow">
                    <img src="{{ Options::domain() }}images/footer_logo.png" alt="{{Options::company_name()}}" class="img-responsive"/>
                </a>

 				<h5 class="hidden ft-section-title JSInlineShort" data-target='{"action":"footer_section_label","id":"{{$footer_section->futer_sekcija_id}}"}'>
 					{{ $footer_section->naslov }}
 				</h5>

 				<div class="contact-foot xs-flex xs-space-between">
 					@if(Options::company_phone())
 						<div class="flex mailto">
 							<a class="mr-4" href="tel:{{ Options::company_phone() }}">
	 							<span class="footer-icons inline-block phone-icon"> </span> 
	 						</a>
	 						<span class="hidden-xs">
	 							<a href="tel:{{ Options::company_phone() }}">
		 							{{ Options::company_phone() }}
		 						</a>
	 						</span>
						</div>
 					@endif

 					@if(Options::company_phone())
 						<div class="hidden mailto">
 							<a class="mr-4" href="tel:{{ Options::company_fax() }}">
	 							<span class="footer-icons inline-block phone-icon"> </span> 
	 						</a>
	 						<span class="hidden-xs">
	 							<a href="tel:{{ Options::company_fax() }}">
		 							{{ Options::company_fax() }}
		 						</a>
	 						</span>
						</div>
 					@endif

 					@if(Options::company_email())
 						<div class="flex mailto">
 							<a class="mr-4" href="mailto:{{ Options::company_email() }}">
	 							<span class="footer-icons inline-block mail-icon"> </span> 
	 						</a>
	 						<span class="hidden-xs">
	 							<a href="mailto:{{ Options::company_email() }}">
		 							{{ Options::company_email() }}
		 						</a>
	 						</span>
						</div>
 					@endif

 					@if(Options::company_adress() OR Options::company_city())
 						<div class="flex relative">
 					
	 						<a href="/kontakt"> <span class="mr-4 footer-icons inline-block adress-icon"> </span> </a>
	 				
	 						<span class="hidden-xs">
	 							<a href="/kontakt">
		 							{{ Options::company_adress() }},  
		 							{{ Options::company_city() }}
		 						</a>
	 						</span>
						</div>
 					@endif
 				
					<?php $query_social_icons=DB::table('preduzece')->where(array('preduzece_id'=>1))->get(); ?>
					@foreach ($query_social_icons as $row)
						@if($row->instagram)

						<div class="flex">
 							<a class="mr-4" href="{{$row->instagram}}">
	 							<span class="footer-icons inline-block ig-icon"> </span> 
	 						</a>
	 						<span class="hidden-xs">
	 							<a href="{{$row->instagram}}">
		 							bg.elektronik
		 						</a>
	 						</span>
						</div>
						@endif


						@if($row->facebook)
						<div class="flex">
 							<a class="mr-4" href="{{$row->facebook}}">
	 							<span class="footer-icons inline-block fb-icon"> </span> 
	 						</a>
	 						<span class="hidden-xs">
	 							<a href="{{$row->facebook}}">
		 							BgElektronikServis
		 						</a>
	 						</span>
						</div>
						@endif
					@endforeach
 				</div>
 			</div>
 			@elseif($footer_section->naziv == 'drustvene_mreze')
 			<div class="col-md-4 col-sm-4 col-xs-12 footer-column"> 
				<h5 class="hidden ft-section-title JSInlineShort" data-target='{"action":"footer_section_label","id":"{{$footer_section->futer_sekcija_id}}"}'>
					{{ $footer_section->naslov }}
				</h5>
				<div class="social-icons">
					{{Options::social_icon()}}
				</div>  
 			</div>	
 			
  			@elseif($footer_section->naziv == 'mapa' AND Options::company_map() != '' AND Options::company_map() != ';')
 			<div class="col-md-4 col-sm-4 col-xs-12 footer-column">
				<div class="map-frame relative">

					<div class="map-info">
						<h5>{{ Options::company_name() }}</h5>
						<h6>{{ Options::company_adress() }}, {{ Options::company_city() }}</h6> 
					</div>
					<iframe src="https://maps.google.com/maps?q={{ All::lat_long()[0] }},{{ All::lat_long()[1] }}&output=embed" width="100%" height="100%" frameborder="0" aria-hidden="false" tabindex="0"></iframe> 

				</div> 
 			</div>						
 			@endif
 			@endforeach  
 
 		</div> 
 	</div> 
  
 	<span class="JSscroll-top"><i class="fas fa-angle-up"></i></span>

	 <div class="text-center foot-note"> 
	 	<div class="container">
		 	<div class="text-center">
				<div class="JSInlineFull" data-target='{"action":"front_admin_content","id":4}'>{{ Support::front_admin_content(4) }}</div> 
			</div> 

			<div>
				Produced by
				<a href="https://www.selltico.com/"> Selltico. </a>  
				<br class="hidden-lg hidden-md hidden-sm"><br class="hidden-lg hidden-md hidden-sm">
				Design by Artigma. 
			</div>
		</div>
	</div> 

</footer>


@if(Support::banca_intesa()) 
<div class="after-footer"> 
	<div class="container"> 
		<div class="banks flex {{(empty(Options::gnrl_options(3023,'str_data')) && empty(Options::gnrl_options(3024,'str_data'))) ? 'justify-between' : 'justify-center' }}">
 			<ul class="list-inline sm-text-center text-right">
 				<li><img alt="bank-logo" class="JSlazy_load" src="{{Options::base_url()}}images/quick_view_loader.gif" data-src="{{ Options::domain() }}images/cards/master-card.png"></li>
 				<li><img alt="bank-logo" class="JSlazy_load" src="{{Options::base_url()}}images/quick_view_loader.gif" data-src="{{ Options::domain() }}images/cards/maestro-card.png"></li>
 				<li><img alt="bank-logo" class="JSlazy_load" src="{{Options::base_url()}}images/quick_view_loader.gif" data-src="{{ Options::domain() }}images/cards/visa-card.png"></li> 

				@if(!empty(Options::gnrl_options(3023,'str_data')) && !empty(Options::gnrl_options(3024,'str_data')))
	 			<li>
					<img alt="bank-logo" class="JSlazy_load" src="{{Options::base_url()}}images/quick_view_loader.gif" data-src="{{ Options::domain() }}images/cards/dinacard.png">
	 			</li>
 				@endif			 				
 				
 				<li><img alt="bank-logo" class="JSlazy_load" src="{{Options::base_url()}}images/quick_view_loader.gif" data-src="{{ Options::domain() }}images/cards/american-express.png"></li>
 			</ul>
 	
 			<ul class="list-inline sm-text-center">
 				<li>
 					@if(!empty(Options::gnrl_options(3023,'str_data')) && !empty(Options::gnrl_options(3024,'str_data')))
 					<a href="https://www.bancaintesa.rs" class="banc_main_logo" target="_blank" rel="nofollow">
 						<img alt="bank-logo" class="JSlazy_load" src="{{Options::base_url()}}images/quick_view_loader.gif" data-src="{{ Options::domain() }}images/cards/banca-intesa.png">
	 				</a>
	 				@else
	 				<a href="http://www.e-services.rs/" target="_blank" rel="nofollow">
 						<img alt="bank-logo" class="JSlazy_load" src="{{Options::base_url()}}images/quick_view_loader.gif" data-src="{{ Options::domain() }}images/cards/chipCard.jpg">
	 				</a> 
	 				@endif
	 			</li> 
 				<li>
 					<a href="https://rs.visa.com/pay-with-visa/security-and-assistance/protected-everywhere.html" target="_blank" rel="nofollow">
 						<img alt="bank-logo" class="JSlazy_load" src="{{Options::base_url()}}images/quick_view_loader.gif" data-src="{{ Options::domain() }}images/cards/verified-by-visa.jpg">
	 				</a>
	 			</li>
 				<li>
 					<a href="https://www.mastercard.rs/sr-rs/consumers/find-card-products/credit-cards.html" target="_blank" rel="nofollow">
	 					<img alt="bank-logo" class="JSlazy_load" src="{{Options::base_url()}}images/quick_view_loader.gif" data-src="{{ Options::domain() }}images/cards/master-card-secure.gif">
	 				</a>
	 			</li>
 			</ul>
	 	</div>
	</div>
</div> 
@endif  
<!-- FOOTER.blade END -->


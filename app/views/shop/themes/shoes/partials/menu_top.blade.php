@if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array('ARTIKLI')) AND Admin_model::check_admin(array('ARTIKLI_PREGLED')))
<div id="admin-menu"> 
    <div class="container text-right">
        <a href="#!" data-toggle="modal" data-target="#FAProductsModal"> <i class="fas fa-clipboard-list"></i> {{ Language::trans('Artikli') }}</a>
        |
        @if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
        <a href="#!" id="JSShortAdminSave"> <i class="fas fa-save"></i> {{ Language::trans('Sačuvaj izmene') }}</a>
        @endif
        | 
        <span class="ms admin-links"><a target="_blank" href="{{ Options::domain() }}admin"><i class="fas fa-cogs"></i> {{ Language::trans('Admin Panel') }}</a></span> |
        <span class="ms admin-links"><a href="{{ Options::domain() }}admin-logout">{{ Language::trans('Odjavi se') }}</a></span>
    </div>
</div> 

@include('shop/front_admin/modals/products')
@include('shop/front_admin/modals/product')
@endif

<div id="preheader">

    <div class="social-icons hidden">  
        {{Options::social_icon()}} 
    </div>

    <div class="container{{(Options::web_options(321)==1) ? '-fluid' : '' }}"> 
        <div class="top-menu row">

            <div class="col-md-8 col-sm-9">     

                @if(Options::stranice_count() > 0)
                <!-- <span class="JStoggle-btn inline-block color-white hidden-md hidden-lg">
                   <i class="fas fa-bars"></i>                 
                </span>
                <span class="hidden-lg hidden-md"> &nbsp; </span> -->
                @endif 
                <ul class="hidden-small JStoggle-content">
                    @foreach(All::menu_top_pages() as $row)
                    <li class="JSpage-menu-top"><a href="{{ Options::base_url().Url_mod::page_slug($row->naziv_stranice)->slug }}">{{ Url_mod::page_slug($row->naziv_stranice)->naziv }}</a></li>
                    @endforeach
                    
                    @if(Options::company_email())
                    <li class="hidden-sm hidden-xs">
                        <a class="mailto inline-block" href="mailto:{{ Options::company_email() }}">{{ Options::company_email() }}</a>
                    </li>
                    @endif
                </ul>  
            </div>

            <div class="col-md-4 col-sm-3 col-xs-12 md-text-right"> 
                <ul class="flex">
                    @if(Options::company_phone())
                    <li class="xs-flex xs-space-between"> 
                        <!-- <span class="inline-block"> {{ Language::trans('BG ELEKTRONIK') }} </span> -->
                        
                        <a class="mailto inline-block" href="tel:{{ Options::company_phone() }}">
                            {{ Options::company_phone() }}
                        </a>
                    </li>
                    @endif

                    @if(Session::has('b2c_kupac'))

                        <a href="{{Options::base_url()}}{{Url_mod::slug_trans('korisnik')}}/{{Url_mod::slug_trans(WebKupac::get_user_name())}}" rel="nofollow">{{ WebKupac::get_user_name() }}</a>

                        <a href="{{Options::base_url()}}{{Url_mod::slug_trans('korisnik')}}/{{Url_mod::slug_trans(WebKupac::get_company_name())}}" rel="nofollow">{{ WebKupac::get_company_name() }}</a>

                        <a id="logout-button" class="inline-block" href="{{Options::base_url()}}logout" rel="nofollow">{{ Language::trans('Odjavi se') }}</a>

                    @endif

                    @if(Options::checkB2B())
                    <!-- <li class="inline-block">
                        <a id="b2b-login-icon" href="{{Options::domain()}}b2b/login" class="inline-block">B2B</a> 
                    </li> -->
                    @endif 
                </ul>
            </div>   
 
        </div> 
    </div>
</div>



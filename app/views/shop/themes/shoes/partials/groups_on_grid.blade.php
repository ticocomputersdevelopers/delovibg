<!-- GROUPS ON GRID -->
<div class="groups-list">
	<div class="groups-list-card">
		<div class="group-image-wrapper relative">
			<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row2->grupa) }}" class="flex">
				<img class="img-responsive JSlazy_load" src="{{Options::domain()}}images/quick_view_loader.gif"
				data-src="{{ Options::domain() }}{{$row2->putanja_slika}}" alt="{{ $row2->grupa }}"/>
			</a>
		</div>
		<h2 class="groups-list-title">{{ Language::trans($row2->grupa) }}</h2>
		<!-- <span class="groups-list-count">{{Articles::brojiArtikleRekurzivno($row2->grupa_pr_id)}} {{ Language::trans('artikla') }}</span> -->
	</div>
</div>
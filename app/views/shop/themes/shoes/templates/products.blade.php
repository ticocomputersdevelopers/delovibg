<!DOCTYPE html>
<html lang="sr">
<head>
	@include('shop/themes/'.Support::theme_path().'partials/head')

	<script type="application/ld+json">
		{
			"@context": "http://schema.org",
			"@type": "WebPage",
			"name": "<?php echo $title; ?>",
			"description": "<?php echo $description; ?>",
			"url" : "<?php echo Options::base_url().$url; ?>",
			"publisher": {
			"@type": "Organization",
			"name": "<?php echo Options::company_name(); ?>"
		}
	}
</script>
</head>
<body id="product-page" 
@if($strana == All::get_page_start()) id="start-page"  @endif 
@if(Support::getBGimg() != null) 
style="background-image: url({{Options::domain()}}{{Support::getBGimg()}}); background-size: cover; background-repeat: no-repeat; background-attachment: fixed; background-position: center;" 
@endif
>

<!-- PREHEADER -->
@include('shop/themes/'.Support::theme_path().'partials/menu_top')

<!-- HEADER -->
@include('shop/themes/'.Support::theme_path().'partials/header')

<!-- MIDDLE AREA -->
<div class="d-content JSmain relative"> 
	<div class="container"> 
		<div class="row">  
			
			@if($strana!='akcija' and $strana!='tip')
			<ul class="breadcrumb">
				@if($strana!='pretraga' and $strana!='filteri' and $strana!='proizvodjac')
				{{ Url_mod::breadcrumbs2()}}
				@elseif($strana == 'proizvodjac')
				<li>
					<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('brendovi') }}">{{ Language::trans('Brendovi') }}</a>  
					@if($grupa)
					<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('proizvodjac') }}/{{ Url_mod::slug_trans($proizvodjac) }}">{{ All::get_manofacture_name($proizvodjac_id) }}</a>
					{{ Groups::get_grupa_title($grupa) }}
					@else
					{{ All::get_manofacture_name($proizvodjac_id) }} 
					@endif
				</li>
				@else					
				<li><a href="{{ Options::base_url() }}">{{ All::get_title_page_start() }}</a></li>
				<li>{{$title}}</li>
				@endif
			</ul>
			@endif   

			<div class="col-xs-12 flex no-padding products-top-options">
				<h2> <span class="page-title"> {{ $title }} </span></h2>
				<div class="JSproduct-list-options row flex">
							
				    <div class="col-md-6 col-sm-6 col-xs-12 sm-text-center hidden">
						<!-- PER PAGE -->
						<span class="product-number"> {{ Language::trans('UKUPNO') }}:  {{ $count_products }}  </span>
				        @if(Options::product_number()==1)
						<div class="dropdown">	 
							 <button class="currency-btn dropdown-toggle" type="button" data-toggle="dropdown">	
							 	@if(Session::has('limit'))
								{{Session::get('limit')}}
								@else
								20
								@endif
				    			<span class="caret"></span>
				    		</button>
							<ul class="dropdown-menu currency-list">			
								<li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('limit') }}/20" rel="nofollow">20</a></li>
								<li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('limit') }}/30" rel="nofollow">30</a></li>
								<li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('limit') }}/50" rel="nofollow">50</a></li>			
							</ul>			 
						</div>
						@endif
					</div>

					<div class="col-xs-12 text-right sm-text-center xs-flex xs-space-between">

						<div class="JSopenFilters hidden-lg hidden-md hiddne-sm"> {{ Language::trans('Filteri') }} </div>
				        @if(Options::product_currency()==1)
				            <div class="dropdown inline-block hidden">
				            	 <button class="currency-btn dropdown-toggle" type="button" data-toggle="dropdown">
				            	 	 {{Articles::get_valuta()}} 
				            	 	 <span class="caret"></span>
				            	 </button>
				                 
				                <ul class="dropdown-menu currency-list">
				                	@foreach(DB::table('valuta')->where('ukljuceno',1)->orderBy('izabran','desc')->get() as $valuta)
				                    <li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('valuta') }}/{{ $valuta->valuta_id }}" rel="nofollow">{{ Language::trans($valuta->valuta_sl) }}</a></li>
				                    @endforeach
				                </ul>
				            </div>
				        @endif

				        @if(Options::compare()==1 AND isset($filter_prikazi) AND $filter_prikazi == 1)
							<!-- MODAL TRIGGER BUTTON -->
							<button type="button" id="JScompareArticles" class="valign {{ count(Session::get('compare_ids')) > 0 ? 'show-compered-active' : 'show-compered' }}" data-toggle="modal" data-target="#compared-articles">	
								<span class="hidden-xs"> {{ Language::trans('Upoređeni artikli') }} </span>
								<span class="hidden-lg hidden-md hidden-sm"> <span class="wish-compare-icons compare-icon inline-block valign"></span>  </span>
							</button>
						@endif
						
				        @if(Options::product_sort()==1)
				            <span class="hidden-xs">{{ Language::trans('Sortiraj po') }}: </span>
				            <div class="dropdown inline-block"> 
				            	<button class="currency-btn dropdown-toggle" type="button" data-toggle="dropdown">
					                 {{Articles::get_sort()}}
					                 <span class="caret"></span>
				            	</button>
				                <ul class="dropdown-menu currency-list">
				                	@if(Options::web_options(207) == 0)
				                    <li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('sortiranje') }}/price_asc" rel="nofollow">{{ Language::trans('Cena min') }}</a></li>
				                    <li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('sortiranje') }}/price_desc" rel="nofollow">{{ Language::trans('Cena max') }}</a></li>
				                    @else
				                    <li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('sortiranje') }}/price_desc" rel="nofollow">{{ Language::trans('Cena max') }}</a></li>
				                    <li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('sortiranje') }}/price_asc" rel="nofollow">{{ Language::trans('Cena min') }}</a></li>
				                    @endif
				                    <li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('sortiranje') }}/news" rel="nofollow">{{ Language::trans('Najnovije') }}</a></li>
				                    <li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('sortiranje') }}/name" rel="nofollow">{{ Language::trans('Prema nazivu') }}</a></li>
				                    <li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('sortiranje') }}/rbr" rel="nofollow">{{ Language::trans('Popularni') }}</a></li>
				                </ul>
				            </div>
				        @endif
					          
					    <!-- GRID LIST VIEW -->
				    	@if(Options::product_view()==1)
						@if(Session::has('list'))
						<div class="view-buttons inline-block"> 
							<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('prikaz') }}/list" rel="nofollow"><span class="fas fa-list"></span></a>
							<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('prikaz') }}/grid" rel="nofollow"><span class="fas fa-th active"></span></a>  		 
						 </div>
						@else
						<div class="view-buttons inline-block">  
							<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('prikaz') }}/list" rel="nofollow"><span class="fas fa-list active"></span></a>
							<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('prikaz') }}/grid" rel="nofollow"><span class="fas fa-th"></span></a>
						 </div>
						@endif
				        @endif  
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-12 col-xs-12">  

				<div class="JSfilter-mob hidden-xs">
					<div class="hidden-lg hidden-md hidden-sm flex xs-space-between">
						<h2> 
							<span class="page-title"> {{ Language::trans('Filteri') }} </span>
						</h2>
						<span class="bg-sprite sprite-wish close-filt inline-block valign"></span>
					</div>


					@if($strana!='pretraga' and $strana!='filteri' and $strana!='proizvodjac' and $strana != 'tagovi')
						@if(isset($sub_cats) and !empty($sub_cats))
						<div class="sub-group">

							@foreach($sub_cats as $row)
								
								<?php $sub_cats2 = Articles::subGroups($row->grupa_pr_id) ?>
								<a class="flex filters_groups_level_1" href="{{ Options::base_url()}}{{ $url }}/{{ Url_mod::slug_trans($row->grupa) }}">
									<span>{{ $row->grupa }}</span>
								</a> 

								@foreach($sub_cats2 as $row2)
									
									<?php $sub_cats3 = Articles::subGroups($row2->grupa_pr_id) ?>
									<a class="flex filters_groups_level_2" href="{{ Options::base_url()}}{{ $url }}/{{ Url_mod::slug_trans($row->grupa) }}">
										<span>{{ $row2->grupa }}</span>
									</a> 

									@foreach($sub_cats3 as $row3)
										
										<?php $sub_cats4 = Articles::subGroups($row3->grupa_pr_id) ?>
										<a class="flex filters_groups_level_3" href="{{ Options::base_url()}}{{ $url }}/{{ Url_mod::slug_trans($row->grupa) }}">
											<span>{{ $row3->grupa }}</span>
										</a> 

										@foreach($sub_cats4 as $row4)
											<a class="flex filters_groups_level_4" href="{{ Options::base_url()}}{{ $url }}/{{ Url_mod::slug_trans($row->grupa) }}">
												<span>{{ $row4->grupa }}</span>
											</a> 
										@endforeach
										
									@endforeach

								@endforeach

							@endforeach
						</div>
						@endif
					@endif

					<!-- FILTERI -->
					@if($filter_prikazi == 0)
						@if($strana == 'proizvodjac')
							@include('shop/themes/'.Support::theme_path().'partials/categories/manufacturer_category')
						@elseif($strana == 'tip')
							@include('shop/themes/'.Support::theme_path().'partials/categories/tip_category')
						@elseif($strana == 'akcija')
							@include('shop/themes/'.Support::theme_path().'partials/categories/akcija_category')
						@endif
					@endif

					@if(Options::enable_filters()==1 AND $filter_prikazi)
						@include('shop/themes/'.Support::theme_path().'partials/products/filters')
					@endif
					&nbsp; <!-- FOR EMPTY SPACE NBSP-->
				</div>
			</div>

			<!-- MAIN CONTENT -->
			<div class="col-md-9 col-sm-12 col-xs-12 product-page">
				@yield('products_list')
			</div>
		</div>
	</div>
</div>


<!-- FOOTER -->
@include('shop/themes/'.Support::theme_path().'partials/footer')


<!-- LOGIN POPUP -->
@include('shop/themes/'.Support::theme_path().'partials/popups')

<!-- BASE REFACTORING -->
<input type="hidden" id="base_url" value="{{Options::base_url()}}" />
<input type="hidden" id="in_stock" value="{{Options::vodjenje_lagera()}}" />
<input type="hidden" id="elasticsearch" value="{{ Options::gnrl_options(3055) }}" />

<!-- js includes -->
@if(Session::has('b2c_admin'.Options::server()) OR (Options::enable_filters()==1 AND $filter_prikazi))
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
@endif
@if(Session::has('b2c_admin'.Options::server()))
<script src="{{Options::domain()}}js/tinymce_5.1.3/tinymce.min.js"></script> 
<script src="{{ Options::domain()}}js/jquery-ui.min.js"></script>
<script src="{{Options::domain()}}js/shop/front_admin/main.js"></script>
<script src="{{Options::domain()}}js/shop/front_admin/products.js"></script>
<script src="{{Options::domain()}}js/shop/front_admin/product.js"></script>
@endif

<script src="{{Options::domain()}}js/shop/translator.js"></script>
<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}main.js"></script>
<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}main_function.js"></script>
<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}cart.js"></script>

@if(Options::header_type()==1)
<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}fixed_header.js"></script>
@endif
</body>
</html>
@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page') 

@foreach($sekcije as $sekcija)
	<div class="sec-wrapp" {{ !empty($sekcija->boja_pozadine) ? 'style="background-color:'.($sekcija->boja_pozadine).'"' : (($sekcija->puna_sirina == 1) ? 'style="background-color: #fff"' : '') }};">  

	@if($sekcija->tip_naziv == 'Text')
	<!-- TEXT SECTION -->
	<div class="{{ ($sekcija->puna_sirina == 1) ? 'container-fluid no-padding' : 'container' }}"> 
		<div class="row padding-v-20">
			<div class="col-xs-12">

				{{ Support::pageSectionContent($sekcija->sekcija_stranice_id) }}

			</div>
		</div>
	</div>

	@elseif($sekcija->tip_naziv == 'Lista artikala' AND !is_null($sekcija->tip_artikla_id))
	<!-- PRODUCTS SECTION -->
	<div class="{{ ($sekcija->puna_sirina == 1) ? 'container-fluid no-padding' : 'container' }}"> 
		<div class="row padding-v-20">
			<div class="col-xs-12 product-padding">

				@if($sekcija->tip_artikla_id == -1 AND count($latestAdded = Articles::latestAdded(20)) > 0)

				<h2><span class="section-title JSInlineShort" data-target='{"action":"page_section","id":"{{ $sekcija->sekcija_stranice_id }}"}'>{{Language::trans($sekcija->naziv)}}</span></h2>

				<div class="@if($strana == All::get_page_start()) JSproducts_slick hidden-xs @endif"> 
					@foreach($latestAdded as $row)
						@include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
					@endforeach
				</div> 

				<!-- FOR MOBILE ON START PAGE -->
				@if($strana == All::get_page_start())
					<div class="product-list hidden-lg hidden-md hidden-sm"> 
						@foreach(Articles::latestAdded(3) as $row)
							@include('shop/themes/'.Support::theme_path().'partials/products/product_on_list')
						@endforeach
					</div> 
				@endif

				@elseif($sekcija->tip_artikla_id == -2 AND count($mostPopularArticles = Articles::mostPopularArticles(20)) > 0)

				<h2><span class="section-title JSInlineShort" data-target='{"action":"page_section","id":"{{ $sekcija->sekcija_stranice_id }}"}'>{{Language::trans($sekcija->naziv)}}</span></h2>

				<div class="@if($strana == All::get_page_start()) JSproducts_slick hidden-xs @endif"> 
					@foreach($mostPopularArticles as $row)
						@include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
					@endforeach 
				</div> 

				<!-- FOR MOBILE ON START PAGE -->
				@if($strana == All::get_page_start())
					<div class="product-list hidden-lg hidden-md hidden-sm"> 
						@foreach(Articles::mostPopularArticles(3) as $row)
							@include('shop/themes/'.Support::theme_path().'partials/products/product_on_list')
						@endforeach
					</div> 
				@endif

				@elseif($sekcija->tip_artikla_id == -3 AND count($bestSeller = Articles::bestSeller()) > 0)

				<h2><span class="section-title JSInlineShort" data-target='{"action":"page_section","id":"{{ $sekcija->sekcija_stranice_id }}"}'>{{Language::trans($sekcija->naziv)}}</span></h2>

				<div class="@if($strana == All::get_page_start()) JSproducts_slick hidden-xs @endif"> 
					@foreach($bestSeller as $row)
						@include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
					@endforeach
				</div> 

				<!-- FOR MOBILE ON START PAGE -->
				@if($strana == All::get_page_start())
					<div class="product-list hidden-lg hidden-md hidden-sm"> 
						@foreach(Articles::bestSeller(3) as $row)
							@include('shop/themes/'.Support::theme_path().'partials/products/product_on_list')
						@endforeach
					</div> 
				@endif

				@elseif($sekcija->tip_artikla_id == 0 AND All::broj_akcije() > 0)

				<h2>
					<span class="section-title JSInlineShort" data-target='{"action":"page_section","id":"{{ $sekcija->sekcija_stranice_id }}"}'>{{Language::trans($sekcija->naziv)}}</span>

					@if($strana != All::get_page_start())
						<span class="section_sort">
						@if(Options::product_sort()==1)
				            <span class="hidden-xs">{{ Language::trans('Sortiraj po') }}: </span>
				            <div class="dropdown inline-block"> 
				            	<button class="currency-btn dropdown-toggle" type="button" data-toggle="dropdown">
					                 {{Articles::get_sort()}}
					                 <span class="caret"></span>
				            	</button>
				                <ul class="dropdown-menu currency-list">
				                	@if(Options::web_options(207) == 0)
				                    <li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('sortiranje') }}/price_asc" rel="nofollow">{{ Language::trans('Cena min') }}</a></li>
				                    <li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('sortiranje') }}/price_desc" rel="nofollow">{{ Language::trans('Cena max') }}</a></li>
				                    @else
				                    <li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('sortiranje') }}/price_desc" rel="nofollow">{{ Language::trans('Cena max') }}</a></li>
				                    <li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('sortiranje') }}/price_asc" rel="nofollow">{{ Language::trans('Cena min') }}</a></li>
				                    @endif
				                    <li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('sortiranje') }}/news" rel="nofollow">{{ Language::trans('Najnovije') }}</a></li>
				                    <li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('sortiranje') }}/name" rel="nofollow">{{ Language::trans('Prema nazivu') }}</a></li>
				                    <li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('sortiranje') }}/rbr" rel="nofollow">{{ Language::trans('Popularni') }}</a></li>
				                </ul>
				            </div>
				        @endif
				        </span>
			        @endif
				</h2>

				<div class="@if($strana == All::get_page_start()) JSproducts_slick hidden-xs @endif"> 
					
					@if($strana != All::get_page_start())
	 	  			<?php
	 	  				foreach($filteri as $filter) {
			                $filteri_tip = DB::table('sekcija_stranice')->select('tip_artikla_id')->where('sekcija_stranice_tip_id', 3)->where('sekcija_stranice_id', $filter->sekcija_stranice_id)->get();
	 	  				}
		 			?>
			 			@if(count($filteri_tip) > 0)
		 	  			<div class="row">
							<div class="col-md-3">

				 	  			<div class="filters">  

				 	  			<?php
				 	  				$groups = array();
				 	  				foreach($filteri as $filter) {
						                $filteri_tip = DB::table('sekcija_stranice')->select('tip_artikla_id')->where('sekcija_stranice_tip_id', 3)->where('sekcija_stranice_id', $filter->sekcija_stranice_id)->get();
						                foreach($filteri_tip as $filter_tip) {
						                	if($filter_tip->tip_artikla_id == 0) {
						                		$roba_grupe = Articles::akcija();
						                	} else {
						                    	$roba_grupe = DB::table('roba_tipovi')->select('roba_id')->where('tip_artikla_id', $filter_tip->tip_artikla_id)->groupBy('roba_id')->get(); 
						                	}
						                    foreach($roba_grupe as $roba_grupa) {
						                    	$grupe = DB::table('roba')->select('grupa_pr_id')->where('roba_id', $roba_grupa->roba_id)->groupBy('grupa_pr_id')->get();
						                       	foreach($grupe as $grupa) {
						                       		if(!in_array($grupa->grupa_pr_id, $groups, true)){
									                    $groups[] = $grupa->grupa_pr_id;
						                       		}

						                       	}
						                    }
						                    ?>
						                    	<ul class="JSlevel-1 valign text-left"> 
						                    		@foreach($groups as $group)
							                    		<?php $grupa_naziv = DB::table('grupa_pr')->where('grupa_pr_id', $group)->pluck('grupa') ?>
						                    			<li>
						                    				@if(Groups::grupa_level($group) > 1)
						                    					<?php $parent_group = DB::table('grupa_pr')->where('grupa_pr_id',$group)->pluck('parrent_grupa_pr_id'); ?>
									                    		<?php $parent_grupa_naziv = DB::table('grupa_pr')->where('grupa_pr_id', $parent_group)->pluck('grupa') ?>
						                    					@if(Groups::grupa_level($parent_group) > 1)
						                    						<?php $parent_group = DB::table('grupa_pr')->where('grupa_pr_id',$group)->pluck('parrent_grupa_pr_id'); ?>
						                    						<?php $parent_parent_group = DB::table('grupa_pr')->where('grupa_pr_id',$parent_group)->pluck('parrent_grupa_pr_id'); ?>
									                    			<?php $parent_grupa_naziv = DB::table('grupa_pr')->where('grupa_pr_id', $parent_group)->pluck('grupa') ?>
									                    			<?php $parent_parent_grupa_naziv = DB::table('grupa_pr')->where('grupa_pr_id', $parent_parent_group)->pluck('grupa') ?>
						                    						<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($parent_parent_grupa_naziv) }}/{{ Url_mod::slug_trans($parent_grupa_naziv) }}/{{ Url_mod::slug_trans($grupa_naziv) }}" class="text-bold">
																		{{ Language::trans($grupa_naziv)  }} 
																	</a>
																@else
																	<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($parent_grupa_naziv) }}/{{ Url_mod::slug_trans($grupa_naziv) }}" class="text-bold">
																		{{ Language::trans($grupa_naziv)  }} 
																	</a>
						                    					@endif
						                    				@else 
						                    					<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($grupa_naziv) }}" class="text-bold">
																	{{ Language::trans($grupa_naziv)  }} 
																</a>
						                    				@endif
						                    			</li>

						                    			@if(Groups::broj_cerki($group) >0)

						                    			@endif
					                    			@endforeach
						                    	</ul>
						                    <?php
						                }
						            }
						        ?>
								</div>

							</div>

							<div class="col-md-9">
								@foreach(Articles::akcija(null,50) as $row)
									@include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
								@endforeach
							</div>
						</div>
	                	@endif
	                @else
	                	@foreach(Articles::akcija(null,50) as $row)
							@include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
						@endforeach
	                @endif
				</div> 

				<!-- FOR MOBILE ON START PAGE -->
				@if($strana == All::get_page_start())
					<div class="product-list hidden-lg hidden-md hidden-sm"> 
						@foreach(Articles::akcija(null,3) as $row)
							@include('shop/themes/'.Support::theme_path().'partials/products/product_on_list')
						@endforeach
					</div> 
				@endif

				@elseif(All::provera_tipa($sekcija->tip_artikla_id)) 

				<h2><span class="section-title JSInlineShort" data-target='{"action":"page_section","id":"{{ $sekcija->sekcija_stranice_id }}"}'>{{Language::trans($sekcija->naziv)}}</span></h2> 

				<div class="@if($strana == All::get_page_start()) JSproducts_slick hidden-xs @endif"> 
					@foreach(Articles::artikli_tip($sekcija->tip_artikla_id,20) as $row)
						@include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
					@endforeach
				</div>   

				<!-- FOR MOBILE ON START PAGE -->
				@if($strana == All::get_page_start())
					<div class="product-list hidden-lg hidden-md hidden-sm"> 
						@foreach(Articles::artikli_tip($sekcija->tip_artikla_id,3) as $row)
							@include('shop/themes/'.Support::theme_path().'partials/products/product_on_list')
						@endforeach
					</div> 
				@endif

				@endif

			</div>
		</div>
	</div>


	@elseif($sekcija->tip_naziv == 'Lista vesti')
	<!-- BLOGS SECTION-->
	<div class="{{ ($sekcija->puna_sirina == 1) ? 'container-fluid no-padding' : 'container' }}"> 
		<div class="row padding-v-20">
			<div class="col-xs-12 product-padding">

				<h2><span class="section-title JSInlineShort" data-target='{"action":"page_section","id":"{{ $sekcija->sekcija_stranice_id }}"}'>{{Language::trans($sekcija->naziv)}}</span></h2>

				<div class="JSblog-slick">   
					@foreach(All::getShortListNews() as $row) 
					<div class="col-md-4">
						<div class="card-blog"> 
							@if(in_array(Support::fileExtension($row->slika),array('jpg','png','jpeg','gif')))

							<a class="bg-img center-block" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}" style="background-image: url('{{ $row->slika }}');"></a>

							@else

							<iframe src="{{ $row->slika }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>                   

							@endif

							<h3 class="blogs-title">
								<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}">{{ $row->naslov }}</a>
							</h3>

							<div class="blogs-date text-uppercase">
								{{ Support::date_convert($row->datum) }}
							</div>
						</div>
					</div>
					@endforeach 

				</div> 

			</div>
		</div>
	</div>

	@elseif($sekcija->tip_naziv == 'Slajder' AND !is_null($sekcija->slajder_id))

	<div class="JSsliderDeviceFlag {{ ($sekcija->puna_sirina == 1) ? 'container-fluid no-padding' : 'container' }}" data-device="{{ All::getSliderDeviceByDeviceId($sekcija->slajder_id) }}"> 

		@if($slajder = Slider::slajder($sekcija->slajder_id) AND count($slajderStavke = Slider::slajderStavke($slajder->slajder_id)) > 0)

			@if($slajder->slajder_tip_naziv == 'Slajder')

			<div class="JSmain-slider">

				@foreach($slajderStavke as $slajderStavka)
				
				<div class="relative">

					<a class="slider-link" href="{{ $slajderStavka->link }}"></a>

					<div class="bg-img" style="background-image: url( '{{ Options::domain() }}{{ $slajderStavka->image_path }}' )"></div>

					<div class="sliderText text-center"> 

						@if($slajderStavka->naslov != '') 
						<h2 class="main-desc JSInlineShort" data-target='{"action":"slide_title","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
							{{ $slajderStavka->naslov }}
						</h2> 
						@endif

						@if($slajderStavka->sadrzaj != '') 
						<div class="short-desc JSInlineFull" data-target='{"action":"slide_content","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
							{{ $slajderStavka->sadrzaj }}
						</div> 
						@endif

						@if($slajderStavka->naslov_dugme != '') 
						<a href="{{ $slajderStavka->link }}" class="slider-btn-link inline-block JSInlineShort" data-target='{"action":"slide_button","id":"{{$slajderStavka->slajder_stavka_id}}"}'>{{ $slajderStavka->naslov_dugme }}</a> 
						@endif
					</div>
				</div>
				@endforeach
			</div>

			@elseif($slajder->slajder_tip_naziv == 'Baner')
			<!-- BANNERS SECTION -->
			<div class="product-padding padding-v-20">
				<h2><span class="section-title JSInlineShort" data-target='{"action":"page_section","id":"{{ $sekcija->sekcija_stranice_id }}"}'>{{Language::trans($sekcija->naziv)}}</span></h2>

				<div class="@if($strana == All::get_page_start()) JSbanners_slick @endif row">
					@foreach($slajderStavke as $slajderStavka)
					<div class="relative banners col-md-3 col-sm-3 col-xs-12 no-padding"> 

						<a class="slider-link" href="{{ $slajderStavka->link }}"></a>

						<div class="bg-img" style="background-image: url('{{ Options::domain() }}{{ $slajderStavka->image_path }}'); ">

							<div class="banner-desc text-center">

								@if($slajderStavka->naslov != '') 
								<h2 class="JSInlineShort" data-target='{"action":"slide_title","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
									{{ $slajderStavka->naslov }}
								</h2> 
								@endif

								@if($slajderStavka->sadrzaj != '') 
								<div class="JSInlineFull" data-target='{"action":"slide_content","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
									{{ $slajderStavka->sadrzaj }}
								</div> 
								@endif

								@if($slajderStavka->naslov_dugme != '') 
								<a href="{{ $slajderStavka->link }}" class="slider-btn-link inline-block JSInlineShort" data-target='{"action":"slide_button","id":"{{$slajderStavka->slajder_stavka_id}}"}'>{{ $slajderStavka->naslov_dugme }}</a> 
								@endif
							</div> 
						</div> 
					</div>
					@endforeach	
				</div>
			</div>		

			@elseif($slajder->slajder_tip_naziv == 'Baner - tip')
			<!-- BANNERS SECTION -->
			<div class=" product-padding banner-type">
			<h2><span class="section-title JSInlineShort" data-target='{"action":"page_section","id":"{{ $sekcija->sekcija_stranice_id }}"}'>{{Language::trans($sekcija->naziv)}}</span></h2>

				<div class="@if($strana != All::get_page_start()) JSbanners_slick @else flex banners_tip_main @endif">
					@foreach($slajderStavke as $slajderStavka)
					<div class="relative banners"> 

						<a class="slider-link" href="{{ $slajderStavka->link }}"></a>

						<div class="bg-img" style="background-image: url('{{ Options::domain() }}{{ $slajderStavka->image_path }}'); ">

							<div class="banner-desc text-center">

								@if($slajderStavka->naslov != '') 
								<h2 class="JSInlineShort" data-target='{"action":"slide_title","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
									{{ $slajderStavka->naslov }}
								</h2> 
								@endif

								@if($slajderStavka->sadrzaj != '') 
								<div class="JSInlineFull" data-target='{"action":"slide_content","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
									{{ $slajderStavka->sadrzaj }}
								</div> 
								@endif

								@if($slajderStavka->naslov_dugme != '') 
								<a href="{{ $slajderStavka->link }}" class="slider-btn-link inline-block JSInlineShort" data-target='{"action":"slide_button","id":"{{$slajderStavka->slajder_stavka_id}}"}'>{{ $slajderStavka->naslov_dugme }}</a> 
								@endif
							</div> 
						</div> 
					</div>
					@endforeach	
				</div>
			</div>		

			<!-- BANER WITH ARROW FOR PAGE NA SNIZENJU -->
			@elseif($slajder->slajder_tip_naziv == 'Baner - sa strelicom')
			<!-- BANNERS SECTION -->
			<div class="product-padding padding-v-20">
				<!-- <h2><span class="section-title JSInlineShort" data-target='{"action":"page_section","id":"{{ $sekcija->sekcija_stranice_id }}"}'>{{Language::trans($sekcija->naziv)}}</span></h2> -->

				<div class="@if($strana == All::get_page_start()) JSbanners_slick @endif row">
					@foreach($slajderStavke as $slajderStavka)
					<div class="relative banners full-w-banner col-xs-12 no-padding"> 

						<a class="slider-link" href="{{ $slajderStavka->link }}"></a>

						<div class="bg-img" style="background-image: url('{{ Options::domain() }}{{ $slajderStavka->image_path }}'); ">
						</div> 

						<div class="text-center"> <i class="JSscroll-down fas fa-chevron-down pointer hidden"> </i> </div>
					</div>
					@endforeach	
				</div>
			</div>	

			@elseif($slajder->slajder_tip_naziv == 'Baner - mali baneri')
			<!-- BANNERS SECTION -->
			<div class="small-bann product-padding padding-v-20">
				<h2><span class="section-title JSInlineShort" data-target='{"action":"page_section","id":"{{ $sekcija->sekcija_stranice_id }}"}'>{{Language::trans($sekcija->naziv)}}</span></h2>

				<div class="JSsmall-bann_slick">
					@foreach($slajderStavke as $slajderStavka)
					<div class="relative banners col-md-3 col-sm-3 col-xs-12 no-padding"> 

						<a class="slider-link" href="{{ $slajderStavka->link }}"></a>

						<div class="bg-img" style="background-image: url('{{ Options::domain() }}{{ $slajderStavka->image_path }}'); ">
						</div> 
					</div>
					@endforeach	
				</div>
			</div>

			@elseif($slajder->slajder_tip_naziv == 'Tekst Baner (tekst levo)')
			<!-- TEXT BANNER LEFT SECTION -->
			@foreach($slajderStavke as $slajderStavka) 
				<div class="row flex padding-v-20 text-banner-section">

				<div class="col-md-6 col-sm-6 col-xs-12 text-center">
					<div class="flex text-flex-banner">   
						@if($slajderStavka->naslov != '') 
						<h2 class="JSInlineShort" data-target='{"action":"slide_title","id":"{{$slajderStavka->slajder_stavka_id}}"}'> 
							{{ $slajderStavka->naslov }}
						</h2> 
						@endif

						<div class="flex justify-center">
							<div>
								@if($slajderStavka->sadrzaj != '') 
								<div class="short-desc-txt-bann content" data-target='{"action":"slide_content","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
									{{ $slajderStavka->sadrzaj }}
								</div> 
								@endif

								@if($slajderStavka->naslov_dugme != '') 
								<a href="{{ $slajderStavka->link }}" class="slider-btn-link JSInlineShort inline-block @if($slajderStavka->sadrzaj == '') link-margin @endif " data-target='{"action":"slide_button","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
									{{ $slajderStavka->naslov_dugme }}
								</a> 
								@endif
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-6 col-sm-6 col-xs-12 txt-banner">
					<!-- <a href="{{ $slajderStavka->link }}" class="slider-link"></a>
					<div class="bg-img" style="background-image: url( '/{{ $slajderStavka->image_path }}' )"></div> -->
					<a href="{{ $slajderStavka->link }}" class="center-block">
						<img src="{{ $slajderStavka->image_path }}">
					</a>
				</div>
			</div>
			@endforeach

			@elseif($slajder->slajder_tip_naziv == 'Tekst Baner (tekst desno)')
			@foreach($slajderStavke as $slajderStavka) 
			<div class="row flex padding-v-20 text-bann-right text-banner-section">
				<div class="col-md-6 col-sm-6 col-xs-12 txt-banner">
					<!-- <a href="{{ $slajderStavka->link }}" class="slider-link"></a>
					<div class="bg-img" style="background-image: url( '/{{ $slajderStavka->image_path }}' )"></div> -->
					<a href="{{ $slajderStavka->link }}" class="center-block">
						<img src="{{ $slajderStavka->image_path }}">
					</a>
				</div>

				<div class="col-md-6 col-sm-6 col-xs-12 text-center">
					<div class="flex text-flex-banner">   
						@if($slajderStavka->naslov != '') 
						<h2 class="JSInlineShort" data-target='{"action":"slide_title","id":"{{$slajderStavka->slajder_stavka_id}}"}'> 
							{{ $slajderStavka->naslov }}
						</h2> 
						@endif

						<div class="flex justify-center">
							<div>
								@if($slajderStavka->sadrzaj != '') 
								<div class="short-desc-txt-bann content" data-target='{"action":"slide_content","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
									{{ $slajderStavka->sadrzaj }}
								</div> 
								@endif

								@if($slajderStavka->naslov_dugme != '') 
								<a href="{{ $slajderStavka->link }}" class="slider-btn-link JSInlineShort inline-block @if($slajderStavka->sadrzaj == '') link-margin @endif " data-target='{"action":"slide_button","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
									{{ $slajderStavka->naslov_dugme }}
								</a> 
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
			@endforeach

			
			@elseif($slajder->slajder_tip_naziv == 'Galerija Baner')
			<!-- GALLERY BANNER SECTION -->
			<div class="row padding-v-20">  
				<div class="col-xs-12"> 
					<div class="gallery-ban">
						@if(isset($slajderStavke[0]))  
						<div class="bg-img relative" style="background-image: url( '/{{ $slajderStavke[0]->image_path }}' )">
							<a href="{{ $slajderStavke[0]->link }}" class="slider-link"></a> 

							<h2 class="gallery-title"> 
								@if($slajderStavke[0]->naslov != '')
								{{ $slajderStavke[0]->naslov }}
								@endif
							</h2> 
						</div>
						@endif

						@foreach(array_slice($slajderStavke,1) as $position => $slajderStavka) 

						@if($position == 0 || $position == 3)   
						<div class="bg-img relative" style="background-image: url( '/{{ $slajderStavka->image_path }}' )"> 
							<a href="{{ $slajderStavka->link }}" class="slider-link"></a> 
							<h2 class="gallery-title"> 
								@if($slajderStavka->naslov != '')
								{{ $slajderStavka->naslov }}
								@endif
							</h2> 
						</div> 
						@endif

						@if($position == 1 || $position == 2)   
						<div class="bg-img relative" style="background-image: url( '/{{ $slajderStavka->image_path }}' )">
							<a href="{{ $slajderStavka->link }}" class="slider-link"></a>

							<h2 class="gallery-title"> 
								@if($slajderStavka->naslov != '')
								{{ $slajderStavka->naslov }}
								@endif
							</h2>  
						</div>
						@endif
						@endforeach 
					</div> 
				</div>
			</div>

			@elseif($slajder->slajder_tip_naziv == 'Galerija Slajder')
			<!-- GALLERY SLIDER SECTION -->
			<div class="row padding-v-20">  
				<div class="col-xs-12"> 
					<div class="main_imgGallery">
	                    <img class="JSmain_img img-responsive" src="{{ Options::domain() }}{{ $slajderStavke[0]->image_path }}" alt="" />
	                </div>
	                <!-- CUSTOM MODAL -->
	                <div class="JSmodal">
	                    <div class="flex full-screen relative"> 
	                      
	                        <div class="modal-cont galery-cont relative text-center"> 
	                      
	                            <div class="inline-block relative"> 
	                                <span class="JSclose-modal"><i class="fas fa-times"></i></span>
	                          
	                                <img src="" class="img-responsive" id="JSmodal_img" alt="modal image">
	                            </div>

	                            <button class="btn JSleft_btn"><i class="fas fa-chevron-left"></i></button>
	                            <button class="btn JSright_btn"><i class="fas fa-chevron-right"></i></button>
	                        </div>
	                
	                    </div>
	                </div>
	     
					<div class="gallery_slider"> 
						@foreach($slajderStavke as $slajderStavka) 
							<a class="flex JSimg-gallery relative" href="javascript:void(0)">
									
								<img class="img-responsive" src="{{ Options::domain().$slajderStavka->image_path }}" id="{{ Options::domain().$slajderStavka->image_path }}">

								<!-- @if($slajderStavka->naslov != '')
								<h2 class="gallery-title"> 
									{{ $slajderStavka->naslov }}
								</h2> 
								@endif -->

							</a> 
						@endforeach 
					</div>
				</div>
			</div>
			@endif 

		@endif

	</div>  <!-- CONTAINER -->

	<!-- GROUPS ON GRID -->
	@elseif($sekcija->tip_naziv == 'Lista kategorija')
	<div class="{{ ($sekcija->puna_sirina == 1) ? 'container-fluid no-padding' : 'container' }}"> 
		<div class="row padding-v-20">
			<div class="col-xs-12 product-padding">
				<h2>
					<span class="section-title relative JSInlineShort" data-target='{"action":"page_section","id":"{{ $sekcija->sekcija_stranice_id }}"}'>{{Language::trans($sekcija->naziv)}}
						<a class="groups-more hidden-lg hidden-md hidden-sm" href="{{ Options::base_url() }}{{ Language::trans('sve-kategorije') }}">
							<i class="fas fa-caret-right"></i>
						</a>
					</span>
				</h2>

				<div class="JSgroups_slick"> 
					@foreach (DB::select("select grupa_pr_id, grupa, putanja_slika from grupa_pr where parrent_grupa_pr_id = 0 and web_b2c_prikazi = 1") as $row2)
						@include('shop/themes/'.Support::theme_path().'partials/groups_on_grid')
					@endforeach
				</div>
			</div>
		</div>
	</div>

	@elseif($sekcija->tip_naziv == 'Newsletter' AND Options::newsletter()==1 AND !is_null($newslatter_description = All::newslatter_description()))
	 
	<div class="{{ ($sekcija->puna_sirina == 1) ? 'container-fluid no-padding' : 'container' }}">
		<div class="row padding-v-20 newsletter"> 

			<div class="col-md-6 col-sm-6 col-xs-12"> 
				<h5 class="JSInlineShort" data-target='{"action":"newslatter_label"}'>
					{{ $newslatter_description->naslov }}
				</h5> 

				<p class="JSInlineFull" data-target='{"action":"newslatter_content"}'>
					{{ $newslatter_description->sadrzaj }}
				</p>
			</div> 


			<div class="col-md-6 col-sm-6 col-xs-12">          
				<div class="relative"> 
					<input type="text" placeholder="E-mail" id="newsletter" /> 
					<button type="button" class="text-uppercase button" onclick="newsletter()">{{ Language::trans('Prijavi se') }}</button>
				</div>
			</div>

		</div> 
	</div>

	@endif

	</div> <!-- END BACKGROUND COLOR -->
@endforeach   


@if(isset($anketa_id) AND $anketa_id>0)
@include('shop/themes/'.Support::theme_path().'partials/poll')		
@endif

@if(Session::has('pollTextError'))
<script>
	$(document).ready(function(){     
		bootboxDialog({ message: "<p>{{ Session::get('pollTextError') }}</p>" }); 
	});
</script>
@endif

@endsection

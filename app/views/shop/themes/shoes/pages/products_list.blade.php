@extends('shop/themes/'.Support::theme_path().'templates/products')

@section('products_list')
<!-- PRODUCT LIST MOVED TO templates/products -->
@if(isset($grupa_pr_id)) 
  <input id="infinite_grupa_pr_id" type="hidden" data-grupa_pr_id="{{$grupa_pr_id}}">
@endif
<input id="infinite_strana" type="hidden" data-strana="{{$strana}}">
<?php
  $infinite_proizvodjac = Request::get('m');
  $infinite_karakteristike = Request::get('ch');
  $infinite_cene = Request::get('pr');
?>
<input id="infinite_proizvodjac" type="hidden" data-proizvodjac="{{isset($infinite_proizvodjac) ? $infinite_proizvodjac : ''}}">
<input id="infinite_karakteristike" type="hidden" data-karakteristike="{{isset($infinite_karakteristike) ? $infinite_karakteristike : ''}}">
<input id="infinite_cene" type="hidden" data-cene="{{isset($infinite_cene) ? $infinite_cene : ''}}">
<input id="infinite_count" type="hidden" data-count="{{isset($count_products) ? $count_products : ''}}">

<!-- 
<div class="text-center hidden">
  {{ Paginator::make($articles, $count_products, $limit)->links() }}
</div>
  -->
   <!-- MODAL FOR COMPARED ARTICLES -->
  <div class="modal fade" id="compared-articles" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
          <h4 class="modal-title text-center">{{ Language::trans('Upoređeni artikli') }}</h4>
        </div>
        <div class="modal-body">
      <div class="compare-section">
        <div id="compare-article">
          <div id="JScompareTable" class="compare-table text-center table-responsive"></div>
        </div>
      </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="button" data-dismiss="modal">{{ Language::trans('Zatvori') }}</button>
        </div>
      </div>    
    </div>
  </div>

  <!-- PRODUCTS -->
  @if(Session::has('list') or Options::product_view()==3)

  <!-- LIST PRODUCTS -->
    @foreach($articles as $row)
      @include('shop/themes/'.Support::theme_path().'partials/products/product_on_list') 
    @endforeach 
    <div class="text-center"> 
      {{ Paginator::make($articles, $count_products, $limit)->links() }}
    </div>
  @else
  <!-- Grid proucts -->
    @if(isset($grupa_pr_id))
      <!-- FOR LATEST ADDED ARTICLES BECAUSE OF HEIGHT ARTICLES FOR IFRAME ARTICLE HEIGHT-->
      <div class="size-div">
        @foreach(Articles::latestAdded(4) as $row)
          @include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
        @endforeach
      </div>

      <div class="first-frame">
        <iframe class="product_list_iframe" src="https://delovi.bgelektronik.rs/infinite-articles/{{$strana}}/{{$grupa_pr_id}}/?page=1{{isset($infinite_proizvodjac) ? '&m='.$infinite_proizvodjac : ''}}{{isset($infinite_karakteristike) ? '&ch='.$infinite_karakteristike : ''}}{{isset($infinite_cene) ? '&pr='.$infinite_cene : ''}}" frameborder="0" style="width: 100%; height: 280vh;"></iframe>
      </div>
    @else
      <div class="row">
      @foreach($articles as $row)
        @include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid') 
      @endforeach 
      </div>
    @endif
  @endif
<!-- PRODUCTS -->

@if(!isset($grupa_pr_id))
  @if($strana != 'akcija')
    @if($strana != 'tip')
    <div class="text-center"> 
      {{ Paginator::make($articles, $count_products, $limit)->links() }}
    </div>
    @endif
  @endif
@endif

<!-- <div class="text-center JSlimit-loader">
  <img src="images/quick_view_loader.gif" alt="">
</div> -->

@if($count_products == 0) 
  <br>
  <div class="empty-page-label"> 
     {{ Language::trans('Trenutno nema artikla za date kategorije') }} 
  </div> 
@endif

<!-- <div class="text-center"> 
  {{ Paginator::make($articles, $count_products, $limit)->links() }}
</div> -->

@endsection
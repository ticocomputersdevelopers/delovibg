@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
<div class="reg-login-form"> 
	<h2><span class="page-title">{{ Language::trans('Unesi podatke') }}</span></h2> 

	<div class="login-title text-center">
		@if(Input::old('flag_vrsta_kupca') == 1)
			<span class="inline-block pointer private-user">{{ Language::trans('fizičko lice') }}</span>
			<span class="inline-block pointer active-user company-user">{{ Language::trans('pravno lice') }}</span>
		@else
			<span class="inline-block pointer active-user private-user">{{ Language::trans('fizičko lice') }}</span>
			<span class="inline-block pointer company-user">{{ Language::trans('pravno lice') }}</span>
		@endif
	</div> 


	<form action="{{ Options::base_url() }}registracija-post" method="post" class="registration-form text-center" autocomplete="off"> 

		<input type="hidden" name="flag_vrsta_kupca" value="0"> 

		<?php 
			if(!is_null($web_kupac_id)) {
				$webKupac = All::getKupacInfo($web_kupac_id);
			};
		?>

		<div class="form-group">
			<!-- <label for="email">{{ Language::trans('E-mail') }}</label> -->
			<input id="email" class="form-control" autocomplete="off" name="email" type="text" value="{{ is_null($web_kupac_id) ? Input::old('email') ? Input::old('email') : '' : $webKupac->email }}" placeholder="{{ Language::trans('E-mail') }}">
			<div class="error red-dot-error">{{ $errors->first('email') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('email') : "" }}
			</div>
		</div>

		<div class="form-group JSshow_login relative">
			<!-- <label for="lozinka">{{ Language::trans('Lozinka') }}</label> -->
			<input id="lozinka" class="form-control JSpassword_login" name="lozinka" type="password" value="{{ htmlentities(Input::old('lozinka') ? Input::old('lozinka') : '') }}" placeholder="{{ Language::trans('Lozinka') }}">

			<input autocomplete="off" class="JSpassword_login2" type="text" value="">
            <a href="#" class="show-password"><span class="fas fa-eye-slash"></span></a>

			<div class="error red-dot-error">{{ $errors->first('lozinka') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('lozinka') : "" }}</div>
		</div>

		<div class="form-group">
			<!-- <label for="ime">{{ Language::trans('Ime') }}</label> -->
			<input id="ime" class="form-control" name="ime" type="text" value="{{ is_null($web_kupac_id) ? Input::old('ime') ? Input::old('ime') : '' : $webKupac->ime }}" placeholder="{{ Language::trans('Ime') }}">
			<div class="error red-dot-error">{{ $errors->first('ime') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('ime') : "" }}
			</div>
		</div>

		<div class="form-group">
			<input id="prezime" class="form-control" name="prezime" type="text" value="{{ is_null($web_kupac_id) ? Input::old('prezime') ? Input::old('prezime') : '' : $webKupac->prezime }}" placeholder="{{ Language::trans('Prezime') }}">
			<div class="error red-dot-error">{{ $errors->first('prezime') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('prezime') : "" }}</div>
		</div>

		<div class="form-group">
			<!-- <label for="naziv">{{ Language::trans('Naziv firme') }}:</label> -->
			<input id="naziv" class="form-control" name="naziv" type="text" value="{{ is_null($web_kupac_id) ? Input::old('email') ? Input::old('email') : '' : $webKupac->naziv }}" placeholder="{{ Language::trans('Naziv firme') }}">
			<div class="error red-dot-error">{{ $errors->first('naziv') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('naziv') : "" }}
			</div>
		</div>

		<div class="form-group">
			<!-- <label for="pib">{{ Language::trans('PIB') }}</label> -->
			<input id="pib" class="form-control" name="pib" type="text" value="{{ is_null($web_kupac_id) ? Input::old('pib') ? Input::old('pib') : '' : $webKupac->pib }}"placeholder="{{ Language::trans('PIB') }}">
			<div class="error red-dot-error">{{ $errors->first('pib') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('pib') : "" }}</div>
		</div>	

		<div class="form-group">
			<!-- <label for="maticni_br">{{ Language::trans('Matični broj') }}</label> -->
			<input id="maticni_br" class="form-control" name="maticni_br" type="text" value="{{ is_null($web_kupac_id) ? Input::old('maticni_br') ? Input::old('maticni_br') : '' : $webKupac->maticni_br }}" placeholder="{{ Language::trans('Matični broj') }}">
			<div class="error red-dot-error">{{ $errors->first('maticni_br') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('maticni_br') : "" }}</div>
		</div>	  

	<!-- 	<div class="form-group">
			<input id="datum" class="form-control" name="datum" type="text" value="{{ Input::old('datum') ? Input::old('datum') : '' }}" placeholder="{{ Language::trans('Dan rođenja') }}" >
			<div class="error red-dot-error">{{ $errors->first('datum') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('datum') : "" }}</div>
		</div> -->

		<div class="form-group">
			<!-- <label for="telefon">{{ Language::trans('Telefon') }}</label> -->
			<input id="telefon" class="form-control" name="telefon" type="text" value="{{ is_null($web_kupac_id) ? Input::old('telefon') ? Input::old('telefon') : '' : $webKupac->telefon }}" placeholder="{{ Language::trans('Telefon') }}">
			<div class="error red-dot-error">{{ $errors->first('telefon') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('telefon') : "" }}</div>
		</div>

		<div class="form-group">
			<!-- <label for="adresa">{{ Language::trans('Adresa') }}</label> -->
			<input id="adresa" class="form-control" name="adresa" type="text" value="{{ is_null($web_kupac_id) ? Input::old('adresa') ? Input::old('adresa') : '' : $webKupac->adresa }}" placeholder="{{ Language::trans('Ulica i broj') }}">
			<div class="error red-dot-error">{{ $errors->first('adresa') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('adresa') : "" }}</div>
		</div>

		<div class="form-group">
			<!-- <label for="mesto"><span class="red-dot"></span> {{ Language::trans('Mesto') }}/{{ Language::trans('Grad') }} </label> -->
			<input id="mesto" type="text" class="form-control" name="mesto" value="{{ is_null($web_kupac_id) ? Input::old('mesto') ? Input::old('mesto') : '' : $webKupac->mesto }}" placeholder="{{ Language::trans('Grad ili mesto') }}">
			<div class="error red-dot-error">{{ $errors->first('mesto') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('mesto') : "" }}</div>
		</div>

		<div class="form-group">  
			<div class="capcha text-center"> 
				{{ Captcha::img(5, 160, 50) }}<br>
				<span>{{ Language::trans('Unesite kod sa slike') }}</span>
				<input type="text" name="captcha-string" class="form-control" tabindex="10" autocomplete="off">
				<div class="error red-dot-error">{{ $errors->first('captcha') ? $errors->first('captcha') : "" }}</div>
			</div>
		</div>

		<div class="text-center">   
			<input type="checkbox" name="reg-newsletter"> - {{ Language::trans('Prijavi se na newsletter') }}
			<br><br>
		</div> 

		<div class="text-center">   
			<button type="submit" class="button text-uppercase">{{ Language::trans('Napravi nalog') }}</button>
		</div> 

		<br>
		<a href="{{ Options::base_url() }}prijava" class="log-link inline-block">{{ Language::trans('Već imaš nalog? Prijavi se') }}</a>
	</form>
</div>

@if(Session::get('message'))
<script>
	$(document).ready(function(){  

		swal(trans('Poslali smo Vam potvrdu o registraciji na e-mail koji ste uneli. Molimo Vas da potvrdite Vašu e-mail adresu klikom na link iz e-mail poruke'));
		
	});
</script>
@endif

@endsection 
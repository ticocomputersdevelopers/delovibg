@extends('shop/themes/'.Support::theme_path().'templates/article')

@section('article_details')

<div id="fb-root"></div> 
<script>(function(d, s, id) {
var js, fjs = d.getElementsByTagName(s)[0];
if (d.getElementById(id)) return;
js = d.createElement(s); js.id = id;
js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12';
fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

@if(Session::has('success_add_to_cart'))
    $(document).ready(function(){    
        alertSuccess("{{ Language::trans('Artikal je dodat u korpu') }}.");
    });
@endif
@if(Session::has('success_comment_message'))
$(document).ready(function(){
    alertSuccess("{{ Language::trans('Vaš komentar je poslat') }}.");
});
@endif
</script>
<div class="d-content JSmain relative"> 
    <div class="container">
   
        <ul class="breadcrumb"> 
            {{ Product::product_bredacrumps(DB::table('roba')->where('roba_id',$roba_id)->pluck('grupa_pr_id')) }}
        </ul>

        <div id="product-preview" class="clearfix">

            <div class="row xs-flex align-unset"> 
                
                <div class="product-preview-info col-md-6 col-sm-6 col-xs-12">

                    <!-- ADMIN BUTTON-->
                    @if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array('ARTIKLI')) AND Admin_model::check_admin(array('ARTIKLI_PREGLED')))
                        <div class="admin-article inline-block"> 
                            @if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
                                <a class="article-level-edit-btn JSFAProductModalCall" data-roba_id="{{$roba_id}}" href="javascript:void(0)" rel="nofollow">{{ Language::trans('IZMENI ARTIKAL') }}</a> 
                            @endif
                            <span class="supplier"> {{ Product::get_dobavljac($roba_id) }}</span> 
                            <span class="supplier">{{ Language::trans('NCP') }}: {{ Product::get_ncena($roba_id) }}</span>
                        </div>
                    @endif


                    <h1 class="article-heading">{{ Product::seo_title($roba_id) }}</h1>

                    <!-- <div class="rate-me-artical">
                        {{ Product::getRating($roba_id) }}
                    </div> -->

                    <!-- PRICE -->
                    <div class="product-preview-price">
                        @if(Product::getStatusArticlePrice($roba_id) == 1)  

                            @if(All::provera_akcija($roba_id))                                      
                               
                                @if(Product::get_mpcena($roba_id) != 0)
                                <!-- <div>
                                    <div class="price-label">{{Language::trans('MP cena')}}: </div>
                                    <div class="price-num">{{ Cart::cena(Product::get_mpcena($roba_id)) }}</div>
                                </div> -->
                                @endif 

                                <div>
                                    <div class="" >{{Language::trans('Stara cena')}}:</div>
                                    <div class="JSaction_price" data-akc_cena="{{Product::get_price($roba_id)}}">{{ Cart::cena(Product::get_price($roba_id,false,false,(!is_null(Input::old('kamata')) ? Input::old('kamata') : 0 ))) }}</div>
                                </div>

                                <div>
                                    <div class="price-label" >{{Language::trans('Akcija')}}:</div>
                                    <div class="JSaction_price price-num" data-akc_cena="{{Product::get_price($roba_id)}}">{{ Cart::cena(Product::get_price($roba_id,true,false,(!is_null(Input::old('kamata')) ? Input::old('kamata') : 0 ))) }}</div>
                                </div>

                                @if(Product::getPopust_akc($roba_id)>0)
                                 <!-- <div>
                                    <div class="price-label">{{Language::trans('Ušteda')}}:</div>
                                    <div class="price-num">{{ Cart::cena(Product::getPopust_akc($roba_id)) }}</div> 
                                </div> -->
                                @endif

                            @else

                                @if(Product::get_mpcena($roba_id) != 0)
                                    <!-- <div>
                                        <div class="price-label">{{Language::trans('MP cena')}}: </div>
                                        <div class="price-num">{{ Cart::cena(Product::get_mpcena($roba_id)) }}</div>
                                    </div> -->
                                @endif

                                <div>
                                    <div class="price-label text-uppercase">{{Language::trans('Cena')}}:</div>
                                    <div class="JSweb_price price-num" data-cena="{{Product::get_price($roba_id)}}">
                                       {{ Cart::cena(Product::get_price($roba_id,true,false,(!is_null(Input::old('kamata')) ? Input::old('kamata') : 0 ))) }}
                                    </div>
                                </div>

                                @if(Product::getPopust($roba_id)>0)
                                    @if(AdminOptions::web_options(132)==1)
                                        <!-- <div class="price-label">{{Language::trans('Ušteda')}}:</div>
                                        <div class="price-num">{{ Cart::cena(Product::getPopust($roba_id)) }}</div> -->
                                    @endif
                                @endif

                            @endif
                            <div class="pdv-info">*{{Language::trans('Cene su prikazane sa PDV-om')}}</div>
                        @endif 
                    </div>

                    <!-- COLOR HOLDER - SRODNI -->
                    <!-- <div class="relative">
                        <div class="flex article-color-scroll">
                            @foreach($srodni_artikli_ids as $srodni_artikl_id) 
                                <div class="color_type {{ DB::table('roba')->where('roba_id', $srodni_artikl_id)->pluck('flag_prikazi_u_cenovniku') == 1 ? '' : 'hidden' }}"> 
                                    <a class="flex full-height" href="{{ Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($srodni_artikl_id)) }}">

                                         <span class="color-circle" style="
                                            background: {{ !empty($boja = All::get_karak_color($srodni_artikl_id)) ? $boja : 'black' }};
                                            border: {{ (All::get_karak_color($srodni_artikl_id) == '#ffffff') ? '1px solid black' : '1px solid transparent';}} ;
                                        ">
                                            @if($srodni_artikl_id == $roba_id)
                                                <span class="checkmark inline-block " style="filter: {{ ((All::get_karak_color($srodni_artikl_id)) == '#ffffff') ? 'unset' : '';}} ;"></span>
                                            @endif
                                        </span>
                                    </a>
                                </div>        
                            @endforeach
                        </div>
                    </div> -->

                    <!-- <div class="related-custom JSproducts_slick row"> 
                    @foreach($srodni_artikli as $srodni_artikl)
                    <div class="JSproduct col-md-4 col-sm-4 col-xs-12">  
                        <div class="card row">  
                            <div class="col-xs-3 no-padding">
                                <div class="img-wrap"> 
                                    <a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($srodni_artikl->srodni_roba_id))}}">
                                        <img class="img-responsive" src="{{ Options::domain() }}{{ Product::web_slika_big($srodni_artikl->srodni_roba_id) }}" alt="{{ Product::seo_title($srodni_artikl->srodni_roba_id)}}" />
                                    </a>
                                </div>
                            </div>
                           
                            <div class="col-xs-9">
                                <a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($srodni_artikl->srodni_roba_id))}}">
                                    <h2 class="title">{{ Product::seo_title($srodni_artikl->srodni_roba_id) }}</h2>
                                </a>

                                <div>{{ Product::get_karakteristika_srodni($srodni_artikl->grupa_pr_vrednost_id) }}</div>

                                <div class="price"> 
                                    <span>{{ Cart::cena(Product::get_price($srodni_artikl->srodni_roba_id)) }}</span>
                                </div>  
                            </div>   
                        </div>
                    </div>
                    @endforeach 
                    </div> -->

                    <!-- ADD DO CART AREA -->
                    <div class="add-to-cart-area clearfix">     
                         @if(Product::getStatusArticle($roba_id) == 1)
                            @if(Cart::check_avaliable($roba_id) > 0)
                            <form method="POST" action="{{ Options::base_url() }}product-cart-add" id="JSAddCartForm" class="article-form"> 

                                @if(Product::check_osobine($roba_id))
                                   
                                    @foreach(Product::osobine_nazivi($roba_id) as $osobina_naziv_id)
                                        <div class="attributes text-bold">
                                           
                                            <div>{{ Product::find_osobina_naziv($osobina_naziv_id,'naziv') }}</div>
                                             
                                            @foreach(Product::osobine_vrednosti($roba_id,$osobina_naziv_id) as $osobina_vrednost_id)
                                            <label class="relative" style="background-color: {{ Product::find_osobina_vrednost($osobina_vrednost_id, 'boja_css') }}">

                                                <input type="radio" name="osobine{{ $osobina_naziv_id }}" value="{{ $osobina_vrednost_id }}" {{ Product::check_osobina_vrednost($roba_id,$osobina_naziv_id,$osobina_vrednost_id,Input::old('osobine'.$osobina_naziv_id)) }}>
                                            
                                                <!-- <span class="inline-block">{{ Product::osobina_vrednost_checked($osobina_naziv_id, $osobina_vrednost_id) }}</span> -->
                                                <span class="inline-block">{{ Product::find_osobina_vrednost($osobina_vrednost_id, 'vrednost') }}</span>
                                                    
                                            </label>
                                            @endforeach
                                     
                                        </div>
                                    @endforeach
                          
                                @endif

                                @if(AdminOptions::web_options(313)==1) 
                                <div class="num-rates"> 
                                    <div> 
                                        <div class="inline-block lorem-1">{{ Language::trans('Broj rata') }}</div>
                                    </div>
                                    <select class="JSinterest" name="kamata">
                                        {{ Product::broj_rata(Input::old('kamata')) }}
                                    </select>
                                </div>
                                @endif
                                
                                <!-- <div class="printer inline-block" title="{{ Language::trans('Štampaj') }}">  
                                    <a href="{{Options::base_url()}}stampanje/{{ $roba_id }}" target="_blank" rel="nofollow"><i class="fas fa-print"></i></a>
                                </div> -->
     
                                <input type="hidden" name="roba_id" value="{{ $roba_id }}">
                               
                               <!--  <div class="inline-block">&nbsp;{{Language::trans('Količina')}}&nbsp;</div> -->
                               
                                <input type="text" name="kolicina" class="hidden cart-amount" value="{{ Input::old('kolicina') ? Input::old('kolicina') : '1' }}">

                                <button type="submit" id="JSAddCartSubmit" class="button">{{Language::trans('Dodaj u korpu')}}</button>
                                 
                                <input type="hidden" name="projectId" value=""> 

                                <div>{{ $errors->first('kolicina') ? $errors->first('kolicina') : '' }}</div>  
                            </form>
                        @else
                            <button class="button not-available">{{Language::trans('Nije dostupno')}}</button>    
                            <button class="dodavanje not-available JSavailable_soon @if (!(Cart::check_avaliable($roba_id) > 0) AND (Product::find_flag_cene(Product::getStatusArticle($roba_id),'naziv') == 'Uskoro na stanju'))  @endif " data-roba-id="{{$roba_id}}" rel="nofollow">
                                {{Language::trans('Obavesti me kada bude dostupno')}}
                            </button>        
                        @endif

                        @else
                        <button class="button" data-roba-id="{{$roba_id}}">
                            {{ Product::find_flag_cene(Product::getStatusArticle($roba_id),'naziv') }}
                        </button>
                        <button class="dodavanje not-available JSavailable_soon @if (!(Cart::check_avaliable($roba_id) > 0) AND (Product::find_flag_cene(Product::getStatusArticle($roba_id),'naziv') == 'Uskoro na stanju'))  @endif " data-roba-id="{{$roba_id}}" rel="nofollow">
                                    {{Language::trans('Obavesti me kada bude dostupno')}}
                        </button>
                        @endif
                    </div>

                    <div class="product-manufacturer hidden">                             
                        @if($proizvodjac_id != -1)
                            @if( Product::slikabrenda($roba_id) != null )
                            <a class="article-brand-img inline-block valign" href="{{Options::base_url()}}{{Url_mod::slug_trans('proizvodjac')}}/{{Url_mod::slug_trans(Product::get_proizvodjac($roba_id)) }}">
                                <img src="{{ Options::domain() }}{{product::slikabrenda($roba_id) }}" alt="{{ product::get_proizvodjac($roba_id) }}" />
                            </a>
                            @else
                            <a class="artical-brand-text inline-block" href="{{Options::base_url()}}{{Url_mod::slug_trans('proizvodjac')}}/{{Url_mod::slug_trans(Product::get_proizvodjac($roba_id)) }}">{{ product::get_proizvodjac($roba_id) }}</a>
                            @endif                                   
                        @endif
                    </div>

                    <!-- ARTICLE TOP DESCRIPTION -->
                    <div class="JSdesc-sec-wrap relative">
                        <div class="JSdesc-section">
                            {{ Product::get_opis($roba_id) }}
                            <!-- {{ Product::get_karakteristike($roba_id) }} -->
                        </div>
                        
                        <div class="JSdesc-short"> <!-- SHORTER DESC FROM JS --> </div>

                        <span class="JSread-more pointer">{{ Language::trans('Vidi više') }}</span>
                    </div>

                    <div>
                        <!-- @if(Product::getStatusArticlePrice($roba_id) == 1)
                            @if(Product::pakovanje($roba_id))
                                <div class="article-desc flex align-unset">
                                    <span>{{Language::trans('Pakovanje')}}: </span>
                                    <span>{{ Product::ambalaza($roba_id) }}</span>
                                </div>
                            @endif   
                        @endif -->   
                        <!-- ARTICLE PASSWORD -->
                        <div class="article-desc flex align-unset">
                            <span> {{Language::trans('Šifra') }}: </span>

                            @if(AdminOptions::sifra_view_web()==1)
                            <span> {{Product::sifra($roba_id)}} </span>
                            @elseif(AdminOptions::sifra_view_web()==4)   
                            <span> {{Product::sifra_d($roba_id)}} </span>    
                            @elseif(AdminOptions::sifra_view_web()==3)         
                            <span> {{Product::sku($roba_id)}} </span>
                            @elseif(AdminOptions::sifra_view_web()==2)       
                            <span> {{Product::sifra_is($roba_id)}} </span>
                            @endif 
                        </div>
                        
                        <!-- ARTICLE GROUP -->
                        @if($grupa_pr_id != -1)
                        <div class="article-desc flex align-unset">
                            <span> {{Language::trans('Kategorija')}}: </span>
                            <span> {{ Product::get_grupa($roba_id) }} </span>
                        </div>
                        @endif

                        <!-- ARTICLE MANUFACTURER -->
                        @if($proizvodjac_id != -1)
                        <div class="article-desc flex align-unset">
                            <span> {{Language::trans('Proizvođač')}}:  </span>

                            <span>
                                @if(Support::checkBrand($roba_id))
                                <a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('proizvodjac')}}/{{ Url_mod::slug_trans(Product::get_proizvodjac($roba_id)) }}">
                                    {{Product::get_proizvodjac($roba_id)}}
                                </a>
                                @else
                                <span> {{Product::get_proizvodjac($roba_id)}} </span>
                                @endif
                            </span>
                        </div>
                        @endif 

                       <!--  @if(Options::vodjenje_lagera() == 1)
                            <li class="product-available-amount">{{Language::trans('Dostupna količina')}}: <span> {{Cart::check_avaliable($roba_id)}}</span></li>
                        @endif -->

                        <!-- ARTICLE MODEL -->
                        @if(Product::get_model($roba_id))
                            <div class="article-desc flex align-unset">
                                <span> {{Language::trans('Model')}}: </span>
                                <span> {{Product::get_model($roba_id)}} </span>
                            </div>
                        @endif

                        <!-- ARTICLE WEIGHT -->
                        @if(Options::checkTezina() == 1 AND Product::tezina_proizvoda($roba_id)>0)
                            <div class="article-desc flex align-unset product-available-amount">
                                <span> {{Language::trans('Težina artikla')}}: </span>
                                <span> {{Product::tezina_proizvoda($roba_id)/1000}} kg </span>
                            </div>
                        @endif
                        
                        <!--@if(Options::checkTags() == 1)
                            @if(Product::tags($roba_id) != '')
                            <li class="product-available-amount">
                                {{Language::trans('Tagovi')}}: {{ Product::tags($roba_id) }}
                            </li>
                            @else
                            <li class="product-available-amount">
                                {{Language::trans('Tagovi')}}: {{Language::trans('Nema tagova')}}
                            </li>
                            @endif
                        @endif -->
                    </div>

                    <!-- DECLARATION -->
                    <div>
                        <div class="text-bold margin-v-10"> 
                            {{Language::trans('Deklaracija')}}
                        </div>

                        @if(Product::get_barkod($roba_id))
                        <div class="article-desc flex align-unset">
                            <span> EAN: </span> 
                            <span> {{Product::get_barkod($roba_id)}} </span>
                        </div>
                        @endif

                        <div class="article-desc flex align-unset">
                            <span> {{Language::trans('Proizvođač')}}: </span> 
                            <span> {{Product::get_proizvodjac($roba_id)}} </span>
                        </div>

                        <?php $uvoznik = DB::table('roba')->where('roba_id', $roba_id)->pluck('naziv_displej'); ?>
                        <div class="article-desc flex align-unset">
                            <span> {{Language::trans('Uvoznik')}}: </span>
                            <span> {{ isset($uvoznik) ? $uvoznik != '' ? $uvoznik : 'BG Elektronik DOO Beograd' : 'BG Elektronik DOO Beograd' }}</span>
                        </div>

                         <div class="article-desc flex align-unset">
                            <span> {{Language::trans('Prava potrošača')}}: </span>
                            <span> {{Language::trans('Zagarantovana sva prava kupaca')}} <br> {{Language::trans('po osnovu zakona o zaštiti potrošača')}} </span>
                        </div>

                        <div class="article-desc flex align-unset">
                            <span class="JSinfoA1"> <!-- Content fromt JS --> </span>
                            <span class="JSinfoA2"> <!-- Content fromt JS --> </span>
                        </div> 

                        <div class="article-desc flex align-unset">
                            <span class="JSinfoB1"> <!-- Content fromt JS --> </span>
                            <span class="JSinfoB2"> <!-- Content fromt JS --> </span>
                        </div>

                        <div class="short_description hidden">{{ Product::get_short_opis($roba_id) }}</div>

                        @if(Product::get_garancija($roba_id)>0)
                            <div class="text-bold margin-v-10"> 
                                {{Language::trans('Garancija')}}
                            </div>

                            <div class="article-desc flex align-unset">
                                <span> {{Language::trans('Period reklamacije')}}: </span>
                                <span> {{Product::get_garancija($roba_id)}} {{Language::trans('meseci')}} </span>
                            </div>
                        @endif

                        <div class="article-desc flex align-unset">
                            <span class="JSinfoC1"> <!-- Content fromt JS --> </span>
                            <span class="JSinfoC2"> <!-- Content fromt JS --> </span>
                        </div>

                    </div>

                    <!-- Facebook button -->
                    <!-- <div class="facebook-btn-share flex"> 
                              <div class="soc-network inline-block"> 
                                 <div class="fb-like" data-href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($roba_id))}}" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
                              </div> 
                             <div class="soc-network"> 
                                <a href="https://twitter.com/share" class="twitter-share-button">Tweet</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                             </div>  
                        </div> -->
                    <!-- FEATURES -->
                 
                    @if(!empty(Product::get_labela($roba_id)))
                    <div class="custom-label inline-block relative">
                        <i class="fa fa-info-circle"></i>
                        {{Product::get_labela($roba_id)}} 
                    </div>
                    @endif 

                    <div class="product-tags">  
                        @if(Options::checkTags() == 1)
                            @if(Product::tags($roba_id) != '')
                                
                                <span>{{ Language::trans('Tagovi') }}:</span>
                           
                                {{ Product::tags($roba_id) }} 
                             
                            @endif
                        @endif    
                    </div> 

                </div>

                <div class="JSproduct-preview-image col-md-6 col-sm-6 col-xs-12 no-padding">
                    <div class="row flex align-unset"> 
                        <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12 disableZoomer hidden-xs md-order-1">  

                            <div class="JSproduct-image-wrapp flex relative">
                                <img class="JSmain_img img-responsive" src="{{ Options::domain() }}{{ $slika_big }}" alt="{{ Product::seo_title($roba_id)}}" />  
                                
                                <div class="wish_compare">
                                    @if(Cart::kupac_id() > 0)
                                    <button type="button" class="like-it JSadd-to-wish" data-roba_id="{{$roba_id}}" title="{{ Language::trans('Dodaj na listu želja') }}"><div class="wish-compare-icons"></div></button> 
                                    @else
                                    <button type="button" class="like-it JSnot_logged" data-roba_id="{{$roba_id}}" title="{{ Language::trans('Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima') }}"><div class="wish-compare-icons"></div></button> 
                                    @endif  
                                </div>
                                <!-- STICKER -->
                                <div class="product-sticker flex">
                                    @if( B2bArticle::stiker_levo($roba_id) != null )
                                        <a class="article-sticker-img">
                                            <img class="img-responsive margin-auto" src="{{ Options::domain() }}{{B2bArticle::stiker_levo($roba_id) }}"  />
                                        </a>
                                    @endif 
                                    
                                    @if( B2bArticle::stiker_desno($roba_id) != null )

                                           <a class="article-sticker-img">
                                            <img class="img-responsive margin-auto" src="{{ Options::domain() }}{{B2bArticle::stiker_desno($roba_id) }}"  />
                                        </a>
                                    @endif   
                                </div>

                                <!-- CUSTOM MODAL -->
                                <div class="JSmodal">
                                    <div class="flex full-screen relative"> 
                                      
                                        <div class="modal-cont relative text-center"> 
                                      
                                            <div class="inline-block relative"> 
                                                <span class="JSclose-modal"><i class="fas fa-times"></i></span>
                                          
                                                <img src="" class="img-responsive" id="JSmodal_img" alt="modal image">
                                            </div>

                                            <button class="btn JSleft_btn"><i class="fas fa-chevron-left"></i></button>
                                            <button class="btn JSright_btn"><i class="fas fa-chevron-right"></i></button>
                                        </div>
                                
                                    </div>
                                </div>
                            </div>
                        </div> 

                         <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 text-center img-gallery-wrapp no-padding md-order-2">
                            @foreach($slike as $image)
                            <div class="img-gallery">
                                <a href="javascript:void(0)" class="flex relative JSimg-gallery"> 
                                    <img alt="{{ Options::domain().$image->putanja }}" id="{{ Options::domain().$image->web_slika_id }}" src="{{ Options::domain().$image->putanja }}"/>
                                </a>
                            </div>
                            @endforeach
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 additional_img">    
                            @foreach($glavne_slike as $slika)
                            <a class="inline-block text-center" href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($roba_id))}}/{{$slika->web_slika_id}}"> 
                                <img src="{{ Options::domain().$slika->putanja }}" alt=" {{ Options::domain().$slika->putanja }}" class="img-responsive inline-block"> 
                            </a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
 
            <!-- PRODUCT PREVIEW TABS-->
            <div id="product_preview_tabs" class="product-preview-tabs row">
                <div class="col-md-12 sm-12 xs-12"> 
                    <!-- <ul class="nav nav-tabs tab-titles">
                        <li class="{{ !Session::has('contactError') ? 'active' : '' }}"><a data-toggle="tab" href="#description-tab" rel="nofollow">{{Language::trans('Specifikacija')}}</a></li>
                        <li><a data-toggle="tab" href="#service_desc-tab" rel="nofollow">{{Language::trans('Opis')}}</a></li>
                        <li><a data-toggle="tab" href="#technical-docs" rel="nofollow">{{Language::trans('Sadržaji')}}</a></li>
                        <li class="{{ Session::has('contactError') ? 'active' : '' }}"><a data-toggle="tab" href="#the-comments" rel="nofollow">{{Language::trans('Komentari')}}</a></li>
                    </ul> -->

                    <div class="text-bold margin-v-10 JSopen_comments pointer"> 
                        <span> {{ Language::trans('Komentari') }} </span>
                        <span class="fa fa-caret-down valign"></span>
                    </div>

                    <div class="tab-content"> 
                        <!-- DESCRIPTION -->
                        <div id="description-tab" class="hidden">
                            {{ Product::get_opis($roba_id) }} 
                            {{ Product::get_karakteristike($roba_id) }}
                        </div>
                        <!-- LOADBEE & FLIXMEDIA -->
                        <div id="service_desc-tab" class="hidden">
                            @if(Product::get_proizvodjac_name($roba_id)=='Bosch')
                            <script type="text/javascript"></script>

                            <div class="loadbeeTabContent" data-loadbee-apikey="{{Options::loadbee()}}" data-loadbee-gtin="{{Product::get_barkod($roba_id)}}" data-loadbee-locale="sr_RS"></div>

                            <script src="https://cdn.loadbee.com/js/loadbee_integration.js" async=""></script>
                            @endif
                            <div id="flix-minisite"></div>
                            <div id="flix-inpage"></div>
                                    <!-- fixmedia -->
                            <script type="text/javascript" src="//media.flixfacts.com/js/loader.js" data-flix-distributor="{{Options::flixmedia()}}" data-flix-language="rs" data-flix-brand="{{Product::get_proizvodjac_name($roba_id)}}" data-flix-mpn="" data-flix-ean="{{Product::get_barkod($roba_id)}}" data-flix-sku="" data-flix-button="flix-minisite" data-flix-inpage="flix-inpage" data-flix-button-image="" data-flix-price="" data-flix-fallback-language="en" async>                                    
                            </script>
                        </div>
                        <!-- TECHNICAL DOCS -->
                        <div id="technical-docs" class="hidden">
                            @if(Options::web_options(120))
                                @if(count($fajlovi) > 0)
                                <div>
                                  <!--   <h2 class="h2-margin">Dodatni fajlovi</h2> -->
                                    @foreach($fajlovi as $row)
                                    <div class="files-list-item">
                                        <a class="files-link" href="{{ $row->putanja != null ? Options::domain().$row->putanja : $row->putanja_etaz }}" target="_blank">
                                            <img src="{{ Options::domain() }}images/file-icon.png" alt="{{ $row->naziv }}">
                                            <div class="files-list-item">
                                                <div class="files-name">{{ Language::trans($row->naziv) }}</div> <!-- {{ Product::getExtension($row->vrsta_fajla_id) }} --> 
                                            </div>
                                        </a>
                                    </div>
                                    @endforeach
                                </div>
                                @endif
                             @endif
                        </div>
                        <!-- COMMENTS -->

                        <div id="the-comments" {{ Session::has('contactError') ? 'style=display:block' : '' }} >
                            <div class="row"> 
                            <?php $query_komentary=DB::table('web_b2c_komentari')->where(array('roba_id'=>$roba_id,'komentar_odobren'=>1));
                            if($query_komentary->count() > 0){?>
                                <div class="col-md-6 col-sm-12 col-xs-12 no-padding"> 
                                    <ul class="comments">
                                        <?php foreach($query_komentary->orderBy('web_b2c_komentar_id', 'DESC')->get() as $row)
                                        { ?>
                                        <li class="comment">
                                            <ul class="comment-content">
                                                <li class="comment-name">{{$row->ime_osobe}}</li>
                                                <li class="comment-date">{{$row->datum}}</li>
                                                <li class="comment-rating">{{Product::getRatingStars($row->ocena)}}</li>
                                                <li class="comment-text">{{ $row->pitanje }}</li>
                                            </ul>
                                            <!-- REPLIES -->
                                            @if($row->odgovoreno == 1)
                                            <ul class="replies">
                                                <li class="comment">
                                                    <ul class="comment-content">
                                                        <li class="comment-name">{{ Options::company_name() }}</li>
                                                        <li class="comment-text">{{ $row->odgovor }}</li>
                                                    </ul>
                                                 </li>
                                             </ul>
                                             @endif
                                        </li>
                                        <?php }?>
                                    </ul>
                                </div>

                                <?php }?>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding"> 
                                    <form method="POST" action="{{ Options::base_url() }}comment-add">
                                        <div class="form-group"> 
                                            <label>{{Language::trans('Vaše ime')}}</label>
                                            <input name="comment-name" type="text" value="{{ Input::old('comment-name') }}" {{ $errors->first('comment-name') ? 'style="border: 1px solid red;"' : '' }} />
                                        </div>

                                        <div class="form-group"> 

                                            <label for="JScomment_message">{{Language::trans('Komentar')}}</label>
                                            <textarea class="comment-message" name="comment-message" rows="5"  {{ $errors->first('comment-message') ? 'style="border: 1px solid red;"' : '' }}>{{ Input::old('comment-message') }}</textarea>
                                            <input type="hidden" value="{{ $roba_id }}" name="comment-roba_id" />
                                            <span class="review JSrev-star">
                                                <span>{{Language::trans('Ocena')}}:</span>
                                                <i id="JSstar1" class="far fa-star review-star" aria-hidden="true"></i>
                                                <i id="JSstar2" class="far fa-star review-star" aria-hidden="true"></i>
                                                <i id="JSstar3" class="far fa-star review-star" aria-hidden="true"></i>
                                                <i id="JSstar4" class="far fa-star review-star" aria-hidden="true"></i>
                                                <i id="JSstar5" class="far fa-star review-star" aria-hidden="true"></i>
                                                <input name="comment-review" id="JSreview-number" value="0" type="hidden"/>
                                            </span>
                                        </div>

                                        <div class="capcha text-center form-group"> 
                                            {{ Captcha::img(5, 160, 50) }}<br>
                                            <span>{{ Language::trans('Unesite kod sa slike') }}</span>
                                            <input type="text" name="captcha-string" tabindex="10" autocomplete="off" {{ $errors->first('captcha') ? 'style="border: 1px solid red;"' : '' }}>
                                        </div>
                                        <button class="pull-right button">{{Language::trans('Pošalji')}}</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- BRENDOVI SLAJDER -->
                    <!--  <div class="row">
                         <div class="col-md-12">
                            <div class="dragg JSBrandSlider"> 
                                   <?php //foreach(DB::table('proizvodjac')->where('brend_prikazi',1)->get() as $row){ ?>
                                <div class="col-md-12 col-sm-6 end sub_cats_item_brend">
                                    <a class="brand-link" href="{{Options::base_url()}}{{ Url_mod::slug_trans('proizvodjac') }}/<?php //echo $row->naziv; ?> ">
                                         <img src="{{ Options::domain() }}<?php //echo $row->slika; ?>" />
                                     </a>
                                </div>
                                <?php //} ?>
                            </div>
                         </div>
                      </div> -->
           
                    @if(Options::web_options(118))
                    <div class="product-padding"> 
                        <h2 class="slickTitle"><span class="section-title">{{Language::trans('Često kupljeni zajedno')}}</span></h2>
                        <div class="JSlinked_products row">
                             
                            @foreach($vezani_artikli as $vezani_artikl)
                                @if(Product::checkView($vezani_artikl->roba_id))
                                <div class="JSproduct col-md-3 col-sm-4 col-xs-12 no-padding">
                                    <div class="shop-product-card relative"> 
                                        <!-- PRODUCT IMAGE -->
                                        <div class="product-image-wrapper relative">

                                            <a class="flex" href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($vezani_artikl->vezani_roba_id))}}">
                                                <img class="product-image img-responsive JSlazy_load" src="{{Options::domain()}}images/quick_view_loader.gif" data-src="{{ Options::domain() }}{{ Product::web_slika($vezani_artikl->vezani_roba_id) }}" alt="{{ Product::seo_title($vezani_artikl->vezani_roba_id) }}" />
                                            </a> 
                                        </div>

                                        <div class="product-meta"> 

                                            <!-- <span class="review">{{ Product::getRating($vezani_artikl->roba_id) }}</span>  -->
                            
                                            <h2 class="product-name">
                                                <a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($vezani_artikl->vezani_roba_id))}}">{{ Product::short_title($vezani_artikl->vezani_roba_id) }}</a>
                                            </h2>
                                            <div class="price-holder hidden">
                                             {{ Cart::cena(Product::get_price_vezani($vezani_artikl->roba_id,$vezani_artikl->vezani_roba_id)) }}
                                            </div>    
                                            
                                            @if($vezani_artikl->flag_cena == 1)
                                                <div class="add-to-cart-container">
                                                    <!-- WISH LIST  --> 

                                                    <div class="wish_compare"> 
                                                        @if(Cart::kupac_id() > 0)
                                                        <!-- <button class="like-it JSadd-to-wish" data-roba_id="{{$vezani_artikl->vezani_roba_id}}" title="{{ Language::trans('Dodaj na listu želja') }}"><div class="wish-compare-icons"></div></button>  -->
                                                        @else
                                                        <!-- <button class="like-it JSnot_logged" data-roba_id="{{$vezani_artikl->vezani_roba_id}}" title="{{ Language::trans('Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima') }}"><div class="wish-compare-icons"></div></button>  -->
                                                        @endif      
                                                    </div>
                                                    
                                                    @if(Product::getStatusArticle($vezani_artikl->roba_id) == 1)
                                                        @if(Cart::check_avaliable($vezani_artikl->vezani_roba_id) > 0)
                                                            <button class="buy-btn JSadd-to-cart-similar" data-vezani_roba_id="{{ $vezani_artikl->vezani_roba_id }}" data-roba_id="{{ $vezani_artikl->roba_id }}">
                                                            {{Language::trans('U korpu')}}</button>
                                                            <!-- <input type="text" class="JSkolicina linked-articles-input like-it" value="1" onkeypress="validate(event)"> -->
                                                        @else  
                                                            <button class="not-available">{{Language::trans('Nije dostupno')}}</button>
                                                        @endif
                                                        @else
                                                            <button class="buy-btn">
                                                                {{ Product::find_flag_cene(Product::getStatusArticle($vezani_artikl->roba_id),'naziv') }}
                                                            </button>
                                                      @endif
                                                </div>
                                            @endif
                                        </div>
                    
                                        @if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
                                            <a class="article-edit-btn JSFAProductModalCall" data-roba_id="{{$vezani_artikl->vezani_roba_id}}" href="#">{{Language::trans('IZMENI ARTIKAL')}}</a>
                                         @endif
                                    </div>
                                </div>
                                @endif
                            @endforeach 
                        </div> 
                    </div>
                    @endif

                    @if(Options::compare()==1)
                        <!-- MODAL TRIGGER BUTTON -->
                        <button type="button" id="JScompareArticles" class="valign {{ Session::get('compare_ids') ? 'show-compered-active' : 'show-compered' }}" data-toggle="modal" data-target="#compared-articles">   
                            <span class="hidden-xs"> {{ Language::trans('Upoređeni artikli') }} </span>
                            <span class="hidden-lg hidden-md hidden-sm"> <span class="wish-compare-icons compare-icon inline-block valign"></span>  </span>
                        </button>
                    @endif
                    <!-- RELATED PRODUCTS --> 
                    <div class="product-padding">
                        <h2 class="slickTitle"><span class="section-title">{{Language::trans('Srodni proizvodi')}}</span></h2>
                        <div class="JSproducts_slick row">
                        @foreach(Product::get_related($roba_id, 12) as $row)
                            @include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
                        @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

 <!-- MODAL FOR COMPARED ARTICLES -->
<div class="modal fade" id="compared-articles" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
          <h4 class="modal-title text-center">{{ Language::trans('Upoređeni artikli') }}</h4>
        </div>
        <div class="modal-body">
      <div class="compare-section">
        <div id="compare-article">
          <div id="JScompareTable" class="compare-table text-center table-responsive"></div>
        </div>
      </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="button" data-dismiss="modal">{{ Language::trans('Zatvori') }}</button>
        </div>
      </div>    
    </div>
</div>

<script type="text/javascript">
    // IMAGES SLICK SLIDER 
    if ($(window).width() < 1200) {
        $('.img-gallery-wrapp').addClass('JSimages_slick_gall');
    }
</script>
@endsection
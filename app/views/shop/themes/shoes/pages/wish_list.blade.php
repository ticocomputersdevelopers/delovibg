@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
<!-- <div class="wish-page">
	<div class="container">


		@if(count(All::getWishIds($web_kupac->web_kupac_id)) > 0)
		
		<div class="row">

			<div class="col-md-12">
				<h2 class="title">
					{{ Language::trans('Lista želja') }}
				</h2>
			</div>
		
			<div class="col-md-12 col-sm-12 col-xs-12">
				@foreach(All::getWishIds($web_kupac->web_kupac_id) as $row)
					<div class="JSproduct"> 		
						<div class="shop-product-card">  

							@if(All::provera_akcija($row->roba_id))
								<div class="sale-label">
									{{ Language::trans('Akcija') }}
								</div>
							@endif
				
							<div class="product-image-div">
								<a class="product-image-wrapper" href="{{Options::base_url()}}{{Url_mod::convert_url('artikal')}}/{{Url_mod::url_convert(Product::seo_title($row->roba_id))}}">
									<img class="product-image img-responsive" src="{{ Options::domain() }}{{ Product::web_slika($row->roba_id) }}" alt="{{ Product::seo_title($row->roba_id) }}" />
								</a>
			

								<div class="button-div">


									@if(AdminSupport::getStatusArticle($row->roba_id) == 1)	
										@if(Cart::check_avaliable($row->roba_id) > 0)	 
										
									         <a class="buy-btn" href="{{Options::base_url()}}{{Url_mod::convert_url('artikal')}}/{{Url_mod::url_convert(Product::seo_title($row->roba_id))}}">
									        	{{ Language::trans('Detaljnije') }}	
									        </a>	

									        <button data-roba_id="{{$row->roba_id}}" title="Ukloni" class="remove-btn JSukloni">
									        	<i class="fas fa-times"></i>
									        </button> 			    
										@else  	
							                <div class="not-available" title="{{ Language::trans('Nije dostupno') }}"> 
							                	{{ Language::trans('Nije dostupno') }}
							                </div>

									        <button data-roba_id="{{$row->roba_id}}" title="Ukloni" class="remove-btn JSukloni">
									        	<i class="fas fa-times"></i>
									        </button>         	                            
										@endif

										@else  
											<div class="not-available">
												{{ AdminSupport::find_flag_cene(AdminSupport::getStatusArticle($row->roba_id),'naziv') }} 	
									 	 	</div>
					 	 
									 	 	<button data-roba_id="{{$row->roba_id}}" title="Ukloni" class="remove-btn JSukloni">
									 	 		<i class="fas fa-times"></i>
									 	 	</button>		 	
									  @endif 	

								</div>    
							</div> 

							<div class="product-name-div">
								<a class="product-name" href="{{Options::base_url()}}{{Url_mod::convert_url('artikal')}}/{{Url_mod::url_convert(Product::seo_title($row->roba_id))}}">
									<h2> 
										{{ Product::short_title($row->roba_id) }} 
									</h2>
								</a>
								
								<div class="price-holder"> 
						            <span class="product-price"> 
						            	{{ Cart::cena(Product::get_price($row->roba_id)) }} 
						            </span>

						           
					            </div>
					        </div>

						</div>
					</div>
				 @endforeach

		 	</div>
		</div>
		 

		@else
			<div class="row">
				<div class="col-md-12">
					<div class="empty-cart">
						<p class="title">
							{{ Language::trans('Lista želja je prazna') }}
						</p>

						<p class="desc">
							<strong>{{ Language::trans('Vaša lista želja je trenutno prazna') }}</strong> <br>
							{{ Language::trans('Ovo možete izmeniti odabirom nekog proizvoda') }} 
						</p>

						<div class="btn-div">
							<a class="orange-btn" href="/pocetna">Nastavite kupovinu <i class="fas fa-chevron-right"></i></a>
						</div>
					</div>
				</div>
			</div>
		@endif

	</div>
</div> -->
@endsection
@include('shop/themes/'.Support::theme_path().'partials/head')

<style type="text/css">
html {
    height: 100%;   
}   
.cross-blanco-card-a:after {
    content: unset !important;
}
.related-custom .card { 
    padding: 15px 0;
}
.related-custom .img-wrap img {
    max-height: 50px;
}
.related-custom .title { 
    white-space: nowrap;
    text-overflow: ellipsis; 
}
.related-custom .price {
    font-size: 16px;
}
.related-custom button {
    display: none !important;
}
/*- PRODUCT */
/*.product-timer {
    position: absolute;
    width: 100%;
    background: #fff;
    bottom: 0;
    left: 0; 
    table-layout: fixed;
}*/
.product-padding {
    padding-right: 5px; /*padding of product = 10px, total is 15px */
    padding-left: 5px;
}
.product-padding .section-title {
    margin: 0 10px;
    padding: 5px 0;
}
/*==================*/
.shop-product-card { 
    background: var(--product_bg); 
    transition: 0.2s all;
    -webkit-transition: 0.2s all;
    padding: 15px 10px 1%;
    overflow: hidden;
}
.shop-product-card .sale-label {
    background: #ff0000 !important;
    padding: 3px 11px;
    margin-bottom: 3px;
    font-size: 12px;
    font-weight: 700;
    border-radius: 5px;
}  
.shop-product-card .product-image-wrapper:after {
    content: '';
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    /* background: #F5F5F5; */
    z-index: 1;
}
.shop-product-card .product-image-wrapper { 
    border-radius: 20px;
    overflow: hidden;
} 
/*==================*/
.article_top_info {
    position: absolute; 
    top: 4px;
    left: 10px;
    z-index: 3; 
    width: calc(100% - 20px);
    pointer-events: none;
} 
#product-preview .product-sticker {
    position: absolute; 
    top: 5px;
    left: 20px;
    z-index: 1; 
    pointer-events: none;
    width: calc(100% - 40px);
}
.article-sticker-img { flex:1; }

.article-sticker-img img { 
    max-height: 65px;
    padding: 2px; 
}
/*=========*/
.buy-btn, .not-available { 
    padding: 10px;
    margin: 5px; 
    -webkit-transition: 0.3s all;
    transition: 0.3s all;
    color: var(--btn_color);
    background-color: var(--btn_bg); 
}
.not-available {
    opacity: 0.8;
}
.like-it, .printer a i { 
    display: none;
    transition: 0.3s all;
    -webkit-transition: 0.3s all; 
    width: 40px;
    height: 40px;
    line-height: 40px;
    -webkit-box-shadow: 0px 0px 0px 1px #DADADA inset, 0px 0px 0px 6px transparent;
    box-shadow: 0px 0px 0px 1px #DADADA inset, 0px 0px 0px 6px transparent;
    color: #999;  
}
.like-it:hover, .like-it.active, .printer a i:hover{
    display: none;
    color: var(--global_color);
    -webkit-box-shadow: 0px 0px 0px 1px var(--global_color) inset, 0px 0px 0px 0px var(--global_color);
    box-shadow: 0px 0px 0px 1px var(--global_color) inset, 0px 0px 0px 0px var(--global_color);
}
  
.product-meta {
    padding: 0 5px;
}
/******************/ 
.num-rates{
    margin: 5px 0 10px;
}
.num-rates .lorem-1 {
    font-size: 90%;
    color: #666;
    margin: 0 30px 0 0;
}
.num-rates select {
    font-family: Consolas, monospace;
}
.num-rates .select-wrapper{
    min-width: 130px;
    display: inline-block;
}
/******************/
   
.price-holder { 
    font-size: 150%;
    color: var(--product_price_color); 
}
 
.product-old-price {
    text-decoration: line-through;
    color: var(--product_old_price_color); 
    font-size: 70%;
}

/******************/
.product-name {  
    font-size: 100%;
    color: var(--product_title_color); 
    margin: 10px 0; 
} 
/******************/

.shop-product-card:hover .product-image-wrapper:after { 
    /* background: #efefef; */
}
/******************/
 
.shop-product-card .product-image {
    max-height: 100%;
    padding: 5px;
} 

/*COLORS*/
.color-circle, .color_type span {
    width: 20px;
    height: 20px;
    border-radius: 50%;
}
.color-scroll {
    max-width: 175px;
    height: 21px;
    margin: 0 0 10px 0;
    grid-gap: 11px;
}
.more-colors {
    position: absolute;
    right: 0;
    top: 0;
    z-index: 5;
}
.JScart-item .color-circle, .JScart-item .attributes .color, .JScart-item .color_type span {
    width: 12px;
    height: 12px;
}
.tootltip_all_color {
    position: absolute;
    bottom: calc(100% + 7px);
    right: -10px;
    z-index: 1;
    min-width: 90px;
    background: rgba(29, 29, 29, .8);
    color: #fff;
    padding: 3px;
    border-radius: 5px;
    font-size: 12px;
}
.tootltip_all_color:after {
    content: "";
    position: absolute;
    right: 11px;
    top: 100%;
    border-width: 7px;
    border-style: solid;
    border-color: rgba(29, 29, 29, .8) transparent transparent transparent;
}
/******************/
    
.buy-btn:hover { 
    background-color: var(--add_to_cart_btn_bg_hover);
}
 
/*========================*/
.generic_car ul, .generic_car_list ul {
    font-size: 13px; 
    color: #333;
    height: 55px; 
}
.generic_car_list ul{
    height: auto;
}
.generic_car ul li, .generic_car_list ul li{
    width: 48%; 
    padding: 0 5px;
} 
.shop-product-card .color_type {
    padding: 0;
}

@media screen and (min-width: 768px) {
    .more-colors:hover .tootltip_all_color { display: block; }

    .generated-features-list:last-child { border-bottom: none; }
    .generated-features-list{
        padding: 0 10px;
        display: table;
        width: 100%;
        border-bottom: 1px solid #ccc;
    }  
    .md-absolute {
        position: absolute;
        z-index: 3;
        right: 5%;
        top: 50%;
        transform: translateY(-50%);
        -webkit-transform: translateY(-50%);
    }
    .shop-product-card .product-image-wrapper { 
        padding-bottom: 144%; 
    } 
    .shop-product-card .product-image-wrapper > a {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        -webkit-transform: translate(-50%, -50%);
        width: 100%;
        height: 100%;
        z-index: 2;
    }
    .shop-product-card .add-to-cart-container { 
        -webkit-transition: 0.2s ease-in-out;
        transition: 0.2s ease-in-out; 
        position: absolute; 
        bottom: 0; 
        z-index: 2;
        width: 100%; 
        background: #fff;
        padding: 5px 0 0;
    } 
    /*=============*/
    .shop-product-card .product-name { 
        height: 35px; 
    } 
    .shop-product-card .product-name a {
        font-size: 16px;
        font-weight: 400;
    } 
    .shop-product-card .price-holder {
        height: 45px; 
        font-size: 24px;
        font-weight: 500;
        color: rgba(29, 29, 29, 0.46);
    }
    .shop-product-card .price-holder .product-old-price {
        color: rgba(29, 29, 29, 0.46);
    }
    /****************/
}
@media only screen and (max-width: 768px){
    .color-circle, .attributes .color, .color_type span {
        width: 13px;
        height: 13px;
    }
    .color-scroll {
        max-width: 115px;
        grid-gap: 6px;
    }
    .color-scroll { height: 14px; }

    .xs-no-padd { 
        padding-left: 0; 
        padding-right: 0; 
    }
    .more-colors { font-size: 12px; }

    .color-box {
        width: 30px;
        height: 30px;
    }
    .box.active .color-check {
        line-height: 30px;
    }
}
</style>
<script>
    // PRODUCTS SLICK SLIDER 


</script>

<div class="cross-blanco-plugin">

    <div class="JSblanco_slick row"> 
        <?php 
            $where = '';
            $query = "SELECT DISTINCT r.roba_id, rt.datum_dodavanja FROM roba r".Product::checkImage('join').Product::checkActiveGroup('join').Product::checkCharacteristics('join')." LEFT JOIN roba_tipovi rt ON r.roba_id = rt.roba_id WHERE r.flag_aktivan = 1".$where." AND r.flag_prikazi_u_cenovniku = 1 AND rt.tip_artikla_id = 3 ".Product::checkImage().Product::checkActiveGroup().Product::checkPrice().Product::checkDescription().Product::checkCharacteristics()." ORDER BY rt.datum_dodavanja DESC NULLS LAST, r.roba_id DESC"; // ORDER BY r.roba_id ASC

            $latest=DB::select($query);

         ?>
        @foreach($latest as $row)
            <?php $srodniIds = Product::srodniIds($row->roba_id); ?>
            <!-- PRODUCT ON GRID -->
            <div class="JSproduct cross-blanco-card col-md-4 col-sm-3 col-xs-6 no-padding"> 
                <div class="shop-product-card relative {{Cart::check_avaliable($row->roba_id) == 0 ? 'product-not-available' : ''}}"> 
                    <!-- ACTION LABEL AND STICKER -->
                    <div class="article_top_info">
                        <div class="row">
                            @if(Cart::check_avaliable($row->roba_id) == 0)
                            <div class="col-xs-12 no-padding text-right sale-wrap"> 
                                <div class="sale-label inline-block">
                                    {{ Language::trans('Uskoro!') }} 
                                </div>
                            </div>
                            @endif
                            @if(All::provera_akcija($row->roba_id))
                            <div class="col-xs-12 no-padding text-right sale-wrap"> 
                                <div class="sale-label inline-block text-uppercase">
                                    {{ Language::trans('Šok cena!') }}  <!-- {{ Product::getSale($row->roba_id) }}% --> 
                                </div>
                            </div>
                            @endif
                            @if(Articles::get_last_articles($row->roba_id))
                            <div class="col-xs-12 no-padding text-right sale-wrap"> 
                                <div class="sale-label inline-block">
                                    {{ Language::trans('Novo!') }} 
                                </div>
                            </div>
                            @endif

                            <div class="{{ All::provera_akcija($row->roba_id) ? 'col-xs-8' : 'col-xs-12' }} no-padding">     
                                <div class="product-sticker flex">
                                    @if( Product::stiker_levo($row->roba_id) != null )
                                        <a class="article-sticker-img">
                                            <img class="img-responsive margin-auto" src="{{ Options::domain() }}{{product::stiker_levo($row->roba_id) }}"  />
                                        </a>
                                    @endif 
                                    
                                    @if( Product::stiker_desno($row->roba_id) != null )
                                        <a class="article-sticker-img">
                                            <img class="img-responsive margin-auto" src="{{ Options::domain() }}{{product::stiker_desno($row->roba_id) }}"  />
                                        </a>
                                    @endif 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="product-image-wrapper relative clearfix">

                        @foreach($srodniIds as $key => $srodni_roba_id)
                        <a class="JSrelated_img flex {{ $key == 0 ? '' : 'hidden' }}" href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($row->roba_id))}}" data-roba_id="{{ $row->roba_id }}" data-srodni_id="{{ $srodni_roba_id }}" target="_blank">

                            <img class="img-responsive" src="{{ Options::domain() }}{{ Product::web_slika($srodni_roba_id) }}" alt="{{ Product::seo_title($srodni_roba_id) }}" />
                        </a>
                        @endforeach
                        
                        @if(Cart::kupac_id() > 0)
                        <button class="like-it JSadd-to-wish" data-roba_id="{{$row->roba_id}}" title="{{ Language::trans('Dodaj na listu želja') }}"><div class="like-it-heart"></div></button> 
                        @else
                        <button class="like-it JSnot_logged" data-roba_id="{{$row->roba_id}}" title="{{ Language::trans('Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima') }}"><div class="like-it-heart"></div></button> 
                        @endif  
                    </div>


                    <div class="product-meta">  
                        
                        <!-- <span class="review">{{ Product::getRating($row->roba_id) }}</span>  -->

                        <h2 class="product-name">
                            <a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($row->roba_id))}}" target="_blank">
                                {{ Product::short_title($row->roba_id) }}  
                            </a>
                        </h2>

                        <!-- COLOR HOLDER  -->
                   <!--      <div class="flex color-scroll">
                            @foreach($srodniIds as $srodni_id)
                                    <div class="JSrelated_color color_type {{ DB::table('roba')->where('roba_id', $srodni_id)->pluck('flag_prikazi_u_cenovniku') == 1 ? '' : 'hidden' }}">
                                        <span class="color-circle" style="
                                            background: {{ !empty($boja = All::get_karak_color($srodni_id)) ? $boja : 'transparent' }};
                                            border: {{ ((All::get_karak_color($srodni_id)) == '#ffffff') ? '1px solid black' : '1px solid transparent'}} ;
                                            display: {{ ((All::get_karak_color($srodni_id)) == '') ? 'none' : 'block'}} ;

                                        " data-roba_id="{{ $row->roba_id }}" data-srodni_id="{{ $srodni_id }}" title="{{ Language::trans('Kupi') }} {{ Product::seo_title($srodni_id) }}"></span>
                                    </div>        
                            @endforeach
                            <span class="more-colors"><a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($row->roba_id))}}"><span>[+] <span class="tootltip_all_color text-center hidden-xs"> {{ Language::trans('Vidi sve boje') }} </span> </span></a></span>

                        </div> -->

                        <div class="price-holder">
                        @if(Product::getStatusArticlePrice($row->roba_id))
                            <div> {{ Cart::cena(Product::get_price($row->roba_id)) }} </div>
                            @if(All::provera_akcija($row->roba_id))
                            <span class="product-old-price relative">{{ Cart::cena(Product::old_price($row->roba_id)) }}</span>
                            @endif     
                        @endif 
                        </div>  

            <!--            @if(AdminOptions::web_options(153)==1)
                        <div class="generic_car">
                            {{ Product::get_web_roba_karakteristike_short($row->roba_id) }}
                        </div>
                        @endif  -->

                        


                        <!-- BUY BTN -->
                        <!-- <div class="add-to-cart-container">   -->
                        <div class="buy-btn-holder hidden">


                            @if(Product::getStatusArticle($row->roba_id) == 1)  
                            @if(Cart::check_avaliable ($row->roba_id) > 0)

                                @if(!Product::check_osobine($row->roba_id)) 

                                <button class="buy-btn JSadd-to-cart" data-org_roba_id="{{$row->roba_id}}" data-roba_id="{{ $srodniIds[0] }}">{{ Language::trans('KUPI') }}</button>

                                @else

                                <a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($row->roba_id))}}" class="buy-btn" target="_blank">
                                    {{ Language::trans('Vidi artikal') }}
                                </a>     
                                @endif

                                @if(Options::compare()==1 AND isset($filter_prikazi) AND $filter_prikazi == 1)
                                <button class="like-it JScompare {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}" title="{{ Language::trans('Uporedi') }}">
                                    <i class="fas fa-exchange-alt" aria-hidden="true"></i>
                                </button>
                                @endif      

                                @else   

                                <button class="not-available">{{ Language::trans('Nije dostupno') }}</button>                    
                                @if(Options::compare()==1 AND isset($filter_prikazi) AND $filter_prikazi == 1)
                                <button class="like-it JScompare {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}" title="{{ Language::trans('Uporedi') }}">
                                    <i class="fas fa-exchange-alt" aria-hidden="true"></i>
                                </button>
                                @endif  

                            @endif

                            @else  
                                <button class="buy-btn">{{ Product::find_flag_cene(Product::getStatusArticle($row->roba_id),'naziv') }}</button>    

                                @if(Options::compare()==1 AND isset($filter_prikazi) AND $filter_prikazi == 1)
                                <button class="like-it JScompare {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}" title="{{ Language::trans('Uporedi') }}">
                                    <i class="fas fa-exchange-alt" aria-hidden="true"></i>
                                </button>
                                @endif   

                            @endif  
                        </div>
                    </div>

                    <!-- ADMIN BUTTON -->
                    @if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
                    <a class="article-edit-btn JSFAProductModalCall" data-roba_id="{{$row->roba_id}}" href="javascript:void(0)" rel="nofollow">{{ Language::trans('IZMENI ARTIKAL') }}</a>
                    @endif
                </div>
            </div>
        @endforeach
    </div> 
</div>


<script src="{{Options::domain()}}js/slick.min.js"></script>
<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}main.js"></script>
 

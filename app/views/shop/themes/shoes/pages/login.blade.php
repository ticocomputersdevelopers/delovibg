@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')

<div class="reg-login-form login-wrapper"> 

	<!-- SHOW ONLY ON LOGIN PART -->
	<h2 class="JSshow_login"><span class="page-title">{{ Language::trans('Prijavi se') }}</span></h2> 
   	<!-- END OF LOGIN PART -->

   	<div class="JSshow_forgot_psw hidden">
	   	<h2><span class="page-title">{{ Language::trans('Zaboravio/la si') }} <br> {{ Language::trans('lozinku') }}?</span></h2> 
	   	
	   	<p> {{Language::trans('Molimo te da uneseš svoju email adresu. Stići će ti link putem kog ćeš moći da napraviš novu lozinku.')}}</p>
	</div>

    <div class="registration-form">
        <div>
            <div class="form-group"> 
                <!-- <label for="JSemail_login">{{ Language::trans('E-mail') }}</label> -->
                <input class="form-control" id="JSemail_login" type="text" autocomplete="off" placeholder="{{ Language::trans('E-mail') }}">
            </div>

            <!-- SHOW ONLY ON LOGIN PART -->
            <div class="form-group JSshow_login relative">
                <!-- label for="JSpassword_login">{{ Language::trans('Lozinka') }}</label> -->
                <input class="form-control JSpassword_login" id="JSpassword_login" autocomplete="off" type="password" placeholder="{{ Language::trans('Lozinka') }}">
                <input autocomplete="off" class="JSpassword_login2" type="text" value="">
                <a href="#" class="show-password"><span class="fas fa-eye-slash"></span></a>
            </div>
            <!-- END OF ONLY ON LOGIN PART -->
        </div>


        <!-- SHOW ONLY ON LOGIN PART -->
        <div class="JSshow_login text-center">
            <button type="submit" id="login" onclick="user_login()" class="button text-uppercase">{{ Language::trans('Prijavi se') }}</button>
            
            <button class="JScall_forgot_pass log-link margin-v-10 no-padding">{{ Language::trans('Zaboravio/la si lozinku') }}?</button>

	        <div class="reg-with-soc text-center">
	            <a class="center-block" href="/google-authorization">
	                <button type="submit">
	                    <span class="goog-icon inline-block"> </span> 
	                    <span class="text-uppercase text-bold">{{ Language::trans('Prijavi se putem google-a') }}</span>
	                </button>
	            </a>
	            <a class="center-block" href="/fb-authorization">
	                <button type="submit">
	                    <i class="fab fa-facebook-f icon"> </i> 
	                    <span class="text-uppercase text-bold">{{ Language::trans('Prijavi se putem facebook-a') }}</span>
	                </button>
	            </a>
	        </div>

	         <a class="log-link inline-block" href="{{Options::base_url()}}{{ Url_mod::slug_trans('registracija') }}">{{Language::trans('Nemaš nalog? Napravi ga')}}</a>
        </div>
         <!-- END OF ONLY ON LOGIN PART -->

        <button type="submit" onclick="user_forgot_password()" class="JSshow_forgot_psw hidden button text-uppercase">{{ Language::trans('Pošalji') }}</button>

        <div class="field-group error-login JShidden-msg" id="JSForgotSuccess"><br>
            {{ Language::trans('Novu lozinku za logovanje dobili ste na navedenoj e-mail adresi') }}.
        </div> 
    </div>   
</div>

@endsection




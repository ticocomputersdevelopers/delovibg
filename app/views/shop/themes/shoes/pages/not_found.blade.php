@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page') 
	<br>
	<h2><span class="section-title">{{ Language::trans('Greška') }}</span></h2> 
    <div class="empty-page-label">{{ Language::trans('Žao nam je, stranica nije dostupna') }}</div>
@endsection
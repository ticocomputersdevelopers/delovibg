@include('shop/themes/'.Support::theme_path().'partials/head')
<!-- <div class="JScart_num JStest" value='34'></div> -->
<div class="cross-blanco-plugin product-page">

    <!-- <div class="JSblanco_slick row">  -->
	<div class="row"> 
		@foreach($newArtikli as $row)
			<?php $srodniIds = Product::srodniIds($row->roba_id);
			$offset_infinite = Language::segment_offset();
			$lang_infinite = Language::multi() ? Request::segment(1) : null;
			$grupa1_infinite = Request::segment(3+$offset_infinite);
            $filter_prikazi_infinite = count(Groups::deca($grupa1_infinite));
			?>
			<!-- PRODUCT ON GRID -->
			<div class="JSproduct cross-blanco-card col-md-4 col-sm-3 col-xs-6 no-padding"> 
				<div class="shop-product-card relative {{Cart::check_avaliable($row->roba_id) == 0 ? 'product-not-available' : ''}}"> 
					<!-- ACTION LABEL AND STICKER -->
					<div class="article_top_info">
						<div class="row">
							@if(Cart::check_avaliable($row->roba_id) == 0)
							<div class="col-xs-12 no-padding text-right sale-wrap"> 
								<div class="sale-label inline-block">
									{{ Language::trans('Uskoro!') }} 
								</div>
							</div>
							@endif
							@if(All::provera_akcija($row->roba_id))
							<div class="col-xs-12 no-padding text-right sale-wrap"> 
								<div class="sale-label inline-block text-uppercase">
									{{ Language::trans('Šok cena!') }}  <!-- {{ Product::getSale($row->roba_id) }}% --> 
								</div>
							</div>
							@endif
							@if(Articles::get_last_articles($row->roba_id))
							<div class="col-xs-12 no-padding text-right sale-wrap"> 
								<div class="sale-label inline-block">
									{{ Language::trans('Novo!') }} 
								</div>
							</div>
							@endif

							<div class="{{ All::provera_akcija($row->roba_id) ? 'col-xs-8' : 'col-xs-12' }} no-padding">     
								<div class="product-sticker flex">
								    @if( Product::stiker_levo($row->roba_id) != null )
							            <a class="article-sticker-img">
							                <img class="img-responsive margin-auto" src="{{ Options::domain() }}{{product::stiker_levo($row->roba_id) }}"  />
							            </a>
							        @endif 
							        
							        @if( Product::stiker_desno($row->roba_id) != null )
							            <a class="article-sticker-img">
							                <img class="img-responsive margin-auto" src="{{ Options::domain() }}{{product::stiker_desno($row->roba_id) }}"  />
							            </a>
							        @endif 
								</div>
							</div>
						</div>
					</div>
					<div class="product-image-wrapper relative clearfix">

						@foreach($srodniIds as $key => $srodni_roba_id)
						<a class="JSrelated_img flex {{ $key == 0 ? '' : 'hidden' }}" href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($row->roba_id))}}" data-roba_id="{{ $row->roba_id }}" data-srodni_id="{{ $srodni_roba_id }}" target="_blank">

							<img class="product-image img-responsive JSlazy_load" src="{{ Options::domain() }}{{ Product::web_slika($srodni_roba_id) }}" data-src="{{ Options::domain() }}{{ Product::web_slika($srodni_roba_id) }}" alt="{{ Product::seo_title($srodni_roba_id) }}" />
						</a>
						@endforeach
			 			
			 			@if(Cart::kupac_id() > 0)
						<button class="like-it JSadd-to-wish" data-roba_id="{{$row->roba_id}}" title="{{ Language::trans('Dodaj na listu želja') }}"><div class="like-it-heart"></div></button> 
						@else
					  	<button class="like-it JSnot_logged" data-roba_id="{{$row->roba_id}}" title="{{ Language::trans('Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima') }}"><div class="like-it-heart"></div></button> 
						@endif	
					</div>


					<div class="product-meta">  
						
						<!-- <span class="review">{{ Product::getRating($row->roba_id) }}</span>  -->

						<h2 class="product-name">
							<a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($row->roba_id))}}" target="_blank">
								{{ Product::short_title($row->roba_id) }}  
							</a>
						</h2>

						<!-- COLOR HOLDER  -->
						<!-- <div class="relative">
				            <div class="flex color-scroll">
								@foreach($srodniIds as $srodni_id)
				                    <div class="JSrelated_color color_type {{ DB::table('roba')->where('roba_id', $srodni_id)->pluck('flag_prikazi_u_cenovniku') == 1 ? '' : 'hidden' }}">
				                        <span class="color-circle" style="
				               				background: {{ !empty($boja = All::get_karak_color($srodni_id)) ? $boja : 'transparent' }};
				                            border: {{ ((All::get_karak_color($srodni_id)) == '#ffffff') ? '1px solid black' : '1px solid transparent'}} ;
				                            display: {{ ((All::get_karak_color($srodni_id)) == '') ? 'none' : 'block'}} ;

				               			" data-roba_id="{{ $row->roba_id }}" data-srodni_id="{{ $srodni_id }}" title="{{ Language::trans('Kupi') }} {{ Product::seo_title($srodni_id) }}"></span>
				                    </div>        
								@endforeach
				                <span class="more-colors"><a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($row->roba_id))}}"><span>[+] <span class="tootltip_all_color text-center hidden-xs"> {{ Language::trans('Vidi sve boje') }} </span> </span></a></span>

							</div>
						</div> -->

						<div class="price-holder">
						@if(Product::getStatusArticlePrice($row->roba_id))
							@if(All::provera_akcija($row->roba_id))
							<span class="product-old-price relative">{{ Cart::cena(Product::old_price($row->roba_id)) }}</span>
							@endif     
							<div> {{ Cart::cena(Product::get_price($row->roba_id)) }} </div>
						@endif 
						</div>	

			<!-- 			@if(AdminOptions::web_options(153)==1)
						<div class="generic_car">
							{{ Product::get_web_roba_karakteristike_short($row->roba_id) }}
						</div>
						@endif  -->

						<div class="wish_compare flex">
                            <!-- WISH-->
                            @if(Cart::kupac_id() > 0)
                                <button class="like-it JSadd-to-wish" data-roba_id="{{$row->roba_id}}" title="{{ Language::trans('Dodaj na listu želja') }}">
                                    <div class="wish-compare-icons"></div>
                                </button> 
                            @else
                                <button class="like-it JSnot_logged" data-roba_id="{{$row->roba_id}}" title="{{ Language::trans('Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima') }}">
                                    <div class="wish-compare-icons"></div>
                                </button> 
                            @endif  

                            <!-- COMPARE -->
                            @if(Product::getStatusArticle($row->roba_id) == 1  AND isset($filter_prikazi_infinite) AND $filter_prikazi_infinite == 0)  
                                @if(Cart::check_avaliable($row->roba_id) > 0)
                                    @if(Options::compare()==1)
                                    <button class="like-it JScompare {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}" title="{{ Language::trans('Uporedi') }}">
                                        <div class="wish-compare-icons compare-icon"></div>
                                    </button>
                                    @endif      
                                @else        
                                    @if(Options::compare()==1)
                                    <button class="like-it JScompare {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}" title="{{ Language::trans('Uporedi') }}">
                                        <div class="wish-compare-icons compare-icon"></div>
                                    </button>
                                    @endif  
                                @endif
                            @else   
                                @if(Options::compare()==1   AND isset($filter_prikazi_infinite) AND $filter_prikazi_infinite == 0)
                                <button class="like-it JScompare {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}" title="{{ Language::trans('Uporedi') }}">
                                    <div class="wish-compare-icons compare-icon"></div>
                                </button>
                                @endif   
                            @endif  
                        </div>


						<!-- BUY BTN -->
						<!-- <div class="add-to-cart-container">   -->
						<div class="buy-btn-holder">


							@if(Product::getStatusArticle($row->roba_id) == 1)	
							@if(Cart::check_avaliable ($row->roba_id) > 0)

								@if(!Product::check_osobine($row->roba_id)) 

								<button class="buy-btn JSadd-to-cart" data-org_roba_id="{{$row->roba_id}}" data-roba_id="{{ $srodniIds[0] }}">{{ Language::trans('U korpu') }}</button>

								@else

								<a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($row->roba_id))}}" class="buy-btn" target="_blank">
									{{ Language::trans('Vidi artikal') }}
								</a>	 
								@endif

								@if(Options::compare()==1 AND isset($filter_prikazi) AND $filter_prikazi == 1)
								<button class="like-it JScompare {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}" title="{{ Language::trans('Uporedi') }}">
									<i class="fas fa-exchange-alt" aria-hidden="true"></i>
								</button>
								@endif		

								@else  	

								<button class="not-available">{{ Language::trans('Nije dostupno') }}</button>	               	 
								@if(Options::compare()==1 AND isset($filter_prikazi) AND $filter_prikazi == 1)
								<button class="like-it JScompare {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}" title="{{ Language::trans('Uporedi') }}">
									<i class="fas fa-exchange-alt" aria-hidden="true"></i>
								</button>
								@endif	

							@endif

							@else  
								<button class="buy-btn">{{ Product::find_flag_cene(Product::getStatusArticle($row->roba_id),'naziv') }}</button>	

								@if(Options::compare()==1 AND isset($filter_prikazi) AND $filter_prikazi == 1)
								<button class="like-it JScompare {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}" title="{{ Language::trans('Uporedi') }}">
									<i class="fas fa-exchange-alt" aria-hidden="true"></i>
								</button>
								@endif	 

							@endif 	
						</div>
					</div>

					<!-- ADMIN BUTTON -->
					@if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
					<a class="article-edit-btn JSFAProductModalCall" data-roba_id="{{$row->roba_id}}" href="javascript:void(0)" rel="nofollow">{{ Language::trans('IZMENI ARTIKAL') }}</a>
					@endif
				</div>
			</div>
		@endforeach
	</div> 
</div>


<script src="{{Options::domain()}}js/slick.min.js"></script>

<script src="{{Options::domain()}}js/shop/translator.js"></script>
<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}main.js"></script>
<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}main_function.js"></script>
<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}cart.js"></script>
 

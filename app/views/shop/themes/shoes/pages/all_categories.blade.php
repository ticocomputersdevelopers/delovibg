@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
    <div class="category-listing">

    <h2><span class="page-title">{{ Language::trans('Kategorije') }}</span></h2> 
        <?php 
            $query_category_first=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>0,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); 
        ?>

        @foreach ($query_category_first as $row1)
            @if(Groups::broj_cerki($row1->grupa_pr_id) >0)
                <!-- ROW 1 -->
               <div>
                    <h2 id="{{ Language::trans($row1->grupa_pr_id)  }}" class="category-heading relative">
                        <a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}"> {{ Language::trans($row1->grupa) }} </a>
                       <!--  <span class="category__list__item__link__span">({{Articles::brojiArtikleRekurzivno($row1->grupa_pr_id)}})</span> -->
                        @if(Groups::broj_cerki($row1->grupa_pr_id) >0)
                            <span class="JSarrow-oppen-cat pointer"> <i class="fas fa-angle-right"></i> </span> 
                        @endif
                    </h2>

                    <?php $query_category_second=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row1->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc') ?>
                    <div class="clearfix all-lvl2-wrapp">
                        @foreach ($query_category_second->get() as $row2)

                            <!-- GROUPS ON GRID -->
                            <div class="groups-list pull-left">
                                <div class="groups-list-card">
                                    <div class="group-image-wrapper relative">
                                        <a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}" class="flex">
                                            <img class="img-responsive JSlazy_load" src="{{Options::domain()}}images/quick_view_loader.gif"
                                            data-src="{{ Options::domain() }}{{$row2->putanja_slika}}" alt="{{ $row2->grupa }}"/>
                                        </a>
                                    </div>
                                    <h2 class="groups-list-title">{{ Language::trans($row2->grupa) }} </h2>
                                    <!-- <span class="groups-list-count">{{Articles::brojiArtikleRekurzivno($row2->grupa_pr_id)}} {{ Language::trans('artikala') }}</span> -->
                                </div>
                            </div>

                        @endforeach
                    </div>
               </div> <!-- end .row -->
                <!-- END ROW 1 -->
            @else
                <div>
                    <h2 id="{{ Language::trans($row1->grupa_pr_id)  }}" class="category-heading relative">
                        <a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}"> {{ Language::trans($row1->grupa) }} </a>
                       <!--  <span class="category__list__item__link__span">({{Articles::brojiArtikleRekurzivno($row1->grupa_pr_id)}})</span> -->

                       @if(Groups::broj_cerki($row1->grupa_pr_id) >0)
                            <span class="JSarrow-oppen-cat pointer"> <i class="fas fa-angle-right"></i> </span> 
                        @endif
                    </h2>

                    <?php $query_category_second=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row1->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc') ?>

                    <div class="clearfix all-lvl2-wrapp">
                        @foreach ($query_category_second->get() as $row2)
                            <div class="groups-list pull-left">
                                <div class="groups-list-card">
                                    <div class="group-image-wrapper relative">
                                        <a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}" class="flex">
                                            <img class="img-responsive JSlazy_load" src="{{Options::domain()}}images/quick_view_loader.gif"
                                            data-src="{{ Options::domain() }}{{$row2->putanja_slika}}" alt="{{ $row2->grupa }}"/>
                                        </a>
                                    </div>
                                    <h2 class="groups-list-title">{{ Language::trans($row2->grupa) }}</h2>
                                    <!-- <span class="groups-list-count">{{Articles::brojiArtikleRekurzivno($row2->grupa_pr_id)}} {{ Language::trans('artikala') }}</span> -->
                                </div>
                            </div>
                        @endforeach
                    </div>

               </div> <!-- end .row -->                        

            @endif
        @endforeach 

    </div><!-- end #main-content -->

    <!-- MAIN CONTENT -->
    <!-- <div class="sticky-element col-md-3 col-sm-3 col-xs-3">  
        <ul class="JScategory-sidebar__list">
            <li class="JScategory-sidebar__list__toggler"></li>
            <li> 
                <ul class="scrollable"> 
                    @foreach ($query_category_first as $row1)
                    <li class="category-sidebar__list__item">
                        <a href="#{{ Language::trans($row1->grupa_pr_id) }}" class="category-sidebar__list__item__link">{{ Language::trans($row1->grupa)  }} 
                            <span class="category-sidebar__list__item__link__span">({{Articles::brojiArtikleRekurzivno($row1->grupa_pr_id)}})</span>
                        </a>
                    </li>
                    @endforeach 
                </ul>
            </li>
        </ul>

    </div> -->

@endsection
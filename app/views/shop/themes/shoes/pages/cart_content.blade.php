@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')

<div class="half-cart-width"> 

	<div class="xs-space-between">
		@if(Session::has('b2c_korpa') and Cart::broj_cart() >= 1 )

		<div class="flex xs-space-between">
			<h2>
				<span class="@if(Session::has('b2c_kupac') AND Options::web_options(318)==1) pb-0 @endif page-title">{{ Language::trans('Korpa') }}</span>
			</h2>

			@if(Session::has('b2c_kupac') AND Options::web_options(318)==1)
				<button class="visible-xs JSpromo-call"> <span class="text-uppercase">Unesi promo kod</span> </button>
			@endif
		</div>
		<!-- COUPON -->
		@if(Session::has('b2c_kupac') AND Options::web_options(318)==1)
			
			<div class="coupon-wrapp">
				<div class="coupon-holder flex">
					<div> 
						<input id="cartBasePrice" type="hidden" value="{{Cart::cart_ukupno()}}">
						<input id="vaucer_code" type="text" name="vaucer_code" placeholder="promo kod" value="{{ htmlentities(Input::old('vaucer_code') ? Input::old('vaucer_code') : '') }}">
						<div class="error red-dot-error">{{ Session::has('vaucer_error') ? Session::get('vaucer_error') : '' }}</div>
					</div>
					<button id="JSvoucherSend" class="line-h"><span class="text-uppercase">Ostvari popust</span></button>	
				</div>
			</div>
		@endif
	</div>

	@if(Session::has('failure-message'))
	<h4>{{ Session::get('failure-message') }}</h4>
	@endif

	<div class="cart-articles-wrapp">

		<div class="JSmob-cart-first-step">
			<ul class="cart-labels row flex">
				<li class="col-md-2 col-sm-2 col-xs-12 cart-count-wrapper text-bold no-padding"> 
					<span class="JScart-count"> {{ Cart::broj_cart() }} </span>
					<span class="JScart-title-count"> @if( Cart::broj_cart() > 1 ) {{ Language::trans('proizvoda') }} @else {{ Language::trans('proizvod') }} @endif </span>
				</li>
				<li class="col-md-3 col-sm-3 hidden-xs text-uppercase no-padding">{{ Language::trans('Naziv') }}</li>			 
				<!-- <li class="col-md-2 col-sm-2 hidden-xs text-uppercase no-padding">{{ Language::trans('Boja') }}</li> -->
				<li class="col-md-3 col-sm-3 hidden-xs text-uppercase no-padding">{{ Language::trans('Količina') }}</li>
				<li class="col-md-3 col-sm-3 hidden-xs text-uppercase no-padding">{{ Language::trans('Cena') }}</li> 	 
			</ul>
			@foreach(DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_id',Cart::korpa_id())->orderBy('web_b2c_korpa_stavka_id','asc')->get() as $row)
			<ul class="JScart-item row flex">	 

				<li class="col-md-2 col-sm-2 col-xs-3 no-padding">
					<div class="cart-wrapper-img flex relative"> 
						<img src="{{ Options::domain() }}{{Product::web_slika($row->roba_id) }}" alt="{{ Product::short_title($row->roba_id) }}" class="cart-image img-responsive" />
					</div>	
				</li>

				<li class="cart-name col-md-3 col-sm-3 col-xs-12 no-padding JS-xs-wrapp">
					<span> 
						<a class="inline-block" href="{{ Options::base_url() }}{{Url_mod::slug_trans('artikal')}}/{{ Url_mod::slugify(Product::seo_title($row->roba_id)) }}">
							{{ Product::short_title($row->roba_id) }}
						</a>
						{{Product::getOsobineStr($row->roba_id,$row->osobina_vrednost_ids)}}   
					</span>
				</li>

				<!-- COLOURS -->
				<!-- <li class="no-padding sm-order-2 JS-xs-wrapp JSwrap col-md-2 col-sm-2 col-xs-12 no-padding">
					<span class="sm-margin-unset {{ !empty($boja = All::get_karak_color($row->roba_id)) ? '' : 'hidden resp-override' }}">
						<div class="option-variant"> 
				            <div class="box inline-block {{ $row->roba_id ? 'active' : '' }}">                     
				                <div class="flex"> 
				                    <span class="color-circle inline-block" style="
				                    	background: {{ !empty($boja = All::get_karak_color($row->roba_id)) ? $boja : 'hide' }};
		                                border: {{ ((All::get_karak_color($row->roba_id)) == '#ffffff') ? '1px solid black' : '1px solid transparent';}} ;
			                    	"></span> &nbsp;
				                    <span >{{ !empty($colorName = Product::colorLabel($row->roba_id)) ? $colorName : '' }}</span>
				                </div>
				            </div> 
				        </div>
			        </span>
				</li> -->

				<li class="no-padding JS-xs-wrapp col-md-3 col-sm-3 col-xs-12">
					<div class="cart-add-amount clearfix text-center">
						<a data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}" class="JScart-less" href="javascript:void(0)" rel=”nofollow”><i class="fas fa-minus"></i></a>

						<input type="text" class="JScart-amount valign" value="{{ round($row->kolicina) }}" onkeypress="validate(event)" data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}">
						<a data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}" class="JScart-more" href="javascript:void(0)" rel=”nofollow”><i class="fas fa-plus"></i></a>
					</div>
				</li>

				<li class="no-padding JS-xs-wrapp cart-price col-md-3 col-sm-3 col-xs-12"><span>{{ Cart::cena($row->jm_cena) }}</span></li>

				<!-- <li class="cart-total-price col-md-2 col-sm-3 col-xs-12"><span class="JScart-item-price" data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}">{{ Cart::cena($row->jm_cena*$row->kolicina) }}</span></li> -->

				<li class="no-padding cart-remove col-md-1 col-sm-1 col-xs-1">
					<a href="javascript:void(0)" data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}" class="remove-item JSdelete_cart_item" rel=”nofollow”><span class="bg-sprite sprite-wish close-filt inline-block"></span></a>
				</li>			 
			</ul>
			@endforeach
		</div>

		<br class="JSmob-cart-first-step"/> 

		<!-- <button class="button" id="JSDeleteCart">{{ Language::trans('Isprazni korpu') }}</button> -->


		<div class="row cart-sum-wrapper">  

			<div class="col-xs-4 no-padding"> {{ Language::trans('Ukupno') }}: </div>
			<div class="cart-sum col-md-8 col-sm-7 col-xs-12 text-right">
				
				@if(($troskovi = Cart::troskovi())>0) 

				<div>
					<span class="sum-label text-right">{{ Language::trans('Cena artikala') }}: </span>
					<span class="JSdelivery_total_amount sum-amount">{{Cart::cena(Cart::cart_ukupno())}}</span>
				</div>

				<div class="JSDelivery {{(Cart::troskovi()>0) ? '' : 'hidden'}}">
					<span class="sum-label text-right">{{ Language::trans('Troškovi isporuke') }}: </span>
					<span class="JSexpenses sum-amount">{{ Cart::cena($troskovi) }}</span>
				</div>

				<div class="JSDelivery {{(Cart::troskovi()>0) ? '' : 'hidden'}}">
					<span class="sum-label text-right">{{ Language::trans('Ukupno') }}: </span>
					<span class="JStotal_amount JStotal_amount_weight sum-amount">{{Cart::cena(Cart::cart_ukupno()+$troskovi)}}</span>
				</div>
				
				<div class="JSDelivery {{(Cart::troskovi()>0) ? 'hidden' : ''}}">
					<span class="sum-label"><b>{{ Language::trans('+Besplatna dostava') }}</b></span>
				</div>
				
				<!-- <div>
					<span class="sum-label text-right">{{ Language::trans('Ukupno sa popustom') }}: </span>
					<span class="JStotal_amount JStotal_amount_weight sum-amount">{{Cart::get_discount_distributiveUser(Cart::cart_ukupno()+$troskovi)}}</span>
				</div> -->

				@else

				<div>
					<!-- @if(WebKupac::check_distributive_user(Session::get('b2c_kupac')))
						<div>
							<span class="sum-label text-right">{{ Language::trans('Cena artikala bez popusta') }}: </span>
							<span class="JSdelivery_total_amount sum-amount">{{Cart::cena(Cart::cart_ukupno())}}</span>
						</div>

						<div>
							<span>{{ Language::trans('Ukupno sa popustom') }}: </span>
							<span class="JSdelivery_total_amount sum-amount">{{ Cart::get_discount_distributiveUser(Cart::cart_ukupno()) }}</span>
						</div>

						<div class="JSfree_delivery text-right">
							<i class="fas fa-plus"></i> {{ Language::trans('Besplatna dostava') }} 
						</div>
					@endif -->
						<span class="JSdelivery_total_amount sum-amount">{{Cart::cena(Cart::cart_ukupno())}}</span>
						
						<div class="JSfree_delivery text-right">	
							<i class="fas fa-plus"></i> {{ Language::trans('Besplatna dostava') }} 
						</div> 
				</div>

				@endif
			</div> 
		</div>

		<div class="pdv-info text-right"> *{{ Language::trans('Cene su prikazane sa PDV-om') }} </div>

		@if(Session::has('b2c_kupac'))
 			@foreach(Cart::getActiveCoupons(Cart::korpa_id()) as $row)
				@foreach(Cart::getCoupons($row->vaucer_id) as $coupon)
					<div class="coupon-box row">
						<div class="col-md-5 col-sm-6 col-xs-12 col-md-offset-7 col-sm-offset-6 col-xs-offset-0 no-padding">
							<div class="margin-v-10">
								<div class="coupon-header flex space-between"><span>{{ Language::trans('Vaučer') }} (<span id="vaucer_broj">{{$coupon->vaucer_broj}})</span> </span> <i class="pointer JSremoveCoupon far fa-times-circle" coupon="{{$coupon->vaucer_broj}}"></i></div>
								<div class="coupon-info flex space-between">
									<div class="coupon-type">
										{{Cart::getCouponType($coupon->vaucer_tip_id)}}
										@if($coupon->grupa_pr_id) 
										{{ $coupon->vaucer_tip_id != 3 ? 'za artikle iz' : '' }} 
										{{ Language::trans('grupe') }} -
										<?php $vaucer_grupa = DB::table('grupa_pr')->where('grupa_pr_id', $coupon->grupa_pr_id)->pluck('grupa'); ?>
										{{ $vaucer_grupa }}
										@endif
									</div>
									<div class="coupon-amount">{{ $coupon->vaucer_tip_id != 3 ?  $coupon->vaucer_tip_id == 2 ? $coupon->iznos.'%' : '-'.Cart::cena($coupon->iznos) : '' }}</div>
								</div>
							</div>
						</div>
					</div>
				@endforeach
			@endforeach
		@endif

		<br class="JSmob-cart-first-step"/> 
		<div class="JSmob-cart-first-step button JScall-step-2 hidden-lg hidden-md hidden-sm text-uppercase text-center"> {{ Language::trans('Unesi podatke') }} </div>
	</div>

	<!-- CART ACTION BUTTONS -->
	<div class="cart-action-buttons" id="cart_form_scroll">
		@if(Session::has('b2c_korpa') and Cart::broj_cart() >= 1 )
			<a class="inline-block text-bold without-btn" href="{{Options::base_url()}}{{Url_mod::slug_trans('prijava')}}">
				<div>{{ Language::trans('Imaš nalog') }}?</div>
				<div> {{ Language::trans('Prijavi se') }}!</div>
			</a>

			@if(Options::neregistrovani_korisnici()==1)
				<div class="text-bold"> 
					<div class="without-btn">
					@if(Input::old('flag_vrsta_kupca') == 1)
						<div class="JScheck_user_type personal" data-vrsta="personal">
							{{ Language::trans('Fizičko lice') }} <i class="fas fa-check"></i> 
						</div>
				
						<div class="JScheck_user_type active none-personal" data-vrsta="non-personal">
							{{ Language::trans('Pravno lice') }} <i class="fas fa-check"></i>
						</div>
					@else

						<div class="JScheck_user_type active personal" data-vrsta="personal">
							{{ Language::trans('Fizičko lice') }} <i class="fas fa-check"></i> 
						</div>
			
						<div class="JScheck_user_type none-personal" data-vrsta="non-personal">
							{{ Language::trans('Pravno lice') }} <i class="fas fa-check"></i> 
						</div>
				
					@endif 
					</div>
				</div>
				<!-- <button class="button" id="JSRegToggle">{{ Language::trans('Kupi bez registracije') }}</button> -->
			@endif
			
		@endif
	</div>

	<br>

	<div id="JSRegToggleSec" class="JSmob-cart-sec-step"> 

		<form method="POST" action="{{ Options::base_url() }}order-create" id="JSOrderForm" class="without-reg-form">

			<input type="hidden" name="flag_vrsta_kupca" value="{{ Input::old('flag_vrsta_kupca') == 1 ? '1' : '0' }}">

			<div class="row flex align-unset">
				@if(Options::neregistrovani_korisnici()==1 AND !Session::has('b2c_kupac'))
				<div class="col-md-6 col-sm-12 col-xs-12 no-padding"> 
					<div class="row"> 

						<div class="col-md-6 col-sm-6 col-xs-12 no-padding">
							<div>
								<label class="big-label-cart JSwithout-reg-personal {{ Input::old('flag_vrsta_kupca') == 1 ? '' : 'active' }}">{{ Language::trans('Gde da dostavimo?') }}</label>

								<label class="big-label-cart JSwithout-reg-none-personal {{ Input::old('flag_vrsta_kupca') == 1 ? '' : 'inactive' }}">{{ Language::trans('Podaci za dostavu') }}</label>
							</div>

							<div class="form-group JSwithout-reg-none-personal {{ Input::old('flag_vrsta_kupca') == 1 ? 'active' : '' }}">
								<!-- <label for="without-reg-company">
									<span class="red-dot-error"><i class="fas fa-star-of-life"></i></span> 
									{{ Language::trans('Naziv Firme') }}:
								</label> -->
								<input id="without-reg-company" class="form-control" name="naziv" type="text" tabindex="1" value="{{ htmlentities(Input::old('naziv') ? Input::old('naziv') : '') }}" placeholder="{{ Language::trans('Naziv Firme') }}">
								<div class="error red-dot-error">{{ $errors->first('naziv') ? $errors->first('naziv') : "" }}</div>
							</div>

							<div class="form-group JSwithout-reg-none-personal {{ Input::old('flag_vrsta_kupca') == 1 ? 'active' : '' }}">
								<!-- <label for="without-reg-pib">{{ Language::trans('PIB') }}:</label> -->
								<input id="without-reg-pib" class="form-control" name="pib" type="text" tabindex="2" value="{{ htmlentities(Input::old('pib') ? Input::old('pib') : '') }}" placeholder="{{ Language::trans('PIB') }}">
								<div class="error red-dot-error">{{ $errors->first('pib') ? $errors->first('pib') : "" }}</div>
							</div>

							<div>
								<div class="form-group"> 
									<input id="without-reg-address" class="form-control" name="adresa" type="text" tabindex="5" value="{{ htmlentities(Input::old('adresa') ? Input::old('adresa') : '') }}" placeholder="{{ Language::trans('Adresa') }}">
									<div class="error red-dot-error">{{ $errors->first('adresa') ? $errors->first('adresa') : "" }}</div>
								</div>
							</div>

							<div class="form-group"> 
								<!-- <label for="without-reg-city">
									<span class="red-dot-error"><i class="fas fa-star-of-life"></i></span> 
									{{ Language::trans('Mesto') }}/{{ Language::trans('Grad') }}
								</label> -->				 
								<input type="text" class="form-control" name="mesto" tabindex="6" value="{{ htmlentities(Input::old('mesto') ? Input::old('mesto') : '') }}" placeholder="{{ Language::trans('Grad ili mesto') }}">
								<div class="error red-dot-error">{{ $errors->first('mesto') ? $errors->first('mesto') : "" }}</div>
							</div>

							<div class="form-group">
								<input type="text" name="postanski_broj" tabindex="6" placeholder="{{ Language::trans('Poštanski broj') }}" value="{{ htmlentities(Input::old('postanski_broj') ? Input::old('postanski_broj') : '') }}">
								<div class="error red-dot-error">{{ $errors->first('postanski_broj') ? $errors->first('postanski_broj') : "" }}</div>
							</div>

					
							<div class="form-group JSwithout-reg-personal {{ Input::old('flag_vrsta_kupca') == 1 ? '' : 'active' }}">
								<!-- <label>{{ Language::trans('Napomena') }}:</label> -->
								@if(Cart::kamata(Cart::korpa_id()) >0 )					
								<textarea class="form-control" rows="5" tabindex="9" name="napomena" placeholder="{{ Language::trans('Napomena') }}">{{ Language::trans('Kupovina na:') }} {{ Cart::kamata(Cart::korpa_id()) }} {{ Language::trans('rata') }}</textarea>
								@else
								<textarea class="form-control" rows="5" tabindex="9" name="napomena" placeholder="{{ Language::trans('Napomena') }}">{{ htmlentities(Input::old('napomena') ? Input::old('napomena') : '') }}</textarea>
								@endif
							</div>
				
						</div>

						
						<div class="col-md-6 col-sm-6 col-xs-12 no-padding">
							<div>
								<label class="big-label-cart JSwithout-reg-personal {{ Input::old('flag_vrsta_kupca') == 1 ? '' : 'active' }}">{{ Language::trans('Kako da te kontaktiramo?') }}</label>

								<label class="hidden-xs big-label-cart JSwithout-reg-none-personal {{ Input::old('flag_vrsta_kupca') == 1 ? '' : 'inactive' }}">&nbsp;</label>
							</div>

								<!-- <div class="form-group JSwithout-reg-none-personal {{ Input::old('flag_vrsta_kupca') == 1 ? 'active' : '' }}">
								<label for="without-reg-maticni_br">{{ Language::trans('Matični broj') }}:</label>
								<input id="without-reg-maticni_br" class="form-control" name="maticni_br" type="text" tabindex="2" value="{{ htmlentities(Input::old('maticni_br') ? Input::old('maticni_br') : '') }}">
								<div class="error red-dot-error">{{ $errors->first('maticni_br') ? $errors->first('maticni_br') : "" }}</div>
							</div> -->
							
							
							<div class="form-group JSwithout-reg-personal {{ Input::old('flag_vrsta_kupca') == 1 ? '' : 'active' }}">
								<!-- <label for="without-reg-name">
									<span class="red-dot-error"><i class="fas fa-star-of-life"></i></span> 
									{{ Language::trans('Ime') }}
								</label> -->
								<input id="without-reg-name" class="form-control" name="ime" type="text" tabindex="1" value="{{ htmlentities(Input::old('ime') ? Input::old('ime') : '') }}" placeholder="{{ Language::trans('Ime') }}">
								<div class="error red-dot-error">{{ $errors->first('ime') ? $errors->first('ime') : "" }}</div>
							</div>

							<div class="form-group JSwithout-reg-personal {{ Input::old('flag_vrsta_kupca') == 1 ? '' : 'active' }}">
								<!-- <label for="without-reg-surname">
									<span class="red-dot-error"><i class="fas fa-star-of-life"></i></span> 
									{{ Language::trans('Prezime') }}
								</label> -->
								<input id="without-reg-surname" class="form-control" name="prezime" type="text" tabindex="2" value="{{ htmlentities(Input::old('prezime') ? Input::old('prezime') : '') }}" placeholder="{{ Language::trans('Prezime') }}">
								<div class="error red-dot-error">{{ $errors->first('prezime') ? $errors->first('prezime') : "" }}</div>
							</div>
							
							<div class="form-group"> 
								<input id="without-reg-phone" class="form-control" name="telefon" type="text" tabindex="3" value="{{ htmlentities(Input::old('telefon') ? Input::old('telefon') : '') }}" placeholder="{{ Language::trans('Telefon') }}">
								<div class="error red-dot-error">{{ $errors->first('telefon') ? $errors->first('telefon') : "" }}</div>
							</div>

							<div class="form-group"> 
								<input id="JSwithout-reg-email" class="form-control" name="email" type="text" tabindex="4" value="{{ htmlentities(Input::old('email') ? Input::old('email') : '') }}" placeholder="{{ Language::trans('E-mail') }}">
								<div class="error red-dot-error">{{ $errors->first('email') ? $errors->first('email') : "" }}</div>
							</div>

							
							<div class="form-group JSwithout-reg-none-personal {{ Input::old('flag_vrsta_kupca') == 1 ? '' : 'inactive' }}"> 
							 
								<!-- <label>{{ Language::trans('Napomena') }}:</label> -->
								@if(Cart::kamata(Cart::korpa_id()) >0 )					
								<textarea class="form-control" rows="5" tabindex="9" name="napomena" placeholder="{{ Language::trans('Napomena') }}">{{ Language::trans('Kupovina na:') }} {{ Cart::kamata(Cart::korpa_id()) }} {{ Language::trans('rata') }}</textarea>
								@else
								<textarea class="form-control" rows="5" tabindex="9" name="napomena" placeholder="{{ Language::trans('Napomena') }}">{{ htmlentities(Input::old('napomena') ? Input::old('napomena') : '') }}</textarea>
								@endif
							</div>
					

						</div>


						
					</div>
				</div>
				@endif
				
				<div class="col-md-6 col-sm-12 col-xs-12 no-padding">
					<br class="visible-sm"/>

					<div class="row"> 
						<div class="col-md-6 col-sm-6 col-xs-12 no-padding">
							<label class="payment-label big-label-cart">{{ Language::trans('Koji način plaćanja ti odgovara?') }}</label>
							<div class="form-group">
								<ul class="payment-method">
								@foreach(DB::table('web_nacin_placanja')->where('selected',1)->orderBy('b2c_default','desc')->get() as $key => $row)
									<li>
										<input type="radio" id="pay-method-{{ $row->web_nacin_placanja_id }}" name="web_nacin_placanja_id" value="{{ ($row->web_nacin_placanja_id) }}" {{ (Input::old('web_nacin_placanja_id') && Input::old('web_nacin_placanja_id') == $row->web_nacin_placanja_id) ? 'checked' : $key == 0 ? 'checked' : '' }}> 
										<label for="pay-method-{{ $row->web_nacin_placanja_id }}" class="flex pay-method-{{ $row->web_nacin_placanja_id }}">
											<span class="payment-span">{{ Language::trans($row->naziv) }}</span>
										</label>
									</li>
								@endforeach
								</ul>
							</div>
						</div>

						<div class="col-md-6 col-sm-6 col-xs-12 no-padding">
							<label class="delivery-label big-label-cart">{{ Language::trans('Način isporuke') }}:</label>
							<div class="form-group">
								<ul class="delivery-method">
									@foreach(DB::table('web_nacin_isporuke')->where('selected', 1)->get() as $key => $nacinIsporuke)
										<li>
											<input type="radio" id="delivery-method-{{ $nacinIsporuke->web_nacin_isporuke_id }}" name="web_nacin_isporuke_id" value="{{ $nacinIsporuke->web_nacin_isporuke_id }}" {{ (Input::old('web_nacin_isporuke_id') && Input::old('web_nacin_isporuke_id') == $nacinIsporuke->web_nacin_isporuke_id) ? 'checked' : $key == 0 ? 'checked' : '' }}>
											<label for="delivery-method-{{ $nacinIsporuke->web_nacin_isporuke_id }}" class="flex delivery-method-{{ $nacinIsporuke->web_nacin_isporuke_id }}"> 
												<span class="delivery-span">{{ Language::trans($nacinIsporuke->naziv) }}</span>	
											</label>	
										</li>
									@endforeach
								</ul>	
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-6 col-sm-12 col-xs-12 no-padding">
					@if(Session::has('b2c_kupac'))
						<div class="form-group JSwithout-reg-personal {{ Input::old('flag_vrsta_kupca') == 1 ? '' : 'active' }}">
							<!-- <label>{{ Language::trans('Napomena') }}:</label> -->
							@if(Cart::kamata(Cart::korpa_id()) >0 )					
							<textarea class="form-control" rows="5" tabindex="9" name="napomena" placeholder="{{ Language::trans('Napomena') }}">{{ Language::trans('Kupovina na:') }} {{ Cart::kamata(Cart::korpa_id()) }} {{ Language::trans('rata') }}</textarea>
							@else
							<textarea class="form-control" rows="5" tabindex="9" name="napomena" placeholder="{{ Language::trans('Napomena') }}">{{ htmlentities(Input::old('napomena') ? Input::old('napomena') : '') }}</textarea>
							@endif
						</div>
					@endif
				</div>
			</div>

			@if(Options::neregistrovani_korisnici()==1 OR Session::has('b2c_kupac'))
				<!-- @if(Session::has('b2c_kupac') AND Options::web_options(314)==1)
				<div class="row">
					<div class="col-md-6 col-sm-12 col-xs-12 form-group">
						<label>{{ Language::trans('Bodovi') }}</label>
						<div>{{ Language::trans('Ovom kupovinom broj bodova koji možete ostvariti je') }} <span id="JSAchievePoints">{{ Cart::bodoviOstvareniBodoviKorpa() }}</span>.</div>
						<div>{{ Language::trans('Broj bodova kiji imate je') }} {{ WebKupac::bodovi() }}.
							@if(!is_null($trajanjeBodova = WebKupac::trajanjeBodova()))
							{{ Language::trans('Rok važenja bodova je') }} {{ $trajanjeBodova }}.
							@endif
						</div>
						<div>{{ Language::trans('Maksimalni broj bodova kiji možete iskoristiti u ovoj kupovini je') }} <span id="JSMaxUsingPoints">{{ Cart::bodoviPopustBodoviKorpa() }}</span>.</div>
					</div>
					<div class="col-md-6 col-sm-12 col-xs-12 form-group">
						<label>{{ Language::trans('Unesite bodove koje želite da iskoristite') }}:</label>
						<input type="text" class="form-control" name="bodovi" tabindex="6" value="{{ htmlentities(Input::old('bodovi') ? Input::old('bodovi') : '') }}">
						<div class="error red-dot-error">{{ Session::has('bodovi_error') ? Session::get('bodovi_error') : '' }}</div>
					</div>
				</div>		
				@endif -->

				<!-- @if(Session::has('b2c_kupac') AND Options::web_options(318)==1)
				<div class="row">
					<div class="col-md-6 col-sm-12 col-xs-12 form-group">
						<label>{{ Language::trans('Vaučeri') }}</label>
						<div>{{ Language::trans('Maksimalni popust preko vaučera kiji možete iskoristiti u ovoj kupovini je') }} <span id="JSMaxUsingVoucherPrice">{{ Cart::cena(Cart::vauceriPopustCenaKorpa()) }}</span></div>
					</div>
					<div class="col-md-6 col-sm-12 col-xs-12 form-group">
						<label>{{ Language::trans('Unesite kod sa vašeg vaučera') }}:</label>
						<input type="text" class="form-control" name="vaucer_code" tabindex="6" value="{{ htmlentities(Input::old('vaucer_code') ? Input::old('vaucer_code') : '') }}">
						<div class="error red-dot-error">{{ Session::has('vaucer_error') ? Session::get('vaucer_error') : '' }}</div>
					</div>
				</div>		
				@endif -->
				<!-- 
				@if(Options::web_options(318)==1)
				<div class="col-md-5 col-sm-12 col-xs-12 hidden">
					<label>{{ Language::trans('Vaučeri') }}</label>
					<div>{{ Language::trans('Maksimalni popust preko vaučera kiji možete iskoristiti u ovoj kupovini je') }} <span id="JSMaxUsingVoucherPrice">{{ Cart::cena(Cart::vauceriPopustCenaKorpa()) }}</span></div>
				</div>
				
				<div class="col-md-12 col-sm-12 col-xs-12 no-padding">
					<div class="row">
						<div class="col-md-5 col-sm-12 col-xs-12 no-padding">
							<label>{{ Language::trans('Unesite kod sa vašeg vaučera') }}:</label>
							<div class="relative">
								<input id="cartBasePrice" type="hidden" value="{{Cart::cart_ukupno()}}">
								<input id="vaucer_code" type="text" name="vaucer_code" tabindex="6" value="{{ htmlentities(Input::old('vaucer_code') ? Input::old('vaucer_code') : '') }}">
								<button type="button" id="JSvoucherSend" class="discountBtn">{{ Language::trans('Primeni') }}</button>
							</div>
							<div class="error red-dot-error">{{ Session::has('vaucer_error') ? Session::get('vaucer_error') : '' }}</div>
						</div>
					</div>

				</div>	
				@endif -->

				<div class="w-390"> 
					<div class="form-group"> 
						<label class="hidden-xs">&nbsp;</label>
						<div class="capcha text-center"> 
							{{ Captcha::img(5, 160, 50) }}<br>
							<span>{{ Language::trans('Unesite kod sa slike') }}</span>
							<input type="text" name="captcha-string" class="form-control" tabindex="10" autocomplete="off">
							<div class="error red-dot-error">{{ $errors->first('captcha') ? $errors->first('captcha') : "" }}</div>
						</div>
					</div>
				</div>
				<!-- <div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">  
						 <span class="red-dot-error"><i class="fas fa-star-of-life"></i></span> {{ Language::trans('Obavezna polja') }}
					</div>
				</div> -->
			@endif

			@if(Options::neregistrovani_korisnici()==1 OR Session::has('b2c_kupac'))

				<div class="text-center cart-terms">
					<div>
						{{ Language::trans('Saglasan/a sam i prihvatam Uslove korišćenja') }}
					</div>
					<div class="JScheck-term">
						<input type="checkbox"> 
						<a href="{{ Options::base_url() }}uslovi-kupovine" target="_blank">{{ Language::trans('Uslovi korišćenja') }}</a><br>
					</div>
				</div>

				<div class="w-390 relative"> 
					<button id="JSOrderSubmit" class="button text-uppercase">{{ Language::trans('Poruči') }}</button>

					<!-- FAKE BTN -->
					<div class="JScall-alert-cart button text-uppercase text-center pointer"> {{ Language::trans('Poruči') }} </div>
				</div>
			@endif
		</form>	
	</div>

	@else

	<div class="empt-cart text-center">
		<img class="img-responsive" src="{{ Options::domain() }}images/cart_empty.png">
		<div class="text-bold"> 
			<p> {{Language::trans('Tvoja korpa je i dalje prazna')}}. </p>
			<p> {{Language::trans('Na sreću, postoji lako rešenje')}}. </p>
		</div>
		<a class="button center-block text-uppercase" href="/pocetna"> {{Language::trans('Kreni u kupovinu')}} </a>
	</div>

	@endif
</div>

<br>

<script type="text/javascript">
	if($(window).width() < 768) {
		$('.JScart-item').each(function(){
			$(this).find('.JS-xs-wrapp').wrapAll("<div class='col-xs-8 JS-cart-info-wrapper'> <ul class='row'> </ul></div>");
		});
	}
</script>

@endsection
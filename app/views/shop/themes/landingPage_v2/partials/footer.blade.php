<footer class="relative" style="background-color: {{ Options::web_options(322,'str_data') }}">

	<div class="footer-content relative"> 

		<div class="container{{(Options::web_options(322)==1) ? '-fluid' : '' }}"> 
			<div class="row"> 

				@foreach(All::footer_sections() as $footer_section)
					@if($footer_section->naziv == 'slika')
					<div class="col-md-3 col-sm-3 col-xs-12">  
						@if(!is_null($footer_section->slika))
						<a class="logo center-block" href="{{ $footer_section->link }}">
							 <img src="{{ Options::domain() }}{{ $footer_section->slika }}" alt="{{Options::company_name()}}" />
						</a>
						@else
						<a class="logo center-block" href="/" title="{{Options::company_name()}}">
							 <img src="{{ Options::domain() }}{{Options::company_logo()}}" alt="{{Options::company_name()}}" />
						</a>
						@endif 
						{{ $footer_section->sadrzaj }}  
					</div> 

					@elseif($footer_section->naziv == 'text')

					<div class="col-md-3 col-sm-3 col-xs-12">  
						<h2 class="footer-title">{{ $footer_section->naslov }}</h2>  
						{{ $footer_section->sadrzaj }}   
					</div>

					@elseif($footer_section->naziv == 'linkovi')
					<div class="col-md-3 col-sm-3 col-xs-12"> 

						<h2 class="footer-title">{{ $footer_section->naslov }}</h2>

						<ul>
							@foreach(All::footer_section_pages($footer_section->futer_sekcija_id) as $page)
				                @if($page->grupa_pr_id != -1 and $page->grupa_pr_id != 0)
				                <li>
				                    <a href="{{ Options::base_url()}}{{ Url_mod::slug_trans(Groups::getGrupa($page->grupa_pr_id)) }}">{{ Language::trans($page->title) }}</a>
				                </li>
				                
								@elseif($page->tip_artikla_id == -1)
								<li>
									<a href="{{ Options::base_url().Url_mod::page_slug($page->naziv_stranice)->slug }}">{{ Url_mod::page_slug($page->naziv_stranice)->naziv }}</a>
								</li>
					            @else

					            @if($page->tip_artikla_id==0)
				                <li>
				                	<a href="{{ Options::base_url().Url_mod::slug_trans('akcija') }}">{{ Language::trans($page->title) }}</a>
				                </li>
					            @else
				                <li>
				                	<a href="{{ Options::base_url().Url_mod::slug_trans('tip').'/'.Url_mod::slug_trans(Support::tip_naziv($page->tip_artikla_id)) }}">
				                    {{ Language::trans($page->title) }}
				                	</a> 
				                </li>
					            @endif

					        	@endif

							@endforeach
						</ul>   
					</div>

					@elseif($footer_section->naziv == 'kontakt')
					<div class="col-md-3 col-sm-3 col-xs-12"> 
						<h2 class="footer-title">{{ $footer_section->naslov }}</h2> 
						
						<ul> 
							<li>{{ Options::company_adress() }}	{{ Options::company_city() }}</li>
							<li>{{ Options::company_phone() }}</li> 
							<li><a class="mailto" href="mailto:{{ Options::company_email() }}">{{ Options::company_email() }}</a></li>  
						</ul>
		
					 </div>

					@elseif($footer_section->naziv == 'drustvene_mreze')
					<div class="col-md-3 col-sm-3 col-xs-12">
				    	<h2 class="footer-title">{{ $footer_section->naslov }}</h2>
						
						<div class="social-icons">
							{{Options::social_icon()}} 
					 	</div>
					</div>	
					
		  			@elseif($footer_section->naziv == 'mapa' AND Options::company_map() != '' AND Options::company_map() != ';')
		 			<div class="col-md-3 col-sm-3 col-xs-12">
						<iframe src="https://maps.google.com/maps?q={{ All::lat_long()[0] }},{{ All::lat_long()[1] }}&output=embed" width="100%" height="100%" frameborder="0" aria-hidden="false" tabindex="0"></iframe>
		 			</div>						
					@endif 

				@endforeach 
			</div> 
		</div> 

		<div class="below-footer text-center"> 
			<p class="no-margin">{{ Options::company_name() }} &copy; {{ date('Y') }}. {{Language::trans('Sva prava zadržana')}}. - <a href="https://www.selltico.com/">{{ Language::trans('Izrada web prezentacija') }}</a> - 
				<a href="https://www.selltico.com/"> Selltico. </a>
			</p> 
		</div>
	 	<a class="JSscroll-top" href="javascript:void(0)" ><i class="fa fa-chevron-up"></i></a>

 	</div>
</footer>
  
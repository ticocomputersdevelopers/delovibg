<?php // echo $kara; ?>
<div class="filters"> 
	<div> 
 
		<div class="clearfix JShidden-if-no-filters">
			<span>{{ Language::trans('Izabrani filteri') }}:</span>
			<a class="JSreset-filters-button inline-block pull-right" role="button" href="javascript:void(0)" rel="nofollow">{{ Language::trans('Poništi filtere') }} <span class="fas fa-times"></span></a>
		</div> 

		<ul class="selected-filters">
			<?php $br=0;
			foreach($niz_proiz as $row){
				$br+=1;
				if($row>0){
					?>
					<li>
						{{All::get_manofacture_name($row)}}     
						<a href="javascript:void(0)" rel="nofollow">
							<span class="JSfilter-close fas fa-times" data-type="1" data-rb="{{$br}}" data-element="{{$row}}"></span>
						</a>
					</li>
				<?php }}

				$br=0;
				foreach($niz_karakteristike as $row){
					$br+=1;
					if($row>0){
						?>
						<li>
							{{All::get_fitures_name($row)}}
							<a href="javascript:void(0)" rel="nofollow">
								<span class="JSfilter-close fas fa-times" data-type="2" data-rb="{{$br}}" data-element="{{$row}}"></span>
							</a>
						</li>
					<?php }}?>
				</ul>
				<input type="hidden" id="JSmanufacturer_ids" value="{{$proizvodjac_ids}}"/>
				<input type="hidden" id="JSmanufacturer_attr" value="{{$kara}}"/>
			</div>	 
 
			<ul>  
				@if(count($manufacturers)>0)
				<li>
					<a href="javascript:void(0)" class="filter-links center-block JSfilters-slide-toggle" rel="nofollow">{{ Language::trans('Proizvođač') }}
						<!-- <span class="choosed_filter">@foreach($manufacturers as $key => $value) @if(in_array($key,$niz_proiz)) {{ All::get_manofacture_name($key).' ' }} @endif @endforeach </span> -->
						<i class="fas fa-angle-down pull-right"></i>
					</a>

					<div class="JSfilters-slide-toggle-content">
						@foreach($manufacturers as $key => $value)
						@if(in_array($key,$niz_proiz))
						<label class="flex">
							<input name="proizvodjac[]" onchange="filters_send()" value="{{ $key }}" type="checkbox" checked>
							<span class="filter-text center-block">
								{{All::get_manofacture_name($key)}} 
							</span>
							<span class="filter-count">
								@if(Options::filters_type()) {{ $value }} @endif
							</span>
						</label>
						@else
						<label class="flex">
							<input name="proizvodjac[]" onchange="filters_send()" value="{{ $key }}" type="checkbox">
							<span class="filter-text center-block">
								{{All::get_manofacture_name($key)}} 
							</span>
							<span class="filter-count">
								@if(Options::filters_type()) {{ $value }} @endif
							</span>
						</label>
						@endif
						@endforeach
					</div>				 				 
				</li>
				@endif

					@foreach($characteristics as $keys => $values)
					<li>
						<a href="javascript:void(0)" class="filter-links center-block JSfilters-slide-toggle" rel="nofollow">
							{{Language::trans($keys)}} 
							<!-- <span class="choosed_filter"> @foreach($values as $key => $value) @if(in_array($key, $niz_karakteristike)) {{ All::get_fitures_name($key) }} @endif @endforeach </span> -->
							<i class="fas fa-angle-down pull-right"></i>  							
						</a>

						<div class="JSfilters-slide-toggle-content">
							@foreach($values as $key => $value)
							<?php if(in_array($key, $niz_karakteristike)){ ?>
								<label class="flex">
									<input type="checkbox" onchange="filters_send()" name="vrednost[]" value="{{ $key }}" checked>
									<span class="filter-text center-block">
										{{All::get_fitures_name($key)}}
									</span>
									<span class="filter-count">
										@if(Options::filters_type()) {{ $value }} @endif
									</span>
								</label>
							<?php }else {?>
								<label class="flex">
									<input type="checkbox" onchange="filters_send()" name="vrednost[]" value="{{ $key }}"> 
									<span class="filter-text center-block">
										{{All::get_fitures_name($key)}}
									</span>
									<span class="filter-count">
										@if(Options::filters_type()) {{ $value }} @endif
									</span>
								</label>
							<?php } ?>
							@endforeach
						</div>
					</li>
					@endforeach			
				</ul>	 
				<input type="hidden" id="JSfilters-url" value="{{$url}}" />

				<!-- SLAJDER ZA CENU -->
				<div class="text-center"> 
					<br>	
					<span id="JSamount" class="filter-price"></span>
					<div id="JSslider-range"></div><br>
				</div>

				<script>
					var max_web_cena_init = {{ $max_web_cena }};
					var min_web_cena_init = {{ $min_web_cena }};
					var max_web_cena = {{ $cene!='0-0' ? explode('-',$cene)[1] : $max_web_cena }};
					var min_web_cena = {{ $cene!='0-0' ? explode('-',$cene)[0] : $min_web_cena }};
					var kurs = {{ Session::has('valuta') ? (Session::get('valuta')!="2" ? 1 : Options::kurs()) : 1 }};
				</script>
			</div>



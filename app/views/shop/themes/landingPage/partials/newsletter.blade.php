
<?php $newslatter_description = All::newslatter_description(); ?>
@if(Options::newsletter()==1)
<div class="newsletter">
<div class="container">
    <div class="row">
    	<div class="col-md-6">
    		<h5 class="JSInlineShort" data-target='{"action":"newslatter_label"}'>{{ $newslatter_description->naslov }}</h5> 
    		<p class="JSInlineFull" data-target='{"action":"newslatter_content"}'>{{ $newslatter_description->sadrzaj }}</p>
    	</div>

    	<div class="col-md-6">
    		<div class="newsletter-form">              
	            <input type="text" class="form-control" placeholder="Unesite Vašu e-mail adresu" id="newsletter" />
	           
	            <button onclick="newsletter()">{{ Language::trans('Prijavi se') }}</button>
	        </div>
    	</div>
    </div>
</div>
</div>
@endif
 
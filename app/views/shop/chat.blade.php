
<?php $chat = Options::chat(); ?>
@if($chat and $chat->int_data == 1 and $chat->str_data and $chat->str_data != '')
<!--Start of Tawk.to Script-->
<script>
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='{{ $chat->str_data }}';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
@endif

@extends('rma.templates.main')
@section('content')
<section id="main-content">
	<div class="row">
		@include('rma/partials/radni_nalog_tabs')
		<div class="flat-box">
			<div class="columns medium-12">
				<h1>{{ $radni_nalog->broj_naloga }} ({{RMA::getStatusTitle($radni_nalog->status_id)}})</h1>
			</div>
		</div>
	</div>
	
 	<div class="row">
 		<div class="flat-box">  
 			<h3 class="title-med">Uput servisu</h3> 
			<div class="columns medium-10 no-padd"> 
				<form method="POST" action="{{RmaOptions::base_url()}}rma/radni-nalog-trosak-dokumenti-save"> 
					<div class="row">
						<div class="columns medium-3">
							<label for="">Uslužni Servis</label>
							<input name="partner_id" class="datum-val has-tooltip" type="text" value="{{ $partner_naziv }}" disabled>
			            </div>
			            <div class="columns medium-3">
							<label for="">Broj Dokumenta</label>
							<input name="radni_nalog_trosak_broj_dokumenta" class="datum-val has-tooltip" type="text" value="{{ $radni_nalog_trosak_uput->radni_nalog_trosak_broj_dokumenta }}" disabled>
		            	</div>
						<div class="columns medium-2">
							<label for="">Datum dokumenta</label>
							<input name="datum_dokumenta" class="datum-val has-tooltip {{ $errors->first('datum_dokumenta') ? 'error' : '' }}" type="text" value="{{ Input::old('datum_dokumenta') ? Input::old('datum_dokumenta') : $radni_nalog_trosak_uput->datum_dokumenta }}" >
						</div>
			        </div>
			        <div class="row">
						<div class="columns medium-6">
							<label for="">Uređaj</label>
							<input name="uredjaj" class="{{ $errors->first('uredjaj') ? 'error' : '' }}" type="text" value="{{ Input::old('uredjaj') ? Input::old('uredjaj') : $radni_nalog_trosak_uput->uredjaj }}" >
						</div>
						<div class="columns medium-6">
							<label for="">Serijski broj</label>
							<input name="serijski_broj" class="{{ $errors->first('serijski_broj') ? 'error' : '' }}" type="text" value="{{ Input::old('serijski_broj') ? Input::old('serijski_broj') : $radni_nalog_trosak_uput->serijski_broj }}" >
						</div>
					</div>

					<div class="row"> 
						<div class="columns medium-6">
							<label for="">Opis kvara</label>
							<textarea name="opis_kvara" rows="3" class="{{ $errors->first('opis_kvara') ? 'error' : '' }}">{{ Input::old('opis_kvara') ? Input::old('opis_kvara') : $radni_nalog_trosak_uput->opis_kvara }}</textarea>
						</div>

						<div class="columns medium-6">
							<label for="">Napomena</label>
							<textarea name="napomena" rows="2">{{ !is_null(Input::old('napomena')) ? Input::old('napomena') : $radni_nalog_trosak_uput->napomena }}</textarea>
						</div>
					</div> 

					<input type="hidden" name="uput" value="{{ $radni_nalog_trosak_uput->uput }}">
					<input type="hidden" name="radni_nalog_trosak_id" value="{{ $radni_nalog_trosak_uput->radni_nalog_trosak_id }}">
					<input type="hidden" name="radni_nalog_trosak_dokument_id" value="{{ $radni_nalog_trosak_uput->radni_nalog_trosak_dokument_id }}">

					<div class="columns medium-12 large-12 text-center">
						<div class="btn-container"> 
							<button type="submit" class="btn btn-primary save-it-btn">Sačuvaj</button>
						</div>
						@if($radni_nalog_trosak_uput->radni_nalog_trosak_dokument_id > 0)
						<div class="btn-container"> 
							<a class="remove icon JSbtn-delete" data-link="{{RmaOptions::base_url()}}rma/radni-nalog-trosak-dokument/{{ $radni_nalog_trosak_uput->radni_nalog_trosak_dokument_id }}/delete"><i class="fa fa-trash" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Obriši</a>
						</div>
						@endif
					</div>
				</form>
			</div>
			<div class="columns medium-2 text-right">
				<label>&nbsp;</label>
				<a class="btn btn-primary btn-small" href="{{ RmaOptions::base_url() }}rma/radni-nalog-usluzni-servis-uput/{{$radni_nalog_trosak_uput->radni_nalog_trosak_id}}">Štampaj</a>
			</div>
		</div>
	</div>

 	<div class="row">
 		<div class="flat-box">  
 		    <h3 class="title-med">Povratni dokument</h3>
		 	<form method="POST" action="{{RmaOptions::base_url()}}rma/radni-nalog-trosak-dokumenti-prijem-save">
				<div class="columns medium-12 no-padd">
					<div class="row">
						<div class="columns medium-4">
							<label for="">Uređaj</label>
							<input name="prijem_uredjaj" class="{{ $errors->first('prijem_uredjaj') ? 'error' : '' }}" type="text" value="{{ Input::old('prijem_uredjaj') ? Input::old('prijem_uredjaj') : $radni_nalog_trosak_prijem->uredjaj }}" >
						</div>
			            <div class="columns medium-3">
							<label for="">Broj Dokumenta</label>
							<input name="prijem_radni_nalog_trosak_broj_dokumenta" class="{{ $errors->first('prijem_radni_nalog_trosak_broj_dokumenta') ? 'error' : '' }}" type="text" value="{{ Input::old('prijem_radni_nalog_trosak_broj_dokumenta') ? Input::old('prijem_radni_nalog_trosak_broj_dokumenta') : $radni_nalog_trosak_prijem->radni_nalog_trosak_broj_dokumenta }}">
		            	</div>
		            	<div class="columns medium-3">
							<label for="">Serijski broj</label>
							<input name="prijem_serijski_broj" class="{{ $errors->first('prijem_serijski_broj') ? 'error' : '' }}" type="text" value="{{ Input::old('prijem_serijski_broj') ? Input::old('prijem_serijski_broj') : $radni_nalog_trosak_prijem->serijski_broj }}" >
						</div>
						<div class="columns medium-2">
							<label for="">Datum dokumenta</label>
							<input name="prijem_datum_dokumenta" class="datum-val has-tooltip {{ $errors->first('prijem_datum_dokumenta') ? 'error' : '' }}" type="text" value="{{ Input::old('prijem_datum_dokumenta') ? Input::old('prijem_datum_dokumenta') : $radni_nalog_trosak_prijem->datum_dokumenta }}" >
						</div> 
			        </div>
			        <div class="row"> 
						<div class="columns medium-4">
							<label for="">Napomena</label>
							<textarea name="prijem_napomena" rows="2" class="{{ $errors->first('prijem_napomena') ? 'error' : '' }}">{{ !is_null(Input::old('prijem_napomena')) ? Input::old('prijem_napomena') : $radni_nalog_trosak_prijem->napomena }}</textarea>
						</div>
					</div> 
				</div> 
				<input type="hidden" name="prijem_uput" value="{{ $radni_nalog_trosak_prijem->uput }}">
				<input type="hidden" name="prijem_radni_nalog_trosak_id" value="{{ $radni_nalog_trosak_prijem->radni_nalog_trosak_id }}">
				<input type="hidden" name="prijem_radni_nalog_trosak_dokument_id" value="{{ $radni_nalog_trosak_prijem->radni_nalog_trosak_dokument_id }}">

				<div class="columns medium-12 large-12 text-center">
					@if($radni_nalog_trosak_uput->radni_nalog_trosak_dokument_id > 0)
					<div class="btn-container"> 
						<button type="submit" class="btn btn-primary save-it-btn">Sačuvaj</button>
					</div>
					@if($radni_nalog_trosak_prijem->radni_nalog_trosak_dokument_id > 0)
					<div class="btn-container"> 
						<a class="remove icon JSbtn-delete" data-link="{{RmaOptions::base_url()}}rma/radni-nalog-trosak-dokument/{{ $radni_nalog_trosak_prijem->radni_nalog_trosak_dokument_id }}/delete"><i class="fa fa-trash" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Obriši</a>
					</div>
					@endif
					@endif
				</div>
			</form>  
		</div>
	</div> 
</section>
@endsection
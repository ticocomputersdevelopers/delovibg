	<div class="m-tabs clearfix">
		<div class="m-tabs__tab{{ in_array($strana,array('rma','radni_nalog_prijem','radni_nalog_opis_rada','radni_nalog_dokumenti','radni_nalog_predaja')) ? ' m-tabs__tab--active' : '' }}"><a href="{{RmaOptions::base_url()}}rma">RADNI NALOZI </a></div>

		<div class="m-tabs__tab{{ $strana=='operacije' ? ' m-tabs__tab--active' : '' }}"><a href="{{RmaOptions::base_url()}}rma/operacije">OPERACIJE </a></div>

	</div>
	@if(Session::has('message'))
		<script>
			alertify.success('{{ Session::get('message') }}');
		</script>
	@endif
	@if(Session::has('error_message'))
		<script>
			alertify.error('{{ Session::get('error_message') }}');
		</script>
	@endif
@if(AdminOptions::checkAOPTrial())
	<div class="row trial-period">
		<span class="time-left">{{ AdminLanguage::transAdmin('Ostalo je još') }} <span>{{ AdminSupport::trial_date()->days+1 }}</span>{{ AdminLanguage::transAdmin('dana probnog perioda') }}</span>
		<a href="{{ AdminOptions::base_url()}}admin/podesavanje-naloga/izaberi-paket">{{ AdminLanguage::transAdmin('Izaberi plan') }}</a>
	</div>
@endif
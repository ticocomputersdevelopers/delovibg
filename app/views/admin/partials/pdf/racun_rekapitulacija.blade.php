<div class="row"> 
        	<br>
			<h4>Rekapitulacija</h4>
		</div>

		<div class="row"> 
			<div class="col-7"> 
				<table class="rekapitulacija">
					<thead>
						<tr>
							<td></td>
							<td>Osnovica</td>
							<td>PDV</td>
						</tr>
					</thead>

					<tbody>
						<tr>
							<td>Oslobođeno poreza</td>
							<td>0,00</td>
							<td>0,00</td>
						</tr>
						<tr>
							<td>Po posebnoj stopi</td>
							<td>0,00</td>
							<td>0,00</td>
						</tr>
						<tr>
							<td>Po opštoj stopi</td>
							@if(AdminOptions::web_options(311)==1)
							<td>{{AdminCommon::cena(AdminCommon::racun_ukupno_bez_pdv($web_b2c_narudzbina_id))}}</td>
							<td>{{AdminCommon::cena(AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id) - AdminCommon::racun_ukupno_bez_pdv($web_b2c_narudzbina_id))}}</td>
							@else
							<td> {{AdminCommon::cena(AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id))}}</td>
							<td>0,00</td>
							@endif
						</tr>
						<tr>
							<td><strong>Ukupno</strong></td>
							@if(AdminOptions::web_options(311)==1)
							<td>{{AdminCommon::cena(AdminCommon::racun_ukupno_bez_pdv($web_b2c_narudzbina_id))}}</td>
							<td>{{AdminCommon::cena(AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id) - AdminCommon::racun_ukupno_bez_pdv($web_b2c_narudzbina_id))}}</td>
							@else
							<td> {{AdminCommon::cena(AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id))}}</td>
							<td>0,00</td>
							@endif
						</tr>
					</tbody>
				</table>
		 	</div>

		 	<div class="col-4"> 
		        <table class="summary">
		        	<tr class="text-right">
		        		 @if(AdminOptions::web_options(311)==1)
		                <td class="summary text-right"><span class="sum-span">Iznos osnovice:</span>
		                 {{AdminCommon::cena(AdminCommon::racun_ukupno_bez_pdv($web_b2c_narudzbina_id))}}</td>
		                 @else
		                <td class="summary text-right"><span class="sum-span">Iznos osnovice:</span>
		                 {{AdminCommon::cena(AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id))}}</td>
		                 @endif
		            </tr>
		            <tr class="text-right">
		                @if(AdminOptions::web_options(311)==1)
		                <td class="summary text-right"><span class="sum-span">Iznos PDV-a:</span>
		                 {{AdminCommon::cena(AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id) - AdminCommon::racun_ukupno_bez_pdv($web_b2c_narudzbina_id))}}</td>
		                @else
		                <td class="summary text-right"><span class="sum-span">Iznos PDV-a:</span>0,00</td>
		                @endif
		            </tr>
		            <?php $vaucer_popust = Order::vaucer_popust($web_b2c_narudzbina_id); ?>
		          	@if($vaucer_popust > 0)
		        	<tr class="text-right">
						<td class="summary text-right"><span class="sum-span">Vaučer popust:</span>
							{{ number_format(floatval($vaucer_popust),2,',','') }}
						</td>
		            </tr>
		            @endif
					@if(AdminOptions::web_options(133)==1)
		            <tr class="text-right">		                
		                <td class="summary text-right"><span class="sum-span">Troškovi isporuke:</span>
		                @if(AdminCommon::cena_dostave_custom($web_b2c_narudzbina_id) > 0 )
		           		 	{{AdminCommon::cena(AdminCommon::cena_dostave_custom($web_b2c_narudzbina_id))}}</td>
		                @else
		                	{{AdminCommon::cena(AdminCommon::troskovi_isporuke($web_b2c_narudzbina_id))}}
		                @endif		                	
		             	</td>
		            </tr>
		            <tr class="text-right">
		                <td class="summary text-right"><span class="sum-span">{{ AdminLanguage::transAdmin('Ukupno') }}:</span> 
		                @if(AdminCommon::cena_dostave_custom($web_b2c_narudzbina_id) > 0 )	
		                	{{AdminCommon::cena(AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id)-$vaucer_popust+AdminCommon::cena_dostave_custom($web_b2c_narudzbina_id))}}
		                @else
		               		{{AdminCommon::cena(AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id)-$vaucer_popust+AdminCommon::troskovi_isporuke($web_b2c_narudzbina_id))}}
		                @endif
		            	</td>
		            </tr>		            
		            @elseif(AdminOptions::web_options(150)==1 && AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id) < AdminCommon::cena_do())
		            <tr class="text-right">		                
		                <td class="summary text-right"><span class="sum-span">Troškovi isporuke:</span>
		                @if(AdminCommon::cena_dostave_custom($web_b2c_narudzbina_id) > 0 )
		           		 {{AdminCommon::cena(AdminCommon::cena_dostave_custom($web_b2c_narudzbina_id))}}</td>
		                @else
		                 {{AdminCommon::cena(AdminCommon::cena_dostave())}}</td>
		                @endif
		            </tr>
		           
		            <tr class="text-right">
		                <td class="summary text-right"><span class="sum-span">{{ AdminLanguage::transAdmin('Ukupno') }}:</span> 
		                @if(AdminCommon::cena_dostave_custom($web_b2c_narudzbina_id) > 0 )	
		                	{{AdminCommon::cena(AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id)-$vaucer_popust+AdminCommon::cena_dostave_custom($web_b2c_narudzbina_id))}}
		                @else
		               		{{AdminCommon::cena(AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id)-$vaucer_popust+AdminCommon::cena_dostave($web_b2c_narudzbina_id))}}
		                @endif
		            	</td>
		            </tr>
		            @else
		            <tr class="text-right">
		                <td class="summary text-right"><span class="sum-span">{{ AdminLanguage::transAdmin('Ukupno') }}:</span> 
		                {{AdminCommon::cena(AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id)-$vaucer_popust)}}</td>
		            </tr>
		            @endif	     
		            @if(AdminCommon::check_discount($web_b2c_narudzbina_id)!= 0)
		            <tr class="text-right">
		                <td class="summary text-right"><span class="sum-span">{{ AdminLanguage::transAdmin('Popust') }}:</span> 	
		                {{AdminCommon::check_discount($web_b2c_narudzbina_id)}}</td>
		          	</tr>
		          	 <tr class="text-right">
		                <td class="summary text-right"><span class="sum-span">{{ AdminLanguage::transAdmin('Ukupno sa popustom') }}:</span> @if(AdminCommon::cena($cena_dostave)>0)	
		                {{AdminCommon::cena(AdminCommon::order_distributive_discount($web_b2c_narudzbina_id) + $cena_dostave)}} @else {{AdminCommon::cena(AdminCommon::order_distributive_discount($web_b2c_narudzbina_id))}} @endif </td>
		          	</tr>
		          	@endif         
		            </tr>
		        </table>
	        </div>
		</div>
<div class="row"> 
	<br>
	<h4>{{ AdminLanguage::transAdmin('Rekapitulacija') }}</h4>
</div>
		
<div class="row"> 
	<div class="col-7"> 
		<table class="rekapitulacija">
			<thead>
			<tr>
				<td></td>
				<td>{{ AdminLanguage::transAdmin('Osnovica') }}</td>
				<td>{{ AdminLanguage::transAdmin('PDV') }}</td>
			</tr>
			</thead>

			<tbody>
					<tr>
					<td>{{ AdminLanguage::transAdmin('Oslobodjeno poreza') }}</td>
					<td>0,00</td>
					<td>0,00</td>
				</tr>
				<tr>
					<td>{{ AdminLanguage::transAdmin('Po posebnoj stopi') }}</td>
					<td>0,00</td>
					<td>0,00</td>
				</tr>
				<tr>
					<td>{{ AdminLanguage::transAdmin('Po opštoj stopi') }}</td>
					<td>{{AdminCommon::cena(AdminCommon::racun_ukupno_bez_pdv($web_b2c_narudzbina_id))}}</td>
					<td>{{AdminCommon::cena(AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id) - AdminCommon::racun_ukupno_bez_pdv($web_b2c_narudzbina_id))}}</td>
				</tr>
				<tr>
					<td><strong>{{ AdminLanguage::transAdmin('Ukupno') }}</strong></td>
					<td>{{AdminCommon::cena(AdminCommon::racun_ukupno_bez_pdv($web_b2c_narudzbina_id))}}</td>
					<td>{{AdminCommon::cena(AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id) - AdminCommon::racun_ukupno_bez_pdv($web_b2c_narudzbina_id))}}</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="col-4"> 	
		<table class="summary">
			<tr class="text-right">
				<td class="summary text-right"><span class="sum-span">{{ AdminLanguage::transAdmin('Iznos osnovice') }}:</span> {{AdminCommon::cena(AdminCommon::racun_ukupno_bez_pdv($web_b2c_narudzbina_id))}}</td>
			</tr>
			<tr class="text-right">
				<td class="summary text-right"><span class="sum-span">{{ AdminLanguage::transAdmin('Iznos PDV-a') }}:</span> {{AdminCommon::cena(AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id) - AdminCommon::racun_ukupno_bez_pdv($web_b2c_narudzbina_id))}}</td>
			</tr>
			@if(AdminOptions::web_options(133)==1)
            <tr class="text-right">		                
                <td class="summary text-right"><span class="sum-span">Troškovi isporuke:</span>
                @if(AdminCommon::cena_dostave_custom($web_b2c_narudzbina_id) > 0 )
           		 	{{AdminCommon::cena(AdminCommon::cena_dostave_custom($web_b2c_narudzbina_id))}}</td>
                @else
                	{{AdminCommon::cena(AdminCommon::troskovi_isporuke($web_b2c_narudzbina_id))}}
                @endif		                	
             	</td>
            </tr>
            <tr class="text-right">
                <td class="summary text-right"><span class="sum-span">{{ AdminLanguage::transAdmin('Ukupno') }}:</span> 
                @if(AdminCommon::cena_dostave_custom($web_b2c_narudzbina_id) > 0 )	
                	{{AdminCommon::cena(AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id)+AdminCommon::cena_dostave_custom($web_b2c_narudzbina_id))}}
                @else
               		{{AdminCommon::cena(AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id)+AdminCommon::troskovi_isporuke($web_b2c_narudzbina_id))}}
                @endif
            	</td>
            </tr>		            
            @elseif(AdminOptions::web_options(150)==1 && AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id) < AdminCommon::cena_do())
            <tr class="text-right">		                
                <td class="summary text-right"><span class="sum-span">Troškovi isporuke:</span>
                @if(AdminCommon::cena_dostave_custom($web_b2c_narudzbina_id) > 0 )
           		 {{AdminCommon::cena(AdminCommon::cena_dostave_custom($web_b2c_narudzbina_id))}}</td>
                @else
                 {{AdminCommon::cena(AdminCommon::cena_dostave())}}</td>
                @endif
            </tr>
            <tr class="text-right">
                <td class="summary text-right"><span class="sum-span">{{ AdminLanguage::transAdmin('Ukupno') }}:</span> 
                @if(AdminCommon::cena_dostave_custom($web_b2c_narudzbina_id) > 0 )	
                	{{AdminCommon::cena(AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id)+AdminCommon::cena_dostave_custom($web_b2c_narudzbina_id))}}
                @else
               		{{AdminCommon::cena(AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id)+AdminCommon::cena_dostave($web_b2c_narudzbina_id))}}
                @endif
            	</td>
            </tr>
            @else
            <tr class="text-right">
                <td class="summary text-right"><span class="sum-span">{{ AdminLanguage::transAdmin('Ukupno') }}:</span> 
                {{AdminCommon::cena(AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id))}}</td>
            </tr>
            @endif
		</table>
    </div>
</div>
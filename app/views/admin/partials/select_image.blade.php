
@foreach($images as $image)
<div class="column medium-3 no-padd text-center">
	<div class="padding-h-8 padding-v-8 margin-bottom col-slika">
		<div class="modal-image-add flex"><img class="JSSelectImage" src="{{ AdminOptions::base_url() }}images/upload_image/{{ $image }}" data-id="{{ $image }}"></div>
		<a class="btn btn-danger JSModalSlikaDelete" data-id="{{ $image }}">Obriši</a>
	</div>
</div>
@endforeach
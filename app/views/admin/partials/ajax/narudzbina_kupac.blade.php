
<div class="columns medium-6">
    <label for="">{{ AdminLanguage::transAdmin('Adresa') }}</label>
    <input name="adresa" type="text" value="{{ $kupac->adresa }}" disabled {{ Admin_model::check_admin(array('NARUDZBINE_AZURIRANJE')) == false ? 'disabled' : '' }}>
</div>
<div class="columns medium-6">
    <label for="">{{ AdminLanguage::transAdmin('Mesto') }}</label> 
    <input name="mesto" type="text" value="{{ $kupac->mesto }}" disabled {{ Admin_model::check_admin(array('NARUDZBINE_AZURIRANJE')) == false ? 'disabled' : '' }}>
</div>
<div class="columns medium-6">
    <label for="">{{ AdminLanguage::transAdmin('Telefon') }}</label>
    <input name="telefon" type="text" value="{{ $kupac->telefon }}" disabled {{ Admin_model::check_admin(array('NARUDZBINE_AZURIRANJE')) == false ? 'disabled' : '' }}>
</div>
<div class="columns medium-6">
    <label for="">{{ AdminLanguage::transAdmin('Mobilni') }}</label>
    <input name="mobilni" type="text" value="{{ $kupac->telefon_mobilni }}" disabled {{ Admin_model::check_admin(array('NARUDZBINE_AZURIRANJE')) == false ? 'disabled' : '' }}>
</div>
<div class="columns medium-6">
    <label for="">{{ AdminLanguage::transAdmin('Email') }}</label>
    <input name="email" type="email" value="{{ $kupac->email }}" disabled {{ Admin_model::check_admin(array('NARUDZBINE_AZURIRANJE')) == false ? 'disabled' : '' }}>
</div>
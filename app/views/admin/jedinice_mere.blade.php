<div id="main-content" >
	@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
	@endif

	@include('admin/partials/tabs')

	<div class="row">
		<div class="medium-3 columns">
			<div class="flat-box">
				<h3 class="title-med">{{ AdminLanguage::transAdmin('Izaberi jedinicu mere') }}</h3>
				<div class="manufacturer"> 
					<select class="JSeditSupport search_select">
						<option value="{{ AdminOptions::base_url() }}admin/jedinica-mere/0">{{ AdminLanguage::transAdmin('Dodaj novi') }}</option>
						@foreach(AdminSupport::getJediniceMere() as $row)
						<option value="{{ AdminOptions::base_url() }}admin/jedinica-mere/{{ $row->jedinica_mere_id }}" @if($row->jedinica_mere_id == $jedinica_mere_id) {{ 'selected' }} @endif>({{ $row->jedinica_mere_id }}) {{ $row->naziv }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>

		<section class="medium-5 columns">
			<div class="flat-box">

				<h1 class="title-med">{{ $title }}</h1> 

				<form method="POST" action="{{ AdminOptions::base_url() }}admin/jedinica-mere-edit" enctype="multipart/form-data">
					<div class="row">
						<input type="hidden" name="jedinica_mere_id" value="{{ $jedinica_mere_id }}"> 

						<div class="columns {{ $errors->first('naziv') ? ' error' : '' }}">
							<label for="naziv">{{ AdminLanguage::transAdmin('Naziv jedinice mere') }}</label>
							<input type="text" name="naziv" value="{{ htmlentities(Input::old('naziv') ? Input::old('naziv') : $naziv) }}" autofocus="autofocus" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}> 
						</div>
					</div>

					@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE'))) 
					<div class="btn-container center">
						<button type="submit" class="btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
						@if($jedinica_mere_id != 0)
						<button class="btn btn-danger JSbtn-delete" data-link="{{ AdminOptions::base_url() }}admin/jedinica-mere-delete/{{ $jedinica_mere_id }}">{{ AdminLanguage::transAdmin('Obriši') }}
						</button>
						@endif
					</div> 
					@endif
				</form> 
			</div>   
		</section>
	</div>  
</div>
@if(Session::has('message'))
<script>
	alertify.success('{{ Session::get('message') }}');
</script>
@endif

<section id="main-content">

	@include('admin/partials/tabs')

	<div class="row">
		<section class="medium-3 columns">
			<div class="flat-box">
				<h3 class="title-med">{{ AdminLanguage::transAdmin('Izaberi Konfigurator') }}</h3>
				<select class="JSeditSupport">
					<option value="{{ AdminOptions::base_url() }}admin/konfigurator">{{ AdminLanguage::transAdmin('Dodaj novi') }}</option>
					@foreach($konfiguratori as $row)
					<option value="{{ AdminOptions::base_url() }}admin/konfigurator/{{ $row->konfigurator_id }}"{{ $row->konfigurator_id == $konfigurator_id ? 'selected' : '' }}>{{ $row->naziv }}</option>
					@endforeach
				</select>
			</div>
		</section>

		<section class="medium-5 columns">
			<div class="flat-box">
				<h3 class="title-med">{{ $title }}</h3>

				<form method="POST" action="{{ AdminOptions::base_url() }}admin/konfigurator-edit" enctype="multipart/form-data">
					<div class="row">
						<input type="hidden" name="konfigurator_id" value="{{ $konfigurator_id }}"> 
						<div class="columns medium-6 {{ $errors->first('naziv') ? ' error' : '' }}">
							<label for="">{{ AdminLanguage::transAdmin('Naziv konfiguratora') }}</label>
							<input type="text" name="naziv" value="{{ htmlentities(Input::old('naziv') ? Input::old('naziv') : $naziv) }}" autofocus="autofocus" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}> 
						</div>

						<div class="columns medium-6">
							<label for="">{{ AdminLanguage::transAdmin('Aktivan') }}</label>
							<select name="dozvoljen" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}>
								{{ AdminSupport::selectCheck($dozvoljen) }}
							</select>
						</div>
					</div>

					@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE'))) 
					<div class="btn-container center">
						<button type="submit" class="btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
						@if($konfigurator_id != null)
						<button class="btn btn-danger JSbtn-delete" data-link="{{ AdminOptions::base_url() }}admin/konfigurator-delete/{{ $konfigurator_id }}">{{ AdminLanguage::transAdmin('Obriši') }}</button>
						@endif
					</div> 
					@endif
				</form> 
			</div>
		</section>

		@if($konfigurator_id != null)
		<section class="medium-4 columns">
			<div class="flat-box clearfix">
				<h3 class="title-med">{{ AdminLanguage::transAdmin('Grupe') }}</h3>
				
				<table id="JSTabelaKonf" class="karakteristike-tabela" data-id="{{ $konfigurator_id }}" role="grid">
					@foreach($konfigurator_grupe as $row)
					<tr>
						<td><input type="text" class="JSRbr ordered-number" value="{{ $row->rbr }}"></td>
						<td>			
							<select class="JSgrupa_pr_id">
								{{AdminSupport::selectGroups($row->grupa_pr_id)}}
							</select>
						</td>
						<td>
							<button class="JSSaveKonf name-ul__li__save button-option tooltipz" aria-label="{{ AdminLanguage::transAdmin('Sačuvaj') }}" data-id="{{ $row->grupa_pr_id }}"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
				 	&nbsp;
							<button class="JSDeleteKonf name-ul__li__remove button-option tooltipz" aria-label="{{ AdminLanguage::transAdmin('Obriši') }}" data-id="{{ $row->grupa_pr_id }}"><i class="fa fa-times" aria-hidden="true"></i></button>
						</td>
						<td></td>
					</tr>
					@endforeach
					<tr>
						<td class="JSRbr" data-id="new" style="width:60px"></td>
						<td>			
							<select class="JSgrupa_pr_id">{{ AdminSupport::selectGroups()}}</select>
						</td>
						<td>
							<button class="JSSaveKonf name-ul__li__save button-option tooltipz" aria-label="{{ AdminLanguage::transAdmin('Sačuvaj') }}" data-id="0"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
						</td>
						<td></td>
					</tr>
				</table>
			</div>
		</section>
		@endif
	</div> 
</section>
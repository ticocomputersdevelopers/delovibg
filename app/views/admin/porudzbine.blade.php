<?php
if($datum_od==0 and $datum_do==0){
	$dat1='';
	$dat2='';
}else 
if($datum_od!=0 and $datum_do==0){
	$dat1=$datum_od;
	$dat2='';
}else 
if($datum_od==0 and $datum_do!=0){
	$dat1='';
	$dat2=$datum_do;
}else 
{
	$dat1=$datum_od;
	$dat2=$datum_do;
}
?>

<section id="main-content">
	<!-- ORDER LIST -->
	<section class="bw"> 

		<!--=============================
		=            Filters            = 
		==============================-->
		<div class="row flex"> 
			<div class="column medium-4"> 
				<h1 class="inline-block no-margin vert-align">{{ AdminLanguage::transAdmin('Narudžbine') }}</h1>
				<!-- ====================== -->
				<a href="#" class="video-manual" data-reveal-id="orders-manual">{{ AdminLanguage::transAdmin('Uputstvo') }} <i class="fa fa-film"></i></a>
				<div id="orders-manual" class="reveal-modal medium" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
					<div class="video-manual-container"> 
						<p><span class="video-manual-title">{{ AdminLanguage::transAdmin('Narudžbine') }}</span></p>
						<iframe src="https://player.vimeo.com/video/271250335" width="840" height="426" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
					</div>
					<a class="close-reveal-modal" aria-label="Close">&#215;</a>
				</div>
				<!-- ====================== -->
			</div>

			<div class="orders-input column medium-4"> 
				<input type="text" class="search-field" id="search" autocomplete="on" placeholder="{{ AdminLanguage::transAdmin('Pretraži narudžbine') }}...">
				<button type="submit" id="search-btn" value="{{$search}}" class="m-input-and-button__btn btn btn-primary btn-radius">
					<i class="fa fa-search" aria-hidden="true"></i>
				</button>
			</div>
			@if(Admin_model::check_admin(array('NARUDZBINE_AZURIRANJE')))
			<div class="columns medium-4 text-right"><a href="/admin/narudzbina/0" class="btn btn-create btn-small">{{ AdminLanguage::transAdmin('Kreiraj narudžbinu') }}</a></div>
			@endif
		</div>
		
		<div class="o-filters row"> 
			<div class="column medium-5">
				<label class="no-margin">{{ AdminLanguage::transAdmin('Filteri kupca') }}:</label> 

				<div class="inline-block">
					<label class="checkbox-label">
						<input type="checkbox" class="JSStatusNarudzbine" data-status="registrovani" {{  in_array('registrovani',$statusi) ? 'checked' : '' }}>
						<span class="label-text">{{ AdminLanguage::transAdmin('Registrovani') }}</span>
					</label>
				</div>

				&nbsp;
				<div class="inline-block">
					<label class="checkbox-label">
						<input type="checkbox" class="JSStatusNarudzbine" data-status="neregistrovani" {{ in_array('neregistrovani',$statusi) ? 'checked' : '' }}>
						<span class="label-text">{{ AdminLanguage::transAdmin('Neregistrovani') }}</span>
					</label>
				</div>

				&nbsp;
				<div class="inline-block">
					<label class="checkbox-label">
						<input type="checkbox" class="JSStatusNarudzbine" data-status="privatna" {{ in_array('privatna',$statusi) ? 'checked' : '' }}>
						<span class="label-text private">{{ AdminLanguage::transAdmin('Fizička Lica') }}</span>
					</label>
				</div>

				&nbsp;
				<div class="inline-block">
					<label class="checkbox-label">
						<input type="checkbox" class="JSStatusNarudzbine" data-status="pravna" {{ in_array('pravna',$statusi) ? 'checked' : '' }}>
						<span class="label-text legal-person">{{ AdminLanguage::transAdmin('Pravna Lica') }}</span>
					</label>
				</div>
			</div>

			<div class="column medium-4"> 
				<label class="no-margin">{{ AdminLanguage::transAdmin('Filteri narudžbina') }}:</label> 

				<div class="inline-block">
					<label class="checkbox-label">
						<input type="checkbox" class="JSStatusNarudzbine" data-status="nove" {{ in_array('nove',$statusi) ? 'checked' : '' }}>
						<span class="label-text nova">{{ AdminLanguage::transAdmin('Nove') }}</span>
					</label>
				</div>

				&nbsp;
				<div class="inline-block">
					<label class="checkbox-label">
						<input type="checkbox" class="JSStatusNarudzbine" data-status="prihvacene" {{ in_array('prihvacene',$statusi) ? 'checked' : '' }}>
						<span class="label-text prihvacena">{{ AdminLanguage::transAdmin('Prihvaćene') }}</span>
					</label>
				</div>

				&nbsp;
				<div class="inline-block">
					<label class="checkbox-label">
						<input type="checkbox" class="JSStatusNarudzbine" data-status="realizovane" {{ in_array('realizovane',$statusi) ? 'checked' : '' }}>
						<span class="label-text">{{ AdminLanguage::transAdmin('Realizovane') }}</span>
					</label>
				</div>

				&nbsp;
				<div class="inline-block">
					<label class="checkbox-label">
						<input type="checkbox" class="JSStatusNarudzbine" data-status="stornirane" {{ in_array('stornirane',$statusi) ? 'checked' : '' }}>
						<span class="label-text stornirana">{{ AdminLanguage::transAdmin('Stornirane') }}</span>
					</label>
				</div>
			</div>

			<div class="column medium-3">
				@if(AdminOptions::checkNarudzbine() == 1)
				<div class="column medium-9 small-11"> 
					<label class="no-margin">{{ AdminLanguage::transAdmin('Filteri dodatnih statusa') }}:</label>
					<select class="m-input-and-button__input import-select " id="narudzbina_status_id" >
						<option value="0">{{ AdminLanguage::transAdmin('Svi dodatni statusi') }}</option>
						@foreach(AdminNarudzbine::dodatni_statusi() as $statusi)
						@if($statusi->narudzbina_status_id == $narudzbina_status_id )	
						<option value="{{ $statusi->narudzbina_status_id }}" selected>{{ $statusi->naziv }}</option>
						@else
						<option value="{{ $statusi->narudzbina_status_id }}">{{ $statusi->naziv }}</option>
						@endif				
						@endforeach
					</select>	
				</div>
				@endif

				<a href="#" class="m-input-and-button__btn btn btn-danger erase-btn tooltipz" aria-label="{{ AdminLanguage::transAdmin('Poništi filtere') }}">
					<span class="fa fa-times" id="JSFilterClear" aria-hidden="true"></span>
				</a>
			</div> 
		</div>
		
		<div class="row o-filters-span-2">

			<div class="columns medium-5">
				<span>{{ AdminLanguage::transAdmin('Ukupno narudžbina') }}: <b>{{count($query)}}</b></span>
			</div>

			<div class="columns medium-7">

				<span class="datepicker-col">
					<div class="datepicker-col__box clearfix">
						<label>{{ AdminLanguage::transAdmin('Od') }}:</label>
						<input id="datepicker-from" class="has-tooltip" data-datumdo="{{$datum_do}}" value="{{$dat1}}" type="text">
					</div>

					<div class="datepicker-col__box clearfix">
						<label>{{ AdminLanguage::transAdmin('Do') }}:</label>
						<input id="datepicker-to" class="has-tooltip" data-datumod="{{$datum_od}}" value="{{$dat2}}" type="text">
					</div>
				</span>
			</div>
		</div>


		<!--====  End of Filters  ====-->
		
		@if(AdminOptions::checkNarudzbine() == 1)
		
		<div class="table-scroll" style="max-height: none;">
			<table class="order-table">
				<thead>
					<tr>
						<th>{{ AdminLanguage::transAdmin('Ime i prezime') }}</th>
						<th>{{ AdminLanguage::transAdmin('Broj narudžbine') }}</th>
						<th>{{ AdminLanguage::transAdmin('Datum') }}</th>
						<th>{{ AdminLanguage::transAdmin('Artikli') }}</th>
						<th class="text-right">{{ AdminLanguage::transAdmin('Iznos') }}</th>
						<th>{{ AdminLanguage::transAdmin('Telefon') }}</th>
						<th>{{ AdminLanguage::transAdmin('Mesto') }}</th>
						<th>{{ AdminLanguage::transAdmin('Adresa kupca') }}</th>
						<th>{{ AdminLanguage::transAdmin('Dodatni status') }}</th>
						<th>{{ AdminLanguage::transAdmin('Status') }}</th>
						
						@if(Admin_model::check_admin(array('NARUDZBINE_AZURIRANJE')))
						<th>{{ AdminLanguage::transAdmin('Obriši') }}</th>
						@endif
					</tr>
				</thead>

				<tbody>
					@foreach($query as $row) 
					<tr data-id-narudzbina="{{$row->web_b2c_narudzbina_id}}" class="JSMoreWiew {{AdminNarudzbine::narudzbina_status_css($row->web_b2c_narudzbina_id)}}">
						
						<td>{{Admin_model::narudzbina_kupac($row->web_b2c_narudzbina_id)}}</td>
						
						<td>{{$row->broj_dokumenta}}</td>
						
						<td>{{AdminNarudzbine::datum_narudzbine($row->web_b2c_narudzbina_id)}}</td>
						
						<td class="text-left"> 
							@foreach(AdminNarudzbine::narudzbina_stavke($row->web_b2c_narudzbina_id) as $stavka) 
							<div class="tooltipz counter" aria-label='{{ AdminArticles::find($stavka->roba_id, "naziv_web") }}'>
								<span class="ellipsis-text vert-align inline-block">{{ AdminArticles::find($stavka->roba_id, 'naziv_web') }}</span>
							</div>  
							@endforeach 
						</td>
						
						<td class="text-right">{{AdminCommon::cena(AdminNarudzbine::narudzbina_iznos_ukupno($row->web_b2c_narudzbina_id))}}</td>
						
						<td>{{AdminNarudzbine::kupacTelefoni($row->web_kupac_id)}}</td>
						
						<td>{{AdminNarudzbine::kupacMesto($row->web_kupac_id)}}</td>    
						
						<td>{{AdminNarudzbine::kupacAdresa($row->web_kupac_id)}}</td>
						
						<td>
							<span class="ellipsis-text vert-align inline-block">{{AdminNarudzbine::status_narudzbine($row->web_b2c_narudzbina_id)}}</span>
						</td>

						<td> 
							<section class="order-status"> 
								<select class="order-status__select" {{ Admin_model::check_admin(array('NARUDZBINE_AZURIRANJE')) == false ? 'disabled' : '' }}>
									<option class="order-status-active">{{AdminNarudzbine::narudzbina_status_active($row->web_b2c_narudzbina_id)}}</option>
									{{AdminNarudzbine::narudzbina_status_posible(AdminNarudzbine::narudzbina_status_active($row->web_b2c_narudzbina_id),$row->web_b2c_narudzbina_id)}}
								</select>
							</section> 
						</td>

						@if(Admin_model::check_admin(array('NARUDZBINE_AZURIRANJE'))) 
						<td class="JSDeleting text-center"><i class="fa fa-times red"></i></td>
						@endif 
					</tr>
					@endforeach
				</tbody> 
			</table>
		</div> 
		
		<!-- PAGINATION -->
		{{ Paginator::make($query, $count, $limit)->links() }}
		
		<!-- OBICAN PREGLED NARUDZBINA	 -->
		@else
		<div class="table-scroll" style="max-height: none;">
			<table class="order-table"> 
				<thead>
					<tr>
						<th>{{ AdminLanguage::transAdmin('Ime i prezime') }}</th>
						<th>{{ AdminLanguage::transAdmin('Broj narudžbine') }}</th>
						<th>{{ AdminLanguage::transAdmin('Datum') }}</th> 
						<th class="text-right">{{ AdminLanguage::transAdmin('Iznos') }}</th>
						<th>{{ AdminLanguage::transAdmin('Telefon') }}</th> 
						<th>{{ AdminLanguage::transAdmin('Adresa kupca') }}</th> 
						<th>{{ AdminLanguage::transAdmin('Status') }}</th>
						
						@if(Admin_model::check_admin(array('NARUDZBINE_AZURIRANJE')))
						<th class="text-center">{{ AdminLanguage::transAdmin('Obriši') }}</th>
						@endif
					</tr>
				</thead>
				
				<tbody> 
					@foreach($query as $row) 
					<tr data-id-narudzbina="{{$row->web_b2c_narudzbina_id}}" class="JSMoreWiew {{AdminNarudzbine::narudzbina_status_css($row->web_b2c_narudzbina_id)}}">
						
						<td>{{Admin_model::narudzbina_kupac($row->web_b2c_narudzbina_id)}}</td>

						<td>{{$row->broj_dokumenta}}</td> 
						
						<td>{{AdminNarudzbine::datum_narudzbine($row->web_b2c_narudzbina_id)}}</td>
						
						@if(WebKupac::check_distributive_user($row->web_kupac_id))
                        	<td data-id-narudzbina="{{$row->web_b2c_narudzbina_id}}" class="text-right">{{ Cart::cena(AdminCommon::order_distributive_discount($row->web_b2c_narudzbina_id)) }}</td>
	                    @else
	                    	<td data-id-narudzbina="{{$row->web_b2c_narudzbina_id}}" class="text-right">{{Cart::cena(Order::narudzbina_ukupno($row->web_b2c_narudzbina_id))}}</td>
	                    @endif


						
						<td>{{AdminNarudzbine::kupacAdresa($row->web_kupac_id)}}</td>

						<td>
							<section class="order-status">
								<select class="order-status__select">
									<option class="order-status-active">{{AdminNarudzbine::narudzbina_status_active($row->web_b2c_narudzbina_id)}}</option>
									{{AdminNarudzbine::narudzbina_status_posible(AdminNarudzbine::narudzbina_status_active($row->web_b2c_narudzbina_id),$row->web_b2c_narudzbina_id)}}
								</select>
							</section>
						</td> 

						@if(Admin_model::check_admin(array('NARUDZBINE_AZURIRANJE'))) 
						<td class="JSDeleting text-center"><i class="fa fa-times red"></i></td>
						@endif 
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>

		<!-- PAGINATION -->
		{{ Paginator::make($query, $count, $limit)->links() }}
		@endif
	</section>
	
</section> <!-- end of #main-content -->

<!-- MODAL ZA DETALJI PORUDJINE -->
<div id="ordersDitailsModal" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<div class="content"></div>
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
<!-- MODAL END -->
<div id="main-content">
	
	@include('admin/partials/tabs')

	<?php
	$messages = array();
	if($errors->first('vaucer_broj')){ $messages[] = $errors->first('vaucer_broj'); }
	if($errors->first('iznos')){ $messages[] = $errors->first('iznos'); }
	if($errors->first('datum_od')){ $messages[] = $errors->first('datum_od'); }
	if($errors->first('datum_do')){ $messages[] = $errors->first('datum_do'); }
	if($errors->first('web_cena')){ $messages[] = $errors->first('web_cena'); }
	if($errors->first('popust')){ $messages[] = $errors->first('popust'); }
	?>
	@if(count($messages)>0)
	<script>
		alertify.error('{{ $messages[0] }}');
	</script>
	@elseif(Session::has('success'))
	<script>
		alertify.success('Uspešno ste sačuvali podatak');
	</script>
	@elseif(Session::has('success-delete'))
	<script>
		alertify.success('Uspešno ste obrisali vrednost.');
	</script>
	@endif

	<!-- procenat vauceri -->
	<div class="row">
		<section class="medium-6 medium-centered columns">
			<div class="flat-box">

				<h3 class="title-med">{{ AdminLanguage::transAdmin('Popust') }}</h3>

				<div class="table-scroll"> 
					<table>
						<thead>  
							<tr>
								<th>{{ AdminLanguage::transAdmin('Cena proizvoda') }} {{ AdminLanguage::transAdmin('od') }}</th>
								<th>{{ AdminLanguage::transAdmin('Cena proizvoda') }} {{ AdminLanguage::transAdmin('do') }}</th>
								<th>{{ AdminLanguage::transAdmin('Popust') }}</th>
								
								@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))
								<th></th>
								<th></th>
								@endif
							</tr>
						</thead>

						<tbody>
							@foreach($vauceri_popust as $row)
							<tr>
								<form method="POST" action="{{AdminOptions::base_url()}}admin/vauceri-podesavanja-save">
									<input type="hidden" name="vauceri_popust_id" value="{{ $row->vauceri_popust_id }}">
									<input type="hidden" name="popust" value="{{ $row->popust }}">				
									<td><input type="text" value="{{ $row->web_cena_od }}" disabled="true"></td>
									<td><input type="text" name="web_cena" value="{{ $row->web_cena_do }}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}></td>
									<td><input type="text" name="popust" value="{{ $row->popust }}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}></td>
									
									@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))
									<td>
										<input class="btn btn-small btn-secondary" type="submit" value="{{ AdminLanguage::transAdmin('Sačuvaj') }}">
									</td>

									<td>@if($row->vauceri_popust_id!=0)
										<a class="JSbtn-delete" data-link="{{AdminOptions::base_url()}}admin/vauceri-podesavanja-delete/{{$row->vauceri_popust_id}}"><i class="fa fa-times" style="color:red;" aria-hidden="true"></i>&nbsp;</a>@endif</td>
										@endif
									</form>
								</tr>
								@endforeach					
							</tbody>
						</table>
					</div>
				</div>
			</section>
		</div>


		<div class="row">
			<section class="medium-12 columns">
				<div class="flat-box">
					
					<div class="row collapse">
						<div class="columns medium-4"> 
							<div class="btn-container"> 
								<input type="text" id="myInput" placeholder="Pretraga.."> 
							</div> 
						</div>

						@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))
						<div class="columns medium-8 text-right">
							<form method="POST" action="{{AdminOptions::base_url()}}admin/vauceri-import" enctype="multipart/form-data">
								<div class="bg-image-file"> 
									<input type="file" name="import_file">					 
									<button class="btn btn-primary btn-small" type="submit">{{ AdminLanguage::transAdmin('Importuj') }}</button>
									<div class="error">{{ $errors->first('import_file') ? $errors->first('import_file') : '' }}</div>
								</div>	 
							</form>
						</div>
						@endif 
					</div>

					<div class="table-scroll"> 
						<table id="myTable">
							<thead>
								<tr>
									<td>Naziv kupona</td>
									<td>Vaučer broj</td>
									<td>Tip vaučera</td>									
									<td>Iznos</td>
									<td>Datum akcije</td>
									<td>Važi do</td>
									<td>Kategorija</td>
									<td>Realizovan</td>
									<td>Ime kupca</td> 
									<td>Narudžbina</td> 
									<td></td>
									<td></td>
								</tr>
							</thead>

							<tbody>
								@foreach($vauceri as $vaucer)
								<form method="POST" action="{{AdminOptions::base_url()}}admin/vauceri-save" autocomplete="off">
									<input type="hidden" name="vaucer_id" value="{{$vaucer->vaucer_id}}"> 
									<tr class="vaucer">
										<td><input type="text" name="naziv" value="{{$vaucer->naziv}}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}></td>

										<td  style="position: relative;">
											<input id="vaucer_broj" type="text" name="vaucer_broj" value="{{$vaucer->vaucer_broj}}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}> <span class="JSvaucerRefresh"><i class="fa fa-refresh"></i></span>
 										</td>

										<td>
											<select name="vaucer_tip_id" class="JSVaucerTip" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}>
												<option value=-1 @if($vaucer->vaucer_tip_id == -1) selected @endif>Izaberite tip vaučera</option>
												@foreach(AdminSupport::getVaucerTip() as $vauceri)
												<option value="{{$vauceri->vaucer_tip_id}}" @if($vauceri->vaucer_tip_id == $vaucer->vaucer_tip_id) selected @endif>
													{{$vauceri->vaucer_tip}}
												</option>
												@endforeach
											</select>
										</td>										
				
										<td><input type="text" class="JSVaucerIznos" name="iznos" value="{{$vaucer->iznos}}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}></td>
										
										<td><input type="date" class="datum" name="datum_od" value="{{$vaucer->datum_od}}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}></td>
										
										<td><input type="date" class="datum" name="datum_do" value="{{$vaucer->datum_do}}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}></td>
										
										<td>
											<select name="grupa_pr_id" class="JSGrupa" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}>
										
												{{ AdminSupport::selectGroups($vaucer->grupa_pr_id) }}
	
											</select>
										</td>

										<td>
											<select name="realizovan" class="JSrealizovan" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}>
												<option value=1 @if($vaucer->realizovan) selected @endif>Da</option>
												<option value=0 @if($vaucer->realizovan == 0) selected @endif>Ne</option>
											</select>
										</td>								
										<td>
											<select name="kupac" class="JSkupac" @if($vaucer->realizovan == 0) disabled @endif {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}>
												<option value=-1 @if($vaucer->web_kupac_id == -1) selected @endif>Izaberite kupca</option>
												@foreach(AdminSupport::getKupac() as $kupac)
												<option value="{{$kupac->web_kupac_id}}" @if($vaucer->web_kupac_id == $kupac->web_kupac_id) selected @endif>{{AdminSupport::find_kupac($kupac->web_kupac_id, 'ime')}} {{AdminSupport::find_kupac($kupac->web_kupac_id, 'prezime')}}</option>
												@endforeach
											</select>
										</td>
										
										<td>
											@if($vaucer->realizovan)
											{{ AdminSupport::find_narudzbina($vaucer->web_b2c_narudzbina_id) }}
											@endif
										</td>

										@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))
										<td>
											<input class="btn btn-small btn-secondary" type="submit" class="btn btn-small" value="Sačuvaj"></td>
											@if($vaucer->vaucer_id!=0)
											<td>
												<a class="JSbtn-delete" data-link="{{AdminOptions::base_url()}}admin/vauceri-delete/{{$vaucer->vaucer_id}}"><i class="fa fa-times" style="color:red;" aria-hidden="true"></i>&nbsp;</a>
											</td>
											@endif
											@endif
										</tr>
									</form>
									@endforeach
								</tbody>
							</table> 
						</div>
					</div> 
				</section>
			</div>
		</div>
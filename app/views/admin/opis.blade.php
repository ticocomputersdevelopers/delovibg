<div id="main-content" class="">
@include('admin/partials/product-tabs')

	<div class="row">
		<div class="column large-6 large-centered">
			<div class="flat-box">
				<form method="POST" action="{{AdminOptions::base_url()}}admin/opis_edit">

					<div class="row">
						<h3 class="title-med">{{ AdminLanguage::transAdmin('Opis artikla') }}</h3>
						<div class="column large-12 medium-12">
							<input type="hidden" id="roba_id" name="roba_id" value="{{ $roba_id ? $roba_id : 0 }}">
							<textarea @if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE'))) class="special-textareas" @endif id="mc" name="web_opis" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>{{ $web_opis }}</textarea>
						</div>
					</div>
					<!-- end of .row -->
					<div class="row">
						<h3 class="title-med">{{ AdminLanguage::transAdmin('Kratak opis') }}</h3>
						<div class="column large-12 medium-12">
							<!-- <textarea name="opis" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>{{ $opis }}</textarea> -->
							<textarea @if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE'))) class="special-textareas" @endif id="mc" name="opis" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>{{ $opis }}</textarea>
						</div>
					</div>

					<!-- end of .row -->
					<div class="row">
						<h3 class="title-med">{{ AdminLanguage::transAdmin('Uvoznik') }}</h3>
						<div class="column large-12 medium-12">
							<!-- <textarea name="opis" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>{{ $opis }}</textarea> -->
							<textarea @if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE'))) class="special-textareas" @endif id="mc" name="naziv_displej" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>{{ $naziv_displej }}</textarea>
						</div>
					</div>
					@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
						<div class="row">
							<div class="column large-12 medium-12">
								<input type="hidden" name="roba_id" value="{{ $roba_id ? $roba_id : 0 }}">
								<div class="btn-container center">
									<a class="setting-button btn btn-primary" href="#" id="JSsablon">{{ AdminLanguage::transAdmin('Unesi šablon') }}</a>
									<button type="submit" class="setting-button btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
								</div>
							</div>	
						</div> <!-- end of .row -->
					@endif
				</form>	
				 
			</div>
			@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
				<div class="image-upload-area clearfix">
					<form method="post" enctype="multipart/form-data" action="{{AdminOptions::base_url()}}admin/image_upload_opis" >
						<label>{{ AdminLanguage::transAdmin('Dodajte sliku u opisu') }}:</label>

						<input type="file" id="img" name="img" class="file-input">
						<button class="file-upload btn btn-secondary" type="submit">{{ AdminLanguage::transAdmin('Učitaj') }}</button>
					</form>
					<div class="images_upload has-tooltip" title='Prevucite sliku na željenu poziciju'>
						{{AdminOsobine::upload_directory_opis()}}
					</div>
				</div>	
			@endif	
		</div>
	</div>
</div>

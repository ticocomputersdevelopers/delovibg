<div id="main-content" class="kupci-page">
	@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
	@endif	

	<div class="m-subnav">  
		<a class="m-subnav__link" href="{{AdminOptions::base_url()}}admin/kupci_partneri/kupci" >
			<div class="m-subnav__link__icon">
				<i class="fa fa-arrow-left" aria-hidden="true"></i>
			</div>
			<div class="m-subnav__link__text">
				{{ AdminLanguage::transAdmin('Nazad') }}
			</div>
		</a>
	</div> 

	<div class="row">
		<div class="medium-6 medium-centered columns">
			<div class="flat-box">

				<form action="{{AdminOptions::base_url()}}admin/kupci_partneri/kupci/{{ $web_kupac_id }}/save" method="post" class="new-customer-form" autocomplete="false">
					<div class="center" @if($web_kupac_id != 0) {{ 'style="display: none;"' }} @endif>
						<div class="toggles center">
							<input type="hidden" name="web_kupac_id" value="{{ $web_kupac_id }}">
							<input type="checkbox" name="flag_vrsta_kupca" id="checkboxPP" class="ios-toggle JSKupacCheckbox" 
							{{ count(Input::old())>0 ? (!is_null(Input::old('flag_vrsta_kupca')) ? 'checked' : '') : ($flag_vrsta_kupca == 1 ? 'checked' : '') }}>
							<label for="checkboxPP" class="switch-label" data-off="{{ AdminLanguage::transAdmin('Fizicko lice') }}" data-on="{{ AdminLanguage::transAdmin('Pravno lice') }}"></label>
						</div>

					</div>

					<div class="row JSPravnoLice">
						<div class="column medium-6 {{ $errors->first('naziv') ? ' error' : '' }}">
							<label>{{ AdminLanguage::transAdmin('Naziv') }}</label>
							<input id="naziv" name="naziv" value="{{ Input::old('naziv') ? Input::old('naziv') : $naziv }}" type="text">
							<div class="error">{{ $errors->first('naziv') }}</div>
						</div>

						<div class="column medium-3 {{ $errors->first('pib') ? ' error' : '' }}">
							<label>{{ AdminLanguage::transAdmin('Pib') }}</label>
							<input id="pib" name="pib" value="{{ Input::old('pib') ? Input::old('pib') : $pib }}" type="text">
							<div class="error">{{ $errors->first('pib') }}</div>
						</div>
						<div class="column medium-3 {{ $errors->first('maticni_br') ? ' error' : '' }}">
							<label>Matični broj</label>
							<input id="maticni_br" name="maticni_br" value="{{ Input::old('maticni_br') ? Input::old('maticni_br') : $maticni_br }}" type="text">
							<div class="error">{{ $errors->first('maticni_br') }}</div>
						</div>
					</div> <!-- end of .row -->

					<div class="row JSPrivatnoLice active">
						<div class="column medium-6 {{ $errors->first('ime') ? ' error' : '' }}">
							<label for="">{{ AdminLanguage::transAdmin('Ime') }}</label>
							<input id="ime" name="ime" value="{{ Input::old('ime') ? Input::old('ime') : $ime }}" type="text">
							<div class="error">{{ $errors->first('ime') }}</div>
						</div>

						<div class="column medium-6 {{ $errors->first('prezime') ? ' error' : '' }}">
							<label for="">{{ AdminLanguage::transAdmin('Prezime') }}</label>
							<input id="prezime" name="prezime" value="{{ Input::old('prezime') ? Input::old('prezime') : $prezime }}" type="text">
							<div class="error">{{ $errors->first('prezime') }}</div>
						</div>
					</div> <!-- end of .row -->

					<div class="row">

						@if(AdminNarudzbine::api_posta(3))
						<div class="column medium-6">
							<label for="without-reg-city">{{ AdminLanguage::transAdmin('Opština') }}</label>				 
							<select class="form-control" name="opstina" id="JSOpstina" tabindex="6">
								@foreach(AdminNarudzbine::opstine() as $key => $opstina)
								@if((Input::old('opstina') ? Input::old('opstina') : AdminNarudzbine::opstina_id($ulica_id)) == $opstina->narudzbina_opstina_id)
								<option value="{{ $opstina->narudzbina_opstina_id }}" selected>{{ $opstina->naziv }}</option>
								@else
								<option value="{{ $opstina->narudzbina_opstina_id }}">{{ $opstina->naziv }}</option>
								@endif
								@endforeach
							</select>
						</div>
						<div class="column medium-6">
							<label for="without-reg-city">{{ AdminLanguage::transAdmin('Naselje') }}</label>
							<select class="form-control" name="mesto_id" id="JSMesto">
								@foreach(AdminNarudzbine::mesta(Input::old('opstina') ? Input::old('opstina') : AdminNarudzbine::opstina_id($ulica_id)) as $key => $mesto_obj)
								@if((Input::old('mesto_id') ? Input::old('mesto_id') : AdminNarudzbine::mesto_id($ulica_id)) == $mesto_obj->narudzbina_mesto_id)
								<option value="{{ $mesto_obj->narudzbina_mesto_id }}" selected>{{ $mesto_obj->naziv }}</option>
								@else
								<option value="{{ $mesto_obj->narudzbina_mesto_id }}">{{ $mesto_obj->naziv }}</option>
								@endif
								@endforeach
							</select>
						</div>
						<div class="column medium-6">
							<label for="without-reg-city">{{ AdminLanguage::transAdmin('Ulica') }}</label>				 
							<select class="form-control" name="ulica_id" id="JSUlica">
								@foreach(AdminNarudzbine::ulice(Input::old('mesto_id') ? Input::old('mesto_id') : AdminNarudzbine::mesto_id($ulica_id)) as $key => $ulica)
								@if((Input::old('ulica') ? Input::old('ulica') : $ulica_id) == $ulica->narudzbina_ulica_id)
								<option value="{{ $ulica->narudzbina_ulica_id }}" selected>{{ $ulica->naziv }}</option>
								@else
								<option value="{{ $ulica->narudzbina_ulica_id }}">{{ $ulica->naziv }}</option>
								@endif
								@endforeach
							</select>
						</div>
						<div class="column medium-6 {{ $errors->first('broj') ? ' error' : '' }}">
							<label for="without-reg-number">{{ AdminLanguage::transAdmin('Broj') }}</label>
							<input id="without-reg-number" class="form-control" name="broj" type="text" value="{{ htmlentities(Input::old('broj') ? Input::old('broj') : $broj) }}">
							{{ $errors->first('broj') }}
						</div>
						@endif

						<div class="column medium-6 {{ $errors->first('adresa') ? ' error' : '' }}">
							<label for="">{{ AdminLanguage::transAdmin('Adresa') }}</label>
							<input id="adresa" name="adresa" value="{{ Input::old('adresa') ? Input::old('adresa') : $adresa }}" type="text">
							{{ $errors->first('adresa') }}
						</div>

						<div class="column medium-6 {{ $errors->first('mesto') ? ' error' : '' }}">
							<label for=""> {{ AdminLanguage::transAdmin('Mesto/Grad') }}</label>
							<input type="text" name="mesto" value="{{ Input::old('mesto') ? Input::old('mesto') : $mesto }}">
							{{ $errors->first('mesto') }}
						</div>

						<div class="column medium-6 {{ $errors->first('telefon') ? ' error' : '' }}">
							<label for="">{{ AdminLanguage::transAdmin('Telefon') }}</label>
							<input id="telefon" name="telefon" value="{{ Input::old('telefon') ? Input::old('telefon') : $telefon }}" type="text">
							{{ $errors->first('telefon') }}
						</div>

						<div class="column medium-6 {{ $errors->first('telefon_mobilni') ? ' error' : '' }}">
							<label>{{ AdminLanguage::transAdmin('Mobilni') }}</label>
							<input id="mobilni" name="telefon_mobilni" value="{{ Input::old('telefon_mobilni') ? Input::old('telefon_mobilni') : $telefon_mobilni }}" type="text">
							{{ $errors->first('telefon_mobilni') }}
						</div>

						<div class="column medium-6 {{ $errors->first('fax') ? ' error' : '' }}">
							<label >{{ AdminLanguage::transAdmin('Fax') }}</label>
							<input id="fax" name="fax" value="{{ Input::old('fax') ? Input::old('fax') : $fax }}" type="text">
							{{ $errors->first('fax') }}
						</div>

						<div class="column medium-6 {{ $errors->first('email') ? ' error' : '' }}">
							<label>{{ AdminLanguage::transAdmin('Email') }}</label>
							<input id="email" autocomplete="off" name="email" value="{{ Input::old('email') ? Input::old('email') : $email }}" type="email">
							{{ $errors->first('email') }} 
						</div>

						<div class="column medium-6 {{ $errors->first('lozinka') ? ' error' : '' }}">
							<label>{{ AdminLanguage::transAdmin('Lozinka') }}</label>
							<input type="password" id="login-pass1" name="lozinka" value="{{ Input::old('lozinka') ? Input::old('lozinka') : $lozinka }}">
							<input style="display: none" id="login-pass2" type="text" value="{{ Input::old('lozinka') ? Input::old('lozinka') : $lozinka }}">
							<label> 
								<a href="#" id="show-pass">{{ AdminLanguage::transAdmin('Prikaži lozinku') }}</a><br>
								{{ $errors->first('lozinka') }}
							</label>
						</div>

						<div class="column medium-6"> 
							<label class="inline-block">
								<input type="checkbox" name="flag_prima_poruke" class=""
								{{ count(Input::old())>0 ? (!is_null(Input::old('flag_prima_poruke')) ? 'checked' : '') : ($flag_prima_poruke == 1 ? 'checked' : '') }}>{{ AdminLanguage::transAdmin('Newsletter') }}
							</label> 

							<label class="inline-block">
								<input type="checkbox" name="status_registracije" class="" {{ count(Input::old())>0 ? (!is_null(Input::old('status_registracije')) ? 'checked' : '') : ($status_registracije == 1 ? 'checked' : '') }}>
								{{ AdminLanguage::transAdmin('Registrovan') }}
							</label>  
						</div>
					</div> <!-- end of .row -->
					<div class="text-center o-filters">
							<label> <b>{{ AdminLanguage::transAdmin('Distributivni kod') }}</b></label>
								<input disabled id="distributivni_kod" autocomplete="off" name="distributivni_kod" value="{{ Input::old('distributivni_kod') ? Input::old('distributivni_kod') : $distributivni_kod }}" type="distributivni_kod">
							{{ $errors->first('distributivni_kod') }} 
							<label class="inline-block">
								<input type="checkbox" name="aktivan_popust" value='1' class="" {{ WebKupac::check_first_discount($web_kupac_id) ? '' : 'checked' }}>
								{{ AdminLanguage::transAdmin('Aktivan') }}
							</label> 

					</div>

					<div class="row"> 
						<div class="btn-container center">
							<button type="submit" id="submit" class="btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
							<a href="{{AdminOptions::base_url()}}admin/kupci_partneri/kupci/" class="btn btn-secondary">{{ AdminLanguage::transAdmin('Otkaži') }}</a>
							@if($web_kupac_id!=0)
							<a class="btn btn-danger JSbtn-delete" data-link="{{AdminOptions::base_url()}}admin/kupci_partneri/kupci/{{ $web_kupac_id }}/0/delete">{{ AdminLanguage::transAdmin('Obriši') }}</a>
							@endif
						</div>
					</div>
				</form> 
			</div>
		</div>
		<input type="hidden" id="JSCheckConfirm" data-id="{{ Session::has('confirm_kupac_id')?Session::get('confirm_kupac_id'):0 }}">
	</div>

	<div class="row">
		<div class="table-scroll"> 
			<table>
				<thead>
					<tr>
						<td>{{ AdminLanguage::transAdmin('NARUDŽBINA') }}</td>
						<td>{{ AdminLanguage::transAdmin('STATUS') }}</td>
						<td>{{ AdminLanguage::transAdmin('STAVKE') }}</td>
					</tr>
				</thead>
				@foreach(AdminNarudzbine::narudzbine($web_kupac_id) as $narudzbina)
				<tr>
					<td><a href="{{ AdminOptions::base_url() }}admin/narudzbina/{{ $narudzbina->web_b2c_narudzbina_id }}" target="_blank">{{ $narudzbina->broj_dokumenta }}</a></td>
					<td>{{ AdminNarudzbine::narudzbina_status_active($narudzbina->web_b2c_narudzbina_id) }}</td>
					<td>
						<table>
							<thead>
								<tr>
									<td>{{ AdminLanguage::transAdmin('ROBA_ID') }}</td>
									<td>{{ AdminLanguage::transAdmin('NAZIV') }}</td>
									<td>{{ AdminLanguage::transAdmin('KOLIČINA') }}</td>
									<td>{{ AdminLanguage::transAdmin('CENA ARTIKLA') }}</td>
								</tr>
							</thead>
							<?php $ukupna_cena = 0; ?>
							@foreach(AdminNarudzbine::narudzbina_stavke($narudzbina->web_b2c_narudzbina_id) as $stavka)
							<?php $ukupna_cena += $stavka->jm_cena*$stavka->kolicina; ?>
							<tr>
								<td>{{ $stavka->roba_id }}</td>
								<td class="max-width-450 no-white-space">{{ AdminArticles::find($stavka->roba_id, 'naziv_web') }}</td>
								<td>{{ $stavka->kolicina }}</td>
								<td>{{ intval($stavka->jm_cena) }}.00 .din</td>
							</tr>
							@endforeach
							<tr>
								<td></td>
								<td></td>
								<td></td>
								<td><b> {{ AdminLanguage::transAdmin('UKUPNO') }}: {{ $ukupna_cena }}.00 .din </b></td>
							</tr>
						</table>
					</td>
				</tr>
				@endforeach
			</table>
		</div>
	</div>
</div>
<section class="osobine-page" id="main-content">
	@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
	@endif

	@include('admin/partials/tabs')
	
	<div class="row">
		<section class="medium-3 columns">
			<div class="flat-box">	
				<h3 class="title-med">{{ AdminLanguage::transAdmin('Osobine') }}</h3>
				<div class="row"> 
					<div class="columns after-select-margin"> 
						<select name="" id="JSselectProperty">
							<option data-id="0">{{ AdminLanguage::transAdmin('Nova osobina') }}</option>
							@foreach($osobine as $row)
							<option data-id="{{ $row->osobina_naziv_id }}" @if($row->osobina_naziv_id == $osobina_naziv_id) selected @endif>{{ $row->naziv }}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
		</section>

		<section class="medium-5 columns">
			<div class="flat-box">		
				@if($osobina_naziv_id != 0)
				
				<form action="{{ AdminOptions::base_url() }}admin/osobine/{{$row->osobina_naziv_id}}" method="POST">
					@foreach($osobina as $row)
					
					<h3 class="title-med">{{ $row->naziv }}</h3>
					<?php if($row->color_picker == 1){ $color_picker_active = 1;}else{$color_picker_active = 0;}; ?>
					
					<div class="row"> 
						<input type="hidden" name="osobina_naziv_id" value="{{ $row->osobina_naziv_id }}">

						<div class="columns medium-6 {{ $errors->first('naziv') ? ' error' : '' }}">
							<label for="naziv">{{ AdminLanguage::transAdmin('Naziv') }}</label>
							<input type="text" name="naziv" value="{{ $row->naziv }}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>

						<div class="columns medium-6 {{ $errors->first('rbr') ? ' error' : '' }}">
							<label for="rbr">{{ AdminLanguage::transAdmin('Redni broj') }}</label>
							<input type="text" name="rbr" value="{{ $row->rbr }}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>

						<div class="columns medium-12">
							<label>
								<input name="aktivna" type="checkbox" @if($row->aktivna == 1) checked @endif {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}> {{ AdminLanguage::transAdmin('Aktivna') }}
							</label>

							<label>
								<input name="prikazi_vrednost" type="checkbox" @if($row->prikazi_vrednost == 1) checked @endif {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}> {{ AdminLanguage::transAdmin('Prikaži nazive vrednosti u prodavnici') }}
							</label>

							<label> 
								<input name="color_picker" type="checkbox" @if($row->color_picker == 1) checked @endif {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}> {{ AdminLanguage::transAdmin('Odabir boje') }}
							</label>
						</div> 
					</div>

					@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))
					<div class="btn-container center">
						<button type="submit" class="btn-edit-simple save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
						<button class="btn btn-danger JSbtn-delete" data-link="{{ AdminOptions::base_url() }}admin/osobine/{{ $row->osobina_naziv_id }}/delete">{{ AdminLanguage::transAdmin('Obriši') }}</button>
					</div>
					@endif
					@endforeach
				</form>

				@else

				<h3 class="title-med">{{ AdminLanguage::transAdmin('Nova osobina') }}</h3>
				<form action="{{ AdminOptions::base_url() }}admin/osobine/0" method="POST">
					<div class="row"> 
						<input type="hidden" name="osobina_naziv_id" value="0">
						<div class="columns medium-6 {{ $errors->first('naziv') ? ' error' : '' }}">
							<label for="naziv">{{ AdminLanguage::transAdmin('Naziv') }}</label>
							<input type="text" name="naziv" value="" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>

						<div class="columns medium-6 {{ $errors->first('rbr') ? ' error' : '' }}">
							<label for="rbr">{{ AdminLanguage::transAdmin('Redni broj') }}</label>
							<input type="text" name="rbr" value="" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>

						<div class="columns">
							<label>
								<input name="aktivna" type="checkbox" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}> {{ AdminLanguage::transAdmin('Aktivna') }}
							</label>

							<label>
								<input name="prikazi_vrednost" type="checkbox" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}> {{ AdminLanguage::transAdmin('Prikaži nazive vrednosti u prodavnici') }}
							</label>

							<label>
								<input name="color_picker" type="checkbox" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}> {{ AdminLanguage::transAdmin('Odabir boje') }}
							</label>
						</div>
					</div>

					@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))
					<div class=" btn-container center">
						<button type="submit" class="btn-edit-simple save-it-btn" value="">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
					</div>
					@endif
				</form>
				@endif  
			</div>
		</section>

		@if($osobina_naziv_id != 0)
		<section class="medium-4 columns">
			<div class="flat-box">
				<h3 class="title-med">{{ AdminLanguage::transAdmin('Nova vrednost') }}</h3>
				<form action="{{ AdminOptions::base_url() }}admin/osobine/{{$osobina_naziv_id}}/0" method="POST">
					
					<div class="row">

						<div class="columns {{ $errors->first('vrednost') ? ' error' : '' }}">
							<label for="vrednost">{{ AdminLanguage::transAdmin('Naziv') }}</label>	
							<input type="text" name="vrednost" value="" placeholder="{{ AdminLanguage::transAdmin('Naziv') }}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>

						<div class="columns medium-12 flex-me">

							<label> 
								<input name="aktivna" type="checkbox" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}>{{ AdminLanguage::transAdmin('Aktivno') }}
							</label>

							@if($color_picker_active == 1)
							<span class="flex-me"> 
								<label for="boja_css">{{ AdminLanguage::transAdmin('Izaberite boju') }}</label>
								<input type="color" class="inp-color" name="boja_css" value="#888888" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}>
							</span>
							@endif
						</div> 
					</div>

					@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))
					<div class="text-center btn-container">
						<button type="submit" class="btn-edit-simple save-it-btn" value=""> {{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
					</div>	
					@endif
				</form>
			</div>

			<ul @if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE'))) class="banner-list name-ul" id="JSListOsobine" @endif>
				@foreach($vrednosti as $row)
				<div class="flat-box" id="{{$row->osobina_vrednost_id}}">
					<form action="{{ AdminOptions::base_url() }}admin/osobine/{{$row->osobina_naziv_id}}/{{$row->osobina_vrednost_id}}" method="POST">
						<div id="{{$row->osobina_vrednost_id}}" class="row">

							<div class="columns medium-6">
					<!-- 			@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))
								<span class="sortableKar-span"> 
									<i class="fa fa-arrows-v tooltipz" aria-label="{{ AdminLanguage::transAdmin('Prevucite da bi sortirali') }}"></i>
								</span>
								@endif -->
								<label for="vrednost">{{ AdminLanguage::transAdmin('Naziv') }}</label>	 
								<input type="text" name="vrednost" value="{{$row->vrednost}}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}>
							</div>

							<div class="columns medium-6">
								<label> 
									<input name="aktivna" type="checkbox" @if($row->aktivna == 1) {{ 'checked' }} @endif {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}> 
									{{ AdminLanguage::transAdmin('Aktivno') }}
								</label>
								
								@if($color_picker_active == 1)
								<span class="flex-me"> 
									<label for="boja_css">{{ AdminLanguage::transAdmin('Izaberite boju') }}</label>
									<input type="color" class="inp-color" name="boja_css" value="{{$row->boja_css}}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}>
								</span>
								@endif
							</div> 
						</div>

						@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))
						<div class="center btn-container">
							<button type="submit" class="btn-edit-simple save-it-btn" value="">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
							<button type="button" class="btn btn-danger JSbtn-delete" data-link="{{ AdminOptions::base_url() }}admin/osobine/{{$row->osobina_naziv_id}}/{{$row->osobina_vrednost_id}}/delete">{{ AdminLanguage::transAdmin('Obriši') }}</button>
						</div>	
						@endif
					</form>
				</div>
				@endforeach
			</ul>
		</section>
		@endif
	</div>
</section>





<section id="main-content" class="banners">
	<section class="medium-12 large-3 columns">
		<div class="flat-box">
			
			<!-- BANNER LIST -->
			<h3 class="text-center h3-margin">Baneri</h3>
			<ul class="banner-list JSListaBaner" id="banner-sortable" data-table="2">
				<li id="0" class="new-banner new-elem">
					<a href="{{AdminOptions::base_url()}}admin/baneri-slajdovi/new-banner">Novi baner</a>
					<div class="icon"><i class="fa fa-plus" aria-hidden="true"></i></div>
				</li>
				@foreach($query_banner_list as $bann)
				<li id="{{$bann->baneri_id}}" class="ui-state-default @if($bann->baneri_id == $stranica) active @endif" id="{{$bann->baneri_id}}">
					<a href="{{AdminOptions::base_url()}}admin/baneri-slajdovi/{{$bann->baneri_id}}">{{$bann->naziv}}</a>
					<div class="delete icon tooltipz" aria-label="Obriši" data-id="{{$bann->baneri_id}}"><i class="fa fa-times" aria-hidden="true"></i></div>
				</li>
				@endforeach
			</ul>
			
			<!-- SLIDE LIST -->		
			<h3 class="text-center h3-margin">Slajderi</h3>
			<ul class="slide-list JSListaSlider" id="slide-sortable" data-table="2">
				<li id="0" class="new-slide new-elem">
					<a href="{{AdminOptions::base_url()}}admin/baneri-slajdovi/new-slider">Novi slajd
					</a>
					<div class="icon"><i class="fa fa-plus" aria-hidden="true"></i></div>
				</li>
				@foreach($query_slider_list as $slid)
					<li id="{{$slid->baneri_id}}" class="ui-state-default @if($slid->baneri_id == $stranica) active @endif"><a href="{{AdminOptions::base_url()}}admin/baneri-slajdovi/{{$slid->baneri_id}}">{{$slid->naziv}}</a>
					<div class="delete icon tooltipz" aria-label="Obriši" data-id="{{$slid->baneri_id}}"><i class="fa fa-times" aria-hidden="true"></i></div></li>
				@endforeach
			</ul>
		</div> <!-- end of .flat-box -->
		
		<!-- BACKGROUND IMAGE -->
		<div class="flat-box">
			<h3 class="text-center h3-margin">Pozadinska slika</h3>
			<ul class="">
				<form action="{{AdminOptions::base_url()}}admin/bg-img-edit" method="POST" enctype="multipart/form-data">
					<input type="file" name="bgImg" value="Izaberi sliku">
					
					@if(AdminSupport::getBGimg() == null)
					<img src="{{AdminOptions::base_url()}}/images/no-image.jpg" alt="" class="bg-img">
					@else
					<img src="{{AdminOptions::base_url()}}{{ AdminSupport::getBGimg() }}" alt="" class="bg-img">
					@endif
					<h3 class="text-center h3-margin">Preporučene dimenzije slike: 1920x1080</h3>
					<br>
					<!-- <label for="aktivna">
						<input type="checkbox" name="aktivna" value=""> Aktivna slika
					</label> -->
					<input type="hidden" value="3" name="flag">
					<div class="btn-container center">
						<button type="submit" class="btn btn-primary save-it-btn">Sačuvaj</button>
						<a class="btn btn-danger" href="{{AdminOptions::base_url()}}admin/bg-img-delete">Obriši</a>
					</div>
				</form>
			</ul>
		</div>
	 
	</section> <!-- end of .medium-3 .columns -->
	
	<!-- BANNER EDIT -->
	<section class="page-edit medium-12 large-9 columns">
		<div class="flat-box"> 
		<form action="{{AdminOptions::base_url()}}admin/banner_uload" name="banner_upload" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="logo-right">
			<h3 class="text-center h3-margin">{{$label['naslov']}}</h3>
			
			<!-- BANNER EDIT TOP -->
			<section class="banner-edit-top row">
				<div class="medium-6 columns">
					<label>{{$label['naziv']}}</label>
					<input id="naziv" onchange="check_fileds('naziv')" value="{{$row['naziv']}}" type="text" name="naziv" />
					<div class="banner-upload-area clearfix">
						<label>{{$label['img']}}</label>
						<span class="banner-upload has-tooltip"><input type="file" name="img"><span>Dodaj sliku</span></span>
					</div>
				</div>
				
				<div class="medium-6 columns">
					<label>{{$label['link']}}</label>
					<input id="link" value="{{$row['link']}}" type="text"  onchange="check_fileds('link')" name="link">
				</div>
			</section>
			
			<!-- BANNER PREVIEW -->
			<section class="banner-preview-area">
				<img class="banner-preview" src="{{AdminOptions::base_url()}}{{$row['img']}}" alt="{{$row['naziv']}}" />
			</section>

			<!-- BANNER DISPLAY PAGES -->			
			<div class="banner-display-pages">
				@if($row['tip_prikaza']==1)
		<!-- 		<label>Prikaži na stranama :</label>
				
				<ul class="clearfix">
					@foreach($query_stranice as $str)
						@if($str->web_b2c_seo_id == 1)
					<?php //if(in_array($str->web_b2c_seo_id,$stranice)) {?>
					<li>
						<label class="checkbox-label">
							<input type="checkbox" name="stranice[]" checked="" value="{{$str->web_b2c_seo_id}}" >
							<span class="label-text">{{$str->title}}</span>
						</label>
					</li>
					<?php //}else { ?>
					<li>
						<label class="checkbox-label">
							<input type="checkbox" name="stranice[]" value="{{$str->web_b2c_seo_id}}" >
							<span class="label-text">{{$str->title}}</span>
						</label>
					</li>
					<?php //} ?>
					@endif
					@endforeach
					
				</ul> -->
				@endif
					<input type="hidden" name="tip_prikaza" value="{{$row['tip_prikaza']}}" />
					<input type="hidden" name="flag" value="{{$row['flag']}}" />
				@if($row['flag']==2)
					<input type="hidden" name="baneri_id" value="{{$row['baneri_id']}}" />
				@else
					<input type="hidden" name="baneri_id" value="0" />
				@endif
				<div class="btn-container center">
					<a href="#" class="banner-edit-save btn btn-primary save-it-btn">{{$label['dugme']}}</a>
				</div>
			</div>
		</form>
		</div>
	 	<!-- ====================== -->
		<div class="text-center">
			<a href="#" class="video-manual" data-reveal-id="banners-manual">Uputstvo <i class="fa fa-film"></i></a>
		   	<div id="banners-manual" class="reveal-modal medium" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
				<div class="video-manual-container"> 
				<p><span class="video-manual-title">Baneri i slajder</span></p> 
				<iframe src="https://player.vimeo.com/video/271254080" width="840" height="426" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
				<a class="close-reveal-modal" aria-label="Close">&#215;</a>
			</div>
		 </div>
		</div>
		<!-- ====================== -->
	</section>
</section>

 
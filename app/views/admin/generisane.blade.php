<div id="main-content" class="">
@include('admin/partials/product-tabs')	
	@include('admin/partials/karak-tabs')

	<input id="roba_id_obj" type="hidden" value="{{ $roba_id }}">

	<div class="row">
		<div class="columns medium-8 medium-centered">
			<div class="flat-box">
				<div class="row">
					<h3 class="title-med">{{ AdminLanguage::transAdmin('Naziv karakteristike') }}</h3>
					<div class="columns medium-12 field-group">
						 
						<div class="m-input-and-button">
							 <select id="grupa_pr_naziv_id" class="m-input-and-button__input">
								@foreach($nazivi as $row)
								<option value="{{ $row->grupa_pr_naziv_id }}">{{ $row->naziv }}</option>
								@endforeach
							</select>
							@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
								<button id="add_karak" class="btn btn-primary btn-small">{{ AdminLanguage::transAdmin('DODAJ') }}</button>
								<button id="add_all_karak" class="btn btn-secondary btn-small">{{ AdminLanguage::transAdmin('DODAJ SVE') }}</button>
								<a class="btn btn-primary btn-small" href="{{AdminOptions::base_url()}}admin/grupe/{{ AdminArticles::find($roba_id, 'grupa_pr_id') }}" target="_blank">{{ AdminLanguage::transAdmin('DODAJ NOVE NAZIVE') }}</a>
							@endif
						</div>
					 </div>		
				</div>
			</div> <!-- end of .flat-box -->


			<div class="flat-box">
				<h2 class="title-med">{{ AdminLanguage::transAdmin('Karakteristike') }}</h2>
				<div class="row"> 
				    <div class="column medium-12"> 
						<table data-id="{{ $roba_id }}" role="grid" class="karakteristike-tabela">
							@foreach(AdminSupport::getGenerisane($roba_id) as $row)
								<tr class="inline-list">
									
						<td class="rbr">
			<!-- 				<span class="JSRbrVrednost">
								{{ $row->rbr }} 
							</span> -->
							<!-- <span> -->
								<input type="text" style="width:35px" class="JSRbrInput rbr_gener center" data-id="{{ $row->web_roba_karakteristike_id }}" value="{{ $row->rbr }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
							<!-- 	<i class="fa fa-pencil JSEditRbrBtn" aria-hidden="true"></i> -->
							<!-- </span> -->
						</td>
									<td> 
										<div class="row"> 
										 	<div class="columns medium-8"> 
										 		<div class="row"> 
										 			<div class="columns medium-4 no-padd"> 
												 		<span>{{ $row->naziv }}</span>
												 	</div>
												 	<div class="columns medium-4"> 
												 		<div class="relative"> 
															<select class="save-gener" data-id="{{ $row->grupa_pr_naziv_id }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}>
																@foreach(AdminSupport::getVrednostKarak($row->grupa_pr_naziv_id) as $row1)
																<option value="{{ $row1->grupa_pr_vrednost_id }}" <?php echo $row->grupa_pr_vrednost_id == $row1->grupa_pr_vrednost_id ? 'selected' : ''  ?>>{{ $row1->naziv }}</option>
																@endforeach
															</select>
															@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
																<button class="delete-gener abs-button btn btn btn-danger m-input-and-button__btn no-margin" data-id="{{ $row->grupa_pr_naziv_id }}"><i class="fa fa-close" aria-hidden="true"></i></button>
															@endif
														</div>
													</div>
													<div class="columns medium-4 no-padd"> 
												 		<span>{{ AdminSupport::getBojaKarak($row->grupa_pr_vrednost_id) }}</span>
												 	</div>
												</div>
											</div>

											<div class="columns medium-4">  
												<div class="relative"> 
													<input placeholder="{{ AdminLanguage::transAdmin('Nova vrednost') }}" type="text" name="naziv" class="JSnaziv" autofocus="autofocus" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}> 
													@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
														<button class="save-gener-value abs-button m-input-and-button__btn btn btn-primary no-margin" data-id="{{ $row->grupa_pr_naziv_id }}" data-vrednost="{{ $row->grupa_pr_vrednost_id }}" class="btn btn-icon btn-small-check tooltipz" aria-label="{{ AdminLanguage::transAdmin('Dodeli') }}"><i class="fa fa-check" aria-hidden="true"></i></button> 
													@endif
												</div>
											</div>
										</div> 
									</td> 
								</tr>
							@endforeach
						</table>
					</div>
				</div>
			</div> <!-- end of .flat-box -->
		</div>
	</div> <!-- end of .row -->
</div>
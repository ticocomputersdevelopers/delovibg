<section class="collapse articles-page" id="main-content">

<div class="column large-2 medium-2">
	<ul class="crm-menu">
		<li><a href="#">{{ AdminLanguage::transAdmin('Zadaci') }}</a></li>
		<li><a href="#">{{ AdminLanguage::transAdmin('Kontakti') }}</a></li>
		<li><a href="#">{{ AdminLanguage::transAdmin('Komunikacija') }}</a></li>
		<li><a href="#">{{ AdminLanguage::transAdmin('Ponude') }}</a></li>
	</ul>
</div>
<div class="column large-10 medium-10">
	<div class="row crm-head">
		<div class="column large-10 medium-10">
			<h1 class="crm-title">{{ AdminLanguage::transAdmin('Zadaci') }}</h1>
		</div>
		<div class="column large-2 medium-2">
			<a href="#" class="btn btn-primary crm-add">+ {{ AdminLanguage::transAdmin('Dodaj') }}</a>

		</div>

	</div>
	
</div>
</section>
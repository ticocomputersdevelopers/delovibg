@extends('adminb2b.defaultlayout')
@section('content')
<section class="collapse" id="main-content">

@include('adminb2b.partials.partner_rabat_tabs')

<div class="row small-gutter art-row">
	<div class="row">
		<div class="medium-3 columns cat-list">
			@include('adminb2b.partials.kategorije')
		</div>
		<div class="medium-9 columns wide-b2b-art">
			 
			<!-- <div class="column medium-5 large-5 bw">
				<div class="">
				<h3 class="text-center">Filtriranje</h3>
				<div class="columns medium-6 large-6">	
					<table class="filter-table">
						<tr>
							<th>Y</th>
							<th>N</th>
							<th></th>
						</tr>
						<tr>
							<td><input class="filter-check" type="checkbox" data-check="1"></td>
							<td><input class="filter-check" type="checkbox" data-check="2"></td>
							<td>Lager (naš)</td>
						</tr>
						<tr>
							<td><input class="filter-check" type="checkbox" data-check="11"></td>
							<td><input class="filter-check" type="checkbox" data-check="12"></td>
							<td>Lager dobavljaca</td>
						</tr>
						<tr>
							<td><input class="filter-check" type="checkbox" data-check="3"></td>
							<td><input class="filter-check" type="checkbox" data-check="4"></td>
							<td>Akcija</td>
						</tr>
						<tr>
							<td><input class="filter-check" type="checkbox" data-check="5"></td>
							<td><input class="filter-check" type="checkbox" data-check="6"></td>
							<td>Zaključan</td>
						</tr>
					</table>
				</div>
				<div class="column medium-6 large-6">
					<table class="filter-table">
						<tr>
							<th>Y</th>
							<th>N</th>
							<th></th>
						</tr>
						<tr>
							<td><input class="filter-check" type="checkbox" data-check="9"></td>
							<td><input class="filter-check" type="checkbox" data-check="10"></td>
							<td>Na web-u</td>
						</tr>
						<tr>
							<td><input class="filter-check" type="checkbox" data-check="7"></td>
							<td><input class="filter-check" type="checkbox" data-check="8"></td>
							<td>Aktivan</td>
						</tr>
					</table>
				-->
	 
	<div class="flat-box"> 
		 <div class="btn-container clearfix"> 
			<div class="column medium-4">
				<div class="b2b-nc-options row">
					<div class="columns medium-4"> 
						<input type="number" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 || event.keyCode === 190 || event.keyCode === 37 || event.keyCode === 39 ? true : !isNaN(Number(event.key))" id="JSCenaOd" class="" placeholder="NC od">
					</div>
					<div class="columns medium-4"> 
						<input type="number" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 || event.keyCode === 190 || event.keyCode === 37 || event.keyCode === 39 ? true : !isNaN(Number(event.key))" id="JSCenaDo" class="" placeholder="NC do">
					</div>
					<div class="column medium-4">  
						<a href="#" id="JSCenaSearch" class="btn btn-primary">
							<i class="fa fa-search" aria-hidden="true"></i>
						</a>
						<a href="#" id="JSCenaClear" class="btn btn-danger ">
							<i class="fa fa-times" aria-hidden="true"></i>
						</a>
					</div>
				</div>	
			</div>
			@if(Admin_model::check_admin(array('B2B_RABAT_AZURIRANJE')))
				<div class="column medium-8">	 
					<div class="row b2b-nc-options b2b-btn-cont">
						<div class="column medium-4">
							<div class="relative"> 
								<input type="number" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 || event.keyCode === 190 || event.keyCode === 37 || event.keyCode === 39 ? true : !isNaN(Number(event.key))" name="rabat_group_edit" class="rabat_group_edit" placeholder="Dodeli maksimalini rabat">
								<button class="btn btn-secondary" id="rabat_group_edit-btn" value=""><i class="fa fa-check" aria-hidden="true"></i></button>
							</div>
						</div>
						<div class="column medium-4">
							<div class="relative"> 
								<input type="number" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 || event.keyCode === 190 || event.keyCode === 37 || event.keyCode === 39 ? true : !isNaN(Number(event.key))" name="akc_rabat_group_edit" class="akc_rabat_group_edit" placeholder="Dodeli akcijski rabat">
								<button class="btn btn-secondary" id="akc_rabat_group_edit-btn" value=""><i class="fa fa-check" aria-hidden="true"></i></button>
							</div>
						</div>
						<div class="column medium-4"> 
							<button class="btn btn-secondary" id="all" value="">Izaberi sve (F3)</button>
						</div>
					</div>
				</div>
			@endif
		</div>
	</div>
	 
<div class="flat-box"> 
	<div class="btn-container"> 
		<div class="row b2b-article-list">
			<div class="column medium-5 order-filters text-right">
				<div class="row"> 
					<div class="column medium-7"> 
						<input type="text" class="search-field m-input-and-button__input" id="search" autocomplete="off" placeholder="Pretraži...">
					</div>
				    <div class="column medium-4 text-left"> 
						<input type="submit" id="search-btn" value="Pronađi" class="btn btn-primary btn-small m-input-and-button__btn">
						<input type="submit" id="clear-btn" value="Poništi" class="btn btn-danger m-input-and-button__btn">
					</div>
				</div>
			</div>  
				<div class="column medium-2">
				<div class="form-bottom-btn relative lineh">
					<select class="admin-select m-input-and-button__input" id="dobavljac_select" multiple="multiple" {{ Admin_model::check_admin(array('B2B_RABAT_AZURIRANJE')) == false ? 'disabled' : '' }}>
						@foreach(AdminB2BSupport::getDobavljaci() as $dobavljac)
							@if( in_array($dobavljac->partner_id ,explode("-",$criteria['dobavljac'])) )
								<option value="{{ $dobavljac->partner_id }}" selected>{{ $dobavljac->naziv }}</option>
							@else
								<option value="{{ $dobavljac->partner_id }}">{{ $dobavljac->naziv }}</option>
							@endif
						@endforeach
					</select>
					@if(Admin_model::check_admin(array('B2B_RABAT_AZURIRANJE')))
						<button class="btn btn-primary dobavljac_proizvodjac "><i class="fa fa-check" aria-hidden="true"></i></button>
					@endif
				</div>
			</div>
			
			<div class="column medium-2">
				<div class="form-bottom-btn relative lineh">
						<select class=" admin-select m-input-and-button__input" id="proizvodjac_select" multiple="multiple" {{ Admin_model::check_admin(array('B2B_RABAT_AZURIRANJE')) == false ? 'disabled' : '' }}>
						@foreach(AdminB2BSupport::getProizvodjaci() as $proizvodjac)
							@if( in_array($proizvodjac->proizvodjac_id ,explode("-",$criteria['proizvodjac'])) )
							<option value="{{ $proizvodjac->proizvodjac_id }}" selected>{{ $proizvodjac->naziv }}</option>
							@else
							<option value="{{ $proizvodjac->proizvodjac_id }}">{{ $proizvodjac->naziv }}</option>
							@endif
						@endforeach
					</select>
					@if(Admin_model::check_admin(array('B2B_RABAT_AZURIRANJE')))
						<button class="btn btn-primary dobavljac_proizvodjac "><i class="fa fa-check" aria-hidden="true"></i></button>
					@endif
				</div>
			</div>
			<div class="column medium-2">
				<div class="form-bottom-btn relative lineh">
					<select class=" admin-select m-input-and-button__input" id="magacin_select" {{ Admin_model::check_admin(array('B2B_RABAT_AZURIRANJE')) == false ? 'disabled' : '' }}>
						<option value="0">Svi magacini</option>
						@foreach(DB::table('orgj')->where('b2b',1)->orderBy('orgj_id','asc')->get() as $row)
							@if( in_array($row->orgj_id ,explode("-",$criteria['magacin'])) )
							<option value="{{ $row->orgj_id }}" selected>{{ $row->naziv }}</option>
							@else
							<option value="{{ $row->orgj_id }}">{{ $row->naziv }}</option>
							@endif
						@endforeach
					</select>
					@if(Admin_model::check_admin(array('B2B_RABAT_AZURIRANJE')))
						<button class="btn btn-primary dobavljac_proizvodjac "><i class="fa fa-check" aria-hidden="true"></i></button>
					@endif
				</div>
			</div>
		</div>
	</div>
 </div>
<div class="flat-box">  
	<div class="btn-container"> 
 		<div class="row">
			<div class="total-article-count columns medium-3">
				@if(isset($count_products))
					<small>Ukupno artikala: <b>{{$count_products}}</b></small> &nbsp;&nbsp;&nbsp;&nbsp;
				@endif
				<small>Po strani: <b>{{$limit}}</b></small>
			</div> <!-- end of .total-article-count -->

			<div class="columns medium-9 pagination-parent-margin-bottom">
				{{ Paginator::make($articles, $count_products, $limit)->links() }}
			</div>
		</div>
	</div> <!-- end of .row -->

	<div class="table-scroll">
		<table class="article-artiacle-table articles-page-tab" style="width: 100%;">
			<thead>
				<tr class="st order-list-titles row">
					<th class="table-head" data-coll="r.sifra_is" data-sort="DESC"><a href="#">ID</a> </th> <!--
					<th class="table-head" data-coll="flag_prikazi_u_cenovniku" data-sort="DESC"><a href="#">WEB</a></th>
					<th class="table-head" data-coll="akcija_flag_primeni" data-sort="DESC"><a href="#">AKCIJA</a></th> -->
					<th class="table-head" data-coll="naziv" data-sort="DESC"><a href="#">NAZIV </a></th>
				<!-- 	<th class="table-head" data-coll="kolicina" data-sort="DESC"><a href="#">LAGER </a></th> -->
					<!-- <th class="table-head" data-coll="web_cena" data-sort="DESC"><a href="#">WEB CENA </a></th> -->
					<th class="table-head" data-coll="rabat" data-sort="DESC"><a href="#">MAKSIMALNI RABAT </a></th>
					<th class="table-head" data-coil="akc_rabat" data-sort="DESC"><a href="#">AKCIJSKI RABAT</a> </th>
					<!-- <th class="table-head" data-coll="kolicina" data-sort="DESC"><a href="#">PDV </a></th>
					<th class="table-head" data-coll="sifra_d" data-sort="DESC"><a href="#">NC</a> </th>
					<th class="table-head" data-coll="dobavljac_id" data-sort="DESC"><a href="#">DOB. </a></th>
					<th class="table-head" data-coll="kolicina" data-sort="DESC"><a href="#">LAGER DOB. </a></th>
					<th class="table-head" data-coll="flag_zakljucan" data-sort="DESC"><a href="#">ZKLJ</a></th>
					<th class="table-head" data-coll="proizvodjac_id" data-sort="DESC"><a href="#">PRO. </a></th>
					<th class="table-head" data-coll="flag_aktivan" data-sort="DESC"><a href="#">AKT</a></th> -->	
				</tr>
			</thead>
			<tbody @if(Admin_model::check_admin(array('B2B_RABAT_AZURIRANJE'))) id="selectable" @endif>
				@foreach($articles as $row)
				<tr class="admin-list order-list-item row ui-widget-content {{$row->flag_aktivan == 1 ? '' : 'text-red'}}" data-id="{{ $row->roba_id }}">
					<td class=""><span>{{ $row->sifra_is }}</span></td> <!--
					<td class="flag_prikazi_u_cenovniku">{{ $row->flag_prikazi_u_cenovniku == 1 ? 'DA' : 'NE' }}</td>
					<td class="akcija_flag_primeni">{{ $row->akcija_flag_primeni == 1 ? 'DA' : 'NE' }}</td> -->
					<td class="naziv-art"><span>{{ $row->naziv }}</span></td>
					<!-- <td class="kolicina">{{ isset($row->kolicina) ? intval($row->kolicina) : '' }}</td> -->
					<!-- <td class="web_cena">{{ $row->web_cena }}</td> -->
					<td class="rabat"><span>{{ $row->b2b_max_rabat }}</span></td>
					<td class="akcrabat"><span>{{ $row->b2b_akcijski_rabat }}</span></td>
					<!-- <td class="tarifna_grupa">{{ AdminB2BSupport::find_tarifna_grupa($row->tarifna_grupa_id,'porez') }}</td>
					<td class="">{{ $row->racunska_cena_nc }}</td>					
					<td class="">{{ AdminB2BSupport::dobavljac($row->dobavljac_id) }}</td>
					<td class="">{{ AdminB2BSupport::lagerDob($row->roba_id) }}</td>
					<td class="flag_zakljucan">{{ $row->flag_zakljucan == 1 ? 'DA' : 'NE' }}</td>
					<td class="proizvodjac_id">{{ AdminB2BCommon::get_manofacture_name($row->proizvodjac_id) }}</td>
					<td class="flag_aktivan">{{ $row->flag_aktivan == 1 ? 'DA' : 'NE' }}</td>	 -->
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	
	<div class="row">
		{{ Paginator::make($articles, $count_products, $limit)->links() }}
	</div>
</div>

		</div> <!-- end of .row -->
	</div> <!-- end of .flat-box -->
	</div>
</div> <!-- end of .row -->


<!-- MODAL END -->
<div id="wait"><img class="loader" src="{{ AdminB2BOptions::base_url()}}images/admin/wheel.gif"> <p>Učitava se... Molim vas sačekajte.</p></div>



<!--  articles criteria -->   
<?php if(isset($criteria)){ ?><script> var criteria = new Array("{{ $criteria['grupa_pr_id'] ? $criteria['grupa_pr_id'] : 0 }}", "{{ isset($criteria['proizvodjac']) ? $criteria['proizvodjac'] : 0 }}", 
"{{ isset($criteria['dobavljac']) ? $criteria['dobavljac'] : 0 }}", "{{ isset($criteria['magacin']) ? $criteria['magacin'] : 0 }}", "{{ isset($criteria['filteri']) ? $criteria['filteri'] : 'nn-nn-nn-nn-nn-nn' }}", "{{ isset($criteria['search']) ? $criteria['search'] : 0 }}", "{{ isset($criteria['nabavna']) ? $criteria['nabavna'] : 'nn-nn' }}"); </script><?php }else{ ?><script> var criteria; </script><?php } ?>


</section>
@endsection



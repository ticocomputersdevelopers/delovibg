<!DOCTYPE html>
<html>

<title>{{ $seo['title'] }} | {{B2bOptions::company_name() }}</title>

<meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="{{ $seo['description'] }}" />
<meta name="author" content="{{$seo['keywords']}}" />
<meta name="viewport" content="width=device-width, initial-scale=1" />

<link rel="icon" type="image/png" href="{{ B2bOptions::base_url()}}favicon.ico">
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
<link href="{{ B2bOptions::base_url()}}css/foundation.min.css" rel="stylesheet" type="text/css" />
<link href="{{ B2bOptions::base_url()}}css/normalize.css" rel="stylesheet" type="text/css" />
<link href="{{ B2bOptions::base_url()}}css/fancybox.css" rel="stylesheet" type="text/css" />
<link href="{{ B2bOptions::base_url()}}css/b2b/style.css" rel="stylesheet" type="text/css" />
<!-- <link href="{{ B2bOptions::base_url()}}css/b2b/custom.css" rel="stylesheet" type="text/css" /> -->
<link href="{{ B2bOptions::base_url()}}css/b2b/b2bstyle.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

@yield('head')
</head>

<body>
 
<div id="main-wrapper">
   <!-- PREHEADER -->
    @include('b2b.menu_top')
<header class="row">
    <div class="row black-bg">
        <div class="medium-12 columns inner-black-bg"> 
            <div class="medium-3 small-12 columns border-right-green"> 
                <span class="opis">Promena valute</span><br>
                @if(Session::get('valuta') == 1) 
                <a href="#" class="menu-top-links" id="valuta_eur">EUR</a> | <a id="valuta_din" class="menu-top-links active" href="#">RSD</a>
                @elseif(Session::get('valuta') == 2) 
                <a href="#" class="menu-top-links active" id="valuta_eur">EUR</a> | <a id="valuta_din" class="menu-top-links" href="#">RSD</a>
                @else
                <a href="#" class="menu-top-links" id="valuta_eur">EUR</a> | <a id="valuta_din" class="menu-top-links active" href="#">RSD</a>
                @endif
            </div>
            @if(Session::has('valuta') and Session::get('valuta') != 1)
            <div class="medium-3 small-12 columns">
                <span class="opis">Kurs</span><br>
                <span class="numbers">{{ B2b::kurs() }}</span>
            </div>
            @endif
            <div class="medium-3 small-12 columns border-left end">  
                <div class="header-cart">
                    <a href="{{ B2bOptions::base_url() }}b2b/{{ B2bCommon::get_korpa() }}">
                        <span class="opis">Sadržaj korpe ({{ B2bBasket::b2bCountItems() }})</span><br>
                        <span><i class="fa fa-shopping-cart numbers"></i> &nbsp; <span id="header_total_amount_show" class="numbers">{{ B2bBasket::cena(B2bBasket::total()->ukupna_cena) }}</span></span>
                    </a>
                    @include('b2b.cart_top')
                </div>
            </div>
        </div>
    </div>
    <!-- LOGO AREA -->
    <div class="row div-in-header"> 
        <section class="text-center small-only-text-left medium-3 columns">
            <a class="logo" href="{{ B2bOptions::base_url()}}b2b" title="{{B2bOptions::company_name()}}">
                <img src="{{ B2bOptions::base_url()}}{{B2bOptions::company_logo()}}" alt="{{B2bOptions::company_name()}}" />
            </a>
        </section>
        <!-- SEARCH AREA -->
        <section class="medium-6 columns left"><div class="search">
            <form action="{{route('b2b.search')}}" method="GET">
                <input class="search-field" type="text" id="search" name="q" value="{{Input::get('q')?Input::get('q'):''}}" placeholder="Pretraga" />
                <button  class="search-button"> <i class="fa fa-search"></i> </button>
            </form>
        </section>
        <div class="menu-icon"><i class="fa fa-bars" aria-hidden="true"></i></div>
    </div>
</header>
        @if(B2bOptions::category_view()==0)
        @include('b2b.categories_horizontal')
        @endif
    <!-- MIDDLE AREA -->

    <section id="middle-area" class="row">
        @if(Route::getCurrentRoute()->getName()=='products_first' or  Route::getCurrentRoute()->getName()=='products_second' or Route::getCurrentRoute()->getName() == 'products_third' or Route::getCurrentRoute()->getName() == 'products_fourth' or Route::getCurrentRoute()->getName()=='b2b.search')

        @yield('breadcrumbs')
        @yield('filters')

        @else
        <aside id="sidebar-left" class="large-3 medium-3 columns">
            @if(B2bOptions::category_view()==1)
            @include('b2b.categories')
            @endif
            @yield('breadcrumbs')
        </aside>

        @endif
        <section id="main-content" class="large-9 medium-9 columns">
            @yield('content')
        </section>
    </section>

    <!-- FOOTER -->
    @include('b2b.footer')
</div>
<!-- LOGIN POPUP -->
 
@yield('footer')

<script>
    function deleteItemCart(cart_id){
        $.ajax({

            type: "POST",
            url:'{{route('b2b.cart_delete')}}',
            cache: false,
            data:{cart_id:cart_id},
            success:function(res){

                location.reload();

            }

        });
    }
</script>

<input type="hidden" id="base_url" value="{{ AdminB2BOptions::base_url() }}">
</body>
</html>
<form class="filter-form" method="get" action="">
    <ul>
        <li>
            <a href="javascript:void(0)" class="filter-links center-block clearfix dropdown-toggle" data-toggle="dropdown">Proizvođač 
                <i class="fa fa-angle-down pull-right"></i>
            </a>
            <ul class="select-fields-content dropdown-menu" role="menu">
                @foreach(B2b::getManufacturers($grupa_pr_id,$filters) as $row)
                    @if(isset($filters['proizvodjac']))
                        @if(in_array($row->proizvodjac_id,$filters['proizvodjac']))
                            <li class="center-block clearfix">
                               
                                <input type="checkbox" data-name="proizvodjac" data-type="remove" data-old-value="{{implode('-',$filters['proizvodjac'])}}"  class="filter-item" value="{{ $row->proizvodjac_id }}" checked> {{$row->naziv}} 
                               
                                <span class="pull-right">{{$row->products}}</span>
                            </li>
                        @else
                            @if( $filters['proizvodjac'][0]!=null )
                                <li class="center-block clearfix">
                                  
                                    <input type="checkbox"  data-name="proizvodjac" data-type="add" data-old-value="{{implode('-',$filters['proizvodjac'])}}" class="filter-item" value="{{ implode('-',$filters['proizvodjac']+[''=>$row->proizvodjac_id]) }}"> {{$row->naziv}}
                                   
                                    <span class="pull-right">{{$row->products}}</span>
                                </li>
                            @else
                                <li class="center-block clearfix">
                                 
                                    <input type="checkbox"  data-name="proizvodjac" data-type="add" data-old-value="{{implode('-',$filters['proizvodjac'])}}"  class="filter-item" value="{{ $row->proizvodjac_id }}"> {{$row->naziv}} 
                                  
                                    <span class="pull-right">{{$row->products}}</span>
                                </li>
                            @endif
                        @endif
                        @else
                            <li class="center-block clearfix">
                              
                                <input type="checkbox"  data-name="proizvodjac" data-type="add" data-old-value=""  class="filter-item" value="{{ $row->proizvodjac_id }}"> {{$row->naziv}}
                               
                                <span class="pull-right">{{$row->products}}</span>
                            </li>
                     @endif
                 @endforeach
            </ul>
        </li>
    @foreach($filtersItems as $filter)
        <li>
            <a href="javascript:void(0)" class="filter-links center-block clearfix">{{$filter['grupa_naziv']}}
                <i class="fa fa-angle-down pull-right"></i>
            </a>
            <div class="select-fields-content">
                @foreach($filter['grupa_pr_vrednost'] as $row)
                    @if(isset($filters[$filter['grupa_naziv_var']]))
                        @if(in_array($row['vrednost_id'],$filters[$filter['grupa_naziv_var']]))
                        <label class="center-block clearfix">
                           
                            <input type="checkbox" data-name="{{$filter['grupa_naziv_var']}}" data-type="remove" data-old-value="{{implode('-',$filters[$filter['grupa_naziv_var']])}}"  class="filter-item" value="{{ $row['vrednost_id'] }}" checked> {{$row['vrednost_naziv']}} 
                            
                            <span class="pull-right">{{$row['broj_proizvoda']}}</span>
                        </label>
                        @else
                        @if( $filters[$filter['grupa_naziv_var']][0]!=null )
                        <label class="center-block clearfix">
                           
                            <input type="checkbox"  data-name="{{$filter['grupa_naziv_var']}}" data-type="add" data-old-value="{{implode('-',$filters[$filter['grupa_naziv_var']])}}"  class="filter-item" value="{{ implode('-',$filters[$filter['grupa_naziv_var']]+[''=>$row['vrednost_id']]) }}"> {{$row['vrednost_naziv']}} 
                            
                            <span> {{$row['broj_proizvoda']}}</span>
                        </label>
                        @else
                        <label class="center-block clearfix">
                           
                            <input type="checkbox"  data-name="{{$filter['grupa_naziv_var']}}" data-type="add" data-old-value="{{implode('-',$filters[$filter['grupa_naziv_var']])}}"  class="filter-item" value="{{ $row['vrednost_id'] }}"> {{$row['vrednost_naziv']}}
                           
                            <span class="pull-right">{{$row['broj_proizvoda']}} </span>
                        </label>
                        @endif
                    @endif
                    @else
                    <label class="center-block clearfix">
                       
                        <input type="checkbox"  data-name="{{$filter['grupa_naziv_var']}}" data-type="add" data-old-value=""  class="filter-item" value="{{ $row['vrednost_id'] }}"> {{$row['vrednost_naziv']}} 
                        
                        <span class="pull-right">{{$row['broj_proizvoda']}}</span>
                    </label>
                    @endif
                @endforeach
            </div>
        </li>
    @endforeach
    </ul>
</form>

<div> 
    <br>
    <a class="JSreset-filters inline-block button" href="{{ Request::url()}}">Poništi filtere</a>
</div>
 

 
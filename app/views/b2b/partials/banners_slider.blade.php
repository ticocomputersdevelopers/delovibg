<div id="JSmain-slider"> 
    <?php foreach(DB::table('baneri_b2b')->where('tip_prikaza',2)->where('aktivan',1)->orderBy('redni_broj','asc')->get() as $row){ ?>
    @if((B2bOptions::trajanje_banera($row->baneri_id)) == 1 && (B2bOptions::future_banners($row->baneri_id)) == 1)
    <div>
        <a href="<?php echo $row->link; ?>"><img class="img-responsive" alt="{{$row->naziv}}" src="{{B2bOptions::base_url()}}<?php echo $row->img; ?>" /></a>
    </div>
    @endif
    <?php } ?>  
</div>

  
<div class="banners-right row bw">
    <?php foreach(DB::table('baneri_b2b')->where('tip_prikaza', 1)->where('aktivan',1)->where('mesto_prikaza',0)->orderBy('redni_broj','asc')->get() as $row){ ?>
        @if((B2bOptions::trajanje_banera($row->baneri_id)) == 1 && (B2bOptions::future_banners($row->baneri_id)) == 1)
        <div class="col-md-4 col-sm-4 col-xs-6 no-padding">
            <a class="inline-block" href="<?php echo $row->link; ?>">
                <img class="img-responsive" src="{{B2bOptions::base_url()}}<?php echo $row->img; ?>" alt="{{$row->naziv}}" />
            </a>
        </div>
        @endif
    <?php } ?>
</div>
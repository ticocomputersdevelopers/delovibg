<div id="preheader"> 
    <div class="container"> 
        <div class="row"> 
            <div class="col-xs-12 text-right sm-nopadd"> 
                              
                @if(B2bOptions::product_currency()==1)
                <div class="currency-change inline-block"> 
                    @if(Session::get('b2b_valuta') == 1)   
                    <a id="currency_rsd" class="active" href="#">RSD</a> 
                    <a href="javascript:void(0)" id="currency_eur">EURO</a>   
                    @elseif(Session::get('b2b_valuta') == 2)  
                    <a id="currency_rsd" href="javascript:void(0)">RSD</a> 
                    <a href="javascript:void(0)" class="active" id="currency_eur">EURO</a>  
                    @else  
                    <a id="currency_rsd" class="active" href="#">RSD</a> 
                    <a href="javascript:void(0)" id="currency_eur">EURO</a> 
                    @endif  
                </div>
                @endif  

                @if(Session::has('b2b_valuta') and Session::get('b2b_valuta') != 1) 
                    <span class="euro right-pseudo inline-block relative">Današnji kurs {{ B2bOptions::kurs() }}</span> 
                @endif 
        
                @if(Session::has('b2b_user_'.B2bOptions::server())) 
                    @if(!is_null($partnerUser = B2bPartner::getPartnerUserObject()))
                    <a class="inline-block" href="#!"> {{ B2bPartner::getPartnerName() }} - {{ $partnerUser->naziv }}</a>  
                    @else 
                    <a class="inline-block" href="{{route('b2b.user_edit')}}"> {{ B2bPartner::getPartnerName() }} </a>   
                    @endif
                @endif

                <a href="/b2b/logout" class="left-pseudo inline-block relative">Odjavi se</a>   

               <!--  @if(Session::has('b2b_user_'.B2bOptions::server()))
                    <a id="user_edit" href="{{route('b2b.user_edit')}}" class="right-pseudo inline-block relative">
                        <i class="fa fa-user-o"></i><span> {{ B2bPartner::getPartnerName() }}</span>
                    </a>
                @endif -->
                
                @if(Session::has('b2c_admin'.B2bOptions::server()))
                    <a target="_blank" href="{{ Options::domain() }}admin" class="left-pseudo inline-block relative"> Admin Panel</a>
                @endif  
            </div>
        </div>
    </div>
</div>

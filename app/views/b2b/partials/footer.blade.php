<footer>
    <div class="container">
        <div class="row"> 
            <div class="col-md-3 col-sm-3 col-xs-12 hidden">    
               <!--  <a class="logo" href="{{ B2bOptions::base_url()}}b2b" title="{{B2bOptions::company_name()}}">
                    <img class="img-responsive" src="{{ B2bOptions::base_url()}}{{B2bOptions::company_logo()}}" alt="{{B2bOptions::company_name()}}" />
                </a> -->
                <ul class="footer-lists">
                    @foreach (B2bCommon::menu_footer() as $row)
                    <li>
                        <a class="inline-block" href="{{ B2bOptions::base_url().'b2b/'.$row->naziv_stranice }}">{{ $row->title }}</a>
                    </li>
                    @endforeach
                </ul> 


            </div>

                <!-- PAGES -->
                <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="bg-footer-pages no-padding">

                    <span class="you-are-here">Ti si ovde!</span>
                    <div class="row bg-footer-pages-card">
                        <div class="col-xs-1 bg-footer-box-1">&nbsp;</div>
                        <div class="col-xs-10 bg-footer-pages-title">SHOP</div>
                        <div class="col-xs-12 bg-footer-pages-text">Lorem ipsum dolor sit atmet lorem ipsum Lorem ipsum dolor sit atmet lorem ipsum. <span class="fas fa-caret-right pages-right">&nbsp;</span></div>
                    </div>

                    <span class="you-are-here">Ti si ovde!</span>
                    <div class="row bg-footer-pages-card">
                        <div class="col-xs-1 bg-footer-box-1">&nbsp;</div>
                        <div class="col-xs-10 bg-footer-pages-title">SHOP</div>
                        <div class="col-xs-12 bg-footer-pages-text">Lorem ipsum dolor sit atmet lorem ipsum Lorem ipsum dolor sit atmet lorem ipsum. <span class="fas fa-caret-right pages-right">&nbsp;</span></div>
                    </div>

                    <span class="you-are-here">Ti si ovde!</span>
                    <div class="row bg-footer-pages-card">
                        <div class="col-xs-1 bg-footer-box-1">&nbsp;</div>
                        <div class="col-xs-10 bg-footer-pages-title">SHOP</div>
                        <div class="col-xs-12 bg-footer-pages-text">Lorem ipsum dolor sit atmet lorem ipsum Lorem ipsum dolor sit atmet lorem ipsum. <span class="fas fa-caret-right pages-right">&nbsp;</span></div>
                    </div>

                    <span class="you-are-here">Ti si ovde!</span>
                    <div class="row bg-footer-pages-card">
                        <div class="col-xs-1 bg-footer-box-1">&nbsp;</div>
                        <div class="col-xs-10 bg-footer-pages-title">SHOP</div>
                        <div class="col-xs-12 bg-footer-pages-text">Lorem ipsum dolor sit atmet lorem ipsum Lorem ipsum dolor sit atmet lorem ipsum. <span class="fas fa-caret-right pages-right">&nbsp;</span></div>
                    </div>

                </div> 
            </div>
            <!-- END OF PAGES -->

            <div class="col-md-5 col-sm-6 col-xs-12 text-bold customContacts">  
                  <a class="logo inline-block" href="{{ B2bOptions::base_url()}}b2b" title="{{B2bOptions::company_name()}}">
                    <img class="img-responsive" src="{{ B2bOptions::base_url()}}{{B2bOptions::company_logo()}}" alt="{{B2bOptions::company_name()}}" />
                </a>

                <ul>
                    @if(Options::company_phone())
                    <li>
                        <i class="bg-footer-icons fas fa-phone-alt"></i>
                        <a class="mailto bg-footer-contact-text" href="tel:{{ Options::company_phone() }}">
                            {{ Options::company_phone() }}
                        </a>
                    </li>                          
                    @endif

                    @if(Options::company_email())
                    <li>
                        <i class="bg-footer-icons far fa-envelope"></i>
                        <a class="mailto bg-footer-contact-text" href="mailto:{{ Options::company_email() }}">{{ Options::company_email() }}</a>
                    </li>
                    @endif

                    @if(Options::company_adress() OR Options::company_city())
                    <li id="bg-foter-adress"> 
                        <i id="bg-footer-adress-icon" class="bg-footer-icons fas fa-map-marked-alt" style=""></i>
                        <span class="bg-footer-contact-text">{{ Options::company_adress() }}</span>
                        <br> 
                        <i class="bg-footer-icons fas fa-map-marked-alt" style="visibility: hidden;"></i>
                        <span class="bg-footer-contact-text">{{ Options::company_city() }}</span>
                    </li>
                    @endif


                    <li>
                        <i class="bg-footer-icons fab fa-instagram"></i>
                        <!-- <span class="bg-footer-contact-text">bg.elektronik</span> -->
                        <a class="bg-footer-contact-text" target="_blank" href="https://www.instagram.com/bg.elektronik/">{{ Options::company_name() }}</a>
                    </li>
                </ul>
               <!--  <div class="relative newsletter-input inline-block"> 
                    {{-- @if(Options::newsletter()==1) --}}
                    <input type="text" class="" placeholder="Vaša E-mail Adresa" id="newsletter" />
                    <button class="button" onclick="newsletter()">Prijavite se</button>
                    {{-- @endif --}}
                </div>   -->
               <!--  <div class="social-icons"> 
                    {{Options::social_icon()}}
                </div> -->
            </div>  

            <!-- ISPORUKE -->
                <div class="col-md-3 col-sm-6 col-xs-12 text-bold customExports">
                    <!-- ABOUT -->
                      <ul class="footer-lists">
                    @foreach (B2bCommon::menu_footer() as $row)
                    <li>
                        <a class="inline-block" href="{{ B2bOptions::base_url().'b2b/'.$row->naziv_stranice }}">{{ $row->title }}
                            <span class="fas fa-caret-right"></span>
                        </a>
                    </li>
                    @endforeach
                </ul> 


                    <!-- END OF ABOUT -->
                </div>

            <!-- ISPORUKE -->

            <!-- <div class="col-md-3 col-sm-3 col-xs-12">     
                <ul> 
                    <li>
                        <h4>Kontakt 
                            @if(Options::company_map() != '' && Options::company_map() != ';') 
                            <span class="JStoggMap inline-block pointer relative">Prikaži mapu <i class="fas fa-map-marker-alt"></i></span>
                            @endif 
                            </h4>  
                        </li>
                    <li>
                        {{ Options::company_adress() }}
                        {{ Options::company_city() }}  
                    </li>

                    <li>{{ Options::company_phone() }}</li> 
                    <li>{{ Options::company_fax() }}</li> 
                        
                    <li> 
                        <a href="mailto:{{ Options::company_email() }}"> {{ Options::company_email() }} </a>
                    </li> 
                </ul>   
            </div> -->  
        </div>  
    </div>  

  <!--   @if(Options::company_map() != '' && Options::company_map() != ';')
    <div class="map-frame JSmap relative">
        
        <div class="map-info">
            <h5>{{ Options::company_name() }}</h5>
            <h6>{{ Options::company_adress() }}, {{ Options::company_city() }}</h6> 
        </div>

        <iframe src="https://maps.google.com/maps?q={{ All::lat_long()[0] }},{{ All::lat_long()[1] }}&output=embed" width="100%" height="100%" frameborder="0" aria-hidden="false" tabindex="0"></iframe> 

    </div> 
    @endif -->
    
    <div class="container">
        <div class="text-center foot-note"> 
            <span>{{B2bOptions::company_name() }}  {{ date('Y') }}. Sva prava zadržana <i class="far fa-copyright"></i></span>
            <a href="https://www.selltico.com/" target="_blank">- Izrada B2B portala</a>  
            <a href="https://www.selltico.com/" target="_blank">- Selltico.</a> 
        </div>
    </div>

    <a class="JSscroll-top" href="javascript:void(0)">
        <i class="fa fa-angle-up"></i>
    </a> 
</footer>



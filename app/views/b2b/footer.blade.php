<footer>
    <div class="row"> 
        <nav class="medium-3 columns">&nbsp;
            <ul class="footer-links">
            @foreach (B2bCommon::menu_footer() as $row)
              <li><a href="{{ B2bOptions::base_url().'b2b/'.$row->naziv_stranice }}">{{ $row->title }}</a></li>
            @endforeach
            </ul>
        </nav>
             <div class="medium-3 small-12 columns newsletter-checkin">
                @if(Options::newsletter()==1)
                    <h4 class="footer-h4">Prijavite se za newsletter</h4>
                    <div class="input-in-footer">
                        <input type="text" class="newsletter-input" placeholder="E-mail" id="newsletter" />
                        <button onclick="newsletter()" class="butt-foot">PRIJAVA</button>
                    </div>
                @endif
                <!--<div class="social-icons hide-for-large-up">
                     {{Options::social_icon()}}
                </div>--> 
             </div>

           <section class="medium-6 columns">
            <h4 class="footer-h4">Partnerski sertifikati</h4>
            <div class="banks-footer"> 
              <div class="medium-2 small-12 columns">
                <img class="for-vert-align image-size-logos" alt="logos" src="{{ B2bOptions::base_url() }}images/footerLogos/Bosch-logo.png">
              </div>

              <div class="medium-2 small-12 columns">
                <img class="for-vert-align image-size-logos" alt="logos" src="{{ B2bOptions::base_url() }}images/footerLogos/Siemens_AG_logo.svg.png">
              </div>

              <div class="medium-2 small-12 columns">
                <img class="image-size-logos" alt="logos" src="{{ B2bOptions::base_url() }}images/footerLogos/1000px-Intel-logo.svg.png">
              </div>

              <div class="medium-2 small-12 columns">
                <img class="image-size-logos" alt="logos" src="{{ B2bOptions::base_url() }}images/footerLogos/amd-elite.png">
              </div>

              <div class="medium-2 small-12 columns end">
                <img class="image-size-logos" alt="logos" src="{{ B2bOptions::base_url() }}images/footerLogos/seagate-premier-partner.png">
              </div>
           </div>
        </section>
    </div>

    <div class="row"> 
      <section class="contact-info medium-4 columns">
         <h4 class="footer-h4">Kontakt</h4>
             <ul> 
                <li class="medium-12 columns footer-h4">Veleprodavac doo</li>
                <li class="medium-12 columns">  {{ Options::company_adress() }} {{ Options::company_city() }}</li>
                <li class="medium-12 columns"><i class="fa fa-phone"></i> {{ Options::company_phone() }} Maloprodaja</li>
                <li class="medium-12 columns"><i class="fa fa-phone"></i> {{ Options::company_phone() }} Veleprodaja</li>
                <li class="medium-12 columns"><i class="fa fa-fax"></i> {{ Options::company_fax() }}</li>
               <!--  <li class="medium-12 columns">PIB: {{ Options::company_pib() }}</li> -->
                <li class="medium-12 columns"><i class="fa fa-envelope"></i>  {{ Options::company_email() }}</li>
            </ul>
      </section>
   </div>

<div class="row text-center">
    <a class="scroll-top" href="javascript:void(0)"><i class="fa fa-caret-up beginning-of-page-caret" style="font-size:24px"></i> Početak strane</a>
</div>

 <div class="row"> 
    <div class="medium-12 columns text-center">
        <p class="info"> Cene su informativnog karaktera. Prodavac ne odgovara za tačnost cena iskazanih na sajtu, zadržava pravo izmena cena. Ponudu za ostale artikle, informacije o stanju lagera i aktuelnim cenama možete dobiti na upit. Plaćanje se isključivo vrši virmanski - bezgotovinski.
        </p>
        <a href="http://www.tico.rs/">Developed by Tico | TiCo &copy; {{ date('Y') }} </a> 
    </div>
</div>
</footer>
@extends('b2b.templates.main')
@section('content')  

<!-- user_orders.blade -->

<div class="row" id="orders">
    <div class="col-xs-12"> 

        <br>

        <h2><span class="section-title">Vaše narudžbine</span></h2>
      
        <div class="row">

            <div class="col-md-3 col-sm-3 col-xs-6 no-padding"> 
                <!-- <span>Filtriraj: </span> -->
                <label>Datum od:</label>
                <input type="text" value="{{ !is_null($date_start) ? $date_start : '' }}" id="date_start">
            </div>

            <div class="col-md-3 col-sm-3 col-xs-6">
                <label>Datum do:</label>
                <input type="text" id="date_end" value="{{ !is_null($date_end) ? $date_end : '' }}">
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="lineh"><label>&nbsp;</label></div>

                <button class="button" id="JSFilterDate">Potvrdi</button>
                <a href="{{B2bOptions::base_url()}}b2b/user/orders" id="filterCustom" class="button" role="button">Poništi filtere</a>
            </div>  

        </div>
        <br>

        <div class="table-responsive user-card-table text-center"> 
            <table class="table table-condensed table-hover"> 
                <thead> 
                    <tr> 
                        <th>Broj</th>
                        <th>Datum</th>
                        <th>Status</th>
                        <th>Način isporuke</th>
                        <th>Način plaćanja</th>
                        <th>Iznos</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody> 
                    @foreach($orders as $order)
                    <tr> 
                        <td>{{$order->broj_dokumenta}}</td>
                        <td>{{date("d-m-Y",strtotime($order->datum_dokumenta))}}</td>
                        <td>{{ B2bCommon::orderStatus($order) }}</td>
                        <td>{{ B2bBasket::getNameNacinIsporuke($order->web_nacin_isporuke_id) }}</td>
                        <td>{{ B2bBasket::getNameNacinPlacanja($order->web_nacin_placanja_id) }}</td>
                        <td>{{ number_format($order->iznos,2,",",".") }}</td>
                        <td class="text-center JSPartnerOrder pointer" data-order_id="{{ $order->web_b2b_narudzbina_id }}"><i class="fa fa-search-plus"></i></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <div class="row">
            {{ $orders->links() }}
        </div> 


        <!-- Modal -->
        <div id="JSPartnerOrdersModal" class="modal fade user-card-modal">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content"> 
                    <div class="modal-body" id="JSPartnerOrderItems"></div>
                    <div class="modal-footer">
                        <button type="button" class="button" data-dismiss="modal">Zatvori</button>
                    </div>
                </div> 
            </div>
        </div>  
    </div>
</div>

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="{{Options::domain() }}js/bootstrap.min.js"></script>
<script>
    $(function() {
        $( "#date_start, #date_end").datepicker({
            dateFormat: 'yy-mm-dd'
        });
    });

    $('#date_start, #date_end').keydown(false);

    $('#JSFilterDate').click(function(){
        var datum_start = $('#date_start').val();
        var datum_end = $('#date_end').val();
        if(datum_start == ''){
            datum_start = 'null';
        }
        if(datum_end == ''){
            datum_end = 'null';               
        }
        location.href = "{{ B2bOptions::base_url() }}b2b/user/orders/"+datum_start+"/"+datum_end;
    });

    $('.JSPartnerOrder').click(function(){
        var narudzbina_id = $(this).attr('data-order_id');

        $.ajax({
            type: "POST",
            url: base_url+'b2b/user/order',
            data: {narudzbina_id:narudzbina_id},
            success: function(response) {

                $('#JSPartnerOrderItems').html(response);
            }
        });    
        $('#JSPartnerOrdersModal').modal('show');
    });
</script>

<!-- user_orders.blade end -->

@endsection
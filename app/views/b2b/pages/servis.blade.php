@extends('b2b.templates.main')
@section('content')  

<!-- servis.blade -->

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <br>
 
        <h3 class="text-bold mb-1">Uslovi za slanje reklamacija o tro&scaron;ku Veleprodavac:</h3>
        <p>- Minimalna vrednost RMA robe za povrat: 1500 din</p>
        <p>- ili minimalno _1___ proizvoda za povrat</p>
        <p>- Isključivo Veleprodavac &scaron;alje nalog za prevoz RMA robe. (Nakon Va&scaron;eg obave&scaron;tenja da je roba spremna za preuzimanje..)</p>
        <p>- Poslati popunjen formular na email adresu:&nbsp;<a href="mailto:servis@gembird.rs">servis@Veleprodavac.rs</a>&nbsp;(011-4140000 Petar Petrović) Po prijemu va&scaron;e reklamacije na email adresi očekujemo da roba bude već spremna za preuzimanje od strane na&scaron;eg kurira. (PS: nalepite od&scaron;tampan RMA formular na vidno mesto na kutiji/kesi u kojoj &scaron;aljete reklamaciju).</p>
        <br><br>
        <h3 class="text-bold mb-1">Procedure i dokumentacija za povrat robe na REKLAMACIJU</h3>
        <p class="heading-red text-muted">Sve dole navedene dokumente isključivo predati kuriru na ruke, u suprotnom roba neće biti preuzeta!</p>
        <p class="text-muted">Ovo podrazumeva povrat neispravne robe na servis koja je u garantnom roku.</p>
        <p>Propratna dokumentacija:</p>
        <p>1.<a href="http://demo1.tico.rs/files/formular%20_POVRAT_ROBE_NA_REKLAMACIJU.pdf" target="_blank">&nbsp;Dokument&nbsp;</a>o slanju reklamacije.</p>
        <p>Kliknite na link za download dokumenta</p>
        <p>popunjava se ručno - pdf</p>
        <p>popunjava se na računaru - xls</p>
        <p>2. Dokaz o kupovini (račun).</p>
        <p>3. Ispravno popunjen garantni list.</p>
        <p><span class="span-red">Napomena:</span>&nbsp;uz ovaj dokument moraju se poslati garancije i kopije računa. (Od&scaron;tampati popunjen dokument i zalepiti na vidno mesto na kutiju u kojoj se &scaron;alju reklamacije).</p>
        <br><br>
        <h3 class="text-bold mb-1">Procedure i dokumentacija za povrat ISPRAVNE robe</h3>
        <p class="text-muted">Sve dole navedene dokumente isključivo predati kuriru na ruke, u suprotnom roba neće biti preuzeta!</p>
        <p class="text-muted">Ovo obuhvata samo robu za koju važi dogovor da može da se vrati(na&scaron;a distribucija).</p>
        <p>Propratna dokumentacija:</p>
        <p>1.&nbsp;<a href="http://demo1.tico.rs/files/NEISPRAVNOST_NOVOG_UREDJAJA_FORMULAR.pdf" target="_blank">Dokument</a>&nbsp;za povrat nove robe na memorandumu va&scaron;e firme. Ili u dogovoru sa na&scaron;im komercijalistom možete napraviti i Va&scaron; račun ka nama naravno sa cenama po kojima ste Vi i zaduženi.</p>
        <p><span class="span-red">Napomena:</span>&nbsp;Povrati moraju da budu najavljeni komercijalisti da bi se proverile povratnice unapred, saznao broj paketa itd. Takođe svi povrati moraju biti upakovani tako da se ne naru&scaron;ava izgled "originalne ambalaže" tj. za&scaron;titite ih od lepljenja bilo kakvih nalepnica.</p>
        <br><br>
  
    </div>
</div>

<div class="row" id="rmaCustom">  
    <div class="col-md-6 col-sm-7 col-xs-12 col-md-offset-3 col-sm-offset-2"> 
        <form action="{{ Options::base_url()}}b2b/servis_send" method="post" class="registration-form" autocomplete="off"><br>
            <h2 class="text-center text-bold">Prijava na servis</h2>
            <div class="form-group">
                <label for="naziv">{{ Language::trans('Uređaj') }}:</label>
                <input id="naziv" class="form-control" name="naziv" type="text" value="{{ htmlentities(Input::old('naziv') ? Input::old('naziv') : '') }}" >
                <div class="error red-dot-error">{{ $errors->first('naziv') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('naziv') : "" }}
                </div>
            </div>
            <div class="form-group">
                <label for="datum">{{ Language::trans('Datum kupovine') }}</label>
                <input id="datum" class="form-control" name="datum" type="text" value="{{ Input::old('datum') ? Input::old('datum') : '' }}" >
                <div class="error red-dot-error">{{ $errors->first('datum') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('datum') : "" }}</div>
            </div>  
            <div class="form-group">
                <label for="brfakture">{{ Language::trans('Broj fakture') }}</label>
                <input id="brfakture" class="form-control" name="brfakture" type="text" value="{{ Input::old('brfakture') ? Input::old('brfakture') : '' }}" >
                <div class="error red-dot-error">{{ $errors->first('brfakture') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('brfakture') : "" }}</div>
            </div>  
            <div class="form-group">
                <label for="serial">{{ Language::trans('Serijski broj') }}</label>
                <input id="serial" class="form-control" name="serial" type="text" value="{{ htmlentities(Input::old('serial') ? Input::old('serial') : '') }}">
                <div class="error red-dot-error">{{ $errors->first('serial') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('serial') : "" }}</div>
            </div>
            <div class="form-group">
                <label for="opis">{{ Language::trans('Opis kvara') }}</label>
                <input id="opis" class="form-control" name="opis" type="text" value="{{ Input::old('opis') ? Input::old('opis') : '' }}" >
                <div class="error red-dot-error">{{ $errors->first('opis') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('opis') : "" }}</div>
            </div>
            <div class="text-right">   
                <button type="submit" class="button">{{ Language::trans('Prijavi') }}</button>
            </div>
        </form>
        @if(Session::get('message'))
        <div class="row"> 
            <div class="success bg-success">{{ Language::trans('Potvrda registracije je poslata na vašu e-mail adresu') }}!</div>
        </div>
        @endif
    </div>
</div>  
<script>
    /*====== SMOOTH SCROLL AFTER RELOAD =======*/
    $(window).on("scroll", function() {
        sessionStorage.setItem("scrollTop", $(this).scrollTop());
    });
  
    $("html, body").animate({
        scrollTop: sessionStorage.scrollTop
    }, 1000);
        
  
    if (location.pathname != "b2b/rma") {
        sessionStorage.removeItem("scrollTop"); 
    }
</script>


<!-- servis.blade end -->

@endsection

@extends('b2b.templates.main')
@section('content')  
<form action="{{route('b2b.user_user_edit')}}" method="post" id="partnerCustom">
    <input type="hidden" name="partner_korisnik_id" value="{{ $partnerKorisnik->partner_korisnik_id }}"> 
    <h2><span class="section-title text-bold">Podaci za dostavu</span></h2>
    <div class="row"> 
        <div class="form-group col-md-4 col-sm-4 col-xs-12">
            <label for="naziv">Naziv*</label>
            <input class="form-control" id="naziv" name="naziv" type="text" value="{{ !is_null(Input::old('naziv')) ? Input::old('naziv') : $partnerKorisnik->naziv}}">
            <div class="error red-dot-error">{{ $errors->first('naziv') }}</div>
        </div>
        <div class="form-group col-md-4 col-sm-4 col-xs-12">
            <label for="adresa">Adresa*</label>
            <input class="form-control" id="adresa" name="adresa" type="text" value="{{ !is_null(Input::old('adresa')) ? Input::old('adresa') : $partnerKorisnik->adresa}}">
            <div class="error red-dot-error">{{ $errors->first('adresa') }}</div>
        </div>
        <div class="form-group col-md-4 col-sm-4 col-xs-12">
            <label for="mesto">Mesto*</label>
            <input class="form-control" id="mesto" name="mesto" type="text" value="{{ !is_null(Input::old('mesto')) ? Input::old('mesto') : $partnerKorisnik->mesto}}">
            <div class="error red-dot-error">{{ $errors->first('mesto') }}</div>
        </div>
    </div>
    <div class="row"> 
        <div class="form-group col-md-4 col-sm-4 col-xs-12">
            <label for="telefon">Telefon*</label>
            <input class="form-control" id="telefon" name="telefon" type="text" value="{{ !is_null(Input::old('telefon')) ? Input::old('telefon') : $partnerKorisnik->telefon}}">
            <div class="error red-dot-error">{{ $errors->first('telefon') }}</div>
        </div>
        <div class="form-group col-md-4 col-sm-4 col-xs-12">
            <label for="email">E-mail*</label>
            <input class="form-control" id="email" name="email" type="text" value="{{ !is_null(Input::old('email')) ? Input::old('email') : $partnerKorisnik->email}}">
            <div class="error red-dot-error">{{ $errors->first('email') }}</div>
        </div>
    </div>

    <h2><span class="section-title text-bold">Pristupni podaci </span></h2>
    <div class="row"> 
        <div class="form-group col-md-4 col-sm-4 col-xs-12">
            <label for="korisnicko_ime">Korisničko ime*</label>
            <input class="form-control" id="korisnicko_ime" name="korisnicko_ime" type="text" value="{{ !is_null(Input::old('korisnicko_ime')) ? Input::old('korisnicko_ime') : $partnerKorisnik->korisnicko_ime}}">
            <div class="error red-dot-error">{{ $errors->first('korisnicko_ime') }}</div>
        </div>
        <div class="form-group col-md-4 col-sm-4 col-xs-12">
            <label for="lozinka">Lozinka*</label>
            <input class="form-control" id="lozinka" name="lozinka" type="text" value="{{ !is_null(Input::old('lozinka')) ? Input::old('lozinka') : $partnerKorisnik->lozinka}}">
            <div class="error red-dot-error">{{ $errors->first('lozinka') }}</div>
        </div>
        <div class="form-group col-md-4 col-sm-4 col-xs-12">
            <label for="aktivan" class="aktivan">Aktivan*</label>
            <select class="form-control" id="aktivan" name="aktivan">
                <option value="1">Da</option>
                <option value="0" {{ ((!is_null(Input::old('aktivan')) ? Input::old('aktivan') : $partnerKorisnik->aktivan) == 0) ? 'selected' : '' }}>Ne</option>
            </select>
            <div class="error red-dot-error">{{ $errors->first('aktivan') }}</div>
        </div>
    </div>

    <div class="row"> 
        <div class="col-md-6 col-sm-6 col-xs-12"> 
            <button class="submit admin-login" id="loginCustom">Sačuvaj </button>
            <a href="/b2b/user/user/{{ $partnerKorisnik->partner_korisnik_id }}/delete" class="submit admin-login btn" id="deleteCustom">Obriši </a>
        </div> 
    </div>

</form>   
@endsection
 
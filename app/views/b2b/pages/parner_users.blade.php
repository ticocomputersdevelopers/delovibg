@extends('b2b.templates.main')
@section('content')  

<div class="row" id="usersCustom">
    <div class="col-xs-12"> 

        <br>

        <h2><span class="section-title">Vaši korisnici</span></h2>

        <div class="table-responsive user-card-table">
            <a class="button" href="/b2b/user/user/0"><i class="fas fa-plus"></i> Dodaj novog korisnika</a>
            <table class="table table-condensed table-striped table-hover"> 
                <thead> 
                    <tr> 
                        <th>Naziv</th>
                        <th>Adresa</th> 
                        <th>Mesto</th>
                        <th>Telefon</th>
                        <th>E-mail</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody> 
                    @foreach($partnerKorisnici as $partnerKorisnik)
                    <tr> 
                        <td>{{$partnerKorisnik->naziv}}</td>
                        <td>{{$partnerKorisnik->adresa}}</td>
                        <td>{{$partnerKorisnik->mesto}}</td>
                        <td>{{$partnerKorisnik->telefon}}</td>
                        <td>{{$partnerKorisnik->email}}</td>
                        <td class="text-center pointer" ><a href="/b2b/user/user/{{ $partnerKorisnik->partner_korisnik_id }}">Vidi</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <div class="row">
            {{ $partnerKorisnici->links() }}
        </div> 
    </div>
</div>

@endsection
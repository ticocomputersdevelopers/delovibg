
$(document).ready(function () {
// LAZY LOAD IMAGES
	$(function() {
		// change data-src to src to disable plugin
		// 1.product_on_grid
		// 2.product_on_list
		// 3.footer

        $('.JSlazy_load').Lazy({
        	scrollDirection: 'vertical'
        });
    });

	  // IF THERE IS NO IMAGE
	$('img').on('error', function(){ 
		$(this).attr('src', '/images/no-image.jpg').addClass('img-responsive');
	});

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

// *********************************************************  
$('header').addClass('header-black-works');	

// PLUCK FIRST SLIDE FROM TIP SLIDER
if($('.JStip-slider').length) {
	$('.JStip-slider .banners').each(function(i) {
		if ($(window).width() > 768) {
			if(i == 0) {
				// $(this).parentsUntil('.d-content').parent().prepend($(this).find('.bg-img'));
				$(this).find('.bg-img').parent().wrap('<div class="JStip-slider-wrap relative"></div>');
				$(this).closest('.d-content').prepend($(this).find('.JStip-slider-wrap')).wrapAll('<div class="JSmain-pull"></div>');
			}
			if(i == 1) {
				$(this).hide();
			}
		} else {
			if(i == 0) {
				$(this).hide();
			}
			if(i == 1) {
				// $(this).parentsUntil('.d-content').parent().prepend($(this).find('.bg-img'));
				$(this).find('.bg-img').parent().wrap('<div class="JStip-slider-wrap relative"></div>');
				$(this).closest('.d-content').prepend($(this).find('.JStip-slider-wrap')).wrapAll('<div class="JSmain-pull"></div>');
			}
		}
	});
}

if ($(window).width() > 768) {
	if($('.JSmain-pull').length) {

		$('.JSmain-pull').each(function(i, el){ 
			var sliderOffset = parseInt($(el).offset().top);
			var headerOffset = parseInt($('header').outerHeight() + $('#preheader').outerHeight());
			if($('#admin-menu').length) {
				headerOffset = parseInt(headerOffset + $('#admin-menu').outerHeight());
			}

			if(sliderOffset == headerOffset) {
				// SLIDER NEGATIVE MARGIN TOP
				$(el).css('margin-top', - $('header').outerHeight());

				if(!$('.JSsticky_header').length) {
					if(sliderOffset == headerOffset) {
						$('header').addClass('header-white-works');
						$('header').removeClass('header-black-works');
					} else {
						$('header').addClass('header-black-works');
						$('header').removeClass('header-white-works');
					}
				}
				else {
					$('header').addClass('header-white-works');
					$('header').removeClass('header-black-works');
				}
			}
		});
	} 
} 


// SLIDERS AND BANNERS SPLIT INTO DESKTOP AND RESPONSIVE
if($('.JSmain-slider').length) {
	$('.JSmain-slider .relative').each(function(i) {
		if($(window).width() > 479 && (i % 2 > 0)) {
			$(this).remove();
		} else if($(window).width() < 479 && (i % 2 == 0)) {
			$(this).remove();
		}
	});
} 


// REMOVE EMPTY LOGGED USER
$('.logged-user').each(function(i, el){ 
	if ( ($(el).text().trim() == '')) {
		$(el).hide();
	}
});

// SLICK SLIDER INCLUDE
if ($('.JSmain-slider').length) {
	if($('#admin_id').val()){

		$('.JSmain-slider').slick({
			autoplay: false,
			draggable: false,
			dots:true
		});

	}else{
		$('.JSmain-slider').slick({
			autoplay: true, 
			dots:true
		});
	} 
}
 
// PRODUCTS SLICK SLIDER 
if ($('.JSproducts_slick').length){ 
	$('.JSproducts_slick').slick({ 
		arrows: true,
		responsive: [
			{
			  breakpoint: 4000,
			  settings: {
				autoplay: true,
				slidesToShow: 6,
				slidesToScroll: 1, 
			  }
			},
			{
			  breakpoint: 1600,
			  settings: {
				slidesToShow: 5,
				slidesToScroll: 1
			  }
			},
			{
			  breakpoint: 1400,
			  settings: {
				slidesToShow: 4,
				slidesToScroll: 1
			  }
			},
			{
			  breakpoint: 1200,
			  settings: {
				slidesToShow: 3,
				slidesToScroll: 1
			  }
			},
			{
			  breakpoint: 991,
			  settings: {
				slidesToShow: 3,
				slidesToScroll: 1,
				draggable: true,
				dots: true
			  }
			},
			// {
			//   breakpoint: 768,
			//   settings: {
			// 	slidesToShow: 2,
			// 	slidesToScroll: 1,
			// 	arrows: false,
			// 	draggable: true,
			// 	dots: true
			//   }
			// },
			{
			  breakpoint: 768,
			  settings: {
				draggable: true,
				infinite: true,
				variableWidth: true,
				variableTransform: true,
				slidesToScroll: 1,
				autoplay: true,
			  }
			}
		]

		
	}); 
}

if ($('.JSblanco_slick').length){ 
	$('.JSblanco_slick').slick({ 
		arrows: true,
		autoplay: true,
		slidesToShow: 5,
		slidesToScroll: 1,
		dots: false,
		responsive: [
			{
			  breakpoint: 991,
			  settings: {
				slidesToShow: 4,
				slidesToScroll: 1,
				draggable: true
			  }
			},
			{
			  breakpoint: 768,
			  settings: {
				slidesToShow: 3,
				slidesToScroll: 1,
				arrows: true,
				draggable: true
			  }
			},
			{
			  breakpoint: 400,
			  settings: {
				slidesToShow: 2,
				slidesToScroll: 1,
				arrows: true,
				draggable: true
			  }
			},
		]

		
	}); 
}


if ($('.JSbought_slick').length){ 
	$('.JSbought_slick').slick({ 
		
		responsive: [
			{
			  breakpoint: 4000,
			  settings: {
				autoplay: true,
				slidesToShow: 7,
				slidesToScroll: 1, 
			  }
			},
			{
			  breakpoint: 1600,
			  settings: {
				slidesToShow: 5,
				slidesToScroll: 1,
			  }
			},
			{
			  breakpoint: 1200,
			  settings: {
				slidesToShow: 4,
				slidesToScroll: 1,
			  }
			},
			{
			  breakpoint: 991,
			  settings: {
				slidesToShow: 2,
				slidesToScroll: 1,
				draggable: true,
			  }
			},
		
			{
			  breakpoint: 768,
			  settings: {
				draggable: true,
				infinite: true,
				variableWidth: true,
				variableTransform: true,
				autoplay: true,
				arrows: false
			  }
			}
		]

		
	}); 
}

// GROUPS SLICK SLIDER 
if ($('.JSgroups_slick').length){ 
	$('.JSgroups_slick').slick({  
		arrows: true,
		responsive: [
			{
			  breakpoint: 4000,
			  settings: {
				autoplay: true,
				draggable: true,
				slidesToShow: 8,
				slidesToScroll: 1, 
			  }
			},
			{
			  breakpoint: 1200,
			  settings: {
				slidesToShow: 6,
				slidesToScroll: 1,
				draggable: true,
			  }
			},
			{
			  breakpoint: 991,
			  settings: {
				slidesToShow: 4,
				slidesToScroll: 1,
				draggable: true,
			  }
			},
			// {
			//   breakpoint: 768,
			//   settings: {
			// 	slidesToShow: 4,
			// 	slidesToScroll: 1,
			// 	arrows: false,
			// 	draggable: true,
			//   }
			// },
			{
			  breakpoint: 768,
			  settings: {
			  	autoplay: true,
				draggable: true,
				infinite: true,
				variableWidth: true,
				variableTransform: true,

			  }
			}
		]
	}); 
}

// BANNERS SLICK SLIDER 
if ($('.JSbanners_slick').length){ 
	$('.JSbanners_slick').slick({ 
		
		responsive: [
			{
			  breakpoint: 4000,
			  settings: {
				autoplay: true,
				slidesToShow: 4,
				slidesToScroll: 1, 
			  }
			},
			{
			  breakpoint: 1200,
			  settings: {
				slidesToShow: 2,
				slidesToScroll: 1,
			  }
			},
			{
			  breakpoint: 991,
			  settings: {
				slidesToShow: 2,
				slidesToScroll: 1,
				arrows: false,
				draggable: true,
			  }
			},
			// {
			//   breakpoint: 768,
			//   settings: {
			// 	slidesToShow: 2,
			// 	slidesToScroll: 1,
			// 	arrows: false,
			// 	draggable: true,
			//   }
			// },
			{
			  breakpoint: 768,
			  settings: {
				draggable: true,
				infinite: true,
				variableWidth: true,
				variableTransform: true,
				slidesToScroll: 1,	
			  }
			}
		]
	}); 
}

// if ($('.JSlike_slick').length){ 
// 	$('.JSlike_slick').slick({ 
// 		draggable: true,
// 		infinite: false,
// 		variableWidth: true,
// 		variableTransform: true,
// 		slidesToScroll: 1,
// 	}); 
// }

if ($('.JSyoulllike_slick').length){ 
$('.JSyoulllike_slick').slick({ 
		autoplay: false,
		slidesToShow: 6,
		slidesToScroll: 1, 
		responsive: [
			{
			  breakpoint: 1160,
			  settings: {
				slidesToShow: 4,
				slidesToScroll: 3
			  }
			},
			{
			  breakpoint: 880,
			  settings: {
				slidesToShow: 2,
				slidesToScroll: 2
			  }
			},
			{
			  breakpoint: 480,
			  settings: {
				slidesToShow: 2,
				slidesToScroll: 1
			  }
			}
		]
	}); 
}

if ($('.JSbanners_gallery_slick').length){ 
	$('.JSbanners_gallery_slick').slick({ 
		responsive: [
			{
			  breakpoint: 4000,
			  settings: {
				autoplay: true,
				slidesToShow: 4,
				slidesToScroll: 1, 
			  }
			},
			{
			  breakpoint: 1200,
			  settings: {
				slidesToShow: 3,
				slidesToScroll: 1,
			  }
			},
			{
			  breakpoint: 991,
			  settings: {
				slidesToShow: 2,
				slidesToScroll: 1,
				arrows: false,
				draggable: true,
			  }
			},
			{
			  breakpoint: 768,
			  settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: false,
				draggable: true,
			  }
			}
			// {
			//   breakpoint: 768,
			//   settings: {
			// 	draggable: true,
			// 	infinite: true,
			// 	variableWidth: true,
			// 	variableTransform: true,
			// 	slidesToScroll: 1,
			//   }
			// }
		]
	}); 
}

// IMAGES SLICK SLIDER 
if ($(window).width() < 1200) {
	$('.img-gallery-sidebar').addClass('JSimages_slick');
}

if ($('.JSimages_slick').length){ 
	$('.JSimages_slick').slick({ 
		autoplay: true,
		slidesToShow: 3,
		slidesToScroll: 1, 
		responsive: [
			{
			  breakpoint: 1200,
			  settings: {
				slidesToShow: 4,
				slidesToScroll: 1,
			  }
			},
			{
			  breakpoint: 991,
			  settings: {
				slidesToShow: 3,
				slidesToScroll: 1,
				arrows: false,
				draggable: true,
				dots: true,
			  }
			},
			// {
			//   breakpoint: 768,
			//   settings: {
			// 	slidesToShow: 1,
			// 	slidesToScroll: 1,
			// 	arrows: false,
			// 	draggable: true,
			// 	dots: true,
			//   }
			// },
			{
			  breakpoint: 768,
			  settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: false,
				draggable: true,
				dots: true
			  }
			}
		]
	}); 
}



// RELATED ARTICLES TITLE 
	$('.slickTitle').each(function(i, tit){
		if($(tit).next('.JSproducts_slick').find('.slick-track').children('.JSproduct ').length){
			$(tit).show();
		}
	})
  
	// if ($('.JSBrandSlider')[0]){
	// 	$('.JSBrandSlider').slick({
	// 		autoplay: true,
	// 	    infinite: true,
	// 	    speed: 600,
	// 	    arrows: true,
	// 		slidesToShow: 5,
	// 		slidesToScroll: 3, 
	// 		responsive: [
	// 			{
	// 			  breakpoint: 1100,
	// 			  settings: {
	// 				slidesToShow: 3,
	// 				slidesToScroll: 3,
	// 				infinite: true,
	// 				dots: false
	// 			  }
	// 			},
	// 			{
	// 			  breakpoint: 800,
	// 			  settings: {
	// 				slidesToShow: 2,
	// 				slidesToScroll: 2
	// 			  }
	// 			},
	// 			{
	// 			  breakpoint: 768,
	// 			  settings: {
	// 				slidesToShow: 2,
	// 				slidesToScroll: 2
	// 			  }
	// 			}
	// 		]
	// 	});
	// }

	if ($('.JSblog-slick').length){

		$('.JSblog-slick').slick({
			autoplay: true,
		    infinite: true,
		    speed: 600, 
			slidesToShow: 3,
			slidesToScroll: 1, 
			responsive: [
				{
				  breakpoint: 1100,
				  settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					infinite: true,
					dots: false
				  }
				},
				{
				  breakpoint: 800,
				  settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					arrows: false,
				  }
				},
				{
				  breakpoint: 768,
				  settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					arrows: false,
				  }
				}
			]
		});
	}
	  
 
	// CATEGORIES - MOBILE  
	
	$('.JSlevel-1 li').each(function(i, li){
		if($(li).children('ul').length){
			$(li).append('<span class="JSsubcategory-toggler"><i class="fas fa-plus"></i></span>');
		}
	});          

	$('.JSsubcategory-toggler').on('click', function() { 

		if ($(this).siblings('ul').css('display') == 'none') {
			$(this).children().removeClass('fas fa-plus').addClass('fas fa-minus'); 
			$(this).parent().siblings().find('span i').removeClass('fas fa-minus').addClass('fas fa-plus');  
		} else {
			$(this).children().removeClass('fas fa-minus').addClass('fas fa-plus');
		}
 
	    $(this).siblings('ul').slideToggle();
		// $(this).parent().siblings().children('ul').slideUp(); 
	});  
	
 
 // SOCIAL ICONS
	$('.social-icons .facebook').append('<i class="fab fa-facebook-f"></i>');
	$('.social-icons .twitter').append('<i class="fab fa-twitter"></i>');
	$('.social-icons .google-plus').append('<i class="fab fa-google-plus-g"></i>');
	$('.social-icons .skype').append('<i class="fab fa-skype"></i>');
	$('.social-icons .instagram').append('<i class="fab fa-instagram"></i>');
 	$('.social-icons .linkedin').append('<i class="fab fa-linkedin"></i>');
	$('.social-icons .youtube').append('<i class="fab fa-youtube"></i>'); 
 
 
	(function(){
 		var img_gallery_href = $('.JSimg-gallery'),
 			img_gallery_img = img_gallery_href.children('img'),
 			main_img = $('.JSmain_img'),
 			modal_img = $('#JSmodal_img'),
 			modal = $('.JSmodal'),

 			img_arr = [], 
			next_prev = 1;

		img_gallery_href.eq(0).addClass('galery_Active');

		img_gallery_href.on('click', function(){
			var img = $(this).children('img');
			main_img.attr({ src: img.attr('src'), alt: img.attr('alt') });
			$(this).addClass('galery_Active').siblings().removeClass('galery_Active');
		});

		main_img.on('click', function(){
			var src = $(this).attr('src');
			if (src.toLowerCase().indexOf('no-image') <= 0){
				modal_img.attr('src', src);
				$('.JSmodal').show();
			} 
		});

		$('.JSclose-modal').on('click', function(){
			modal.hide();
		});

		// $(document).on('click', function(){
		// 	if (modal.css('display') == 'block')  {
		// 		// $('body').css('overflow', 'hidden');
		// 		$('.d-content').addClass('overflow-hidden');
		// 	} else {
		// 		// $('body').css('overflow', '');  
		// 		$('.d-content').removeClass('overflow-hidden');
		// 	} 
		// });

		modal.on('click', function(e){
			if ( ! $('.JSmodal img, .JSmodal .btn, .JSmodal .btn > *').is(e.target)) {
				modal.hide();
			} 
		}); 

		img_gallery_img.each(function(i, img){
			img_arr.push($(img).attr('src'));  
		});

		$('.JSleft_btn').on('click', function(){ 
			modal_img.attr('src', img_arr[next_prev]); 
			 
			if (next_prev == img_gallery_img.length - 1) {
				next_prev = 0; 
			} else {
				next_prev++;
			}	  
		}); 

		$('.JSright_btn').on('click', function(){ 
			modal_img.attr('src', img_arr[next_prev]);
	 		next_prev--;

	 		if (next_prev < 0 ) {
	 			next_prev = img_gallery_img.length - 1;
	 		}   
		});  

		if (img_gallery_href.length > 1) {
			$('.modal-cont .btn').show();
		}
 
	}());
 
// ======== SEARCH ============
 
	var searchTime;
	$('#JSsearch2').on("keyup", function(event) {
		if(event.keyCode == 13 && $('#JSsearch2').val() != ''){
        	$(".JSsearch-button2").trigger("click");
    	}
    	if($('#elasticsearch').val() == 1){
			timer2();
    	}else{	
			clearTimeout(searchTime);
			searchTime = setTimeout(timer2, 300);
    	}
	});
 
	$('.JSSearchGroup2').change(function(){	timer2(); });

	function timer2(){
		var search_length = 2;
		if($('#elasticsearch').val() == 1){
			search_length = 0;
		}
		var search = $('#JSsearch2').val();
		if (search != '' && search.length > search_length) {
			$.post(base_url + 'livesearch', {search:search, grupa_pr_id: $('.JSSearchGroup2').val()}, function (response){
				if(response != ''){
					$('.JSsearch_list').remove();
					$('.JSsearchContent2').append(response);
				}
			});
		} 
	}

	$('html :not(.JSsearch_list)').on("click", function() {
		$('.JSsearch_list').remove();
	});
		 
// RATING STARS
	$('.JSrev-star i').each(function(i, el){
		var val = 0;
		$(el).on('click', function(){    
			$(el).addClass('fas').removeClass('far'); 
		    $(el).prevAll('i').addClass('fas').removeClass('far'); 
			$(el).nextAll('i').removeClass('fas').addClass('far'); 
		 	if (i == 0) { val = 1; }
		 	else if(i == 1){ val = 2; }
		 	else if(i == 2){ val = 3; }
		 	else if(i == 3){ val = 4; }
		 	else if(i == 4){ val = 5; } 
		 	$('#JSreview-number').val(val); 
		}); 
	}); 
 
 // CATHEGORIES HIGH
 // 	function action_manage(){  
 // 		var action_cont = $('.JSproducts_slick'), 
 // 			lvl_1_height = $('.JSlevel-1');

	// 	if (lvl_1_height.height() > 630 && ($(window).width() >= 1024)) {
	// 		action_cont.css('margin-left', '280px'); 
	// 	} 
	// }
 // 	action_manage(); 


	$('.JStoggle-btn').on('click', function(){
		$(this).closest('div').find('.JStoggle-content').toggleClass('hidden-small'); 
	});
	 
 // // RESPONSIVE NAVIGATON
 	$(".resp-nav-btn").on("click", function(){  
		$("#responsive-nav").toggleClass("open_cat");		 
	});
	$(".JSclose-nav").on("click", function(){  
		$("#responsive-nav").removeClass("open_cat");		 
	});
 
// SCROLL TO TOP 
 	$(window).scroll(function () {
        if ($(this).scrollTop() > 150) {
            $('.JSscroll-top').css('right', '20px');
        } else {
            $('.JSscroll-top').css('right', '-70px');
        }
    });
 
    $('.JSscroll-top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 600);
        return false;
    }); 
    
// filters slide down
	$(".JSfilters-slide-toggle").on('click', function(){
	    $(this).next(".JSfilters-slide-toggle-content").slideToggle('fast');

	    $(this).find('.JSfilter-arrow').toggleClass('ninty-degrees');

	 //    if ($(this).children().attr('class') == 'fas fa-plus') {
		// 	$(this).children().removeClass('fas fa-plus').addClass('fas fa-minus'); 
		// } else {
		// 	$(this).children().removeClass('fas fa-minus').addClass('fas fa-plus'); 
		// } 
	});

	if ($('.selected-filters li').length) {
		$('.JShidden-if-no-filters').show();
	} 
 
 // SELECT ARROW - FIREFOX FIX
	$('select').wrap('<span class="select-wrapper"></span>');
	 
// POPUP BANER
	if($('body').is('#start-page') && !(sessionStorage.getItem('popup')) ){   
		setTimeout(function(){ 
			$('.JSfirst-popup').animate({ top: '50%' }, 700);
		}, 1500);   

		$(document).on('click', function(e){
			var target = $(e.target);
			if(!target.is($('.popup-img'))) { 
				$('.JSfirst-popup').hide();
			} 
		}); 
		if ($('.JSfirst-popup').length) {
			sessionStorage.setItem('popup','1');
		}
	}
 
 
	if ($(window).width() > 991 ) { 

		// CATEGORIES WRAP
		$('.JSlevel-1 li').each(function () {	
			var subitems = $(this).find(".JSlevel-2 > li");
			for(var i = 0; i < subitems.length; i+=4) {
				subitems.slice(i, i+4).wrapAll("<div class='clearfix level-2-wrap'></div>");
			}
		});

	}

// ALL PRODUCTS WRAP 
if($(window).width() < 768 ) {

	var subitems = $('.JSproduct');
	for(var i = 0; i < subitems.length; i+=3) {
		if($('.JSproduct-list').length) {
			subitems.slice(i, i+3).wrapAll("<div class='clearfix flex products-set'></div>");
		} else {
			$('.JSslick-toggle').each(function(e) {
				if(!$(this).is('.JSproducts-slick')) {
					subitems.slice(i, i+3).wrapAll("<div class='clearfix flex products-set'></div>");
				}
			});
		}
	}
	$('.products-set').each(function() {
		var newsub = $(this).find(subitems);
		var count = 0;
		for(var i = 1; i < newsub.length+1; i++) { 
			count++;
		}
		if(count < 3) {
			$(this).addClass('JSproducts-wrap-fix');
		}
	});

// ALL CATEGORIES WRAP 
	$('.JScategory-wrap').each(function () {
		var subitems = $(this).find('.groups-list');
		for(var i = 0; i < subitems.length; i+=4) { 
			subitems.slice(i, i+4).wrapAll("<div class='clearfix flex categories-set'></div>");
		}	
		$('.categories-set').each(function() {
			var newsub = $(this).find(subitems);
			var count = 0;
			for(var i = 1; i < newsub.length+1; i++) { 
				count++;
			}
			if(count < 4) {
				$(this).addClass('JScategories-wrap-fix');
			}
		});
	});

// CATEGORIES SET WRAP
	var wrap = '<div class="categories-set-wrap"></div>';
	$('.JScategory-wrap').each(function(e){
		$(this).find('.categories-set').wrapAll(wrap);
	});

} else {
	$('.JScategory-wrap').each(function () {
		var subitems = $(this).find('.groups-list');
		subitems.wrapAll("<div class='categories-set-wrap'><div class='clearfix flex categories-set'></div></div>");
	});

}

// CATEGORIES WRAp
if($(window).width() < 480 ) {
// ALL CATEGORIES WRAP 
	$('.JScategory-wrap').each(function () {
		var subitems = $(this).find('.groups-list');
		for(var i = 0; i < subitems.length; i+=3) { 
			subitems.slice(i, i+3).wrapAll("<div class='clearfix flex categories-set'></div>");
		}	
		$('.categories-set').each(function() {
			var newsub = $(this).find(subitems);
			var count = 0;
			for(var i = 1; i < newsub.length+1; i++) { 
				count++;
			}
			if(count < 3) {
				$(this).addClass('JScategories-wrap-fix');
			}
		});
	});

	var subitems = $('.JSproduct');
	for(var i = 0; i < subitems.length; i+=2) {
		if($('.JSproduct-list').length) {
			subitems.slice(i, i+2).wrapAll("<div class='clearfix flex products-set'></div>");
		} else {
			$('.JSslick-toggle').each(function(e) {
				if(!$(this).is('.JSproducts-slick')) {
					subitems.slice(i, i+2).wrapAll("<div class='clearfix flex products-set'></div>");
				}
			});
		}
	}

	$('.products-set').each(function() {
		var newsub = $(this).find(subitems);
		var count = 0;
		for(var i = 1; i < newsub.length+1; i++) { 
			count++;
		}
		if(count < 2) {
			$(this).addClass('JSproducts-wrap-fix');
		}
	});
}

 // LEFT & RIGHT BODY LINK 
	if ($(window).width() > 1024 ) {  
		var right_link = $('.JSright-body-link'),
			left_link = $('.JSleft-body-link'),
			main = $('.JSmain'); 

		setTimeout(function(){ 
			main.before((left_link).css('top', main.position().top + 'px'));
			main.after((right_link).css('top', main.position().top + 'px'));
			left_link.width(($(window).outerWidth() - $('.container').outerWidth())/2).height(main.height()); 
			right_link.width(($(window).outerWidth() - $('.container').outerWidth())/2).height(main.height()); 

 		}, 1000); 
  	} 

// FOOTER COLS CLEARFIX
  	(function(){
  		var cols = $('.JSfooter-cols > div');
  		for(var i = 0; i < cols.length; i+=4){
  			cols.slice(i, i+4).wrapAll('<div class="clearfix JSfooter-secs"></div>');
  		}
  	}()); 

// SEARCH BY GROUP TEXT
	function selectTxt(){    
		var txt = $('.JSSearchGroup2 option:first').text(trans('Sve')); 
		// if ($(window).width() > 1024 ){ 
		// 	txt.text('Pretraga po kategorijama');  
		// }else{
		// 	txt.text('po kategorijama');  
		// }
	}
	selectTxt(); 

// OPEN SEARCH MODAL
	$('.JSopen-search-modal').on('click', function(){

		if($(window).width() > 768) {
			$('.JSsearch-modal').show();
			$('.JSopen-search-modal').hide();
		} 
		else {
			if($('.JSmain-slider').length || $('.JStip-slider-wrap').length) {
				if(!$('.open_cat').length) {
					$('.JSsearch-modal').addClass('JSsearch-absolute');
				}
				$('.JSsearch-modal').slideToggle();	
			} else {
				$('.JSsearch-modal').slideToggle();	
				$('.search-field').css('color', '#000');
			}
			$('.JSmain-slider').toggleClass('header-afterground');	
			$('.JStip-slider-wrap').toggleClass('header-afterground');	

			$('#responsive-nav').toggleClass('JSresponsive-pad-bot');
		}
	});

	$(document).on('click', function(e){
		if ($('.JSsearch-modal').css('display') == 'block') { 
			if ( ! $('.search-content *, .JSopen-search-modal, .JSopen-search-modal *').is(e.target)) { 
				if($('.JSmain-slider').length || $('.JStip-slider-wrap').length) {
					$('.JSsearch-modal').removeClass('JSsearch-absolute');
					$('.JSsearch-modal').toggle();	
				} else {
					$('.JSsearch-modal').toggle();	
				}
				$('#responsive-nav').removeClass('JSresponsive-pad-bot');
				$('.JSsearch-modal').slideUp();
				$('.JSopen-search-modal').show();
				if($(window).width() < 768) { 
					$('.JSmain-slider').removeClass('header-afterground');	
					$('.JStip-slider-wrap ').removeClass('header-afterground');	
				}
			} 
		} 
	});

// WISH COL MAKER 
	if($(window).width() > 768) {
		$('.wish-container .wish-remove').addClass('col-xs-1');
		$('.wish-container .product-image-wrapper').addClass('col-xs-3');
		$('.wish-container .product-meta').addClass('col-xs-7');
	}

	// if($(window).width() > 768) {
	// 	$('.JSopen-search-modal').on('click', function(){
	// 		$('.JSsearch-modal').show();
	// 		$('.JSopen-search-modal').hide();

	// 	});
	 
		
	// } else {
	// 	$('.JSopen-search-modal').on('click', function(){
	// 		$('.JSsearch-modal').toggleClass('JSflex-search');
	// 		$('.JSopen-search-modal').toggleClass('JSflex-search');
	// 		$('.bg-sprite').toggle();
	// 		$('.JScart_num').toggle();
	// 		$('.sm-border-box').toggle();
	// 	});
	 
	// 	$(document).on('click', function(e){
	// 		if ($('.JSsearch-modal').css('display') == 'flex') { 
	// 			if ( ! $('.search-content *, .JSopen-search-modal, .JSopen-search-modal *').is(e.target)) { 
	// 				$('.JSsearch-modal').toggleClass('JSflex-search');
	// 				$('.JSopen-search-modal').toggleClass('JSflex-search');
	// 				$('.bg-sprite').toggle();
	// 				$('.JScart_num').toggle();
	// 				$('.sm-border-box').toggle();
	// 			} 
	// 		}   
	// 	});
	// }
	
// / OPEN MENU MODAL
	var headerOffset = parseInt($('header').outerHeight());
		if($('#admin-menu').length) {
			headerOffset = parseInt(headerOffset + $('#admin-menu').outerHeight());
		};
		$(document).scroll(function() { 
		   if($(window).scrollTop() === 0) {
		     headerOffset = parseInt(headerOffset + $('#preheader').outerHeight());
		   }
		});
	var respHeight = $(window).height() - headerOffset;
	$('#responsive-nav').css('height', respHeight);

	$('.menu-open').on('click', function(){
		$('body').addClass('overflow-hidden').addClass('JSbody-after');
		$("#responsive-nav").show();	
		$('.menu-close').show();
		$('.JSmenu-bars').hide();
		$('header').addClass('imp-header-black-works');
		$('#JSfixed_header').addClass('background-white');
		$('.JSsearch-modal').removeClass('JSsearch-absolute');
		$('.JSsearch-modal').addClass('JSsearch-static');
		if($('.JSmain-slider').length || $('.JStip-slider-wrap').length) {
			$('.sm-border-box').toggleClass('border-box-light');
			$('.sm-border-box').toggleClass('border-box-dark');
		}
		if($('.JScart-bottom').is(':visible')) {
			$('.cart-user-type').hide();
		};

	});

	$('.menu-close').on('click', function(){
		$('body').removeClass('overflow-hidden').removeClass('JSbody-after');
		$("#responsive-nav").hide();	
		$('.menu-close').hide();
		$('.JSmenu-bars').show();
		$('header').removeClass('imp-header-black-works');
		$('#JSfixed_header').removeClass('background-white');
		$('.JSsearch-modal').removeClass('JSsearch-static');
		if($('.JSmain-slider').length || $('.JStip-slider-wrap').length) {
			$('.JSsearch-modal').addClass('JSsearch-absolute');
			$('.sm-border-box').toggleClass('border-box-light');
			$('.sm-border-box').toggleClass('border-box-dark');
		}
		if($('.JScart-bottom').is(':visible')) {
			$('.cart-user-type').show();
		};
	});


	// MENU WISH LIST 
	$('.JS-wish-btn').on('click', function(){
		$('.menu-back').show();
		$('.wish-list').show();
		$('.main-menu-content').hide();
	});
	$('.JS-user-btn').on('click', function(){
		$('.menu-back').show();
		$('.user-info').show();
		$('.main-menu-content').hide();
	});
	$('.menu-back').on('click', function(){
		$('.menu-back').hide();
		$('.wish-list').hide();
		$('.user-info').hide();
		$('.main-menu-content').show();
	});

	// CART USER TYPE
	$('.JScart-user-btn').on('click', function(){
		$('.JScart-user-btn').toggle();
		$('.JScart-user-1-np').toggle();
		$('.JScart-user-0-np').toggle();
	});

	$('.JScart-user-1-np, .JScart-user-0-np, .JScart-user-1-p, .JScart-user-0-p').on('click', function(){
		$('.JScart-user-1-p').toggle();
		$('.JScart-user-0-p').toggle();
		$('.JScart-user-1-np').toggle();
		$('.JScart-user-0-np').toggle();
		$('.form-holder').toggleClass('flex');
	});

	// GUEST USER MENU WHITE SPACE TRIM

	if( !$.trim( $(".menu-user").html() ) ) {
		$('.menu-user').css('padding', '0');
	};

	//USER FORGOTTEN PASSWORD SWITCH
	$('.forgot-psw').on('click', function(){
		$('.login-set').toggle();
		$('.password-set').toggle();
	});

	//PROMO KOD RESPONSIVE
	$('.promo-btn').on('click', function(){
		$('.cpn-wrap').slideToggle();
	});


	// COLOR CIRCLES LIMITER
	$('.color-scroll').each(function() {
	  var $colorCollection = $(this);
	  if ($colorCollection.find('.color_type').length > 4) { 
	      $colorCollection.find('.more-colors').show();
	  }
	});

	// EXAPND SLIDER
	$('.JStip-slider-wrap').append($('.expand-slider'));
	$('.expand-slider').on('click', function(){
		window.scrollBy({
			top: $('.JStip-slider-wrap').outerHeight() + $('#preheader').height(),
			behavior: 'smooth'
		});
	});

	// DESKTOP FLUID NO PADDING IF SLIDER
	if($('.JSmain-slider').length && $(window).width() > 768) {
		$('.container-fluid').css('padding', '0');
	}

	// FILTERS
	$('.JSfilters-btn').on('click', function(){
		$('.filters-wrap').toggle();
		$('.product-page').toggle();
		$('.JSfilters-label').toggle();
		$('.JSproducts-label').toggle();
		$('.JSfilter-sort-toggle').toggle();

	});

	// REMOVE MANUFAC CATS ON TIP PAGE
	if($('.JSmc-tip').length) {
		$('.JSmc-tip').parent().siblings().closest('.col-md-9').removeClass('col-md-9').addClass('col-md-12');
		$('.JSmc-tip').parent().siblings().closest('.above-products').hide();
	}

	// ORDER FOR BOUGHT TOGETHER
	if($(window).width() < 768) {
		$('.desc-wrap').after($('.bought-together'));
	};

	// HEIGHT FOR CART CONTENT 
	if($(window).width() < 768) {

		var cart_height = $(window).height() - $('header').outerHeight() - $('#preheader').outerHeight() - 20;

		if($('#admin-menu').length) {
			$('.bg-cont').css('min-height', cart_height - $('#admin-menu').outerHeight());
		} else {
			$('.bg-cont').css('min-height', cart_height);
		}

		// if($('.bg-cont').css('min-height') < ('500')) {
		// 	$('.bg-cont').css('min-height', '500px');
		// };
	};

	// COL REMOVE ON RESPONSIVE
	if($(window).width() < 768) {
		$('.resp-col-hide').hide();

		$('.JScart-item').each(function(e) {
			if($(this).find('.color-circle').css('background', 'hide')) {
				// $(this).hide();
			};
		});
	};

	// RESP HIDDEN OVERRIDE
	if($(window).width() > 768) {
		$('.resp-override').removeClass('hidden');
	};

	// WRAP CART CONTENT FOR RESPONSIVE
	if($(window).width() < 768) {
		var wrap = '<div class="resp-cart-wrap col-xs-8 no-padding"></div>';
		$('.JScart-item').each(function(e){
			$(this).find('.JSwrap').wrapAll(wrap);
		});
	};

	// RESP COUNT 
	if($(window).width() < 768) {
		$('.JScart-item').each(function(e){
			var target = $(this).find(".cart-img-wrap").find('.relative');
			$(this).find(".JScart-amount").appendTo(target);
		});
	};



	// JScategory-count-text

	var cat_amount = $('.category-count');
	cat_amount.each(function(){
		var cat_count = parseInt(cat_amount.val());
		if(cat_count % 10 != 1) {
			$(this).find('.JScategory-count-text').text(trans('artikla'));

		} else {
			$(this).find('.JScategory-count-text').text(trans('artikl'));
		}
	});

	// CATEGORIES DROP DOWN
	$('.category-dropdown-label').on('click', function() {
		$(this).parent().find('.categories-set-wrap').slideToggle();
		$(this).toggleClass('JScategory-open');
	});

	// REMOVE SECTION TITLE IF SLIDER ON BUT NOT ON BANNERS
	// if($('.JSmain-slider').length && !$('body').is('#start-page')) {
	// 	$('.section-title').each(function(e) {
	// 		if(!$(this).parent().parent().is('.slick-slider')) {
	// 			$(this).hide();
	// 		}
	// 	});
	// }	

	// HEIGHT FOR RESPONSIVE TIP SLIDER
	if ($(window).width() < 768 && $('.JStip-slider-wrap').length) {
		var adminHeight = 0;
		if($('#admin-menu').length) {
			adminHeight = $('#admin-menu').height();
		}
		$('.JStip-slider-wrap').find('.bg-img').css('padding-top', $(window).height() - adminHeight - $('.top-menu').height());
	}

	// BORDER BOX
	// if($(window).width() < 768) {
	// 	if($('.JSmain-slider').length || $('.JStip-slider-wrap').length) {
	// 		$('.sm-border-box').addClass('border-box-dark');
	// 	} else {
	// 		alert()
	// 		$('.sm-border-box').addClass('border-box-light');
	// 	}
	// }	

	// ARTICLE DETAILS EXTENDED PRODUCT INFO PLUCKED FROM SHORT DESC
	if($('body').is('#artical-page') && $('.short_description').length) {
		$('.short_description p').each(function(i, item) {
			var string = $(this).text(),
				leftString = string.split(':', 1),
				rightString = string.split(':').pop();

				leftString = leftString.concat(':');
			if(i == 0) {
				$('.JSinfoA1').append(leftString);
				$('.JSinfoA2').append(rightString);
			} 

			if(i == 1) {
				$('.JSinfoB1').append(leftString);
				$('.JSinfoB2').append(rightString);
			} 

			if(i == 2) {
				$('.JSinfoC1').append(leftString);
				$('.JSinfoC2').append(rightString);
			} 
		});
	}

	// READ MORE ON PRODUCT DESC
	if($('body').is('#artical-page') && $('.description-section').length) {
		var desc = $('.desc-sec-wrap'),
			shortDesc = $('.desc-short'),
			descHeight = parseInt($('.desc-sec-wrap').css('height'));
		if(descHeight > 40) {
			$('.JSread-more').show();
			$('.JSread-more').on('click', function() {
				desc.toggle();
				shortDesc.toggle();
				if(desc.height() == 40) {
					$('.JSread-more').text('Vidi više');
				} else {
					$('.JSread-more').text('Vidi manje');
				}
			});
		} else {
			$('.JSread-more').hide();

		}
	}

	// FOOTER APPEND
	$('.JSbgFooter').append($('.bg-footer-pages'));

	// BANNERS WIDTH
	if($(window).width() < 768) {
		$('.JSbanners_slick .banners').css('width', $(window).width() - 20);
		$('.JSbanners_gallery_slick .banners').css('width', $(window).width() - 20);
	}

	// REMOVE RELATED PRODUCTS ON DETAILS IF EMPTY
	if(!$('.JSbought_slick .JSproduct').length) {
		$('.bought-together').hide();
	}
	if(!$('.related-details .JSproduct').length) {
		$('.related-details').hide();
	}
	if(!$('.JSlike_slick .JSproduct').length) {
		$('.JSlike_slick').hide();
	}

	// USER BLACK BOX
	$(document).on('click','#JSUserAccountFormSubmit',function(){
		var formData = new FormData();

		$("#JSUserAccountForm").find('input').each(function(index,value){
			var input_element = $(value);
			formData.append(input_element.attr('name'),input_element.val());
		});

		$.ajax({
			url: base_url+'/korisnik-edit',
			type: 'POST',
			data: formData,
			async: false,
			cache: false,
			contentType: false,
			enctype: 'multipart/form-data',
			processData: false,
			success: function (response) {
				$("#JSImeError").text('');
				$("#JSPrezimeError").text('');
				$("#JSNazivError").text('');
				$("#JSPibError").text('');
				$("#JSEmailError").text('');
				$("#JSLozinkaError").text('');
				$("#JSTelefonError").text('');
				$("#JSAdresaError").text('');
				$("#JSMestoError").text('');
					
				if(response.success){
					alertSuccess(response.success_message);
				}else{

					if(response.error_messages.ime && response.error_messages.ime[0]){
						$("#JSImeError").text(response.error_messages.ime[0]);
					}
					if(response.error_messages.prezime && response.error_messages.prezime[0]){
						$("#JSPrezimeError").text(response.error_messages.prezime[0]);
					}
					if(response.error_messages.naziv && response.error_messages.naziv[0]){
						$("#JSNazivError").text(response.error_messages.naziv[0]);
					}
					if(response.error_messages.pib && response.error_messages.pib[0]){
						$("#JSPibError").text(response.error_messages.pib[0]);
					}
					if(response.error_messages.email && response.error_messages.email[0]){
						$("#JSEmailError").text(response.error_messages.email[0]);
					}
					if(response.error_messages.lozinka && response.error_messages.lozinka[0]){
						$("#JSLozinkaError").text(response.error_messages.lozinka[0]);
					}
					if(response.error_messages.telefon && response.error_messages.telefon[0]){
						$("#JSTelefonError").text(response.error_messages.telefon[0]);
					}
					if(response.error_messages.adresa && response.error_messages.adresa[0]){
						$("#JSAdresaError").text(response.error_messages.adresa[0]);
					}
					if(response.error_messages.mesto && response.error_messages.mesto[0]){
						$("#JSMestoError").text(response.error_messages.mesto[0]);
					}
				}
			}
		});

	});

	// DESC WRAP TAG REMOVER
	// function strip(html){
	//    let doc = new DOMParser().parseFromString(html, 'text/html');
	//    return doc.body.textContent || "";
	// }

	// var descShort = $('.desc-short-purgatory div').attr('data');
	// $('.desc-short-purgatory').remove();
	// $('.desc-short').text(strip(descShort));

	var desc_sec = $('.description-section'),
		desc_sec2 = $('.desc-short'),
		maxLength = 210,
		result = desc_sec.text().substring(0, maxLength) + '...';

	desc_sec2.text(result);

	$('.JSenquiry').click(function(){
		var obj = $(this);
		var roba_id = obj.data('roba_id');
		$('#JSStateArtID').val(roba_id);
		$('#checkState').modal('show');
	});
	
	 // QUICK VIEW
	$(document).on('click', '.JSQuickViewButton', function() {
		$('#JSQuickView').modal('show');
		$.post(base_url + 'quick-view', { roba_id: $(this).data('roba_id') }, function(response) {
			$('#JSQuickViewContent').html(response);
		});	
	});
	$(document).on('click', '#JSQuickViewCloseButton', function() { 
		$('#JSQuickView').modal('hide');
	});

	$("#JSConditionsCart").change(function() {
    if(this.checked) {
        $('#JSOrderSubmit').removeAttr('disabled');
    } else {
        $('#JSOrderSubmit').attr('disabled', 'disabled');
    }
	});

	$('.payment-method').on('change', function() {
		if($('#pay-method-3').is(':checked')) {
			$('.JStosFlag').hide();
	        $('#JSOrderSubmit').removeAttr('disabled');
		} else {
			$('.JStosFlag').show();
		}
	});

}); // DOCUMENT READY END
 
// Slickify 

if($('body').is('#start-page')) {
	$('.JSslick-toggle').addClass('JSproducts_slick');
	$('.responsive-banner').addClass('JSbanners_slick');
} else {
	$('.products-on-list').hide();
	if($(window).width() > 768) {
		$('.responsive-banner').addClass('JSbanners_slick');
	}
}

// ALERTIFY
function alertSuccess(message) {
	swal(message, "", "success");
		
	setTimeout(function() {
		swal.close();  
	}, 2000);
}	

function alertError(message) {
	sweetAlert(message, "", "error");
	setTimeout(function() {
		sweetAlert.close();  
	}, 2000);
}
 

// ZOOMER 
document.onreadystatechange = function(){
    if (document.readyState === "complete") { 

		$('.product-image').each(function(i, img){
			if ($(img).attr('src') != 'quick_view_loader.gif') {
				$(img).attr('src', '/images/no-image.jpg').addClass('img-responsive'); 
			}
		});   

		// IMAGE REPLACEMENT
		if($(window).width() > 1024) {
			$('.product-image-wrapper').on('mouseenter', function(){  

				var img = $(this).find('img'),
					placeholder = $(this).find('.JSimg-placeholder');
	 			
	 			if(img.attr('src').includes('no-image')) {
		 
					img.siblings('.JSimg-placeholder').remove();

			  	} else if (img.attr('src') != placeholder.data('src') && placeholder.length) {
 
					sessionStorage.setItem('src', img.attr('src')); 

					img.attr('src', placeholder.data('src')).hide().show();  

			  	} 

			}).on('mouseleave', function(){
				var img = $(this).find('img');
	 
	 			if (sessionStorage.getItem('src')) {
					
					img.attr('src', sessionStorage.getItem('src')).hide().show();

					sessionStorage.removeItem('src');
				}    
			});
			// IF IMAGE CLICKED
			sessionStorage.removeItem('src');
		}
		
	} 


 	$(document).on('click','.JSrelated_color',function(){
 		var roba_id = $(this).find('span').data('roba_id'),
 		    srodni_id = $(this).find('span').data('srodni_id');

 		$('.JSadd-to-cart[data-org_roba_id="'+roba_id+'"]').data('roba_id',srodni_id);

 		// $(this).addClass('active-color').siblings().removeClass('active-color');

 		// $('.JSrelated_img[data-roba_id="' + roba_id + '"]').addClass('hidden');
 		// $('.JSrelated_img[data-srodni_id="' + srodni_id + '"]').removeClass('hidden');
 		$(this).closest('.product-meta').siblings('.product-image-wrapper').find($('.JSrelated_img[data-roba_id="' + roba_id + '"]')).addClass('hidden');
	 	$(this).closest('.product-meta').siblings('.product-image-wrapper').find($('.JSrelated_img[data-srodni_id="' + srodni_id + '"]')).removeClass('hidden');
 		
 	});

}


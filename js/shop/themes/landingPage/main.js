
$(document).ready(function () { 


	(function(d, s, id){
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) {return;}
		js = d.createElement(s); js.id = id;
		js.src = "https://connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));


 // SLICK SLIDER INCLUDE  
	 if($('#admin-menu').length){
	 	$('.JSmain-slider').slick({
	 		autoplay: false,
	 		draggable: false
	 	});
	 }else{
	 	$('.JSmain-slider').slick({
	 		autoplay: true
	 	});
	 }  

 /*  SLICK NEWS  */  
	 if ($('.JSblog-slick').length){ 
	 	$('.JSblog-slick').slick({
	 		autoplay: true,
	 		infinite: true,
	 		speed: 600, 
	 		slidesToShow: 3,
	 		slidesToScroll: 1, 
	 		responsive: [
	 		{
	 			breakpoint: 1100,
	 			settings: {
	 				slidesToShow: 3,
	 				slidesToScroll: 1,
	 				infinite: true,
	 				dots: false
	 			}
	 		},
	 		{
	 			breakpoint: 800,
	 			settings: {
	 				slidesToShow: 2,
	 				slidesToScroll: 2,
	 				arrows: false,
	 			}
	 		},
	 		{
	 			breakpoint: 480,
	 			settings: {
	 				slidesToShow: 1,
	 				slidesToScroll: 1,
	 				arrows: false,
	 			}
	 		}
	 		]
	 	});
	 }
 

	 // SOCIAL ICONS
	 $('.social-icons .facebook').append('<i class="fab fa-facebook-f"></i>');
	 $('.social-icons .twitter').append('<i class="fab fa-twitter"></i>');
	 $('.social-icons .google-plus').append('<i class="fab fa-google-plus-g"></i>');
	 $('.social-icons .skype').append('<i class="fab fa-skype"></i>');
	 $('.social-icons .instagram').append('<i class="fab fa-instagram"></i>');
	 $('.social-icons .linkedin').append('<i class="fab fa-linkedin"></i>');
	 $('.social-icons .youtube').append('<i class="fab fa-youtube"></i>'); 


 // SCROLL TO TOP
	 $(window).scroll(function () {
	 	if ($(this).scrollTop() > 150) { $('.JSscroll-top').css('right', '20px'); }
	 	else { $('.JSscroll-top').css('right', '-70px'); }
	 });
	 
	 $('.JSscroll-top').click(function () {
	 	$('body,html').animate({
	 		scrollTop: 0
	 	}, 600);
	 	return false;
	 });
 
// SELECT ARROW - FIREFOX FIX 

	/* Moblie navigation */
	if($(window).width() < 991) { 
		$('.menu-toggle').click(function() {
			$('.menu').toggleClass('open-menu');  
		}); 
		$('.close-nav').click(function() {
			$('.menu').removeClass('open-menu'); 
		});  
	}

// POPUP 
	if ($(window).width() > 991 ) {
		
	// POPUP BANER 
		if($('body').is('#start-page') && !(sessionStorage.getItem('popup')) ){   
			setTimeout(function(){ 
				$('.JSfirst-popup').animate({ top: '50%' }, 700);
			}, 1500);   

			$(document).on('click', function(e){
				var target = $(e.target);
				if(!target.is($('.popup-img'))) { 
					$('.JSfirst-popup').hide();
				} 
			}); 
			if ($('.JSfirst-popup').length) {
				sessionStorage.setItem('popup','1');
			}
		}
 	}
 
// GALLERY SLIDER
   	(function(){
 		var img_gallery_href = $('.JSimg-gallery'),
 			img_gallery_img = img_gallery_href.children('img'),
 			main_img = $('.JSmain_img'),
 			modal_img = $('#JSmodal_img'),
 			modal = $('.JSmodal'),

 			img_arr = [], 
			next_prev = 1;

		img_gallery_href.eq(0).addClass('galery_Active');

		img_gallery_href.on('click', function(){
			var img = $(this).children('img');
			main_img.attr({ src: img.attr('src'), alt: img.attr('alt') });
			$(this).addClass('galery_Active').siblings().removeClass('galery_Active');
		});

		main_img.on('click', function(){
			var src = $(this).attr('src');
			if (src.toLowerCase().indexOf('no-image') <= 0){
				modal_img.attr('src', src);
				$('.JSmodal').show();
			} 
		});

		$('.JSclose-modal').on('click', function(){
			modal.hide();
		});

		$(document).on('click', function(){
			if (modal.css('display') == 'block')  {
				$('body').css('overflow', 'hidden');
			} else {
				$('body').css('overflow', '');  
			} 
		});

		modal.on('click', function(e){
			if ( ! $('.JSmodal img, .JSmodal .btn, .JSmodal .btn > *').is(e.target)) {
				modal.hide();
			} 
		}); 

		img_gallery_img.each(function(i, img){
			img_arr.push($(img).attr('src'));  
		});

		$('.JSleft_btn').on('click', function(){ 
			modal_img.attr('src', img_arr[next_prev]); 
			 
			if (next_prev == img_gallery_img.length - 1) {
				next_prev = 0; 
			} else {
				next_prev++;
			}	  
		}); 

		$('.JSright_btn').on('click', function(){ 
			modal_img.attr('src', img_arr[next_prev]);
	 		next_prev--;

	 		if (next_prev < 0 ) {
	 			next_prev = img_gallery_img.length - 1;
	 		}   
		});  

		if (img_gallery_href.length > 1) {
			$('.modal-cont .btn').show();
		}
 
	}());

}); // DOCUMENT READY END



// SWEET ALERT
function alertSuccess(message) {
	swal(message, "", "success"); 
	setTimeout(function() {
		swal.close();  
	}, 2000);
}	
 
function alertError(message) {
	sweetAlert(message, "", "error");
	setTimeout(function() {
		sweetAlert.close();  
	}, 2000);
}	


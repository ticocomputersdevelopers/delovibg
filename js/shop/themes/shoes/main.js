
$(document).ready(function () {
// LAZY LOAD IMAGES
	$(function() {
		// change data-src to src to disable plugin
		// 1.product_on_grid
		// 2.product_on_list
		// 3.footer
        $('.JSlazy_load').Lazy({
        	scrollDirection: 'horizontal'
        });    
    });

	  // IF THERE IS NO IMAGE
	$('img').on('error', function(){ 
		$(this).attr('src', '/images/no-image.jpg').addClass('img-responsive');
	});

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

// *********************************************************  
// var found_type = {};
//   $('.filter-label').each(function(){
//     var $this = $(this);
//     if(found_type[$this.attr('value')]){
//       $this.parent().remove();
//     }else{
//       found_type[$this.attr('value')] = true;
//     }
//   });

// REMOVE EMPTY LOGGED USER
$('.logged-user').each(function(i, el){ 
	if ( ($(el).text().trim() == '')) {
		$(el).hide();
	}
});

// SLICK SLIDER INCLUDE
if ($('.JSmain-slider').length) {
	if($('#admin_id').val()){

		$('.JSmain-slider').slick({
			autoplay: false,
			draggable: false,
			dots: true
		});

	}else{
		$('.JSmain-slider').slick({
			autoplay: true,
			dots: true
		});
	} 
}

if ($('.JSblanco_slick').length){ 
	$('.JSblanco_slick').slick({ 
		arrows: true,
		autoplay: true,
		slidesToShow: 5,
		slidesToScroll: 1,
		dots: false,
		responsive: [
			{
			  breakpoint: 991,
			  settings: {
				slidesToShow: 4,
				slidesToScroll: 1,
				draggable: true
			  }
			},
			{
			  breakpoint: 768,
			  settings: {
				slidesToShow: 3,
				slidesToScroll: 1,
				arrows: true,
				draggable: true
			  }
			},
			{
			  breakpoint: 400,
			  settings: {
				slidesToShow: 2,
				slidesToScroll: 1,
				arrows: true,
				draggable: true
			  }
			},
		]

		
	}); 
}
// PRODUCTS SLICK SLIDER 
if ($('.JSproducts_slick').length){
	$('.JSproducts_slick').slick({ 
		autoplay: true,
		slidesToShow: 6,
		slidesToScroll: 1, 
		responsive: [
			{
			  breakpoint: 1160,
			  settings: {
				slidesToShow: 5,
				slidesToScroll: 1
			  }
			},
			{
			  breakpoint: 880,
			  settings: {
				slidesToShow: 3,
				slidesToScroll: 1,
				arrows: false
			  }
			},
			{
			  breakpoint: 480,
			  settings: {
				slidesToShow: 2,
				slidesToScroll: 1,
				arrows: false
			  }
			}
		]
	}); 
}

if ($('.JSbanners_slick').length){
	$('.JSbanners_slick').slick({ 
		autoplay: true,
		slidesToShow: 4,
		slidesToScroll: 1, 
		responsive: [
			{
			  breakpoint: 1160,
			  settings: {
				slidesToShow: 3,
				slidesToScroll: 3
			  }
			},
			{
			  breakpoint: 880,
			  settings: {
				slidesToShow: 2,
				slidesToScroll: 2,
				arrows: false
			  }
			},
			{
			  breakpoint: 480,
			  settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: false
			  }
			}
		]
	}); 
}

if ($('.JSsmall-bann_slick').length){
	$('.JSsmall-bann_slick').slick({ 
		autoplay: true,
		slidesToShow: 7,
		slidesToScroll: 1, 
		responsive: [
			{
			  breakpoint: 1160,
			  settings: {
				slidesToShow: 5,
				slidesToScroll: 1
			  }
			},
			{
			  breakpoint: 880,
			  settings: {
				slidesToShow: 4,
				slidesToScroll: 1,
				arrows: false
			  }
			},
			{
			  breakpoint: 480,
			  settings: {
				slidesToShow: 3,
				slidesToScroll: 1,
				arrows: false
			  }
			}
		]
	}); 
}
 
// GROUPS SLICK SLIDER 
if ($('.JSgroups_slick').length){

	$('.JSgroups_slick').slick({ 
		autoplay: true,
		slidesToShow: 8,
		slidesToScroll: 1, 
		responsive: [
			{
			  breakpoint: 1200,
			  settings: {
				slidesToShow: 6,
				slidesToScroll: 1
			  }
			},
			{
			  breakpoint: 880,
			  settings: {
				slidesToShow: 3,
				slidesToScroll: 1,
				arrows: false
			  }
			},
			{
			  breakpoint: 480,
			  settings: {
				slidesToShow: 2,
				slidesToScroll: 1,
				arrows: false,
				centerMode: true
			  }
			}
		]
	}); 
}

if ($('.JSlinked_products').length){

	$('.JSlinked_products').slick({ 
		autoplay: true,
		slidesToShow: 6,
		slidesToScroll: 1, 
		responsive: [
			{
			  breakpoint: 1160,
			  settings: {
				slidesToShow: 5,
				slidesToScroll: 1
			  }
			},
			{
			  breakpoint: 880,
			  settings: {
				slidesToShow: 3,
				slidesToScroll: 1,
				arrows: false
			  }
			},
			{
			  breakpoint: 480,
			  settings: {
				slidesToShow: 2,
				slidesToScroll: 1,
				arrows: false
			  }
			}
		]
	}); 
}
// RELATED ARTICLES TITLE 
$('.slickTitle').each(function(i, tit){
	if($(tit).next('.JSproducts_slick, .JSlinked_products').find('.slick-track').children('.JSproduct').length){
		$(tit).show();
	}
	else {
		$(this).parent().removeClass('product-padding');
	}
})
	// if ($('.JSBrandSlider')[0]){
	// 	$('.JSBrandSlider').slick({
	// 		autoplay: true,
	// 	    infinite: true,
	// 	    speed: 600,
	// 	    arrows: true,
	// 		slidesToShow: 5,
	// 		slidesToScroll: 3, 
	// 		responsive: [
	// 			{
	// 			  breakpoint: 1100,
	// 			  settings: {
	// 				slidesToShow: 3,
	// 				slidesToScroll: 3,
	// 				infinite: true,
	// 				dots: false
	// 			  }
	// 			},
	// 			{
	// 			  breakpoint: 800,
	// 			  settings: {
	// 				slidesToShow: 2,
	// 				slidesToScroll: 2
	// 			  }
	// 			},
	// 			{
	// 			  breakpoint: 480,
	// 			  settings: {
	// 				slidesToShow: 2,
	// 				slidesToScroll: 2
	// 			  }
	// 			}
	// 		]
	// 	});
	// }

	if ($('.JSblog-slick').length){

		$('.JSblog-slick').slick({
			autoplay: true,
		    infinite: true,
		    speed: 600, 
			slidesToShow: 4,
			slidesToScroll: 1, 
			responsive: [
				{
				  breakpoint: 1100,
				  settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
					infinite: true,
					dots: false
				  }
				},
				{
				  breakpoint: 800,
				  settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
					arrows: false,
				  }
				},
				{
				  breakpoint: 480,
				  settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					arrows: false,
					centerMode: true
				  }
				}
			]
		});
	}
	 
	if ($('.JSimages_slick_gall').length){

		$('.JSimages_slick_gall').slick({ 
			autoplay: true,
			slidesToShow: 3,
			slidesToScroll: 1, 
			responsive: [
				{
				  breakpoint: 992,
				  settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
					arrows: false
				  }
				},
				{
				  breakpoint: 770,
				  settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					arrows: false,
					dots: true
				  }
				}
			]
		}); 
	}

 	
 	// HEIGHT OF RESPONSIVE NAV
 	var header_height = $('header').height(),
 		preheader = $('#preheader'),
 		admin_menu = $('#admin-menu'),
		height = preheader.length ? header_height + preheader.height() + 100 : header_height + 100,
		total_height = admin_menu.length ? height + admin_menu.height() : height;

		$("#responsive-nav >  div").css('height', 'calc(100vh - ' + total_height + 'px)');

	// CATEGORIES - MOBILE  
	
	$('.JSlevel-1 li').each(function(i, li){
		if($(li).children('ul').length){
			$(li).append('<span class="JSsubcategory-toggler text-right"><i class="fa fa-caret-right"></i></span>');
		}
	});          

	$('.JSsubcategory-toggler').on('click', function() { 

		if ($(this).siblings('ul').css('display') == 'none') {
			$(this).children().addClass('JSrotateArrow'); 
			$(this).parent().siblings().find('span i').removeClass('JSrotateArrow');  
		} else {
			$(this).children().removeClass('JSrotateArrow').addClass('fa fa-caret-right');
		}
 
	    $(this).siblings('ul').slideToggle();
		$(this).parent().siblings().children('ul').slideUp(); 
	});  

	$('.JSopen_comments').on('click', function() { 
		$('#the-comments').slideToggle('fast');
		$(this).find('span').eq(1).toggleClass('JSrotateArrow180');
	});  

	

	// OPEN CATEGORIES LEVEL 2 - PAGE ALL CAT
	$('.JSarrow-oppen-cat').on('click', function(){
		$(this).parent().next('div').slideToggle('fast');
		$(this).children('i').toggleClass('JSrotateArrow');
	});

	// WRAPPER - PAGE ALL CAT
	$('.all-lvl2-wrapp').each(function () {
		var subitems = $(this).find('.groups-list');
		var item = 
		$(window).width() < 479  ? 3 : 
		$(window).width() > 479 && $(window).width() < 768 ? 4 : 
		$(window).width() > 768 && $(window).width() < 991 ? 5 : 
		$(window).width() > 991 && $(window).width() < 1200 ? 7 :
		$(window).width() > 1200 ? 9 : '';
		
		for(var i = 0; i < subitems.length; i+=item) { 
			subitems.slice(i, i+item).wrapAll("<div class='clearfix'></div>");
		}	
	});

 	// SOCIAL ICONS
	$('.social-icons .facebook').append('<i class="fab fa-facebook-f"></i>');
	$('.social-icons .twitter').append('<i class="fab fa-twitter"></i>');
	$('.social-icons .google-plus').append('<i class="fab fa-google-plus-g"></i>');
	$('.social-icons .skype').append('<i class="fab fa-skype"></i>');
	$('.social-icons .instagram').append('<i class="fab fa-instagram"></i>');
 	$('.social-icons .linkedin').append('<i class="fab fa-linkedin"></i>');
	$('.social-icons .youtube').append('<i class="fab fa-youtube"></i>'); 
 
	// GALLERY
	(function(){
 		var img_gallery_href = $('.JSimg-gallery'),
 			img_gallery_img = img_gallery_href.children('img'),
 			main_img = $('.JSmain_img'),
 			modal_img = $('#JSmodal_img'),
 			modal = $('.JSmodal'),

 			img_arr = [], 
			next_prev = 1;

		img_gallery_href.eq(0).addClass('galery_Active');

		img_gallery_href.on('click', function(){
			var img = $(this).children('img');
			main_img.attr({ src: img.attr('src'), alt: img.attr('alt') });
			$(this).addClass('galery_Active').siblings().removeClass('galery_Active');
		});

		main_img.on('click', function(){
			var src = $(this).attr('src');
			if (src.toLowerCase().indexOf('no-image') <= 0){
				modal_img.attr('src', src);
				$('.JSmodal').show();
			} 
		});

		$('.JSclose-modal').on('click', function(){
			modal.hide();
		});

		$(document).on('click', function(){
			if (modal.css('display') == 'block')  {
				$('body').css('overflow', 'hidden');
			} else {
				$('body').css('overflow', '');  
			} 
		});

		modal.on('click', function(e){
			if ( ! $('.JSmodal img, .JSmodal .btn, .JSmodal .btn > *').is(e.target)) {
				modal.hide();
			} 
		}); 

		img_gallery_img.each(function(i, img){
			img_arr.push($(img).attr('src'));  
		});

		$('.JSleft_btn').on('click', function(){ 
			modal_img.attr('src', img_arr[next_prev]); 
			 
			if (next_prev == img_gallery_img.length - 1) {
				next_prev = 0; 
			} else {
				next_prev++;
			}	  
		}); 

		$('.JSright_btn').on('click', function(){ 
			modal_img.attr('src', img_arr[next_prev]);
	 		next_prev--;

	 		if (next_prev < 0 ) {
	 			next_prev = img_gallery_img.length - 1;
	 		}   
		});  

		if (img_gallery_href.length > 1) {
			$('.modal-cont .btn').show();
		}
 
	}());
 
// ======== SEARCH ============
 
	var searchTime;
	$('#JSsearch2').on("keyup", function(event) {
		if(event.keyCode == 13 && $('#JSsearch2').val() != ''){
        	$(".JSsearch-button2").trigger("click");
    	}
    	if($('#elasticsearch').val() == 1){
			timer2();
    	}else{	
			clearTimeout(searchTime);
			searchTime = setTimeout(timer2, 300);
    	}
	});
 
	$('.JSSearchGroup2').change(function(){	timer2(); });

	function timer2(){
		var search_length = 2;
		if($('#elasticsearch').val() == 1){
			search_length = 0;
		}
		var search = $('#JSsearch2').val();
		if (search != '' && search.length > search_length) {
			$.post(base_url + 'livesearch', {search:search, grupa_pr_id: $('.JSSearchGroup2').val()}, function (response){
				if(response != ''){
					$('.JSsearch_list').remove();
					$('.JSsearchContent2').append(response);
				}
			});
		} 
	}

	$('html :not(.JSsearch_list)').on("click", function() {
		$('.JSsearch_list').remove();
	});
		 
// RATING STARS
	$('.JSrev-star i').each(function(i, el){
		var val = 0;
		$(el).on('click', function(){    
			$(el).addClass('fas').removeClass('far'); 
		    $(el).prevAll('i').addClass('fas').removeClass('far'); 
			$(el).nextAll('i').removeClass('fas').addClass('far'); 
		 	if (i == 0) { val = 1; }
		 	else if(i == 1){ val = 2; }
		 	else if(i == 2){ val = 3; }
		 	else if(i == 3){ val = 4; }
		 	else if(i == 4){ val = 5; } 
		 	$('#JSreview-number').val(val); 
		}); 
	}); 
 
  
	$('.JStoggle-btn').on('click', function(){
		$(this).closest('div').find('.JStoggle-content').toggleClass('hidden-small'); 
	});
	 
 // RESPONSIVE NAVIGATON
 // 	$(".resp-nav-btn").on("click", function(){  
	// 	$("#responsive-nav").toggleClass("JSopen_cat");		 
	// });
	// $(".JSclose-nav").on("click", function(){  
	// 	$("#responsive-nav").removeClass("JSopen_cat");		 
	// });

	$(".JSmeni").on("click", function(){
 		$('.resp-nav-btn').toggleClass('close-resp-nav');  
		$("#responsive-nav").toggleClass('JSopen_cat');
		$('.JSopen-search-modal').parent('li').removeClass('JSopened-search');

		if ($(".JSopen_cat").length)  {
			$('body').addClass('hidden-ovf');
		} else {
			$('body').removeClass('hidden-ovf');
		} 
	});
 
// SCROLL TO TOP 
 	$(window).scroll(function () {
        if ($(this).scrollTop() > 150) {
            $('.JSscroll-top').css('right', '20px');
        } else {
            $('.JSscroll-top').css('right', '-70px');
        }
    });
 
    $('.JSscroll-top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 600);
        return false;
    }); 

    // SCROLL DOWN ON BANNER ARROW
    $('.full-w-banner').each(function(){
	    var ful_w_wrapp = $(this).closest('.sec-wrapp'),
	        sec_wrapp = $('.sec-wrapp'),
	        scrol_down = $(this).find('.JSscroll-down');
	    
	    if(ful_w_wrapp.next(sec_wrapp).length) {
	    	scrol_down.removeClass('hidden');
	        scrol_down.on('click', function(){
	            $('body,html').animate({
	                scrollTop: 
	                $(window).width() > 991 ? 
	                sec_wrapp.offset().top + ful_w_wrapp.height() - header.height() : 
	                sec_wrapp.offset().top + ful_w_wrapp.height()
	            }, 400);
	        });
	    }
	});
    
    
// filters slide down
	$(".JSfilters-slide-toggle").on('click', function(){
	    $(this).next(".JSfilters-slide-toggle-content").slideToggle('fast');
  
		// if ($(this).children().attr('class') == 'fas fa-plus') {
		// 	$(this).children().removeClass('fas fa-plus').addClass('fas fa-minus'); 
		// } else {
		// 	$(this).children().removeClass('fas fa-minus').addClass('fas fa-plus'); 
		// } 
	  	$(this).children('i').toggleClass('JSrotateArrow')
	});

	$('.JSopenFilters, .close-filt').on('click', function(){
	    $('.JSfilter-mob, .product-page, .products-top-options').toggleClass('hidden-xs');
	});

	if ($('.selected-filters li').length) { 
		$('.JShidden-if-no-filters').show();
	} 	
 
 // SELECT ARROW - FIREFOX FIX
	$('select').wrap('<span class="select-wrapper"></span>');
	 

// POPUP BANER 
	if($('body').is('#start-page') && !(sessionStorage.getItem('popup')) ){   
		setTimeout(function(){ 
			$('.JSfirst-popup').animate({ top: '50%' }, 700);
		}, 1500);   

		$(document).on('click', function(e){
			var target = $(e.target);
			if(!target.is($('.popup-img'))) { 
				$('.JSfirst-popup').hide();
			} 
		}); 
		if ($('.JSfirst-popup').length) {
			sessionStorage.setItem('popup','1');
		}
	}
	
	if ($(window).width() < 991 ) {
		$('#main-menu').append($('.JSpage-menu-top'));
	}

	if ($(window).width() > 991 ) {
 
		// CATEGORIES WRAP
		// $('.JSlevel-1 li').each(function () {	
		// 	var subitems = $(this).find(".JSlevel-2 > li");
		// 	for(var i = 0; i < subitems.length; i+=4) {
		// 		subitems.slice(i, i+4).wrapAll("<div class='clearfix level-2-wrap'></div>");
		// 	}
		// });

		// FOOTER COLS CLEARFIX
	  	(function(){
	  		var cols = $('.JSfooter-cols > div');
	  		for(var i = 0; i < cols.length; i+=3){
	  			cols.slice(i, i+3).wrapAll('<div class="clearfix JSfooter-secs"></div>');
	  		}
	  	}()); 
 
 	 // SLIDER MARGIN && ANIMATION
	 	if($('body').is('#start-page')){   
		 	$('.JSmain-slider').each(function(i, el){
		 		if ($(el).offset().top < $('header').offset().top + $('header').outerHeight()) {
		 			$(el).css('margin-top', - $('header').outerHeight());
		 		} else {
		 			$(el).css('margin-top', 0);
		 		}
		 		$(el).addClass('fadeIn'); 
		 	}); 
		}
  	 
	}
 

 // LEFT & RIGHT BODY LINK
	if ($(window).width() > 1024 ) {  
		var right_link = $('.JSright-body-link'),
			left_link = $('.JSleft-body-link'),
			main = $('.JSmain'); 

		setTimeout(function(){ 
			main.before((left_link).css('top', main.position().top + 'px'));
			main.after((right_link).css('top', main.position().top + 'px'));
			left_link.width(($(window).outerWidth() - $('.container').outerWidth())/2).height(main.height()); 
			right_link.width(($(window).outerWidth() - $('.container').outerWidth())/2).height(main.height()); 

		}, 1000); 
  	} 

 

// SEARCH BY GROUP TEXT
	function selectTxt(){    
		// var txt = $('.JSSearchGroup2 option:first').text(trans('Sve')); 
		// if ($(window).width() > 1024 ){ 
		// 	txt.text('Pretraga po kategorijama');  
		// }else{
		// 	txt.text('po kategorijama');  
		// }
	}
	selectTxt(); 

// OPEN SEARCH MODAL
	$('.JSopen-search-modal').on('click', function(){
		$('.JSopen-search-modal').parent('li').toggleClass('JSopened-search');
		if ($(window).width() < 991 ) {
			$('#responsive-nav').removeClass('JSopen_cat');
			$('.resp-nav-btn').removeClass('close-resp-nav');
		}
	});
 
	// $(document).on('click', function(e){  

	// 	if($(window).width() > 991 ) {
	// 		if ( ! $('header *').is(e.target)) { 
	// 			$('.JSopen-search-modal').parent('li').removeClass('JSopened-search');
	// 		}  
	// 	} else {
	// 		if ( ! $('.search-content *, .JSopen-search-modal, .JSopen-search-modal *').is(e.target)) { 
	// 			$('.JSopen-search-modal').parent('li').removeClass('JSopened-search');
	// 		}  
	// 	}
	// });

	// SHOW PASS LOGIN
    $(".show-password").click(function(){
    	var pass_login = $('.JSpassword_login'),
    		pass_login2 = $('.JSpassword_login2');

    	pass_login2.val(pass_login.val());
	    pass_login2.on('change', function(){
	        pass_login.val(pass_login2.val());
	    });
	    $(this).find('span').toggleClass('fas fa-eye-slash fa fa-eye');
	   	pass_login2.toggle();
	    pass_login.toggle();
	});

	// SHOW FORGOTTEN PASS PART
	$('.JScall_forgot_pass').on('click', function(){ 
		$('.JSshow_forgot_psw').removeClass('hidden');
		$('.JSshow_login').addClass('hidden');
	});


	// CHANGE WISH LIST AND USER EDIT ON LOGIN
    (function(){
        $('.JScall-wish').on('click', function(){
            localStorage.setItem('callWish',1);
            localStorage.removeItem('callUserEdit');
            $('.user-wrapp').addClass('JSactive-wish-part');
        });
        if(localStorage.getItem('callWish')) {
            $('.user-wrapp').addClass('JSactive-wish-part');
        }
        $('.JScall-edit-user').on('click', function(){
            localStorage.setItem('callUserEdit',1);
            localStorage.removeItem('callWish');
            $('.user-wrapp').removeClass('JSactive-wish-part');
        });
        if(localStorage.getItem('callUserEdit')) {
            $('.user-wrapp').removeClass('JSactive-wish-part');
        }
    	// REMOVE WISH LIST AND USER STORAGE
        if(!($('.user-data').length)) {
            localStorage.removeItem('callWish');
            localStorage.removeItem('callUserEdit');
        }
    }());


    // DESCRIPTION ARTICLE
    (function(){
	    var desc_sec = $('.JSdesc-sec-wrap'),
			desc_sec2 = $('.JSdesc-short'),
			result = desc_sec.text().substring(0, 210) + '...';

		desc_sec2.text(result);

	    $('.JSread-more').on('click', function(){
			$(this).text(function(i, text){
			   	return text == 'Vidi manje' ? 'Vidi više' : 'Vidi manje'
			}); 
			$('.JSdesc-short, .JSdesc-section').toggle();
		}); 
	}());

	// ARTICLE DETAILS EXTENDED PRODUCT INFO PLUCKED FROM SHORT DESC
	if($('body').is('#artical-page') && $('.short_description').length) {
		$('.short_description p').each(function(i, item) {
			var string = $(this).text(),
				leftString = string.split(':', 1),
				rightString = string.split(':').pop(),
				leftString = leftString.concat(':');

			if(i == 0) {
				$('.JSinfoA1').append(leftString);
				$('.JSinfoA2').append(rightString);
			} 

			if(i == 1) {
				$('.JSinfoB1').append(leftString);
				$('.JSinfoB2').append(rightString);
			} 

			if(i == 2) {
				$('.JSinfoC1').append(leftString);
				$('.JSinfoC2').append(rightString);
			} 
		});
	}

	// function scrollpage() {
	// 	var articleCount = 0;
	// 	var limitBreak = 0;
	// 	var limit = parseInt($('#article_limit').val());
	// 	$('.JSproduct-list').each(function() {
	// 		var articles = $(this).find('.JSproduct');
	// 		for(articleCount; articleCount < articles.length; articleCount+=limit) {
	// 			limitBreak++;
	// 			articles.slice(articleCount, articleCount+limit).wrapAll("<div id='"+limitBreak+"' class='clearfix JSarticle-limit JSarticle-break-"+limitBreak+"'></div>");
	// 		}
	// 	});

	// 	$(window).click(function() {
	// 		// $('.JSarticle-limit').each(function() {
	// 		// 	var limiterId = $(this).attr('id');
	// 		// 	var limiterIdCounter = parseInt(limiterId)+1;
	// 		// 	if (($(window).height()+$(window).scrollTop())>($(".JSlimit-loader").offset().top + 150)) {
	// 		// 		$('.JSarticle-break-'+limiterIdCounter).css('height', 'unset');
	// 		// 	}
	// 		// });

	// 		// if($('.JSarticle-break-'+limitBreak).css('height') != '0px') {
	// 		// 	$('.JSlimit-loader').hide();
	// 		// }


	// 		$.post(base_url + 'infinite-articles', {limitBreak:limitBreak}, function (response){
	// 			if(response != ''){
	// 				var results = JSON.parse(response);
					
	// 				// $('.JSproduct-list').append("<div id='"+(limitBreak+1)+"' class='clearfix JSarticle-limit JSarticle-break-"+(limitBreak+1)+"'></div>");
	// 				// for(var i = 0; i < results.length; i++) {
	// 				// 	$(".JSarticle-break-"+(limitBreak+1)).append("<div class='JSproduct col-md-3 col-sm-4 col-xs-6 no-padding'>"+results[i].roba_id+"</div");
	// 				// }
	// 			}
	// 		});
	// 	})
	// } scrollpage();

	if($(window).width() < 768) {

		$('.JScall-step-2').on('click', function() {
			$('.JSmob-cart-sec-step, .JSmob-cart-first-step').toggle();
			$('.cart-action-buttons').toggleClass('JScart-btn-actions-active');
		});

		$('.JSpromo-call').on('click', function() {
			$('.coupon-holder').parent().slideToggle();
		});
		
	}
	// Check Term Cart
	$('.JScall-alert-cart').on('click', function() {
		alertError(trans('Prihvatite uslove korišćenja') + '.');
	});

	$('.JScheck-term').on('change', function() {
		if(!$('.JScheck-term').find('input').prop('checked')) {
			sessionStorage.removeItem('checkedTerm')
			$('.JScall-alert-cart').show();
		} else {
			sessionStorage.setItem('checkedTerm', '1');
			$('.JScall-alert-cart').hide();
		}
	});

	if(sessionStorage.getItem('checkedTerm')) {
		$('.JScheck-term').find('input').prop('checked', 'checked');
		$('.JScall-alert-cart').hide();
	}

	if(!('.JScheck-term').length) {
		sessionStorage.removeItem('checkedTerm');
	}

	$('#JSvoucherSend').click(function(){
		var vaucer_broj = $('#vaucer_code').val(),
			cart_ukupno = $('#cartBasePrice').val();

		bootbox.confirm({
            message: "<p>"+trans('Da li ste sigurni da želite da primenite vaučer')+"?</p>",
            buttons: {
                cancel: {
                    label: trans('Ne')
                },
                confirm: {
                    label: trans('Da')
                }
            },
            callback: function (result) {
                if(result){
					$.post(base_url+'vaucer-primeni',{vaucer_broj: vaucer_broj, cart_ukupno: cart_ukupno},function(response){ 
						var results = $.parseJSON(response); 

						console.log(results.vaucer_broj);
						if(results.success){ 
							alertSuccess(trans('Vaučer uspešno primenjen'), 2200); 
							setTimeout(function(){
								location.reload(); 
							}, 600);  
						} else {
							alertError(trans('Vaučer neispravan ili već iskorišćen'), 2200); 
						}
					});
                }
            }
        });
	});

	$('.JSremoveCoupon').click(function(){
		var vaucer_broj = $(this).attr('coupon'),
			cart_ukupno = $('#cartBasePrice').val();

		bootbox.confirm({
            message: "<p>"+trans('Da li ste sigurni da želite da deaktivirate vaučer')+"?</p>",
            buttons: {
                cancel: {
                    label: trans('Ne')
                },
                confirm: {
                    label: trans('Da')
                }
            },
            callback: function (result) {
                if(result){
					$.post(base_url+'vaucer-obrisi',{vaucer_broj: vaucer_broj, cart_ukupno: cart_ukupno},function(response){ 
						var results = $.parseJSON(response); 

						console.log(results.vaucer_broj);
						if(results.success){ 
							alertSuccess( trans('Vaučer uspešno deaktiviran'), 2200); 
							setTimeout(function(){
								location.reload(); 
							}, 600);  
						} else {
							alertError(trans('Vaučer neispravan ili već iskorišćen'), 2200); 
						}
					});
                }
            }
        });
	});

	// FIRST IFRAME HEIGHT 

	if( (!$('.shop-product-card-list').length) && $('body').is('#product-page') )   {
		var horizontal = 4;
		if($(window).width() < 768) {
			horizontal = 2;
		}

		var vertical = 20 / horizontal;
		var product_height = $('.size-div .shop-product-card').first().outerHeight();
		var iframe_height = vertical * product_height;

		$('.first-frame iframe').css('height', iframe_height);

		function scrollpage() {
			var articleCount = 0;
			var grupa_pr_id = $('#infinite_grupa_pr_id').data('grupa_pr_id');
			var strana = $('#infinite_strana').data('strana');
			var proizvodjac = $('#infinite_proizvodjac').data('proizvodjac');
			var karakteristike = $('#infinite_karakteristike').data('karakteristike');
			var cene = $('#infinite_cene').data('cene');
			var count = $('#infinite_count').data('count');
			var max_frames = Math.ceil(count / 20);
			var product_count = 20;

			if(proizvodjac == '') {
				proizvodjac = 0;
			}
			if(karakteristike == '') {
				karakteristike = 0;
			}
			if(cene == '') {
				cene = '0-0';
			}


			$(window).scroll(function() {
				var limitBreak = 1;
				var lastFrame = $('.product-page .product_list_iframe').last();

				$('.product-page .product_list_iframe').each(function() {
					limitBreak++;
				});
		        if(($(window).scrollTop() >= lastFrame.offset().top + lastFrame.outerHeight() - window.innerHeight) && limitBreak <= max_frames) {
					// iFrame height

					if(limitBreak == max_frames) {
						product_count = count - (20 * Math.floor(count / 20));
					}

					var horizontal = 4;
					if($(window).width() < 768) {
						horizontal = 2;
					}

					var vertical = Math.ceil(product_count / horizontal);
					var product_height = $('.size-div .shop-product-card').first().outerHeight();
					var iframe_height = vertical * product_height;

					if(iframe_height < product_height) {
						iframe_height = 535;
					}

	    			$('.product-page').append("<div class='infinite_wrap'><iframe class='product_list_iframe' src='https://delovi.bgelektronik.rs/infinite-articles/"+strana+"/"+grupa_pr_id+"/?page="+limitBreak+"&m="+proizvodjac+"&ch="+karakteristike+"&pr="+cene+"' frameborder='0' style='width: 100%; height: "+iframe_height+"px;'></iframe></div>")
					
					$.post(base_url + 'infinite-articles-set', {limitBreak:limitBreak}, function (response){
						if(response != ''){
							var results = JSON.parse(response);
							$('.infinite_wrap').removeClass('infinite_wrap');
						} else {
							$('.product-page').append("<div class='infinite_wrap'>Kraj</div>")
						}
					});
			    }

			})
		} scrollpage();
	}

	// $('.fixed-filters').on('click', function() {
	// 	$('.sections').toggleClass('hidden');
	// 	$('.section-filters').toggleClass('hidden-xs').toggleClass('hidden-sm');
	// });

	// var filters_grupa_pr_id = $('#infinite_grupa_pr_id').data('grupa_pr_id');
	// console.log(filters_grupa_pr_id);
	// $('.JSfilter-mob li').each(function() {
	// 	if($(this).val() == filters_grupa_pr_id) {
	// 		$(this).closest('.JSlevel-2').show();
	// 		$(this).closest('.JSlevel-3').show();
	// 		$(this).closest('.JSlevel-4').show();
	// 		$(this).parent().next().find('i').addClass('JSrotateArrow');
	// 		$(this).closest('.JSlevel-2').next().find('i').addClass('JSrotateArrow');;

	// 		$(this).find('a').first().css('color', '#fcd400').css('font-weight', '500');
	// 	}
	// });

	$('.JSchangeToActive').on('click', function() {
		if($(this).is('.JSformSubmitBtn')) {
			$('.login-form').submit();
		}
	});

	$('.JSchangeToActive').on('click', function() {
		$('.login-form input').prop('readonly', false);
		$(this).addClass('JSformSubmitBtn');
		$(this).text('Sačuvaj');
	});

	$(".show-password2").click(function(){
    	var pass_login = $('.JSpassword_login_2'),
    		pass_login2 = $('.JSpassword_login22');

    	pass_login2.val(pass_login.val());
	    pass_login2.on('change', function(){
	        pass_login.val(pass_login2.val());
	    });
	    $(this).find('span').toggleClass('fas fa-eye-slash fa fa-eye');
	   	pass_login2.toggle();
	    pass_login.toggle();
	});

	$(".show-password3").click(function(){
    	var pass_login = $('.JSpassword_login_3'),
    		pass_login2 = $('.JSpassword_login23');

    	pass_login2.val(pass_login.val());
	    pass_login2.on('change', function(){
	        pass_login.val(pass_login2.val());
	    });
	    $(this).find('span').toggleClass('fas fa-eye-slash fa fa-eye');
	   	pass_login2.toggle();
	    pass_login.toggle();
	});

	if($('.company-user').is('.active-user')) {
		$("input[name='flag_vrsta_kupca']").val(1);
	}

	//Proverava da li je validna e-mail adresa
	function isValidEmailAddress(emailAddress) {
		var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
		return pattern.test(emailAddress);
	};

	$('.JSavailable_soon').click(function(){
 		var obj = $(this),
			roba_id = obj.data('roba-id'),
			roba_id_input = $('#JSRobaId').val(roba_id);
 			
 		$('#available_soon').modal('show');
 	});
	
	$('.JSInformMe').click(function(){

		var name = $('#name').val();
		var mail = $('#mail').val();
		var telefon = $('#telefon').val();
		var roba_id = $('#JSRobaId').val();
		
		if(!isNaN(name)){
			$('.JSinfo-popup').fadeIn("fast").delay(2000).fadeOut("fast");
			$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>Niste uneli ime.</p>");
		}else if(isNaN(telefon) || (document.getElementById("telefon").value.length == 0) ){
			$('.JSinfo-popup').fadeIn("fast").delay(2000).fadeOut("fast");
			$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>Niste uneli broj telefona.</p>");
		}else if(!isValidEmailAddress(mail)){
			$('.JSinfo-popup').fadeIn("fast").delay(2000).fadeOut("fast");
			$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>Unesite ispravanu email adresu.</p>");
		}else{
			$.post(base_url+'lager-inform-customer',{roba_id: roba_id, mail: mail, name: name, telefon: telefon},function(response){
				if(response=='OK'){
					$('.JSinfo-popup').fadeIn("fast").delay(2500).fadeOut("fast");
					$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>Hvala! Poruka sa informacijom o dostupnosti proizvoda stići će na Vašu e-mail adresu.</p>");
					$('#available_soon').modal('hide');
				}else{
					$('.JSinfo-popup').fadeIn("fast").delay(2200).fadeOut("fast");
					$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>Već ste poslali zahtev za obaveštenje o ponovnoj dostupnosti ovog proizvoda.</p>");
				}
			});
		}

		
	});
}); // DOCUMENT READY END
 

// ALERTIFY
function alertSuccess(message) {
	swal(message, "", "success");
		
	setTimeout(function() {
		swal.close();  
	}, 2000);
}	
 
function alertError(message) {
	sweetAlert(message, "", "error");
	setTimeout(function() {
		sweetAlert.close();  
	}, 2000);
}
 

// ZOOMER 
document.onreadystatechange = function(){
    if (document.readyState === "complete") { 

		$('.product-image').each(function(i, img){
			if ($(img).attr('src') != 'quick_view_loader.gif') {
				$(img).attr('src', '/images/no-image.jpg').addClass('img-responsive'); 
			}
		});   

		// IMAGE REPLACEMENT
		if($(window).width() > 1024) {
			$('.product-image-wrapper').on('mouseenter', function(){  

				var img = $(this).find('img'),
					placeholder = $(this).find('.JSimg-placeholder');
	 			
	 			if(img.attr('src').includes('no-image')) {
		 
					img.siblings('.JSimg-placeholder').remove();

			  	} else if (img.attr('src') != placeholder.data('src') && placeholder.length) {
 
					sessionStorage.setItem('src', img.attr('src')); 

					img.attr('src', placeholder.data('src')).hide().show();  

			  	} 

			}).on('mouseleave', function(){
				var img = $(this).find('img');
	 
	 			if (sessionStorage.getItem('src')) {
					
					img.attr('src', sessionStorage.getItem('src')).hide().show();

					sessionStorage.removeItem('src');
				}    
			});
			// IF IMAGE CLICKED
			sessionStorage.removeItem('src');
		}
	} 

	// COLOR CIRCLES LIMITER
	$('.color-scroll').each(function() {
		if ($(window).width() > 991 && $(window).width() < 1365 ) {
			if ($(this).find('.color_type').length > 4) { 
			    $(this).find('.more-colors').show();
			    $(this).addClass('JScolor-media-width');
			}
		}
		else {
			if ($(this).find('.color_type').length > 5) { 
			    $(this).find('.more-colors').show();
			}
		}
	});

	$(document).on('click','.JSrelated_color',function(){
 		var roba_id = $(this).find('span').data('roba_id'),
 		    srodni_id = $(this).find('span').data('srodni_id');

 		$('.JSadd-to-cart[data-org_roba_id="'+roba_id+'"]').data('roba_id',srodni_id);

 		// $(this).addClass('active-color').siblings().removeClass('active-color');
 		// $('.JSrelated_img[data-roba_id="' + roba_id + '"]').addClass('hidden');
 		// $('.JSrelated_img[data-srodni_id="' + srodni_id + '"]').removeClass('hidden');

 		$(this).closest('.product-meta').siblings('.product-image-wrapper').find($('.JSrelated_img[data-roba_id="' + roba_id + '"]')).addClass('hidden');
	 	$(this).closest('.product-meta').siblings('.product-image-wrapper').find($('.JSrelated_img[data-srodni_id="' + srodni_id + '"]')).removeClass('hidden');
 		
 	});

}

 
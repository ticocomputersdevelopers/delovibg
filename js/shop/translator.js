

function trans(string){
	var string_trans = string;
	var translates = [];
	if(localStorage.getItem('translates').length > 0){
		translates = JSON.parse(localStorage.getItem('translates'));
	}

	var trans = translates.filter(function(item){
		return item.izabrani_jezik_reci.trim().toLowerCase() == string.trim().toLowerCase();
	});
	if(trans[0]){
		string_trans = trans[0].reci;
	}else{
		string_trans = '';
		var string_array = string.split(' ');
		for (var i = 0;i < string_array.length; i++) {
			var trans_str = translates.filter(function(item){
				return item.izabrani_jezik_reci.trim().toLowerCase() == string_array[i].trim().toLowerCase();
			});
			if(trans_str[0]){
				string_trans += trans_str[0].reci+' ';
			}else{
				string_trans += string_array[i]+' ';
			}	
		}
	}
	return string_trans.trim();
}

$(document).ready(function () {

	
	//upload i import
	  $('#webImportUploadSelect').change(function(){
	  	var valueDobavljac = $('#webImportUploadSelect').val();
	  	var valueKurs = $('#kurs').val();
	  	
	  	if (valueKurs == '' || valueKurs < 1)
	  		$('#webImportUploadForm').attr('action', '/admin/upload_web_import/'+valueDobavljac);
	  	else
	  		$('#webImportUploadForm').attr('action', '/admin/upload_web_import/'+valueDobavljac+'/'+valueKurs);
	  	$('#webImportUploadForm').removeAttr('onclick');
	  	$('#webImportUploadResponse').html('');
		
	  });

	  $('#JSaddKurs').click(function(){
	  	var valueDobavljac = $('#webImportUploadSelect').val();
	  	var valueKurs = $('.JSkursVal').val();

	  	//alert(valueDobavljac);
	  });


	  // $('#kursButton').click(function(){
	  // 	var valueDobavljac = $('#webImportUploadSelect').val();
	  // 	var valueKurs = $('#kurs').val();
	  	
	  // 	if (valueKurs == '' || valueKurs < 1)
	  // 		$('#webImportUploadForm').attr('action', '/admin/upload_web_import/'+valueDobavljac);
	  // 	else
	  // 		$('#webImportUploadForm').attr('action', '/admin/upload_web_import/'+valueDobavljac+'/'+valueKurs);
	  // 	$('#webImportUploadForm').removeAttr('onclick');
	  // 	$('#webImportUploadResponse').html('');
  		  
	  // });

	  $('#webImportUploadButton').click(function(){
	  	if(('#webImportTabelaForm').attr('action') != '#')
	  		$('#webImportUploadResponse').html('<i class="fa fa-cog fa-spin fa-2x fa-fw margin-bottom"></i><span class="sr-only">translate(Importujem...)</span>');
	  });

	// UCITAVANJE FAJLA ZA IMPORT
	if($('#has_import').val() == 'import'){
		$('#message_import').html(translate('Fajl se importuje! Sacekajte nekoliko minuta!'));
		var data = {
			partner_id : $("input[name='partner_id']").val(),
			kurs : $("input[name='kurs']").val(),
			upload_file : $('#upload_file').val(),
			upload_file_extension : $('#upload_file_extension').val(),
			roba_insert : $('#roba_insert').val()
		}

		$('#wait').show();
		$.post(base_url+'admin/web_import_ajax', data, function (response){
			
			var response = $.parseJSON(response);

			if(response.success == 1){
				// if(response.roba_ids && response.roba_ids.length > 0){
				// 	$.post(base_url+'admin/ajax/dc_articles', {action: 'take_images', roba_ids: response.roba_ids}, function (response){
				// 		$('#wait').hide();
				// 		alertify.alert('Fajl je uspešno importovan!');
				// 		window.location.href = base_url+'admin/web_import/'+criteriaImport[0]+'/'+criteriaImport[1]+'/'+data.partner_id+'/'+criteriaImport[3]+'/'+criteriaImport[4]+'/'+criteriaImport[5];
				// 	});
				// }else{
					$('#wait').hide();
					alertify.alert('Fajl je uspešno importovan!');
					window.location.href = base_url+'admin/web_import/'+criteriaImport[0]+'/'+criteriaImport[1]+'/'+data.partner_id+'/'+criteriaImport[3]+'/'+criteriaImport[4]+'/'+criteriaImport[5];					
				// }


			}else{
				$('#wait').hide();
				$('#message_import').html(response.message);
			}
		});
	}


	  //selected rows
     var dc_ids = new Array();
     var dc_select_ids;
     var table_roba_id;
	  $(function() {
	    $( "#selectable" ).selectable({
    	  filter: 'tr',
	      stop: function() {
	      	dc_ids = [];
	      	$("td").removeClass("ui-selected");
	      	$("td").find('input').removeClass("ui-selected");
	        
	      	var selectedItems = $('.ui-selected', this);
       		$('#JSselektovano').html(selectedItems.length);

	        $( ".ui-selected", this ).each(function() {
	          var id = $( this ).data("id");
	          dc_ids.push(id);
	          
	        });
	        dc_ids=dc_ids.filter(function(n){return n !== undefined}); 
	        dc_select_ids = dc_ids;

	        }
	    });
	  });

	  //all select
	  $( "#all" ).click(function(){
	  		$("#selectable").find("tr").addClass("ui-selected");
	  		 $( ".ui-selected", "#selectable" ).each(function() {
	          var id = $( this ).data("id");

	          var selectedItems = $('.ui-selected', this);
	          $('#JSselektovano').html('Sve');

	          dc_ids.push(id);
	        });
	  		dc_select_ids=dc_ids.filter(function(n){return n !== undefined});
	  	//	dc_ids = dc_select_ids;
	  		dc_ids = all_ids;
	  });

	$(document).on('click','.JSRobaRow',function(){
		table_roba_id = $(this).data('id');

		$(this).siblings().removeClass('row-selected');
		$(this).toggleClass('row-selected');
	});

	//order
	var ord = location.pathname.split('/')[9];
	if(ord != null){
		var order = ord.split('-');
		if(order[1] == 'ASC'){
			$('.JSSort').filter('[data-coll="'+ order[0] +'"]').attr('data-sort', 'ASC');
			$('.JSSort').filter('[data-coll="'+ order[0] +'"]').find('span').html('&#x25B2');
		}else{
			$('.JSSort').filter('[data-coll="'+ order[0] +'"]').attr('data-sort', 'DESC');
			$('.JSSort').filter('[data-coll="'+ order[0] +'"]').find('span').html('&#x25BC');
		}
		
	}
	
	$('.JSSort').click(function(){
		
		if($(this).data('sort') == 'DESC'){
			var coll = $(this).data('coll');
			var sort = 'ASC';
		}else{
			var coll = $(this).data('coll');
			var sort = 'DESC';
		}

		location.href = base_url+'admin/web_import/'+criteriaImport[0]+'/'+criteriaImport[1]+'/'+criteriaImport[2]+'/'+criteriaImport[3]+'/'+criteriaImport[4]+'/'+criteriaImport[5]+'/'+coll+'-'+sort;
		
	});

	//search
	if ($('#search').length>0) {
		if(criteriaImport[4] != 0){
			$('#search').val(criteriaImport[4].replace(/\+/g, ' ')); 
		}
	}
	$('#search-btn').click(function(){

		var search = $('#search').val();
		var search1 = search.replace(/\//g, '+').replace(/ /g, '+').replace(/\"/g, '').replace(/\'/g, '');
		if(search != ''){
			location.href = base_url+'admin/web_import/'+criteriaImport[0]+'/'+criteriaImport[1]+'/'+criteriaImport[2]+'/'+criteriaImport[3]+'/'+search1+'/'+criteriaImport[5];
		}
	});

	$('#search').on("keyup", function(event) {
		if(event.keyCode == 13 && $('#search').val() != ''){
        	$("#search-btn").trigger("click");
    	}
    });
    
	$('#clear-btn').click(function(){

		location.href = base_url+'admin/web_import/'+criteriaImport[0]+'/'+criteriaImport[1]+'/'+criteriaImport[2]+'/'+criteriaImport[3]+'/0/'+criteriaImport[5];

	});

	//search cena
	if(criteriaImport[5] != null && criteriaImport[5] != 'nn-nn') {
		var nabavna_arr = criteriaImport[5].split('-');
		if($.isNumeric(nabavna_arr[0])){
			$('#JSCenaOd').val(nabavna_arr[0]); 
		}
		if($.isNumeric(nabavna_arr[1])){
			$('#JSCenaDo').val(nabavna_arr[1]); 
		}
	}

	$('#JSCenaSearch').click(function(){
		var nabavna_od = $('#JSCenaOd').val();
		var nabavna_do = $('#JSCenaDo').val();
		if($.isNumeric(nabavna_od) && $.isNumeric(nabavna_do)){
			location.href = base_url+'admin/web_import/'+criteriaImport[0]+'/'+criteriaImport[1]+'/'+criteriaImport[2]+'/'+criteriaImport[3]+'/'+criteriaImport[4]+'/'+nabavna_od+'-'+nabavna_do;
		}
	});
	$('#JSCenaClear').click(function(){

		location.href = base_url+'admin/web_import/'+criteriaImport[0]+'/'+criteriaImport[1]+'/'+criteriaImport[2]+'/'+criteriaImport[3]+'/'+criteriaImport[4]+'/nn-nn';

	});

	// Select2

	$("#webImportGrupa").select2({
		placeholder: translate('Izaberite grupu'),
		language: {
            noResults: function () {
            return translate("Nema rezultata");
            }
        }
	});

	$("#webImportProizvodjac").select2({
		placeholder: translate('Izaberite proizvođača'),
		language: {
            noResults: function () {
            return translate("Nema rezultata");
            }
        }
	});

	$("#grupa_select").select2({
		placeholder: translate('Izaberite grupu'),
		language: {
            noResults: function () {
            return translate("Nema rezultata");
            }
        }
	});

	//proizvojac i dobavljac filteri
	  $("#proizvodjac_select").select2({
	  	placeholder: translate('Izaberi proizvodjace'),
	  	language: {
            noResults: function () {
            return translate("Nema rezultata");
            }
        }
	  	});

	   $( ".JSGrProDob" ).click(function(){
	  		var proizvodjaci = '';
	  		$("#proizvodjac_select :selected").each(function(){
	  			proizvodjaci += '+' + $(this).val();
	  		});
	  		if(proizvodjaci == ''){
	  			proizvodjaci = '+0';
	  		}

	  		var grupa = '';
	  		grupa = $('#grupa_select').val();
	  		if(grupa == 'null'){
	  			grupa = '0';
	  		}
	  		
	  		location.href = base_url+'admin/web_import/'+grupa+'/'+proizvodjaci.substr(1)+'/'+criteriaImport[2]+'/'+criteriaImport[3]+'/'+criteriaImport[4]+'/'+criteriaImport[5];
	  });
	   
	  	$("#dobavljac_select").change(function(){
	  		var partner_id = $(this).val();
	  		location.href = base_url+'admin/web_import/'+criteriaImport[0]+'/'+criteriaImport[1]+'/'+partner_id+'/'+criteriaImport[3]+'/'+criteriaImport[4]+'/'+criteriaImport[5];
	  	});


	  	//Definisanje marzi
	  	

	  //FILTERI
		if(criteriaImport != null){
			var filters = criteriaImport[3].split("-");
			var i=1;
			$.each(filters, function( index, value ) {
				if(value == "dd"){
					$( ".row" ).find('[data-check="'+i+'"]').attr('checked', '');
					$( ".row" ).find('[data-check="'+(i+1)+'"]').attr('checked', '');
				}
				if(value == "1"){
					$( ".row" ).find('[data-check="'+i+'"]').attr('checked', '');
				}
				if(value == "0"){
					$( ".row" ).find('[data-check="'+(i+1)+'"]').attr('checked', '');
				}
				i=i+2;
			});	 
		}

	   $( ".filter-check" ).click(function(){
	   		var element = $(this);
	     		if(element.attr('checked')){
	 				$.each(filters, function( index, value ) 
	 				{
			     		if(element.data("check") == 2*index+1 && !element.parent().parent().find('[data-check="'+(2*index+2)+'"]').attr('checked')){
			     			filters[index] = "nn";
			     		}
			     		if(element.data("check") == 2*index+1 && element.parent().parent().find('[data-check="'+(2*index+2)+'"]').attr('checked')){
			     			filters[index] = "0";
			     		}
			     		if(element.data("check") == 2*index+2 && element.parent().parent().find('[data-check="'+(2*index+1)+'"]').attr('checked')){
			     			filters[index] = "1";
			     		}
			     		if(element.data("check") == 2*index+2 && !element.parent().parent().find('[data-check="'+(2*index+1)+'"]').attr('checked')){
			     			filters[index] = "nn";
			     		}
					});
		     	}else{
	 				$.each(filters, function( index, value ) {
			     		if(element.data("check") == 2*index+1 && !element.parent().parent().find('[data-check="'+(2*index+2)+'"]').attr('checked')){
			     			filters[index] = "1";
			     		}
			     		if(element.data("check") == 2*index+1 && element.parent().parent().find('[data-check="'+(2*index+2)+'"]').attr('checked')){
			     			filters[index] = "1"; //dd
			     		}
			     		if(element.data("check") == 2*index+2 && !element.parent().parent().find('[data-check="'+(2*index+1)+'"]').attr('checked')){
			     			filters[index] = "0";
			     		}
			     		if(element.data("check") == 2*index+2 && element.parent().parent().find('[data-check="'+(2*index+1)+'"]').attr('checked')){
			     			filters[index] = "0"; //dd
			     		}			
					});
		     	}
		  		var filteri = '';
				$.each(filters, function( index, value ) {
					filteri += '-' + value;
				});
				location.href = base_url+'admin/web_import/'+criteriaImport[0]+'/'+criteriaImport[1]+'/'+criteriaImport[2]+'/'+filteri.substr(1)+'/'+criteriaImport[4]+'/'+criteriaImport[5];	  
		});
	
		//vise kod liste robe
		$( "#JSMore" ).click(function(){
			var page = $(this).data('page');
			var data = {
				action: 'more', 
				page: page, 
				grupa_pr_id : criteriaImport[0], 
				proizvodjac : criteriaImport[1], 
				dobavljac : criteriaImport[2], 
				search : criteriaImport[4]
			};
			$.post(base_url+'admin/ajax/dc_articles', data, function (response){
				if(response != ''){
					$('.JSRobaRow').last().after(response);
					$("#JSMore").data('page',parseInt(page)+1);
				}else{
					$("#JSMore").hide();
				}
			});
		});

	// KLIK NA DUGMETA ZA RAZLICITE AKCIJE NAD ARTIKLIMA
	  $( ".JSexecute" ).click(function(){
	  	var dataUpd = $(this).data('execute');
	  	var buttonName = $(this).text();
	  	if(dc_ids[0]){
	  		$('#wait').show();
	  		$.post(base_url+'admin/ajax/dc_articles', {action:'execute', data: dataUpd, dc_ids: dc_ids}, function (response){
  				alertify.success("Akcija '"+buttonName+"' je izvršena!");
				var roba_ids = $.parseJSON(response);

				//CUSTOM, ISPISIVANJE PODATAKA U TABELI
			  	if(dataUpd[0].val == 1 || dataUpd[0].val == 'true'){
			  		// za tabelu dobavljac
				  	$.each(dc_select_ids, function( index, value ) {
				  		var obj_row = $("#selectable").find('[data-id="'+ value +'"]');

				  		if(obj_row.find(".check-roba_id").html() != -1){
							if(dataUpd[0].column == 'flag_zakljucan'){
							  	obj_row.find('.'+dataUpd[0].column).text('DA');
							  	obj_row.find('.JSEditRow').data('zakljucan','1');
							}								
							if(obj_row.find(".JSEditRow").data('zakljucan')==0){								
								if(dataUpd[0].column == 'flag_aktivan'){
								  	if(filters[5] == 0){
								  		obj_row.remove();
								  	}else{
								  		obj_row.removeClass('text-red').addClass('text-green');
										obj_row.find('.'+dataUpd[0].column).html('<i class="fa fa-check green" aria-hidden="true"></i>');
								  	}
								}
								else if(dataUpd[0].column == 'flag_prikazi_u_cenovniku'){
								  	if(filters[7] == 0){
								  		obj_row.remove();
								  	}else{
										obj_row.find('.'+dataUpd[0].column).html('<i class="fa fa-check green" aria-hidden="true"></i>');
								  	}
								}
								else{
									obj_row.find('.'+dataUpd[0].column).text('DA');
								}
							}
						}else{
							if(dataUpd[0].column == 'flag_aktivan' && obj_row.find(".JSEditRow").data('zakljucan')==0){
							  	if(filters[5] == 0){
							  		obj_row.remove();
							  	}else{
							  		obj_row.removeClass('text-red');
									obj_row.find('.'+dataUpd[0].column).html('<i class="fa fa-check green" aria-hidden="true"></i>');
							  	}
							}
						}
					});

				  	// za tabelu roba
				  	$.each(roba_ids, function( index, value ) {
				  		$("#JSRobaList").find('[data-id="'+ value +'"]').find('.'+dataUpd[0].column).text('DA');
					});					
				}
				else{

					// za tabelu dobavljac
				  	$.each(dc_select_ids, function( index, value ) {
				  		var obj_row = $("#selectable").find('[data-id="'+ value +'"]');
				  		if(obj_row.find(".check-roba_id").html() != -1){
							if(dataUpd[0].column == 'flag_zakljucan'){
							  	obj_row.find('.'+dataUpd[0].column).text('NE');
							  	obj_row.find('.JSEditRow').data('zakljucan','0');
							}								
							if(obj_row.find(".JSEditRow").data('zakljucan')==0){
								if(dataUpd[0].column == 'flag_aktivan'){
								  	if(filters[5] == 1){
								  		obj_row.remove();
								  	}else{
								  		obj_row.removeClass('text-green').addClass('text-red');
										obj_row.find('.'+dataUpd[0].column).html('<i class="fa fa-times red" aria-hidden="true"></i>');
								  	}
								}
								else if(dataUpd[0].column == 'flag_prikazi_u_cenovniku'){
								  	if(filters[7] == 1){
								  		obj_row.remove();
								  	}else{
										obj_row.find('.'+dataUpd[0].column).html('<i class="fa fa-times red" aria-hidden="true"></i>');
								  	}
								}
								else{
									obj_row.find('.'+dataUpd[0].column).text('NE');
								}
							}
							
						}else{
							if(dataUpd[0].column == 'flag_aktivan' && obj_row.find(".JSEditRow").data('zakljucan')==0){
							  	if(filters[5] == 1){
							  		obj_row.remove();
							  	}else{
									obj_row.removeClass('text-green').addClass('text-red');
									obj_row.find('.'+dataUpd[0].column).html('<i class="fa fa-times red" aria-hidden="true"></i>');
							  	}
							}							
						}

					});

					// za tabelu roba
				  	$.each(roba_ids, function( index, value ) {
				  		$("#JSRobaList").find('[data-id="'+ value +'"]').find('.'+dataUpd[0].column).text('NE');
					});
				}
				$('#wait').hide();
	  		});
	  	}else{
	  		alertify.alert(translate(translate('Nijedan od artikala nije selektovan!')));
	  	}

	  });

	//KLIK NA DUGME SA INPUTOM
	  $( ".JSexecuteBtn" ).click(function(){
	  	var dataUpd = $(this).data('execute');
	  	var val = $(this).parent().find('.JSexecuteInput').val();
		if(val != '' && val != null){
		  	$.each(dataUpd, function( index, value ) {
				dataUpd[index].val = val;
			});

		  	if(dc_ids[0]){
		  		$('#wait').show();
		  		$.post(base_url+'admin/ajax/dc_articles', {action:'execute', data: dataUpd, dc_ids: dc_ids}, function (response){  	
		  			//custom
					if(dataUpd[0].column == 'grupa_pr_id' || dataUpd[0].column == 'proizvodjac_id' || dataUpd[0].column == 'tarifna_grupa_id' || dataUpd[0].column == 'kolicina'){
							if(dataUpd[0].column == 'proizvodjac_id'){
								alertify.success(translate('Proizvodjač je dodeljen.'));
							}
							else if(dataUpd[0].column == 'grupa_pr_id'){
								alertify.success(translate('Grupa je dodeljena.'));
							}
							else if(dataUpd[0].column == 'tarifna_grupa_id'){
								alertify.success(translate('Porez je dodeljen.'));
							}
							else if(dataUpd[0].column == 'kolicina'){
								alertify.success(translate('Količina je dodeljena. Ukoliko je neki artikal zaključan, neće doći do promene kod istog.'));
								$.each(dc_select_ids, function( index, value ) {
							  		var obj_row = $("#selectable").find('[data-id="'+ value +'"]');
							  		var check = obj_row.find('.flag_zakljucan').html();
							  		if(check == 'NE') {
							  			var test = obj_row.find('.'+dataUpd[0].column).find('.JSkolicinaVrednost').html(val);
							  			
							  		}
								});
							}
							
							if(dataUpd[0].column != 'kolicina'){
						  	$.each(dc_select_ids, function( index, value ) {
						  		var obj_row = $("#selectable").find('[data-id="'+ value +'"]');
								if(dataUpd[0].column != 'grupa_pr_id' && dataUpd[0].column != 'proizvodjac_id'){
									if(obj_row.find(".check-roba_id").html() == -1){
										obj_row.find('.'+dataUpd[0].column).text(response);
										if(dataUpd[0].column == 'tarifna_grupa_id'){
											var cena_nc = parseInt(obj_row.find('.cena_nc').text());
											var cena_pdv = Math.round(cena_nc*(1+parseInt(response)/100)).toString()+'.00';
											obj_row.find('.cena_nc_pdv').text(cena_pdv);
										}
									}
								}else if (dataUpd[0].column == 'grupa_pr_id' || dataUpd[0].column == 'proizvodjac_id') {
										obj_row.find('.'+dataUpd[0].column).text(response);	
								}
							});
						  }
					}
					else if(dataUpd[0].column == 'a_marza' || dataUpd[0].column == 'mp_marza' || dataUpd[0].column == 'web_marza'){

						if(dataUpd[0].column == 'a_marza'){
							alertify.success(translate('Akcijska marža je dodeljena.'));
						}
						else if(dataUpd[0].column == 'mp_marza'){
							alertify.success(translate('Maloprodajna marža je dodeljena.'));
						}
						else if(dataUpd[0].column == 'web_marza'){
							alertify.success(translate('Web marža je dodeljena.'));
						}

			  			var response2 = $.parseJSON(response);
			  			$.each(dc_select_ids, function( index, value ) {
			  				var obj_row = $("#selectable").find('[data-id="'+ value +'"]');
			  				if(obj_row.find(".JSEditRow").data('zakljucan')==0){		  					
				  				obj_row.find('.'+dataUpd[0].column).text(val);
								obj_row.find('.cena_'+dataUpd[0].column).text(response2[value]);
			  				}
						});
					}

					$('#wc-col').width($('.cena_web_marza').width());
					$('#wm-col').width($('.web_marza').width());
					$('#ncp-col').width($('.cena_nc_pdv').width());
					$('#mc-col').width($('.cena_mp_marza').width());
					$('#mpm-col').width($('.mp_marza').width());
					$('#pdv-col').width($('.tarifna_grupa_id').width());
					$('#ngr-col').width($('.grupa_pr_id').width());
					$('#npr-col').width($('.proizvodjac_id').width());
					$('#wait').hide();			
		  		});
		  	}else{
		  		alertify.alert(translate(translate('Nijedan od artikala nije selektovan!')));
		  	}
		}else{
			alertify.alert(translate('Niste popunili input polje!'));
		}
	  });

	  //unos novih
	  $( ".JSunesi_nove" ).click(function(){
	  	var flag;
	  	var fast_insert = 0;

	    // alertify.confirm(translate("Da li želite da stavite artikle i na webu?"),
     //        function(e){
     //            if(e){
     //            	flag = 1;
     //            } else {
     //            	flag = 0;
     //            }
     //    });
		var check = confirm(translate("Da li želite da stavite artikle i na webu?"));
		if(check){
			flag = 1;
		}else{
			flag = 0;
		}
		if(fast_insert_option==1){
			var check_insert = confirm(translate("Brzo umatičenje?"));
			if(check_insert){
				fast_insert = 1;
			}
		}
		
	  	if(dc_ids[0]){
	  		$('#wait').show();
	  		$.post(base_url+'admin/ajax/dc_articles', {action: 'unesi_nove', flag:flag, fast_insert:fast_insert , dc_ids: dc_ids}, function (response){
	  			var response2 = $.parseJSON(response).data;

	  			if($.parseJSON(response).roba_ids.length == 0){
	  				alertify.alert(translate('Niste podesili neko od navedenih polja: grupu, proizvodjača, porez, web maržu, ili je artikal već unet ili je neaktivan!'));
	  			}else{
	  				alertify.success(translate('Uspešno ste uneli artikle!'));
	  				//image download
	  				$.post(base_url+'admin/ajax/dc_articles', {action: 'take_images', roba_ids: $.parseJSON(response).roba_ids}, function (response){ });

		  			$.each(response2, function( index, value ) {
		  				var obj_row = $("#selectable").find('[data-id="'+ index +'"]');
		  				obj_row.find(".check-roba_id").text(response2[index].roba_id);
		  				
						if(obj_row.find(".check-roba_id").val() != -1){
						  	if(filters[6] == 0){
						  		obj_row.remove();
						  	}else{					  		
								if(flag===1){							
				  					obj_row.find(".flag_prikazi_u_cenovniku").html('<i class="fa fa-check green" aria-hidden="true"></i>');
								}else{
				  					obj_row.find(".flag_prikazi_u_cenovniku").html('<i class="fa fa-times red" aria-hidden="true"></i>');							
								}
			  					obj_row.removeClass('text-red').addClass('text-green');
			  					if(fast_insert==1){
			  					obj_row.find(".web_marza").text(Math.round(response2[index].web_marza).toString()+'.00');
			  					obj_row.find(".mp_marza").text(Math.round(response2[index].mp_marza).toString()+'.00');
			  					obj_row.find(".tarifna_grupa_id").text(response2[index].porez);
			  					obj_row.find(".grupa_pr_id").text(response2[index].nasa_grupa);
			  					obj_row.find(".proizvodjac_id").text(response2[index].nas_proizvodjac);
			  					obj_row.find('.cena_web_marza').text(response2[index].web_cena);
		  						obj_row.find('.cena_mp_marza').text(response2[index].mpcena);
		  						obj_row.find('.cena_nc_pdv').text(Math.round(response2[index].cena_nc*(1+response2[index].porez/100)).toString()+'.00');
			  					}
						  	}
		  				}

					}); 

		  		}
		  		$('#rid-col').width($('.check-roba_id').width());
		  		if(fast_insert==1){
				$('#wc-col').width($('.cena_web_marza').width());
				$('#wm-col').width($('.web_marza').width());
				$('#ncp-col').width($('.cena_nc_pdv').width());
				$('#mc-col').width($('.cena_mp_marza').width());
				$('#mpm-col').width($('.mp_marza').width());
				$('#pdv-col').width($('.tarifna_grupa_id').width());
				$('#ngr-col').width($('.grupa_pr_id').width());
				$('#npr-col').width($('.proizvodjac_id').width());
				}
		  		$('#wait').hide();
			});
	 	}else{
	 		alertify.alert(translate('Nijedan od artikala nije selektovan!'));
	 	}
	  	
	  	
	  });
	  
	  //prihvati lager
	  $( "#JSLagerRunModal" ).click(function(){
		$('#JSPrihvatiLagerModal').foundation('reveal', 'open');	
	  });
	  $( ".JSPrihvatiLager" ).click(function(){
		$('.close-reveal-modal').trigger('click');
		var orgj_id = $('#magacin_select').val();
  	
  		if(dc_ids[0]){
  			$('#wait').show();
	  		$.post(base_url+'admin/ajax/dc_articles', {action: 'prihvati_lager', dc_ids: dc_ids, orgj_id: orgj_id}, function (response){
	  			alertify.success(translate('Lager je prihvaćen!'));
	  			var data = $.parseJSON(response);
	  			$.each(data, function( index, value ) {
	  				$("#JSRobaList").find('[data-id="'+ value.roba_id +'"]').find('.kolicina').text(parseInt(value.kolicina));
				});
				$('#wait').hide();
	  	 	});
  		}else{
	 		alertify.alert(translate('Nijedan od artikala nije selektovan!'));
	 	}
	  	
	  });

	  //prihvati cene
	  $( "#JSCeneRunModal" ).click(function(){
		$('#JSPrihvatiCeneModal').foundation('reveal', 'open');	
	  });
	  $( ".JSPrihvatiCeneNaziv" ).click(function(){
		$('.close-reveal-modal').trigger('click');
		var check_naziv = $(this).data("naziv");

	  	var is_kolicina = 0;
	  	var is_cena = 0;
	  	var partner_id = $("#JSPrimaryPartner").val();
	  	if($("#kolicina").is(':checked')){
	  		is_kolicina = 1;
	  	}
	  	if($("#cena").is(':checked')){
	  		is_cena = 1;
	  	}
		if(dc_ids[0]){
			$('#wait').show();
			$.post(base_url+'admin/ajax/dc_articles', {action: 'prihvati_cene', dc_ids: dc_ids, check_naziv: check_naziv, is_kolicina: is_kolicina, is_cena: is_cena, partner_id: partner_id}, function (response){
				if(check_naziv==0){ 
					alertify.success(translate('Nove cene su prihvaćene!'));
				}else{
					alertify.success(translate('Nazivi su prihvaćeni!'));
				}	
	  			var data = $.parseJSON(response);
	  			$.each(data, function( index, value ) {
	  				var obj = $("#JSRobaList").find('[data-id="'+ value.roba_id +'"]');
	  				if(check_naziv==0){
	  					obj.find('.dobavljac').text(value.partner);
		  				obj.find('.racunska_cena_nc').text(value.cena_nc);
		  				obj.find('.web_marza').text(value.web_marza);
		  				obj.find('.web_cena').text(value.web_cena);
	  				}else{
	  					obj.find('.roba_naziv').text(value.naziv);
	  				}
				});

				$('#wait').hide(); 			
			});
		}else{
			alertify.alert(translate('Nijedan od artikala nije selektovan!'));
		}
	  });	  
		
		//Edit Texta
	   	$(" #JStextEditNaziv, #JStextEditOpis ").click(function(){
			if($(this).attr('id') == 'JStextEditOpis'){
	   			$("#JStextEditNazivInput").attr("placeholder", translate("Ovo je novi opis"));
	   		}else if($(this).attr('id') == 'JStextEditNaziv'){
	   			$("#JStextEditNazivInput").attr("placeholder", translate("Ovo je novi naziv"));
	   		}
		});

		$("#JSEditTeksta").click(function(){
			if(dc_select_ids){
				$('#JStextEditModal').foundation('reveal', 'open');
			}
		  	else{
		  		alertify.alert(translate('Nema selektovanih artikala!'));
		  	}
		});

	  //  	$( "#JSEditTeksta" ).click(function(){
			// $('#JStextEditModal').foundation('reveal', 'open');	
		 //  });

		
	    $(" #JStextInsertButton, #JStextReplaceButton ").click(function(){	   	
			var dugme=$(this).attr('id');
   			if((dugme == 'JStextReplaceButton' && $("#JStextEditInput1").val() == '') || (dugme == 'JStextInsertButton' && $("#JStextEditNazivInput").val() == '')){
	   			$("#Results").text(translate("Niste popunili polje!"));
	   		}else{
	   			$("#Results").text("");

		   		var data = {
		  			dc_ids: dc_ids,
		  			insert: $("#JStextEditNazivInput").val(),
		  			replaceFrom: $("#JStextEditInput1").val(),
		  			replaceTo: $("#JStextEditInput2").val()
	  			}		

	  			$("#JStextEditNazivInput").val('');
	  			$("#JStextEditInput1").val('');
	  			$("#JStextEditInput2").val('');

				var naziv_check = 1;
		  		if($("#JStextEditOpis").is(':checked')){
		  			naziv_check = 0;
		  		}
		  		var first = $("#JStextEditNaziv");
		  		first.prop("checked", true);

		  		var end_check = 0;
		  		if($("#JStextEditEnd").is(':checked')){
		  			end_check = 1;
		  		}
		  		var second = $("#JStextEditStart");
		  		second.prop("checked", true);

		  		var deoTeksta = 0;
		  		if($("#JStextEditDeo").is(':checked')){
		  			deoTeksta = 1;
		  		}
		  		var third = $("#JStextEditDeo");
		  		third.prop("checked", true);


				if(dugme == 'JStextInsertButton' && naziv_check == 1){
		  			if(data.insert.length > 500) //i got a problem with this one i think
		  			{
		  				alert(translate("Naziv ne sme biti duži od 500 karaktera."));
		  				return false;
		  			}
	  			}

	  			if(dugme == 'JStextInsertButton'){	  					  		
		  			data.action = 'text_edit'; 
		  			data.naziv_check = naziv_check;
		  			data.end_check = end_check;
	  			}else if(dugme == 'JStextReplaceButton'){
		  			data.action = 'text_replace';
			  		data.naziv_check = naziv_check;
			  		data.deoTeksta = deoTeksta;
	  			}

		  		$.post(base_url+'admin/ajax/dc_articles', data, function (response){
		  			alertify.success(translate('Data akcija je izvršena'));
		  			if(naziv_check == 1 && dugme == 'JStextInsertButton'){
		  				var result = $.parseJSON(response);
		  				$.each(result.dc_ids, function( index, value ) {
							var obj = $("#selectable").find('[data-id="'+ index +'"]').find('.naziv');
							obj.html("<span>" + value + "<span>");					 
						});
		  		// 		$.each(dc_select_ids, function( index, value ) {
						// 	var obj = $("#selectable").find('[data-id="'+ value +'"]').find('.naziv');	
						// 	var obj1 = $("#selectable").find('[data-id="'+ value +'"]').find('.naziv').text();	
						//   	obj.html("<span>" + data.insert + ' ' + obj1+ "</span>");
						// });
		  			}
		  		// 	else if(naziv_check == 1 && dugme == 'JStextInsertButton' && end_check == 1){
		  		// 		$.each(dc_select_ids, function( index, value ) {
						// 	var obj = $("#selectable").find('[data-id="'+ value +'"]').find('.naziv');	
						// 	var obj1 = $("#selectable").find('[data-id="'+ value +'"]').find('.naziv').text();	
						//   	obj.html("<span>" + obj1 + ' ' + data.insert+ "</span>");
						// });
		  		// 	}
		  			else if(dugme == 'JStextReplaceButton'){
		  				var result = $.parseJSON(response);
					  	$("#Results").text("Izmenjeno " + result.counter);
					  	if(naziv_check == 1){
							$.each(result.dc_ids, function( index, value ) {
								var obj = $("#selectable").find('[data-id="'+ index +'"]').find('.naziv');
							  	obj.html("<span>" + value + "<span>");					 
							});
						}	
				  	}
		  		});
	  		}		  	
	  	});


	  //pmp cena u web cenu
	 $( "#prepisi_cenu" ).click(function(){
	  	
		if(dc_ids[0]){
			$('#wait').show();
			$.post(base_url+'admin/ajax/dc_articles', {action: 'prepisi_cenu', dc_ids: dc_ids}, function (response){
				alertify.success(translate('Web cena je prepisana! Ukoliko ne postoji PMC cena, vrednost se ne prepisuje'));
				var response2 = $.parseJSON(response);	  	
	  			$.each(dc_select_ids, function( index, value ) {
					var obj_row = $("#selectable").find('[data-id="'+ value +'"]');	  			
	  				if(obj_row.find(".JSEditRow").data('zakljucan')==0){
	  					obj_row.find('.cena_web_marza').text(response2[value].web_cena);
	  					obj_row.find('.web_marza').text(response2[value].web_marza);
	  				}
				}); 
				$('#wc-col').width($('.cena_web_marza').width());
				$('#wm-col').width($('.cena_web_marza').width());

				$('#wait').hide(); 

			}); 
		}else{
			alertify.alert(translate('Nijedan od artikala nije selektovan!'));
		}
	  }); 
	  //preracunaj cene
	  $( "#preracunaj_cene" ).click(function(){
	  	
		if(dc_ids[0]){
			$('#wait').show();
			$.post(base_url+'admin/ajax/dc_articles', {action: 'preracunaj_cene', dc_ids: dc_ids}, function (response){
				alertify.success(translate('Cene su preračunate!'));
				var response2 = $.parseJSON(response);	  			
	  			$.each(dc_select_ids, function( index, value ) {
	  				var row_cene = $("#selectable").find('[data-id="'+ value +'"]');
	  				if(row_cene.find(".JSEditRow").data('zakljucan')==0){
		  				row_cene.find('.cena_web_marza').text(response2[value].web_cena);
		  				row_cene.find('.cena_mp_marza').text(response2[value].mpcena);
	  				}
				});
				$('#wc-col').width($('.cena_web_marza').width());
				$('#mc-col').width($('.cena_mp_marza').width());
				$('#wait').hide(); 
			});
		}else{
			alertify.alert(translate('Nijedan od artikala nije selektovan!'));
		}
	  });

	  //preracunavanje cena za odredjeni kurs

	  $( "#JSaddKurs" ).click(function(){
	  	var kurs = $(".JSkursVal").val();

		if(dc_ids[0]){
			$('#wait').show();
			$.post(base_url+'admin/ajax/dc_articles', {action: 'preracunaj_cene_kurs', dc_ids: dc_ids, kurs: kurs}, function (response){
				alertify.success(translate('Cene su preračunate!'));
				var response2 = $.parseJSON(response);
	  			$.each(dc_select_ids, function( index, value ) {
	  				var row_cene = $("#selectable").find('[data-id="'+ value +'"]');
	  				if(row_cene.find(".JSEditRow").data('zakljucan')==0){
		  				row_cene.find('.cena_web_marza').text(response2[value].web_cena);
		  				row_cene.find('.cena_mp_marza').text(response2[value].mpcena);
	  				}
				});
				$('#wc-col').width($('.cena_web_marza').width());
				$('#mc-col').width($('.cena_mp_marza').width());
				$('#wait').hide(); 
			});
		}else{
			alertify.alert(translate('Nijedan od artikala nije selektovan!'));
		}
	  });

	  //storniranje kolicina na 0
	  $( "#kolicina_null" ).click(function(){
	  	
		if(dc_ids[0]){
			$('#wait').show();
			$.post(base_url+'admin/ajax/dc_articles', {action: 'kolicina_null', dc_ids: dc_ids}, function (response){
				alertify.success(translate('Količine su stornirane na 0!'));
				$.each(dc_select_ids, function( index, value ) {
	  				var obj_row = $("#selectable").find('[data-id="'+ value +'"]');
	  				if(obj_row.find(".JSEditRow").data('zakljucan')==0){
						obj_row.find('.kolicina').text('0');
	  				}
				});
				$('#wait').hide(); 
			});
		}else{
			alertify.alert(translate('Nijedan od artikala nije selektovan!'));
		}
	  });
	  //obrisi artikle
	  $( "#obrisi" ).click(function(){
	  	
		if(dc_ids[0]){
			var check = confirm(translate("Da li ste sigurni da želite obrisati artikal/artikle?"));
				
			if(check){
				$('#wait').show();
				$.post(base_url+'admin/ajax/dc_articles', {action: 'obrisi', dc_ids: dc_ids}, function (response){
					alertify.success('Artikli su obrisani!');
		  			$.each(dc_select_ids, function( index, value ) {
		  				var obj_row = $("#selectable").find('[data-id="'+ value +'"]');
		  				if(obj_row.find(".JSEditRow").data('zakljucan')==0){
							obj_row.remove();
		  				}
					});
					$('#wait').hide(); 
				});
			}
		} else {
			alertify.alert(translate('Nijedan od artikala nije selektovan!'));
		}
	  });

	  //preuzimanje karakteristika i slika
	  $( "#JSKarakSlikeRunModal" ).click(function(){
	  	$('#JSKarakSlikeModal').foundation('reveal', 'open');
	  });
	  $( ".JSKarakSlike" ).click(function(){
	  	var partner_id = $('#JSPrPartnerId').val();
		if(dc_ids[0]){
			$('#wait').show();
			$.post(base_url+'admin/ajax/dc_articles', {action: 'karak_slike', dc_ids: dc_ids, partner_id: partner_id}, function (response){
				alertify.success(translate('Dodatne karakteristike i slike su preuzete!'));
				$('#wait').hide(); 
			});
		}else{
			alertify.alert(translate('Nijedan od artikala nije selektovan!'));
		}
	  });

	  //povezivanje vise artikala od dobavljaca sa jednim u roba
	  $( "#povezi_artikle" ).click(function(){
	  		if(table_roba_id != null){
	  			$('#wait').show(); 			
		  		$.post(base_url+'admin/ajax/dc_articles', {action: 'povezi_artikle', dc_ids: dc_ids, table_roba_id: table_roba_id}, function (response){
		  			if(response == 'break'){
		  				alertify.alert(translate('Nemoguće je povezati artikle,<br> proverite da li su artikali aktivani, zakljucani i da li imaju definisanu nabavnu cenu!'));
					}else{
						var info_data = $.parseJSON(response);

			  			alertify.success(translate('Artikli su povezani!'));
						$.each(dc_select_ids, function( index, value ) {
							var obj_row = $("#selectable").find('[data-id="'+ value +'"]');
							if(obj_row.find(".JSEditRow").data('zakljucan')==0){								
				  				obj_row.addClass('text-green');
				  				obj_row.find(".check-roba_id").text(table_roba_id);
				  				$.each(info_data[value], function (key, value) {
								    obj_row.find("."+key).text(value);
								});
							}
						});
					}

					$('#rid-col').width($('.check-roba_id').width());
					$('#wc-col').width($('.cena_web_marza').width());
					$('#wm-col').width($('.web_marza').width());
					$('#ncp-col').width($('.cena_nc_pdv').width());
					$('#mc-col').width($('.cena_mp_marza').width());
					$('#mpm-col').width($('.mp_marza').width());
					$('#pdv-col').width($('.tarifna_grupa_id').width());
					$('#ngr-col').width($('.grupa_pr_id').width());
					$('#npr-col').width($('.proizvodjac_id').width());
					$('#wait').hide();
		  		});
	  		}else{
	  			alertify.alert(translate('Niste selektovali artikal u tabeli robe!'));
	  		}
	  });

	$( "#razvezi_artikle" ).click(function(){ 			
		$.post(base_url+'admin/ajax/dc_articles', {action: 'razvezi_artikle', dc_ids: dc_ids}, function (response){
			var actioned = $.parseJSON(response);
			$.each(actioned, function( index, value ) {
				var obj_row = $("#selectable").find('[data-id="'+ value +'"]');
				if(obj_row.find(".JSEditRow").data('zakljucan')==0){
	  				obj_row.find(".check-roba_id").text('-1');
					obj_row.find(".flag_prikazi_u_cenovniku").html('<i class="fa fa-times red" aria-hidden="true"></i>');
	  				obj_row.removeClass('text-green');
	  				obj_row.removeClass('text-red');
	  			}
  				alertify.success(translate('Artikli su razvezani!'));
			});
		});

	});

	// Definisanje marzi
	  $( "#JSimpDefMarza" ).click(function(){
	  	
		if(dc_ids[0]){
			$('#wait').show();
			$.post(base_url+'admin/ajax/dc_articles', {action: 'definisane_marze_primeni', dc_ids: dc_ids}, function (response){
				alertify.success(translate('Marže su prihvaćene!'));
				var result = $.parseJSON(response);

	  			$.each(result, function( index, value ) {
	  				var row_cene = $("#selectable").find('[data-id="'+ value.dobavljac_cenovnik_id +'"]');
	  				
	  				row_cene.find('.web_marza').html('<span>'+value.web_marza+'</span>');
	  				row_cene.find('.mp_marza').html('<span>'+value.mp_marza+'</span>');
	  				row_cene.find('.cena_web_marza').html('<span>'+value.web_cena+'</span>');
	  				row_cene.find('.cena_mp_marza').html('<span>'+value.mpcena+'</span>');

				});
				$('#wc-col').width($('.cena_web_marza').width());
				$('#mc-col').width($('.cena_mp_marza').width());
				$('#wait').hide(); 
			});
		}else{
			alertify.alert(translate('Nijedan od artikala nije selektovan!'));
		}
	  });
	


	// CUSTOM RIGHT CLICK MENU
	var active = 0;
	$('.webimport-table').on("contextmenu", function(e) {
		$('.custom-menu').css('top', e.pageY + "px");
		$('.custom-menu').css('left', e.pageX + "px");

		if(active == 0) {
			$('.custom-menu').addClass('custom-menu-active');
			active = 1;
		} else if(active == 1) {
			$('.custom-menu').removeClass('custom-menu-active');
			active = 0;
		}
		
    	return false;
  	});

	$(document).on('click', function(){
		$('.custom-menu').removeClass('custom-menu-active');
		active = 0;
	});

	$('.custom-menu-item').on('click', function(){
		active = 0;
	});

	

	$(document).keydown(function(e){
		if(e.which == 114){
			$('#all').trigger('click');
			return false;
		}

		if(e.which == 113){
			$('.ui-selected').find('.JSEditNazivBtn').trigger('click');
			return false;
		}

		if(e.which == 120){
			$('#povezi_artikle').trigger('click');
			return false;
		}

		if(e.which == 121){
			$('#razvezi_artikle').trigger('click');
			return false;
		}
		
	});
    

	$("#JSRefresh").click(function(){
			location.reload(true);
	});

	//modal article edit
	$('.JSEditRow').on('click', function(){
		var zakljucan = $(this).data('zakljucan');
		var sifra = $(this).data('sifra');
		var dc_id = $(this).closest('tr').data('id');

		if(zakljucan==0){
			$('#JSEditDCId').val(dc_id);

			$('#JSEditNaziv').removeClass('error');
			$('#JSEditKolicina').removeClass('error');
			$('#JSEditNC').removeClass('error');
			$('#JSEditWM').removeClass('error');
			$('#JSEditWC').removeClass('error');
			$('#JSEditMC').removeClass('error');
			$('#JSEditMM').removeClass('error');

			$.post(base_url+'admin/ajax/dc_articles', {action: 'product_data', dc_id: dc_id, sifra:sifra}, function (response){
				var data = $.parseJSON(response);

				$('#JSEditNaziv').val(data.naziv);				
				$('#JSOpis').val(data.opis);	
				$('#JSEditKolicina').val(data.kolicina);
				$('#JSEditNC').val(data.cena_nc);
				$('#JSEditWM').val(data.web_marza);
				$('#JSEditWC').val(data.web_cena);
				$('#JSEditMC').val(data.mpcena);
				$('#JSEditMM').val(data.mp_marza);
				$('#JSEditPorez').val(data.porez);
				var html_slike ='';
				for (var i =0; i < data.slike.length; i++) {
					html_slike += '<img class="edit-modl-img" src="'+data.slike[i]+'" >' ;
				}
				$('#JSDobSlike').html(html_slike);

				$('#JSEditFieldsModal').foundation('reveal', 'open');
			});
		}else{
			$('#JSEditYes').data('zakljucan-id',dc_id);
			$('#JSEditModal').foundation('reveal', 'open');			
		}
	});

	$('#JSEditYes').click(function(){
		var dc_id = $(this).data('zakljucan-id');
		$('.close-reveal-modal').trigger('click');

		var dataUpd = [{"table":"dobavljac_cenovnik", "column":"flag_zakljucan", "val":"0"},{"table":"roba", "column":"flag_zakljucan", "val":"false"}];
  		$.post(base_url+'admin/ajax/dc_articles', {action:'execute', data: dataUpd, dc_ids: [dc_id]}, function (response){
			alertify.success(translate("Artikal je otključan!"));
		});
		var obj_row = $("#selectable").find('[data-id="'+ dc_id +'"]');
	  	obj_row.find('.flag_zakljucan').text('NE');
	  	obj_row.find('.JSEditRow').data('zakljucan','0');

		$('#JSEditDCId').val(dc_id);
		$('#JSEditNaziv').removeClass('error');
		$('#JSEditKolicina').removeClass('error');
		$('#JSEditNC').removeClass('error');
		$('#JSEditWM').removeClass('error');
		$('#JSEditWC').removeClass('error');
		$('#JSEditMC').removeClass('error');
		$('#JSEditMM').removeClass('error');

		$.post(base_url+'admin/ajax/dc_articles', {action: 'product_data', dc_id: dc_id}, function (response){
			var data = $.parseJSON(response);

			$('#JSEditNaziv').val(data.naziv);
			$('#JSEditKolicina').val(data.kolicina);
			$('#JSOpis').val(data.opis);
			$('#JSEditNC').val(data.cena_nc);
			$('#JSEditWM').val(data.web_marza);
			$('#JSEditWC').val(data.web_cena);
			$('#JSEditMC').val(data.mpcena);
			$('#JSEditMM').val(data.mp_marza);
			$('#JSEditPorez').val(data.porez);

			$('#JSEditFieldsModal').foundation('reveal', 'open');
		});

	});
	$('#JSEditNo').click(function(){
		$('.close-reveal-modal').trigger('click');
	});
	$('#submitBtn').on('click', function(){
		var data = {
			action: 'product_submit',
			dobavljac_cenovnik_id: $('.JSdcid').val(),
			naziv: $('#JSEditNaziv').val(),
			opis: $('#JSOpis').val(),
			kolicina: $('#JSEditKolicina').val(),
			cena_nc: $('#JSEditNC').val(),
			web_marza: $('#JSEditWM').val(),
			web_cena: $('#JSEditWC').val(),
			mpcena: $('#JSEditMC').val(),
			mp_marza: $('#JSEditMM').val()
		}

		$.post(base_url+'admin/ajax/dc_articles', data, function (response){
			var res_data = $.parseJSON(response);
			if(res_data.message!='success'){
				var errors = res_data.errors;
				if(errors.naziv!=''){
					$('#JSEditNaziv').addClass('error');
				}
				else{
					$('#JSEditNaziv').removeClass('error');
				}
				if(errors.opis!=''){
					$('#JSOpis').addClass('error');
				}
				else{
					$('#JSOpis').removeClass('error');
				}
				if(errors.kolicina!=''){
					$('#JSEditKolicina').addClass('error');
				}
				else{
					$('#JSEditKolicina').removeClass('error');
				}
				if(errors.cena_nc!=''){
					$('#JSEditNC').addClass('error');
				}
				else{
					$('#JSEditNC').removeClass('error');
				}
				if(errors.web_marza!=''){
					$('#JSEditWM').addClass('error');
				}
				else{
					$('#JSEditWM').removeClass('error');
				}
				if(errors.web_cena!=''){
					$('#JSEditWC').addClass('error');
				}
				else{
					$('#JSEditWC').removeClass('error');
				}
				if(errors.mpcena!=''){
					$('#JSEditMC').addClass('error');
				}
				else{
					$('#JSEditMC').removeClass('error');
				}
				if(errors.mp_marza!=''){
					$('#JSEditMM').addClass('error');
				}
				else{
					$('#JSEditMM').removeClass('error');
				}
			}else{
				var res_data = res_data.data;
				$('.close-reveal-modal').trigger('click');
				alertify.success(translate('Uspesno ste sačuvali izmene.'));
				var target = $("#selectable").find('[data-id="'+ data.dobavljac_cenovnik_id +'"]');
				target.find('.naziv').text(res_data.naziv);
				if(res_data.kolicina != null){
					target.find('.kolicina').text(res_data.kolicina);
				}
				if(res_data.cena_nc != null){
					target.find('.cena_nc').text(res_data.cena_nc);
					var pdv = parseInt(target.find('.tarifna_grupa_id').text());
					var cena_pdv = Math.round(res_data.cena_nc*(1+parseInt(pdv)/100)).toString()+'.00';
					target.find('.cena_nc_pdv').text(cena_pdv);					
					$('#nc-col').width($('.cena_nc').width());
					$('#ncp-col').width($('.cena_nc_pdv').width());
				}
				if(res_data.web_marza != null){
					target.find('.web_marza').text(res_data.web_marza);
					$('#wm-col').width($('.web_marza').width());
				}
				
				if(res_data.opis != '' && res_data.opis != null){
					target.find('.flag_opis_postoji').html('<i class="fa fa-check green" aria-hidden="true"></i>');
					$('#op-col').width($('.flag_opis_postoji').width());				
				}else{
					target.find('.flag_opis_postoji').html('<i class="fa fa-times red" aria-hidden="true"></i>');
					
				}
		
				if(res_data.web_cena != null){
					target.find('.cena_web_marza').text(res_data.web_cena);
					$('#wc-col').width($('.cena_web_marza').width());
				}
				if(res_data.mpcena != null){
					target.find('.cena_mp_marza').text(res_data.mpcena);
					$('#mc-col').width($('.cena_mp_marza').width());
				}
				if(res_data.mp_marza != null){
					target.find('.mp_marza').text(res_data.mp_marza);
					$('#mpm-col').width($('.mp_marza').width());
				}

			}

		});
	});

	$('.JSEditNCbtn').on("click", function(){
		var porez = $('#JSEditPorez').val();
		var cena_nc = $('#JSEditNC').val();
		var marza_web = $('#JSEditWM').val();
		var marza_mp = $('#JSEditMM').val();
		if($.isNumeric(cena_nc) && $.isNumeric(marza_web)){
			var res_web = Math.round((1+porez/100)*cena_nc*(1+(marza_web/100)))+'.00';
			$('#JSEditWC').val(res_web);
		}
		if($.isNumeric(cena_nc) && $.isNumeric(marza_mp)){
			var res_mp = Math.round((1+porez/100)*cena_nc*(1+marza_mp/100))+'.00';
			$('#JSEditMC').val(res_mp);
		}
	});
	$('.JSEditWMbtn').on("click", function(){
		var porez = $('#JSEditPorez').val();
		var cena_nc = $('#JSEditNC').val();
		var marza_web = $('#JSEditWM').val();
		console.log(porez, cena_nc, marza_web);
		if($.isNumeric(cena_nc) && $.isNumeric(marza_web)){
			var res_web = Math.round((1+porez/100)*cena_nc*(1+(marza_web/100)))+'.00';
			$('#JSEditWC').val(res_web);
		}
	});
	$('.JSEditMMbtn').on("click", function(){
		var porez = $('#JSEditPorez').val();
		var cena_nc = $('#JSEditNC').val();
		var marza_mp = $('#JSEditMM').val();
		if($.isNumeric(cena_nc) && $.isNumeric(marza_mp)){
			var res_mp = Math.round((1+porez/100)*cena_nc*(1+marza_mp/100))+'.00';
			$('#JSEditMC').val(res_mp);
		}
	});
	$('.JSEditWCbtn').on("click", function(){
		var porez = $('#JSEditPorez').val();
		var cena_nc = $('#JSEditNC').val();
		var cena_web = $('#JSEditWC').val();
		if($.isNumeric(cena_nc) && $.isNumeric(cena_web)){
			var res_wm = ((cena_web / (cena_nc*(1+porez/100)))-1) * 100;
			$('#JSEditWM').val(round(res_wm,2));
		}
	});
	$('.JSEditMCbtn').on("click", function(){
		var porez = $('#JSEditPorez').val();
		var cena_nc = $('#JSEditNC').val();
		var cena_mc = $('#JSEditMC').val();
		if($.isNumeric(cena_nc) && $.isNumeric(cena_mc)){
			var res_mm = ((cena_mc / (cena_nc*(1+porez/100)))-1) * 100;
			$('#JSEditMM').val(round(res_mm,2));
		}
	});


	// (function($){ 
	// 	$('#selectable').find('tr').find('td').css('min-width',34);
	// 	$('.but-col').css('min-width',32);
	// 	$('.dob-col').css('min-width',37);
	// 	$('.check-roba_id').css('min-width',60);
	// 	$('.sfr-col').css('min-width',50);
	// 	$('.kolicina').css('min-width',50);
	// 	$('.mp_marza').css('min-width',40);
	// 	$('.model').css('min-width',60);
	// 	$('.cena_web_marza').css('min-width',60);
	// 	$('.cena_nc_pdv').css('min-width',60);
	// 	$('.cena_mp_marza').css('min-width',50);
	// 	$('.pmc-col').css('min-width',40);
	// 	$('.gr-col').css('min-width',50);
	// 	$('.pgr-col').css('min-width',110);
	// 	$('.grupa_pr_id').css('min-width',90);
	// 	$('.proizvodjac_id').css('min-width',120);
	// 	$('.pro-col').css('min-width',90);
	// 	$('.flag_aktivan').css('min-width',36);
	// 	$('.flag_prikazi_u_cenovniku').css('min-width',36); 
	// 	$('.sl-col').css('min-width',45); 

	// 	$('#but-col').width($('.but-col').width());
	// 	$('#dob-col').width($('.dob-col').width());
	// 	$('#rid-col').width($('.check-roba_id').width());
	// 	// $('#sku-col').width($('.sku-col').width());
	// 	$('#sfr-col').width($('.sfr-col').width());
	// 	$('#akt-col').width($('.flag_aktivan').width());
	// 	$('#fpc-col').width($('.flag_prikazi_u_cenovniku').width());
	// 	$('#op-col').width($('.flag_opis_postoji').width());
	// 	$('#naz-col').width($('.naziv').width());
	// 	$('#kol-col').width($('.kolicina').width());
	// 	$('#wc-col').width($('.cena_web_marza').width());
	// 	$('#wm-col').width($('.web_marza').width());
	// 	$('#nc-col').width($('.cena_nc').width());
	// 	$('#ncp-col').width($('.cena_nc_pdv').width());
	// 	$('#mc-col').width($('.cena_mp_marza').width());
	// 	$('#mpm-col').width($('.mp_marza').width());
	// 	$('#pmc-col').width($('.pmc-col').width());
	// 	$('#pdv-col').width($('.tarifna_grupa_id').width());
	// 	$('#gr-col').width($('.gr-col').width());
	// 	$('#pgr-col').width($('.pgr-col').width());
	// 	$('#ngr-col').width($('.grupa_pr_id').width());
	// 	$('#pro-col').width($('.pro-col').width());
	// 	$('#npr-col').width($('.proizvodjac_id').width());
	// 	// $('#mod-col').width($('.model').width());
	// 	$('#zak-col').width($('.flag_zakljucan').width()); 
	// 	$('#sl-col').width($('.sl-col').width()); 
 
	// 	// $('.table-scroll').scroll(function(){
	// 	// 	var left_pos = $(this).find('.dc_table').position().left;
	// 	// 	$('#table_header').offset({ left: left_pos });
	// 	// });

		
	// 	// var headcolumns = $('.table-head');
	// 	// var columns = $('#selectable').find('tr').first().find('td');
	// 	// columns.each(function(i, obj){
	// 	// 	if(i<columns.length-1){			
	// 	// 		var width = $(obj).width();
	// 	// 		var new_width = width*(1-10/100);
	// 	// 		$(headcolumns.get(i)).width(new_width);
	// 	// 	}
	// 	// });	
	// })(jQuery);	
});
 
function round(value, exp) {
  if (typeof exp === 'undefined' || +exp === 0)
    return Math.round(value);

  value = +value;
  exp = +exp;

  if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0))
    return NaN;

  // Shift
  value = value.toString().split('e');
  value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp)));

  // Shift back
  value = value.toString().split('e');
  return +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
}



// BRZI EDIT kolicina
$('.JSkolicinaInput').hide();

$('.JSEditKolicinaBtn').on('click', function(){
	$(this).parent().children('.JSkolicinaInput').show();
	$(this).parent().children('.JSkolicinaInput').focus();
});

$('.JSkolicinaInput').focusout(function(){
	$(this).css('display', 'none');
});

$('.JSkolicinaInput').on("keyup", function(event) {
	var val = $(this).val();
	var dobavljac_cenovnik_id = $(this).closest('tr').data('id');

	if(event.keyCode == 13){
		$(this).closest('.kolicina').children('.JSkolicinaVrednost').html(val);
    	$(this).focusout();
    	$.post(base_url+'admin/ajax/dc_articles', {action:'fast_edit_quantity', dobavljac_cenovnik_id: dobavljac_cenovnik_id, val: val}, function (response){
    		alertify.success(translate('Uspešno ste promenili količinu'))
    	});
   	}
});
// end brzi edit kolicina


// brzi edit naziv
$('.JSnazivInput').hide();

$('.JSEditNazivBtn').on('click', function(){
	$(this).parent().children('.JSnazivInput').show();
	$(this).parent().children('.JSnazivInput').focus();
});

$('.JSnazivInput').focusout(function(){
	$(this).css('display', 'none');
});

$('.JSnazivInput').on("keyup", function(event) {
	var val = $(this).val();
	var art_id = $(this).closest('tr').data('id');

	if(event.keyCode == 13){
		$(this).closest('.naziv').children('.JSnazivVrednost').html(val);
    	$(this).focusout();

    	$.post(base_url+'admin/ajax/dc_articles', {action:'fast_edit_name', art_id: art_id, val: val}, function (response){
    		alertify.success(translate('Uspešno ste sačuvali naziv'))
    	});
   	}
});



// end brzi edit naziv


// brzi edit sifra
$('.JSsifraInput').hide();

$('.JSEditSifraBtn').on('click', function(){
	$(this).parent().children('.JSsifraInput').show();
	$(this).parent().children('.JSsifraInput').focus();
});

$('.JSsifraInput').focusout(function(){
	$(this).css('display', 'none');
});

$('.JSsifraInput').on("keyup", function(event) {
	var val = $(this).val();
	var art_id = $(this).closest('tr').data('id');

	if(event.keyCode == 13){
		$(this).closest('.sifra').children('.JSsifraVrednost').html(val);
    	$(this).focusout();

    	$.post(base_url+'admin/ajax/dc_articles', {action:'fast_edit_sifra', art_id: art_id, val: val}, function (response){
    		console.log(response);
    	});
   	}
});
// end brzi edit sifra


// GRUPA AJAX REFRESH
$(window).focus(function(){
	var val = $('select#grupa_select').val();
	var val2 = $('select#webImportGrupa').val();

	$.get(base_url+'admin/ajax/import/groups', function (response){
		$('#grupa_select').html(response);
		$('select#grupa_select').val(val);
		$('#webImportGrupa').html(response);
		$('select#webImportGrupa').val(val2);
	});	

	var val3 = $('select#webImportProizvodjac').val();
	$.get(base_url+'admin/ajax/import/brands', function (response){
		$('select#webImportProizvodjac').html(response);
		$('select#webImportProizvodjac').val(val3);
	});	
	
	$.get(base_url+'admin/ajax/import/brands_mul', {criteria: criteriaImport[1]}, function (response){
		$('select#proizvodjac_select').html(response);
	});	

	// var val4 = $('select#webImportTarifnagrupa').val();
	// $.get(base_url+'admin/ajax/import/tarifna', function (response){
	// 	$('select#webImportTarifnagrupa').html(response);
	// 	$('select#webImportTarifnagrupa').val(val4);	
	// });
	
});



$(document).ready(function () {

	$('.JSdeleteProperty').on('click', function(e){

		var link = $(this).attr('href');

		alertify.confirm(translate("Da li ste sigurni da želite da obrišete osobinu?<br>Biće obrisane i vrednosti vezane za tu osobinu!"), function (e) {

		if (e) {
		    document.location.href = link;
			}
		});

		return false;
	});

	$('.JSdeletePropertyValue').on('click', function(e){

		var link = $(this).attr('href');

		alertify.confirm(translate("Da li ste sigurni da želite da obrišete vrednost?"), function (e) {

		if (e) {
		    document.location.href = link;
			}
		});

		return false;
	});

    $('#JSosobine').on('change', function(){
        var osobina_naziv_id = $(this).find(':selected').data('id');
        location.href = base_url + 'admin/osobine/709' + osobina_naziv_id;
    });

	$(function() {
     
    $('#JSListOsobine').sortable({
        //observe the update event...
        update: function(event, ui) {
            //create the array that hold the positions...
            var moved = ui.item[0].id;
            var order = []; 
            //loop trought each li...
            $('#JSListOsobine li').each( function(e) {
            //add each li position to the array...     
            // the +1 is for make it start from 1 instead of 0
            order.push( $(this).attr('id') );                
            });
            $.ajax({
                type: "POST",
                url: base_url+'admin/position-osobine',
                data: {order:order, moved:moved},
                success: function(msg) {
                   console.log('test');
                }
            });
        }
    });                    
    $( "#JSListOsobine").disableSelection();
                        
    });  

    $('#JSOsobinaKombinacijaNaziv').on('change', function(){
        var osobina_naziv_id = $(this).val();
        $.ajax({
            type: "POST",
            url: base_url+'admin/product-osobine-naziv-vrednosti',
            data: {osobina_naziv_id: osobina_naziv_id},
            success: function(response) {
                $('#JSOsobinaKombinacijaVrednosti').html(response);
            }
        });
    });

});
$(document).ready(function() {
	var live_base_url= $('#live_base_url').val();
	
	$(document).on('change','#JSOpstina',function(){
		$.post(live_base_url+'select-narudzbina-mesto',{narudzbina_opstina_id: $(this).val()},function(response){
			var response_arr = response.split('===DEVIDER===');
			$('#JSMesto').html(response_arr[0] ? response_arr[0] : '');
			$('#JSUlica').html(response_arr[1] ? response_arr[1] : '');
		});
	});

	$(document).on('change','#JSMesto',function(){
		$.post(live_base_url+'select-narudzbina-ulica',{narudzbina_mesto_id: $(this).val()},function(response){
			$('#JSUlica').html(response);
		});
	});

	$(document).on("change", "#JSPartnerId", function() {
		$('#JSPartnerNaziv').val($(this).find('.JSPartnerSearchNaziv').text());
		var id = $('#JSPartnerId').val();

		$.post(base_url + 'admin/ajax/partner-data', { partner_id:id }, function (response){
			var result = $.parseJSON(response);			
			$('#JSPartnerAdresa').val(result.adresa);
			$('#JSPartnerMesto').val(result.ptt_cityexpress);
		});		
	});
});